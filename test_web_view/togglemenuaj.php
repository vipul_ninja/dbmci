<!DOCTYPE html>
<html lang=en ng-app="myApp">
<head>
    <meta charset=utf-8 />
    <title>Ranjeet</title>
        <meta name=viewport content="width=device-width; initial-scale=1.0" />
        <link type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
        <link type=text/css href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" rel=stylesheet />
        <link type=text/css href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" rel=stylesheet />
        
        <link type=text/css href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel=stylesheet />
        <!-- <link type=text/css href="assets/css/style.css" rel=stylesheet /> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js">
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="css/slider.css">
        <script src="js/slider.js"></script>
</head>
<style type="text/css">
.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #fff;
    overflow-x: hidden;
    xtransition: 0.5s;
    padding-top: 60px;
}
.sidenav a {
    padding: 8px 8px 8px 12px;
    text-decoration: none;
    font-size: 16px;
    color: #818181;
    display: block;
    xtransition: 0.3s;
}
.sidenav a:hover {
    color: #f1f1f1;
}
.sidenav .closebtn {
    position: absolute;
    top: 0;
    left: 0;
    font-size: 20px;
    margin-left: 0px;
}
.sidenav .gridbtn{
    position: absolute;
    top:2px;
    right:22px; 
}
.sidenav .listbtn{
    position: absolute;
    top:2px;
    right: 0; 
}
.serachbtn{
    position: absolute;
    right: 60px;
    top: 7px;
    width: 150px;
    overflow: hidden;
}
.form-control:focus{
    box-shadow: 0 0 0 0.2rem #ced4da;
    border-color: transparent;
}
.serachbtn select{

    height: calc(1.8rem + 2px) !important;
    xborder-radius: 18px;
    font-size: 12px;

}
.btn-submit-section{
    border-radius: 0px !important;
    color: #fff !important;
    position: absolute;
    bottom: 0;
    width: 100%;
    font-size: 16px !important;
    font-weight: 600;
}
#main {
    xtransition: margin-left .5s;
    padding: 16px;
}
.statusofquestion{
    position: absolute;
    width: 109%;
    bottom: 40px;
    margin-left: -30px;
    background-color: #fff;
}
.statusofquestion ul{
    list-style: none;
}
.statusofquestion ul li{
    list-style: none;
    width: 50%;
    float: left;
    font-size: 12px;
}
.green{
  color: #28a745;
  font-size: 17px;
}
.red{
  color: red;
  font-size: 17px;
}
.gray{
  color: #ccc;
  font-size: 17px;
}
.yellow{
  color: yellow;
  font-size: 17px;
}
.questionciler{
    height: 85%;
    overflow: scroll;
    margin-top: -15px;
}
.questionciler a{
   padding:15px;
   margin: 10px;
   border-radius: 100%;
   color: #fff;
   width: 20px;
   height: 20px;
   float: left;
   position: relative;
}
.questionciler a p{
   position: absolute;
   top: 4px;
   left: 8px;
   font-size: 13px;
}
.listview a{
  float: inherit !important;
}
.listview{
  display: none;
}
.listview ul{
  list-style: none;
  margin-left: -41px;
}
.listview ul li{
  float: left;
  width: 80%;
  font-size: 16px;
  height: 41px;
  overflow: hidden;
}
.mrtop{
  margin-top: 13px;
}
.icons-width{
  width: 20% !important;
  float: left;
}
.right-answer{
   background-color: #28a745;
}
.wrong-answer{
   background-color: red;
}
.attempt{
   background-color: #3a3a00;
}
.unattempt{
   background-color: #ccc;
}
.togllemenu{
  font-size: 16px;
  font-weight: bolder;
  cursor:pointer;
  color: #7d7777;
}

/*****------ header **/
.header-tab-menu{
  width: 20%;
  display: inline-block;
  float: left;
}
.header-tab-time{
  width: 58%;
  float: left;
}
.header-tab-lang{
  width: 20%;
  float: left;
  margin-bottom: 20px;
}
.togll-lang{
  float: right;
  font-size: 18px;
  color: #7d7777 !important;
}
.header-tab-time a{
  float: left;
  margin-right: 10px;
  margin-left: 52px;
  font-size: 18px;
  color: #7d7777 !important;
}
.subject-cate select{
  font-size: 14px;
}

/*------- question section ----*/
.questions-section{
  padding: 16px;
}
.question-q{
  font-size: 13px;
}
.question-q p{
  color: #615d5d;
}
.question-q p span{
  font-weight: bolder;
}
.subject-cate{
  padding: 15px;
}
 .swiper-container { width: 100%;}
 .swiper-slide {overflow: hidden;}





@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
@media screen and (max-width: 360px) {
  .questionciler{
    height: 79%;
  }
  .statusofquestion ul li{
    font-size: 10px;
  }
  .btn-submit-section{
    font-size: 14px !important;
  }
}

</style>
<body>
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn float-left" onclick="closeNav()"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
  <div class="serachbtn">
     <select class="form-control" id="sel1">
        <option>General</option>
        <option>General 2</option>
        <option>General 3</option>
        <option>General 4</option>
      </select>
  </div>
  <a  class="float-right gridbtn" ><i class="fa fa-th-large" aria-hidden="true"></i></a>
  <a  class="float-right listbtn" ><i class="fa fa-bars" aria-hidden="true"></i></a>
   
  <div class="questionciler">
  <a href="#" class="right-answer"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="right-answer"><p>22</p></a>
  <a href="#" class="wrong-answer"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="right-answer"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="right-answer"><p>22</p></a>
  <a href="#" class="wrong-answer"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="right-answer"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="right-answer"><p>22</p></a>
  <a href="#" class="wrong-answer"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="right-answer"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="right-answer"><p>22</p></a>
  <a href="#" class="wrong-answer"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="attempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
  <a href="#" class="unattempt"><p>22</p></a>
   </div> 
   <div class="questionciler listview">
    <ul>
      <li class="icons-width"><a href="#" class="unattempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="attempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="wrong-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="right-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="unattempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="attempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="wrong-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="right-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="unattempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="attempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="wrong-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="right-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="unattempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="attempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="wrong-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="right-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="unattempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="attempt"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="wrong-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
      <li class="icons-width"><a href="#" class="right-answer"><p>22</p></a></li>
      <li><p class="mrtop">What is Ranjeet ?</p></li>
    </ul>
   </div> 

  <div class="statusofquestion">
    <ul>
      <li><span class="green">&#9679;</span> Answered</li>
      <li><span class="red">&#9679;</span> Unanswered</li>
      <li><span class="gray">&#9679;</span> Not Visited</li>
      <li><span class="yellow">&#9679;</span> Marked for Reviwe</li>
    </ul>
  </div>
  <a href="" class="btn btn-success btn-submit-section">SUBMIT SECTION</a>
</div>

<div id="main">
          <div class="header-tab-menu">
            <span class="togllemenu" onclick="openNav()"><i class="fa fa-th" aria-hidden="true"></i></span>
          </div>
       <div class="header-tab-time">
          <a class="toglle-time"><i class="fa fa-pause-circle" aria-hidden="true"></i></a><p id="demo"></p>
       </div>
       <div class="header-tab-lang">
          <span class="togll-lang"><i class="fa fa-language" aria-hidden="true"></i></span>
       </div>
</div>
<div id="subject">
        <div class="subject-cate">
            <select class="form-control" id="sel1">
              <option>General</option>
              <option>General 2</option>
              <option>General 3</option>
              <option>General 4</option>
            </select>
        </div>  
</div>
<div class="questions-section">
    
    <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <div class="">
          <div class="question-q">
             <p><span>Q.1</span> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
               <form>
              <div class="radio">
             <label><input type="radio" name="optradio">Option 1</label>
             </div>
            <div class="radio">
            <label><input type="radio" name="optradio">Option 2</label>
          </div>
           <div class="checkbox">
            <label><input type="checkbox" value="">Option 1</label>
           </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Option 2</label>
                  </div>
               </form>
             </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="">
          <div class="question-q">
      <p><span>Q.1</span> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
      <form>
    <div class="radio">
      <label><input type="radio" name="optradio">Option 1</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="optradio">Option 2</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" value="">Option 1</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" value="">Option 2</label>
    </div>
  </form>
    </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="">
          <div class="question-q">
      <p><span>Q.1</span> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
      <form>
    <div class="radio">
      <label><input type="radio" name="optradio">Option 1</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="optradio">Option 2</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" value="">Option 1</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" value="">Option 2</label>
    </div>
  </form>
    </div>
        </div>
      </div>
    </div>
</div>
</div> 

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    // document.getElementById("main").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "white";
}
</script>
<script type="text/javascript">
  $(".gridbtn").click(function(){
    $(".questionciler").show();
    $(".listview").hide();
});
   $(".listbtn").click(function(){
    $(".questionciler").hide();
    $(".listview").show();
});
</script>

<!-- time counter -->
<script type="text/javascript">
 
// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2019 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("demo").innerHTML =  hours + ":"
  + minutes + ":" + seconds + "";

  // If the count down is finished, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>
<!-- screeen slider -->
<script type="text/javascript">
  var swiper = new Swiper('.swiper-container', {
      zoom: true,
      pagination: {
        el: '.swiper-pagination',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
</script>
</body>
</html>
