<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

// following files need to be included
require_once("./lib/config_paytm.php");
require_once("./lib/encdec_paytm.php");


/*

key-: description -: type -: required 

MID -: This is a unique merchant Id provided to merchant by Paytm at the time of integration.
 -: Mixed -: Yes

TXNID -: This is a unique Paytm transaction Id that is issued by Paytm for each valid transaction request received from the merchant.
-: String -: Yes

ORDERID -: This is the application transaction Id that was sent by merchant to Paytm at the time of transaction request.
-: String -: Yes

REFUNDAMOUNT -: Can be equal to or lesser than the txn amount
-: String -: Yes

TXNTYPE -: Any one of below values: REFUND
-: String -: Yes

CHECKSUM -: URL encoded checksum which is calculated based on a pre defined logic. This is used to verify the integrity of the transaction.
-:String -: Yes

COMMENTS -: Any comments can be given here
-: String -: No

REFID -: Unique ID for every refund request sent by merchant to Paytm
-: Alphanumeric (length: upto 20 characters) -:	Yes

*/

$ORDER_ID = "";
$requestParamList = array();
$responseParamList = array();
$REFID = time();
$requestParamList = array("MID" => $_GET['MID'] ,
						 "TXNID" =>  $_GET['TXNID'],
						 "ORDERID" =>  $_GET['ORDERID'],
						 "REFUNDAMOUNT" =>  $_GET['REFUNDAMOUNT'],
						 "TXNTYPE" => 'REFUND',
						 "REFID" => "$REFID"
						);  
//echo "<pre>";
//print_r($requestParamList);
//echo "</pre>";
$checkSum = getRefundChecksumFromArray($requestParamList,"nqu%88@X8m6CIdXY");//PAYTM_MERCHANT_KEY);
$requestParamList['CHECKSUMHASH'] = urlencode($checkSum);

$data_string = "JsonData=".json_encode($requestParamList);

$ch = curl_init();  
// initiate curl
$url = "https://secure.paytm.in/oltp/HANDLER_INTERNAL/REFUND?";

// if($_GET['SERVER'] == "LIVE"){
// echo 	$url = "https://secure.paytm.in/oltp/HANDLER_INTERNAL/REFUND?";
// 	//$url = "https://securegw.paytm.in/refund/HANDLER_INTERNAL/REFUND?"; // where you want to post data
// }elseif($_GET['SERVER'] == "LOCAL"){
// 	$url = "https://securegw-stage.paytm.in/refund/HANDLER_INTERNAL/REFUND?"; // where you want to post data
// }

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string); // define what you want to post
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string fo	rmat
$headers = array();
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$output = curl_exec($ch); // execute
$info = curl_getinfo($ch);
$data = json_decode($output, true);
echo json_encode($data);
