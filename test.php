<?php
function sendAndroidPush($deviceToken, $msg,$badge=0,$check=0,$type="") {
     $registrationIDs = array($deviceToken);
 
     if (is_array($deviceToken)) {
 
         $registrationIDs = $deviceToken;
     } else {
         $registrationIDs = array($deviceToken);
     }
     // Message to be sent
     $message = $msg;
     $type = json_decode($msg,true);
     $type = $type['notification_code'];
     $url = 'https://android.googleapis.com/gcm/send';
 
     $fields = array(
         'registration_ids' => $registrationIDs,
         'data' => array("message" => $message,"type"=> $type)
     );
 
     $headers = array(
         'Authorization: key=AIzaSyDIg-lfBU35stC18Kc-HxWDmE_rdjAPeUU',
         'Content-Type: application/json'
     );
 
     $ch = curl_init();
 
     //Set the url, number of POST vars, POST data
     curl_setopt($ch, CURLOPT_URL, $url);
 
     curl_setopt($ch, CURLOPT_POST, true);
     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
     curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
     //Execute post
     $result = curl_exec($ch);
 
     curl_close($ch);
 
     return  $result;
} 

 echo "<pre>";
 $push_data = json_encode(
    array(
        'notification_code' => 701,
        'message' => "DO NOT OPEN !! testing message for android.",
        'data' => array('user_id' =>1)
    )
);
 $return = sendAndroidPush($_GET['tokken'], $push_data);
 print_R($return);

