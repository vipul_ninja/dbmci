<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
class Add_post extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		if( $this->session->userdata('web_panel_active_backend_user_id') == null){
			redirect("web_panel/user/login/index");
		}
	}

	public function index(){
		$data = array();

		if( $_POST && isset($_POST['normal_post']) ){
			$data['result'] = $this->save_normal_post();
		}
		if($_POST &&   isset($_POST['mcq_post']) ){
			$data['result'] = $this->save_mcq_post();
			$data['form']   = 'mcq';
			
		}
		$this->load->view("user/add_post",$data);
	}

	public function amazon_s3_upload($name,$aws_path) {
		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');
				
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).$_FILES["file"]["name"], 
							'SourceFile' => $_FILES["file"]["tmp_name"], 
							'ContentType' => 'image', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	
}

	public function amazon_s3_upload_videos($name,$aws_path) {
		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');
				
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => rand(0,7896756).str_replace(" ","-",$_FILES["file"]["name"]), 
							'SourceFile' => $_FILES["file"]["tmp_name"], 
							'ContentType' => 'video', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));
				$data=$result->toArray();
				//echo "<pre>";print_r($data);die;
				$video = explode('/',$data['ObjectURL']);
				return end($video);
				
				
	
	}
	public function save_normal_post(){
		$file =  array();
		if($_FILES['video']['error'] == 0) {
			$video = $this->amazon_s3_upload_videos($_FILES['video'],FANWALL_IMAGES_S3);
			$info['file_type'] = "video";
			$info['link'] = $video;
			$info['file_info'] = $video;
			$file[] = $info;  

			$v_name = explode('.',$video);
			$v_name = $v_name[0];
			require_once(FCPATH.'aws/aws-autoloader.php');
				
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => VIDEO_THUMB_IMAGES_S3.'/'.$v_name.".png", 
							'SourceFile' => $_POST["thumb_image"], 
							'ContentType' => 'image', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));
		}
		if($_FILES['image1']['error'] == 0) {
			$image1 = $this->amazon_s3_upload($_FILES['image1'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image1;
			$info['file_info'] = "";
			$file[] = $info;  

		} 
		if($_FILES['image2']['error'] == 0) {
			$image2 = $this->amazon_s3_upload($_FILES['image2'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image2;
			$info['file_info'] = "";
			$file[] = $info;  
		}
		if($_FILES['image3']['error'] == 0) {
			$image3 = $this->amazon_s3_upload($_FILES['image3'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image3;
			$info['file_info'] = "";
			$file[] = $info;  
		}
		if($_FILES['image4']['error'] == 0) {
			$image4 = $this->amazon_s3_upload($_FILES['image4'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image4;
			$info['file_info'] = "";
			$file[] = $info;  
		} 
		if($_FILES['image5']['error'] == 0) {
			$image5 = $this->amazon_s3_upload($_FILES['image5'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image5;
			$info['file_info'] = "";
			$file[] = $info;  
		} 
		/*if($_FILES['video']['error'] == 0) {
			$video = $this->amazon_s3_upload($_FILES['video']);
		}*/
		if($_FILES['doc']['error'] == 0) {
			$doc = $this->amazon_s3_upload($_FILES['doc'],DOC_FOLDER_S3);
			$info['file_type'] = "doc";
			$info['link'] = $doc;
			$info['file_info'] = $_FILES['doc']['name'];
			$file[] = $info;  
		} 
		if($_FILES['pdf']['error'] == 0) {
			$pdf = $this->amazon_s3_upload($_FILES['pdf'],DOC_FOLDER_S3);
			$info['file_type'] = "pdf";
			$info['link'] = $pdf;
			$info['file_info'] = $_FILES['pdf']['name'];
			$file[] = $info;  
		} 
		if(count($file) == 0) { $file = ''; } else { $file = json_encode($file); }
		$option = array(
			'user_id'=> $this->session->userdata('web_panel_active_backend_user_id'),
			'post_type' => 'text',
			'text'=>$this->input->post('text'),
			'post_tag'=> $this->input->post('post_tag'),
			'file'=> $file
  );

	$ch = curl_init(site_url('data_model/user/post_text/add_post'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $option);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	curl_close($ch);

	$result = json_decode($result, true);
	return $result;
	}


	public function save_mcq_post() {

		$file =  array();
		if($_FILES['video']['error'] == 0) {
			//echo "<pre>";print_r($_FILES['video']);die;
			$video = $this->amazon_s3_upload_videos($_FILES['video'],FANWALL_IMAGES_S3);
			$info['file_type'] = "video";
			$info['link'] = $video;
			$info['file_info'] = $video;
			$file[] = $info;  
			
			$v_name = explode('.',$video);
			$v_name = $v_name[0];
			require_once(FCPATH.'aws/aws-autoloader.php');
				
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => VIDEO_THUMB_IMAGES_S3.'/'.$v_name.".png", 
							'SourceFile' => $_POST["thumb_image"], 
							'ContentType' => 'image', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));			
		}
		
		if($_FILES['image1']['error'] == 0) {
			$image1 = $this->amazon_s3_upload($_FILES['image1'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image1;
			$info['file_info'] = "";
			$file[] = $info;  

		} 
		if($_FILES['image2']['error'] == 0) {
			$image2 = $this->amazon_s3_upload($_FILES['image2'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image2;
			$info['file_info'] = "";
			$file[] = $info;  
		}
		if($_FILES['image3']['error'] == 0) {
			$image3 = $this->amazon_s3_upload($_FILES['image3'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image3;
			$info['file_info'] = "";
			$file[] = $info;  
		}
		if($_FILES['image4']['error'] == 0) {
			$image4 = $this->amazon_s3_upload($_FILES['image4'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image4;
			$info['file_info'] = "";
			$file[] = $info;  
		} 
		if($_FILES['image5']['error'] == 0) {
			$image5 = $this->amazon_s3_upload($_FILES['image5'],FANWALL_IMAGES_S3);
			$info['file_type'] = "image";
			$info['link'] = $image5;
			$info['file_info'] = "";
			$file[] = $info;  
		} 
		/*if($_FILES['video']['error'] == 0) {
			$video = $this->amazon_s3_upload($_FILES['video']);
		}*/
		if($_FILES['doc']['error'] == 0) {
			$doc = $this->amazon_s3_upload($_FILES['doc'],DOC_FOLDER_S3);
			$info['file_type'] = "doc";
			$info['link'] = $doc;
			$info['file_info'] = $_FILES['doc']['name'];
			$file[] = $info;  
		} 
		if($_FILES['pdf']['error'] == 0) {
			$pdf = $this->amazon_s3_upload($_FILES['pdf'],DOC_FOLDER_S3);
			$info['file_type'] = "pdf";
			$info['link'] = $pdf;
			$info['file_info'] = $_FILES['pdf']['name'];
			$file[] = $info;  
		} 
		if(count($file) == 0) { $file = ''; } else { $file = json_encode($file); }
		
		$option = array(
			'user_id'=> $this->session->userdata('web_panel_active_backend_user_id'),
			'question'=>$this->input->post('question'),
			'answer_one'=>$this->input->post('answer_one'),
			'answer_two'=>$this->input->post('answer_two'),
			'answer_three'=>$this->input->post('answer_three'),
			'answer_four'=>$this->input->post('answer_four'),
			'answer_five'=>$this->input->post('answer_five'),
			'post_tag'=> $this->input->post('post_tag'),
			'right_answer' => '1',
			'file'=> $file
  		);

	$ch = curl_init(site_url('data_model/user/post_mcq/add_mcq'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $option);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	curl_close($ch);

	$result = json_decode($result, true);
	return $result;
	}

	public function logout(){

		$newdata = array(
					    'web_panel_active_backend_user_id'    => null
					);

		$this->session->set_userdata($newdata);
		redirect("web_panel/user/add_post/index");
	}
}	