<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index(){

		if($this->input->post()){
			$this->db->where('email',$this->input->post('email'));
			$this->db->where('password',md5($this->input->post('password')));
			if($record = $this->db->get('users')->row_array()){
						$newdata = array(
						        'web_panel_active_backend_user_flag'  => True,
						        'web_panel_active_backend_user_id'    => $record['id'],
								'web_panel_active_user_data'		=> $record
						);

						$this->session->set_userdata($newdata);
				redirect("web_panel/user/add_post/index");
			}
			$this->session->set_flashdata('message', '<div class="text-center error">Invalid credentials.</div>');
		}

		$this->load->view("user/login");
	}
	
}	