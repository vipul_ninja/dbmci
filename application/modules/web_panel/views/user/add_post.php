<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Mosaddek">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
		<link rel="shortcut icon" href="img/favicon.png">

		<title>DAMS</title>

		<!-- Bootstrap core CSS -->
		<link href="<?php echo base_url(); ?>auth_panel_assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>auth_panel_assets/css/bootstrap-reset.css" rel="stylesheet">
		<!--external css-->
		<link href="<?php echo base_url(); ?>auth_panel_assets/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

		<!--right slidebar-->
		<link href="<?php echo base_url(); ?>auth_panel_assets/css/slidebars.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="<?php echo base_url(); ?>auth_panel_assets/css/style.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>auth_panel_assets/css/style-responsive.css" rel="stylesheet" />
		
		<link  rel="stylesheet" href="<?php echo base_url(); ?>auth_panel_assets/assets/bootstrap-datepicker/css/datepicker.css">	

		<!--toastr-->
		<link href="<?php echo base_url(); ?>auth_panel_assets/assets/toastr-master/toastr.css" rel="stylesheet" type="text/css" />

		<!--dynamic table-->
		<!--<link href="<?php echo base_url(); ?>auth_panel_assets/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>auth_panel_assets/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>auth_panel_assets/assets/data-tables/DT_bootstrap.css" />-->
		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
		<!--[if lt IE 9]>
		  <script src="<?php echo base_url(); ?>auth_panel_assets/js/html5shiv.js"></script>
		  <script src="<?php echo base_url(); ?>auth_panel_assets/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<section id="container" class="">
			<!--header start-->
			<header class="header white-bg">
				<div class="sidebar-toggle-box">
					<div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
				</div>
				<!--logo start-->
				<a href="" class="logo" >D<span>ams</span></a>
				<!--logo end-->
				<div class="nav notify-row" id="top_menu">
				</div>
				<div class="top-nav ">
					<ul class="nav pull-right top-menu">
						<li class="dropdown">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class ="fa fa-user"></i>
								<span class="username">ACCOUNT</span>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu extended logout">
								<div class="log-arrow-up"></div>
								<li><a class="  hide"  href="#"><i class=" fa fa-suitcase hide"></i>Profile</a></li>
								<li><a  class="  hide"  href="#"><i class="fa fa-cog hide"></i> Settings</a></li>
								<li><a  class=""  href="#"><i class="fa fa-bell-o hide "></i>
										Logged in as
										NORMAL USER								</a></li>
								<li><a href="<?php echo site_url('web_panel/user/add_post/logout');?>"><i class="fa fa-key"></i> Log Out</a></li>
							</ul>
						</li>

						<!-- user login dropdown end -->
					</ul>
				</div>
			</header>
			<!--header end-->
			<!--sidebar start-->
			<aside>

        <!-- sidebar menu end-->
    </div>
</aside>

			<!--sidebar end-->
			<!--main content start-->
			<section id="main-content">
				<section class="wrapper site-min-height">
					<div class="row">
						<div class="col-lg-12 hide">
							<ul class="breadcrumb">
								<li class="active capitalize">welcome page</li>
							</ul>
						</div>
						<div class="col-lg-12">
							<div class="row state-overview">
								<div class="col-lg-12">
								  <section class="panel">
								      <header class="panel-heading">
								          ADD NEW POST
								      </header>

								      <div class="panel-body">
  									      <select id="post_selection"  onchange="myFunction()"  class="form-control input-sm m-bot15">
                                              <option value="normal_post" >Normal text</option>
                                              <option value="mcq_post" >Post MCQ</option>
                                   
                                          </select>
								          <form role="form" action="" method="post" id="normal_post" enctype="multipart/form-data">
								          <?php if(isset($result)) { 
								          		if($result['status'] == 'true') { ?>
								          	 		<div class="form-group alert bg-success">
								          	  	<?php } else { ?>
								          	  		<div class="form-group alert bg-danger">
								          	  	<?php } ?>
								                  <label for="text"><?php echo $result['message']; ?></label>
								              </div>
								           <?php } ?>
								              <div class="form-group">
								                  <label for="text">ENTER TEXT</label>
								                  <textarea  class="col-md-12" name="text"></textarea>
								              </div>
												<div class="form-group">
													<label for="text">SELECT TAG</label>
													<select class="form-control input-sm m-bot15" name='post_tag'>
													<option value=''>Select Tag</option>
													<?php 

														$post_tags = $this->db->get('post_tags')->result_array();

														foreach($post_tags as $tags) { 
															echo "<option value='".$tags['id']."'>".$tags['text']."</option>";

														}
													?>
													</select>
												</div>
								              <div class="col-md-6">
									              <div class="form-group">
				                                      <label for="">Image 1</label>
				                                      <input type="file" id="" name="image1" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>
									              <div class="form-group">
				                                      <label for="">Image 2</label>
				                                      <input type="file" id="" name="image2" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>
	  								              <div class="form-group">
				                                      <label for="">Image 3</label>
				                                      <input type="file" id="" name="image3" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>
	  								              <div class="form-group">
				                                      <label for="">Image 4</label>
				                                      <input type="file" id="" name="image4" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>
				                                  <div class="form-group">
				                                      <label for="">Image 5</label>
				                                      <input type="file" id="" name="image5" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>								              
			                                  </div>

								              <div class="col-md-6">
									              <div class="form-group">
				                                      <label for="">Video file </label>
				                                      <input type="file" id="video_p" name="video" accept="video/mp4">
				                                      <p class="help-block"></p>
				                                  </div>

									              <div class="form-group">
				                                      <label for="">Doc file </label>
				                                      <input type="file" id="" name="doc" accept=".doc">
				                                      <p class="help-block"></p>
				                                  </div>				                                  							 																					<div class="form-group">
				                                      <label for="">pdf file </label>
				                                      <input type="file" id="" name="pdf" accept=".pdf">
				                                      <p class="help-block"></p>
				                                  </div>	             
			                                  </div>	
			                                  <input type="hidden" name="thumb_image" value="">			                                  	
								              <input type="submit" name="normal_post" value="submit" class="btn btn-info" />
								              
								          </form>

								          <form style="display:none" role="form" action="" method="post" id="mcq_post" enctype="multipart/form-data">
											<div class="form-group">
												<label for="text">SELECT TAG</label>
												<select class="form-control input-sm m-bot15" name='post_tag'>
												<option value=''>Select Tag</option>
												<?php 

													$post_tags = $this->db->get('post_tags')->result_array();

													foreach($post_tags as $tags) { 
														echo "<option value='".$tags['id']."'>".$tags['text']."</option>";

													}
												?>
												</select>
											</div>

								              <div class="form-group">
								                  <label for="text">ENTER Question</label>
								                  <textarea  class="col-md-12" name="question"	></textarea>

								              </div>
			                                  <div class="form-group has-success">
			                                      <label for="inputSuccess" class="col-sm-2 control-label col-lg-2">A</label>
			                                      <div class="col-lg-10">
			                                          <input  name="answer_one" type="text" id="inputSuccess" class="form-control">
			                                      </div>
			                                  </div>
			                                  <div class="form-group has-warning">
			                                      <label for="inputWarning" class="col-sm-2 control-label col-lg-2">B</label>
			                                      <div class="col-lg-10">
			                                          <input name="answer_two" type="text" id="inputWarning" class="form-control">
			                                      </div>
			                                  </div>
			                                  <div class="form-group has-error">
			                                      <label for="inputError" class="col-sm-2 control-label col-lg-2">C</label>
			                                      <div class="col-lg-10">
			                                          <input  name="answer_three" type="text" id="inputError" class="form-control">
			                                      </div>
			                                  </div>	
  			                                  <div class="form-group has-error">
			                                      <label for="inputError" class="col-sm-2 control-label col-lg-2">D</label>
			                                      <div class="col-lg-10">
			                                          <input  name="answer_four" type="text" id="inputError" class="form-control">
			                                      </div>
			                                  </div>	
  			                                  <div class="form-group has-error">
			                                      <label for="inputError" class="col-sm-2 control-label col-lg-2">E</label>
			                                      <div class="col-lg-10">
			                                          <input  name="answer_five" type="text" id="inputError" class="form-control">
			                                      </div>
			                                  </div>
			                                  <div class="col-md-6">
									              <div class="form-group">
				                                      <label for="">Image 1</label>
				                                      <input type="file" id="" name="image1" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>
									              <div class="form-group">
				                                      <label for="">Image 2</label>
				                                      <input type="file" id="" name="image2" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>
	  								              <div class="form-group">
				                                      <label for="">Image 3</label>
				                                      <input type="file" id="" name="image3" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>
	  								              <div class="form-group">
				                                      <label for="">Image 4</label>
				                                      <input type="file" id="" name="image4" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>
				                                  <div class="form-group">
				                                      <label for="">Image 5</label>
				                                      <input type="file" id="" name="image5" accept="image/*">
				                                      <p class="help-block"></p>
				                                  </div>								              
			                                  </div>
			                                  <div class="col-md-6">
									              <div class="form-group ">
				                                      <label for="">Video file </label>
				                                      <input type="file" id="video_t" name="video" accept="video/mp4">
				                                      <p class="help-block"></p>
				                                  </div>

									              <div class="form-group">
				                                      <label for="">Doc file </label>
				                                      <input type="file" id="" name="doc" accept=".doc">
				                                      <p class="help-block"></p>
				                                  </div>				                                  							 																					<div class="form-group">
				                                      <label for="">pdf file </label>
				                                      <input type="file" id="" name="pdf" accept=".pdf">
				                                      <p class="help-block"></p>
				                                  </div>	             
			                                  </div>			
			                                  <input type="submit" name="mcq_post" value="submit" class="btn btn-info" />			
			                                  <input type="hidden" name="thumb_image" value="">			              
								              <button class=" hide btn btn-info" type="submit">Submit</button>
								          </form>

								      </div>
								  </section>
								</div>


							</div>
						</div>

					</div>
				</section>
			</section>
			<!--main content end-->

			<!--footer start-->
			<footer class="site-footer">
				<div class="text-center">
					
					<a href="#" class="go-top">
						<i class="fa fa-angle-up"></i>
					</a>
				</div>
			</footer>
			<!--footer end-->
		</section>

		<!-- js placed at the end of the document so the pages load faster -->
		<script src="<?php echo base_url(); ?>auth_panel_assets/js/jquery.js"></script>
		<script src="<?php echo base_url(); ?>auth_panel_assets/js/bootstrap.min.js"></script>
		<script class="include" type="text/javascript" src="<?php echo base_url(); ?>auth_panel_assets/js/jquery.dcjqaccordion.2.7.js"></script>
		<script src="<?php echo base_url(); ?>auth_panel_assets/js/jquery.scrollTo.min.js"></script>
		<script src="<?php echo base_url(); ?>auth_panel_assets/js/slidebars.min.js"></script>
		<script src="<?php echo base_url(); ?>auth_panel_assets/js/jquery.nicescroll.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>auth_panel_assets/js/respond.min.js" ></script>

		<!--toastr-->
		<script src="<?php echo base_url(); ?>auth_panel_assets/assets/toastr-master/toastr.js"></script>

		<!--common script for all pages-->
		<script src="<?php echo base_url(); ?>auth_panel_assets/js/common-scripts.js"></script>

				<script>
            $(document).ajaxComplete(function (event, xhr, settings) {
                if (xhr.draw) {
                    alert("ALL current AJAX calls have completed");
                }
            });
		</script>

						<script type="text/javascript">
            var i = -1;
            var toastCount = 0;
            var $toastlast;
            function show_toast(type, text, title) {
                var shortCutFunction = type;
                var msg = text;
                var toastIndex = toastCount++;

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "3000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "10000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }

                if ($('#addBehaviorOnToastClick').prop('checked')) {
                    toastr.options.onclick = function () {
                        alert('You can perform some custom action after a toast goes away');
                    };
                }


                if (!msg) {
                    msg = getMessage();
                }

                $("#toastrOptions").text("Command: toastr["
                        + shortCutFunction
                        + "](\""
                        + msg
                        + (title ? "\", \"" + title : '')
                        + "\")\n\ntoastr.options = "
                        + JSON.stringify(toastr.options, null, 2)
                        );

                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;
                if ($toast.find('#okBtn').length) {
                    $toast.delegate('#okBtn', 'click', function () {
                        alert('you clicked me. i was toast #' + toastIndex + '. goodbye!');
                        $toast.remove();
                    });
                }
                if ($toast.find('#surpriseBtn').length) {
                    $toast.delegate('#surpriseBtn', 'click', function () {
                        alert('Surprise! you clicked me. i was toast #' + toastIndex + '. You could perform an action here.');
                    });
                }
            }



            $('#clearlasttoast').click(function () {
                toastr.clear(getLastToast());
            });
            $('#cleartoasts').click(function () {
                toastr.clear();
            });


           function myFunction(){
            		var id = $("#post_selection").val();
            		
            		if(id == "normal_post"){
            			$("#"+id).css("display","block");$("#mcq_post").css("display","none");
            		}
            		else{
            			$("#normal_post").css("display","none");$("#mcq_post").css("display","block");
            		}	
            }

            <?php if(isset($form) && $form == 'mcq'){?>
            	$('#post_selection').val('mcq_post').change();
            <?php } ?>	
		</script>
		<video style="display:none"  width="100px"  class="sam" controls autoplay muted ></video>
		<canvas style="display:none" ></canvas>	
		<script>
				// Get handles on the video and canvas elements
				var video = document.querySelector('video');
				var canvas = document.querySelector('canvas');
				// Get a handle on the 2d context of the canvas element
				var context = canvas.getContext('2d');
				// Define some vars required later
				var w, h, ratio;
				
				// Add a listener to wait for the 'loadedmetadata' state so the video's dimensions can be read
				video.addEventListener('loadedmetadata', function() {
					// Calculate the ratio of the video's width to height
					ratio = video.videoWidth / video.videoHeight;
					// Define the required width as 100 pixels smaller than the actual video's width
					w = video.videoWidth - 100;
					// Calculate the height based on the video's width and the ratio
					h = parseInt(w / ratio, 10);
					// Set the canvas width and height to the values just calculated
					canvas.width = w;
					canvas.height = h;			
				}, false);	
			
			// Takes a snapshot of the video
				function snap() {	
					// Define the size of the rectangle that will be filled (basically the entire element)
					context.fillRect(0, 0, w, h);
					// Grab the image from the video
					context.drawImage(video, 0, 0, w, h);
					var dataURL = canvas.toDataURL();
					$("input[name=thumb_image]").val(dataURL);
					$("input[name=normal_post]").prop('disabled', false).val("submit");
				}
	 			$(document).on("change", "#video_p", function(evt) {
	 			   $("input[name=normal_post]").prop('disabled', true).val("Video thumbnail is not ready please wait...");
				   var fileInput = document.getElementById('video_p');
				   var fileUrl = window.URL.createObjectURL(fileInput.files[0]);
				   $(".sam").html('').html('<source type="video/mp4" src="'+fileUrl+'" > ');
				    $("video").load();
				   setTimeout(function(){
					    snap();
					}, 3000);
				});
	 			$(document).on("change", "#video_t", function(evt) {
	 				
				   var fileInput = document.getElementById('video_t');
				   var fileUrl = window.URL.createObjectURL(fileInput.files[0]);
				   $(".sam").html('').html('<source type="video/mp4" src="'+fileUrl+'" > ');
				    $("video").load();
				   setTimeout(function(){
					    snap();
					}, 3000);
				});

		</script>	

	</body>
</html>
