<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Category_list_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    
    public function get_category_list() {
      	return  $this->db->get("master_category")->result_array();
    }

    public function get_sub_category_list_by_id($id) {
    	$this->db->where('id',$id);
    	return  $this->db->get("master_category_level_one")->row_array();
    }


    public function get_sub_category_list() {
    	return  $this->db->get("master_category_level_one")->result_array();
    }

    public function get_sub_sub_category_list_by_id($id) {
    	$this->db->where('id',$id);
    	return  $this->db->get("master_category_level_two")->row_array();
    }


    public function get_category($id){ 
        $id = $id['id'];
        $this->db->where('id',$id);
       // $sql ="SELECT cc.*,mc.text as main_cat
               // FROM   course_category as cc  
                //join master_category as mc on mc.id=cc.parent_fk where cc.id=$id ";
        //$result = $this->db->query($sql)->row_array();
        $result = $this->db->get('course_category')->row_array();
        return $result;
    }
     public function insert_category($data){ 
        $result = $this->db->insert('course_category',$data);
        return $result;
    }
    public function update_category($data){ 
        $id = $data['id'];
        $this->db->where('id',$id);
        $result = $this->db->update('course_category',$data);
        return $result;
    }

    public function get_course_group_by_id($id){
        $this->db->where('id',$id);
        $res=$this->db->get('course_group_type')->row_array();
        return $res;
    }

    
}
