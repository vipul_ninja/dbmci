<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Web_user_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    public function get_user_basic_detail($id) {
		$this->db->where('id',$id);
		return $query = $this->db->get('users')->row_array();
		
	}
    public function get_user_profile($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('users')->row_array();
		  
			if($query['is_course_register'] == 1) {
			$sql = $this->db->query('SELECT mc.text as Stream,mclo.text as Course,urd.interested_course,mclt.text as speciality from `user_registration_data` as urd INNER JOIN `master_category` as mc ON urd.master_id = mc.id INNER JOIN master_category_level_one mclo ON urd.master_id_level_one = mclo.id LEFT JOIN master_category_level_two mclt ON urd.master_id_level_two = mclt.parent_id where user_id="'.$query['id'].'"  ')->row_array();
				//echo $this->db->last_query();
			$query['stream'] = $sql['Stream'];
			$query['course'] = $sql['Course'];
			$query['interested_course'] = $sql['interested_course'];

			$interested_course = explode(',',$query['interested_course']);
			$query['interested_course']= array();
			foreach($interested_course as $key=>$value) {
				$this->db->select('text');
				$this->db->where('id',$value);
				$course = $this->db->get('course_intersted_in_list')->row_array();
				$query['interested_course'][$key] = $course['text'];
			}

			}
			
			//echo "<pre>";print_r($query);die;  
			return $query;  
    }
	
	public function get_user_registered_details($id){
		$this->db->where('user_id',$id);
		$query = $this->db->get('user_registration_data')->row_array();
		return $query;
	}
	
	public function get_user_instructor_details($id) {
				
		//$this->db->join('course_instructor_information', 'users.id = request_helping_hand.request_id');
		$this->db->where('user_id',$id);
		$query = $this->db->get('course_instructor_information')->row_array();
		  
			
		if($query){
		//echo "<pre>";print_r($query);die;  
		return $query; 
		}
		else{
			return false;
		}
    }
	
	public function get_instructor_rating_details_by_id($id) {
		$this->db->select("course_instructor_rating.*,users.name,DATE_FORMAT(FROM_UNIXTIME(course_instructor_rating.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time");
		$this->db->join('users','users.id=course_instructor_rating.user_id');
    	$this->db->where('course_instructor_rating.id',$id);
    	return $this->db->get('course_instructor_rating')->row_array();

    }


    public function update_user_status($status,$id) {
    	if($status == 'delete') {
    		$data=array('status'=>2);
    		$this->db->where('id',$id);
			return $this->db->update('users',$data);

    	} elseif($status == 'disable') {
    		$data=array('status'=>1);
    		$this->db->where('id',$id);
			return $this->db->update('users',$data);

    	} elseif($status == 'enable') {
    		$data=array('status'=>0);
    		$this->db->where('id',$id);
			return $this->db->update('users',$data);

    	}
    }
	
	public function add_instructor_id($id){  
		$data = array('user_id'=>$id);
		$this->db->where('user_id',$id);
		$checkStatus = $this->db->get('course_instructor_information')->row_array();
		if(empty($checkStatus)){		
			$result = $this->db->insert("course_instructor_information",$data);
		}
		else{
		
		}		
	}
	
	public function delete_review($id) {
		
    	$this->db->where('id',$id);
		$this->db->delete('course_instructor_rating');
    	return true;

    }
	
	public function update_user_name($data) 
	{
		//print_r($data);
		$this->db->set('name',$data['name']);
    	$this->db->where('id',$data['id']);
		$this->db->update('users');
		//echo $this->db->last_query();
    	return true;

	}
	public function update_desg_name($data) 
	{
		//print_r($data);
		$this->db->set('designation',$data['designation']);
    	$this->db->where('id',$data['id']);
		$this->db->update('users');
		//echo $this->db->last_query();
    	return true;

	}
	public function update_speciality($data) 
	{
		//print_r($data);
		$this->db->set('speciality',$data['speciality']);
    	$this->db->where('id',$data['id']);
		$this->db->update('users');
		//echo $this->db->last_query();
    	return true;

    }
	// this->db->set('last_login','current_login',false);
// $this->db->where('id','some_id');
// $this->db->update('login_table',$data);


}