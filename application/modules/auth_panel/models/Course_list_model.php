<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Course_list_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }


    public function get_course_list() {
    	return $this->db->get('course_intersted_in_title')->result_array();
    }


    public function get_sub_course_list_by_id($id) {
    	$this->db->where('id',$id);
    	return $this->db->get('course_intersted_in_list')->row_array();

    }

    public function get_course_image_exist_by_id($id) {
        $this->db->select('course_master.cover_image as image');
        $this->db->where('id',$id);
        return $this->db->get('course_master')->row_array();

    }

    public function delete_filter($id,$filter_name){
		$this->db->where('course_id',$id);
        $this->db->where('f_name',$filter_name);
		$result = $this->db->delete('course_filtration');
		return $result;
    }

  /*  public function get_topic_exist_in_course($id) {
        $sql = "SELECT cse.* , cstm.topic as topic_name ,(CASE WHEN ctfmm.file_type = 1
        THEN 'pdf' WHEN ctfmm.file_type = 2
        THEN 'ppt' WHEN ctfmm.file_type = 3
        THEN 'video'
        WHEN ctfmm.file_type = 4
        THEN 'epub'
        WHEN ctfmm.file_type = 5
        THEN 'doc' ELSE 'test' END ) AS file_type ,ctfmm.file_url , ctfmm.title , ctfmm.description ,ctsm.test_series_name , ctsm.description as test_description
        FROM course_segment_element as cse
        LEFT JOIN course_topic_file_meta_master as ctfmm ON cse.element_fk = ctfmm.id and cse.type != 'test'
        RIGHT JOIN course_segment_master as csm ON csm.topic_id = cse.segment_fk and csm.course_fk = $id and csm.is_preview = 1
        LEFT JOIN course_subject_topic_master as cstm ON cstm.id = csm.topic_id
        LEFT JOIN course_test_series_master as ctsm ON ctsm.id = cse.element_fk and cse.type = 'test'
        WHERE cse.id is not null
        ORDER BY csm.position desc";
        return $result=$this->db->query($sql)->num_rows();
    }*/

    public function get_topic_exist_in_course($id) {
        $sql = "SELECT cse.* ,ctm.id as topic_id ,  ctm.topic_name as topic_name ,ctfmm.id as file_id,
            (CASE
            WHEN ctfmm.file_type = 1 THEN 'pdf'
            WHEN ctfmm.file_type = 2 THEN 'ppt'
            WHEN ctfmm.file_type = 3 THEN 'video'
            WHEN ctfmm.file_type = 4 THEN 'epub'
            WHEN ctfmm.file_type = 5 THEN 'doc'
            ELSE 'test'
            END ) AS file_type ,
            ctfmm.file_url ,  ctfmm.title , ctfmm.description ,ctsm.id as test_series_id ,
             ctsm.test_series_name , ctsm.description as test_description
            FROM course_segment_element as cse
            LEFT JOIN course_topic_file_meta_master as ctfmm ON cse.element_fk = ctfmm.id and cse.type != 'test'
            RIGHT JOIN course_topic_master 	as ctm ON ctm.id = cse.segment_fk and ctm.course_fk = $id and ctm.is_preview = 1
            LEFT JOIN course_test_series_master as ctsm ON ctsm.id = cse.element_fk and cse.type = 'test'
            WHERE cse.id is not null
            ORDER BY ctm.position asc";
        return $result=$this->db->query($sql)->num_rows();
    }


    public function get_sub_course_list() {
    	return $this->db->get('course_intersted_in_list')->result_array();
    }

    public function get_course_list_by_id($id) {
    	$this->db->where('master_category',$id);
    	return $this->db->get('course_intersted_in_title')->result_array();
    }

	public function get_course_review_details_by_id($id) {
		$this->db->select("course_user_rating.*,users.name,DATE_FORMAT(FROM_UNIXTIME(course_user_rating.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time");
		$this->db->join('users','users.id=course_user_rating.user_id');
	$this->db->where('course_user_rating.id',$id);
	return $this->db->get('course_user_rating')->row_array();

	}

	public function delete_course_review($id) {
	$this->db->where('id',$id);
		$status = $this->db->delete('course_user_rating');
		if($status){
		return true;
		}
		else{
			return false;
		}

	}

	public function delete_course($id){
		$data['state'] = 1;
		$this->db->where('id',$id);
      	$result = $this->db->update("course_master",$data);
      	return true;

    }
    
    public function get_course_group(){
        $this->db->where('status',1);
        $res=$this->db->get('course_group_type')->result_array();
        return $res;
    }

}
