<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Backend_user extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    public function create_backend_user($data){
      $result = $this->db->insert("backend_user",$data);
      return $result;
    }
    public function get_user_data($id){
      $result = $this->db->select('bu.*,burp.permission_group_id')
      ->join('backend_user_role_permissions as burp', 'bu.id = burp.user_id','LEFT')
      ->where('bu.id',$id)
      ->get("backend_user as bu")->row_array();
      
      return $result;
     
    }
    public function update_backend_user($data,$id){
      $this->db->where('id',$id);
      $result = $this->db->update("backend_user",$data);
      return $result;
    }
    public function delete_backend_user($id){
      $data = array('status' =>2);
      $this->db->where('id',$id);
      $result = $this->db->update("backend_user",$data);
    }
    public function block_backend_user($id,$status){
      $data = array('status' =>$status);
      $this->db->where('id',$id);
      $result = $this->db->update("backend_user",$data);
    }

     public function change_password_backend_user($data){
      $data_array = array('password'=> md5($data['new_password']));
      $result = $this->db->where('id',$data['id'])->update("backend_user",$data_array);
    }

    public function getStudentRecords() {
        $sql_result = $this->db->get("users")->result_array();
        $total = 0;$dams_student=0;$non_dams_student=0;
        $result =  array();
        foreach($sql_result as $key=>$sql) {
            $total = $total + 1;
                if($sql['dams_tokken'] != '') {
                    $dams_student  = $dams_student + 1;
                } else {
                    $non_dams_student  = $non_dams_student + 1;
                }
        } 
        $result['total_student']  = $total;
        $result['dams_student']  = $dams_student;
        $result['non_dams_student']  = $non_dams_student;
        return $result;
    }

    public function getJoinedStudent() {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT month(FROM_UNIXTIME( creation_time /1000, '%Y-%m-%d %H:%i:%s' )) as month , count(id) as total FROM users where year(FROM_UNIXTIME( creation_time /1000, '%Y-%m-%d %H:%i:%s' )) = '2017' group by month(FROM_UNIXTIME( creation_time /1000, '%Y-%m-%d %H:%i:%s' ))")->result_array();
        $result=array();$result['jan']=0;$result['feb']=0;$result['mar']=0;$result['apr']=0;$result['may']=0;$result['jun']=0;$result['jul']=0;$result['aug']=0;$result['sep']=0;$result['oct']=0;$result['nov']=0;$result['dec']=0;
        foreach ($query as $key => $q) {
            if($q['month'] == 1) { $result['jan'] = $q['total']; }
            elseif($q['month'] == 2) { $result['feb'] = $q['total']; }
            elseif($q['month'] == 3) { $result['mar'] = $q['total']; }
            elseif($q['month'] == 4) { $result['apr'] = $q['total']; }
            elseif($q['month'] == 5) { $result['may'] = $q['total']; }
            elseif($q['month'] == 6) { $result['jun'] = $q['total']; }
            elseif($q['month'] == 7) { $result['jul'] = $q['total']; }
            elseif($q['month'] == 8) { $result['aug'] = $q['total']; }
            elseif($q['month'] == 9) { $result['sep'] = $q['total']; }
            elseif($q['month'] == 10) { $result['oct'] = $q['total']; }
            elseif($q['month'] == 11) { $result['nov'] = $q['total']; }
            elseif($q['month'] == 12) { $result['dec'] = $q['total']; }

            }
            
            return $result;
        }
        

        public function get_permission_list() {
          return $this->db->query("SELECT * FROM `backend_user_permission` GROUP BY permission_merge")->result_array();
        }


        public function get_permission_detail_by_id($id) {
          return $this->db->where('id',$id)
          ->get('permission_group')->row_array();
        }
	
		public function getStudentCount(){
			$result = array();
			$dams_total =	$this->db->query("SELECT count(id) as dams FROM `users` WHERE `dams_tokken` != ''")->row_array();			
			$non_dams_total =	$this->db->query("SELECT count(id) as non_dams FROM `users` WHERE `dams_tokken` = ''")->row_array();
			$total = $dams_total['dams'] + $non_dams_total['non_dams'];
			$result['dams_students'] = $dams_total['dams'];
			$result['non_dams_students'] = $non_dams_total['non_dams'];
			$result['total_students'] = $total;
			
			return $result;
		
		}
	
		public function feed_count_details(){
			$result = array();
			$total_feeds_dams =	$this->db->query("SELECT count(pc.id) total_feeds_dams from post_counter pc join users u on u.id=pc.user_id where u.dams_tokken != ''")->row_array();
			$total_feeds_no_dams =	$this->db->query("SELECT count(pc.id) total_feeds_non_dams from post_counter pc join users u on u.id=pc.user_id where u.dams_tokken = ''")->row_array();
			$total_report_abuse =	$this->db->query("SELECT count(id) total_report_abuse FROM `user_post_report`")->row_array();
			$total_likes   = $this->db->query("SELECT count(id) total_likes FROM `user_post_like` ")->row_array();
			$total_comments = $this->db->query("SELECT count(id) total_comments FROM `user_post_comment` ")->row_array();
			$result['total_feeds_dams'] = $total_feeds_dams['total_feeds_dams'];
			$result['total_feeds_non_dams'] = $total_feeds_no_dams['total_feeds_non_dams'];
			$result['total_report_abuse'] = $total_report_abuse['total_report_abuse'];
			$result['total_likes'] = $total_likes['total_likes'];
			$result['total_comments'] = $total_comments['total_comments'];
			
			return $result;
		}
	
		public function video_count_details(){  
			$result = array();
			$total_videos =	$this->db->query("SELECT count(id) total_videos FROM `video_master`")->row_array();
		    $videos_view_count =	$this->db->query("SELECT count(id) videos_view_count FROM `video_view_meta` ")->row_array();
			$dams_videos =	$this->db->query("SELECT count(id) dams_videos FROM `video_master` WHERE for_dams = 1")->row_array();
			$non_dams_videos =	$this->db->query("SELECT count(id) non_dams_videos FROM `video_master` WHERE for_non_dams = 1")->row_array();
			
			$result['total_videos'] = $total_videos['total_videos'];
			$result['videos_view_count'] = $videos_view_count['videos_view_count'];
			$result['dams_videos'] = $dams_videos['dams_videos'];
			$result['non_dams_videos'] = $non_dams_videos['non_dams_videos'];
			
			return $result;
		}
	
		public function course_count_details(){  
			$result = array();
			$total_published =	$this->db->query("SELECT count(id) total_published FROM `course_master` WHERE `publish` = 1")->row_array();
			$total_purchased =	$this->db->query("SELECT count(id) total_purchased FROM `course_transaction_record` WHERE `transaction_status` = 1")->row_array();		   
			$total_paid_course =	$this->db->query("SELECT count(id) total_paid_course FROM `course_master` WHERE `mrp` != '' and publish = 1")->row_array();
			$total_free_course =	$this->db->query("SELECT count(id) total_free_course FROM `course_master` WHERE `mrp` = '' and publish = 1")->row_array();
			
			$result['total_published'] = $total_published['total_published'];
			$result['total_purchased'] = $total_purchased['total_purchased'];
			$result['total_paid_course'] = $total_paid_course['total_paid_course'];
			$result['total_free_course'] = $total_free_course['total_free_course'];
			
			return $result;
		}
	
		public function faculty_count_details(){
			$result = array();
			$total_faculty =	$this->db->query("SELECT count(id) total_faculty FROM `users` WHERE `is_expert` = 1")->row_array();
			$total_dams_faculty =	$this->db->query("SELECT count(id) total_dams_faculty FROM `users` WHERE `is_expert` = 1 and `dams_tokken` != ''")->row_array();	   
			$total_non_dams_faculty =	$this->db->query("SELECT count(id) total_non_dams_faculty FROM `users` WHERE `is_expert` = 1 and `dams_tokken` = ''")->row_array();
			
			$result['total_faculty'] = $total_faculty['total_faculty'];
			$result['total_dams_faculty'] = $total_dams_faculty['total_dams_faculty'];
			$result['total_non_dams_faculty'] = $total_non_dams_faculty['total_non_dams_faculty'];
			
			
			return $result;
		}
	
		public function instructor_count_details(){
			$result = array();
			$total_instructor =	$this->db->query("SELECT count(id) total_instructor FROM `users` WHERE `is_instructor` = 1")->row_array();
			$total_dams_instructor =	$this->db->query("SELECT count(id) total_dams_instructor FROM `users` WHERE `is_instructor` = 1 and `dams_tokken` != ''")->row_array();	   
			$total_non_dams_instructor =	$this->db->query("SELECT count(id) total_non_dams_instructor FROM `users` WHERE `is_instructor` = 1 and `dams_tokken` = ''")->row_array();
			
			$result['total_instructor'] = $total_instructor['total_instructor'];
			$result['total_dams_instructor'] = $total_dams_instructor['total_dams_instructor'];
			$result['total_non_dams_instructor'] = $total_non_dams_instructor['total_non_dams_instructor'];
			
			
			return $result;
		}
	
		public function adv_count_details(){
			
			$adv_details =	$this->db->query("SELECT 	banner_title,DATE_FORMAT(FROM_UNIXTIME(from_date/1000), '%d-%m-%Y ') as from_date,hit_count FROM `fan_wall_banner` order by id desc  limit 0,5")->result_array();
			
			
			return $adv_details;
		}
	
		public function top_5_user_details(){
			
			$top_5_users_details =	$this->db->query("SELECT pc.user_id , u.name , u.followers_count ,count(pc.id) as total ,(SELECT count(upl.id) FROM user_post_like as upl join post_counter as pcp on upl.post_id = pcp.id WHERE pcp.user_id = pc.user_id ) as likes FROM post_counter as pc join users as u on u.id = pc.user_id group by pc.user_id order by total desc limit 0, 5")->result_array();
			
			
			return $top_5_users_details;
		}
	
		public function top_5_purchased_courses_details(){
			
			$top_5_purchased_courses_details =	$this->db->query("SELECT ctr.course_id, ctr.course_price, cm.course_rating_count as course_rating,cm.title as course_name
																  FROM `course_transaction_record` ctr
																  JOIN course_master cm ON ctr.course_id = cm.id
																  GROUP BY ctr.course_id
																  ORDER BY course_rating DESC
																  LIMIT 0 , 3")->result_array();
			
			
			return $top_5_purchased_courses_details;
		}
	
    
}
