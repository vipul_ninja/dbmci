<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Add_post_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    public function get_main_category() {
    	return $this->db->get("master_category")->result_array();
  	}


  	public function get_post_by_id($id) {
  		$this->db->where('id',$id);
  		return $this->db->get('post_tags')->row_array();
  	}



}