<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Currency_master_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    
    public function get_currency_list() {
      	return  $this->db->get("currency_value_master")->result_array();
    }

    public function get_currency($id){ 
		$id = $id['id'];
		$this->db->where('id',$id);
		$result = $this->db->get('currency_value_master')->row_array();
		return $result;
	}
	 public function insert_currency($data){ 
		$result = $this->db->insert('currency_value_master',$data);
		return $result;
	}
	public function update_currency($data){ 
		$id = $data['id'];
		$this->db->where('id',$id);
		$result = $this->db->update('currency_value_master',$data);
		return $result;
	}
	public function check_unique_mcc($data){ 
		$id = $data['id'];
		$this->db->select('mcc_code');
		$this->db->where('id',$id);
		$result = $this->db->get('currency_value_master')->row_array();
		return $result['mcc_code'];
	}
	public function check_unique_name($data){ 
		$id = $data['id'];
		$this->db->select('name');
		$this->db->where('id',$id);
		$result = $this->db->get('currency_value_master')->row_array();
		return $result['name'];
	}
}