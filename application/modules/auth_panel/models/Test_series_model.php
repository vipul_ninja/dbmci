<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Test_series_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    public function get_test_series_by_id($id) {
    	$this->db->where('id',$id);
    	return $this->db->get('course_test_series_master')->row_array();

    }

	public function get_subject_list() {
    	
    	return $this->db->get('course_subject_master')->result_array();

    }
    public function get_questions_exist_in_test_series($id) {
    	$sql="select count(id) as total from course_testseries_question_relation where test_series_id=$id";
    	return $result=$this->db->query($sql)->row()->total;
    }
	
	public function test_series_result_status_count(){   
		
		$today = date('d-m-Y');
		$currentWeekDate = date('d-m-Y', strtotime('-7 days', strtotime($today)));
		$currentMonth = date('m');
		$currentYear = date('Y');
		$current_millisecond = strtotime(date('01-m-Y 00:00:00'))*1000;
		
		
		$result = array();
		$this->db->select("count(id) total,sum(case when result = 1  then  1 when result IS NULL then 0  else 0 end) passed,sum(case when result = 0 then  1 when result IS NULL then 0 else 0 end) failed");
		$this->db->where("DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%d-%m-%Y')='$today'");
		$daily = $this->db->get('course_test_series_report')->row_array();		
		$result['daily'] = $daily;
		
		$this->db->select("count(id) total,sum(case when result = 1 then 1 when result IS NULL then 0 else 0 end) passed,sum(case when result = 0 then 1 when result IS NULL then 0 else 0 end) failed");
		$this->db->where("DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%d-%m-%Y') BETWEEN '$currentWeekDate' AND '$today'");
		$weekly = $this->db->get('course_test_series_report')->row_array();
		$result['weekly'] = $weekly;
		
		$this->db->select("count(id) total,sum(case when result = 1 then 1 when result IS NULL then 0 else 0 end) passed,sum(case when result = 0 then 1 when result IS NULL then 0 else 0 end) failed");
		$this->db->where(" creation_time >'$current_millisecond'");
		$monthly = $this->db->get('course_test_series_report')->row_array();		
		$result['monthly'] = $monthly;
		
		return $result;
	}
	
	public function delete_question_from_testseries($id,$test_series_id){
      $data = array('state' =>1);
      $this->db->where('test_series_id',$test_series_id);
	  $this->db->where('question_id',$id);
	  $result = $this->db->delete("course_testseries_question_relation");
	  if($result){
		return   1;
	}else{
	return  2;
		
	}
    }
	
	public function add_question_to_testseries($test_series_id,$question_id,$section_id) {
		$this->db->select('no_of_questions');	
		$this->db->where('test_series_id',$test_series_id);
		$this->db->where('section_id',$section_id);	
	    $question_no= $this->db->get('test_series_sections')->row()->no_of_questions;
		$this->db->where('test_series_id',$test_series_id);
		 $this->db->where('section_id',$section_id);	
		$count_question_of_sec= $this->db->get('course_testseries_question_relation')->num_rows();
    	 $this->db->where('test_series_id',$test_series_id);
		$this->db->where('question_id',$question_id);
		$this->db->where('section_id',$section_id);

	 	$check = $this->db->get('course_testseries_question_relation')->result_array();
		if($check ){
			echo '3';
		}else if($count_question_of_sec < $question_no){
			$data = array('test_series_id'=>$test_series_id,
						   'question_id'=>$question_id,
						   'section_id'=>$section_id,
						);
						   
			$result = $this->db->insert("course_testseries_question_relation",$data);
			if($result){
				echo  '1';
			}else{
			echo  '2';
				
			}
			
		}

	}	

function count_section_question($test_series_id,$section_id){
	
	$this->db->select('no_of_questions');	
	$this->db->where('test_series_id',$test_series_id);
	$this->db->where('section_id',$section_id);	
	$question_no= $this->db->get('test_series_sections')->row()->no_of_questions;
	$this->db->where('test_series_id',$test_series_id);
	 $this->db->where('section_id',$section_id);	
	$count_question_of_sec= $this->db->get('course_testseries_question_relation')->num_rows();
	if($count_question_of_sec == $question_no){
return 1  ;
	}else{

		return 2;
	}

}
	

	public function edit_section_testseries($data,$id) {
    	$this->db->where('test_series_id',$data['test_series_id']);
		$this->db->where('section_id',$data['section_id']);
		$this->db->where('id!=',$id);
    	$check = $this->db->get('test_series_sections')->result_array();
		if($check){
			return false;
		}else{
			$this->db->where('test_series_id',$data['test_series_id']);
			$this->db->where('id',$id);	
			$result = $this->db->update("test_series_sections",$data);

			if($result){
				return true;
			}else{
			return false;
				
			}
			
		}

	}	
	public function add_section_to_testseries($data) {
		
		$this->db->where('test_series_id',$data['test_series_id']);
		$this->db->where('section_id',$data['section_id']);
		
		//$this->db->where('question_id',$question_id);
    	$check = $this->db->get('test_series_sections')->row_array();
		if($check){
			return 1;
		}else{
		
			$result = $this->db->insert("test_series_sections",$data);
			if($result>0){
			
				$this->db->select('sum(`section_timing`)as total_time,sum(`no_of_questions`) as total_questions, sum((no_of_questions*marks_per_question) ) as total_marks ');
				$this->db->where('test_series_id',$data['test_series_id']);
				$check = $this->db->get('test_series_sections')->row_array();
			$array=array('time_in_mins'=>$check['total_time'],'total_questions'=>$check['total_questions'],'total_marks'=>$check['total_marks']) ;
			$this->db->where('id',$data['test_series_id']);
			
		  $this->db->update("course_test_series_master",$array);
		  
			}	
			if($result){
				return 2;
			}
			
		}

    }	
	public function get_stream_list() {    
		$this->db->where('parent_id',0);
		$this->db->select('name');	
		$this->db->select('id');	
		$this->db->select('parent_id');	
    	return $this->db->get('course_stream_name_master')->result_array();
	}
	
	public function get_sub_stream_list() {
		$this->db->where('parent_id !=',0);
		$this->db->select('name');	
		$this->db->select('id');	
		$this->db->select('parent_id');	
		return $this->db->get('course_stream_name_master')->result_array();		
		 
		}
	public function get_langs() {
		return $this->db->get('language_code')->result_array();
		   }
		   
		   public function connect_section_to_testseries($section_id,$id) {
			   //print_r($id);
			$this->db->where('section_id',$section_id);
			$this->db->where('id',$id);
			$check = $this->db->get('course_testseries_question_relation')->result_array();
			if($check){
				return 'error';
			}else{
				$data = array('section_id'=>$section_id);
                                   $this->db->where('id',$id);
						$result = $this->db->update("course_testseries_question_relation",$data);

				if($result){
					return 'success';
				}else{
			return 'error';
					
				}
				
			}
	
		}	
	   

}