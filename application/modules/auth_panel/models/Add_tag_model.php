<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Add_tag_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    public function get_main_category() {
      $this->db->where('parent_id',0);
    	return $this->db->get("course_stream_name_master")->result_array();
  	}


  	public function get_tag_by_id($id) {
  		$this->db->where('id',$id);
  		return $this->db->get('post_tags')->row_array();
  	}

    public function get_tags_by_group() {
      $array = $this->db->query('SELECT * FROM `post_tags` GROUP BY id')->result_array();
      foreach ($array as $key => $value) {
          if($value['master_id'] == 1) {
            $group['Medical'][] = $value;
          } else if($value['master_id'] == 2) {
            $group['Dental'][] = $value;
          } else if($value['master_id'] == 3) {
            $group['Other'][] = $value;
          }
      }
      return $group;
    }

    public function get_sub_category_by_master_cat_id($id) {
        return $this->db->where('master_id',$id)
        ->get('post_tags')->result_array();
    }


    public function get_tag_group_detail_by_id($id) {
      $sql =  $this->db->where('id',$id)
      ->get('post_tag_group')->row_array();
      $master_id = $this->db->query('SELECT master_id FROM `post_tags` WHERE `id` IN ("'.$sql['tag_fk_id'].'")')->row_array();
      $sql['master_id'] = $master_id['master_id'];
      return $sql;
    }



}