<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Instructor_pay_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    public function add_record($info) {
      $info['creation_time'] = milliseconds(); 
      $this->db->insert('instructor_pay_record',$info);
    }
}
