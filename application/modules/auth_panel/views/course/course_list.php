<div class="col-sm-12">
<header class="panel-heading">
    Main Category(s) LIST
    <span class="tools pull-right">
    <a href="javascript:;" class="fa fa-chevron-down"></a>

    </span>
    </header>
    <form method="get">
<select   class="form-control " onchange="javascript:submit('form')" name="cat">
    <option value="" >Select category</option>
    <?php foreach($categories as $key=>$cat) {
          if($cat['text'] != 'Other') {
     ?>
    <option value="<?php echo $cat['id']; ?>"><?php echo $cat['text']; ?></option>
    <?php } }?>
</select>
</form>
</div>
<br/>
<?php if($this->input->get('cat')) { ?>
<!-- subcategory list -->
<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		Main Course(s) LIST
		<span class="tools pull-right">
		<a href="javascript:;" class="fa fa-chevron-down"></a>

		</span>
		</header>
		<div class="panel-body">
		<div class="adv-table">

		<div class="col-lg-12">
        <div class="col-lg-10">
		      <a href="<?php echo AUTH_PANEL_URL.'course/add_course'; ?>"><button class="pull-right btn btn-info btn-sm">Add Course </button></a>
        </div>
        <div class="col-lg-2">
           <a href="<?php echo AUTH_PANEL_URL.'course/add_sub_course'; ?>"><button class="pull-right btn btn-info btn-sm">Add Sub Course </button></a>
        </div>
		</div>


		<table  class="display table table-bordered table-striped" id="all-subcategory-grid">
  		<thead>
    		<tr>
				<th>#</th>
				<th>Main Title</th>
        <th>Course</th>
				<th>Action</th>
    		</tr>
  		</thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
              <th></th>
          </tr>
      </thead>
		</table>

		</div>
		</div>
	</section>
</div>
<?php } ?>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD

               <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {

                       var table = 'all-subcategory-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course/ajax_course_list_level_one/"+$category_id, // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
						 $('.search-input-select').on( 'change', function () {   // for select box
							    var i =$(this).attr('data-column');
							    var v =$(this).val();
							    dataTable.columns(i).search(v).draw();
							} );
                   } );

                   $("select[name=cat]").val('$category_id');
               </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
