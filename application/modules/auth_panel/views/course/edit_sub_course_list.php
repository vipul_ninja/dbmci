<div class="col-lg-12">
    <div class="col-lg-6">
      <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
    </div>
    <div class="col-lg-6"></div>
</div><br><br>

<div class="col-lg-6">
  <section class="panel">
      <header class="panel-heading">
          Edit Sub course
      </header>
      <div class="panel-body">
          <form role="form" action="" method="post">
              <div class="form-group">
                  <label for="exampleInputEmail1">Sub course Name</label>
                  <input type="text" placeholder="Enter Sub category Name" name="text" value="<?php echo $sub_course['text']; ?>" class="form-control">
                  <span style="color:red"><?php echo form_error("text"); ?></span>
              </div>
              <button class="btn btn-info" type="submit">Submit</button>
          </form>
      </div>
  </section>
</div>