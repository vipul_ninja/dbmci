<div class="col-lg-12">
    <div class="col-lg-6">
      <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
    </div>
    <div class="col-lg-6"></div>
</div><br><br>

<div class="col-lg-6">
  <section class="panel">
    <header class="panel-heading">
        Add Sub Course
        <span class="tools pull-right">
        <a href="javascript:;" class="fa fa-chevron-down"></a>
        
        </span>
    </header>
    <div class="panel-body">
    <form method="post">
        <div class="form-group">
        <select   class="form-control select_cat"  name="main_cat">
                <option value="" >Select category</option>
                <?php foreach($categories as $key=>$cat) {
                        if($cat['text'] != 'Other') {
                 ?>
                <option value="<?php echo $cat['id']; ?>"><?php echo $cat['text']; ?></option>
            <?php } } ?>
        </select>
        <span style="color:red"><?php echo form_error('main_cat'); ?></span>
         </div>
         <div class="form-group">
        <select   class="form-control main_course"  name="main_course">
                <option value="" >Select course</option>
        </select>
        <span style="color:red"><?php echo form_error('main_course'); ?></span>
         </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Sub Course Name</label>
          <input type="text" class="form-control" value="" name="text" placeholder="Enter course Name">
          <span style="color:red"><?php echo form_error('text'); ?></span>
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
    </form>
    </div>
  </section>
</div>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
  $('.select_cat').change(function() {
  var id = $(this).val();

  $.ajax({           
       url:"<?php echo AUTH_PANEL_URL.'course/get_sub_course';?>",
       data: {id:id},
       dataType: 'json',
       method: 'POST',
       success : function(data) {
          $('.main_course').html(data.html);
       }
  });
});
 

</script>