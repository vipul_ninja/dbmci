
<div class="col-lg-6">
  <section class="panel">
      <header class="panel-heading">
          Add category
      </header>
      <div class="panel-body">
          <form role="form" method="post">
              <div class="form-group">
                <label for="exampleInputEmail1">Category</label>
                   <input type="text" placeholder="Enter Category Name" value="<?php echo set_value('text'); ?>" name="text" class="form-control">
                  <span style="color:red"><?php echo form_error("text"); ?></span>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Position</label>
                   <input type="text" placeholder="Enter Position" value="<?php echo set_value('position'); ?>" name="position" class="form-control">
                  <span style="color:red"><?php echo form_error("position"); ?></span>
              </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">Category Icon</label>
                  <input type="file" value="" name="image" >
                  <span style="color:red"><?php echo form_error("image"); ?></span>
              </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">Category Images</label>
                  <input type="file" value="" name="images" multiple>
                  <span style="color:red"><?php echo form_error("images"); ?></span>
              </div>
              <button class="btn btn-info" type="submit">ADD CATEGORY</button>
          </form>
      </div>
  </section>
</div>