<div class="col-lg-12">
    <div class="col-lg-6">
      <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
    </div>
    <div class="col-lg-6"></div>
</div><br><br>


<div class="col-lg-6">
  <section class="panel">
      <header class="panel-heading">
          Add Sub category
      </header>
      <div class="panel-body">
          <form role="form" method="post">
              
              <div class="form-group">
                <label for="exampleInputEmail1">Sub Category</label>
                  <input type="text" value="<?php echo $sub_category['text']; ?>" class="form-control" disabled>
                  <span style="color:red"><?php echo form_error("main_cat"); ?></span>
              </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">Subcategory Name</label>
                  <input type="text" placeholder="Enter Subcategory Name"  value="<?php echo set_value('text'); ?>" name="text" class="form-control">
                  <span style="color:red"><?php echo form_error("text"); ?></span>
              </div>
              <button class="btn btn-info" type="submit">Submit</button>
          </form>

      </div>
  </section>
</div>