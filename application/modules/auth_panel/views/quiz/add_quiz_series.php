<?php  $error = validation_errors();
$display = "display:none;";
if(!empty($error)){ $display = "";}

?>
<div class="col-lg-12 add_series_element" style="<?php echo $display; ?>">
	<section class="panel">
		<header class="panel-heading">
			Add Quiz
		</header>
		<div class="panel-body">
			<form role="form" method="post" action="<?php echo AUTH_PANEL_URL.'quiz/Quiz/add_quiz_series' ?>"  enctype="multipart/form-data">				
			<div class="form-group col-md-12" style="display:none">
				<select class="form-control" name="set_type">            				
				<option value="1" selected>Quiz</option>				
				</select>
			</div>
				<div class="form-group col-md-4">
					<label >Quiz Name *</label>
					<input type="text" placeholder="Enter name" value="<?php echo  set_value('quiz_series_name'); ?>" name = "test_series_name" id="test_series_name" class="form-control input-sm"><span class="error bold"><?php echo form_error('test_series_name');?></span>

				</div>
				<div class="form-group col-md-4 ">
					<label >Total questions</label>
					<input type="text" placeholder="Enter total no. question" value="<?php echo  set_value('total_questions'); ?>" name = "total_questions" id="total_questions" class="form-control input-sm">
					<span class="error bold"><?php echo form_error('total_questions');?></span>
				</div>
				<div class="form-group col-md-4 ">
					<label>Session (year)</label>
					<input type="text" placeholder="Session (year)" name = "session" value="<?php echo  set_value('session'); ?>" id="session" class="form-control input-sm">
					<span class="error bold"><?php echo form_error('session');?></span>
				</div>
				<div class="form-group col-md-4 ">
					<label for="exampleInputEmail1">Subject</label>
					<select class="form-control input-sm subject_element_select" name="subject">
					</select>
					<span class="error bold"><?php echo form_error('subject');?></span>
				</div>
				<div class="form-group col-md-4 ">
					<label for="exampleInputEmail1">Difficulty level</label>
					<select class="form-control input-sm" name="difficulty_level" >
						<option  value="1" <?php echo set_select('difficulty_level', '1', TRUE); ?>>Easy</option>
						<option value="2" <?php echo set_select('difficulty_level', '2' ); ?>>Medium</option>
						<option value="3" <?php echo set_select('difficulty_level', '3' ); ?>>Hard</option>
					</select>
					<span class="error bold"><?php echo form_error('difficulty_level');?></span>
				</div>

				<div class="form-group col-md-4">
					<label for="exampleInputEmail1">Quiz Type</label>
					<select class="form-control input-sm" name="test_type" class="form-control input-sm">
						<!--<option value="1" <?php echo set_select('test_type', '1', TRUE); ?>>PGI</option>-->
						<option value="2" <?php echo set_select('test_type', '2'); ?>>Normal</option>
					</select>
					<span class="error bold"><?php echo form_error('test_type');?></span>
				</div>
				<?php
					 		$all_option = $this->db->where('status',0)->get('course_stream_name_master')->result(); 
					 		$main_option = $sub_option = "";
					 		foreach($all_option as $ao){
					 			if($ao->parent_id == 0 ){
									
					 				$main_option .= "<option value='".$ao->id."' >".$ao->name."</option>";
					 			}else{
									
					 				$sub_option .= "<option style='display: none;' class='substream sub".$ao->parent_id."' value='".$ao->id."'>".$ao->name."</option>";
					 			}
							 }
							 $test_type_data = $this->db->get('course_test_type_attribute')->result_array(); 
							 
							
					 	?>
				 <input type="hidden"  name = "set_type"  value="0" class="form-control input-sm">
				 <div class="form-group col-md-4">
				 <label >SELECT STREAM</label>
				<select class="form-control  stream_element_select" name="stream">
							<option value=''>--select Stream--</option>
							<?php echo $main_option;?>
							</select>
							<span class="error bold"><?php echo form_error('stream');?></span>

            </div> 	
			<div class="form-group col-md-4">
			<label >SELECT SUB STREAM</label>
				<select class="form-control  sub_element_select" name="sub_stream">
							<option value=''>--select Sub Stream--</option>
							<?php echo $sub_option;?>
							</select>
							<span class="error bold"><?php echo form_error('sub_stream');?></span>

            </div>
				<div class="form-group col-md-12">
					<label for="exampleInputEmail1">Description</label>
					<textarea rows="4" cols="50" class="form-control input-sm" name="description" value="<?php echo  set_value('description'); ?>" ></textarea>
					<span class="error bold"><?php echo form_error('description');?></span>
				</div>
				<div class="form-group col-md-4 hide">
					<label for="exampleInputEmail1">Test_price</label>
					<select class="form-control input-sm" name="test_price" class="form-control">
						<option value="1" <?php echo set_select('test_price', '1', TRUE); ?>>Free</option>
						<option value="2" <?php echo set_select('test_price', '2'); ?>>Paid</option>
					</select>
					<span class="error bold"><?php echo form_error('test_price');?></span>
				</div>
				<div class="form-group col-md-4">
					<label>Time in minutes</label>
					<input type="text" placeholder="Time in minutes" name = "time_in_mins" value="<?php echo  set_value('time_in_mins'); ?>" id="time_in_mins" class="form-control input-sm">
					<span class="error bold"><?php echo form_error('time_in_mins');?></span>
				</div>
				<div class="form-group col-md-4">
					<label>Negetive marking per question</label>
					<input type="text" placeholder="Negative marking" name = "negative_marking" value="<?php echo  set_value('negative_marking'); ?>" id="negative_marking" class="form-control input-sm">
					<span class="error bold"><?php echo form_error('negative_marking');?></span>
				</div>
				<div class="form-group col-md-4">
					<label>Marks per question</label>
					<input type="text" placeholder="Marks per question" name = "marks_per_question" value="<?php echo  set_value('marks_per_question'); ?>" id="marks_per_question" class="form-control input-sm">
					<span class="error bold"><?php echo form_error('marks_per_question');?></span>
				</div>
				<div class="form-group col-md-4">
					<label>Total marks</label>
					<input type="text" placeholder="Total marks" name = "total_marks" value="<?php echo  set_value('total_marks'); ?>" id="total_marks" class="form-control input-sm">
					<span class="error bold"><?php echo form_error('total_marks');?></span>
				</div>
				<div class="form-group col-md-4">
					<label>Pass percentage</label>
					<input type="text" placeholder="Pass percentage"  name = "pass_percentage" value="<?php echo  set_value('pass_percentage'); ?>" id="pass_percentage"  class="form-control input-sm">
					<span class="error bold"><?php echo form_error('pass_percentage');?></span>
				</div>

				<div class="form-group col-md-12">
					<button class="btn btn-info btn-sm"  type="submit" >Upload</button>
					<button class="btn btn-danger btn-sm" onclick="$('.add_series_element').hide('slow');" type="button" >Cancel</button>
				</div>
			</form>

		</div>
	</section>
</div>

<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
			Quiz List
			<button onclick="$('.add_series_element').show('slow');" class="btn-success btn-xs btn pull-right"><i class="fa fa-plus"></i> Add</button>
		</header>
		<div class="panel-body">
			<div class="adv-table">
				<table  class="display table table-bordered table-striped" id="all-user-grid">
					<thead>
						<tr>

							<th>#</th>
							<th>Name</th>
							<th>Subject</th>
							<th>Stream</th>
							<th>Substream</th>
							
							<th>Total ques </th>
							<th>Type</th>
							<th>Quiz Id</th>
							<th>Action </th>
						</tr>
					</thead>
					<thead>
						<tr>
							<th><input type="text" data-column="0"  class="search-input-text form-control"></th>
							<th><input type="text" data-column="1"  class="search-input-text form-control"></th>
							<th><input type="text" data-column="2"  class="search-input-text form-control"></th>
							<th><input type="text" data-column="3"  class="search-input-text form-control"></th>
							<th><input type="text" data-column="4"  class="search-input-text form-control"></th>
							<th><input type="text" data-column="5"  class="search-input-text form-control"></th>
							<th><input type="text" data-column="6"  class="search-input-text form-control"></th>
							<th><input type="text" data-column="7"  class="search-input-text form-control"></th>
							<th><input type="text" data-column="8"  class="search-input-text form-control"></th>
						
							<th></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</section>
</div>

<?php

$subject = set_value('subject');

$adminurl = AUTH_PANEL_URL;

$custum_js = <<<EOD

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" >

$( ".stream_element_select" ).change(function() {
	val = $(this).val();
	$('.sub_element_select').val('');
	$('.substream').hide();
	$('.sub'+val).show();
  });



jQuery.ajax({
	url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
	method: 'Get',
	dataType: 'json',
	success: function (data) {

		var html = "<option value=''>--select--</option>";
		$.each( data , function( key , value ) {
			html += "<option value='"+value.id+"' >"+value.name+"</option>";
		});
		$(".subject_element_select").html(html).val('$subject');
	}
});

$( ".subject_element_select" ).change(function() {

	id = $(this).val();
	jQuery.ajax({
		url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
		method: 'Get',
		dataType: 'json',
		success: function (data) {

			var html = "<option value=''>--select--</option>";
			$.each( data , function( key , value ) {
				html += "<option value='"+value.id+"'>"+value.topic+"</option>";
			});
			$(".topic_element_select").html(html);
		}
	});
});

jQuery(document).ready(function() {
	var table = 'all-user-grid';
	var dataTable = jQuery("#"+table).DataTable( {
		"processing": true,
		"pageLength": 100,
		// "lengthMenu": [[15, 25, 50], [15, 25, 50]],
		"serverSide": true,
		"order": [[ 0, "desc" ]],
		"ajax":{
			url :"$adminurl"+"quiz/Quiz/ajax_quiz_series_list/", // json datasource
			type: "post",  // method  , by default get
			error: function(){  // error handling
				jQuery("."+table+"-error").html("");
				jQuery("#"+table+"_processing").css("display","none");
			}
		}
	} );
	jQuery("#"+table+"_filter").css("display","none");
	$('.search-input-text').on( 'keyup click', function () {   // for text boxes
		var i =$(this).attr('data-column');  // getting column index
		var v =$(this).val();  // getting search input value
		dataTable.columns(i).search(v).draw();
	} );
	$('.search-input-select').on( 'change', function () {   // for select box
		var i =$(this).attr('data-column');
		var v =$(this).val();
		dataTable.columns(i).search(v).draw();
	} );
} );

</script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
