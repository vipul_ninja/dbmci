<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="question_list" class="modal fade">
  <div class="modal-dialog modal-xl">
      <div class="modal-content">
          <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h4 class="modal-title">Questions List</h4>
          </div>
<?php
$all_language_meta  = $this->db->select('id,language')->get('language_code')->result_array();


?> 
          <div class="modal-body" id = "questions_list">
										 <div class="panel-body">
                       <button class="btn-success btn btn-sm add_questions_element pull-right ">Add All</button>
										<div class="adv-table">
											<table  class="display table table-bordered table-striped col-md-12" style="width:100%"  id="subject_questions_list">
  											<thead>
  												<tr>
    											  <th>#</th>
    											  <th>Question</th>
    											  <th>Subject</th>
                                                  <th>Stream</th>
							                      <th>Substream</th>
    											  <th>Topic</th>
    											  <th>Question type</th>
    											  <th>Added by </th>
                                                  <th>Q_ID </th>
    											  <th>Action </th>
                            <th><input name="select_all" value="1" id="example-select-all" type="checkbox"></th>
  												</tr>
  											</thead>
  										  <thead>
  											  <tr>
    												<th><input type="text" data-column="0"  class="search-input-text form-control"></th>
    												<th><input type="text" data-column="1"  class="search-input-text form-control"></th>
    												<th><input type="text" data-column="2"  class="search-input-text form-control"></th>
    												<th><input type="text" data-column="3"  class="search-input-text form-control"></th>
    												<th><input type="text" data-column="4"  class="search-input-text form-control"></th>
    												<th><input type="text" data-column="5"  class="search-input-text form-control"></th>
                                                    <th><input type="text" data-column="6"  class="search-input-text form-control"></th>
                                                    <th><input type="text" data-column="7"  class="search-input-text form-control"></th>
                                                    <th><input type="text" data-column="8"  class="search-input-text form-control"></th>
                                                    <th></th>
                            <th></th>
  											  </tr>
  										  </thead>
											</table>
										</div>
								  </div>
          </div>

      </div>
  </div>
</div>

<div class="col-md-3">
  <section class="panel">
  <header class="panel-heading">
      Quiz Menu <small data-original-title="Note" data-content="You can change it from 'Advance option' tab." data-placement="bottom" data-trigger="hover" title="" aria-hidden="true" class="bold popovers label label-info pull-right"> Rewards Points <?php echo $quiz_series_detail['reward_points']; ?></small>
  </header>
  <div class="panel-body">
      <ul class="nav prod-cat">
          <li><a href="javascript:void(0)" data-div="1" ><i class=" fa fa-angle-right"></i> Basic information</a></li>
          <li><a href="javascript:void(0)" data-div="4" ><i class=" fa fa-angle-right"></i> Advance option </a></li>
          <li><a href="javascript:void(0)" data-div="5" ><i class=" fa fa-angle-right"></i> Cover image </a></li>
          <li class="hide"><a href="javascript:void(0)" data-div="2" ><i class=" fa fa-angle-right"></i> Add questions(CSV) </a></li>
          <li><a href="javascript:void(0)" data-div="3" ><i class=" fa fa-angle-right"></i> Questions list </a></li>
          <li><a href="javascript:void(0)" data-div="6" ><i class=" fa fa-angle-right"></i> Report </a></li>
		<li><a href="javascript:void(0)" data-div="7" ><i class=" fa fa-angle-right"></i> Used In </li>
        <li><a href="#"> <i class=" fa fa-angle-right"></i> QUIZ ID = <?PHP echo  $quiz_series_detail['id'] ;?></a></li>
      </ul>
  </div>
      <div class="panel-body">

      <?php  if($quiz_series_detail['publish'] == 0 ){ ?>
          <a href="<?php  echo AUTH_PANEL_URL."quiz/Quiz/set_quiz_series_publish/".$quiz_series_detail['id'];?>"><button class="btn btn-xs btn-success pull-right bold">Publish</button></a>
      <?php  }else{ ?>
          <a href="<?php  echo AUTH_PANEL_URL."quiz/Quiz/set_quiz_series_unpublish/".$quiz_series_detail['id'];?>"><button class="btn btn-xs btn-danger pull-right bold">Unpublish</button></a>
      <?php  } ?>
  </div>
  </section>
  <?php
      /* get sale status */
  $sql = "select count(id) total,
              sum(case when result = 0 then 1 else 0 end) fail,
              sum(case when result = 1 then 1 else 0 end) pass
              from course_test_series_report
              Where test_series_id = ".$quiz_series_detail['id'];
  $sale_result = $this->db->query($sql)->row();
  $total =  $pass = $fail = 0;
  if($sale_result){
      $total = $sale_result->total;
      $pass = $sale_result->pass;
      $fail = $sale_result->fail;
  }
  ?>

  <section class="panel">
  <div class="weather-bg">
      <div class="panel-body">
          <div class="row">
              <div class="col-xs-6">
              <i class="fa fa-users"></i>Attempts</div>
              <div class="col-xs-6">
                  <div class="degree"><?php echo $total;?></div>
              </div>
          </div>
      </div>
  </div>

  <footer class="weather-category">
      <ul>
          <li class="active"><h5>Pass</h5><?php echo $pass;?></li>
          <li><h5>fail</h5><?php echo $fail;?></li>
      </ul>
  </footer>

  </section>

</div>

<div class="col-md-9 no-padding">

<div style="" class="alert bg-danger show_question_warning">
  <h4 class="bold">
    <i class="fa fa-ok-sign"></i>
    Note !
  </h4>
  <p>To publish this Quiz you need to add <?php echo $quiz_series_detail['total_questions']; ?>  question(s).</p>
</div>


<div id="tabContent1" class="col-lg-12 tabu">
    <section class="panel">
        <header class="panel-heading">
            Edit Quiz
        </header>
        <div class="panel-body">
            <form role="form" method="post" action="<?php echo AUTH_PANEL_URL.'quiz/Quiz/edit_quiz_series/'. $quiz_series_detail['id'] ?>"  enctype="multipart/form-data">             
			  <input type="hidden"  name = "id" id="id" value="<?php echo $quiz_series_detail['id']; ?>" class="form-control input-sm">       

         <?php //echo $quiz_series_detail['set_type']; ?>

			 <div class="form-group col-md-4">
				  <label >Quiz Name</label>
				  <input type="text" placeholder="Enter name" name = "test_series_name" id="test_series_name" value="<?php echo $quiz_series_detail['test_series_name']; ?>" class="form-control input-sm"><span class="error bold"><?php echo form_error('test_series_name');?></span>

			  </div>
			   <div class="form-group col-md-4 ">
				  <label >Total questions</label>
				  <input type="text" placeholder="Enter total no. question" name = "total_questions" id="total_questions" value="<?php echo $quiz_series_detail['total_questions']; ?>" class="form-control input-sm">
				   <span class="error bold"><?php echo form_error('total_questions');?></span>
			  </div>

			   <div class="form-group col-md-4">
				  <label>Session (year)</label>
				  <input type="text" placeholder="Session (year)" name = "session" id="session" value="<?php echo $quiz_series_detail['session']; ?>" class="form-control input-sm">
				   <span class="error bold"><?php echo form_error('session');?></span>
			  </div>

			  <div class="form-group col-md-4 ">
                  <label for="exampleInputEmail1">Subject</label>
                  <select class="form-control input-sm subject_element_select" name="subject" onChange="chnge_topic(this.value);" >
                  </select>
				  <span class="error bold"><?php echo form_error('subject');?></span>
              </div>
              <div class="form-group col-md-4 ">
                  <label for="exampleInputEmail1">Topic</label>
                  <select class="form-control input-sm topic_element_select" name="topic" >
                  </select>
				  <span class="error bold"><?php echo form_error('topic');?></span>
              </div>
			  <div class="form-group col-md-4 ">
                  <label for="exampleInputEmail1">Difficulty level</label>
                  <select class="form-control input-sm" name="difficulty_level" value="<?php echo $quiz_series_detail['difficulty_level']; ?>" >
					<option value="1" <?php if($quiz_series_detail['difficulty_level'] == 1){ echo "selected";} ?> >Easy</option>
					<option value="2" <?php if($quiz_series_detail['difficulty_level'] == 2){ echo "selected";} ?> >Medium</option>
					<option value="3" <?php if($quiz_series_detail['difficulty_level'] == 3){ echo "selected";} ?>>Hard</option>
					 <span class="error bold"><?php echo form_error('difficulty_level');?></span>
                  </select>
              </div>

			  <?php
					 		$all_option = $this->db->where('status',0)->get('course_stream_name_master')->result(); 
					 		$main_option = $sub_option = "";
					 		foreach($all_option as $ao){
					 			if($ao->parent_id == 0 ){
                                    if($ao->id==$quiz_series_detail['stream']){
                                        $stream_st="selected";
                                                            }else{
                                                             $stream_st="";
                                 
                                                                }
					 				$main_option .= "<option value='".$ao->id."' $stream_st  required>".$ao->name."</option>";
					 			}else{
                                    if($ao->id==$quiz_series_detail['sub_stream']){
                                        $stream_st="selected";
                                                            }else{
                                                             $stream_st="";
                                 
                                                                }
					 				$sub_option .= "<option style='display: none;' class='substream sub".$ao->parent_id."' value='".$ao->id."' $stream_st required>".$ao->name."</option>";
					 			}
							 }
							 
                             $test_type_data = $this->db->get('course_test_type_attribute')->result_array(); 
					 	?>
				 <input type="hidden"  name = "set_type"  value="0" class="form-control input-sm">
				 <div class="form-group col-md-4">
				 <label >SELECT STREAM</label>
				<select class="form-control  stream_element_select" name="stream">
							<option value=''>--select Stream--</option>
							<?php echo $main_option;?>
							</select>
							<span class="error bold"><?php echo form_error('stream');?></span>

            </div> 	
			<div class="form-group col-md-4">
			<label >SELECT SUB STREAM</label>
				<select class="form-control  sub_element_select" name="sub_stream">
							<option value=''>--select Sub Stream--</option>
							<?php echo $sub_option;?>
							</select>
							<span class="error bold"><?php echo form_error('sub_stream');?></span>

            </div>
			  <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Description</label>
                   <textarea rows="4" cols="50" class="form-control input-sm" name="description" value="" ><?php echo $quiz_series_detail['description']; ?></textarea>
                  <span class="error bold"><?php echo form_error('description');?></span>
              </div>
              <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Quiz Type</label>
                  <select class="form-control input-sm" name="test_type" value="<?php echo $quiz_series_detail['test_type']; ?>" class="form-control input-sm">
					<!--<option value="1" <?php if($quiz_series_detail['test_type'] == 1){ echo "selected";} ?>>PGI</option>-->
					<option value="2" <?php if($quiz_series_detail['test_type'] == 2){ echo "selected";} ?>>Normal</option>
					 <span class="error bold"><?php echo form_error('test_type');?></span>
                  </select>
              </div>
			    <div class="form-group col-md-4 hide">
                  <label for="exampleInputEmail1">Test_price</label>
                  <select class="form-control input-sm" name="test_price" value="<?php echo $quiz_series_detail['test_price']; ?>" class="form-control">
					<option value="1" <?php if($quiz_series_detail['test_price'] == 1){ echo "selected";} ?> >Free</option>
					<option value="2" <?php if($quiz_series_detail['test_price'] == 2){ echo "selected";} ?>>Paid</option>
					 <span class="error bold"><?php echo form_error('test_price');?></span>
                  </select>
              </div>
			   <div class="form-group col-md-4">
				  <label>Time in minutes</label>
				  <input type="text" placeholder="Time in minutes" name = "time_in_mins" id="time_in_mins" value="<?php echo $quiz_series_detail['time_in_mins']; ?>" class="form-control input-sm">
				   <span class="error bold"><?php echo form_error('session');?></span>
			  </div>
			   <div class="form-group col-md-4 ">
				  <label>Negetive marking per question</label>
				  <input type="text" placeholder="Negative marking" name = "negative_marking" id="negative_marking" value="<?php echo $quiz_series_detail['negative_marking']; ?>" class="form-control input-sm">
				   <span class="error bold"><?php echo form_error('negative_marking');?></span>
			  </div>
			   <div class="form-group col-md-4 ">
				  <label>Marks per question</label>
				  <input type="text" placeholder="Marks per question" name = "marks_per_question" id="marks_per_question" value="<?php echo $quiz_series_detail['marks_per_question']; ?>" class="form-control input-sm">
				   <span class="error bold"><?php echo form_error('marks_per_question');?></span>
			  </div>
			  <div class="form-group col-md-4 ">
				  <label>Total marks</label>
				  <input readonly type="text" placeholder="Total marks" name = "total_marks" id="total_marks" value="<?php echo $quiz_series_detail['total_marks']; ?>" class="form-control input-sm">
				   <span class="error bold"><?php echo form_error('total_marks');?></span>
			  </div>
			   <div class="form-group col-md-4">
				<label>Pass percentage</label>
				<input type="text" placeholder="Pass percentage"  name = "pass_percentage" id="pass_percentage" value="<?php echo $quiz_series_detail['pass_percentage']; ?>" class="form-control input-sm">
				 <span class="error bold"><?php echo form_error('pass_percentage');?></span>
			  </div>

        <div class="form-group col-md-12">
			    <input class="btn btn-info btn-sm"  type="submit" name="basic_details_submit" value="Upload" >
        </div>
		  </form>

        </div>
    </section>
</div>
<div id="tabContent2" class="col-lg-12 tabu hide">
    <section class="panel">
        <header class="panel-heading">
            Upload CSV <a href="<?php echo base_url().'auth_panel_assets/sample_csv_questions.csv'; ?>" class="pull-right btn-sm btn btn-info btn-xs">Download Sample CSV</a>
        </header>
	  <div class="panel-body">
                        <div class="panel-body">
                            <form role="form" action="<?php echo  AUTH_PANEL_URL .'quiz/Quiz/uploadCSV';  ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
                               <?php if(isset($_SESSION['question_csv_error'])){ echo $_SESSION['question_csv_error']; unset($_SESSION['question_csv_error']); } ?>
                                <div class="form-group">
                                    <label for="exampleInputFile">File input</label>
								  <input type="hidden"  id="id" name ="id" value="<?php echo $quiz_series_detail['id']; ?>">
                                    <input type="file" accept=".csv" id="exampleInputFile" name ="userfile">

                                </div>
                                <button class="btn btn-info btn-sm" type="submit">Submit</button>
                            </form>
                        </div>
		   <div class="alert alert-success">
                  <span class="bold"> GUIDLINES :-</span>
				<ul class="margin-bottom-none padding-left-lg">
          <li><span class="bold">question : </span>This field is for question content.</li>
          <li><span class="bold">description : </span>This field is for question description.</li>
          <li><span class="bold">question_type : </span>This field is for question type :- MC - for multiple choice , SC -  for single choice , TF - for True & False</li>
          <li><span class="bold">difficulty_level : </span>This field is for questions difficulty :- valueshold be 1,2,or 3 (1 - Easy , 2 -  medium , 3 - hard) </li>
          <li><span class="bold">duration : </span>This field is for test duration in minutes</li>
          <li><span class="bold">Option_1 : </span>This field is for question's option-1 </li>
          <li><span class="bold">Option_2 : </span>This field is for question's option-2</li>
          <li><span class="bold">Option_3 : </span>This field is for question's option-3</li>
          <li><span class="bold">Option_4 : </span>This field is for question's option-4</li>
          <li><span class="bold">Option_5 : </span>This field is for question's option-5</li>
          <li><span class="bold">answer : </span>This field contains the answer of particular question and it should contain answer/answers separated by comma ex - 1 or 1,2,5</li>

				</ul>
			 </div>
	  </div>

	</section>
</div>

<div id="tabContent3" class="col-lg-12 tabu">
    <section class="panel">
        <header class="panel-heading">
           Questions List
           <?php
//if($quiz_series_detail['publish'] == 0 ){ ?> 
  <button class="btn-success btn-xs btn pull-right"  data-toggle="modal" href="#question_list"><i class="fa fa-plus"></i> Add</button>
<?php // } ?>   
        </header>
        <div class="panel-body">
      		<div class="adv-table">
      		<table  class="display table table-bordered table-striped col-md-12" style="width:100%"  id="all-user-grid">
        		<thead>
          		<tr>
                <th>#</th>
                <th>Question</th>
                <th>Subject</th>
                <th>Topic</th>
                <th>Stream</th>
                <th>Substream</th>
                <th>Question type</th>
                 <th>Language</th> 
                <th>Q_ID</th>
                <th>Action </th>
          		</tr>
        		</thead>
            <thead>
                <tr>
                  <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="5"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="6"  class="search-input-text form-control"></th>
                  <th>  <select data-column="7"  class="form-control search-input-select">
                             <option value="">All</option>
                   <?php   foreach($all_language_meta as $language){  ?>
                                        <option value="<?php echo $language['id']; ?>"><?php echo $language['language']; ?></option>
                                        
                                        <?php } ?>
									</select></th>
                  <th><input type="text" data-column="8"  class="search-input-text form-control"></th>
                <th></th>
                </tr>
            </thead>
      		</table>
      		</div>
	  </div>
	 </section>
</div>

<div id="tabContent4" class="col-lg-12 tabu">
    <section class="panel">
        <header class="panel-heading">
           Advance option quiz
        </header>
        <div class="panel-body">
            <form role="form" method="post" action="<?php echo AUTH_PANEL_URL.'quiz/Quiz/edit_quiz_series/'. $quiz_series_detail['id'];?>"  enctype="multipart/form-data">
			      <input type="hidden"  name = "id" id="id" value="<?php echo $quiz_series_detail['id']; ?>" class="form-control input-sm">

            <div class="form-group col-md-3">
            <label for="exampleInputEmail1">Consider time</label>
            <select class="form-control input-sm" name="consider_time" >
              <option value="1" <?php if($quiz_series_detail['consider_time'] == 1){ echo "selected";} ?>>Test time</option>
              <option value="2" <?php if($quiz_series_detail['consider_time'] == 2){ echo "selected";} ?>>Question time</option>
            </select>
            <span class="error bold"><?php echo form_error('consider_time');?></span>
            </div>

        <div class="form-group col-md-3">
          <label for="exampleInputEmail1">Shuffle Question</label>
          <select class="form-control input-sm" name="shuffle"  >
            <option value="1" <?php if($quiz_series_detail['shuffle'] == 1){ echo "selected";} ?>>Yes</option>
            <option value="0" <?php if($quiz_series_detail['shuffle'] == 0){ echo "selected";} ?>>No</option>
          </select>
		  <span class="error bold"><?php echo form_error('shuffle');?></span>
        </div>

        <div class="form-group col-md-3">
                  <label for="exampleInputEmail1">Shuffle answers</label>
                  <select class="form-control input-sm" name="answer_shuffle" >
				<option value="1" <?php if($quiz_series_detail['answer_shuffle'] == 1){ echo "selected";} ?> >Yes</option>
				<option value="0" <?php if($quiz_series_detail['answer_shuffle'] == 0){ echo "selected";} ?>>No</option>
                  </select>
          <span class="error bold"><?php echo form_error('answer_shuffle');?></span>
        </div>
        <div class="form-group col-md-3">
                  <label for="exampleInputEmail1">Mandatory check</label>
                  <select class="form-control input-sm" name="mandatory_check" >
				<option value="1" <?php if($quiz_series_detail['mandatory_check'] == 1){ echo "selected";} ?> >Yes</option>
				<option value="0" <?php if($quiz_series_detail['mandatory_check'] == 0){ echo "selected";} ?> >No</option>
                  </select>
          <span class="error bold"><?php echo form_error('mandatory_check');?></span>
        </div>
        <div class="form-group col-md-3">
                  <label for="exampleInputEmail1">Allow user to move</label>
                  <select class="form-control input-sm" name="allow_user_move" >
				<option value="1" <?php if($quiz_series_detail['allow_user_move'] == 1){ echo "selected";} ?>  >Yes</option>
				<option value="0" <?php if($quiz_series_detail['allow_user_move'] == 0){ echo "selected";} ?> >No</option>
                  </select>
          <span class="error bold"><?php echo form_error('allow_user_move');?></span>
        </div>
        <div class="form-group col-md-3">
                  <label for="exampleInputEmail1">Time boundation</label>
                  <select class="form-control input-sm" name="time_boundation" >
				<option value="1"<?php if($quiz_series_detail['time_boundation'] == 1){ echo "selected";} ?>   >Yes</option>
				<option value="0" <?php if($quiz_series_detail['time_boundation'] == 0){ echo "selected";} ?>  >No</option>
                  </select>
           <span class="error bold"><?php echo form_error('time_boundation');?></span>
        </div>
        <div class="form-group col-md-3">
                  <label for="exampleInputEmail1">Show question time</label>
                  <select class="form-control input-sm" name="show_question_time" >
				<option value="1" <?php if($quiz_series_detail['show_question_time'] == 1){ echo "selected";} ?> >Yes</option>
				<option value="0" <?php if($quiz_series_detail['show_question_time'] == 0){ echo "selected";} ?> >No</option>
                  </select>
          <span class="error bold"><?php echo form_error('show_question_time');?></span>
        </div>
        <div class="form-group col-md-3">
                  <label for="exampleInputEmail1">Duplicate rank</label>
                  <select class="form-control input-sm" name="allow_duplicate_rank" >
				<option value="1" <?php if($quiz_series_detail['allow_duplicate_rank'] == 1){ echo "selected";} ?> >Yes</option>
				<option value="0" <?php if($quiz_series_detail['allow_duplicate_rank'] == 0){ echo "selected";} ?> >No</option>
                  </select>
          <span class="error bold"><?php echo form_error('allow_duplicate_rank');?></span>
        </div>
        <div class="form-group col-md-3">
                  <label for="exampleInputEmail1">Skip rank</label>
                  <select class="form-control input-sm" name="skip_rank" >
				<option value="1"  <?php if($quiz_series_detail['skip_rank'] == 1){ echo "selected";} ?> >Yes</option>
				<option value="0"  <?php if($quiz_series_detail['skip_rank'] == 0){ echo "selected";} ?> >No</option>
                  </select>
          <span class="error bold"><?php echo form_error('skip_rank');?></span>
        </div>
        <div class="form-group col-md-6 hide">
         <label for="exampleInputPassword1">Date Range</label>
				<div data-date-format="mm/dd/yyyy" data-date="13/07/2013" class="input-group input-large">
					<input type="text" name="start_date" class="form-control test_start_date"value="<?php  echo ($quiz_series_detail['start_date'] != "")?date('Y-m-d',$quiz_series_detail['start_date']/1000) : ""; ?>">
					<span style="color:red"><?php echo form_error('start_date'); ?></span>
					<span class="input-group-addon">To</span>
					<input type="text" name="end_date" class="form-control test_end_date" value="<?php echo ($quiz_series_detail['end_date'] != "")?date('Y-m-d',$quiz_series_detail['end_date']/1000) : ""; ?>">
					<span style="color:red"><?php echo form_error('end_date'); ?></span>
				</div>
				<span class="help-block text-center">Select date range</span>
        </div>
        <div class="col-md-12 hide">
          <div class="form-group col-md-3 bootstrap-timepicker">
            <label>Start time</label>
            <input type="text" placeholder="Start time" name = "start_time" id="start_time" value="<?php echo $quiz_series_detail['start_time']; ?>"  class="form-control input-sm test_start_timepicker">
             <span class="error bold"><?php echo form_error('start_time');?></span>
          </div>
          <div class="form-group col-md-3 bootstrap-timepicker">
            <label>End time</label>
            <input type="text" placeholder="End time" name = "end_time" id="end_time" value="<?php echo $quiz_series_detail['end_time']; ?>" class="form-control input-sm test_end_timepicker">
             <span class="error bold"><?php echo form_error('end_time');?></span>
          </div>
        </div>
        <div class="form-group col-md-6">
          <label>Pass message</label>
          <input type="text" placeholder="Pass message" name = "pass_message" id="pass_message" value="<?php echo $quiz_series_detail['pass_message']; ?>" class="form-control input-sm">
           <span class="error bold"><?php echo form_error('pass_message');?></span>
        </div>
        <div class="form-group col-md-6">
          <label>General message</label>
          <input type="text" placeholder="General message" name = "general_message" id="general_message" value="<?php echo $quiz_series_detail['general_message']; ?>" class="form-control input-sm">
           <span class="error bold"><?php echo form_error('general_message');?></span>
        </div>
        <div class="form-group col-md-6">
          <label>Fail message</label>
          <input type="text" placeholder="Fail message" name = "fail_message" id="fail_message" value="<?php echo $quiz_series_detail['fail_message']; ?>" class="form-control input-sm">
           <span class="error bold"><?php echo form_error('fail_message');?></span>
        </div>
		 <div class="form-group col-md-6">
          <label>Reward Points</label>
          <input type="text" placeholder="Reward Points" name = "reward_points" id="reward_points" value="<?php echo $quiz_series_detail['reward_points']; ?>" class="form-control input-sm">
           <span class="error bold"><?php echo form_error('reward_points');?></span>
        </div>
        <div class="form-group col-md-12">
          <input class="btn btn-info btn-sm"  type="submit" name="account_details_button" value="Update" >
        </div>
      </form>
        </div>
   </section>
</div>
<div id="tabContent5" class="col-lg-12 tabu">
    <section class="panel">
        <header class="panel-heading">
            Upload Cover image
        </header>
    <div class="panel-body">
    <div class="cover_image_element"><img src="<?php echo $quiz_series_detail['image']; ?>" style="max-width:100px;"></div>
                        <div class="panel-body">
                            <form role="form" action="<?php echo  AUTH_PANEL_URL .'quiz/Quiz/uploadimage';  ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
                                <div class="form-group">
                                    <label for="exampleInputFile">File input</label>
                                      <input type="hidden"  id="id" name ="id" value="<?php echo $quiz_series_detail['id']; ?>">
                                      <input type="file" accept="image/*" id="exampleInputFile" name ="userfile">
                                </div>
                                <button class="btn btn-info" type="submit">Submit</button>
                            </form>
                        </div>
    </div>
  </section>
</div>
<div id="tabContent6" class="col-lg-12 tabu">
    <section class="panel">
        <header class="panel-heading">
          Report
        </header>
        <div class="panel-body">
      		<div class="adv-table">
				<div class="col-md-6 pull-right">
					<div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
						 <div  class="input-group-addon">From</div>
						<input type="text" id="min-date-test-series" class="form-control date-range-filter input-sm course_start_date"  placeholder="">

						<div class="input-group-addon">to</div>

						<input type="text" id="max-date-test-series" class="form-control date-range-filter input-sm course_end_date"  placeholder="">

                      </div>
				</div>
      		<table  class="display table table-bordered table-striped col-md-12" style="width:100%" id="all-test-series-report-grid">
        		<thead>
          		<tr>
                <th>#</th>
                <th>User name</th>
                <th>Result</th>
			  <th> Creation time</th>
             <!--<th>Action </th>-->
          		</tr>
        		</thead>
            <thead>
                <tr>
                  <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
				<th><input type="text" data-column="3"  class="search-input-text form-control"></th>

                </tr>
            </thead>
      		</table>
      		</div>
	  </div>
	 </section>
</div>

	 <?php
            /* related course code start here
			* do not send key if no relation found
			*/

			$sql = "SELECT id ,title ,cover_image,mrp ,tags,  description,is_new ,for_dams ,non_dams
					from course_master
					where id IN (SELECT ctm.course_fk FROM course_segment_element as cse join course_topic_master ctm on ctm.id = cse.segment_fk where type='test' && element_fk = '".$quiz_series_detail['id']."' group by ctm.course_fk  ) ";
			$match = $this->db->query($sql)->result_array();
			if(count($match) > 0){

			}
    ?>


	<div id="tabContent7" class="col-lg-12 tabu">
      <section class="panel">
            <header class="panel-heading">
                    Used In Courses
            </header>
            <style>
            .course_desc {

                max-height: 40px;
                    min-height: 40px;
                    overflow: hidden;
                }
                .course_tags {
                    max-height: 40px;
                    min-height: 40px;
                    overflow: hidden;
                }
            </style>
            <div class="panel-body">
                <div class="row-fluid">
                    <ul class="thumbnails">
                        <?php foreach($match as $m){  ?>
                        <li class="col-md-4">
                            <div class="thumbnail">
                            <img alt="<?php echo $m['title'];?>" style="max-width: 300px; height: 100px;" src="<?php echo $m['cover_image'];?>">
                            <div class="caption">
                                <h4 class="capitalize" ><?php echo $m['title'];?></h4>
                                <p class="course_desc" ><?php echo substr($m['description'],0,80) . ' ...';?></p>
                                <p><a class="btn btn-success btn-xs " href="<?php echo AUTH_PANEL_URL.'course_product/course/edit_course_page?course_id='.$m['id'] ;?>">View</a></p>
                            </div>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </section>
	</div>



</div>







<?php
$adminurl = AUTH_PANEL_URL;
$test_series_id =  $quiz_series_detail['id'];
$subject_id =  $quiz_series_detail['subject'];
$topic_id =  $quiz_series_detail['topic_id'];
$total_question_reqd =  $quiz_series_detail['total_questions'];
$custum_js = <<<EOD
<script>
$('#container').addClass('sidebar-closed');
$('#main-content').css('margin-left',"0px");
</script>

         <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
         <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
         <script>

         
$( ".stream_element_select" ).change(function() {
	val = $(this).val();
	$('.sub_element_select').val('');
	$('.substream').hide();
	$('.sub'+val).show();
  });


		 function getCookie(cn) {
                    var name = cn+"=";
                    var allCookie = decodeURIComponent(document.cookie).split(';');
                    var cval = [];
                    for(var i=0; i < allCookie.length; i++) {
                        if (allCookie[i].trim().indexOf(name) == 0) {
                            cval = allCookie[i].trim().split("=");
                        }
                    }
                    return (cval.length > 0) ? cval[1] : "";
                }
                /* show hide magic */
                  $('.prod-cat a').click(function (e) {
                    div =  $(this).data('div');
                    $('.tabu').hide();
                    $(this).tab('show');

                    var tabContent = '#tabContent' + div;
                    $(tabContent).show();

                    document.cookie = "activediv_test="+tabContent;
                  });


                  if(getCookie("activediv_test")){
                       $('.tabu').hide();
                     $(getCookie('activediv_test')).show();
                  }

				  jQuery(document).ready(function() {
                       var table = 'all-user-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"quiz/Quiz/ajax_quiz_series_question_list/?test_series_id=$test_series_id", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('#all-user-grid .search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('#all-user-grid .search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );

                        $( document ).ajaxComplete(function( event, xhr, settings ) {
                          if ( settings.url === "$adminurl"+"test_series/test_series/ajax_test_series_question_list/?test_series_id=$test_series_id" ) {
                            //$( ".log" ).text( "Triggered ajaxComplete handler. The result is " +
                            //  xhr.responseText );
                            var obj = jQuery.parseJSON(xhr.responseText);
                            if('$total_question_reqd' != obj.recordsTotal ){
                              $('.show_question_warning').show();
                            }else{
                              $('.show_question_warning').hide();
                            }
                            //console.log(obj.recordsTotal);
                          }
                        });

                   } );

				   jQuery(document).ready(function() {
                       var table = 'all-test-series-report-grid';
                       var dataTable_test_series = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"test_series/test_series/ajax_test_series_report_list/?test_series_id=$test_series_id", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('#all-test-series-report-grid .search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_test_series.columns(i).search(v).draw();
                       } );
                        $('#all-test-series-report-grid .search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable_test_series.columns(i).search(v).draw();
                        } );
						// Re-draw the table when the a date range filter changes
                        $('#all-test-series-report-grid  .date-range-filter').change(function() {
                            if($('#min-date-test-series').val() !="" && $('#max-date-test-series').val() != "" ){
                                var dates = $('#min-date-test-series').val()+','+$('#max-date-test-series').val();
                                dataTable_test_series.columns(3).search(dates).draw();
                            }
                        });
                   } );

			               jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"'>"+value.name+"</option>";
                        });
                         $(".subject_element_select").html(html).val('$subject_id');
                      }
                    });
                    //*----onchnge subject_get_topic----*//
                    
                  function  chnge_topic(val) {
						
                      id = val;
                    //  alert(id);
                   //   alert(val);
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.topic+"</option>";
                          });
                           $(".topic_element_select").html(html).val('$topic_id');
                        }
                      });
                    }
                    //*----onchnge subject_get_topic-----*//

					 $('.test_start_date').datepicker({
						autoclose: true
					});
					$('.test_end_date').datepicker({
						autoclose: true
					});
					$('.test_start_timepicker').timepicker({
						autoclose: true,
						minuteStep: 1,
						showSeconds: false,
						showMeridian: false
					});
					$('.test_end_timepicker').timepicker({
						autoclose: true,
						minuteStep: 1,
						showSeconds: false,
						showMeridian: false
					});
					$('#min-date-test-series').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true

					});
					$('#max-date-test-series').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true

					});




			  jQuery(document).ready(function() {
			  			var subject_id = '$subject_id';
                       var table = 'subject_questions_list';
                       var dataTable_q_list = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"quiz/Quiz/ajax_subject_wise_question_list/?subject_id="+subject_id+"&test_series_id=$test_series_id", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           },
                           "pageLength": 100,
                           'columnDefs': [{
                               'targets': 7,
                               'searchable': false,
                               'orderable': false,
                               'className': 'dt-body-center',
                               /*'render': function (data, type, full, meta){
                                   return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                               }*/
                            }]
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('#subject_questions_list .search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_q_list.columns(i).search(v).draw();
                       } );
                        $('#subject_questions_list .search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable_q_list.columns(i).search(v).draw();
                        } );
                        // Handle click on "Select all" control
                        $('#example-select-all').on('click', function(){
                           // Get all rows with search applied
                           var rows = dataTable_q_list.rows({ 'search': 'applied' }).nodes();
                           // Check/uncheck checkboxes for all rows in the table
                           $('input[type="checkbox"]', rows).prop('checked', this.checked);
                        });

                        // Handle click on checkbox to set state of "Select all" control
                        $('#example tbody').on('change', 'input[type="checkbox"]', function(){
                           // If checkbox is not checked
                           if(!this.checked){
                              var el = $('#example-select-all').get(0);
                              // If "Select all" control is checked and has 'indeterminate' property
                              if(el && el.checked && ('indeterminate' in el)){
                                 // Set visual state of "Select all" control
                                 // as 'indeterminate'
                                 el.indeterminate = true;
                              }
                           }
                        });

                        // Handle form submission event
                         //$('#frm-example').on('submit', function(e){
                         $('.add_questions_element').on('click', function(e){
                         //add_questions_element
                            var form = this;

                            // Iterate over all checkboxes in the table
                            dataTable_q_list.$('input[type="checkbox"]').each(function(){
                                  // If checkbox is checked
                                  if(this.checked){
                                     // Create a hidden element
                                     //console.log($(this).val());
                                     var testSeriesId =	'$test_series_id';
                                     var questionId =	$(this).val();
                                     jQuery.ajax({
                                       url: "$adminurl"+"quiz/Quiz/add_question_to_quiz",
                                       method: 'POST',
                                       dataType: 'json',
                                       async:false,
                                       data: {
                                         "testSeriesId":  testSeriesId,
                                         "questionId":  questionId,
                                       },
                                       success: function (data) {
                                         $('#all-user-grid').DataTable().ajax.reload();
                                         show_toast('success', "Question added to test series.", "Question Added");
                                       }
                                     });
                                  }
                            });
                            $('#subject_questions_list').DataTable().ajax.reload();

                         });

                   } );



            $(document).on('click','.add_question_to_series',function() {
              var testSeriesId =	$(this).data('test-series-id');
              var questionId =	$(this).data('question-id');
              var subject_id =	$(this).data('section-id');
              jQuery.ajax({
                url: "$adminurl"+"quiz/Quiz/add_question_to_quiz",
                method: 'POST',
                dataType: 'json',
                async:false,
                data: {
                  "testSeriesId":  testSeriesId,
                  "questionId":  questionId,
                  "subject_id":  subject_id,
                  
                },
                success: function (data) {
                  $('#subject_questions_list').DataTable().ajax.reload();
                  $('#all-user-grid').DataTable().ajax.reload();
                  show_toast('success', "Question added to test series.", "Question Added");
                }
              });
            });

            $(function () {
                chnge_topic('$subject_id');
            });
            
    </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
