<?php //echo "<pre>";print_r($userQueryDetail);die;
//echo $this->session->userdata('active_backend_user_id');die; ?>
<div class="col-md-12">
<a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back to user query list</button></a>
</div>
<div class="col-md-12"><br></div>
<aside class="profile-nav col-lg-3">
      <section class="panel">
          <div class="user-heading round">
              <a href="#">
                  <img alt="" src="<?php echo $userQueryDetail[0]['profile_picture']; ?>">
              </a>
              <h1><?php echo $userQueryDetail[0]['name']; ?></h1>
              <p><?php echo $userQueryDetail[0]['email']; ?></p>
          </div>
      </section>
  </aside>
  <aside class="profile-info col-lg-9">
      
      <section class="panel">
          <div class="panel-body bio-graph-info">
              <h1>Bio Graph</h1>
              <div class="row">
                  <div class="bio-row">
                      <p><span>Sr.No </span>: <?php echo $userQueryDetail[0]['id']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Student Name </span>: <?php echo $userQueryDetail[0]['name']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Email </span>: <?php echo $userQueryDetail[0]['email']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>ROll Number </span>: N/A</p>
                  </div>

                  <div class="bio-row">
                      <p><span>Mobile</span>: <?php echo $userQueryDetail[0]['mobile']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Course</span>:
                       <?php 
                        if(array_key_exists('course', $userQueryDetail[0])) {
                              echo $userQueryDetail[0]['course'];
                            } else {
                               echo 'N/A';
                        }
                      ?>
                      
                       </p>
                  </div>
                  <div class="bio-row">
                      <p><span>Stream</span>: 
                      <?php
                        if(array_key_exists('stream', $userQueryDetail[0])) {
                              echo $userQueryDetail[0]['stream'];
                            } else {
                                echo 'N/A';
                              }
                      ?>
                      </p>
                  </div>
                  <div class="bio-row">
                      <p><span>Exam Interested</span>: 
                          <?php 
                              
                              if(array_key_exists('interested_course', $userQueryDetail[0]) && count($userQueryDetail[0]['interested_course'] > 0 )) {
                                foreach($userQueryDetail[0]['interested_course'] as $value) {
                                    echo $value.' | ';
                                }
                              } else { echo 'N/A';}
                           ?>
                      </p>
                  </div>
                  <div class="bio-row">
                      <p><span>Status</span>: N/A</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Date/Time of Registration </span>: <?php echo date("d-m-Y H:i:s", $userQueryDetail[0]['creation_time']/1000) ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Date/Time of Last Login </span>: <?php echo date("d-m-Y H:i:s", $userQueryDetail[0]['creation_time']/1000) ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>DAMS User/Non DAMS User</span>: N/A</p>
                  </div>
                  <div class="bio-row">
                      <p><span>User can be Active/Deactive</span>: N/A</p>
                  </div>
                   <div class="bio-row">
                      <p><span>Location</span>: N/A</p>
                  </div>
                   <div class="bio-row">
                      <p><span>Ip Address</span>: N/A</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Dams Token </span>: <?php echo $userQueryDetail[0]['dams_tokken']; ?></p>
                  </div>
                  
              </div>
          </div>
      </section>
</aside>
<?php foreach ($userQueryDetail as  $value) {  ?>
<div class="col-lg-9 pull-right">
    <section class="panel">
        <div class="panel-body">
            <div class="fb-user-thumb">
                <img alt="" src="<?php echo $value['profile_picture']; ?>">
            </div>
            <div class="fb-user-details">
                <h3><a class="bold" href="#"><?php echo $value['name']; ?></a></h3>
                <?php
                    $date1 = date("Y-m-d H:i:s", $value['time']/1000);
                    $date1 = strtotime($date1);
                    $date2 = strtotime(date('Y-m-d H:i:s'));
                    $days = $date2 - $date1;
                    $days = round($days / 86400);
                    if ($days < 1) {
                        $days = 'Today';
                    } else if ($days <= 31) {
                        $days = $days . ' days ago';
                    } else if ($days > 32) {
                        $m = $days / 30;
                        $m = intval($m);
                        $days = $m . ' month ago';
                    }
                ?>
                <p><?php echo $days .' | '. $value['category']; ?></p>

            </div>
            <div class="clearfix"></div>
            <p class=""><span style="font-weight: 700;">Title : </span><?php echo  $value['title']; ?>
            </p>
            
            <p class=""><?php echo $value['description']; ?>
            </p>

            <img style="height:100px" class="img-thumbnail"  src="<?php echo  $value['file']; ?>">

            <div class="fb-status-container fb-border fb-gray-bg">
                
                <ul class="fb-comments">
                    <?php if(!empty($value['query_reply'])) { 
                        foreach ($value['query_reply'] as  $query_value) {
                          //find reply time ........
                          $query_date1 = strtotime($query_value['create_date']);
                          $query_date2 = strtotime(date('Y-m-d H:i:s'));
                          $query_days = $query_date2 - $query_date1;
                          $query_days = round($query_days / 86400);
                          if ($query_days < 1) {
                              $query_days = 'Today';
                          } else if ($query_days <= 31) {
                              $query_days = $query_days . ' day(s) ago';
                          } else if ($query_days > 32) {
                              $m = $query_days / 30;
                              $m = intval($m);
                              $query_days = $m . ' month(s) ago';
                          }
                    ?>
                    <li>
                        <a class="cmt-thumb" href="#">
                            <i class="fa fa-user fa-4x"></i>
                        </a>
                        <div class="cmt-details">
                            <a href="#"><?php echo $query_value['username']; ?></a>
                            <span style="color:#797979 !important"><?php echo $query_value['text']; ?></span>
                            <p style="color:#797979 !important"><?php echo $query_days; ?></p>
                        </div>
                    </li>
                    <?php } 
                       }
                    ?>

                    <li>
                        <a class="cmt-thumb" href="#">
                            <i class="fa fa-user fa-4x"></i>
                        </a>
                        <form action="<?php echo AUTH_PANEL_URL.'user_query/user_query_admin_reply'; ?>" method="post">
                        <div class="cmt-form">
                            <textarea name="text" placeholder="Write a comment..." class="form-control submit_on_enter" required="required"></textarea>
                            <input type="hidden"  name="query_id" value="<?php echo $value['id']; ?>">
                            <input type="hidden"  name="backend_user_id" value="<?php echo $this->session->userdata('active_backend_user_id'); ?>">
                            <input type="hidden" name="queried_user_email" value="<?php echo $userQueryDetail[0]['email']; ?>">
                        </div>
                        <button class="btn btn-primary pull-right btn-xs bold" style="position: relative;right:1.6%" type="submit">Send</button>
                        </form>
                    </li>
                </ul>

                <div class="clearfix"></div>
            </div>
        </div>
    </section>
</div>
<?php } ?>


<?php
/*$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              
               <script type="text/javascript" language="javascript" >

                  jQuery(document).ready(function() {
                    $('.submit_on_enter').keydown(function(event) {
                      // enter has keyCode = 13, change it if you want to use another button
                      if (event.keyCode == 13 ) {
                        this.form.submit();
                        return false;
                      }
                    });
                  });
               </script>

EOD;

  echo modules::run('auth_panel/template/add_custum_js',$custum_js );*/
?>
