
<?php 
$tags = $this->db->where('master_id',$user_register_record['master_id'])->get('post_tags')->result_array();
?>
<aside class="profile-nav col-lg-3"> 
      <section class="panel">
          <div class="user-heading round">
              <a href="#">
                  <img alt="" src="<?php echo $user_data['profile_picture']; ?>">
              </a>
              <h1><?php echo $user_data['name']; ?></h1>
              <p><?php echo $user_data['email']; ?></p>
          </div>

          <ul class="nav nav-pills nav-stacked">
              <li class="active"><a href="#"> <i class="fa fa-user"></i> Profile</a></li>
              <li><a href="<?php echo AUTH_PANEL_URL.'fan_wall/all_post?search_user_post_id='.$user_data['id']; ?>"> <i class="fa fa-calendar"></i> Post<span class="label label-danger pull-right"><?php echo $user_data['post_count']; ?></span></a></li>
              <li><a href="#"> <i class="fa fa-calendar"></i> Followers <span class="label label-danger pull-right"><?php echo $user_data['followers_count']; ?></span></a></li>
              <li><a href="#"> <i class="fa fa-calendar"></i> Following <span class="label label-danger pull-right"><?php echo $user_data['following_count']; ?></span></a></li>
          </ul>

      </section>

      <!--moderator form -->
              <section class="panel hide">
                  <header class="panel-heading">
                      Moderator
                  </header>
                  <div class="panel-body">
                      <form role="form" class="form-inline" method="post" action="<?php echo AUTH_PANEL_URL.'web_user/is_moderator/'.$user_data['id']; ?>">
                          <div class="form-group col-md-4">
                              <label><input type="radio" <?php echo $checked = ($user_data['is_moderate'] == 1)?'checked':''; ?> value="1"  name="modrator">Yes</label>
                          </div>
                          <div class="form-group col-md-4">
                              <label><input type="radio" <?php echo $checked = ($user_data['is_moderate'] == 0)?'checked':''; ?> value="0"  name="modrator">No</label>
                          </div>
                          <div class="form-group col-md-4">
                          <button class="btn btn-xs bold btn-success pull-right" type="submit">Save</button>
                          </div>
                      </form>

                  </div>
              </section>

			  <section class="panel hide">
                  <header class="panel-heading">
                      Instructor
                  </header>
                  <div class="panel-body">
                      <form role="form" class="form-inline" method="post" action="<?php echo AUTH_PANEL_URL.'web_user/is_instructor/'.$user_data['id']; ?>">
                          <div class="form-group col-md-4">
                              <label><input type="radio" <?php echo $checked = ($user_data['is_instructor'] == 1)?'checked':''; ?> value="1"  name="instructor">Yes</label>
                          </div>
                          <div class="form-group col-md-4">
                              <label><input type="radio" <?php echo $checked = ($user_data['is_instructor'] == 0)?'checked':''; ?> value="0"  name="instructor">No</label>
                          </div>
                          <div class="form-group col-md-4">
                          <button class="btn btn-xs bold btn-success pull-right" type="submit">Save</button>
                          </div>
                      </form>

                  </div>
              </section>

		          <section class="panel">
                  <header class="panel-heading">
                      Expert
                  </header>
                  <div class="panel-body">
                      <form role="form" class="form-inline" method="post" action="<?php echo AUTH_PANEL_URL.'web_user/is_expert/'.$user_data['id']; ?>">
                          <div class="form-group col-md-4">
                              <label><input type="radio" <?php echo $checked = ($user_data['is_expert'] == 1)?'checked':''; ?> value="1"  name="expert">Yes</label>
                          </div>
                          <div class="form-group col-md-4">
                              <label><input type="radio" <?php echo $checked = ($user_data['is_expert'] == 0)?'checked':''; ?> value="0"  name="expert">No</label>
                          </div>
                          <div class="form-group col-md-4">
                          <button class="btn btn-xs bold btn-success pull-right" type="submit">Save</button>
                          </div>
                      </form>                     
                  </div>
              </section>

  </aside>
  <aside class="profile-info col-lg-9">

      <section class="panel">
          <div class="panel-body bio-graph-info">
              <h1>Bio Graph <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back to user list</button></a> </h1>
              <div class="row">
                  <div class="bio-row">
                      <p><span>Sr.No </span>: <?php echo $user_data['id']; ?></p>
                  </div>
                  <div class="bio-row">
                    <p ><span>Student Name </span>: <span id='lblName' class="editable"><?php echo $user_data['name']; ?></span>&nbsp <a href="javascript:void(0)"><i id="" data-edit_id="#lblName" class=" editable_input fa btn btn-xs btn-success">edit</i></a></span>
                    </p>
                  </div>
                  <div class="bio-row">
                    <p ><span>Designation </span>: <span id='lbldesg' class="editabl"><?php echo $user_data['designation']; ?></span>&nbsp <a href="javascript:void(0)"><i id="" data-edit_id="#lbldesg" class=" editable_input_desg fa btn btn-xs btn-success">edit</i></a></span>
                    </p>
                  </div>

                  <div class="bio-row">
                    <p ><span>Speciality </span>: <span id='lblspec' class="editable"><?php echo $user_data['speciality']; ?></span>&nbsp <a href="javascript:void(0)"><i id="" data-edit_id="#lblspec" class=" editable_input_spec fa btn btn-xs btn-success">edit</i></a></span>
                    </p>
                  </div>
                 
                  <div class="bio-row">
                      <p><span>Email </span>: <?php echo $user_data['email']; ?></p>
                  </div>
                  <div class="bio-row hide ">
                      <p><span>MME ID </span>: <?php echo ($user_data['dams_tokken'] !="")?$user_data['dams_tokken']:"Non MME Student"; ?></p>
                  </div>

                  <div class="bio-row">
                      <p><span>Mobile</span>: <?php echo $user_data['mobile']; ?></p>
                  </div>
                  <div class="bio-row hide">
                      <p><span>Course</span>:
                       <?php
                        if(array_key_exists('course', $user_data)) {
                              echo $user_data['course'];
                            } else {
                               echo 'N/A';
                        }
                      ?>

                       </p>
                  </div>
                  <div class="bio-row hide">
                      <p><span>Stream</span>:
                      <?php
                        if(array_key_exists('stream', $user_data)) {
                              echo $user_data['stream'];
                            } else {
                                echo 'N/A';
                              }
                      ?>
						 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span>Speciality</span>:
                      <?php
                        if(array_key_exists('speciality', $user_data)) {
                              echo $user_data['speciality'];
                            } else {
                                echo 'N/A';
                              }
                      ?>
                      </p>
                  </div>
                  <div class="bio-row hide">
                      <p><span>Exam Interested</span>:
                          <?php

                              if(array_key_exists('interested_course', $user_data) && count($user_data['interested_course'] > 0 )) {
                                foreach($user_data['interested_course'] as $value) {
                                    echo $value.' | ';
                                }
                              } else { echo 'N/A';}
                           ?>
                      </p>
                  </div>
                  <!--<div class="bio-row">
                      <p><span>Status</span>: N/A</p>
                  </div>-->
                  <div class="bio-row">
                      <p><span>Date/Time of Registration </span>: <?php echo date("d-m-Y H:i:s", $user_data['creation_time']/1000) ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Date/Time of Last Login </span>: <?php echo date("d-m-Y H:i:s", $user_data['creation_time']/1000) ?></p>
                  </div>
                  <div class="bio-row hide ">
                      <p><span>LOGIN INFO </span>: <?php echo $user_data['user_info'];?></p>
                  </div>
                  <!--<div class="bio-row">
                      <p><span>DAMS User/Non DAMS User</span>: N/A</p>
                  </div>-->
                  <!--<div class="bio-row">
                      <p><span>User can be Active/Deactive</span>: N/A</p>
                  </div>-->
                  <!--<div class="bio-row">
                      <p><span>Location</span>: N/A</p>
                  </div>-->
                  <!--<div class="bio-row">
                      <p><span>Ip Address</span>: N/A</p>
                  </div>-->
              </div>
            <div class="col-md-12 pull-right">

              <?php if($user_data['status'] != '2') { ?>
                <div class="col-md-2 pull-right">
                    <a href="<?php echo AUTH_PANEL_URL . 'web_user/delete_user/delete/'.$user_data['id']; ?>" onclick="return confirm('Are you sure to delete this user');"><button class="pull-right btn btn-danger btn-xs bold">Delete User</button></a>
                </div>
                <div class="col-md-2 pull-right">
                <?php if($user_data['status'] == '1') { ?>
                    <a href="<?php echo AUTH_PANEL_URL . 'web_user/enable_user/enable/'.$user_data['id']; ?>"><button class="pull-right btn btn-warning btn-xs bold">Enable login</button></a>
                <?php } else { ?>
                    <a href="<?php echo AUTH_PANEL_URL . 'web_user/disable_user/disable/'.$user_data['id']; ?>" onclick="return confirm('Are you sure to disable this user');"><button class="pull-right btn btn-warning btn-xs bold">Disable login</button></a>
                <?php } ?>
                </div>
                <?php } else { ?>
                <div class="col-md-2 pull-right">
                    <a href="#"><button class="pull-right btn btn-info btn-xs bold">User Deleted</button></a>
                </div>
              <?php } ?>


              <div class="col-md-2 pull-right">
                  <a href="<?php echo AUTH_PANEL_URL . 'bulk_messenger/push_notification/send_push_notification?q='.base64_encode(json_encode(array('id'=>$user_data['id'],'name'=>$user_data['name'],'device_type'=>$user_data['device_type'],'device_tokken'=>$user_data['device_tokken']))); ?>"><button class="pull-right btn btn-info btn-xs bold">Push Notification</button></a>
              </div>

              <div class="col-md-2 pull-right">
                  <a onclick="return confirm('You are going to refresh session of logged in user. User session will be destroyed.')" href="<?php echo AUTH_PANEL_URL . 'web_user/reset_session/'.$user_data['id']; ?>"><button class="pull-right btn btn-danger btn-xs bold">Refresh session</button></a>
              </div>

            </div>
          </div>
      </section>

<?php
    $query = $this->db->query("select  `id`, `title`, `course_main_fk`, `course_category_fk`, `subject_id`, `description`, `cover_image`,`desc_header_image` ,`cover_video`, `tags`, `mrp`, `publish`, `is_new`, `state`, `for_dbmci`, `non_dbmci`, `instructor_id`, `instructor_share`, course_rating_count as rating , `course_review_count` as review_count, course_learner+fake_learner as learner from course_master where id in (SELECT distinct(course_id) from course_transaction_record where user_id = ".$user_data['id']."  and transaction_status = 1) ");
    $user_courses =  $query->result_array();
?>

        <section class="panel">
          <div class="panel-body bio-graph-info">
                  <h1>Courses Added</h1>
            <ul class="thumbnails">

            <style>
            .course_desc {

                max-height: 40px;
                    min-height: 40px;
                    overflow: hidden;
                }
                .course_tags {
                    max-height: 40px;
                    min-height: 40px;
                    overflow: hidden;
                }
                .c-title{
                    max-height: 40px;
                    min-height: 40px;
                    overflow: hidden;
                }
            </style>

             <?php foreach($user_courses as $m){  ?>
                        <li class="col-md-4">
                            <div class="thumbnail">
                            <img alt="<?php echo $m['title'];?>" style="max-width: 300px; height: 100px;" src="<?php echo $m['cover_image'];?>">
                            <div class="caption">
                                <h4 class="capitalize c-title" ><?php echo $m['title'];?></h4>
                                <p class="course_desc" ><?php echo substr($m['description'],0,80) . ' ...';?></p>
                                <?php $tags = explode(',',$m['tags']);?>
                                <p class="course_tags"> Tags -:
                                  <?php foreach($tags as $t  ) { ?>
                                  <span class="badge bg-inverse capitalize"><?php echo $t;?></span>
                                  <?php } ?>
                                </p>
                                <p><a class="btn btn-success btn-xs " href="<?php echo AUTH_PANEL_URL.'course_product/course/edit_course_page?course_id='.$m['id'] ;?>">View</a></p>
                            </div>
                            </div>
                        </li>
              <?php } ?>

            </ul>
          </div>
        </section>

	  <?php  if($user_data['is_instructor']== 1){?>
	   <section class="panel hide">
                          <header class="panel-heading">
                              Instructor Details
                          </header>
                          <div class="panel-body">
                              <form role="form" method="post">
                                  <div class="form-group">
                                      <input type="hidden" placeholder="" id="user_id" name = "user_id" value ="<?php if(isset($user_instructor_data['user_id'])){ echo $user_instructor_data['user_id']; } ?>" class="form-control">
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">Beneficiary name</label>
                                      <input type="text" placeholder="Enter beneficiary name"  name = "u_name" value ="<?php if(isset($user_instructor_data['u_name'])){ echo $user_instructor_data['u_name']; } ?>" class="form-control">
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">Beneficiary account</label>
                                      <input type="text" placeholder="Enter beneficiary account"  name = "b_account" value ="<?php if(isset($user_instructor_data['b_account'])){ echo $user_instructor_data['b_account']; } ?>" class="form-control">
                                  </div>
								                    <div class="form-group">
                                      <label for="exampleInputEmail1">Bank IFSC</label>
                                      <input type="text" placeholder="Enter bank IFSC"  name = "b_ifsc" value ="<?php if(isset($user_instructor_data['b_ifsc'])){ echo $user_instructor_data['b_ifsc']; } ?>" class="form-control">
                                  </div>
								                  <div class="form-group">
                                      <label for="exampleInputEmail1">Bank address</label>
                                      <input type="text" placeholder="Enter bank address"  name = "b_address" value ="<?php if(isset($user_instructor_data['b_address'])){ echo $user_instructor_data['b_address']; }  ?>" class="form-control">
                                  </div>
							                 	  <div class="form-group">
                                      <label for="exampleInputEmail1">Bank name</label>
                                      <input type="text" placeholder="Enter bank name"  name = "b_name" value ="<?php if(isset($user_instructor_data['b_name'])){ echo $user_instructor_data['b_name']; } ?>" class="form-control">
                                  </div>
                                   <div class="form-group">
                                      <label for="exampleInputEmail1">Resource sharing</label>
                                      <input type="text" placeholder="Enter resource sharing"  name = "r_sharing" value ="<?php if(isset($user_instructor_data['r_sharing'])){ echo $user_instructor_data['r_sharing']; } ?>" class="form-control">
                                  </div>
                                  <button class="btn btn-info btn-sm" type="submit" name="update_instructor_detail">Update</button>
                              </form>

                          </div>
                      </section>

		<div class="col-sm-12 hide">
		<section class="panel">
			<header class="panel-heading">
		 Instructor's reviews
	</header>
			<div class="panel-body">
			<div class="adv-table">
			<table  class="display table table-bordered table-striped" id="all-instructor-reviews-grid">
			<thead>
				<tr>

			  <th>#</th>
			  <th>User name</th>
			  <th>Rating</th>
			  <th>Review</th>
			  <th>Created at</th>

			 <th>Action </th>
				</tr>
			</thead>
		  <thead>
			  <tr>
				  <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="4"  class="search-input-text form-control"></th>


				 <th></th>
			  </tr>
		  </thead>
			</table>
			</div>
			</div>
		</section>
	</div>
	  <?php } ?>
</aside>
<?php
$is_instructor = 0;
if(isset($user_instructor_data)){
	$is_instructor = 1; }

$instructor_id = $user_data['id'];
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
                <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
				<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
                <script type="text/javascript" language="javascript" >
					var is_instructor = '$is_instructor';
					if(is_instructor == 1){
						jQuery(document).ready(function() {
						   var table = 'all-instructor-reviews-grid';
						   var dataTable = jQuery("#"+table).DataTable( {
							   "processing": true,
								"pageLength": 15,
								"lengthMenu": [[15, 25, 50], [15, 25, 50]],
							   "serverSide": true,
							   "order": [[ 4, "desc" ]],
							   "ajax":{
								   url :"$adminurl"+"web_user/ajax_instructor_ratings_list/$instructor_id", // json datasource
								   type: "post",  // method  , by default get
								   error: function(){  // error handling
									   jQuery("."+table+"-error").html("");
									   jQuery("#"+table+"_processing").css("display","none");
								   }
							   }
						   } );
						   jQuery("#"+table+"_filter").css("display","none");
						   $('.search-input-text').on( 'keyup click', function () {   // for text boxes
							   var i =$(this).attr('data-column');  // getting column index
							   var v =$(this).val();  // getting search input value
							   dataTable.columns(i).search(v).draw();
						   } );
							$('.search-input-select').on( 'change', function () {   // for select box
								var i =$(this).attr('data-column');
								var v =$(this).val();
								dataTable.columns(i).search(v).draw();
							} );
					   } );
				   }

               </script>			   
				<script>
				
				  $( ".editable_input" ).click(function(){
              var thiss = $(this);
              var get = $(this).data('edit_id');
              if($(this).html() == 'edit'){
                $(this).html('save');
                
                inner = $(get).html();
                var input = "<input id='edit_name_in' type = 'text' value='"+inner+"'  />";
                $(get).html(input);
              }else{
                var inp = $.trim($('#edit_name_in').val())
                 $(this).html("<i class='fa fa-refresh fa-spin'></i>");
                $.ajax({  
                   url :"$adminurl"+"web_user/update_name/$instructor_id",
                   data: {a:inp},
                   method:'POST',
                    success: function(response){
                     console.log("success");
                      $(thiss).html('edit');
                   }
                });

                 $(get).html(inp);
              }
      
						});		

                </script>
                
                <script>
                //for designation editable
				  $( ".editable_input_desg" ).click(function(){
                      //alert("hiii");
              var thiss = $(this);
              var get = $(this).data('edit_id');
              if($(this).html() == 'edit'){
                $(this).html('save');
                
                inner = $(get).html();
                var input = "<input id='edit_name_in' type = 'text' value='"+inner+"'  />";
                $(get).html(input);
              }else{
                var inp = $.trim($('#edit_name_in').val())
                 $(this).html("<i class='fa fa-refresh fa-spin'></i>");
                $.ajax({  
                   url :"$adminurl"+"web_user/update_desg/$instructor_id",
                   data: {a:inp},
                   method:'POST',
                    success: function(response){
                     console.log("success");
                      $(thiss).html('edit');
                   }
                });

                 $(get).html(inp);
              }
      
                        });
                        
                        
                //for speciality
                $( ".editable_input_spec" ).click(function(){
                    ///alert("hiii");
            var thiss = $(this);
            var get = $(this).data('edit_id');
            if($(this).html() == 'edit'){
              $(this).html('save');
              
              inner = $(get).html();
              var input = "<input id='edit_name_in' type = 'text' value='"+inner+"'  />";
              //alert(input);
              $(get).html(input);
            }else{
              var inp = $.trim($('#edit_name_in').val())
               $(this).html("<i class='fa fa-refresh fa-spin'></i>");
              $.ajax({  
                 url :"$adminurl"+"web_user/update_spec/$instructor_id",
                 data: {a:inp},
                 method:'POST',
                  success: function(response){
                   console.log("success");
                    $(thiss).html('edit');
                 }
              });

               $(get).html(inp);
            }
    
                      });        

				</script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
