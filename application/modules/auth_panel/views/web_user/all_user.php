<?php $period = $this->input->get('period'); ?>
<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		<?php echo strtoupper($page); ?> USER(s) LIST
    <!-- dtae filters -->

    <span class="tools pull-right">
     <form role="form" method="get" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  >
      <select name="period" class="form-control input-sm m-bot15 period" onchange="this.form.submit()">
          <option <?php if($period == "today"){ echo "selected";} ?> value="today">Today</option>
          <option <?php if($period == "yesterday"){ echo "selected";} ?>  value="yesterday">Yesterday</option>
          <option <?php if($period == "7days"){ echo "selected";} ?>  value="7days">Last 7 Days</option>
          <option <?php if($period == "current_month"){ echo "selected";} ?>  value="current_month">Current Month</option>
          <option <?php if($period == "custom"){ echo "selected";} ?>  value="custom">Custom</option>
          <option <?php if($period == "" || $period == "all"){ echo "selected";} ?>  value="all">All</option>
      </select>
      <?php
        foreach($_GET as $key=>$value){
          if($key != 'period'){
            echo "<input type='hidden' value='".$value."' name='".$key."'>";
          }        
        }
      ?>
    </form>
    </span>
    <span class="tools pull-right">
      <form id="download_content_csv" method="post" action=""  >
        <button class="btn btn-sm btn-danger margin-right bold"> 
          <i class="fa fa-file" aria-hidden="true"></i>
          Download csv 
        </button>
        <input name="download_pdf" class="btn btn-info btn-sm  margin-right bold" value="Download PDF" type="submit">
        <textarea style="display:none;" name="input_json"></textarea>
      </form>
    </span>
		</header>
    <div class="clearfix"></div>
		<div class="panel-body">
            <div class="adv-table ">

                <!-- date filter  -->

           <?php if($period == "custom" ){  ?>
              <div class="col-md-6 pull-right custom_search" >
                    <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
                        <div  class="input-group-addon">From</div>
                        <input type="text" id="min-date-course-transaction" class="form-control date-range-filter input-sm course_start_date"  placeholder="">
                        <div class="input-group-addon">to</div>
                        <input type="text" id="max-date-course-transaction" class="form-control date-range-filter input-sm course_end_date"  placeholder="">
                        <div class="input-group-addon btn date-range-filter-clear">Clear</div>
                    </div>
                </div>
              <?php } ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
                    <table  class="table display table-bordered table-striped" id="all-user-grid">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name </th>
                                <th>Email </th>
                                <th>Mobile </th>
                                <th>DBMCI User </th>
                                <th>DBMCI ID </th>
                                <th>Is mod. </th>
                                <th>Status </th>
                                <th>On </th>
                                <th>@</th>
                                <th>Points</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
                                <th><select data-column="4"  class="form-control search-input-select">
                                        <option value="0">All</option>
                                        <option value="1">Yes</option>
                                        <option value="2">No</option>
                                    </select></th>
                                <th><input type="text" data-column="5"  class="search-input-text form-control"></th>
                                <th><select data-column="6"  class="form-control search-input-select">
                                        <option value="">All</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select></th>
                                <th><select data-column="7"  class="form-control search-input-select">
                                        <option value="">All</option>
                                        <option value="0">Active</option>
                                        <option value="1">Disable</option>

                                    </select></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>

                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
		</div>
	</section>
</div>

<?php
$query_string = "";
$adminurl = AUTH_PANEL_URL;
if($page == 'android'){
	$device_type = 1;
}elseif($page == 'ios'){
	$device_type = 2;
}elseif($page == 'all'){
	$device_type = '0';
}elseif($page == 'instructor') {
	$device_type = '0';
	$query_string = 'instructor';
}elseif($page == 'expert') {
	$device_type = '0';
	$query_string = 'expert';
}elseif($page == 'mentor') {
	$device_type = '0';
	$query_string = 'mentor';
}

$custum_js = <<<EOD
              <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
               <script type="text/javascript" language="javascript" >
                   var all_user_all = "$adminurl"+"web_user/ajax_all_user_list/"+"$device_type"+"?period=$period&user=$query_string"; 
                   var all_user_csv = "$adminurl"+"web_user/get_request_for_csv_download/"+"$device_type"+"?period=$period&user=$query_string";

                   jQuery(document).ready(function() {
                       var table = 'all-user-grid';
                       var dataTable_user = jQuery("#"+table).DataTable( {
                            "processing": true,
                            "language": {
                                "processing": "<i class='fa fa-spin  fa-spinner fa-4x' style='z-index:999'><i>" //add a loading image,simply putting <img src="loader.gif" /> tag.
                            },
                            // "autoWidth": false,
                            // "responsive": true,
                            // "scrollX": true,
                            "pageLength": 100,
                            "lengthMenu": [[15, 25, 50 ,100], [15, 25, 50,100]],
                            "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url : all_user_all, // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_user.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable_user.columns(i).search(v).draw();
                        } );
						            // Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date-user').val() !="" && $('#max-date-user').val() != "" ){
                                var dates = $('#min-date-user').val()+','+$('#max-date-user').val();
                                dataTable_user.columns(8).search(dates).draw();
                            }
                        });
                        $('.date-range-filter-clear').on( 'click', function () {
                            // clear date filter
                            $('#min-date-course-transaction').val('');
                            $('#max-date-course-transaction').val("");
                            dataTable_transaction.columns(9).search('').draw();
                        });
                        $( document ).ajaxComplete(function( event, xhr, settings ) {
                            if ( settings.url === all_user_all ) {
                               var obj = jQuery.parseJSON(xhr.responseText);
                               var read =  obj.posted_data;

                              $('#download_content_csv').attr('action',all_user_csv);
                              $('textarea[name=input_json]').val(JSON.stringify(read));
                              
                            }
                        });
                   } );

              $('#min-date-course-transaction').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true
              });
              $('#max-date-course-transaction').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true
              });

              $('.period').on( 'change', function () {
                period = $(this).val();
                if(period == "custom"){
                  $('.custom_search').show();
                }
              });
     </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
