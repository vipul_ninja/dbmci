<?php
$sql = "SELECT count(*) as total  
FROM video_master ";
$total =  $this->db->query($sql)->row()->total;

$sql = "SELECT count(id) as total   
FROM video_master where for_dams =1 or for_non_dams = 1";
$published =  $this->db->query($sql)->row()->total;

$sql = "SELECT count(id) as total   
FROM video_master where for_dams =1";
$for_dams_video =  $this->db->query($sql)->row()->total;
$sql = "SELECT count(id) as total   
FROM video_master where  for_non_dams = 1";
$for_non_dams_video =  $this->db->query($sql)->row()->total;
?>
<div class="col-sm-12">
    <div class=" state-overview">
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol terques">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class="count">
                    <?php echo $total; ?>                
                    </h1>
                    <p>Total videos</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol red">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class=" count2">
                       <?php echo $published; ?>                
                    </h1>
                    <p>Published videos</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol yellow">
                    <i class="fa  fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class=" count3"><?php echo $for_dams_video; ?>  </h1>
                    <p>For dams <br><sup class="">(published)</sup> </p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol blue">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class=" count4"><?php echo $for_non_dams_video; ?>  </h1>
                    <p>For non dams <br><sup class="">(published)</sup></p>
                </div>
            </section>
        </div>
    </div>
</div>

<?php
    $year_graph =  date('Y');
    $yearly_sql = "SELECT 
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '01' THEN 1 ELSE 0 END),0) AS 'January',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '02' THEN 1 ELSE 0 END),0) AS 'February',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '03' THEN 1 ELSE 0 END),0) AS 'March',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '04' THEN 1 ELSE 0 END),0) AS 'April',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '05' THEN 1 ELSE 0 END),0) AS 'May',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '06' THEN 1 ELSE 0 END),0) AS 'June',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '07' THEN 1 ELSE 0 END),0) AS 'July',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '08' THEN 1 ELSE 0 END),0) AS 'August',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '09' THEN 1 ELSE 0 END),0) AS 'September',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '10' THEN 1 ELSE 0 END),0) AS 'October',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '11' THEN 1 ELSE 0 END),0) AS 'November',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '12' THEN 1 ELSE 0 END),0) AS 'December',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%Y') WHEN  $year_graph THEN 1 ELSE 0 END),0) AS 'TOTAL'
                    FROM video_view_meta
                    where DATE_FORMAT(FROM_UNIXTIME(time/1000), '%Y') = $year_graph ";
                    $out_yearly = $this->db->query($yearly_sql)->row();  
                           

                    $yearly_sql = "SELECT 
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '01' THEN 1 ELSE 0 END),0) AS 'January',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '02' THEN 1 ELSE 0 END),0) AS 'February',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '03' THEN 1 ELSE 0 END),0) AS 'March',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '04' THEN 1 ELSE 0 END),0) AS 'April',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '05' THEN 1 ELSE 0 END),0) AS 'May',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '06' THEN 1 ELSE 0 END),0) AS 'June',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '07' THEN 1 ELSE 0 END),0) AS 'July',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '08' THEN 1 ELSE 0 END),0) AS 'August',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '09' THEN 1 ELSE 0 END),0) AS 'September',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '10' THEN 1 ELSE 0 END),0) AS 'October',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '11' THEN 1 ELSE 0 END),0) AS 'November',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '12' THEN 1 ELSE 0 END),0) AS 'December',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%Y') WHEN  $year_graph THEN 1 ELSE 0 END),0) AS 'TOTAL'
                    FROM video_like_meta
                    where DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%Y') = $year_graph  ";
                    $out_yearly_likes = $this->db->query($yearly_sql)->row();   

                    $yearly_sql = "SELECT 
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '01' THEN 1 ELSE 0 END),0) AS 'January',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '02' THEN 1 ELSE 0 END),0) AS 'February',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '03' THEN 1 ELSE 0 END),0) AS 'March',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '04' THEN 1 ELSE 0 END),0) AS 'April',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '05' THEN 1 ELSE 0 END),0) AS 'May',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '06' THEN 1 ELSE 0 END),0) AS 'June',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '07' THEN 1 ELSE 0 END),0) AS 'July',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '08' THEN 1 ELSE 0 END),0) AS 'August',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '09' THEN 1 ELSE 0 END),0) AS 'September',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '10' THEN 1 ELSE 0 END),0) AS 'October',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '11' THEN 1 ELSE 0 END),0) AS 'November',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '12' THEN 1 ELSE 0 END),0) AS 'December',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%Y') WHEN  $year_graph THEN 1 ELSE 0 END),0) AS 'TOTAL'
                    FROM video_master_comment 
                    where DATE_FORMAT(FROM_UNIXTIME(time/1000), '%Y') = $year_graph
                    ";
                    $out_yearly_comments = $this->db->query($yearly_sql)->row();  
                                         
                    $out_yearly_js ="['JAN',  ".$out_yearly->January." ,  ".$out_yearly_likes->January." ,  ".$out_yearly_comments->January."  ],
                    ['FEB',  ".$out_yearly->February." ,  ".$out_yearly_likes->February." ,  ".$out_yearly_comments->February." ],
                    ['MAR',  ".$out_yearly->March." ,  ".$out_yearly_likes->March."  ,  ".$out_yearly_comments->March." ],
                    ['APR',  ".$out_yearly->April.",  ".$out_yearly_likes->April." ,  ".$out_yearly_comments->April." ],
                    ['MAY',  ".$out_yearly->May." ,  ".$out_yearly_likes->May.",  ".$out_yearly_comments->May."  ],
                    ['JUN',  ".$out_yearly->June." ,  ".$out_yearly_likes->June." ,  ".$out_yearly_comments->June." ],
                    ['JUL',  ".$out_yearly->July." ,  ".$out_yearly_likes->July."  ,  ".$out_yearly_comments->July." ],
                    ['AUG',  ".$out_yearly->August." ,  ".$out_yearly_likes->August.",  ".$out_yearly_comments->August." ],
                    ['SEP',  ".$out_yearly->September." ,  ".$out_yearly_likes->September." ,  ".$out_yearly_comments->September." ],
                    ['OCT',  ".$out_yearly->October." ,  ".$out_yearly_likes->October." ,  ".$out_yearly_comments->October." ],
                    ['NOV',  ".$out_yearly->November." ,  ".$out_yearly_likes->November."  ,  ".$out_yearly_comments->November." ],
                    ['DEC',  ".$out_yearly->December." ,  ".$out_yearly_likes->December.",  ".$out_yearly_comments->December." ] "; 
                    

                    $weekly_sql  = "SELECT 
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Sunday' THEN 1 ELSE 0 END),0) AS 'Sunday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Monday' THEN 1 ELSE 0 END),0) AS 'Monday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Tuesday' THEN 1 ELSE 0 END),0) AS 'Tuesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Wednesday' THEN 1 ELSE 0 END),0) AS 'Wednesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Thursday' THEN 1 ELSE 0 END),0) AS 'Thursday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Friday' THEN 1 ELSE 0 END),0) AS 'Friday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Saturday' THEN 1 ELSE 0 END),0) AS 'Saturday'
                    FROM `video_view_meta` 
                    where WEEK(date(from_unixtime(floor(time/1000)))) = WEEK(CURDATE()) ";                
                    $out_weekly_view = $this->db->query($weekly_sql)->row();  

                    $weekly_sql  = "SELECT 
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Sunday' THEN 1 ELSE 0 END),0) AS 'Sunday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Monday' THEN 1 ELSE 0 END),0) AS 'Monday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Tuesday' THEN 1 ELSE 0 END),0) AS 'Tuesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Wednesday' THEN 1 ELSE 0 END),0) AS 'Wednesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Thursday' THEN 1 ELSE 0 END),0) AS 'Thursday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Friday' THEN 1 ELSE 0 END),0) AS 'Friday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Saturday' THEN 1 ELSE 0 END),0) AS 'Saturday'
                    FROM `video_like_meta` 
                    where WEEK(date(from_unixtime(floor(creation_time/1000)))) = WEEK(CURDATE()) ";                
                    $out_weekly_likes = $this->db->query($weekly_sql)->row();  

                    $weekly_sql  = "SELECT 
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Sunday' THEN 1 ELSE 0 END),0) AS 'Sunday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Monday' THEN 1 ELSE 0 END),0) AS 'Monday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Tuesday' THEN 1 ELSE 0 END),0) AS 'Tuesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Wednesday' THEN 1 ELSE 0 END),0) AS 'Wednesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Thursday' THEN 1 ELSE 0 END),0) AS 'Thursday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Friday' THEN 1 ELSE 0 END),0) AS 'Friday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Saturday' THEN 1 ELSE 0 END),0) AS 'Saturday'
                    FROM `video_master_comment` 
                    where WEEK(date(from_unixtime(floor(time/1000)))) = WEEK(CURDATE())" ;                
                    $out_weekly_comments = $this->db->query($weekly_sql)->row(); 
                    $weekly_view_chart ="['Sunday',  ".$out_weekly_view->Sunday." ,  ".$out_weekly_likes->Sunday." ,  ".$out_weekly_comments->Sunday." ],
                    ['Monday',  ".$out_weekly_view->Monday."  ,  ".$out_weekly_likes->Monday." ,  ".$out_weekly_comments->Monday."],
                    ['Tuesday',  ".$out_weekly_view->Tuesday." ,  ".$out_weekly_likes->Tuesday.",  ".$out_weekly_comments->Tuesday." ],
                    ['Wednesday',  ".$out_weekly_view->Wednesday." ,  ".$out_weekly_likes->Wednesday." ,  ".$out_weekly_comments->Wednesday."],
                    ['Thursday',  ".$out_weekly_view->Thursday." ,  ".$out_weekly_likes->Thursday." ,  ".$out_weekly_comments->Thursday."],
                    ['Friday',  ".$out_weekly_view->Friday."  ,  ".$out_weekly_likes->Friday.",  ".$out_weekly_comments->Friday." ],
                    ['Saturday',  ".$out_weekly_view->Saturday.",  ".$out_weekly_likes->Saturday." ,  ".$out_weekly_comments->Saturday."]";        
                    
    ?>

<div id="" class="col-md-12" >
    <section class="panel">
        <div class="panel-body">
            <!--custom chart start-->
            <div class="border-head">
                <h3>Reports</h3>
            </div>
            <div id="video_div_weekly_views" style="height:280px"></div>
            <div id="video_div_views" style="height:280px"></div>
        </section>
    </div>
</div>
<?php
$adminurl = AUTH_PANEL_URL;

$custum_js = <<<EOD
        <script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
              
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Month', 'Views' ,'Likes' , 'Comments' ],
                    $out_yearly_js                                         
                ]);

                var options = {
                    title: 'Monthly graph for Views,Likes,Comments',
                    hAxis: {title: 'Year $year_graph',  titleTextStyle: {color: '#333'}},
                    vAxis: {minValue: 0}
                };

                var chart = new google.visualization.AreaChart(document.getElementById('video_div_views'));
                chart.draw(data, options);

                var data = google.visualization.arrayToDataTable([
                    ['Day', 'Views' ,'Likes','Comments'],
                    $weekly_view_chart                                        
                ]);

                var options = {
                    title: 'Weekly graph for Views,Likes,Comments ',
                    hAxis: {title: 'Current Week',  titleTextStyle: {color: '#333'}},
                    vAxis: {minValue: 0}
                };

                var chart = new google.visualization.AreaChart(document.getElementById('video_div_weekly_views'));
                chart.draw(data, options);
            }
        </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>