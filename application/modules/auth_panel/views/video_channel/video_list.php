<?php
$sql = "SELECT count(*) as total  
FROM video_master ";
$total =  $this->db->query($sql)->row()->total;

$sql = "SELECT count(id) as total   
FROM video_master where for_dams =1 or for_non_dams = 1";
$published =  $this->db->query($sql)->row()->total;

$sql = "SELECT count(id) as total   
FROM video_master where for_dams =1";
$for_dams_video =  $this->db->query($sql)->row()->total;
$sql = "SELECT count(id) as total   
FROM video_master where  for_non_dams = 1";
$for_non_dams_video =  $this->db->query($sql)->row()->total;
?>
<div class="col-sm-12">
    <div class=" state-overview">
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol terques">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class="count">
                    <?php echo $total; ?>                
                    </h1>
                    <p>Total videos</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol red">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class=" count2">
                       <?php echo $published; ?>                
                    </h1>
                    <p>Published videos</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6 hide ">
            <section class="panel">
                <div class="symbol yellow">
                    <i class="fa  fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class=" count3"><?php echo $for_dams_video; ?>  </h1>
                    <p>For dams <br><sup class="">(published)</sup> </p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6 hide ">
            <section class="panel">
                <div class="symbol blue">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class=" count4"><?php echo $for_non_dams_video; ?>  </h1>
                    <p>For non dams <br><sup class="">(published)</sup></p>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		<?php echo strtoupper($page); ?>
		</header>
		<div class="panel-body">
		<div class="adv-table">
			<div class="col-md-6 pull-right">
				<div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
				 <div  class="input-group-addon">From</div>
				<input type="text" id="min-date-video-list" class="form-control date-range-filter input-sm course_start_date"  placeholder="">

				<div class="input-group-addon">to</div>

				<input type="text" id="max-date-video-list" class="form-control date-range-filter input-sm course_end_date"  placeholder="">

				</div>		
			</div>
		<table  class="display table table-bordered table-striped" id="all-video-grid">
  		<thead>
    		<tr>
          <th>#</th>
      		<th>Title </th>
      		<th>Type </th>
			<th>Category </th>
      		<th>Sub Category </th>
          <th>Author</th>
          <th>Thumbnail </th>
          <th>Views </th>
          <th>Creation time</th>
          <th>State</th>
          <th>Action</th>
    		</tr>
  		</thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
              <th>
                    <select data-column="2"  class="form-control search-input-select">
                        <option value="">All</option>
                        <option value="0">Normal</option>
                        <option value="1">Youtube</option>
                        <option value="2">Vimeo</option>
                        <option value="2">Vimeo Stream</option>
                    </select>
                </th>
			  <th><select data-column="3"  class="form-control search-input-select">  
                     <option value="">All</option>                 
                     <option value="Medical">Medical</option>
                     <option value="Dental">Dental</option>
                </select></th>
			  <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="5"  class="search-input-text form-control"></th>
              <th></th>
              <th></th>
              <th></th>    
              <th></th>      
              <th></th>
              
          </tr>
      </thead>
		</table>
		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-video-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"video_channel/Video_control/ajax_all_video_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );
						// Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date-video-list').val() !="" && $('#max-date-video-list').val() != "" ){
                                var dates = $('#min-date-video-list').val()+','+$('#max-date-video-list').val();
                                dataTable.columns(8).search(dates).draw();
                            } 
                            if($('#min-date-video-list').val() =="" || $('#max-date-video-list').val() == "" ){
                                var dates = "";
                                dataTable.columns(8).search(dates).draw();
                            }  
                        }); 
                   } );
				   
				   $('#min-date-video-list').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true
						
					});
					$('#max-date-video-list').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true
						
					});
               </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
