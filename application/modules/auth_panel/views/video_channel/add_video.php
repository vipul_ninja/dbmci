<div class="hello">
   <div class="col-lg-6">
      <section class="panel">
         <header class="panel-heading">
            Add Video 
         </header>
         <div class="panel-body">
            <form autocomplete="off" id="add_video_form" novalidate="novalidate" role="form" method= "POST" onsubmit="return ValidateRules()" enctype="multipart/form-data">
               <div class="form-group">
                  <label for="exampleInputPassword1">Video Type</label>
                  <?php
                  $options= array(
                    '0'=>'Normal',
                    '1'=>'Youtube',
                    '2'=>'Vimeo',
                    '3'=>'Vimeo streaming'
                  );
                  $extra=array(
                    'class'=>'form-control input-sm m-bot15',
                    'id'   =>'video_type'
                  );
                  echo form_dropdown('video_type',$options,set_value('video_type','0'),$extra);
                  ?>

                  <span class="text-danger"><?php echo form_error('video_type');?></span>
               </div>

               <div class="form-group video_input_meta" id="youtube_video_box" style="display: none;">
                 <label for="video_url_youtube">Youtube Video URL</label><input type="text" class="form-control" placeholder="Enter Youtube video URL" name = "video_url_youtube" id="video_url_youtube">
	                <span class="text-danger"><?php echo form_error('video_url_youtube');?></span>
               </div>

               <div class="form-group video_input_meta " id="vimeo_video_box" style="display: none;">
                 <label for="video_url_vimeo">Vimeo Video URL</label><input type="text" class="form-control" placeholder="Enter Vimeo video URL" name = "video_url_vimeo" id="video_url_vimeo">
               </div>

               <div class="form-group video_input_meta " id="vimeo_streaming_video_box" style="display: none;">
                 <label for="video_url_vimeo">Vimeo Streaming URL</label><input type="text" class="form-control" placeholder="Enter Vimeo video URL" name = "video_url_vimeo_streaming" id="video_url_vimeo_streaming">
               </div>

               <div class="form-group" id="add_video">
                  <label for="addvideo">Add Video</label>
                  <input type="file" accept="video/mp4" name = "video_file" id="addvideo">
                  <small> Video format supported -: mp4</small>
                  <span class="text-danger"><?php echo form_error('video_file');?></span>
               </div>

               <div class="form-group">
                  <label for="video_title">Title</label>
                  <input type="text" class="form-control" name = "video_title" id="video_title" placeholder="Enter Title">
                  <span class="text-danger"><?php echo form_error('video_title');?></span>
               </div>
               <div class="form-group">
                  <label for="author_name">Author Name</label>
                  <input type="text" class="form-control" name = "author_name" id="author_name" placeholder="Enter Author Name">
                  <span class="text-danger"><?php echo form_error('author_name');?></span>
               </div>
               <div class="form-group" id="thumbnail_url_box" style="display: none;">
                  <label for="thumbnail_url">Thumbnail URL</label>
                  <input type="text" class="form-control" name = "thumbnail_url" id="thumbnail_url" placeholder="Enter Thumbnail URL">
                  <span class="text-danger"><?php echo form_error('thumbnail_url');?></span>
               </div>
               <div class="form-group" id="add_thumbnail" >
                  <label for="thumbnail_file">Add Thumbnail</label>
                  <input type="file" accept="" name = "thumbnail_file" id="thumbnail_file">
                  <span class="text-danger"><?php echo form_error('thumbnail_file');?></span>
               </div>
               <div class="form-group">
                  <label for="video_desc">Description</label>
                  <textarea class="form-control" id="video_desc" name = "video_desc" rows="3" placeholder="Enetr Description"></textarea>
                  <span class="text-danger"><?php echo form_error('video_desc');?></span>
               </div>
               <div class="form-group hide">
                  <label for="maincategory">Main Category</label>
                  <select class="form-control input-sm m-bot15" name = "main_cat" id="maincategory">
                     <option value="1">Medical</option>
                     <option value="2">Dental</option>

                     <span class="text-danger"><?php echo form_error('main_cat');?></span>
                  </select>
               </div>
               <div class="form-group hide">
                  <label for="subcategory">Sub Category</label>
                  <select class="form-control input-sm m-bot15" name = "sub_cat" id="subcategory">
                  </select>
                  <span class="text-danger"><?php echo form_error('sub_cat');?></span>
               </div>
               <div class="form-group hide">
                  <label for="tgs">Tags</label>
                  <input type="text" class="form-control" name = "tags" id="tgs" placeholder="Enter Tags">
                  <span class="text-danger"><?php echo form_error('tags');?></span>
               </div>
               <div class="form-group hide ">
                  <label for="startdate">Start Date</label>
                  <input type="text" class="form-control dpd1" name = "start_date" id="startdate" placeholder="Enter Start Date">
                  <span class="text-danger"><?php echo form_error('start_date');?></span>
               </div>
               <div class="form-group hide ">
                  <label for="enddate">End Date</label>
                  <input type="text" class="form-control dpd2" name = "end_date" id="enddate" placeholder="Enter End Date">
                  <span class="text-danger"><?php echo form_error('end_date');?></span>
               </div>
               <div class="form-group hide ">
                  <label for="initialview">Initial View</label>
                  <input type="text" class="form-control" name = "initial_view" id="initialview" placeholder="Enter Initial View">
                  <span class="text-danger"><?php echo form_error('initial_view');?></span>
               </div>
               <div class="form-group hide">
                  <label for="creentags"> Tags (Screen Tag)</label>
                  <select name="screen_tag[]" class="form-control"  multiple>
                        <?php
                        $all_tags = $this->db->get('video_search_tag_list')->result();
                        foreach($all_tags as $at){
                            echo '<option value="'.$at->id.'">'.$at->tag_name.'</option>';
                        }
                        ?>
                  </select>
                  <span class="text-danger"><?php echo form_error('screen_tag');?></span>
               </div>
               <div class="radio hide">
                  <label class="col-sm-4 control-label col-lg-3" >Featured</label>
                  <label>
                  <input type="radio" name="featured" id="featured" value="1" checked>
                  Yes
                  </label>
                  <label>
                  <input type="radio" name="featured" id="featured" value="0">
                  No
                  </label>
               </div>
               <div class="radio">
                  <label class="col-sm-4 control-label col-lg-3" >Allow Comments</label>
                  <label>
                  <input type="radio" name="allow_comments" value="1" checked>
                  Yes
                  </label>
                  <label>
                  <input type="radio" name="allow_comments"  value="0">
                  No
                  </label>
               </div>
                <div class="radio hide">
                  <label class="col-sm-4 control-label col-lg-3" >Is New</label>
                  <label>
                  <input type="radio" name="is_new" value="1" >
                  Yes
                  </label>
                  <label>
                  <input type="radio" name="is_new"  value="0" checked>
                  No
                  </label>
               </div>
               <div class="checkbox hide ">
                  <label>
                      <input type="checkbox" value="1" id="for_dams" name="for_dams"> For Dams
                  </label>
               </div>
               <div class="checkbox hide ">
                  <label>
                      <input type="checkbox" value="1" id="for_non_dams" name="for_non_dams"> For Non Dams
                  </label>
               </div>
               <button type="submit" class="btn btn-info submit_video_form">Submit</button>
            </form>
         </div>
      </section>
   </div>
   <div class="clearfix"></div>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$validation_js = AUTH_ASSETS."js/jquery.validate.min.js";
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
			   <script src="$validation_js" type="text/javascript"></script>
               <script type="text/javascript" language="javascript" >

						$( function() {
					$( ".dpd1" ).datetimepicker();
					$( ".dpd2" ).datetimepicker();
					 }); // datepicker closed
               </script>
              <script type="text/javascript" charset="utf8">
               $('#video_type').change(function(){
                   $('.video_input_meta').hide();
                  if($(this).val() == '1'){
                      $('#youtube_video_box').show();
                      $('#add_video').hide();
                      $('#vimeo_video_box').hide();
                      $('#thumbnail_url_box').show();
                      $('#add_thumbnail').hide();
                  }
                  if($(this).val() == '2'){
                      $('#youtube_video_box').hide();
                      $('#vimeo_video_box').show();
                      $('#add_video').hide();
                      $('#add_thumbnail').hide();
                      $('#thumbnail_url_box').show();
                  }
                  if($(this).val() == '0'){
                      $('#add_video').show();
                      $('#vimeo_video_box').hide();
                      $('#youtube_video_box').hide();
                      $('#thumbnail_url_box').hide();
                      $('#add_thumbnail').show();
                  }

                  if($(this).val() == '3'){
                      $('.video_input_meta').hide();
                      $('#add_video').hide();
                      $('#vimeo_streaming_video_box').show();
                      $('#add_thumbnail').hide();
                      $('#add_thumbnail').show();
                  }
                  /*else{
                      $('#add_video').remove();
                  }*/

              }).change();
              </script>
              <script type="text/javascript" charset="utf8">
                $(document).ready(function(){
                    $("#video_url_youtube").on("keyup", function(){
                      var youtube_url=$('#video_url_youtube').val();
                      $.ajax({
                        url: '$adminurl'+'video_channel/Video_control/get_youtube_details',
                        method: 'POST',
                        dataType: 'json',
                        //crossDomain:true,
                        data: {
                               youtube_url: youtube_url
                        },
                        success: function (data) {
                            console.log(data);
                            //alert(data.author_name);
                            $('#author_name').val(data.author_name);
                            $('#video_title').val(data.title);
                            $('#thumbnail_url').val(data.thumbnail_url);
                            $('#video_desc').val(data.title);
                        }
                       });
                    });
                });
              </script>
              <script type="text/javascript" charset="utf8">
                $(document).ready(function(){
                    $("#video_url_vimeo").on("keyup", function(){
                      var vimeo_url=$('#video_url_vimeo').val();
                      $.ajax({
                        url: '$adminurl'+'video_channel/Video_control/get_vimeo_details',
                        method: 'POST',
                        dataType: 'json',
                        //crossDomain:true,
                        data: {
                               vimeo_url: vimeo_url
                        },
                        success: function (data) {
                            console.log(data);
                            //alert(data.title);
                            $('#author_name').val(data.author_name);
                            $('#video_title').val(data.title);
                            $('#thumbnail_url').val(data.thumbnail);
                            $('#video_desc').val(data.description);
                        }
                       });
                    });
                });

				$( "#maincategory" ).change(function() {
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"video_channel/video_control/get_video_subcategory/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.text+"</option>";
                          });
                           $("#subcategory").html(html);
                        }
                      });
                    }).change();



						function ValidateRules (){
							var video_type_value = $("#video_type").val();


							if(video_type_value == 0){
								  var video_file = $("#addvideo").val();
								  var video_title = $("#video_title").val();
								  var thumbnail_file = $("#thumbnail_file").val();
								  var video_desc = $("#video_desc").val();
								  var sub_cat = $("#subcategory").val();

								 if(video_file == "" ){

									 show_toast("error", "Video file should not be empty", "No Video File");
									 return false;
								 }
								 if(video_title == ""){

									show_toast("error", "Video title should not be empty", "No Video Title File");
									return false;
								 }
								 if(thumbnail_file == ""){

									show_toast("error", "Thumbnail file should not be empty", "No File Thumbnail File");
									return false;
								 }
								 if(video_desc == ""){

									show_toast("error", "Video desc should not be empty", "No Video Description File");
									return false;
								 }
								 if(sub_cat == ""){

									show_toast("error", "Sub category should not be empty", "No Sub Category File");
									return false;
								 }


							}


						}

    </script>
EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
