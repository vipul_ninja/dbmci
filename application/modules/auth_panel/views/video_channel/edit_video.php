<style>
.lbl_block{
    display:block !important;
}
.fix-height{
    max-height:195px;
    overflow-x:hidden;
    overflow-y:scroll;
}
</style>
<div class="col-md-3">
<section class="panel">
  <header class="panel-heading">
      Video Channel
  </header>
  <?php
  	$video_id = $this->input->get('id');
    $c_id         = $this->input->get('course_id');
    $main_stream_result = $this->db->get('`course_stream_name_master`')->result_array();
    $get_save_type      = $this->db->get('`course_type_master`')->result_array();
    $type_ids      = $this->db->select('GROUP_CONCAT(sub_cate_id) as type_ids')->where('video_id',	$video_id)->get('video_category_relationship')->row()->type_ids;
    $study_type = $this->db->where('status','0');
    
    $study_type = $this->db->get('`course_subject_master`')->result_array();
?>
  <div class="panel-body">
      <ul class="nav prod-cat">
        <li><a href="javascript:void(0)" data-div="1" ><i class=" fa fa-angle-right"></i> Edit</a></li>
        <li><a href="javascript:void(0)" data-div="2" ><i class=" fa fa-angle-right"></i> Comments list</a></li>
        <li><a href="javascript:void(0)" data-div="3" ><i class=" fa fa-angle-right"></i> Reports</a></li>
        <li><a href="javascript:void(0)" data-div="4" ><i class=" fa fa-angle-right"></i> Filtration</a></li>
      </ul>
  </div>

    <div class="panel-body">
        <span class="badge bg-important"><?php echo $video_detail['views'];?> Views </span>
        <span class="badge bg-success"><?php echo $video_detail['likes'];?> Likes </span>
        <span class="badge bg-info"><?php echo $video_detail['comments'];?> comments </span>
    </div>
    <div class="panel-body">
    <a href="<?php echo AUTH_PANEL_URL . 'video_channel/video_control/add_video'; ?>" ><button class="btn btn-success col-md-12 btn-sm bold "> Add new video </button></a>
    <a href="<?php echo AUTH_PANEL_URL . 'video_channel/video_control/video_list'; ?>" ><button class="btn btn-danger col-md-12 btn-sm bold "> Back to all videos list </button></a>
    <?php if( $video_detail['video_type'] == 0 ){ ?>
	     <a href="<?php echo AUTH_PANEL_URL.'video_channel/video_control/download_video_from_channel?file='.urlencode($video_detail['URL']);?>" target="_blank" ><button class="btn btn-warning col-md-12 btn-sm bold ">Download</button></a>
    <?php } ?>

	</div>
</section>
<?php
if($video_detail['for_dams'] == 1 || $video_detail['for_non_dams'] == 1  ){
    ?>
    <section class="panel">
    <header class="panel-heading">
        Send Push Notification
    </header>
        <div class="panel-body">
        <button onclick="$(this).addClass('disabled');fire_push();" class="btn btn-info col-md-12">Click to Send</button>
        </div>
        <div id="show_socket_state" class="alert bold" ></div>
    </section>
    <?php
}

?>

</div>
<div class="col-md-9">
   <div id="tabContent1" class="col-lg-12 tabu">
      <section class="panel">
         <header class="panel-heading">
            Edit Video
            <?php
                if($video_detail['for_dams'] == 1 and $video_detail['for_non_dams'] == 1  ){

                    echo '<span  data-original-title="Note" data-content="This video is published for all users you can change it from edit menu available in video channel menu." data-placement="bottom" data-trigger="hover" title="" aria-hidden="true" class="badge bg-info pull-right popovers">Published for all</span>';
                }elseif($video_detail['for_dams'] == 1){
                    echo '<span data-original-title="Note" data-content="This video is published for Dams users only, you can change it from edit menu available in video channel menu." data-placement="bottom" data-trigger="hover" title="" aria-hidden="true" class="badge bg-info pull-right popovers">Published for dams user only </span>';
                }elseif($video_detail['for_non_dams'] == 1){
                    echo '<span data-original-title="Note" data-content="This video is published for non Dams users only, you can change it from edit menu available in video channel menu." data-placement="bottom" data-trigger="hover" title="" aria-hidden="true" class="badge bg-info pull-right popovers">Published for non dams user only </span>';
                }else{
                    echo '<span data-original-title="Note" data-content="This video is not published for users you can change it from edit menu available in video channel menu." data-placement="bottom" data-trigger="hover" title="" aria-hidden="true" class="badge bg-info pull-right popovers">Unpublished</span>';
                }
            ?>
         </header>
         <div class="panel-body">
            <form autocomplete="off" role="form" method= "POST" enctype="multipart/form-data">
              <?php if($video_detail['id']) { ?>
              <input type="hidden" id="video_id" value="<?php echo $video_detail['id'];?>">
              <?php } ?>
              <!-- category data start -->
            

              <div class="panel-body">
          <form method="POST">
          <label for="title">Category </label>
            <input type="hidden" name="course_id" value="<?php //echo $c_id;?>">
            <div class="col-md-12">
                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                <?php $p=1;$holder=array();
                             //   print_r($main_stream_result);
                                  foreach($main_stream_result as $res){
                                  if($res['parent_id']==0){
                                    $holder[]=$res['id'];
                                    ?>
                                <li class="<?php echo ($p==1)?'active':''?>"><a class="cate_parent_head parent_holder<?php echo $res['id'];?>" href="#tabprimary<?php echo $res['id'];?>" data-toggle="tab"><?php echo($res['name'].'&nbsp');?><label class="badge"></label></a></li>
                                <?php } $p++;}?>
                                <div class="pull-right margin-right  "><span>Select All &nbsp</span><input id="select-all" type="checkbox" value="check all"></div>
                            </ul>
                    </div>

                    <div class="panel-body">
                        <div class="tab-content">
                          <?php
                            $all_course_cate_ids =$this->db->select('GROUP_CONCAT(sub_cate_id) as type_ids')->where('video_id',	$video_id)->get('video_category_relationship')->row()->type_ids;
                          if($all_course_cate_ids !=""){
                            $all_course_cate_ids = explode(',',$all_course_cate_ids);
                          }else{
                             $all_course_cate_ids=array();
                          }

                          $hold=1;
                          for($i=0;$i<count($holder);$i++){?>
                            <div class="tab-pane fade <?php echo ($hold==1)?' in active':''?>" id="tabprimary<?php echo $holder[$i];?>">
                              <?php
                                foreach($main_stream_result as $res){
                                if($holder[$i]==$res['parent_id']){
                                echo('<div class="col-md-2"><input '.(in_array($res['id'], $all_course_cate_ids)?'checked':'').' data-holder_id="'.$res['parent_id'].'" id="course_cat_id" type="checkbox" name="course_cat_id[]" value="'.$res['id'].'" class="cat_check">'.$res['name'].'</div>');
                                }
                              }
                              ?>
                            </div>
                          <?php $hold++;} ?>
                        </div>
                    </div>
                </div>
            </div>
         
          <!--category data end  -->
             <div class="form-group">
                  <label for="exampleInputPassword1">Video Type</label>
                  <?php
                  $options= array(
                    '0'=>'Normal',
                    '1'=>'Youtube',
                    '2'=>'Vimeo',
                    '3'=>'Vimeo streaming'
                  );
                  $extra=array(
                    'class'=>'form-control input-sm m-bot15',
                    'id'   =>'video_type'
                  );
                  echo form_dropdown('video_type',$options,set_value('video_type',$video_detail['video_type']),$extra);
                  ?>

                  <span class="text-danger"><?php echo form_error('video_type');?></span>
               </div>
               <div class="form-group video_input_meta " id="youtube_video_box" style="display: none;">
                 <label for="video_url_youtube">Youtube Video URL</label><input type="text" class="form-control" placeholder="Enter Youtube video URL" name = "video_url_youtube" id="video_url_youtube" value="<?php if($video_detail['video_type']=='1'){ echo $video_detail['URL']; }?>">
               </div>
               <div class="form-group video_input_meta " id="vimeo_video_box" style="display: none;">
                 <label for="video_url_vimeo">Vimeo Video URL</label><input type="text" class="form-control" placeholder="Enter Vimeo video URL" name = "video_url_vimeo" id="video_url_vimeo" value="<?php if($video_detail['video_type']=='2'){ echo $video_detail['URL']; }?>">
               </div>

                <div class="form-group video_input_meta " id="vimeo_streaming_video_box" style="display: none;">
                    <label for="video_url_vimeo">Vimeo Streaming URL</label><input type="text" class="form-control" placeholder="Enter Vimeo video URL" name = "video_url_vimeo_streaming" id="video_url_vimeo_streaming" value="<?php if($video_detail['video_type']=='3'){ echo $video_detail['URL']; }?>">
                 </div>
               <div class="form-group" id="add_video">
                  <label for="addvideo">Add Video</label>
                  <input type="file" accept="video/mp4" name = "video_file" id="addvideo">
                  <small> Video format supported -: mp4</small>
                  <span class="text-danger"><?php echo form_error('video_file');?></span>
               </div>
               <div class="form-group">
                  <label for="video_title">Title</label>
                  <input type="text" class="form-control" name = "video_title" id="video_title" placeholder="Enter Title" value="<?php if($video_detail['video_title']){ echo $video_detail['video_title']; }?>">
                  <span class="text-danger"><?php echo form_error('video_title');?></span>
               </div>
               <div class="form-group">
                  <label for="author_name">Author Name</label>
                  <input type="text" class="form-control" name = "author_name" id="author_name" placeholder="Enter Author Name" value="<?php if($video_detail['author_name']){ echo $video_detail['author_name']; }?>">
                  <span class="text-danger"><?php echo form_error('author_name');?></span>
               </div>
               <div class="form-group ">
                  <label for="thumbnail_url" id="thumbnail_url_box">Thumbnail URL</label>
                  <input type="text" class="form-control" name = "thumbnail_url" id="thumbnail_url" placeholder="Enter Thumbnail URL" value="<?php if($video_detail['thumbnail_url']){ echo $video_detail['thumbnail_url']; }?>">
                  <span class="text-danger"><?php echo form_error('thumbnail_url');?></span>
               </div>
                <div class="form-group" id="add_thumbnail" >
                  <label for="thumbnail_file">Add Thumbnail</label>
                  <input type="file" accept="" name = "thumbnail_file" id="thumbnail_file">
                  <span class="text-danger"><?php echo form_error('thumbnail_file');?></span>
               </div>
               <div class="form-group">
                  <label for="video_desc">Description</label>
                  <textarea class="form-control" id="video_desc" name = "video_desc" rows="3" placeholder="Enter Description"><?php if($video_detail['video_desc']){ echo $video_detail['video_desc']; }?></textarea>
                  <span class="text-danger"><?php echo form_error('video_desc');?></span>
               </div>

               <div id="tabContent12" class="col-md-12 tabu ">
     

      
               <!-- <div class="form-group">
                  <label for="maincategory">Main Category</label>
                  <select class="form-control input-sm m-bot15" name = "main_cat" id="maincategory">
                     <option value="1" <?php if($video_detail['main_cat']=='1'){ echo 'selected';}?>>Medical</option>
                     <option value="2" <?php if($video_detail['main_cat']=='2'){ echo 'selected';}?>>Dental</option>
                  </select>
				   <span class="text-danger"><?php echo form_error('main_cat');?></span>
               </div>
               <div class="form-group">
                  <label for="subcategory">Sub Category</label>
                  <select class="form-control input-sm m-bot15" name = "sub_cat" id="subcategory">
                  </select>
				    <span class="text-danger"><?php echo form_error('sub_cat');?></span>
               </div> -->
               <div class="form-group hide">
                  <label for="tgs">Tags</label>
                  <input type="text" class="form-control" name = "tags" id="tgs" placeholder="Enter Tags" value="<?php if($video_detail['tags']){ echo $video_detail['tags']; }?>">
                  <span class="text-danger"><?php echo form_error('tags');?></span>
               </div>
               <div class="form-group hide ">
                  <label for="startdate">Start Date</label>
                  <input type="text" class="form-control dpd1" name = "start_date" id="startdate" placeholder="Enter Start Date" value="<?php if($video_detail['start_date']){ echo $video_detail['start_date']; }?>">
                  <span class="text-danger"><?php echo form_error('start_date');?></span>
               </div>
               <div class="form-group hide ">
                  <label for="enddate">End Date</label>
                  <input type="text" class="form-control dpd2" name = "end_date" id="enddate" placeholder="Enter End Date" value="<?php if($video_detail['end_date']){ echo $video_detail['end_date']; }?>">
                  <span class="text-danger"><?php echo form_error('end_date');?></span>
               </div>
               <div class="form-group hide ">
                  <label for="initialview">Initial View</label>
                  <input type="text" class="form-control" name = "initial_view" id="initialview" placeholder="Enter Initial View" value="<?php if($video_detail['initial_view']){ echo $video_detail['initial_view']; }?>">
                  <span class="text-danger"><?php echo form_error('initial_view');?></span>
               </div>
               <div class="form-group hide">
                  <label for="creentags"> Tags (Screen Tag)</label>
                  <select name="screen_tag[]" class="form-control"  multiple>
                        <?php
                        $all_tags = $this->db->get('video_search_tag_list')->result();
                        $selected = explode(',',$video_detail['screen_tag']);


                        foreach($all_tags as $at){
                            $set = "";
                            if(in_array($at->id,$selected)){
                                $set = "selected=selected";
                            }
                            echo '<option '.$set.' value="'.$at->id.'">'.$at->tag_name.'</option>';
                        }
                        ?>
                  </select>
                  <span class="text-danger"><?php echo form_error('screen_tag');?></span>
               </div>
               <div class="radio">
                  <label class="col-sm-4 control-label col-lg-3" >Featured</label>
                  <label>
                  <input type="radio" name="featured" id="featured" value="1" <?php if($video_detail['featured']=='1'){ echo 'checked';}?>>
                  Yes
                  </label>
                  <label>
                  <input type="radio" name="featured" id="featured" value="0" <?php if($video_detail['featured']=='0'){ echo 'checked';}?>>
                  No
                  </label>
               </div>
               <div class="radio">
                  <label class="col-sm-4 control-label col-lg-3" >Allow Comments</label>
                  <label>
                  <input type="radio" name="allow_comments" value="1" <?php if($video_detail['allow_comments']=='1'){ echo 'checked';}?>>
                  Yes
                  </label>
                  <label>
                  <input type="radio" name="allow_comments"  value="0" <?php if($video_detail['allow_comments']=='0'){ echo 'checked';}?>>
                  No
                  </label>
               </div>
               <div class="radio">
                  <label class="col-sm-4 control-label col-lg-3" >Is New</label>
                  <label>
                  <input type="radio" name="is_new" value="1" <?php if($video_detail['is_new']=='1'){ echo 'checked';}?>>
                  Yes
                  </label>
                  <label>
                  <input type="radio" name="is_new"  value="0" <?php if($video_detail['is_new']=='0'){ echo 'checked';}?>>
                  No
                  </label>
               </div>
                <div class="checkbox hide ">
                  <label>
                      <input type="checkbox" value="1" id="for_dams" name="for_dams" <?php if($video_detail['for_dams']=='1'){ echo 'checked';}?>><strong>Publish For Dams
                  </strong></label>
               </div>
               <div class="checkbox hide">
                  <label>
                      <input type="checkbox" value="1" id="for_non_dams" name="for_non_dams" <?php if($video_detail['for_non_dams']=='1'){ echo 'checked';}?>><strong>Publish For Non Dams</strong>
                  </label>
               </div>
               <label for="title">Study Type </label>
            <input type="hidden" name="course_id" value="<?php //echo $c_id;?>">
            <div class="col-md-12">
                <div class=" ">
             

                    <div class="panel-body">
                        <div class="tab-content">
                          <?php
                            $all_video_study_ids =$this->db->select('GROUP_CONCAT(study_id) as type_ids')->where('video_id',$video_id)->get('video_study_relationship')->row()->type_ids;
                            if($all_video_study_ids !=""){
                            $all_video_study_ids = explode(',',$all_video_study_ids);
                          }else{
                             $all_video_study_ids=array();
                          }
                          ?>
                            <div class="tab-pane fade  in active" id="tabprimary">
                            <ul class="nav nav-tabs">
                              <?php
                                foreach($study_type as $res){
                                    echo('<li><input '.(in_array($res['id'], $all_video_study_ids)?'checked':'').' data-holder_id="" type="checkbox" name="stdy_type[]" value="'.$res['id'].'" class="">'.  ucfirst($res['name'].'').'</li>');
                                }
                              ?>
                              </ul>
                            </div>
                        </div>
                    </div>
                </div>
               <button type="submit" class="btn btn-info">Update</button>
            </form>
         </div>
      </section>
   </div>

 <div id="tabContent2" class="col-md-12 tabu ">
        <section class="panel">
            <header class="panel-heading">
                Comments
            </header>
            <div class="panel-body ">
                <div class="adv-table">
                    <div class="col-md-6 pull-right">
                        <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
                        <div  class="input-group-addon">From</div>
                        <input type="text" id="comment_start_date" class="form-control date-range-filter input-sm comment_start_date"  placeholder="">

                        <div class="input-group-addon">to</div>

                        <input type="text" id="comment_end_date" class="form-control date-range-filter input-sm comment_end_date"  placeholder="">

                        </div>
                    </div>
                    <table  class="display table table-bordered table-striped col-md-12" id="all-video-comments-video-grid" style="width:100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>User Name </th>
                                <th>Comment</th>
                                <th>Time </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                                <th></th>
                                <th></th>

                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>

<?php
    $year_graph =  date('Y');
    $yearly_sql = "SELECT
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '01' THEN 1 ELSE 0 END),0) AS 'January',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '02' THEN 1 ELSE 0 END),0) AS 'February',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '03' THEN 1 ELSE 0 END),0) AS 'March',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '04' THEN 1 ELSE 0 END),0) AS 'April',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '05' THEN 1 ELSE 0 END),0) AS 'May',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '06' THEN 1 ELSE 0 END),0) AS 'June',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '07' THEN 1 ELSE 0 END),0) AS 'July',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '08' THEN 1 ELSE 0 END),0) AS 'August',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '09' THEN 1 ELSE 0 END),0) AS 'September',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '10' THEN 1 ELSE 0 END),0) AS 'October',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '11' THEN 1 ELSE 0 END),0) AS 'November',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '12' THEN 1 ELSE 0 END),0) AS 'December',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%Y') WHEN  $year_graph THEN 1 ELSE 0 END),0) AS 'TOTAL'
                    FROM video_view_meta
                    Where video_id = ".$video_detail['id'];
                    $out_yearly = $this->db->query($yearly_sql)->row();


                    $yearly_sql = "SELECT
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '01' THEN 1 ELSE 0 END),0) AS 'January',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '02' THEN 1 ELSE 0 END),0) AS 'February',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '03' THEN 1 ELSE 0 END),0) AS 'March',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '04' THEN 1 ELSE 0 END),0) AS 'April',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '05' THEN 1 ELSE 0 END),0) AS 'May',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '06' THEN 1 ELSE 0 END),0) AS 'June',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '07' THEN 1 ELSE 0 END),0) AS 'July',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '08' THEN 1 ELSE 0 END),0) AS 'August',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '09' THEN 1 ELSE 0 END),0) AS 'September',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '10' THEN 1 ELSE 0 END),0) AS 'October',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '11' THEN 1 ELSE 0 END),0) AS 'November',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '12' THEN 1 ELSE 0 END),0) AS 'December',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%Y') WHEN  $year_graph THEN 1 ELSE 0 END),0) AS 'TOTAL'
                    FROM video_like_meta
                    Where video_id = ".$video_detail['id'];
                    $out_yearly_likes = $this->db->query($yearly_sql)->row();

                    $yearly_sql = "SELECT
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '01' THEN 1 ELSE 0 END),0) AS 'January',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '02' THEN 1 ELSE 0 END),0) AS 'February',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '03' THEN 1 ELSE 0 END),0) AS 'March',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '04' THEN 1 ELSE 0 END),0) AS 'April',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '05' THEN 1 ELSE 0 END),0) AS 'May',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '06' THEN 1 ELSE 0 END),0) AS 'June',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '07' THEN 1 ELSE 0 END),0) AS 'July',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '08' THEN 1 ELSE 0 END),0) AS 'August',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '09' THEN 1 ELSE 0 END),0) AS 'September',
                    IFNULL( SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '10' THEN 1 ELSE 0 END),0) AS 'October',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '11' THEN 1 ELSE 0 END),0) AS 'November',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%m') WHEN '12' THEN 1 ELSE 0 END),0) AS 'December',
                    IFNULL(SUM(CASE DATE_FORMAT(FROM_UNIXTIME(time/1000), '%Y') WHEN  $year_graph THEN 1 ELSE 0 END),0) AS 'TOTAL'
                    FROM video_master_comment
                    Where video_id = ".$video_detail['id'];
                    $out_yearly_comments = $this->db->query($yearly_sql)->row();

                    $out_yearly_js ="['JAN',  ".$out_yearly->January." ,  ".$out_yearly_likes->January." ,  ".$out_yearly_comments->January."  ],
                    ['FEB',  ".$out_yearly->February." ,  ".$out_yearly_likes->February." ,  ".$out_yearly_comments->February." ],
                    ['MAR',  ".$out_yearly->March." ,  ".$out_yearly_likes->March."  ,  ".$out_yearly_comments->March." ],
                    ['APR',  ".$out_yearly->April.",  ".$out_yearly_likes->April." ,  ".$out_yearly_comments->April." ],
                    ['MAY',  ".$out_yearly->May." ,  ".$out_yearly_likes->May.",  ".$out_yearly_comments->May."  ],
                    ['JUN',  ".$out_yearly->June." ,  ".$out_yearly_likes->June." ,  ".$out_yearly_comments->June." ],
                    ['JUL',  ".$out_yearly->July." ,  ".$out_yearly_likes->July."  ,  ".$out_yearly_comments->July." ],
                    ['AUG',  ".$out_yearly->August." ,  ".$out_yearly_likes->August.",  ".$out_yearly_comments->August." ],
                    ['SEP',  ".$out_yearly->September." ,  ".$out_yearly_likes->September." ,  ".$out_yearly_comments->September." ],
                    ['OCT',  ".$out_yearly->October." ,  ".$out_yearly_likes->October." ,  ".$out_yearly_comments->October." ],
                    ['NOV',  ".$out_yearly->November." ,  ".$out_yearly_likes->November."  ,  ".$out_yearly_comments->November." ],
                    ['DEC',  ".$out_yearly->December." ,  ".$out_yearly_likes->December.",  ".$out_yearly_comments->December." ] ";


                    $weekly_sql  = "SELECT
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Sunday' THEN 1 ELSE 0 END),0) AS 'Sunday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Monday' THEN 1 ELSE 0 END),0) AS 'Monday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Tuesday' THEN 1 ELSE 0 END),0) AS 'Tuesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Wednesday' THEN 1 ELSE 0 END),0) AS 'Wednesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Thursday' THEN 1 ELSE 0 END),0) AS 'Thursday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Friday' THEN 1 ELSE 0 END),0) AS 'Friday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Saturday' THEN 1 ELSE 0 END),0) AS 'Saturday'
                    FROM `video_view_meta`
                    where WEEK(date(from_unixtime(floor(time/1000)))) = WEEK(CURDATE()) and video_id = ".$video_detail['id'];
                    $out_weekly_view = $this->db->query($weekly_sql)->row();

                    $weekly_sql  = "SELECT
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Sunday' THEN 1 ELSE 0 END),0) AS 'Sunday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Monday' THEN 1 ELSE 0 END),0) AS 'Monday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Tuesday' THEN 1 ELSE 0 END),0) AS 'Tuesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Wednesday' THEN 1 ELSE 0 END),0) AS 'Wednesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Thursday' THEN 1 ELSE 0 END),0) AS 'Thursday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Friday' THEN 1 ELSE 0 END),0) AS 'Friday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(creation_time/1000))) WHEN 'Saturday' THEN 1 ELSE 0 END),0) AS 'Saturday'
                    FROM `video_like_meta`
                    where WEEK(date(from_unixtime(floor(creation_time/1000)))) = WEEK(CURDATE()) and video_id = ".$video_detail['id'];
                    $out_weekly_likes = $this->db->query($weekly_sql)->row();

                    $weekly_sql  = "SELECT
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Sunday' THEN 1 ELSE 0 END),0) AS 'Sunday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Monday' THEN 1 ELSE 0 END),0) AS 'Monday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Tuesday' THEN 1 ELSE 0 END),0) AS 'Tuesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Wednesday' THEN 1 ELSE 0 END),0) AS 'Wednesday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Thursday' THEN 1 ELSE 0 END),0) AS 'Thursday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Friday' THEN 1 ELSE 0 END),0) AS 'Friday',
                    IFNULL(SUM(CASE dayname(from_unixtime(floor(time/1000))) WHEN 'Saturday' THEN 1 ELSE 0 END),0) AS 'Saturday'
                    FROM `video_master_comment`
                    where WEEK(date(from_unixtime(floor(time/1000)))) = WEEK(CURDATE()) and video_id = ".$video_detail['id'];
                    $out_weekly_comments = $this->db->query($weekly_sql)->row();
                    $weekly_view_chart ="['Sunday',  ".$out_weekly_view->Sunday." ,  ".$out_weekly_likes->Sunday." ,  ".$out_weekly_comments->Sunday." ],
                    ['Monday',  ".$out_weekly_view->Monday."  ,  ".$out_weekly_likes->Monday." ,  ".$out_weekly_comments->Monday."],
                    ['Tuesday',  ".$out_weekly_view->Tuesday." ,  ".$out_weekly_likes->Tuesday.",  ".$out_weekly_comments->Tuesday." ],
                    ['Wednesday',  ".$out_weekly_view->Wednesday." ,  ".$out_weekly_likes->Wednesday." ,  ".$out_weekly_comments->Wednesday."],
                    ['Thursday',  ".$out_weekly_view->Thursday." ,  ".$out_weekly_likes->Thursday." ,  ".$out_weekly_comments->Thursday."],
                    ['Friday',  ".$out_weekly_view->Friday."  ,  ".$out_weekly_likes->Friday.",  ".$out_weekly_comments->Friday." ],
                    ['Saturday',  ".$out_weekly_view->Saturday.",  ".$out_weekly_likes->Saturday." ,  ".$out_weekly_comments->Saturday."]";

    ?>
    <div id="tabContent3" class="col-md-12 tabu " >
    <section class="panel">
        <div class="panel-body">
            <!--custom chart start-->
            <div class="border-head">
                <h3>Reports</h3>
            </div>
            <div id="video_div_weekly_views" style="height:280px"></div>
            <div id="video_div_views" style="height:280px"></div>
        </section>
    </div>
    <!-- Filtration -->
    <div id="tabContent4" class="col-md-12 tabu ">
        <section class="panel">
            <header class="panel-heading">
                Filtration
                 <button onclick="$('.wrapar').show('slow');" class="btn-success btn-xs btn pull-right"><i class="fa fa-plus"></i> Add Filter</button>
            </header>

            <div class="panel-body ">
            <div class="wrapar" style="display:none;">
                     <label for="exampleInputEmail1">Select Session</label>
                    <div class="year_segment"></div>
                    <div class="f_element">
                     <label for="exampleInputEmail1">Select Franchise </label>
						<div class="fr_segment">
						</div>
                    </div>

					 <div class="c_element">
                     <label for="exampleInputEmail1">Select Course Group </label>
						<div class="c_segment">
						</div>
                    </div>

					 <div class="cc_element">
                     <label for="exampleInputEmail1">Select Course</label>
						<div class="cc_segment">
						</div>
                    </div>

					<div class="course_element">
                     <label for="exampleInputEmail1">Select Batch</label>
						<div class="course_segment">
						</div>
                    </div>

					<button class="btn btn-success  btn-sm save_filtration">Save</button>
          <button class="btn btn-danger btn-sm" onclick="$('.wrapar').hide('slow');" type="button" >Cancel</button>

                </div>

                <div>
                    <table  class="display table table-bordered table-striped col-md-12" id="allfiltration-video-grid" style="width:100%">
                        <thead>
                            <tr>
                                <th>Session</th>
                                <th>FranchiseName</th>
                                <th>CourseGroup </th>
                                <th>Course</th>
                                <th>Batch </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
                                <th></th>

                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

<div role="dialog"  id="myModal2" class="modal fade" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title file-modal-element-head">Edit Video Comment</h4>
            </div>
            <div class="modal-body " >
             <form autocomplete="off" role="form" method= "POST" action="<?php echo AUTH_PANEL_URL."video_channel/video_control/update_video_comment";?>">
				 <div id = "comment_details"  >
					 <input type="hidden" readonly class="form-control" name = "video_id" id="comment_video_id" value="">
					 <input type="hidden" readonly class="form-control" name = "comment_id" id="comment_id" value="">
					<div class="form-group">
					  <label for="author_name">User Name</label>
					  <input type="text" readonly class="form-control" name = "user_name" id="user_name" value="">
				   </div>
					 <div class="form-group">
					  <label for="author_name">Comment</label>
					  <input type="text" class="form-control" name = "comment" id="user_comment" placeholder="Enter Comment" value="">
					  <span class="text-danger"><?php echo form_error('comment');?></span>
				   </div>
				 </div>
				  <button type="button" onclick="update_comment()" class="btn btn-info">Update</button>
			</form>
            </div>
        </div>
    </div>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$main_cat_id =  $video_detail['main_cat'];
$sub_cat_id  =  $video_detail['sub_cat'];
$video_id    =  $video_detail['id'];
$video_title    =  $video_detail['video_title'];
if($video_detail['for_dams'] == 1 and $video_detail['for_non_dams'] == 1  ){
    $push_for = "ALL";
}elseif($video_detail['for_dams'] == 1){
    $push_for = "DAMS";
}elseif($video_detail['for_non_dams'] == 1){
    $push_for = "NON_DAMS";
}else{
    $push_for = "";
}
$socketjs = 'http://18.223.244.127/web_socket/examples/js/socket.js';
$custum_js = <<<EOD

              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

						$( function() {
					$( ".dpd1" ).datetimepicker();
					$( ".dpd2" ).datetimepicker();
					 }); // datepicker closed
              </script>
              <script type="text/javascript" charset="utf8">
               $('#video_type').change(function(){
                     $('.video_input_meta').hide();
                  if($(this).val() == '1'){
                      $('#youtube_video_box').show();
                      $('#add_video').hide();
                      $('#vimeo_video_box').hide();
                      $('#thumbnail_url_box').show();
                      $('#add_thumbnail').hide();
                  }
                  if($(this).val() == '2'){
                      $('#youtube_video_box').hide();
                      $('#vimeo_video_box').show();
                      $('#add_video').hide();
                      $('#add_thumbnail').hide();
                      $('#thumbnail_url_box').show();
                  }
                  if($(this).val() == '0'){
                      $('#add_video').show();
                      $('#vimeo_video_box').hide();
                      $('#youtube_video_box').hide();
                      $('#thumbnail_url_box').hide();
                      $('#add_thumbnail').show();
                  }
                  if($(this).val() == '3'){
                    $('.video_input_meta').hide();
                    $('#add_video').hide();
                    $('#vimeo_streaming_video_box').show();
                    $('#add_thumbnail').hide();
                    }
                  /*else{
                      $('#add_video').remove();
                  }*/

              }).change();
              </script>
              <script type="text/javascript" charset="utf8">
                $(document).ready(function(){
                    $("#video_url_youtube").on("keyup", function(){
                      var youtube_url=$('#video_url_youtube').val();
                      $.ajax({
                        url: '$adminurl'+'video_channel/Video_control/get_youtube_details',
                        method: 'POST',
                        dataType: 'json',
                        //crossDomain:true,
                        data: {
                               youtube_url: youtube_url
                        },
                        success: function (data) {
                            $('#author_name').val(data.author_name);
                            $('#video_title').val(data.title);
                            $('#thumbnail_url').val(data.thumbnail_url);
                            $('#video_desc').val(data.title);
                        }
                       });
                    });
                });
              </script>
              <script type="text/javascript" charset="utf8">
                $(document).ready(function(){
                    $("#video_url_vimeo").on("keyup", function(){
                      var vimeo_url=$('#video_url_vimeo').val();
                      $.ajax({
                        url: '$adminurl'+'video_channel/Video_control/get_vimeo_details',
                        method: 'POST',
                        dataType: 'json',
                        //crossDomain:true,
                        data: {
                               vimeo_url: vimeo_url
                        },
                        success: function (data) {
                            $('#author_name').val(data.author_name);
                            $('#video_title').val(data.title);
                            $('#thumbnail_url').val(data.thumbnail);
                            $('#video_desc').val(data.description);
                        }
                       });
                    });
                });
				$( "#maincategory" ).change(function() {
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"video_channel/video_control/get_video_subcategory/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.text+"</option>";
                          });
                           $("#subcategory").html(html).val('$sub_cat_id');
                        }
                      });
                    }).change();

				function getCookie(cn) {
                    var name = cn+"=";
                    var allCookie = decodeURIComponent(document.cookie).split(';');
                    var cval = [];
                    for(var i=0; i < allCookie.length; i++) {
                        if (allCookie[i].trim().indexOf(name) == 0) {
                            cval = allCookie[i].trim().split("=");
                        }
                    }
                    return (cval.length > 0) ? cval[1] : "";
                }
                /* show hide magic */
                  $('.prod-cat a').click(function (e) {
                    div =  $(this).data('div');
                    $('.tabu').hide();
                    $(this).tab('show');

                    var tabContent = '#tabContent' + div;
                    $(tabContent).show();

                    document.cookie = "activediv_test="+tabContent;
                    if(div == 3){
                        google.charts.setOnLoadCallback(drawChart);
                    }

                  });


                  if(getCookie("activediv_test")){
                       $('.tabu').hide();
                     $(getCookie('activediv_test')).show();
                  }

				   jQuery(document).ready(function() {
                       var table = 'all-video-comments-video-grid';
                       var dataTable_video_comments = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 3, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"video_channel/video_control/ajax_video_comments_list/"+"$video_id", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );

                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_video_comments.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable_video_comments.columns(i).search(v).draw();
                        } );
                        // Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#comment_start_date').val() !="" && $('#comment_end_date').val() != "" ){
                                var dates = $('#comment_start_date').val()+','+$('#comment_end_date').val();
                                dataTable_video_comments.columns(3).search(dates).draw();
                            }
                        });
                   } );

				   jQuery(document).ready(function() {
                    var table = 'allfiltration-video-grid';
                    var dataTable_video_comments = jQuery("#"+table).DataTable( {
                        "processing": true,
                        "serverSide": true,
                        "order": [[ 0, "desc" ]],
                        "ajax":{
                            url :"$adminurl"+"video_channel/video_control/ajax_applied_filter_list/"+"$video_id", // json datasource
                            type: "post",  // method  , by default get
                            error: function(){  // error handling
                                jQuery("."+table+"-error").html("");
                                jQuery("#"+table+"_processing").css("display","none");
                            }
                        }
                    } );

                    jQuery("#"+table+"_filter").css("display","none");
                    $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                        var i =$(this).attr('data-column');  // getting column index
                        var v =$(this).val();  // getting search input value
                        dataTable_video_comments.columns(i).search(v).draw();
                    } );

                } );

				   $('#comment_start_date').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true

					});
					$('#comment_end_date').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true

					});

					function update_video_comment(id){
						$('#myModal2').modal('show');
						//$(".file-modal-element-head").html(id);

						 jQuery.ajax({
                        url: "$adminurl"+"video_channel/video_control/get_video_comment_details_by_id/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {
                          if(data){
                            //html =  '<div class="panel-body bg-info "></div>';
                            //$('#comment_details').html(html);
							$('#user_name').val(data.user_name);
							$('#user_comment').val(data.comment);
							$('#comment_video_id').val(data.video_id);
							$('#comment_id').val(data.id);

                          }
                        }
                      });
                    }

					function update_comment(){

						    var user_name = $('#user_name').val();
							var user_comment = $('#user_comment').val();
							var comment_video_id = $('#comment_video_id').val();
							var comment_id = $('#comment_id').val();

						$.ajax({
						   url:"$adminurl"+"video_channel/video_control/update_video_comment",
						   data: {user_comment: user_comment,
						   		  comment_id: comment_id

						   },
						   dataType: 'json',
						   method: 'POST',
						   success : function(status) {
							  $('#myModal2').modal('hide');
							  jQuery("#all-video-comments-video-grid").DataTable().draw();
						   }
					  	});

					}
              </script>
        <script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Month', 'Views' ,'Likes' , 'Comments' ],
                    $out_yearly_js
                ]);

                var options = {
                    title: 'Monthly graph for Views,Likes,Comments',
                    hAxis: {title: 'Year 2017',  titleTextStyle: {color: '#333'}},
                    vAxis: {minValue: 0}
                };

                var chart = new google.visualization.AreaChart(document.getElementById('video_div_views'));
                chart.draw(data, options);

                var data = google.visualization.arrayToDataTable([
                    ['Day', 'Views' ,'Likes','Comments'],
                    $weekly_view_chart
                ]);

                var options = {
                    title: 'Weekly graph for Views,Likes,Comments ',
                    hAxis: {title: 'Current Week',  titleTextStyle: {color: '#333'}},
                    vAxis: {minValue: 0}
                };

                var chart = new google.visualization.AreaChart(document.getElementById('video_div_weekly_views'));
                chart.draw(data, options);
            }
        </script>

        <script src="$socketjs"></script>
        <script type="text/javascript" language="javascript" >
            /***
             *       _____               _          _     __  __                _
             *      / ____|             | |        | |   |  \/  |              (_)
             *     | (___    ___    ___ | | __ ___ | |_  | \  / |  __ _   __ _  _   ___
             *      \___ \  / _ \  / __|| |/ // _ \| __| | |\/| | / _` | / _` || | / __|
             *      ____) || (_) || (__ |   <|  __/| |_  | |  | || (_| || (_| || || (__
             *     |_____/  \___/  \___||_|\_\\___| \__| |_|  |_| \__,_| \__, ||_| \___|
             *                                                            __/ |
             *                                                           |___/
             */
          var socket=$.websocket('ws://125');

            function fire_push(){
                json_var = {};
                json_var.users_type =  "$push_for";
                json_var.users_message =  "$video_title ";
                json_var.device_type = "ALL";
                json_var.notification_type = 'video_channel';
                json_var.notification_text = "$video_id";
                console.log(json_var);
                socket.emit('sync_process', json_var);
                return false;
            }

            socket.on('sync_process', function(msg){
              $('#show_socket_state').text(msg);
              //console.log(msg);
            });
            socket.on('connect', function(user){
              console.log('web_socket connected');
            });

          socket.listen();
        </script>

        <script>
        /* jquery for filtration highly senstive */

            //get all years
            $.ajax({
                url:"$adminurl"+"video_channel/video_control/years_list_for_filtration",
                dataType: 'json',
                method: 'POST',
                success : function(data) {

                  html = '<div class="checkbox">';
                  $.each( data , function( key , value ) {
                    html +=  '<label class="lbl_block"><input type="checkbox" value="'+value.Session+'"> '+ value.Session +'</label>' ;
                  });
                  html +='</div>';
                   $(".year_segment").html(html);
                }
               });

               /* if clicked on radio button of year */
               $(document).on('click', '.year_segment input[type=checkbox]', function () {
                        if (this.checked){
                           year = $(this).val();
                           $.ajax({
                            url:"$adminurl"+"video_channel/video_control/franchise_with_year/"+year,
                            dataType: 'json',
                            method: 'POST',
                            success : function(data) {
                              html = '<div  class="fix-height checkbox main_record_'+year+' fr_year_list"><p class="help-block">Select franchise for year '+year+'</p>';
                              $.each( data , function( key , value ) {
                                 html +=  '<label  class="lbl_block" ><input data-year ="'+year+'" type="checkbox" value="'+value.FranchiseName+'"> '+ value.FranchiseName +'</label>' ;
                              });
                              html +='</div>';
                               $(".fr_segment").append(html);
                            }
                           });


                        } else{
							year = $(this).val();
                            $('.main_record_'+year).remove();
                            $('.handle_'+year).remove();
						}
              });

			  /* while clicking on franchise checkbox */
			  $(document).on('click', '.fr_year_list input[type=checkbox]', function () {
			  			$('.c_segment').html('');
					    $('.fr_year_list input[type=checkbox]').each(function () {
                        if (this.checked){
                           franchise = $(this).val();
						   year = $(this).data('year');
                           $.ajax({
                            url:"$adminurl"+"video_channel/video_control/courses_group_with_year_and_franchise/"+franchise+"/"+year,
                            dataType: 'json',
                            method: 'POST',
							async: false,
                            success : function(data) {
                              html = '<div  data-franchise="'+franchise+'" class=" fix-height handle_'+year+' checkbox main_course_record_'+year+franchise.replace(/ /g,"_")+' fr_course_list"><p class="help-block">Select CourseGroup for year '+year+' , '+franchise+'</p>';
                              $.each( data , function( key , value ) {
                                 html +=  '<label  class="lbl_block" ><input data-franchise="'+franchise+'"  data-year ="'+year+'" type="checkbox" value="'+value.CourseGroup+'"> '+ value.CourseGroup +'</label>' ;
                              });
                              html +='</div>';
                               $(".c_segment").append(html);

                            }
                           });
                        }else{
                            franchise = $(this).val();
                            $('div[data-franchise="'+franchise+'"]').remove();
                        }
                    });
              });


				// Generating courses for course group selected

				 $(document).on('click', '.fr_course_list input[type=checkbox]', function () {
			  			$('.cc_segment').html('');
					    $('.fr_course_list input[type=checkbox]').each(function () {

                        if (this.checked){
                           course_group = $(this).val();
						   year = $(this).data('year');
						   franchise = $(this).data('franchise');
                           $.ajax({
                            url:"$adminurl"+"video_channel/video_control/courses_with_year_and_franchise_and_course_group/"+franchise+"/"+year+"/"+course_group,
                            dataType: 'json',
                            method: 'POST',
							async: false,
                            success : function(data) {
                              html = '<div  data-franchise="'+franchise+'" class=" fix-height handle_'+year+'  checkbox main_course_record_'+year+franchise.replace(/ /g,"_")+course_group+' fr_course_child_list"><p class="help-block">Select Course for year '+year+' , '+franchise+' , course_group- '+course_group+'</p>';
                              $.each( data , function( key , value ) {
                                 html +=  '<label  class="lbl_block" ><input data-course_group="'+course_group+'" data-franchise="'+franchise+'"  data-year ="'+year+'" type="checkbox" value="'+value.Course+'"> '+ value.Course +'</label>' ;
                              });
                              html +='</div>';
                               $(".cc_segment").append(html);

                            }
                           });
                        }
                    });
              });

                / * getting batches */
                $(document).on('click', '.fr_course_child_list input[type=checkbox]', function () {
                    $('.course_segment').html('');
                     $('.fr_course_child_list input[type=checkbox]').each(function () {

                  if (this.checked){
                      course = $(this).val();
                     course_group = $(this).data('course_group');
                     year = $(this).data('year');
                     franchise = $(this).data('franchise');

                     $.ajax({
                      url:"$adminurl"+"video_channel/video_control/batches_with_year_and_franchise_and_course_group_and_course/"+franchise+"/"+year+"/"+course_group+"/"+course,
                      dataType: 'json',
                      method: 'POST',
                      async: false,
                      success : function(data) {
                        html = '<div style ="display:" data-franchise="'+franchise+'" class="fix-height handle_'+year+'  checkbox main_course_record_'+year+franchise.replace(/ /g,"_")+course_group+course+'"><p class="help-block">Select Course for year '+year+' , '+franchise+' , course_group- '+course_group+'</p>';
                        $.each( data , function( key , value ) {
                           html +=  '<label  class="lbl_block" ><input data-course="'+course+'" data-course_group="'+course_group+'" data-franchise="'+franchise+'"  data-year ="'+year+'" type="checkbox" value="'+value.Batch+'"> '+ value.Batch +'</label>' ;
                        });
                        html +='</div>';
                         $(".course_segment").append(html);
                      }
                     });
                  }
              });
        });

        /* save all batches */

        $(document).on('click', '.save_filtration ', function () {
            var values = [];
            $('.course_segment input[type=checkbox]').each(function () {
                if (this.checked){
                    var temp = {};
                    temp.video_id  = '$video_id';
                    temp.Session  = $(this).data('year');
                    temp.FranchiseName = $(this).data('franchise');
                    temp.CourseGroup  = $(this).data('course_group');
                    temp.Course = $(this).data('course');
                    temp.Batch  = $(this).val();
                    values.push(temp);
                }
            });
            $.ajax({
                url:"$adminurl"+"video_channel/video_control/save_batches_record",
                dataType: 'json',
                data: { data : values},
                method: 'POST',
                success : function(data) {
                    console.log(123);
                    location.reload();
                }
               });

        });

        
        </script>
        <script>
        $('#select-all').click(function(event) {
            if(this.checked) {
                // Iterate each checkbox
                $('.cat_check:checkbox').each(function() {
                    this.checked = true;
                });
            } else {
                $('.cat_check:checkbox').each(function() {
                    this.checked = false;
                });
            }
        
            jQuery('.cat_check').change();
        });
        </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
