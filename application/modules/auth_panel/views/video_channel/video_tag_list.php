<?php  $error = validation_errors();
$display = "display:none;";
if(!empty($error)){ $display = "";} 
if($this->input->get('tag_id')){$display = "display:none;";}
?>
<div class="col-lg-12 add_series_element" style="<?php echo $display; ?>">
   <div class="col-lg-12">
      <section class="panel">
         <header class="panel-heading">
            Add Video Tag
         </header>
         <div class="panel-body">
            <form autocomplete="off" role="form" method= "POST" action="<?php echo AUTH_PANEL_URL.'video_channel/Video_control/add_video_tag' ?>" enctype="multipart/form-data">
               <div class="form-group col-sm-3">
                  <label for="tag_name">Tag Name</label>
                  <input type="text" class="form-control input-sm" name = "tag_name" id="tag_name" value="<?php echo set_value('tag_name'); ?>" placeholder="Enter Tag Name" >
                  <span class="text-danger"><?php echo form_error('tag_name');?></span>
               </div>

               <div class="form-group col-md-12">    
                 <input type="submit" name="add_tag" class="btn btn-info btn-sm" value="Submit">
                 <button class="btn btn-danger btn-sm" onclick="$('.add_series_element').hide('slow');" type="button" >Cancel</button>
               </div>
            </form>
         </div>
      </section>
   </div>
   <div class="clearfix"></div>
</div>
<!-- end of add_currency section -->
<!--- edit currency section -->
<?php if($this->input->get('tag_id')){ ?>
    <div class="col-lg-12">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Edit Tag
            </header>
            <div class="panel-body">
                <form autocomplete="off" role="form" id="edit_tag" method= "POST" action="<?php echo AUTH_PANEL_URL.'video_channel/Video_control/add_video_tag?tag_id='.$this->input->get('tag_id'); ?>" enctype="multipart/form-data">
                  <?php if($this->input->get('tag_id')){
                    ?>
                  <input type="hidden" name="tag_id" value="<?php echo $this->input->get('tag_id'); ?>">
                  <?php } ?>
                <div class="form-group col-sm-3">
                    <label for="tag_name">Tag Name</label>
                    <input type="text" class="form-control input-sm" name = "tag_name" id="tag_name" placeholder="Enter Tag Name" value="<?php if($tags){ echo $tags['tag_name'];}?>" >
                    <span class="text-danger"><?php echo form_error('tag_name');?></span>
                </div>
               
                <div class="form-group col-md-12">    
                    <input type="submit" name="edit_tag" class="btn btn-info btn-sm" value="submit">
                    <a class='btn btn-danger btn-sm' href="<?php echo AUTH_PANEL_URL.'video_channel/Video_control/add_video_tag';?>" >Cancel</a>
                </div>
                </form>
            </div>
        </section>
    </div>
    <div class="clearfix"></div>
    </div>
<?php } ?>
<!--- and of edit  currency section -->
<div class="col-sm-12">
	<section class="panel">
    <?php
    if(!$this->input->get('tag_id')){
    ?>
		<header class="panel-heading">
      Video Tag List
       <button onclick="$('.add_series_element').show('slow');" class="btn-success btn-xs btn pull-right"><i class="fa fa-plus"></i> Add</button>
		<?php //echo strtoupper($page); ?>
		</header>
    <?php } ?>
		<div class="panel-body">
		<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="all-currency-grid">
  		<thead>
    		<tr>
          <th>#</th>
      		<th>Tag Name </th>
          <th>Action</th>
    		</tr>
  		</thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
              <th></th>
              
          </tr>
      </thead>
		</table>
		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
//if($page == 'android') { $device_type = 1; } elseif ($page == 'ios') { $device_type = 2; } elseif ($page == 'all') { $device_type = '0'; } elseif ($page == 'instructor') { $device_type = '0'; }
//$query_string = ($this->input->get('user' ) =='instructor')? 'instructor' : '' ;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-currency-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"video_channel/Video_control/ajax_all_tag_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );
                   } );
               </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>

