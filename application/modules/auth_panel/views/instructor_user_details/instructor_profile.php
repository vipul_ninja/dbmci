
<?php //echo "<pre>";print_r($user_data);die; ?>

<div class="col-md-12"><a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back to instructor list</button></a></div><br><br>

<aside class="profile-nav col-lg-3">
      <section class="panel">
          <div class="user-heading round">
              <a href="#">
                  <img alt="" src="<?php echo $user_data['profile_picture']; ?>">
              </a>
              <h1><?php echo $user_data['username']; ?></h1>
              <p><?php echo $user_data['email']; ?></p>
          </div>

      

      </section>

      <!--moderator form -->
       
			  
	  <section class="panel">
		  <header class="panel-heading">
			  Instructor
		  </header>
		  <div class="panel-body">
			  <form role="form" class="form-inline" method="post" action="<?php echo AUTH_PANEL_URL.'instructor_user_details/is_instructor/'.$user_data['id']; ?>">
				  <div class="form-group col-md-4">
					  <label><input type="radio" <?php  echo $checked = ($user_data['status'] == 0)?'checked':''; ?> value="1"  name="instructor">Yes</label>
				  </div>
				  <div class="form-group col-md-4">
					  <label><input type="radio" <?php  echo $checked = ($user_data['status'] == 1)?'checked':''; ?> value="0"  name="instructor">No</label>
				  </div>
				  <div class="form-group col-md-4">
				  <button class="btn btn-xs bold btn-success pull-right" type="submit">Save</button>
				  </div>
			  </form>

		  </div>
	  </section>

	
  </aside>
  <aside class="profile-info col-lg-9">
      
 
	  
	  <?php // if($user_data['is_instructor']== 1){?>
	   <section class="panel">
		  <header class="panel-heading">
			  Instructor Details
		  </header>
		  <div class="panel-body">
			  <form role="form" method="post">
				  <div class="form-group">                                   
					  <input type="hidden" placeholder="" id="user_id" name = "user_id" value ="<?php if(isset($user_instructor_data['user_id'])){ echo $user_instructor_data['user_id']; } ?>" class="form-control">
				  </div>
				  <div class="form-group">
					  <label for="exampleInputEmail1">Beneficiary name</label>
					  <input type="text" placeholder="Enter beneficiary name"  name = "u_name" value ="<?php if(isset($user_instructor_data['u_name'])){ echo $user_instructor_data['u_name']; } ?>" class="form-control">
				  </div>
				  <div class="form-group">
					  <label for="exampleInputEmail1">Beneficiary account</label>
					  <input type="text" placeholder="Enter beneficiary account"  name = "b_account" value ="<?php if(isset($user_instructor_data['b_account'])){ echo $user_instructor_data['b_account']; } ?>" class="form-control">
				  </div>
				  <div class="form-group">
					  <label for="exampleInputEmail1">Bank IFSC</label>
					  <input type="text" placeholder="Enter bank IFSC"  name = "b_ifsc" value ="<?php if(isset($user_instructor_data['b_ifsc'])){ echo $user_instructor_data['b_ifsc']; } ?>" class="form-control">
				  </div>
				  <div class="form-group">
					  <label for="exampleInputEmail1">Bank address</label>
					  <input type="text" placeholder="Enter bank address"  name = "b_address" value ="<?php if(isset($user_instructor_data['b_address'])){ echo $user_instructor_data['b_address']; }  ?>" class="form-control">
				  </div>
				  <div class="form-group">
					  <label for="exampleInputEmail1">Bank name</label>
					  <input type="text" placeholder="Enter bank name"  name = "b_name" value ="<?php if(isset($user_instructor_data['b_name'])){ echo $user_instructor_data['b_name']; } ?>" class="form-control">
				  </div>
				   <div class="form-group">
					  <label for="exampleInputEmail1">Resource sharing</label>
					  <input type="text" placeholder="Enter resource sharing"  name = "r_sharing" value ="<?php if(isset($user_instructor_data['r_sharing'])){ echo $user_instructor_data['r_sharing']; } ?>" class="form-control">
				  </div>
				  <button class="btn btn-info btn-sm" type="submit" name="update_instructor_detail">Update</button>
			  </form>

		  </div>
	  </section>
		


		<div class="col-sm-12">
		<section class="panel">
			<header class="panel-heading">
		 Instructor's reviews
	</header>
			<div class="panel-body">
			<div class="adv-table">
			<table  class="display table table-bordered table-striped" id="all-instructor-reviews-grid">
			<thead>
				<tr>

			  <th>#</th>
			  <th>User name</th>
			  <th>Rating</th>
			  <th>Review</th>
			  <th>Created at</th> 

			 <th>Action </th> 	 
				</tr>
			</thead>
		  <thead>
			  <tr>
				  <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="4"  class="search-input-text form-control"></th>


				 <th></th> 
			  </tr>
		  </thead>
			</table>
			</div>
			</div>
		</section>
	</div>
	  <?php // } ?>
</aside>
<?php 


	
$instructor_id = $user_data['id'];
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
                <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
				<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
                <script type="text/javascript" language="javascript" > 
			
						jQuery(document).ready(function() {
						   var table = 'all-instructor-reviews-grid';
						   var dataTable = jQuery("#"+table).DataTable( {
							   "processing": true,
								"pageLength": 15,
								"lengthMenu": [[15, 25, 50], [15, 25, 50]],
							   "serverSide": true,
							   "order": [[ 4, "desc" ]],
							   "ajax":{
								   url :"$adminurl"+"instructor_user_details/ajax_instructor_ratings_list/$instructor_id", // json datasource
								   type: "post",  // method  , by default get
								   error: function(){  // error handling
									   jQuery("."+table+"-error").html("");
									   jQuery("#"+table+"_processing").css("display","none");
								   }
							   }
						   } );
						   jQuery("#"+table+"_filter").css("display","none");
						   $('.search-input-text').on( 'keyup click', function () {   // for text boxes
							   var i =$(this).attr('data-column');  // getting column index
							   var v =$(this).val();  // getting search input value
							   dataTable.columns(i).search(v).draw();
						   } );
							$('.search-input-select').on( 'change', function () {   // for select box
								var i =$(this).attr('data-column');
								var v =$(this).val();
								dataTable.columns(i).search(v).draw();
							} );											
					   } );			   
				
				    
               </script>              

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );





