
<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		<?php echo strtoupper($page); ?> USER(s) LIST
		</header>
		<div class="panel-body ">
            <div class="adv-table ">
                <div class="col-md-6 pull-right ">
                    <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
                            <div  class="input-group-addon">From</div>
                        <input type="text" id="min-date-user" class="form-control date-range-filter input-sm course_start_date"  placeholder="">

                        <div class="input-group-addon">to</div>

                        <input type="text" id="max-date-user" class="form-control date-range-filter input-sm course_end_date"  placeholder="">

                    </div>
                </div>
                <div class="table-responsive col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
                    <table  class="table display table-bordered table-striped" id="all-user-grid">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User name </th>
                                <th>Email </th>
                                <th>Mobile </th>                            
                                <th>Status </th>
                                <th>Creation time </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
                      
                                
                                <th>
                                    <select data-column="4"  class="form-control search-input-select">
                                        <option value="">All</option>
                                        <option  value="1">Active</option>
                                        <option value="2">Disable</option>
                                    </select>
                                </th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>    
            </div>
		</div>
	</section>
</div>

<?php
$query_string = "";
$adminurl = AUTH_PANEL_URL;
if($page == 'instructor') { 
	$device_type = '0'; 
	$query_string = 'instructor';
}
$custum_js = <<<EOD
              <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-user-grid';
                       var dataTable_user = jQuery("#"+table).DataTable( {
                            "processing": true,
                            "language": {
                                "processing": "<i class='fa fa-spin  fa-spinner fa-4x' style='z-index:999'><i>" //add a loading image,simply putting <img src="loader.gif" /> tag.
                            },
                            "autoWidth": false,
                            "responsive": true,
                            "scrollX": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                            "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"instructor_user_details/ajax_all_instructors_list/"+"$device_type"+"?user=$query_string", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_user.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable_user.columns(i).search(v).draw();
                        } );
						// Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date-user').val() !="" && $('#max-date-user').val() != "" ){
                                var dates = $('#min-date-user').val()+','+$('#max-date-user').val();
                                dataTable_user.columns(5).search(dates).draw();
                            }    
                        }); 
                   } );
				   
				   $('#min-date-user').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true
						
					});
					$('#max-date-user').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true
						
					});
               </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
