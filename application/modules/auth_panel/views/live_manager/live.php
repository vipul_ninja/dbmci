<?php 
    $query = $this->db->query(" SELECT id,name,profile_picture,email
                                from users
                                where is_live_agent = 1"
                                );
    $live_agent = $query->result_array();


?>
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Go live
                <p>Server -: rtmp://ec2-54-255-222-242.ap-southeast-1.compute.amazonaws.com/livepkgr</p>
                <p>Stream name-: livestream?adbe-live-event=liveevent </p>
            </header>
            <div class="panel-body">
                <form method="post" role="form" class="form-inline">
                  <input type="hidden" name="id" value="<?php echo $live_row['id'];?>">
                    <div class="form-group">
                        <label for="exampleInputEmail2" class="">Person Name </label>
                        <select name="user_id" class="form-control">
                               <?php 
                                    foreach($live_agent as $la) {
                                        echo '<option value='.$la['id'].'>'.$la['name'].'</option>';
                                    } 
                                 ?>
                        </select>

                        <!--<input name="name" value="<?php //echo $live_row['name'];?>" type="text" placeholder="Name" id="exampleInputEmail2" class="form-control">-->
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword2" class="">Select User</label>
                        <select name="stream_for" class="form-control">
                                <option <?php if($live_row['stream_for'] == "ALL"){ echo 'selected="selected"'; }?> value="ALL">ALL</option>
                                <option  <?php if($live_row['stream_for'] == "DAMS_USER"){ echo 'selected="selected"'; }?> value="DAMS_USER" >DAMS USER</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword2" class="">Send notification</label>
                            <label class="checkbox-inline">
                                 <input type="radio" value="1" id="inlineCheckbox1" name="send_notify"> Yes
                            </label>
                             <label class="checkbox-inline">
                                 <input type="radio" value="0" id="inlineCheckbox2" checked="checked" name="send_notify"> No
                            </label>
                    </div>
                    <input type="hidden" name="state" value="<?php echo $live_row['state'];?>">
                    <?php if($live_row['state'] == 0 ){ ?>
                    <button class="btn btn-success" type="submit">Start Streaming</button>
                    <?php }else{ ?>
                    <button class="btn btn-danger" type="submit">Stop Streaming</button>
                    <?php  } ?>
                </form>
            </div>
        </section>
    </div>
