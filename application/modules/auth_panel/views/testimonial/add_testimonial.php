<div>
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Add <?php echo $p_heading; ?>
      </header>
      <div class="panel-body">
        <form autocomplete="off" id="dose_form_sub" novalidate="novalidate" role="form" method="POST" action="" enctype="multipart/form-data">
         
          <div class="form-group">
            <label for="desc">Testimonial</label>
            <textarea class="form-control" id="description" name = "description" rows="3" placeholder="Enetr Testimonial" required></textarea>
            <span class="text-danger"><?php echo form_error('description');?></span>
          </div>
          <div class="form-group">
            <label for="post_type">User id </label>
              <input type='text' name="user_id"  class="form-control" placeholder="Enter user id" required="required" />
            <span class="text-danger"><?php echo form_error('user_id');?></span>
          </div>
         
       
          <link href="<?php echo AUTH_ASSETS; ?>assets/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet"/>
          <link href="<?php echo AUTH_ASSETS; ?>css/image-crop.css" rel="stylesheet"/>

         
          <div class="col-md-12"> <button type="submit"  id="submit" class="btn btn-info pull-right ">Submit</button> </div>
        </div>

      </form>
    </div>
  </section>
</div>
<div class="clearfix"></div>
</div>
<!-- page end-->
</section>




  <?php
$adminurl             = AUTH_PANEL_URL;
$jquery_color_js      = AUTH_ASSETS.'assets/jcrop/js/jquery.color.js';
$jquery_Jcrop_min_js  = AUTH_ASSETS.'assets/jcrop/js/jquery.Jcrop.min.js';
$validation_js        = AUTH_ASSETS."js/jquery.validate.min.js";
$ckeditor_js = AUTH_ASSETS.'assets/ckeditor/ckeditor.js';

$custum_js = <<<EOD

<script src="$jquery_color_js"></script>
<script src="$jquery_Jcrop_min_js"></script>
<script src="$validation_js"></script>
<script src="$ckeditor_js"></script>

<script>
  CKEDITOR.replace('description');
</script>

<script>


</script>
EOD;

echo modules::run('auth_panel/template/add_custum_js',$custum_js );
