<div class="col-lg-6">
	  <section class="panel">
		  <header class="panel-heading">
			  Edit Video 
		  </header>
		  <div class="panel-body">
			  <form role="form" method="post" enctype="multipart/form-data">
				 <input type="hidden"  name = "id" id="id" value="<?php echo $video_detail['id']; ?>" class="form-control input-sm">
				  <div class="form-group">
                    <label for="exampleInputEmail1">Subject</label>
                    <select class="form-control input-sm subject_element_select" name="subject_id" class="form-control">						 
                    </select>
					  <span class="error bold"><?php echo form_error('subject_id');?></span>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Topic</label>
                    <select class="form-control input-sm topic_element_select" name="topic_id" class="form-control">						 
                    </select>
					<span class="error bold"><?php echo form_error('topic_id');?></span>
                </div>
				     <div class="form-group">
                    <label for="exampleInputEmail1">Description</label>
                   <textarea rows="4" cols="50" class="form-control input-sm" name="description"  class="form-control"><?php echo $video_detail['description'] ?></textarea><span class="error bold"><?php echo form_error('description');?></span>
                </div>
                                 
				  <div class="form-group">
					  <label >Video Title</label>
					  <input type="test" placeholder="Enter title" name = "title" id="title" value = "<?php echo $video_detail['title'] ?>" class="form-control">
					   <span class="error bold"><?php echo form_error('title');?></span>
				  </div>
          <div class="form-group">
					  <label >Video Type</label>
				 <select class="form-control input-sm video_type" name="video_type" id="video_type" class="form-control">
            <option value="0" <?php  if($video_detail['video_type']=='0') { echo 'selected';} ?> >Normal</option>	
            <option value="1"  <?php  if($video_detail['video_type']=='1') { echo 'selected';} ?>>Youtube</option>
            <option value="2" <?php  if($video_detail['video_type']=='2') { echo 'selected';} ?>>Vimeo</option>
            <option value="3"  <?php  if($video_detail['video_type']=='3') { echo 'selected';} ?>>Vimeo streaming</option>		
        </select>
					   <span class="error bold"><?php echo form_error('video_type');?></span>
				  </div>
		        <div class="form-group upload_video_cls">
					  <label for="exampleInputFile">Upload Video</label>
					  <input type="file" accept="video/mp4" name = "video_file" id="exampleInputFile">
					  <span class="error bold"><?php echo form_error('video_file');?></span>
				  </div> 
          <div class="form-group video_input_meta " id="vimeo_video_url" style="display: none;">
                 <label for="vimeo_video_url"> Video URL</label><input type="text" class="form-control" placeholder="Enter Vimeo video URL" name = "vimeo_video_url" id="vimeo_video_url" value = "<?php echo $video_detail['file_url'] ?>" >
                 <span class="error bold"><?php echo form_error('vimeo_video_url');?></span>

               </div> 
				  <div class="form-group">
					  <label for="exampleInputFile">Video Thumbnail</label>
					  <input type="file" accept="image/*" name = "thumbnail" id="">
					  <span class="error bold"><?php echo form_error('thumbnail');?></span>
				  </div> 
		
				
				  <button class="btn btn-info"  type="submit" >Update</button>
			  </form>

		  </div>
	  </section>
  </div>

<?php
$adminurl = AUTH_PANEL_URL;
$subject_id = $video_detail['subject_id'];
$topic_id = $video_detail['topic_id'];
$custum_js = <<<EOD
                
				 <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" > 
			   
                  jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"'>"+value.name+"</option>"; 
                        });
                         $(".subject_element_select").html(html).val('$subject_id').change();
                      }
                    });

                    $( ".subject_element_select" ).change(function() {
                      id = $(this).val(); 
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.topic+"</option>"; 
                          });
                           $(".topic_element_select").html(html).val('$topic_id'); 
                        }
                      });
                    });
					
                    $('#video_type').change(function(){
                      //  alert($('#video_type').val());
                      if($('#video_type').val() == '2' || $('#video_type').val() == '3' || $('#video_type').val() == '1'){
                        $(".upload_video_cls").hide();
                     $("#vimeo_video_url").show();
                     
                     }else{
                         $(".upload_video_cls").show();
                         
                         $("#vimeo_video_url").hide();
                         
                     }
                    });
                    $(function () {
                      $("select#video_type").change();
                  });
               </script>              

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );