
<div class="col-lg-6">
	  <section class="panel">
		  <header class="panel-heading">
			  Edit Concept
		  </header>
		  <div class="panel-body">
			  <form role="form" method="post" enctype="multipart/form-data">
				 <input type="hidden"  name = "id" id="id" value="<?php echo $pdf_detail['id']; ?>" class="form-control input-sm">
           
         <div class="form-group">
              <label for="exampleInputEmail1">Subject</label>
              <select class="form-control input-sm subject_element_select" name="subject_id" class="form-control">						
              </select>
              <span class="error bold"><?php echo form_error('subject_id');?></span>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Topic</label>
              <select class="form-control input-sm topic_element_select" name="topic_id" class="form-control">						
              </select>
              <span class="error bold"><?php echo form_error('topic_id');?></span>
              </div>  
         <div class="form-group">
            <label >Heading</label>
            <input type="test" placeholder="Enter Heading" name = "title" id="title" value = "<?php echo $pdf_detail['title'] ?>" class="form-control">
             <span class="error bold"><?php echo form_error('title');?></span>
          </div>
          <div class="form-group">
            <label >Sub Heading</label>
            <input type="text" placeholder="Enter subheading" value="<?php echo $pdf_detail['sub_title'] ?> " name = "sub_title" id="sub_title" class="form-control">
             <span class="error bold"><?php echo form_error('sub_title');?></span>
          </div>
            

            <div class="form-group">
            <label for="exampleInputEmail1">Description</label>
            <textarea rows="4" cols="50" class="form-control input-sm" name="description"  id="description" class="form-control"><?php echo $pdf_detail['description'] ?></textarea>					 
            <span class="error bold"><?php echo form_error('description');?></span>
            </div>
         
		        
<!--
				  <div class="form-group">
					  <label for="exampleInputFile">PDF Thumbnail</label>
					  <input type="file" accept="image/*" name = "thumbnail" id="">
					  <span class="error bold"><?php //echo form_error('thumbnail');?></span>
				  </div>

				   <div class="form-group">
                    <label for="exampleInputEmail1">Page Count</label>
                     <input type="test" placeholder="Enter page count" name = "page_count" id="page_count" value = "<?php //echo $pdf_detail['page_count'] ?>" class="form-control">
						 <span class="error bold"><?php //echo form_error('page_count');?></span>                  
                </div>-->
				
				  <button class="btn btn-info btn-sm"  type="submit" >Update</button>
          <a href="<?php echo AUTH_PANEL_URL . 'library/library/add_concept'; ?>"><button class="btn btn-primary btn-sm"  type="button" >Back</button></a>
			  </form>

		  </div>
	  </section>
  </div>
  <!-- only hindi section -->
  <div class="col-lg-6">
	  <section class="panel">
		  <header class="panel-heading">
			  Edit Concept
		  </header>
		  <div class="panel-body">
			  <form role="form" method="post" enctype="multipart/form-data">
				 <input type="hidden"  name = "id" id="id" value="<?php echo $pdf_detail['id']; ?>" class="form-control input-sm">

         <div class="form-group">
            <label >Heading (Hindi)</label>
            <input type="test" placeholder="Enter Heading" name = "title_2" id="title_2 " value = "<?php echo $pdf_detail['title_2'] ?>" class="form-control">
             <span class="error bold"><?php echo form_error('title_2');?></span>
          </div>
          <div class="form-group">
            <label >Sub Heading (Hindi)</label>
            <input type="text" placeholder="Enter subheading" value="<?php echo $pdf_detail['sub_title_2'] ?> " name = "sub_title_2" id="sub_title_2" class="form-control">
             <span class="error bold"><?php echo form_error('sub_title_2');?></span>
          </div>
            <!-- <div class="form-group">
              <label for="exampleInputEmail1">Subject</label>
              <select class="form-control input-sm subject_element_select" name="subject_id" class="form-control">						
              </select>
              <span class="error bold"><?php echo form_error('subject_id');?></span>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Topic</label>
              <select class="form-control input-sm topic_element_select" name="topic_id" class="form-control">						
              </select>
              <span class="error bold"><?php echo form_error('topic_id');?></span>
              </div> -->

            <div class="form-group">
            <label for="exampleInputEmail1">Description (Hindi)</label>
            <textarea rows="4" cols="50" class="form-control input-sm" name="description_2"  id="description_2" class="form-control"><?php echo $pdf_detail['description_2'] ?></textarea>					 
            <span class="error bold"><?php echo form_error('description_2');?></span>
            </div>
         
		        
<!--
				  <div class="form-group">
					  <label for="exampleInputFile">PDF Thumbnail</label>
					  <input type="file" accept="image/*" name = "thumbnail" id="">
					  <span class="error bold"><?php //echo form_error('thumbnail');?></span>
				  </div>

				   <div class="form-group">
                    <label for="exampleInputEmail1">Page Count</label>
                     <input type="test" placeholder="Enter page count" name = "page_count" id="page_count" value = "<?php //echo $pdf_detail['page_count'] ?>" class="form-control">
						 <span class="error bold"><?php //echo form_error('page_count');?></span>                  
                </div>-->
				
				  <button class="btn btn-info btn-sm" name="hindi" type="submit" value="hindi" >Update</button>
          <a href="<?php echo AUTH_PANEL_URL . 'library/library/add_concept'; ?>"><button class="btn btn-primary btn-sm"  type="button" >Back</button><a>
			  </form>

		  </div>
	  </section>
  </div>
<?php
$adminurl = AUTH_PANEL_URL;
$subject_id = $pdf_detail['subject_id'];
$topic_id = $pdf_detail['topic_id'];
$ckeditor_js = AUTH_ASSETS.'assets/ckeditor/ckeditor.js';
$custum_js = <<<EOD

<script src="$ckeditor_js"></script>
<script>
CKEDITOR.replace('description');
CKEDITOR.replace('description_2');
</script>
                
				 <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" > 
			   
                  jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"'>"+value.name+"</option>"; 
                        });
                         $(".subject_element_select").html(html).val('$subject_id').change();
                      }
                    });

                    $( ".subject_element_select" ).change(function() {
                      id = $(this).val(); 
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.topic+"</option>"; 
                          });
                           $(".topic_element_select").html(html).val('$topic_id');
                        }
                      });
                    });
					
				
               </script>              

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );