<div class="col-md-12">
<div class="col-md-3">
   <div class="panel panel-primary" style="min-height: 200px ! important;" >
      <div class="panel-heading"><i class="fa fa-folder-open fa-2x "></i> Course </div>
      <div class="panel-body">
         <li><a href="<?php echo AUTH_PANEL_URL."course_product/Course/add_course_product"; ?>">Add New Course</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL."course_product/Course/all_Courses_list"; ?>">View All Course</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL."course_product/Course_category/add_course_stream"; ?>">Add/View Course Category</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL."course_product/Course_faq/course_faq_management"; ?>">Add/view Course FAQ</a></li>
      </div>
   </div>
</div>
<div class="col-md-3">
   <div class="panel panel-primary" style="min-height: 200px ! important;" >
      <div class="panel-heading"><i class="fa fa-2x  fa-money"></i> Coupon</div>
      <div class="panel-body">
         <li><a href="<?php echo AUTH_PANEL_URL."coupon_master/coupon/add_coupon"; ?>">Add/View Coupon List</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL."course_product/Coupon_course_relation/course_coupon/"; ?>">View course <=> coupon relation</a></li>
      </div>
   </div>
</div>
<div class="col-md-3">
   <div class="panel panel-primary" style="min-height: 200px ! important;" >
      <div class="panel-heading"><i class="fa  fa-2x  fa-briefcase"></i> File Library</div>
      <div class="panel-body">
         <li><a href="<?php echo AUTH_PANEL_URL . 'library/library/index'; ?>">Add/View pdf library</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_ppt'; ?>">Add/View ppt library</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_video'; ?>">Add/View video library</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_epub'; ?>">Add/View epub library</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_doc'; ?>">Add/View doc library</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_image'; ?>">Add/View image library</a></li>
      </div>
   </div>
</div>
</div>
<div class="col-md-12">
<div class="col-md-3">
   <div class="panel panel-primary" style="min-height: 200px ! important;" >
      <div class="panel-heading"><i class="fa fa-2x  fa-pencil"></i> Test Series</div>
      <div class="panel-body">
         <li><a href="<?php echo AUTH_PANEL_URL . 'test_series/test_series'; ?>">Add/View test series</a></li>
         <li><a href="<?php echo AUTH_PANEL_URL . 'question_bank/question_bank/add_question'; ?>">Add/View questions</a></li>
      </div>
   </div>
</div>
<div class="col-md-3">
   <div class="panel panel-primary" style="min-height: 200px ! important;" >
      <div class="panel-heading"><i class="fa fa-2x  fa-rupee"></i> Account Section</div>
      <div class="panel-body">
         <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions'; ?>">View course transactions</a></li>
      </div>
   </div>
</div>
<div class="col-md-3 hide"  >
   <div class="panel panel-primary" style="min-height: 200px ! important;" >
      <div class="panel-heading"><i class="fa fa-2x  fa-money"></i> Currency</div>
      <div class="panel-body">
         <li><a href="<?php echo AUTH_PANEL_URL . 'currency_master/Currency_master/add_currency'; ?>">View Currency</a></li>
      </div>
   </div>
</div>
</div>

