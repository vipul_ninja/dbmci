	<div class="col-lg-6">
	  <!--Course info table start-->
	  <section class="panel">
		  <div class="panel-body">
			  <a class="task-thumb" href="#">
				  <img alt="" height="90" width="90" src="<?php echo $course_transactions_details['course_image']; ?>">
			  </a>
			  <div class="task-thumb-details">
				  <h1><a href="#"><?php echo $course_transactions_details['course_name']; ?></a></h1>
				  <p>Course</p>
			  </div>
		  </div>
		  
		   <div class="panel-body">
              <form role="form" method="post" action="<?php echo AUTH_PANEL_URL.'/course_product/course_transactions/update_course_transaction_status' ?>" >
				<input type="hidden"  placeholder="" name = "id" id="id" value="<?php echo $course_transactions_details['id']; ?>" class="form-control input-sm">
				 
				   <div class="form-group col-md-12 ">
					  <label >Pre Transaction Id</label>
					  <input type="text" readonly placeholder="Enter total no. question" name = "pre_transaction_id" id="pre_transaction_id" value="<?php echo $course_transactions_details['pre_transaction_id']; ?>" class="form-control input-sm">
					   <span class="error bold"><?php echo form_error('pre_transaction_id');?></span>
				  </div>
				   <div class="form-group col-md-12 ">
					  <label>Post Transaction Id</label>
					  <input type="text" <?php if($course_transactions_details['transaction_status'] == 1){echo "readonly";} ?> placeholder="post trnasaction id" name = "post_transaction_id" id="post_transaction_id" value="<?php echo $course_transactions_details['post_transaction_id']; ?>" class="form-control input-sm">
					   <span class="error bold"><?php echo form_error('post_transaction_id');?></span>
				  </div>					
				   <div class="form-group col-md-12 ">
                    <label for="exampleInputEmail1">Pay Via</label>
                      <select class="form-control input-sm" name="pay_via"  id="pay_via" value="" >						
						<option value="PAY_TM" <?php if($course_transactions_details['pay_via'] == 1){ echo "selected";} ?> >PayTm</option>
							<!--<option value="2" <?php if($course_transactions_details['pay_via'] == 2){ echo "selected";} ?>>Others</option>-->					     		  
					  </select> 
					   <span class="error bold"><?php echo form_error('pay_via');?></span>					 						 
                </div>
				  <div class="form-group col-md-12 ">
                    <label for="exampleInputEmail1">Transaction Status</label>
                      <select class="form-control input-sm" name="transaction_status"  id="transaction_status" value="" >
						  <?php if($course_transactions_details['transaction_status'] != 1){ ?>
							<option value="0" <?php if($course_transactions_details['transaction_status'] == 0){ echo "selected";} ?> >Pending</option>
							<option value="1" <?php if($course_transactions_details['transaction_status'] == 1){ echo "selected";} ?> >Completed</option>
							<option value="2" <?php if($course_transactions_details['transaction_status'] == 2){ echo "selected";} ?>>Failed</option>
							<option value="3" <?php if($course_transactions_details['transaction_status'] == 3){ echo "selected";} ?>>Refund Requested </option>	
							<option value="4" <?php if($course_transactions_details['transaction_status'] == 4){ echo "selected";} ?>>Refunded </option>				                    
				<?php  }else{ ?>					 
							<option value="1" <?php if($course_transactions_details['transaction_status'] == 1){ echo "selected";} ?> >Completed</option>										<?php } ?>	  
					  </select> 
					   <span class="error bold"><?php echo form_error('transaction_status');?></span>					 						 
                </div>
	  			 
          <div class="form-group col-md-12 ">    
				    <button class="btn btn-info btn-sm"  type="submit" >Update</button>
          </div>
			  </form>

          </div>
  
	  </section>
	 
	</div>  
	<div class="col-lg-6">
	  <!--user info table start-->
	  <section class="panel">
		  <div class="panel-body">
			  <a class="task-thumb" href="#">
				  <img alt="" height="90" width="90" src="<?php echo $course_transactions_details['user_profile_picture']; ?>">
			  </a>
			  <div class="task-thumb-details">
				  <h1><a href="#"><?php echo $course_transactions_details['user_name']; ?></a></h1>
				  <p>User</p>
			  </div>
				<div class="pull-right">
					<a target="_blank" href="<?php echo AUTH_PANEL_URL."/course_product/course_transactions/genrate_receipt/".$course_transactions_details['id']; ?>">
						<button class="btn btn-xs btn-success bold "> <i class="fa fa-file"></i>Download Receipt </button>
					</a>
		  </div>
		  <table class="table table-hover personal-task">
			  <tbody>
				<tr>
					<td>
						<strong>Email</strong>
					</td>
					<td><?php echo $course_transactions_details['user_email']; ?></td>
					
				</tr>
				<tr>
					<td>
						<strong>Phone</strong>
					</td>
					<td><?php echo $course_transactions_details['user_mobile']; ?></td>
				</tr>			
			  </tbody>
		  </table>
	  </section>
	  <!--user info table end-->
	</div>

	<div class="col-lg-6">
	  <!--instructor info table start-->
	  <section class="panel">
		  <div class="panel-body">
			  <a class="task-thumb" href="#">
				  <img alt="" height="90" width="90" src="<?php echo $course_transactions_details['instructor_profile_picture']; ?>">
			  </a>
			  <div class="task-thumb-details">
				  <h1><a href="#"><?php echo $course_transactions_details['instructor_name']; ?></a></h1>
				  <p>Instructor</p>
			  </div>
		  </div>
		  <table class="table table-hover personal-task">
			  <tbody>
				<tr>
					<td>
						<strong>Email</strong>
					</td>
					<td><?php echo $course_transactions_details['instructor_email']; ?></td>
					
				</tr>
				<tr>
					<td>
						<strong>Phone</strong>
					</td>
					<td><?php echo $course_transactions_details['instructor_mobile']; ?></td>
				</tr>			
			  </tbody>
		  </table>
	  </section>
	  <!--instructor info table end-->
	</div>
	<div class="clearfix"></div>
	<div class="col-lg-6">
	  <!--instructor info table start-->
	  <section class="panel">
  		  <div class="panel-body">
				  <h3>
				  Paytm Record 
		  			<?php
	  		  			$refund_url = "";
	  		  			if($course_transactions_details['post_transaction_id'] != ""){
					    	$json  = file_get_contents(base_url()."Paytm/TxnStatus.php?MID=DAMSDe47047643996320&ORDERID=".$course_transactions_details['pre_transaction_id']."&SERVER=LIVE");
					    	$array =  json_decode($json);
					    	if(count($array) > 0 ){
					    	}
					    

		  		  			$refund_url = AUTH_PANEL_URL."course_product/course_transactions/refund_amount/".$course_transactions_details['id']."?MID=DAMSDe47047643996320&ORDERID=".$course_transactions_details['pre_transaction_id']."&TXNID=".$course_transactions_details['post_transaction_id']."&REFUNDAMOUNT=".$array->TXNAMOUNT."&SERVER=LIVE";
		  		  			?>
		  		  			<a href="<?php echo $refund_url; ?>"><button class="btn btn-sm btn-info bold pull-right">Refund Amount</button></a>
		  		  			<?php
	  		  			}
  		  			?>
				  
				  </h3>

		  </div>
		  <div class="panel-body">
		  <?php 
		    if($course_transactions_details['course_price']){
		    	$json  = @file_get_contents(base_url()."Paytm/TxnStatus.php?MID=DAMSDe47047643996320&ORDERID=".$course_transactions_details['pre_transaction_id']."&SERVER=LIVE");
		    	$array =  json_decode($json);
		    	if(count($array) > 0 ){
		    		foreach($array	as $keys=>$value ){
    					$out_color = "bg-success";

		    			echo "<div class='col-md-12 clearfix $out_color'><span class='bold'> ".$keys .'</span> -: <span class="pull-right">'.$value."</span></div>";
		    		}
		    	}
		    }
		  	
		  ?>
		  </div>
	  </section>
	  <!--instructor info table end-->
	</div>
	<div class="col-lg-6">
	  <!--instructor info table start-->
	  <section class="panel">
  		  <div class="panel-body">
				  <h3>
				  Paytm Refund Status  
				  </h3>

		  </div>
		  <div class="panel-body">
		  <?php 
		    if($course_transactions_details['course_price']){
		    	$json  = @file_get_contents(base_url()."Paytm/Refund_api_status.php?MID=DAMSDe47047643996320&ORDERID=".$course_transactions_details['pre_transaction_id']."&REFID=".$course_transactions_details['refund_id']."&SERVER=LIVE");
		    	$array =  json_decode($json);

		    	if(count($array) > 0  && array_key_exists('REFUND_LIST', $array)){
		    		$array = $array->REFUND_LIST;

		    		foreach($array	as $a ){//$keys=>$value ){
		    			//echo "<span class='badge bg-warning'> ".$keys .'</span> -: '.$value."</br>";
		    			foreach($a	as $keys=>$value ){
		    				$out_color ="";
		    				if($keys == "TOTALREFUNDAMT"){
		    					$out_color = "bg-success";
		    				}
							echo "<div class='col-md-12 clearfix $out_color '><span class='bold'> ".$keys .'</span> -: <span class="pull-right">'.$value."</span></div>";
		    			}

		    		}
		    	}
		    }
		  	
		  ?>
		  </div>
	  </section>
	  <!--instructor info table end-->
	</div>