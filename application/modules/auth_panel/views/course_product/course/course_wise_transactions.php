

<div class="col-sm-12">
  <section class="panel">
    <header class="panel-heading">
    Course Transaction details
    <span class="tools pull-right">
    <a href="javascript:;" class="fa fa-chevron-down"></a>
    
    </span>
    </header>
    <div class="panel-body">
    <div class="adv-table">       
	<div class="col-md-6 pull-right">
        <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
            <div  class="input-group-addon">From</div>
            <input type="text" id="min-date-course-transaction" class="form-control date-range-filter input-sm course_start_date"  placeholder="">
            <div class="input-group-addon">to</div>
            <input type="text" id="max-date-course-transaction" class="form-control date-range-filter input-sm course_end_date"  placeholder="">
            <div class="input-group-addon btn date-range-filter-clear">Clear</div>
        </div>		
    </div>
    <table  class="display table table-bordered table-striped" id="all-Course-transactions-grid">
      <thead>

        <tr>
        <th>#</th>
        <th>Course Name</th>
        <th>Total Earning</th>
		<th>Instructor Revenue</th>
        <th>Transactions Count</th>
        <th>Action</th>
        </tr>
      </thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
			  <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
			  <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
      
			  <th></th>
          </tr>
      </thead>
    </table>
    </div>
    </div>
  </section>
</div>


<?php
$adminurl = AUTH_PANEL_URL;
$assetsurl_morris_js = AUTH_ASSETS.'assets/morris.js-0.4.3/morris.min.js';
$assetsurl_morris_raphael_js = AUTH_ASSETS.'assets/morris.js-0.4.3/raphael-min.js';
$assetsurl_morris_css = AUTH_ASSETS.'assets/morris.js-0.4.3/morris.css';
$custum_js = <<<EOD

               
               <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {

                       var table = 'all-Course-transactions-grid';
                       var dataTable_transaction = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/Course_wise_transactions/get_ajax_course_all_transactions_list", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_transaction.columns(i).search(v).draw();
                       } );
					 $('.search-input-select').on( 'change', function () {   // for select box
						  var i =$(this).attr('data-column');
						  var v =$(this).val();
						  dataTable_transaction.columns(i).search(v).draw();
					  } );
					  // Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date-course-transaction').val() !="" && $('#max-date-course-transaction').val() != "" ){
                                var dates = $('#min-date-course-transaction').val()+','+$('#max-date-course-transaction').val();
                                dataTable_transaction.columns(9).search(dates).draw();
                            }    
                        }); 

                        $('.date-range-filter-clear').on( 'click', function () { 
                            // clear date filter 
                            $('#min-date-course-transaction').val('');
                            $('#max-date-course-transaction').val("");
                            dataTable_transaction.columns(9).search('').draw();
                        });                         
                   } );
				   
				   $('#min-date-course-transaction').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true
						
					});
					$('#max-date-course-transaction').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true
						
					});
               </script>       

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
