<?php
$period = $this->input->get('period');

$today = date('d-m-Y');
$currentWeekDate = date('d-m-Y', strtotime('-7 days', strtotime($today)));
$currentMonth = date('m');
$currentYear = date('Y');
$current_millisecond = strtotime(date('01-m-Y 00:00:00'))*1000;
if($period == "today"){
				$where = " WHERE DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y')= '$today' ";
				$sql = "SELECT sum(`instructor_share`) as total  FROM `course_transaction_record` where DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y') = '$today' and transaction_status =1" ;
				$total_revenue =  $this->db->query($sql)->row()->total;

				$sql = "SELECT sum(course_price) as total FROM `course_transaction_record` where DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y') = '$today'  and transaction_status = 1";
				$total_sale =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(id) as total FROM `course_transaction_record` WHERE  transaction_status =1 and course_price > 0 and   DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y') = '$today'";
				$total_transactions =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(`id`) as total FROM `course_user_rating` $where";
				$total_review =  $this->db->query($sql)->row()->total;



			}elseif($period == "yesterday"){
			    $yesterday = date('d-m-Y', strtotime($today. ' - 1 days'));
				$where = " WHERE DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y')= '$yesterday' ";


				$sql = "SELECT sum(`instructor_share`) as total  FROM `course_transaction_record` $where and transaction_status =1" ;
				$total_revenue =  $this->db->query($sql)->row()->total;

				$sql = "SELECT sum(course_price) as total FROM `course_transaction_record` $where and transaction_status = 1";
				$total_sale =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(id) as total FROM `course_transaction_record` $where AND  transaction_status =1 and course_price > 0";
				$total_transactions =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(`id`) as total FROM `course_user_rating` $where ";
				$total_review =  $this->db->query($sql)->row()->total;
			}elseif($period == "7days"){
				$week = strtotime("-1 week")."000";
				$where = " WHERE creation_time >=  $week ";

				$sql = "SELECT sum(`instructor_share`) as total  FROM `course_transaction_record` $where and transaction_status =1" ;
				$total_revenue =  $this->db->query($sql)->row()->total;

				$sql = "SELECT sum(course_price) as total FROM `course_transaction_record` $where AND  transaction_status =1";
				$total_sale =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(id) as total FROM `course_transaction_record` $where and course_price > 0";
				$total_transactions =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(`id`) as total FROM `course_user_rating` $where ";
				$total_review =  $this->db->query($sql)->row()->total;
			}elseif($period == "current_month"){
				$current_month = date('m-Y');
				$where = " WHERE DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%m-%Y') = '$current_month' ";

				$sql = "SELECT sum(`instructor_share`) as total  FROM `course_transaction_record` $where  and transaction_status =1" ;
				$total_revenue =  $this->db->query($sql)->row()->total;

				$sql = "SELECT sum(course_price) as total FROM `course_transaction_record` $where  and transaction_status = 1  ";
				$total_sale =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(id) as total FROM `course_transaction_record` $where AND  transaction_status =1 and course_price > 0";
				$total_transactions =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(`id`) as total FROM `course_user_rating` $where ";
				$total_review =  $this->db->query($sql)->row()->total;
			}elseif($period == "all" || $period == "" || $period == "custom"){
				$sql = "SELECT sum(`instructor_share`) as total  FROM `course_transaction_record` where transaction_status =1 " ;
				$total_revenue =  $this->db->query($sql)->row()->total;

				$sql = "SELECT sum(course_price) as total FROM `course_transaction_record` where transaction_status = 1";
				$total_sale =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(id) as total FROM `course_transaction_record` WHERE transaction_status =1 and course_price > 0 ";
				$total_transactions =  $this->db->query($sql)->row()->total;

				$sql = "SELECT count(`id`) as total FROM `course_user_rating` ";
				$total_review =  $this->db->query($sql)->row()->total;
			}


?>
<?php if($period != "custom"){ ?>
<div class=" state-overview">
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel">
                          <a href="<?php echo AUTH_PANEL_URL."course_product/course/all_courses_reviews_list?period=$period";?>" >
                          <div class="symbol terques">
                              <i class="fa fa-user"></i>
                          </div>
                          <div class="value">
                              <h1 class="count"><?php  echo $total_review; ?></h1>
                              <p>Reviews</p>
                          </div>
                          </a>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel">
                          <div class="symbol red">
                              <i class="fa fa-tags"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count2"><?php  echo $total_transactions; ?></h1>
                              <p>Paid Transactions</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel">
                          <div class="symbol yellow">
                              <i class="fa fa-shopping-cart"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count3"><?php  echo $total_sale; ?></h1>
                              <p>Sale amount</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel">
                          <div class="symbol blue">
                              <i class="fa fa-bar-chart-o"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count4"><?php  echo $total_revenue; ?></h1>
                              <p>Revenue</p>
                          </div>
                      </section>
                  </div>
              </div>

<?php } ?>
<div class="col-sm-12">
  <section class="panel">
    <header class="panel-heading">
    All Course(s) Transactions
    <span class="tools pull-right">
		 <form role="form" method="get" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  >
		  <select name="period" class="form-control input-sm m-bot15 period" onchange="this.form.submit()">
          <option <?php if($period == "today"){ echo "selected";} ?> value="today">Today</option>
          <option <?php if($period == "yesterday"){ echo "selected";} ?>  value="yesterday">Yesterday</option>
          <option <?php if($period == "7days"){ echo "selected";} ?>  value="7days">Last 7 Days</option>
          <option <?php if($period == "current_month"){ echo "selected";} ?>  value="current_month">Current Month</option>
          <option <?php if($period == "custom"){ echo "selected";} ?>  value="custom">Custom</option>
          <option <?php if($period == "" || $period == "all"){ echo "selected";} ?>  value="all">All</option>
		  </select>
      <?php
        foreach($_GET as $key=>$value){
          if($key != 'period'){
            echo "<input type='hidden' value='".$value."' name='".$key."'>";
          }        
        }
      ?>
		</form>
    </span>
    <!-- download csv  -->
    <span class="tools pull-right">
      <form id="download_content_csv" method="post" action=""  >
        
        <button class="btn btn-sm btn-danger margin-right bold"> 
          <i class="fa fa-file" aria-hidden="true"></i>
          Download csv 
        </button>
        <input name="download_pdf" class="btn btn-info btn-sm  margin-right bold" value="Download PDF" type="submit">
        <textarea style="display:none;" name="input_json"></textarea>
      </form>
    </span>

    </header>
    <div class="clearfix"></div>
    <div class="panel-body">
    <div class="adv-table">
		<?php if($period == "custom" ){  ?>
  	<div class="col-md-6 pull-right custom_search" >
          <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
              <div  class="input-group-addon">From</div>
              <input type="text" id="min-date-course-transaction" class="form-control date-range-filter input-sm course_start_date"  placeholder="">
              <div class="input-group-addon">to</div>
              <input type="text" id="max-date-course-transaction" class="form-control date-range-filter input-sm course_end_date"  placeholder="">
              <div class="input-group-addon btn date-range-filter-clear">Clear</div>
          </div>
      </div>
		<?php } ?>

    <div class="col-md-12 custom_filter_label  margin-top  margin-bottom" >

    </div>

    <table  class="display table table-bordered table-striped " id="all-Course-transactions-grid">
      <thead>

        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Course </th> 		    
          <th>Pay Via</th>
  		    <th>Coupon </th>
          <th>Ins. Name</th>
          <th>Status</th>
          <th>On</th>
          <th>Type</th>
          <th>Points</th>
          <th>Price</th>
          <th>Gst</th>
          <th>Net Amt.</th>
          <th>Ins. Share</th>
          <th>Profit</th>
          <th>Action</th>
        </tr>
      </thead>
      <thead>
          <tr>
            <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="2"  value="<?php echo urldecode($this->input->get('course_name'));?>" class="search-input-text form-control course_name_search"></th>
            <th></th>
            <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="5" value="<?php echo urldecode($this->input->get('instructor_name'));?>" class="search-input-text form-control"></th>
            <th>
              <select data-column="6"  class="form-control search-input-select">
              <option value="" >(All)</option>
              <option value="0" <?php echo ($this->input->get('status') == 'pending')?'selected':'';?> >Pending</option>
              <option value="1" <?php echo ($this->input->get('status') == 'complete')?'selected':'';?> >Complete</option>
              <option value="2" <?php echo ($this->input->get('status') == 'cancel')?'selected':'';?> >Cancel</option>
              <option value="3" <?php echo ($this->input->get('status') == 'refund_req')?'selected':'';?> >Refund Req.</option>
              <option value="4" <?php echo ($this->input->get('status') == 'refunded')?'selected':'';?> >Refunded</option>
              </select>
            </th>
            <th><input type="text" data-column="7"    class="search-input-text instructor_name_search form-control"></th>
            <th>
              <select data-column="8"  class="form-control search-input-select">
                <option value="">(All)</option>
                <option selected="selected" value="1">Paid</option>
                <option value="2">Free</option>
              </select>
            <th></th>
            <th></th>
            <th> </th>
            <th><input type="text" data-column="12"  class="search-input-text form-control"></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
      </thead>
    </table>
    </div>
    </div>
  </section>
</div>
<?php
$yearly_sql = "SELECT
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '01' THEN 1 ELSE 0 END) AS 'January',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '02' THEN 1 ELSE 0 END) AS 'February',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '03' THEN 1 ELSE 0 END) AS 'March',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '04' THEN 1 ELSE 0 END) AS 'April',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '05' THEN 1 ELSE 0 END) AS 'May',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '06' THEN 1 ELSE 0 END) AS 'June',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '07' THEN 1 ELSE 0 END) AS 'July',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '08' THEN 1 ELSE 0 END) AS 'August',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '09' THEN 1 ELSE 0 END) AS 'September',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '10' THEN 1 ELSE 0 END) AS 'October',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '11' THEN 1 ELSE 0 END) AS 'November',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%m') WHEN '12' THEN 1 ELSE 0 END) AS 'December',
              SUM(CASE DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%Y') WHEN  ".date('Y')." THEN 1 ELSE 0 END) AS 'TOTAL'
              FROM course_transaction_record
              Where transaction_status =1
              And DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%Y')  = '".date('Y')."'";
              $out_yearly = $this->db->query($yearly_sql)->row();
              $out_yearly_js ="['JAN',  ".$out_yearly->January."  ],
              ['FEB',  ".$out_yearly->February."  ],
              ['MAR',  ".$out_yearly->March."   ],
              ['APR',  ".$out_yearly->April." ],
              ['MAY',  ".$out_yearly->May."  ],
              ['JUN',  ".$out_yearly->June."  ],
              ['JUL',  ".$out_yearly->July."   ],
              ['AUG',  ".$out_yearly->August." ],
              ['SEP',  ".$out_yearly->September."  ],
              ['OCT',  ".$out_yearly->October."  ],
              ['NOV',  ".$out_yearly->November."   ],
              ['DEC',  ".$out_yearly->December." ] ";
              $a = $out_yearly->TOTAL;
              $b = str_split($a);
              $max_chunk = (end($b) != 0)?((10-end($b))+$a):$a;
              ?>

<div class="col-sm-12">
  <section class="panel">
    <div class="panel-body">
        <!--custom chart start-->
        <div class="border-head">
            <h3>Earning Graph</h3>
        </div>

        <div id="earning_div" style="height:280px"></div>
    </section>
</div>

<div class="col-sm-4 hide">
  <section class="panel">
    <div class="panel-body">
        <!--custom chart start-->
        <div class="border-head">
            <h3>Earning Graph</h3>
        </div>
        <div class="panel-body">
            <div id="hero-donut" class="graph"></div>
        </div>
    </section>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$assetsurl_morris_js = AUTH_ASSETS.'assets/morris.js-0.4.3/morris.min.js';
$assetsurl_morris_raphael_js = AUTH_ASSETS.'assets/morris.js-0.4.3/raphael-min.js';
$assetsurl_morris_css = AUTH_ASSETS.'assets/morris.js-0.4.3/morris.css';
$custum_js = <<<EOD
<script>
$('#container').addClass('sidebar-closed');
$('#main-content').css('margin-left',"0px");
</script>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                    ['month', 'Transactions' ],
                    $out_yearly_js
                    ]);

                    var options = {
                    title: 'Transaction',
                    hAxis: {title: 'month',  titleTextStyle: {color: '#333'}},
                    vAxis: {minValue: 0}
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('earning_div'));
                    chart.draw(data, options);
                }
                </script>

                <link href="$assetsurl_morris_css" rel="stylesheet" />
                <script src="$assetsurl_morris_js" type="text/javascript"></script>
                <script src="$assetsurl_morris_raphael_js" type="text/javascript"></script>
                <script>
                 $(function () {
                    Morris.Donut({
                        element: 'hero-donut',
                        data: [
                        {label: 'Dams', value: 25 },
                        {label: 'Normal', value: 40 }
                        ],
                        colors: ['#41cac0', '#49e2d7', '#34a39b'],
                        formatter: function (y) { return y + "%" }
                    });
                 });
                </script>
               <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {

                      var get_all_record = "$adminurl"+"course_product/course_transactions/get_ajax_course_transactions_list/?period=$period";

                      var get_all_record_forcsv = "$adminurl"+"course_product/course_transactions/get_ajax_course_transactions_download/?period=$period";

                       var table = 'all-Course-transactions-grid';
                       var dataTable_transaction = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "pageLength": 50,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url : get_all_record , // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_transaction.columns(i).search(v).draw();
                       } ).click();
            					 $('.search-input-select').on( 'change', function () {   // for select box
            						  var i =$(this).attr('data-column');
            						  var v =$(this).val();
            						  dataTable_transaction.columns(i).search(v).draw();
            					  } ).change();
					             // Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date-course-transaction').val() !="" && $('#max-date-course-transaction').val() != "" ){
                                var dates = $('#min-date-course-transaction').val()+','+$('#max-date-course-transaction').val();
                                dataTable_transaction.columns(9).search(dates).draw();
                            }
                        });

                        $('.date-range-filter-clear').on( 'click', function () {
                            // clear date filter
                            $('#min-date-course-transaction').val('');
                            $('#max-date-course-transaction').val("");
                            dataTable_transaction.columns(9).search('').draw();
                        });
                    /* 
                    * speacial magic function after getting result from server 
                    */ 

                    $( document ).ajaxComplete(function( event, xhr, settings ) {
                          if ( settings.url == get_all_record ) {
                            var obj = jQuery.parseJSON(xhr.responseText);
                            var read =  obj.client_filters;
                             var target_class = '.custom_filter_label';
                             $('.custom_filter_label').html('');
                             if(read.course_name && read.course_name != "" ){
                               console.log(read.course_name);
                               $(target_class).append('<span onclick="console.log($(\'.course_name_search\').val(\'\').click())" class="label label-primary margin-right"><i class="fa fa-book"></i> '+read.course_name+' <i class="fa fa-times"></i></span>');
                             }

                             if(read.instructor_name && read.instructor_name != "" ){
                               console.log(read.instructor_name);
                               $(target_class).append('<span onclick="console.log($(\'.instructor_name_search\').val(\'\').click())" class="label label-default margin-right "><i class="fa fa-user"></i> '+read.instructor_name+' <i class="fa fa-times"></i></span>');
                             }
                          }
                          if( settings.url == get_all_record ) {
                              var obj = jQuery.parseJSON(xhr.responseText);
                              var read =  obj.posted_data;
                              console.log(read);
                              $('#download_content_csv').attr('action',get_all_record_forcsv);
                              $('textarea[name=input_json]').val("");
                              $('textarea[name=input_json]').val(JSON.stringify(read));
                                                        
                          }
                        });

                   } );


                        /* custum date filter */
                        $('#min-date-course-transaction').datepicker({
                          format: 'dd-mm-yyyy',
                          autoclose: true
                        });
                        $('#max-date-course-transaction').datepicker({
                          format: 'dd-mm-yyyy',
                          autoclose: true
                        });

                        $('.period').on( 'change', function () {
                          period = $(this).val();
                          if(period == "custom"){
                            $('.custom_search').show();
                          }
                        });
               </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
