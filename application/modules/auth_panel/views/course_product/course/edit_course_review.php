
<div class="col-md-9 no-padding">
<div id="tabContent1" class="col-lg-12 tabu">
      <section class="panel">
          <header class="panel-heading">
              Edit Course Review
          </header>
          <div class="panel-body">
              <form role="form" method="post" action="<?php echo AUTH_PANEL_URL.'course_product/course/edit_course_review/'.$course_review_detail['id']; ?>"  >
				  <input type="hidden"  name = "course_fk_id" id="course_fk_id" value="<?php echo $course_review_detail['course_fk_id']; ?>" class="form-control input-sm">
				  <input type="hidden"  name = "id" id="id" value="<?php echo $course_review_detail['id']; ?>" class="form-control input-sm">
				 <div class="form-group col-md-6">
					  <label >User name</label>
					  <input type="text" readonly placeholder="Enter user name" name = "name" id="name" value="<?php echo $course_review_detail['name']; ?>" class="form-control input-sm">
					   
				  </div>
				   <div class="form-group col-md-6 ">
					  <label >Rating</label>
					  <input type="text" placeholder="Add rating" name = "rating" id="rating" value="<?php echo $course_review_detail['rating']; ?>" class="form-control input-sm">
					   <span class="error bold"><?php echo form_error('rating');?></span>
				  </div>
				   <div class="form-group col-md-6 ">
					  <label>Review</label>
					  <input type="text" placeholder="Review" name = "text" id="text" value="<?php echo $course_review_detail['text']; ?>" class="form-control input-sm">
					   <span class="error bold"><?php echo form_error('text');?></span>
				  </div>
				  <div class="form-group col-md-6 ">
					  <label>Created at</label>
					  <input type="text" readonly placeholder="Creation time" name = "creation_time" id="creation_time" value="<?php echo $course_review_detail['creation_time']; ?>" class="form-control input-sm">					   
				  </div>
				  
			  <div class="form-group col-md-6">    
						<input class="btn btn-success btn-sm" name="update_course_review" value="Save"  type="submit" >
			  </div>
			  <div class="form-group col-md-6 align-right ">    
				    <input class="btn btn-info btn-sm pull-right" value="Back" onclick="window.history.go(-1); return false;" type="button" >
         	 </div>
			  </form>

          </div>
      </section>
  </div>
	</div>