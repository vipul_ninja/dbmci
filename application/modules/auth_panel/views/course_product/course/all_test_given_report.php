   <div id="tabContent8" class="col-md-12 tabu ">
        <section class="panel">
            <header class="panel-heading">
                All Test Taken Reports
            </header>
            <div class="panel-body ">
                <div class="adv-table">
                    <div class="col-md-6 pull-right">
                        <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
                        <div  class="input-group-addon">From</div>
                        <input type="text" id="min-date" class="form-control date-range-filter input-sm course_start_date"  placeholder="">

                        <div class="input-group-addon">to</div>

                        <input type="text" id="max-date" class="form-control date-range-filter input-sm course_end_date"  placeholder="">

                        </div>		
                    </div>
                    <table  class="display table table-bordered table-striped col-md-12" id="all-course-review-grid" style="width:100%">
                        <thead>
                            <tr>
                              <th>id</th>
                              <th>Test name</th>
                              <th>User Name </th>                               
                              <th>Time Spent  </th>
                              <th>Test time </th>
                              <th>Rewards Points </th>
                              <th>Time</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
							                 	<th><input type="text" data-column="4"  class="search-input-text form-control"></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div> 

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-course-review-grid';
                       var dataTable_review = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "pageLength": 100,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/course/ajax_all_test_given_report", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );

                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_review.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable_review.columns(i).search(v).draw();
                        } ); 
                        // Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date').val() !="" && $('#max-date').val() != "" ){
                                var dates = $('#min-date').val()+','+$('#max-date').val();
                                dataTable_review.columns(4).search(dates).draw();
                            }    
                        });                      
                   } );
				   
				   
				   $('#min-date').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true
						
					});
					$('#max-date').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true
						
					});
               </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>