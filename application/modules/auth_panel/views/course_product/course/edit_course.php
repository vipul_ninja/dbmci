<?php
    $c_id         = $this->input->get('course_id');
    $main_stream_result = $this->db->where('status',0)->get('`course_stream_name_master`')->result_array();
    $main_stream_result_json = json_encode($main_stream_result);
    $get_save_type      = $this->db->get('`course_type_master`')->result_array();
    $type_ids      = $this->db->select('GROUP_CONCAT(type_id) as type_ids')->where('course_id',$c_id)->get('course_type_relationship')->row()->type_ids;
    $get_content_type      = $this->db->get('`course_content_type`')->result_array();
    $type_ids_content      = $this->db->select('GROUP_CONCAT(content_type_id) as content_type_id')->where('course_id',$c_id)->get('course_content_type_relationship')->row()->content_type_id;
    

    // subject data in json 
    $subject_data_json = json_encode($subject_data);
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    var test_data="<?=$course_data['course_for']?>";
    if(test_data==0){
        $('#course_group_category').removeClass('hide');
    }
    if(test_data==2){
        $('#course_group_category').removeClass('hide');
    }
    if(test_data==1){
        $('#course_group_category').addClass('hide');
    }
    $("input[name='course_for']").click(function(){
        var course_for = $(this).val();
        console.log(course_for);
        if(course_for == 2){
            //console.log(2);
            $('#course_group_category').removeClass('hide');
          $("[name='course_group_type[]']").prop("required", true);
        }else if(course_for== 0){
            //console.log(0);
            $('#course_group_category').removeClass('hide');
            $("[name='course_group_type[]']").prop("required", true);
        }else{
            //console.log(1);
            $('#course_group_category').addClass('hide');
            $("[name='course_group_type[]']").prop("required", false);
        }
    });
});
</script>
<div class="col-md-3">
    <section class="panel">
    <header class="panel-heading">
        Course menu
        <?php if($course_data['is_new'] == 1 ){ ?>
            <sup class="bold text-danger popovers" aria-hidden="true" title=""  data-trigger="hover" data-placement="bottom" data-content="This course is saved with 'NEW' tag  you can change it from 'Basic information' tab." data-original-title="Note" >New</sup>
        <?php  } ?>
        <span>
        <?php $rating = $course_data['course_rating_count'];
            $go = 0;
            for($i=0;$i<$rating;$i++){
                echo '<i class="fa fa-star text-danger" aria-hidden="true"></i> ';
                $go++;
            }
            if(($rest = 5 - $go) > 0 ){

                for($i=0;$i<$rest;$i++){
                    echo '<i class="fa fa-star-o text-danger" aria-hidden="true"></i> ';
                }
            }
         ?>
        </span>
    </header>

    <div class="panel-body">
        <ul class="nav prod-cat">
            <li><a href="javascript:void(0)" data-div="1" ><i class=" fa fa-angle-right"></i> Basic information</a></li>
            <li><a href="javascript:void(0)" data-div="2" ><i class=" fa fa-angle-right"></i> Cover & Header Image </a></li>
            <li><a href="javascript:void(0)" data-div="3" ><i class=" fa fa-angle-right"></i> Cover Video </a></li>
            <li class="hide"><a href="javascript:void(0)" data-div="4" ><i class=" fa fa-angle-right"></i> Price managment </a></li>
            <li><a href="javascript:void(0)" data-div="7" ><i class=" fa fa-angle-right"></i> Topic managment </a></li>
            <li class="hide" ><a href="javascript:void(0)" data-div="10" ><i class=" fa fa-angle-right"></i> Filtration </a></li>
            <li><a href="javascript:void(0)" data-div="6" ><i class=" fa fa-angle-right"></i> Coupon </a></li>
            <!-- <li><a href="javascript:void(0)" data-div="8" ><i class=" fa fa-angle-right"></i> Reviews </a></li> -->
            <li><a href="javascript:void(0)" data-div="9" ><i class=" fa fa-angle-right"></i> Related Course(s) </a></li>
            <li class="hide" ><a href="javascript:void(0)" data-div="11" ><i class=" fa fa-angle-right"></i> Send push notification(s) </a></li>
        </ul>
    </div>

        <div class="panel-body">
        <a  href="<?php echo AUTH_PANEL_URL."course_product/course/delete_course/".$c_id;?>" onclick="return confirm('Are you really want to delete this course ?');" ><button class="btn btn-xs btn-danger bold pull-left">Delete</button> </a>
		<?php $user_data = $this->session->userdata('active_user_data');
		$instructor_id = $user_data->instructor_id;

		if($instructor_id != 0){
			if($course_data['publish'] == 0 ){?>
			 <a href="<?php echo AUTH_PANEL_URL."course_product/course/request_course_publish/".$c_id;?>"><button class="btn btn-xs btn-success pull-right bold">Request Publish</button></a>
        <?php  }else if($course_data['publish'] == 2){ ?>
            <a href="<?php echo AUTH_PANEL_URL."course_product/course/set_course_unpublish/".$c_id;?>"><button class="btn btn-xs btn-info pull-right bold">Requested|Set Unpublish</button></a>
        <?php }
			else{ ?>
            <a href="<?php echo AUTH_PANEL_URL."course_product/course/set_course_unpublish/".$c_id;?>"><button class="btn btn-xs btn-danger pull-right bold">Unpublish</button></a>
        <?php } ?>


	<?php } else{ ?>

        <?php if($course_data['publish'] == 2 ){ ?>
            <a href="<?php echo AUTH_PANEL_URL."course_product/course/set_course_publish/".$c_id;?>"><button class="btn btn-xs btn-success pull-right bold">Accept Publish Request</button></a>
        <?php  }else if($course_data['publish'] == 1){ ?>
            <a href="<?php echo AUTH_PANEL_URL."course_product/course/set_course_unpublish/".$c_id;?>"><button class="btn btn-xs btn-danger pull-right bold">Unpublish</button></a>
        <?php }else { ?>
			 <a href="<?php echo AUTH_PANEL_URL."course_product/course/set_course_publish/".$c_id;?>"><button class="btn btn-xs btn-success pull-right bold">Publish </button></a>

			<?php	}}?>
    </div>
    </section>
</div>

<div class="col-md-9 no-padding">
  <?php
  //error message if image not exist
  if($this->session->flashdata('image_not_exist')){
    echo '<div class="col-md-12">
            <div class="alert alert-danger alert-dismissable">
            <header class="bold"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>ERROR ! </header>'.$this->session->flashdata('image_not_exist').'</div></div>';
  }
  ?>
  <div id="tabContent1" class="col-lg-12 tabu">
      <section class="panel">
          <header class="panel-heading">
              Edit Course
          </header>
          <div class="panel-body">
              <form autocomplete="off" id="edit_basicinfo_from" method="POST" action="<?php echo AUTH_PANEL_URL."course_product/course/edit_course_basic_info?course_id=$c_id"?>" onsubmit="return validate();">
                  <div class="form-group col-md-12">
                      <label for="exampleInputEmail1">Course Title</label>
                      <input type="text" placeholder="" name="title" value="<?php echo $course_data['title']; ?>" class="form-control input-sm required">
                  </div>
                  <div class="form-group col-md-12">
                      <label for="exampleInputEmail1">Course Attribute</label>
                      <input type="text" placeholder="" name="course_attribute" value="<?php echo $course_data['course_attribute'] ?>" class="form-control input-sm required">
                  </div>
                  <div class="form-group col-md-4 ">
                      <label for="exampleInputEmail1">Course Category</label>
                      <select name="course_main_fk"  class="form-control input-sm course_element_select ">

                      </select>
                  </div>

                  <div class="form-group col-md-4 ">
                      <label for="exampleInputEmail1">Course Stream</label>
                      <select name="course_category_fk"  class="form-control input-sm stream_element_select ">
                      </select>
                  </div>


                  <div class="form-group col-md-4 hide">
                      <label for="exampleInputEmail1">Subject</label>
                      <select name="subject_id"  class="form-control input-sm subject_element_select ">
                      </select>
                  </div>
                  <div class="form-group col-md-6 hide">
                      <label for="exampleInputEmail1">Topic</label>
                      <select  class="form-control input-sm topic_element_select">
                      </select>
                  </div>
        				  <div class="col-md-12 pull-right hide ">
          					<div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
          						<div  class="input-group-addon">From</div>
          						<input type="text" id="min-date-course" name ="start_date" value="<?php echo (strtotime($course_data['start_date'])  <= 0 )?"":date("d-m-Y", strtotime($course_data['start_date']));  ?>" class="form-control date-range-filter input-sm course_start_date"  placeholder="">
          						<div class="input-group-addon">to</div>
          						<input type="text" id="max-date-course" name ="end_date" value="<?php echo (strtotime($course_data['end_date'])  <= 0 )?"":date("d-m-Y", strtotime($course_data['end_date'])); ?>" class="form-control date-range-filter input-sm course_end_date"  placeholder="">

          					</div>
        				</div>

                  <div class="form-group col-md-12 ">
                      <label for="exampleInputEmail1">Description</label>
                      <textarea name="description" id="description" placeholder="" class="form-control input-sm  "><?php echo $course_data['description']; ?></textarea>
                  </div>


                  <div class="form-group col-md-12 ">
                  <span class="label label-info label-">Note-:</span> <small class="bold">If you want to make this course free please insert 0 i.e. zero in MRP input box. </small>
                   </div>
                  <div class="form-group col-md-4">
                      <label for="exampleInputEmail1">MRP (For -: INDIA ) </label>
                      <input name="mrp" value="<?php echo $course_data['mrp']; ?>" type="text" placeholder=""   onkeypress="return validate_data_keypress(event)" class="form-control input-sm required">
                  </div>
                  <div class="form-group col-md-4 hide">
                      <label for="exampleInputEmail1">Offer Price</label>
                      <input type="text" placeholder="" name="course_sp" value="<?php echo $course_data['course_sp'] ?>"  onkeypress="return validate_data_keypress(event)"  class="form-control input-sm required">
                  </div>
                  <div class="form-group col-md-4">
                    <div class="col-md-6 no-padding">
                      <label for="exampleInputEmail1">(for dbmci)</label>
                      <input type="text" name="for_dbmci" value="<?php echo $course_data['for_dbmci']; ?>"  placeholder="" class="form-control col-md-2 input-sm for_dams_input required">
                    </div>
                    <div class="col-md-6 no-padding hide">
                      <label for="exampleInputEmail1">(%)</label>
                      <input type="text"  name="for_dams_percent"  placeholder="" class="form-control col-md-2  input-sm for_dams_input">
                    </div>
                  </div>

                  <div class="form-group col-md-4">
                    <div class="col-md-6 no-padding">
                      <label for="exampleInputEmail1">(for non dbmci)</label>
                     <input  name="non_dbmci" type="text" value="<?php echo $course_data['non_dbmci']; ?>"  placeholder="" class="form-control input-sm for_dams_input required ">
                    </div>
                    <div class="col-md-6 no-padding hide">
                      <label for="exampleInputEmail1">(%)</label>
                       <input type="text"  name="non_dams_percent" placeholder="" class="form-control col-md-2  input-sm for_dams_input ">
                    </div>
                  </div>

                  <div class="form-group col-md-12 " >
                      <label for="exampleInputEmail1">Instructor</label>
                      <select required name="instructor_id" class="form-control input-sm instructor_element_select  ">
                      </select>
                  </div>
                  <div class="col-md-12 hide" id="instructor_profile" >

                  </div>

                  <div class="form-group col-md-12">
                      <label for="exampleInputEmail1">GST</label>
                      <label class="checkbox-inline">
                        <input <?php if($course_data['gst_include'] == 0){ echo "checked";}  ?>  type="radio" value="0" name="gst_include" id="inlineCheckbox1" class="required"> Include
                        </label>
                        <label class="checkbox-inline">
                            <input <?php if($course_data['gst_include'] == 1){ echo "checked";}  ?> type="radio" value="1" name="gst_include"  class="required"  id="inlineCheckbox1"> Exclude
                        </label>
                  </div>
                  <?php
                  $user_data = $this->session->userdata('active_user_data');
                  $instructor_id = $user_data->instructor_id;
                  $backend_user_id = $user_data->id;
                  $where = "";
                  if($instructor_id != 0){
                  	$disable_status = "disabled";
                  }
                  else{
                  	$disable_status = "";
                  }
                    ?>
                  <div class="form-group col-md-12 hide">
                      <label for="exampleInputEmail1">Instructor Sharing(%) </label>
                       <input <?php echo $disable_status; ?>  name="instructor_share" value="<?php echo $course_data['instructor_share']; ?>"  type="text" placeholder="" class="form-control input-sm ">
                  </div>

                  <div class="form-group col-md-12 ">
                      <label for="exampleInputEmail1">Search Tags</label>
                      <textarea   name="tags" class="form-control input-sm  "></textarea>
                  </div>
                    <div class="checkboxes col-md-12">
                      <label for="checkbox-01" class="">
                          <input type="checkbox" <?php if($course_data['is_new']  ==1 ){echo 'checked="checked"';}?>  value="1" id="checkbox-01" name="is_new"> Set this course as new
                      </label>
                    </div>

                    <div class="checkboxes col-md-12">
                      <label for="checkbox-02" class="">
                          <input type="checkbox" <?php if($course_data['is_locked']  == 1 ){echo 'checked="checked"';}?>  value="1" id="checkbox-02" name="is_locked"> Course have locked content
                      </label>
                    </div>

				          <div class="form-group col-md-12 ">
                          

                    <label for="exampleInputEmail1">Course For</label>
                    <label class="radio-inline">
                      <input  <?=($course_data['course_for']==0)?"checked":""?> type="radio" value="0" name="course_for" > Dbmci
                      </label>
                      <label class="radio-inline">
                          <input  <?=($course_data['course_for']==1)?"checked":""?>  type="radio" value="1" name="course_for"  > Non Dbmci
                      </label>
					             <label class="radio-inline">
                          <input  <?=($course_data['course_for']==2)?"checked":""?>  type="radio" value="2" name="course_for" > Both
                      </label>
                  </div>
                 
                   <!-- <div class="checkboxes col-md-12">
                      <label for="checkbox-01" class="">
                          <input type="checkbox" <?php if($course_data['in_suggested_list']  ==1 ){echo 'checked="checked"';}?>  value="1" name="in_suggested_list"> Course in suggessted list
                      </label>
                    </div> -->
                    <div class="form-group col-md-12 hide">
                      <label for="exampleInputEmail1">Left Text</label>
                      <input type="text" placeholder="" name="left_text" value="<?php echo $course_data['left_text']?>" class="form-control input-sm ">
                  </div>
                  <div class="form-group col-md-12 hide">
                      <label for="exampleInputEmail1 ">Right text</label>
                      <input type="text" placeholder="" name="right_text" value="<?php echo $course_data['right_text']?>" class="form-control input-sm ">
                  </div>
                    <div class=" col-md-4 hide">
                      <label for="exampleInputEmail1 ">Fake Learner</label>
                      <input name="fake_learner" class="form-control input-sm  "/>
                    </div>

                    <div class=" col-md-12 hide">
                      <label for="exampleInputEmail1">Please provide user ids to whom you want to show this course free at course view page (you can get user_id from user profile page for multiple please provide comma seprated ids like 1,2,3,788 etc ) </label>
                      <input name="free_ids" value="<?php echo $course_data['free_ids'];?>" class="form-control input-sm "/>
                    </div>

                    <div class=" col-md-12">
                      <label for="exampleInputEmail1">Course Validity  </label>
                      <input name="validity" value="<?php echo $course_data['validity'];?>" class="form-control input-sm "/>
                    </div>
                    <div class="col-md-12 hide" id="course_group_category">
                        <label for="exampleInputEmail1">Course Group Type  </label><br/>
                        <?php 
                        $course_group_type_selected=explode(',',$course_data['course_group_type']);
                        foreach($course_group as $group){?>
                            <div class="col-md-3 checkbox-inline">                                           
                                <input <?=in_array($group['id'],$course_group_type_selected)?'checked':''?> type="checkbox" value="<?=$group['id']?>" name="course_group_type[]"> <?=$group['name']?>
                            </div>                  
                        <?php }?>
                    </div><br>

                   <div class="form-group col-md-12 " style="padding-top:5px">
                     <button class="btn btn-info btn-sm pull-right" type="submit">Submit</button>
                   </div>

            </form>

          </div>
      </section>
  </div>

    <div id="tabContent2" class="col-md-12 tabu ">
      <section class="panel">
          <header class="panel-heading">
              Course Cover
          </header>
          <div class="panel-body">
          <div class="row">
          <div class="col-md-6">
              <form role="form" action="<?php echo AUTH_PANEL_URL."course_product/course/update_cover_image";?>" method="POST" enctype="multipart/form-data" >
                  <div class="cover_image_element"></div>
                  <div class="form-group">
                      <label for="exampleInputFile">Upload Image
                      <small> -: For best View please upload image with height of 300px and width of 300px </small>
                      </label>
                      <input name="cover_image" type="file" id="exampleInputFile" required>
                      <p class="help-block"> </p>
                  </div>
                 
                  <input type="hidden" name="course_id" value="<?php echo $this->input->get('course_id');?>">
                  <button class="btn btn-info btn-sm pull-right" type="submit">Submit</button>
              </form>
              </div>
              <div class="col-md-6">
              <form role="form" action="<?php echo AUTH_PANEL_URL."course_product/course/update_color";?>" method="POST" enctype="multipart/form-data" >
              <div class="col-md-12">
                  <div class="form-group" >
                    <label for="name">Color Picker</label>
                    <input type="color" class="form-control input-sm" value="<?php echo  $course_data['color_code']; ?>" id="color" name="color" placeholder="Enter color code" required>
                </div>
                </div>
                <div class="col-md-12">
                  <input type="hidden" name="course_id" value="<?php echo $this->input->get('course_id');?>">
                  <button class="btn btn-info btn-sm pull-right" type="submit">Submit</button>
                  </div>
              </form>
              </div>
              </div>
          </div>
      </section>
		  <section class="panel">
          <header class="panel-heading">
              Course Description  Wall Image
          </header>
          <div class="panel-body">
          <div class="col-md-6">
              <form role="form" action="<?php echo AUTH_PANEL_URL."course_product/course/update_desc_header_image";?>" method="POST" enctype="multipart/form-data" >
                  <div class="desc_header_image_element"></div>
                  <div class="form-group">
                      <label for="exampleInputFile">Upload Image
                      <small> -: For best View please upload image with height of 320px and width of 640px </small></label>
                      <input name="desc_header_image" type="file" id="exampleInputFile">
                      <p class="help-block"> </p>
                  </div>
                  <input type="hidden" name="course_id" value="<?php echo $this->input->get('course_id');?>">
                  <button class="btn btn-info btn-sm pull-right" type="submit">Submit</button>
              </form>
              </div>
              <div class="col-md-6">
              <form role="form" action="<?php echo AUTH_PANEL_URL."course_product/course/update_color_header_image";?>" method="POST" enctype="multipart/form-data" >
              <div class="col-md-12">
                  <div class="form-group" >
                    <label for="name">Color Picker</label>
                    <input type="color" class="form-control input-sm" value="<?php echo  $course_data['color_header']; ?>" id="color_code" name="color_code" placeholder="Enter color code" required>
                </div>
                </div>
                <div class="col-md-12">
                  <input type="hidden" name="course_id" value="<?php echo $this->input->get('course_id');?>">
                  <button class="btn btn-info btn-sm pull-right" type="submit">Submit</button>
                  </div>
              </form>
              </div>
              </div>
      </section>
    </div>


    <div id="tabContent3"  class="col-md-12 tabu ">
      <section class="panel">
          <header class="panel-heading">
              Course Video
          </header>
          <div class="panel-body">
              <form role="form" action="<?php echo AUTH_PANEL_URL."course_product/course/update_cover_video";?>" method="POST" enctype="multipart/form-data">
                  <!-- <div class="cover_video_element"></div> -->
                  <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputFile">Select Video Type</label>
                            <select name="cover_video_type" class="form-control">
                               <option <?=($course_data['cover_video_type']==0)?'selected':''?> value="0">YouTube</option>
                               <option <?=($course_data['cover_video_type']==1)?'selected':''?> value="1">Vimeo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputFile">Video URL</label>
                            <input name="cover_video" placeholder="Video URL" value="<?=$course_data['cover_video']?>" required class="form-control"  type="text" >
                            
                        </div>
                    </div>
                  </div>
                  <input type="hidden" name="course_id" value="<?php echo $this->input->get('course_id');?>">
                  <button class="btn btn-info btn-sm pull-right" type="submit">Submit</button>
              </form>

          </div>
      </section>
    </div>

    <?php
       $master = $this->db->get('currency_value_master')->result_array();
    ?>
    <div id="tabContent4"  class="col-lg-12 tabu hide">
      <section class="panel">
          <header class="panel-heading">
              Price Manager
          </header>
          <div class="panel-body">
              <form role="form" >
              <?php
              //print_r($master);
                  foreach($master as $m ){
                    $c = $this->db->where(array('course_id'=> $c_id , "mcc_code"=> $m['mcc_code']))->get('course_price_master')->row_array();
                    if($c){
                      $mrp =  $c['mrp'];
                      $for_dams = $c['for_dbmci'] ;
                      $non_dams =  $c['non_dbmci'];
                    }else{
                      $mrp = round($course_data['mrp']/$m['value'],2);
                      $for_dams = round($course_data['for_dbmci']/$m['value'],2);
                      $non_dams =  round($course_data['non_dbmci']/$m['value'],2);
                     }
                    ?>

                    <div class="">
                      <div class="form-group col-md-4">
                          <label for="exampleInputEmail2" class="">MRP (For -: <?php echo $m['name']?>) </label>
                          <input type="text" placeholder="Enter MRP" value="<?php echo $mrp; ?>" id="exampleInputEmail2" class="form-control input-sm ">
                      </div>
                      <div class="form-group col-md-4">
                          <label for="exampleInputEmail2" class="">for dams </label>
                          <input type="text" placeholder="" value="<?php echo $for_dbmci; ?>"  id="exampleInputEmail2" class="form-control input-sm ">
                      </div>
                      <div class="form-group col-md-4">
                          <label for="exampleInputEmail2" class="">Non dams  </label>
                          <input type="text" placeholder="" value="<?php echo $non_dbmci; ?>"  id="exampleInputEmail2" class="form-control input-sm">
                      </div>
                     </div>
                    <?php
                  }
              ?>
                 <div class="form-group col-md-12 ">
                  <button class="btn btn-info btn-sm pull-right" type="submit">Update</button>
                 </div>
              </form>

          </div>
      </section>
    </div>

    <?php
      $this->db->order_by("position","asc");
      $this->db->where('course_id',$this->input->get('course_id'));
      $faq = $this->db->get('course_faq_master')->result_array();
    ?>
    
    <?php
        $cid =  $this->input->get("course_id");
        $query = $this->db->query("SELECT ccm.*
                          FROM course_coupon_master as ccm
                          Left join coupon_course_relation_master as ccrm on ccrm.coupon_id = ccm.id
                          where ccrm.course_id = $cid");
        $coupon = $query->result_array();

    ?>
    <div id="tabContent6"  class="col-lg-12 tabu ">
      <section class="panel">
          <header class="panel-heading">
              Coupon
          </header>
          <div class="panel-body"><small class="bold" >Note-: To add/delete coupon(s) to this course please <a href="<?php echo AUTH_PANEL_URL;?>course_product/Coupon_course_relation/course_coupon/"><button class="btn btn-xs bold btn-success">click here</button></a></small></div>

          <div class="panel-body">
              <form role="form" >
                <div class="row state-overview">
                <?php foreach($coupon as $c){ ?>
                    <div class="col-lg-3 col-sm-6">
                        <section class="panel">
                            <div class="symbol red">
                                <i class="fa fa-tags"></i>
                            </div>
                            <div class="value">
                                <h1 class=" count2"><?php echo $c['coupon_tilte']?></h1>
                                <p></p>
                            </div>
                        </section>
                    </div>
                <?php  } ?>

                </div>
              </form>
          </div>
      </section>
    </div>
    <?php

    /*$query =  $this->db->query("SELECT csm.* , cstm.topic as title
                                FROM `course_segment_master` as csm
                                left join course_subject_topic_master as cstm on csm.topic_id =  cstm.id
                                where csm.course_fk = '$c_id' order by position asc ");
    $topic = $query->result_array(); */


    $query =  $this->db->query("SELECT ctm.* , ctm.topic_name as title
                                FROM course_topic_master as ctm
                                where ctm.course_fk = '$c_id' order by position asc ");
    $topic = $query->result_array();


  ?>
  <div id="tabContent7"  class="col-lg-12 tabu ">
    <section class="panel">
        <header class="panel-heading">
            Topic
        </header>

        <?php
          /*
          * Get all topic list from the selected subject
          */
          $this->db->where('subject_id',$course_data['subject_id']);
          $topic_data =  $this->db->get('course_subject_topic_master')->result_array();
        ?>
        <div class="panel-body select_topic_element" style="display: none;">
            <form novalidate="novalidate"  id="add_topic_from_select" class="form-inline " action="<?php echo AUTH_PANEL_URL."course_product/study/add_topic_to_course";?> " method="post" role="form" autocomplete="off">
                <div class="form-group">
                    <label class="sr-only" for="exampleInputEmail2">Topic name</label>
                    <select class="input-sm form-control required" name="topic_id" id="">
                      <option value="">--select Topic--</option>
                      <?php
                        foreach($topic_data as $td){
                          echo '<option value="'.$td['id'].'">'.$td['topic'].'</option>';
                        }
                      ?>
                    </select>
                    <input type="hidden" name="course_id" value="<?php echo $course_data['id']; ?>">

                    <i onclick="$('.add_topic_element').show('slow');$('.select_topic_element').hide('slow');"  class="fa fa- fa-plus-square btn btn-info popovers" aria-hidden="true" title=""  data-trigger="hover" data-placement="top" data-content="Add topic manually if not available in list." data-original-title="Note"></i>
                </div>
                <button type="submit" class="btn btn-success btn-sm">Add to course</button>
            </form>
        </div>
        
        <div style="display:none" class="panel-body add_topic_element">
            <form id="add_topic_from" novalidate="novalidate" autocomplete="off" role="form" method="post" action="<?php echo AUTH_PANEL_URL."course_product/study/save_course_topic";?>" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2" class="sr-only">Topic name</label>
                    <input type="text" name="topic" placeholder="Topic name " id="exampleInputEmail2" class="input-sm form-control required">
                </div>
                <input type="hidden" name="subject_id" value="<?php echo $course_data['subject_id']; ?>">
                <input type="hidden" name="course_id" value="<?php echo $course_data['id']; ?>">
                <button class="btn btn-success btn-sm" type="submit">Save</button>
                <button class="btn btn-danger btn-sm" onclick="$('.add_topic_element').hide('slow');$('.select_topic_element').show('slow');" type="reset">Cancel</button>
            </form>
        </div>
        <?php $show_add_topic = ($course_data['course_type'] == 1 )?'':''; ?>

        <div class="panel-body t  <?php echo $show_add_topic; ?>">
            <form id="add_topic_to_course" novalidate="novalidate" autocomplete="off" role="form" method="post" action="<?php echo AUTH_PANEL_URL."course_product/course/add_topic_name_to_course";?>" enctype="multipart/form-data" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2" class="sr-only">Topic name</label>
                    <input type="text" name="topic_name" placeholder="Topic name " id="exampleInputEmail2" class="input-sm form-control required">
                </div>
                <div class="form-group">
                    <label for="exampleInputimage" class="sr-only">Image</label>
                    <input type="file" name="imagetopic" id="imagetopic" class="">
                </div>
                <input type="hidden" name="topic_type" value="" />
                <input type="hidden" name="course_id" value="<?php echo $course_data['id']; ?>">
                <button class="btn btn-success btn-sm" type="submit">Add</button>
            </form>
        </div>




        <div style="display:none" class="panel-body edit_topic_name_element">
            <form id="edit_topic_name_from" novalidate="novalidate" enctype="multipart/form-data"  autocomplete="off" role="form" method="post" action="<?php echo AUTH_PANEL_URL."course_product/course/edit_course_topic_name";?>" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2" class="sr-only">Edit Topic name</label>
                    <input type="text" name="edit_topic" placeholder="Topic name " id="exampleInputEmail2" class="input-sm required form-control">
                </div>
                <div class="form-group">
                    <label for="exampleInputimage" class="sr-only">Image</label>
                    <input type="file" name="imagetpc_udt" id="imagetpc_udt" class="" >
                </div>
                <input type="hidden" name="topic_id" value="">
                 <input type="hidden" name="course_id" value="<?php echo $c_id;?>">
                <button class="btn btn-success btn-sm" type="submit">Update</button>
                <button class="btn btn-danger btn-sm" onclick="$('.edit_topic_name_element').hide('fast')" type="reset">Cancel</button>
            </form>
        </div>
        <!--  course chapters view starts from here -->
        <div class="panel-body">
              <div class="tab-content">
              <button class=" refresher pull-right btn  btn-lg btn-warning col-md-2 margin-right" style="display:none" onclick="location.reload();" ><i class="fa fa-check-circle "></i> Refresh</button>
                <ul class="task-list col-md-12 margin-top" id="sortable_topic">

                <?php foreach($topic as $tp){ ?>
                    <li data-position="" data-faqid="<?php echo $tp['id'];?>" >
                      <div class="panel panel-info">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a class="accordion-toggle " data-toggle="collapse" data-target="#aaccordion1_<?php echo $tp['id']; ?>">
                                    <?php if($tp['is_preview'] == 1 ){?>
                                     <i class="fa fa-circle text-success popovers" aria-hidden="true" title=""  data-trigger="hover" data-placement="top" data-content="The Green color indicate that you are using this section with course preview."></i>
                                    <?php }else{ ?>
                                    <i class="fa fa-circle text-muted"></i>
                                    <?php } ?>
                                    <?php echo $tp['title'];
                                   if($tp['image']!=''){
                                    ?>

                                    <img src="<?php echo $tp['image'] ?>" height="20px;" weidth="20px; "/>
                                   <?php  } ?>
                                  </a>
                                  <div class="btn-group pull-right">
                                    <button ria-expanded="false" data-toggle="dropdown" class="btn btn-info dropdown-toggle btn-xs" type="button" aria-expanded="false">Action <span class="caret"></span></button>
                                    <ul role="menu" class="dropdown-menu">

                                      <li><a href="<?php echo AUTH_PANEL_URL."course_product/study/add_topic_to_preview/".$c_id;?>?topic_id=<?php echo $tp['id'];?>" > <i class="fa fa-book "></i> Add to preview </a></li>
                                      <li><a onclick=" return confirm('Are you really want to delete this topic ?'); " href="<?php echo AUTH_PANEL_URL."course_product/course/delete_topic_id_from_course/".$c_id;?>?topic_id=<?php echo $tp['id'];?>"><i class="fa fa-times"></i> Delete</a></li>
                                      <li class=""><a onclick="$('.edit_topic_name_element').show('slow');$('input[name=topic_id]').val('<?php echo $tp['id']; ?>');$('input[name=edit_topic]').val('<?php echo $tp['title']; ?>').focus()"  href="javascript:void(0)" > <i class="fa fa-pencil "></i> Edit Topic </a></li>
                                      <li class="hide" ><a class="add_media_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>" data-trigger="pdf" > <i class="fa fa-plus-circle"></i> Add Pdf</a></li>
                                      <li class="hide" ><a class="add_media_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>" data-trigger="ppt" > <i class="fa fa-plus-circle"></i> Add Ppt</a></li>
                                      <li class="hide" ><a class="add_media_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>"  data-trigger="doc" > <i class="fa fa-plus-circle"></i> Add Doc</a></li>
                                      <li class="hide" ><a class="add_media_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>"  data-trigger="epub" > <i class="fa fa-plus-circle"></i> Add Epub</a></li>
                                      <li class="hide" ><a class="add_media_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>"  data-trigger="image" > <i class="fa fa-plus-circle"></i> Add Flash Card</a></li>
                                      <?php if($tp['topic_type'] == "test") { ?>
                                          <li><a class="add_test_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>"  data-trigger="test" > <i class="fa fa-plus-circle"></i> Add Test</a></li>
                                      <?php } ?>    
                                          <li><a class="add_media_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>"  data-trigger="concept" > <i class="fa fa-plus-circle"></i> Add Concepts</a></li>
                                          <li><a class="add_media_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>"  data-trigger="video" > <i class="fa fa-plus-circle"></i> Add Video</a></li>                                       
                                          <li><a class="add_test_practice hide " href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>"   data-trigger="practice" > <i class="fa fa-plus-circle"></i> Add Practice</a></li>                                                                                  
                                          <li><a class="add_media_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>" data-trigger="pdf" > <i class="fa fa-plus-circle"></i> Add Pdf</a></li>
                                          <li><a class="add_test_element" href="javascript:void(0)" data-topic_id="<?php echo $tp['id']; ?>"  data-trigger="test" > <i class="fa fa-plus-circle"></i> Add Quiz</a></li>
                                    </ul>
                                  </div>
                              </h4>
                          </div>
                          <div id="aaccordion1_<?php echo $tp['id']; ?>" class="panel-collapse collapse in" aria-expanded="true" style="">
                              <div class="panel-body" id="topic-element<?php echo $tp['id']; ?>">
                                <?php
                                // show added files in topic
                                  $this->db->where('segment_fk',  $tp['id']);
                                  $this->db->order_by('position');
                                  $files =  $this->db->get('course_segment_element')->result_array();
                                  $rotation = 1;
                                  foreach($files as $f){
                                    $color =  $view_flash_card= "";
                                    if($f['type'] != "test" and $f['type'] != "practice" ){
                                       $this->db->where('id',  $f['element_fk']);
                                       $file_id =  $this->db->get('course_topic_file_meta_master')->row_array();
                                        $file_type_id = $file_id['file_type'];
                                        $file_ext = "";
                                        if($file_type_id == 1 ){ $file_ext = "pdf"; $color="#6ccac9";}
                                        if($file_type_id == 2 ){ $file_ext = "ppt"; $color="#ff6c60";}
                                        if($file_type_id == 3 ){ $file_ext = "video"; $color="#f8d347";}
                                        if($file_type_id == 4 ){ $file_ext = "epub"; $color="#57c8f2"; }
                                        if($file_type_id == 5 ){ $file_ext = "Doc"; $color="#a9d96c";}
                                        if($file_type_id == 6 ){
                                          $file_ext = "Flash-card"; $color="#a9d96c";
                                          $view_flash_card = '<a href="'.$file_id['file_url'].'" target="_blank"> --view</a>';
                                        }
                                      if($file_type_id == 7 ){ $file_ext = "Concepts"; $color="#a9d96c";}

                                        $f_title =  $file_id['title'];
                                    }else{
                                        if($f['type'] == "practice"){ $file_ext = "Practice"; }
                                        else{ $file_ext = "Test Series"; }
                                        $this->db->where('id',  $f['element_fk']);
                                        $test_info =  $this->db->get('course_test_series_master')->row_array();
                                        $f_title = $test_info['test_series_name'];
                                    }
                                    ?>
                                        <?php
                                          if($f['tag'] == 0){
                                            $f_state = 'Free';
                                            $f_state_rev  = 'Paid';
                                          }else{
                                            $f_state = 'Paid';
                                            $f_state_rev = 'Free';
                                          }
                                        ?>
                                        <div class="col-md-12" style="border-left:5px solid <?php echo $color;?>;margin:5px 0;padding-top: 10px;box-shadow:4px 3px 5px #a6daff;"><?php if(@$file_id['thumbnail_url']!=''){ ?><img src="<?php echo $file_id['thumbnail_url']; ?>" height="20px;" weidth="20px; "/> <?php }else{ ?><i class="fa fa-file fa-1x  "></i>  <?php } ?>
                                        <span class="bold capitalize "> <?php echo $file_ext.'<sup class=" badge '.(($f['tag']==0)?'bg-success':'bg-important').' bold"> '.$f_state.'</sup>' ; ?></span>
                                           <a onclick=" return confirm('Are you really want to delete this file from topic ?'); " href="<?php echo AUTH_PANEL_URL."course_product/study/remove_file_from_topic/".$f['id']?>?course_id=<?php echo $c_id;?>"  ><button class="btn btn-xs pull-right btn-danger"><i class="fa fa-times "></i></button></a>
                                  
                                           <?php if($rotation != count($files) ){
                                               
                                             //  echo AUTH_PANEL_URL."ourse_product/course/change_order/".$f['id'];
                                               
                                               ?>
                                              <a href="<?php echo AUTH_PANEL_URL."course_product/study/change_order/".$f['id'].""?>?swap_action=down&&course_id=<?php echo  $c_id ?>&&tid=<?php echo $tp['id']; ?>"><button class="btn btn-xs pull-right margin-right btn-warning"><i class="fa fa-long-arrow-down"></i></button></a>
                                           <?php }else{ ?>
                                             <button class="btn btn-xs pull-right margin-right btn-warning disabled"><i class="fa fa-long-arrow-down"></i></button>
                                           <?php } ?>

                                           <?php if($rotation !== 1){?>
                                              <a href="<?php echo AUTH_PANEL_URL."course_product/study/change_order/".$f['id'].""?>?swap_action=up&&course_id=<?php echo  $c_id ?>&&tid=<?php echo $tp['id']; ?>"> <button class="btn btn-xs pull-right margin-right btn-success" onclick="change_orderrr(<?php echo $f['id'] ?>);"><i class="fa fa-long-arrow-up"></i></button></a>
                                           <?php }else{ ?>
                                             <button class="btn btn-xs pull-right margin-right btn-success disabled" onclick="change_orderrr(<?php echo $f['id'] ?>);"><i class="fa fa-long-arrow-up"></i></button>
                                            <?php } ?>
                                           <p> <?php echo $f_title. $view_flash_card;  ?> </p>

                                        </div>
                                  <?php $rotation++; } ?>
                              </div>
                          </div>
                      </div>
                    </li>
                <?php }
                ?>
                </ul>
                
                <div class="panel-body col-md-12">

                  <small class="bold">Note-: You can set order of Topic(s) by using drag and drop. </small>
                  <button class="btn btn-xs btn-success topic_pos">Update</button>
                  </div>
              </div>
        </div>
    </section>
  </div>
    <!-- <div id="tabContent8" class="col-md-12 tabu ">
        <section class="panel">
            <header class="panel-heading">
                Course Reviews
            </header>
            <div class="panel-body ">
                <div class="adv-table">
                    <div class="col-md-6 pull-right">
                        <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
                        <div  class="input-group-addon">From</div>
                        <input type="text" id="min-date" class="form-control date-range-filter input-sm course_start_date"  placeholder="">

                        <div class="input-group-addon">to</div>

                        <input type="text" id="max-date" class="form-control date-range-filter input-sm course_end_date"  placeholder="">

                        </div>
                    </div>
                    <table  class="display table table-bordered table-striped col-md-12" id="all-course-review-grid" style="width:100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Name </th>
                                <th>Rating</th>
                                <th>Review </th>
                                <th>Time </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div> -->
    <?php
            /* related course code start here
			* do not send key if no relation found
			*/
			$learner = (string)rand(1000,5000);
			$rating = (string)rand(1,5);
			$sql = "SELECT id ,title ,cover_image,mrp ,tags,  description,is_new ,for_dbmci ,non_dbmci ,  $learner as learner , $rating as rating
					from course_master
					where match(course_master.tags) against((select tags
															from course_master
															where id = $c_id ) in boolean mode)
					and id != $c_id";
			$match = $this->db->query($sql)->result_array();
			if(count($match) > 0){

			}
    ?>
    <div id="tabContent9" class="col-md-12 tabu ">
        <section class="panel">
            <header class="panel-heading">
                    Related Courses
            </header>
            <style>
            .course_desc {

                max-height: 40px;
                    min-height: 40px;
                    overflow: hidden;
                }
                .course_tags {
                    max-height: 40px;
                    min-height: 40px;
                    overflow: hidden;
                }
            </style>
            <div class="panel-body">
                <div class="row-fluid">
                    <ul class="thumbnails">
                        <?php foreach($match as $m){  ?>
                        <li class="col-md-4">
                            <div class="thumbnail">
                            <img alt="<?php echo $m['title'];?>" style="max-width: 300px; height: 100px;" src="<?php echo $m['cover_image'];?>">
                            <div class="caption">
                                <h4 class="capitalize" ><?php echo $m['title'];?></h4>
                                <p class="course_desc" ><?php echo substr($m['description'],0,80) . ' ...';?></p>
                                <?php $tags = explode(',',$m['tags']);?>
                                <p class="course_tags"> Tags -:
                                  <?php foreach($tags as $t  ) { ?>
                                  <span class="badge bg-inverse capitalize"><?php echo $t;?></span>
                                  <?php } ?>
                                </p>
                                <p><a class="btn btn-success btn-xs " href="<?php echo AUTH_PANEL_URL.'course_product/course/edit_course_page?course_id='.$m['id'] ;?>">View</a></p>
                            </div>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </section>
    </div>

<?php
  $edit_mode = false;
  // if($this->input->get('edit_filter') == "edit" && array_key_exists('filter_name', $_GET)) {
  //   $edit_mode = True;
  //   $filter_name = urldecode($this->input->get('edit_filter'));
  //   $sql = "SELECT `id`,`course_id`, group_concat(FranchiseName SEPARATOR ',') as FranchiseName , group_concat(CourseGroup SEPARATOR ',' ) as CourseGroup , group_concat(Session SEPARATOR ',' ) as Session , group_concat(Course SEPARATOR ',' ) as Course , group_concat(Batch SEPARATOR ',' ) as Batch ,f_name ,price FROM `course_filtration` where course_id = $c_id and f_name = '".$filter_name."'  group by f_name ";
  //   $selected_filter  = $this->db->query($sql)->result();
  //   $selected_filter_Session = explode(',', $selected_filter->Session);
  //   $selected_filter_FranchiseName = explode(',', $selected_filter->FranchiseName);
  //   $selected_filter_CourseGroup = explode(',', $selected_filter->CourseGroup);
  //   $selected_filter_Session = explode(',', $selected_filter->Course);
  //   $selected_filter_Batch = explode(',', $selected_filter->Batch);
  // }
?>

    <div id="tabContent10" class="col-md-12 tabu hide">
            <section class="panel">
                <header class="panel-heading">
                    <?php echo ($edit_mode)?'Edit ':""; ?>Filtration
                    <button onclick="$('.add_filter_element').slideToggle('slow');" class="btn-success btn-xs btn pull-right"><i class="fa fa-plus"></i> Add Filter</button>
                </header>

                <div class="panel-body ">

<link rel="stylesheet" href="http://davidstutz.de/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" type="text/css"/>
    <!-- Build your select: -->
    <div class="add_filter_element" style="display:<?php echo ($edit_mode)?'':"none"; ?>;">
      <form action="" method="POST">
        <?php echo ($edit_mode)?'<input type="hidden" name="f_name" value="'.$_GET['filter_name'].'">':""; ?>
        <input type="hidden" name="course_id" value="<?php echo $c_id;?>">
        <div class="form-group col-md-2">
          <label class="col-md-12" for="exampleInputEmail1">Session</label>
          <select name="Session[]" class="example-getting-started" multiple="multiple">
            <?php
              $session = $this->db->select('distinct(session)')->get('users_dams_info')->result();
              echo '<option  value="*">ALL</option>';
              foreach($session as $sess){
                echo '<option value="'.$sess->session.'">'.$sess->session.'</option>';

              }

            ?>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label class="col-md-12"  for="exampleInputEmail1">Franchise</label>
          <select name="FranchiseName[]" class="example-getting-started" multiple="multiple">
            <?php
              $FranchiseName = $this->db->select('distinct(FranchiseName)')->order_by("CourseGroup", "asc")->get('users_dams_info')->result();
              echo '<option   value="*">ALL</option>';
              foreach($FranchiseName as $sess){
                echo '<option value="'.$sess->FranchiseName.'">'.$sess->FranchiseName.'</option>';
              }

            ?>

          </select>
        </div>
        <div class="form-group col-md-2">
          <label class="col-md-12"  for="exampleInputEmail1">Course Group</label>
          <select name="CourseGroup[]"  class="example-getting-started" multiple="multiple">
            <?php
              $CourseGroup = $this->db->select('distinct(CourseGroup)')->order_by("CourseGroup", "asc")->get('users_dams_info')->result();
              echo '<option   value="*">ALL</option>';
              foreach($CourseGroup as $sess){
                echo '<option value="'.$sess->CourseGroup.'">'.$sess->CourseGroup.'</option>';
              }

            ?>

          </select>
        </div>
        <div class="form-group col-md-2">
          <label class="col-md-12"  for="exampleInputEmail1">Course</label>
          <select name="Course[]"  class="example-getting-started" multiple="multiple">
             <?php
              $Course = $this->db->select('distinct(Course)')->order_by("Course", "asc")->get('users_dams_info')->result();
              echo '<option   value="*">ALL</option>';
              foreach($Course as $sess){
                echo '<option value="'.$sess->Course.'">'.$sess->Course.'</option>';
              }

            ?>

          </select>
        </div>


        <div class="form-group col-md-3">
          <label class="col-md-12" for="exampleInputEmail1">Batch</label>
          <select name="Batch[]"   class="example-getting-started" multiple="multiple">
            <?php
              $Batch = $this->db->select('distinct(Batch)')->order_by("Batch", "asc")->get('users_dams_info')->result();
              echo '<option   value="*">ALL</option>';
              foreach($Batch as $sess){
                echo '<option value="'.$sess->Batch.'">'.$sess->Batch.'</option>';
              }

            ?>

          </select>
        </div>

        <div class=" hide form-group col-md-2">
          <label class="col-md-12" for="exampleInputEmail1">Course Type</label>
          <select class="form-control col-md-12 input-sm filtration_price_button" name="c_type">
           <option value="paid">Paid</option>
            <option value="free">Free</option>

          </select>
        </div>

        <div class=" hide form-group col-md-2 filtration_element_price_div">
          <label class="col-md-12" for="exampleInputEmail1">Price</label>
          <input  class=" col-md-12  form-control input-sm filtration_element_price_input" type="text" name="price" value="<?php echo $course_data['for_dbmci'];?>" >
        </div>
        <div class="  form-group col-md-2">
         <label class="col-md-12" for="exampleInputEmail1">&nbsp;</label>
         <!-- onclick="return confirm('Are you sure ?');" -->
          <input  onclick="return confirm('Are you sure ?');"  name="filter_save" type="submit" class="btn btn-info col-md-12 btn-sm bold" value="Save">
        </div>

      </form>
    </div>

                <div class="llwrapar" style="display:none;">
                        <label for="exampleInputEmail1">Select Session</label>
                        <div class="year_segment"></div>
                        <div class="f_element">
                        <label for="exampleInputEmail1">Select Franchise </label>
                            <div class="fr_segment">
                            </div>
                        </div>

                        <div class="c_element">
                        <label for="exampleInputEmail1">Select Course Group </label>
                            <div class="c_segment">
                            </div>
                        </div>

                        <div class="cc_element">
                        <label for="exampleInputEmail1">Select Course</label>
                            <div class="cc_segment">
                            </div>
                        </div>

                        <div class="course_element">
                        <label for="exampleInputEmail1">Select Batch</label>
                            <div class="course_segment">
                            </div>
                        </div>

                        <button class="btn btn-success  btn-sm save_filtration">Save</button>
                        <button class="btn btn-danger btn-sm" onclick="$('.wrapar').hide('slow');" type="button" >Cancel</button>

                    </div>

                    <div>
                        <table  class="display table table-bordered table-striped col-md-12" id="allfiltration-video-grid" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Filter Name </th>
                                    <th>Session</th>
                                    <th>FranchiseName</th>
                                    <th>CourseGroup </th>
                                    <th>Course</th>
                                    <th>Batch </th>
                                    <th>Price </th>
                                    <th>Action </th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                                    <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                                    <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                                    <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
                                    <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
                                    <th><input type="text" data-column="5"  class="search-input-text form-control"></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </section>
        </div>


    <div id="tabContent11"  class="col-lg-12 tabu hide">
      <section class="panel">
          <header class="panel-heading">
              Send Push Notification
          </header>
          <div class="panel-body">
            <small class="bold text-danger" >Note-: Course should be published before using this service  </small>

            <div class="form-group col-md-12">
              <label for="exampleInputEmail1">Notification Title</label>
              <input id="push_notification_text" placeholder="" name="title" class="form-control input-sm" type="text">
           </div>
           <div class="form-group col-md-12">
            <button onclick="$(this).addClass('disabled');fire_push();" class="btn-sm btn btn-success " type="button">Send Notification</button>
           </div>
           <div id="show_socket_state"></div>
          </div>
      </section>
    </div>
    </div>
</div>


<input type="hidden" value="" name="show_file_type">
<input type="hidden" value="" name="active_topic_id">
<div role="dialog"  id="myModal2" class="modal fade" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title file-modal-element-head"> </h4>
            </div>
            <div class="modal-body">
                <div class="">
                <section class="panel">
                  <div class="panel-body">
                  <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="bold" >Subject</label>
                            <select id="subject_element_multiple" class="search-input-select-file" data-column="3"  name="subject_element_multiple " data-live-search="true" multiple data-style="btn-info" data-selected-text-format="count">
                                
                            </select>
                        </div> 
                        <div class="form-group col-md-3 ">
                            <label class="bold" >Topic</label>
                            <select  id="subject_topic_element_multiple"   data-column="4"   class=" subject_topic_element_multiple search-input-select-file "  name="subject_topic_element_multiple" data-live-search="true" multiple data-style="btn-warning" data-selected-text-format="count">
                               
                            </select>
                        </div>                         
                    </div>

                  <div class="adv-table col-md-12 ">
                  <table  class="display table table-bordered table-striped" id="all-file-grid" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Title </th>
                        <th>File URL</th>
                        <th>Subject </th>
                        <th>Topic</th>
                        <th>Page Count</th>
                        <th>File type</th>
                        <th>Action </th>
                      </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th><input type="text" data-column="0"  class=" hide search-input-text form-control"></th>
                            <th><input type="text" data-column="1"  class=" hide search-input-text form-control"></th>
                            <th><input type="text" data-column="2"  class=" hide search-input-text form-control"></th>
                            <th></th>
                            <th></th>
                            <th><input type="text" data-column="5"  class=" hide search-input-text form-control"></th>
                            <th><select disabled data-column="6"   class="form-control search-input-select-file file-selector">
                                  <option value="3">video</option>
                                  <option value="7">concept</option>
                                </select>
                            </th>
                            <th></th>
                        </tr>
                    </thead>

                  </table>
                  </div>
                  </div>
                </section>
              </div>
            </div>
        </div>
    </div>
</div>

<div role="dialog"  id="myModal3" class="modal fade" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title "> Add Test series to topic </h4>
            </div>
            <div class="modal-body">
                <div class="">
                <section class="panel">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="bold" for="sel1">Test Type</label>
                            <select  data-column="1" class="test_type_element_multiple search-input-select-test"  name="text_type_element" data-live-search="true" multiple data-style="btn-success" data-selected-text-format="count">
                            <?php   
							 $test_type_data = $this->db->get('course_test_type_attribute')->result_array(); 
                                foreach($test_type_data as $td) { ?>
                                     <option value="<?php echo $td['id']; ?>"><?php echo $td['name']; ?></option>
                                <?php  }   ?> 

                            </select>
                        </div> 
                        <div class="form-group col-md-3">
                            <label class="bold" >Exam Type</label>
                            <select  data-column="2"  class="exam_type_element_multiple search-input-select-test"  name="exam_type_element" data-live-search="true" multiple data-style="btn-info" data-selected-text-format="count">
                                <?php
                                    $sub_array =  array(); 
                                    foreach($main_stream_result as $msr){
                                        if($msr['parent_id'] == 0 ){
                                            echo "<option value='".$msr['id']."'>".$msr['name']."</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div> 
                        <div class="form-group col-md-3 ">
                            <label class="bold" >Sub Exam Type</label>
                            <select data-column="3"  id="exam_sub_type_element_multiple"   class=" search-input-select-test exam_sub_type_element_multiple"  name="exam_sub_type_element" data-live-search="true" multiple data-style="btn-warning" data-selected-text-format="count">
                               
                            </select>
                        </div>                         
                    </div>
                  <div class="adv-table col-md-12">
                  <table  class="display table table-bordered table-striped"   id="all-test-grid" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Test Type </th>
                        <th>Exam Type </th>
                        <th>Sub Category </th>
                        <th>Name </th>
                        <th>Action </th>
                      </tr>
                    </thead>
                  </table>
                  </div>
                  </div>
                </section>
              </div>
            </div>
        </div>
    </div>
</div>


<div role="dialog"  id="myModal_practice" class="modal fade" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title "> Add practice to topic </h4>
            </div>
            <div class="modal-body">
                <div class="">
                <section class="panel">
                  <div class="panel-body">
                  <div class="adv-table col-md-12">
                  <table  class="display table table-bordered table-striped"   id="all-quiz-grid" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>subject </th>
                        <th>topic</th>
                        <th>Name </th>
                        <th>Action </th>
                      </tr>
                    </thead>
                  </table>
                  </div>
                  </div>
                </section>
              </div>
            </div>
        </div>
    </div>
</div>




<div role="dialog"  id="myModal5" class="modal fade" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title "> Add Course Type </h4>
            </div>
            <div class="modal-body">
            <div class="panel-body">
            <form id="add_course_type_course" novalidate="novalidate" autocomplete="off" role="form" method="post" action="<?php echo AUTH_PANEL_URL."course_product/course/add_course_type";?>" enctype="multipart/form-data" class="form-inline">
                <div class="form-group">
                    <!-- <label for="exampleInputEmail2" >Course Type</label> -->
                    <input type="text" name="course_type" placeholder="course type "   id="practice_id" value="<?php //echo @$course_data['practice_id']; ?>" class="input-sm form-control required">
                </div>
              <!-- <input type="hidden" name="active_topic_id" value="<?php //echo @$course_data['practice_id']; ?>"> -->
                <input type="hidden" name="course_id" value="<?php echo $c_id  ?>">
                <button class="btn btn-success btn-sm" type="submit">Add</button>
            </form>
        </div>

               
              </div>
            </div>
        </div>
    </div>
</div>
<div role="dialog"  id="myModal6" class="modal fade" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title "> Add Content Type </h4>
            </div>
            <div class="modal-body">
            <div class="panel-body">
            <form id="add_content_course_type" novalidate="novalidate" autocomplete="off" role="form" method="post" action="<?php echo AUTH_PANEL_URL."course_product/course/add_content_type";?>" enctype="multipart/form-data" class="form-inline">
                <div class="form-group">
                    <!-- <label for="exampleInputEmail2" >Course Type</label> -->
                    <input type="text" name="content_type" placeholder="content type "   id="conent_type" value="<?php //echo @$course_data['practice_id']; ?>" class="input-sm form-control required">
                </div>
              <!-- <input type="hidden" name="active_topic_id" value="<?php //echo @$course_data['practice_id']; ?>"> -->
                <input type="hidden" name="course_id" value="<?php echo $c_id  ?>">
                <button class="btn btn-success btn-sm" type="submit">Add</button>
            </form>
        </div>

               
              </div>
            </div>
        </div>
    </div>
</div>
<div role="dialog"  id="myModal4" class="modal fade" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title "> Add Practice </h4>
            </div>
            <div class="modal-body">
            <div class="panel-body t">
            <form id="add_topic_to_course" novalidate="novalidate" autocomplete="off" role="form" method="post" action="<?php echo AUTH_PANEL_URL."course_product/course/add_practice_to_course";?>" enctype="multipart/form-data" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2" class="sr-only">Practice Id</label>
                    <input type="text" name="practice_id" placeholder="Practice Id " onkeypress="return isNumberKey(event)"  id="practice_id" value="" class="input-sm form-control required">
                </div>
               
                <input type="hidden" name="active_topic_id" value="<?php //echo @$course_data['practice_id']; ?>">
                <input type="hidden" name="course_id" value="<?php echo $c_id ?>">
                <button class="btn btn-success btn-sm" type="submit">Add</button>
            </form>
        </div>

                </section>
              </div>
            </div>
        </div>
    </div>
</div>

<div class="fix-loader"></div>

<?php
$adminurl = AUTH_PANEL_URL;
$instructor_id = $course_data['instructor_id'];
$course_main_fk = $course_data['course_main_fk'];
$subject_id = $course_data['subject_id'];
$course_category_fk = $course_data['course_category_fk'];
$validation_js = AUTH_ASSETS."js/jquery.validate.min.js";
if($course_data['course_for'] == 0){
  $notification_user_type = 'DAMS';
}elseif($course_data['course_for'] == 1){
  $notification_user_type = 'NON_DAMS';
}else{
  $notification_user_type = 'ALL';
}
$topic_data_for_use = json_encode($topic_data_for_json);
$custum_js = <<<EOD
<script type="text/javascript" src="http://davidstutz.de/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>
<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<script>CKEDITOR.replace(description);</script>
<script>


    var test_behaviour = "";
    $(document).ready(function(){
        $('.test_type_element_multiple,.exam_type_element_multiple,#exam_sub_type_element_multiple ,#subject_topic_element_multiple').selectpicker('setStyle', 'btn-lg', 'add');;    
    });


    $('.exam_type_element_multiple').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        all_cat = $main_stream_result_json;
        main_selected  = $(this).val();
        var html = '';
        $.each(main_selected, function( index, value ) {
            $.each(all_cat, function( i, v ) {
                if(value == v.id){
                    html +=  '<optgroup label="'+v.name+'">';
                }
            });
            $.each(all_cat, function( i, v ) {
                if(value == v.parent_id){
                    html +=  '<option value="'+v.id+'">'+v.name+'</option>';
                }
            });
            html +='</optgroup>';
        });
        $('#exam_sub_type_element_multiple').empty().append($.parseHTML(html)).selectpicker('refresh');
    });

    $('#subject_element_multiple').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {

        main_text = $('#subject_element_multiple').val();
        overall = $subject_data_json;
        sub_cate = $topic_data_for_use; // php varibale
        var html = '';

        $.each(overall, function( index, value ) {
            if($.inArray(value.id, main_text ) >= 0 ){
                var option = ""; 
                $.each(sub_cate, function( i, v ) {
                    if(parseInt(value.id) == parseInt(v.subject_id)){
                       option +=  '<option value="'+v.id+'">'+v.topic+'</option>';

                    }
                });
                if(option != ""){
                   html = html+'<optgroup label="'+value.name+'">'+option+'</optgroup>';
                }
            }
        });

        $('#subject_topic_element_multiple').empty().append($.parseHTML(html)).selectpicker('refresh');
    });
</script>
<script>
    jQuery('.cat_check').change(function() {
      $('.cate_parent_head .badge').html('');
      var arr = [];
      var c=0;
      $('.cat_check').each(function (){
        if ($(this).prop('checked'))
         {
          p_id = $(this).data('holder_id');
          arr.push(p_id);
         }
      });

      var counts = {};
      arr.forEach(function(x) {
        counts[x] = (counts[x] || 0)+1;
        if(counts[x] > 0 ){
          $('.parent_holder'+x+' .badge').html(counts[x]);
         // console.log('#tabprimary'+x+' .badge');
        }
      });
    }).change();
</script>

<script>
$('#select-all').click(function(event) {
     if(this.checked) {
        // Iterate each checkbox
        $('.cat_check:checkbox').each(function() {
            this.checked = true;
        });
    } else {
        $('.cat_check:checkbox').each(function() {
            this.checked = false;
        });
    }

    jQuery('.cat_check').change();
});



$('.select-sub').click(function(event) {
    to = $(this).data('id');
    if(this.checked) {
        // Iterate each checkbox
        $('#'+to+' .cat_check:checkbox').each(function() {
            this.checked = true;
        });
    } else {
        $('#'+to+' .cat_check:checkbox').each(function() {
            this.checked = false;
        });
    }
   jQuery('.cat_check').change();
});
        function validate() {
            if ((parseFloat($("#mrp").val()) < parseFloat($("#course_sp").val()))) {
            show_toast('error', 'The Offer price cannot be greater than MRP.','Price');
            
                return false;
            } else {
            // show_toast('error', 'The Offer price cannot be less than MRP.','Price');
            
            return true;
            }
            }


        //     function validate_data_keypress(event){
        //     if (event.which != 46 && (event.which < 47 || event.which > 59))
        //     {
        //         event.preventDefault();
        //         if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
        //             event.preventDefault();
        //         }
        //     }
        // }

</script>

<script>
$('#container').addClass('sidebar-closed');
$('#main-content').css('margin-left',"0px");
</script>
         <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
         <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
         <script src="$validation_js" type="text/javascript"></script>
                <script>
                $('input[name=mrp]').bind('blur keyup change ', function(e){
                    if($(this).val() < 1){
                        $('input[name=for_dams]').val(0).change();
                        $('input[name=non_dams]').val(0).change();
                    }
                });
                  /* ****************** price jquery handler start here  **********************  */
                  $('.for_dams_input').bind('blur keyup change ', function(e){
                     name = $(this).attr('name');
                     mrp  = $('input[name=mrp]').val();
                     for_dams = $('input[name=for_dams]').val();
                     for_dams_percent = $('input[name=for_dams_percent]').val();

                     non_dams = $('input[name=non_dams]').val();
                     non_dams_percent = $('input[name=non_dams_percent]').val();
                     /* event on for dams */
                     if(name == "for_dams"){
                      percent = 100 - (for_dams/mrp)*100;
                      if(for_dams == "" || for_dams == 0 ){
                        $('input[name=for_dams_percent]').val(100);
                      }else{
                        $('input[name=for_dams_percent]').val(percent);
                      }
                     }
                     /* event on for dams percent */
                     if(name == "for_dams_percent"){
                      value = mrp- (mrp/100)*for_dams_percent;
                      $('input[name=for_dams]').val(value);

                     }
                      /* event on for non  dams */
                      if(name == "non_dams" ){
                          percent = 100 - (non_dams/mrp)*100;
                          if(non_dams == ""  || non_dams == 0){
                            $('input[name=non_dams_percent]').val(100);
                          }else{
                            $('input[name=non_dams_percent]').val(percent);
                          }
                         }
                       /* event on for dams percent */
                     if(name == "non_dams_percent"){
                      value = mrp- (mrp/100)*non_dams_percent;
                      $('input[name=non_dams]').val(value);

                     }
                  });
                  $('.for_dams_input').change();

                  /* ****************** price jquery handler end here  ****************** */

                    function add_file_to_topic(file_title,file_id){
                      topic_id = $('input[name=active_topic_id]').val();

                      //html = '<form  class="form-inline" action="$adminurl'+'course_product/course/add_file_to_topic" method="post" role="form"><div class="form-group"><label class="" for="exampleInputEmail2"><i class="fa fa-file fa-2x" > </i>'+file_title+'</label></div><input type="hidden" value="'+file_id+'" name="file_id"><input type="hidden" value="" name="file_type"><input type="hidden" value="$c_id" name="course_id"><input type="hidden" value="'+topic_id+'" name="topic_id"> <button type="submit" class="btn btn-success btn-xs bold">Save</button> <button type="button" class="btn btn-danger btn-xs bold" onclick="$(this).parent().hide(\'slow\');">Cancel</button></form>';
                      //$('#topic-element'+topic_id).append(html);

                      console.log('file id is -: '+file_id);
                      console.log('topic_id id is -: '+topic_id);
                      console.log('course id is -: '+'$c_id');

                      jQuery.ajax({
                        url: "$adminurl"+"course_product/course/ajax_add_file_to_topic",
                        method: 'post',
                        dataType: 'json',
                        data: {'course_id':'$c_id','file_id':file_id,'topic_id':topic_id,'file_type':''},
                        success: function (data) {
                          $('#all-file-grid').DataTable().ajax.reload();
                          show_toast('success','Saved successfully','File added');
                          $('.refresher').show();
                        }
                       });

                    }

                    function add_test_to_topic(file_title,file_id){
                      topic_id = $('input[name=active_topic_id]').val();
                      console.log("here you are "+topic_id);
                      //html = '<form  class="form-inline" action="$adminurl'+'course_product/course/add_file_to_topic" method="post" role="form"><div class="form-group"><label class="" for="exampleInputEmail2"><i class="fa fa-file fa-2x" > </i>'+file_title+'</label></div><input type="hidden" value="'+file_id+'" name="file_id"><input type="hidden" value="test" name="file_type"><input type="hidden" value="$c_id" name="course_id"><input type="hidden" value="'+topic_id+'" name="topic_id"> <button type="submit" class="btn btn-success btn-xs bold">Save</button> <button type="button" class="btn btn-danger btn-xs bold" onclick="$(this).parent().hide(\'slow\');">Cancel</button></form>';
                      //$('#topic-element'+topic_id).append(html);
                        console.log({'course_id':'$c_id','file_id':file_id,'topic_id':topic_id,'file_type':test_behaviour});
                      jQuery.ajax({
                        url: "$adminurl"+"course_product/course/ajax_add_file_to_topic",
                        method: 'post',
                        dataType: 'json',
                        data: {'course_id':'$c_id','file_id':file_id,'topic_id':topic_id,'file_type':test_behaviour},
                        success: function (data) {
                          $('#all-test-grid').DataTable().ajax.reload();
                          show_toast('success',test_behaviour+' added to course. Saved successfully',test_behaviour+' added');
                          $('.refresher').show();
                        }
                       });

                    }


                    $( ".add_media_element" ).click(function(){
                      action =  $(this).data('trigger');
                      topic_id = $(this).data('topic_id');

                      if(action =="pdf"){
                        $('input[name=show_file_type]').val(action);
                        $('input[name=active_topic_id]').val(topic_id);
                        $('.file-selector').val('1').change();
                        $('#myModal2').modal('show');
                        $(".file-modal-element-head").html("Add Pdf files to topic");
                      }
                      if(action =="ppt"){
                        $('input[name=show_file_type]').val(action);
                        $('.file-selector').val('2').change();
                        $('input[name=active_topic_id]').val(topic_id);
                        $('#myModal2').modal('show');
                        $(".file-modal-element-head").html("Add Ppt files to topic");
                      }
                      if(action =="epub"){
                        $('input[name=show_file_type]').val(action);
                        $('.file-selector').val('4').change();
                        $('input[name=active_topic_id]').val(topic_id);
                        $('#myModal2').modal('show');
                        $(".file-modal-element-head").html("Add Epub files to topic");
                      }
                      if(action =="video"){
                        $('.file-selector').val('3').change();
                        $('input[name=show_file_type]').val(action);
                        $('input[name=active_topic_id]').val(topic_id);
                        $('#myModal2').modal('show');
                        $(".file-modal-element-head").html("Add Video files to topic");
                      }
                      if(action =="doc"){
                        $('.file-selector').val('5').change();
                        $('input[name=show_file_type]').val(action);
                        $('input[name=active_topic_id]').val(topic_id);
                        $('#myModal2').modal('show');
                        $(".file-modal-element-head").html("Add Video files to topic");
                      }
                      if(action =="image"){
                        $('.file-selector').val('6').change();
                        $('input[name=show_file_type]').val(action);
                        $('input[name=active_topic_id]').val(topic_id);
                        $('#myModal2').modal('show');
                        $(".file-modal-element-head").html("Add flash cards to topic");
                      }
                      if(action =="concept"){
                        $('.file-selector').val('7').change();
                        $('input[name=show_file_type]').val(action);
                        $('input[name=active_topic_id]').val(topic_id);
                        $('#myModal2').modal('show');
                        $(".file-modal-element-head").html("Add Concept to topic");
                      }
                      jQuery("#all-file-grid").DataTable().draw();
                    });

                    $( ".add_test_practice" ).click(function(){
                        test_behaviour = "practice";
                        topic_id = $(this).data('topic_id');
                        $('input[name=active_topic_id]').val(topic_id);
                        $('#myModal_practice').modal('show');
                      });
  
                    $( ".add_test_element" ).click(function(){
                       test_behaviour = "test";
                      topic_id = $(this).data('topic_id');
                      $('input[name=active_topic_id]').val(topic_id);
                      $('#myModal3').modal('show');
                    });

                    /* this button is used to set faq positions */
                    $( "#faq_pos" ).click(function(){
                      var count = 1;
                      var array = {};
                      $('#sortable li').each(function(){
                         id =  $(this).data('faqid');
                         array[count] = id;
                         count++;
                      });
                          jQuery.ajax({
                            url: "$adminurl"+"course_product/course/update_faq_order",
                            method: 'post',
                            dataType: 'json',
                            data: array,
                            success: function (data) {
                              show_toast('success','Order of Faq in course saved successfully','Faq order');
                            }
                           });

                    });

                    /* this button is used to set topic  positions */
                    $( ".topic_pos" ).click(function(){
                      var count = 1;
                      var array = {};
                      $('#sortable_topic li').each(function(){
                         id =  $(this).data('faqid');
                         array[count] = id;
                         count++;
                      });
                      console.log(array);
                          jQuery.ajax({
                            // url: "$adminurl"+"course_product/course/update_topic_order/"+"$c_id",
                            url: "$adminurl"+"course_product/course/update_topic_name_order/"+"$c_id",
                            method: 'post',
                            dataType: 'json',
                            data: array,
                            success: function (data) {
                              show_toast('success','Order of topic in course saved successfully','Topic order');
                            }
                           });

                    });

                function getCookie(cn) {
                    var name = cn+"=";
                    var allCookie = decodeURIComponent(document.cookie).split(';');
                    var cval = [];
                    for(var i=0; i < allCookie.length; i++) {
                        if (allCookie[i].trim().indexOf(name) == 0) {
                            cval = allCookie[i].trim().split("=");
                        }
                    }
                    return (cval.length > 0) ? cval[1] : "";
                }
                /* show hide magic */
                  $('.prod-cat a').click(function (e) {
                    div =  $(this).data('div');
                    $('.tabu').hide();
                    $(this).tab('show');

                    var tabContent = '#tabContent' + div;
                    $(tabContent).show();

                    document.cookie = "activediv="+tabContent;
                  });


                  if(getCookie("activediv")){
                       $('.tabu').hide();
                     $(getCookie('activediv')).show();
                  }

                  jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = ""; 
                        $.each( data , function( key , value ) {
                          html += '<option value="'+value.id+'">'+value.name+'</option>';

                        });
                         $(".subject_element_select").html(html).val('$subject_id');
                         $('#subject_element_multiple').html(html).selectpicker('setStyle', 'btn-lg', 'add');;
                      }
                    });

                  jQuery.ajax({
                      url: "$adminurl"+"course_product/Course_category/get_main_category?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = '<option value="">--select--</option>';
                        $.each( data , function( key , value ) {
                          html += '<option value="'+value.id+'">'+value.name+'</option>';

                        });
                         $(".course_element_select").html(html);
                         $(".course_element_select").val('$course_main_fk').change();
                         
                      }
                    });


                    $( ".subject_element_select" ).change(function() {
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = '<option value="">--select--</option>';
                          
                          $.each( data , function( key , value ) {
                            html += '<option value="'+value.id+'">'+value.topic+'</option>';
                          });
                          $(".topic_element_select").html(html);
                        }
                      });
                    });

                    $( ".course_element_select" ).change(function() {
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/Course_category/get_main_category_stream/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = '<option value="">--select--</option>';
                          $.each( data , function( key , value ) {
                            html += '<option value="'+value.id+'">'+value.name+'</option>';
                          });
                           $(".stream_element_select").html(html);
                           $(".stream_element_select").val("$course_category_fk");
                        }
                      });
                    });

                     jQuery.ajax({
                      url: "$adminurl"+"course_product/course/get_all_instructor?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {
                        var html = '<option value="">--select--</option>';
                        $.each( data , function( key , value ) {
                          if(value.status == 0 || value.status == '$instructor_id'){
                             html += '<option value="'+value.id+'">'+value.name+'</option>';
                          }

                        });
                         $(".instructor_element_select").html(html);
                         $(".instructor_element_select").val('$instructor_id').change();
                      }
                    });

                    $( ".instructor_element_select" ).change(function() {
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/course/get_instructor_basic_info/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {
                          if(data){

                            html =  '<div class="panel-body bg-info "><a class="task-thumb" href="#"><img style="max-width:100px"  alt="" src="'+data.profile_picture+'"></a><div class="task-thumb-details" style="margin-left: 30px;" ><h1><a href="#">'+data.name+'</a></h1><p>'+data.email+'</p></div></div>';
                            $('#instructor_profile').html(html);
                            if(!data.name){
                              $('#instructor_profile').html("");
                            }
                          }else{
                              $('#instructor_profile').html("");
                          }

                        }
                      });
                    });

                    jQuery.ajax({
                        url: "$adminurl"+"course_product/Course/get_course_basic_information/"+$c_id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {
                          if(data){
                            $('input[name=title]').val(data.title);
                            $('select[name=course_main_fk]').val(data.course_main_fk);
                            $('textarea[name=tags]').val(data.tags);
                            $('input[name=mrp]').val(data.mrp);
                            $('input[name=for_dams]').val(data.for_dams);
                            $('input[name=non_dams]').val(data.non_dams);
                            $('input[name=fake_learner]').val(data.fake_learner);
                            if(data.cover_image != "" ){
                              html = '<img  style="max-width:100px;" src="'+data.cover_image+'">';
                              $('.cover_image_element').html(html);
                            }
							if(data.desc_header_image != "" ){
                              html = '<img  style="max-width:100px;" src="'+data.desc_header_image+'">';
                              $('.desc_header_image_element').html(html);
                            }
                            if(data.cover_video != "" ){
                              //html = '<iframe src="'+data.cover_video+'" width="200" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                              $('.cover_video_element').html(html);
                              
                            }
                            if(data.color_code != "" ){
                              //html = '<video style=""  width="200px"  class="sam" controls muted ><source  src="'+data.cover_video+'" ></video>';
                              $('#color').val(data.color_code);
                            }
                          }
                        }
                      });

                  jQuery(document).ready(function() {
                       var table = 'all-file-grid';
                       var dataTable_file = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/course/ajax_file_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );

                       $('.search-input-select-file').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            if(v !=""){
                                dataTable_file.columns(i).search(v).draw();
                            }
                        } );                       

                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_file.columns(i).search(v).draw();
                       } );
                    //    $('#subject_element_multiple').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                    //         console.log($(this).val());
                    //         var i = 3;
                    //         var v = $(this).val();
                    //         dataTable_file.columns(i).search(v).draw();
                    //     });

                    //     $('#subject_topic_element_multiple').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                    //         console.log($(this).val());
                    //         var i = 4;
                    //         var v = $(this).val();
                    //         dataTable_file.columns(i).search(v).draw();
                    //     });
                   } );


                  jQuery(document).ready(function() {
                       var table = 'all-test-grid';
                       var dataTable_test = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/course/ajax_test_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                        console.log("case ini ");
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_test.columns(i).search(v).draw();
                       } );
                       $('.search-input-select-test').on( 'change', function () {   // for select box
                            console.log('basic input change called and the value was');
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            console.log(v);
                            if(v !=""){
                                dataTable_test.columns(i).search(v).draw();
                            }
                        } );

                        // $('.test_type_element_multiple').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                        //     console.log("case one ");
                        //     var i = 1;
                        //     var v = $(this).val();
                        //     dataTable_test.columns(i).search(v).draw();
                        // });

                        // $('.exam_type_element_multiple').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                        //     console.log("case two ");
                        //     var i = 2;
                        //     var v = $(this).val();
                        //     dataTable_test.columns(i).search(v).draw();
                        // });

                        // $('.exam_sub_type_element_multiple').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                        //     console.log("case three ");
                        //     var i = 3;
                        //     var v = $(this).val();
                        //     dataTable_test.columns(i).search(v).draw();
                        // });
                   } );
                   /* course review section */


                    jQuery(document).ready(function() {
                       var table = 'all-course-review-grid';
                       var dataTable_review = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 4, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/course/ajax_course_review_list/"+"$c_id", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );

                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_review.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable_review.columns(i).search(v).draw();
                        } );
                        // Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date').val() !="" && $('#max-date').val() != "" ){
                                var dates = $('#min-date').val()+','+$('#max-date').val();
                                dataTable_review.columns(4).search(dates).draw();
                            }
                        });
                   } );

                   //all-quiz-grid
                   // table for quiz 
                    jQuery(document).ready(function() {
                        var table = 'all-quiz-grid';
                        var dataTable_quiz = jQuery("#"+table).DataTable( {
                            "processing": true,
                            "serverSide": true,
                            "order": [[ 0, "desc" ]],
                            "ajax":{
                                url :"$adminurl"+"course_product/course/ajax_quiz_list/", // json datasource
                                type: "post",  // method  , by default get
                                error: function(){  // error handling
                                    jQuery("."+table+"-error").html("");
                                    jQuery("#"+table+"_processing").css("display","none");
                                }
                            }
                        } );

                        jQuery("#"+table+"_filter").css("display","none");
                        $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                            var i =$(this).attr('data-column');  // getting column index
                            var v =$(this).val();  // getting search input value
                            dataTable_quiz.columns(i).search(v).draw();
                        } );
                    } );
               </script>
              <script type="text/javascript">
                  $(document).ready(function () {
                      var form = $("#add_topic_from_select");
                      form.validate({
                          errorPlacement: function errorPlacement(error, element) {
                              element.after(error);
                          }
                      });
                  });

              </script>
              <script type="text/javascript">
                  $(document).ready(function () {
                      var form = $("#add_topic_from");
                      form.validate({
                          errorPlacement: function errorPlacement(error, element) {
                              element.after(error);
                          }
                      });
                  });
              </script>

              <script type="text/javascript">
                  $(document).ready(function () {
                      var form = $("#add_topic_to_course");
                      form.validate({
                          errorPlacement: function errorPlacement(error, element) {
                              element.after(error);
                          }
                      });
                  });
              </script>


              <script type="text/javascript">

                  $(document).ready(function () {
                      var form = $("#edit_topic_from");
                      form.validate({
                          errorPlacement: function errorPlacement(error, element) {
                              element.after(error);
                          }
                      });

                      var form = $("#edit_basicinfo_from");

                      jQuery.validator.addMethod("max_price", function(value, element) {
                        var max = $('input[name=mrp]').val();
                        if(parseFloat(value) > parseFloat(max)){
                             console.log(value+"-"+max);
                             return false;
                        }else{
                        return true;
                        }
                      }, "Please provide value equal or less than mrp.");


                      form.validate({
                          errorPlacement: function errorPlacement(error, element) {
                              element.after(error);
                          },
                          rules: {
                            mrp: {
                              required: true,
                              min: 0
                            },
                            course_sp:{
                                required: true,
                                min: 0,
                                max_price : true 
                            }
                          }
                      });
                  });

				  $('#min-date').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true

					});
					$('#max-date').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true

					});
					$('#min-date-course').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true

					});
					$('#max-date-course').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true

					});

              </script>
              <script>
              /* jquery for filtration highly senstive */

                  //get all years
                  $.ajax({
                      url:"$adminurl"+"course_product/Course/years_list_for_filtration",
                      dataType: 'json',
                      method: 'POST',
                      success : function(data) {

                        html = '<div class="checkbox">';
                        $.each( data , function( key , value ) {
                          html +=  '<label class="lbl_block"><input type="checkbox" value="'+value.Session+'"> '+ value.Session +'</label>' ;
                        });
                        html +='</div>';
                         $(".year_segment").html(html);
                      }
                     });

                     /* if clicked on radio button of year */
                     $(document).on('click', '.year_segment input[type=checkbox]', function () {
                              if (this.checked){
                                 year = $(this).val();
                                 $.ajax({
                                  url:"$adminurl"+"course_product/Course/franchise_with_year/"+year,
                                  dataType: 'json',
                                  method: 'POST',
                                  success : function(data) {
                                    html = '<div  class="fix-height checkbox main_record_'+year+' fr_year_list"><p class="help-block">Select franchise for year '+year+'</p>';
                                    $.each( data , function( key , value ) {
                                       html +=  '<label  class="lbl_block" ><input data-year ="'+year+'" type="checkbox" value="'+value.FranchiseName+'"> '+ value.FranchiseName +'</label>' ;
                                    });
                                    html +='</div>';
                                     $(".fr_segment").append(html);
                                  }
                                 });


                              } else{
                                  year = $(this).val();
                                  $('.main_record_'+year).remove();
                                  $('.handle_'+year).remove();
                              }
                    });

                    /* while clicking on franchise checkbox */
                    $(document).on('click', '.fr_year_list input[type=checkbox]', function () {
                                $('.c_segment').html('');
                              $('.fr_year_list input[type=checkbox]').each(function () {
                              if (this.checked){
                                 franchise = $(this).val();
                                 year = $(this).data('year');
                                 $.ajax({
                                  url:"$adminurl"+"course_product/Course/courses_group_with_year_and_franchise/"+franchise+"/"+year,
                                  dataType: 'json',
                                  method: 'POST',
                                  async: false,
                                  success : function(data) {
                                    html = '<div  data-franchise="'+franchise+'" class=" fix-height handle_'+year+' checkbox main_course_record_'+year+franchise.replace(/ /g,"_")+' fr_course_list"><p class="help-block">Select CourseGroup for year '+year+' , '+franchise+'</p>';
                                    $.each( data , function( key , value ) {
                                       html +=  '<label  class="lbl_block" ><input data-franchise="'+franchise+'"  data-year ="'+year+'" type="checkbox" value="'+value.CourseGroup+'"> '+ value.CourseGroup +'</label>' ;
                                    });
                                    html +='</div>';
                                     $(".c_segment").append(html);

                                  }
                                 });
                              }else{
                                  franchise = $(this).val();
                                  $('div[data-franchise="'+franchise+'"]').remove();
                              }
                          });
                    });


                      // Generating courses for course group selected

                       $(document).on('click', '.fr_course_list input[type=checkbox]', function () {
                                $('.cc_segment').html('');
                              $('.fr_course_list input[type=checkbox]').each(function () {

                              if (this.checked){
                                 course_group = $(this).val();
                                 year = $(this).data('year');
                                 franchise = $(this).data('franchise');
                                 $.ajax({
                                  url:"$adminurl"+"course_product/Course/courses_with_year_and_franchise_and_course_group/"+franchise+"/"+year+"/"+course_group,
                                  dataType: 'json',
                                  method: 'POST',
                                  async: false,
                                  success : function(data) {
                                    html = '<div  data-franchise="'+franchise+'" class=" fix-height handle_'+year+'  checkbox main_course_record_'+year+franchise.replace(/ /g,"_")+course_group+' fr_course_child_list"><p class="help-block">Select Course for year '+year+' , '+franchise+' , course_group- '+course_group+'</p>';
                                    $.each( data , function( key , value ) {
                                       html +=  '<label  class="lbl_block" ><input data-course_group="'+course_group+'" data-franchise="'+franchise+'"  data-year ="'+year+'" type="checkbox" value="'+value.Course+'"> '+ value.Course +'</label>' ;
                                    });
                                    html +='</div>';
                                     $(".cc_segment").append(html);

                                  }
                                 });
                              }
                          });
                    });

                      / * getting batches */
                      $(document).on('click', '.fr_course_child_list input[type=checkbox]', function () {
                          $('.course_segment').html('');
                           $('.fr_course_child_list input[type=checkbox]').each(function () {

                        if (this.checked){
                           course_name = $(this).val();
                           course = btoa(course_name);
                           console.log(course);
                           course_group = $(this).data('course_group');
                           year = $(this).data('year');
                           franchise = $(this).data('franchise');

                           $.ajax({
                            url:"$adminurl"+"course_product/Course/batches_with_year_and_franchise_and_course_group_and_course/"+franchise+"/"+year+"/"+course_group+"/?course="+course,
                            dataType: 'json',
                            method: 'POST',
                            async: false,
                            success : function(data) {
                              html = '<div style ="display:" data-franchise="'+franchise+'" class="fix-height handle_'+year+'  checkbox main_course_record_'+year+franchise.replace(/ /g,"_")+course_group+course_name+'"><p class="help-block">Select Course for year '+year+' , '+franchise+' , course_group- '+course_group+'</p>';
                              $.each( data , function( key , value ) {
                                 html +=  '<label  class="lbl_block" ><input data-course="'+course_name+'" data-course_group="'+course_group+'" data-franchise="'+franchise+'"  data-year ="'+year+'" type="checkbox" value="'+value.Batch+'"> '+ value.Batch +'</label>' ;
                              });
                              html +='</div>';
                               $(".course_segment").append(html);
                            }
                           });
                        }
                    });
              });

              /* save all batches */

              $(document).on('click', '.save_filtration ', function () {
                  var values = [];
                  $('.course_segment input[type=checkbox]').each(function () {
                      if (this.checked){
                          var temp = {};
                          temp.course_id  = '$c_id';
                          temp.Session  = $(this).data('year');
                          temp.FranchiseName = $(this).data('franchise');
                          temp.CourseGroup  = $(this).data('course_group');
                          temp.Course = $(this).data('course');
                          temp.Batch  = $(this).val();
                          values.push(temp);
                      }
                  });
                  $.ajax({
                      url:"$adminurl"+"course_product/Course/save_batches_record",
                      dataType: 'json',
                      data: { data : values},
                      method: 'POST',
                      success : function(data) {
                          console.log(123);
                          location.reload();
                      }
                     });

              });
              jQuery(document).ready(function() {
                var table = 'allfiltration-video-grid';
                var dataTable_video_comments = jQuery("#"+table).DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "order": [[ 0, "desc" ]],
                    "ajax":{
                        url :"$adminurl"+"course_product/Course/ajax_applied_filter_list/"+"$c_id", // json datasource
                        type: "post",  // method  , by default get
                        error: function(){  // error handling
                            jQuery("."+table+"-error").html("");
                            jQuery("#"+table+"_processing").css("display","none");
                        }
                    }
                } );

                jQuery("#"+table+"_filter").css("display","none");
                $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                    var i =$(this).attr('data-column');  // getting column index
                    var v =$(this).val();  // getting search input value
                    dataTable_video_comments.columns(i).search(v).draw();
                } );




                $( "#add_course_type" ).click(function(){
                        topic_id = $(this).data('topic_id');
                        practice_id = $(this).data('practice_id');
                        $('input[name=active_topic_id]').val(topic_id);
                        $('input[name=practice_id]').val(practice_id);

                         $('#myModal5').modal('show');
                      });

                      $( "#add_content_type" ).click(function(){
            
                        topic_id = $(this).data('topic_id');
                        practice_id = $(this).data('practice_id');
                        $('input[name=active_topic_id]').val(topic_id);
                        $('input[name=practice_id]').val(practice_id);

                         $('#myModal6').modal('show');
                      });

            } );
            
           

            

			$("#add_faq_form").on('submit',(function(e) {
				var question = $('#faq_question').val();
				var description = $('#faq_description').val();
				var course_id = $('#faq_course_id').val();
				var faq_id = $('#faq_id').val();
				e.preventDefault();

			    $.ajax({
					url: "$adminurl"+"course_product/course/add_faq",
					type: "POST",
					dataType: 'json',
					data: {question: question,description: description,course_id:course_id,faq_id:faq_id},
					success: function(data){
						if(data.status == true){
							if(data.code == 1){
								show_toast('success', 'FAQ added to course successfully !!','FAQ Added ');
							}
							else if(data.code == 2){
								show_toast('success', 'FAQ updated to course successfully !!','FAQ Updated ');
							}

							location.reload();
						 }
						 else if(data.status == false){
							//$(".faq_error").html(data.data_value);
							show_toast('error', 'Both fields are required !!','FAQ Not Added ');
						 }
					}
		   		});

			}));

			$( ".add_faq_button" ).click(function(){
                     $('.add_faq_div').show();
            });



			$( ".edit_faq_data" ).click(function(){


				var course_id = $(this).data('course-id');
				var question = $(this).data('question');
				var description = $(this).data('description');
				var faq_id = $(this).data('faq-id');


				$('#faq_question').val(question);
				$('#faq_description').val(description);
				$('#faq_id').val(faq_id);

				$('.add_faq_div').show();
            });

              </script>

        <script src="http://18.223.244.127/web_socket/examples/js/socket.js"></script>
        <script type="text/javascript" language="javascript" >
            /***
             *       _____               _          _     __  __                _
             *      / ____|             | |        | |   |  \/  |              (_)
             *     | (___    ___    ___ | | __ ___ | |_  | \  / |  __ _   __ _  _   ___
             *      \___ \  / _ \  / __|| |/ // _ \| __| | |\/| | / _` | / _` || | / __|
             *      ____) || (_) || (__ |   <|  __/| |_  | |  | || (_| || (_| || || (__
             *     |_____/  \___/  \___||_|\_\___| \__| |_|  |_| \__,_| \__, ||_| \___|
             *                                                            __/ |
             *                                                           |___/
             */
          var socket=$.websocket('ws://.com:2000');

            function fire_push(){
                json_var = {};
                json_var.users_type =  "$notification_user_type";
                json_var.users_message =  $('#push_notification_text').val();
                json_var.device_type = "ALL";
                json_var.notification_type = 'course_section';
                json_var.notification_text = "$c_id";
                console.log(json_var);
                socket.emit('sync_process', json_var);
                return false;
            }

            socket.on('sync_process', function(msg){
              $('#show_socket_state').text(msg);
              //console.log(msg);
            });
            socket.on('connect', function(user){
              console.log('web_socket connected');
            });

          socket.listen();
        </script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.example-getting-started').multiselect({
            enableFiltering: true,
            /*includeSelectAllOption: true,*/
            buttonWidth: '100%',
            maxHeight: 400,
            onChange: function(option, checked) {
              //alert(option.length + ' options ' + (checked ? 'selected' : 'deselected'));
              console.log(option.val());

            }
        });

        $( ".filtration_price_button" ).change(function() {
          if($(this).val() == 'free'){
            $('.filtration_element_price_div').hide('fast');
            $('.filtration_element_price_input').val('');
          }else{
            $('.filtration_element_price_div').show('slow');
          }
        }).change();

      });
</script>

<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}    


    /* on button click add topic element */
    
    $( ".add_main_head" ).click(function() {
        if (!$(this).hasClass('disabled')){
            var name = $(this).data('name');
            var type = $(this).data('type');
            $('#add_topic_to_course input[name=topic_name]').val(name);
            $('#add_topic_to_course input[name=topic_type]').val(type);
            $('#add_topic_to_course').submit();
            $('.fix-loader').html(loading);
        }
    });
    var loading = '<div id="divLoading" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 0.8;"><p style="position: absolute; color: White; top: 50%; left: 45%;"><i class="fa fa-spin fa-refresh fa-4x"></i></p></div>';
</script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
