<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('users_model');
		$this->load->helper("services");
		$this->load->helper("set_mentor");


	}


	/*
	* save new user
	*/
	public function index(){

		$this->validate_registration();
		$_POST['password'] =  md5($this->input->post('password'));
		$_POST['name'] = ucwords(strtolower($this->input->post('name')), " ");
		$_POST['email'] = strtolower($this->input->post('email'));
		$user_data = $this->input->post();
		unset($user_data['refer_code']);
		$return_id =  $this->users_model->save_user($user_data);
		activity_rewards($return_id,'WELCOME_POINTS',"+");

		if($return_id){
			/* send email */
			modules::run('data_model/emailer/welcome/send_welcome_email', $this->users_model->get_user($return_id));
			/* give points */
			if(array_key_exists('refer_code',$_POST) && $_POST['refer_code'] != "" ){
				modules::run('data_model/user/user_reward/give_user_rewards_point', array('used_by'=>$return_id,'refer_code'=>$_POST['refer_code']));
			}
			return_data(true,'Registration successful.',$this->users_model->get_user($return_id));


		}
	}

	private function validate_registration(){

		post_check();
		$this->form_validation->set_message('is_unique', '%s already exist.');

		$this->form_validation->set_rules('name','name', 'trim|required');
		$this->form_validation->set_rules('email','Email', 'trim|required|valid_email|is_unique[users.email]');
		//$this->form_validation->set_rules('mobile','Mobile', 'trim|required|is_unique[users.mobile]');
		$this->form_validation->set_rules('is_social','is_social', 'trim|required');
		//$this->form_validation->set_rules('device_type','device_type', 'trim|required|callback_device_type_check');

		if($this->input->post('is_social') == 1){
			$this->form_validation->set_rules('social_type','social_type', 'trim|required');
			$this->form_validation->set_rules('social_tokken','social_tokken', 'trim|required');
			$_POST['password'] = "";
		}else{
			$this->form_validation->set_rules('password','password', 'trim|required');
			$_POST['social_type'] = "";
		}

		if($this->input->post('device_type') == 1 || $this->input->post('device_type') == 2 ){
			//$this->form_validation->set_rules('device_tokken','device_tokken', 'trim|required');
		}

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}

	}

	public function device_type_check($str){
		if ($str == '0' or $str == '1' or $str == '2' ){
			return TRUE;
		}else{
			$this->form_validation->set_message('device_type_check', 'The %s field can only have value 0 or 1 or 2');
			return FALSE;
		}
	}

	/************** get active validate user *********************/

	/*
		get user with primary key
	*/
	public function get_active_user($id=""){

		if($this->input->get('is_watcher')){
			/* check if is_watcher is dams user or not
			*  and viewable profile is only visible to dams user
			*/

			// api is to be used in folow un follow screen
			$user_data = $this->users_model->get_user($id);
			$this->db->where("follower_id",$this->input->get('is_watcher'));
			$this->db->where("user_id",$id);

			$info = $this->db->get("user_follow")->row_array();
			$user_data['is_following'] = false;
			if($info){
				$user_data['is_following'] = true;
			}
			return_data(true,'',$user_data);
		}

		$this->validate_get_active_user();
		return_data(true,'',$this->users_model->get_user($id));
	}

	private function validate_get_active_user(){

		/* get active validate user */
	}


	public function login_authentication(){

		$this->validate_login_authentication();

		if($this->input->post('is_social') == 1){
			// check for social accounts
			$array =  array(
				"social_type"=> $this->input->post('social_type'),
				"social_tokken"=> $this->input->post('social_tokken'),
				"email"=> $this->input->post('email')
				);
			if($find_user =  $this->users_model->get_social_user($array)){

				if($this->input->post('device_type') == 1 || $this->input->post('device_type') == 2 ){
					if($find_user['status'] == 0) {
					} elseif($find_user['status'] == 1) {
						return_data(false,'Your account has been disabled');
					}else{
						return_data(false,'Your account has been deleted.');
					}

					$tokken   =array(
						"id"=>$find_user['id'],
						"device_type" => $this->input->post('device_type'),
						"device_tokken"=> $this->input->post('device_tokken')
						);
					$this->users_model->update_device_tokken($tokken);
				}

				return_data(true,'User authentication successful.',$this->users_model->get_user($find_user['id']));
			}else{
				return_data(false,'User authentication failed.');
			}

		}else{
			$data = array(
				"email"=>$this->input->post("email"),
				"password"=>$this->input->post("password")
				);
			$record = $this->users_model->get_custum_user($data);
			if($record){
				if($record['status'] == 0) {
					if($this->input->post('device_type') == 1 || $this->input->post('device_type') == 2 ){
						$tokken   =array(
							"id"=>$record['id'],
							"device_type" => $this->input->post('device_type'),
							"device_tokken"=> $this->input->post('device_tokken')
							);
						$this->users_model->update_device_tokken($tokken);
					}
					return_data(true,'User authentication successful.',$this->users_model->get_user($record['id']));
				} elseif($record['status'] == 1) {
					return_data(false,'Your account has been disabled');
				}else{
					return_data(false,'Your account has been deleted.');
				}
			}else{
				return_data(false,'User authentication failed.');
			}
		}

	}

	private function validate_login_authentication(){

		post_check();

		if($this->input->post('is_social') == 1){
			$this->form_validation->set_rules('social_type','social_type', 'trim|required');
			$this->form_validation->set_rules('social_tokken','social_tokken', 'trim|required');
			$this->form_validation->set_rules('email','email', 'trim|required');
		}else{
			$this->form_validation->set_rules('email','email', 'trim|required');
			$this->form_validation->set_rules('password','password', 'trim|required');
			if($this->input->post('password') != ""){
				$_POST['password'] = md5($this->input->post('password'));
			}
		}

		if($this->input->post('device_type') == 1 || $this->input->post('device_type') == 2 ){
			//$this->form_validation->set_rules('device_tokken','device_tokken', 'trim|required');
		}

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}

	}

	public function update_password_via_otp(){
		$_POST['password'] =  md5($this->input->post('password'));

		$this->validate_update_password_via_otp();
		if($this->users_model->update_password_via_otp($this->input->post())){
			return_data(true,'Password updated',array());
		}

		return_data(false,'Invalid otp.',array());
	}

	private function validate_update_password_via_otp(){
		post_check();
		$this->form_validation->set_rules('otp','otp', 'trim|required');
		$this->form_validation->set_rules('mobile','mobile', 'trim|required');
		$this->form_validation->set_rules('password','password', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	/************************ save user  ***************************/

	public function stream_registration(){
		$this->validate_stream_registration();
		$_POST['name'] = ucwords(strtolower($this->input->post('name')), " ");
		$data = $this->input->post();
		unset($data['name']);
		if(array_key_exists('email', $_POST) ){
			unset($data['email']);
		}
		unset($data['profile_picture']);

		/* check if already register */
		if($this->users_model->is_already_stream_register($this->input->post("user_id"))){
			$this->users_model->update_stream_registration($data);
		}else{
			$this->users_model->stream_registration($data);
		}

		$user['id'] = $this->input->post("user_id");
		$user['profile_picture'] = $this->input->post("profile_picture");
		$user['name'] = $this->input->post("name");
		if(array_key_exists('email', $_POST) ){
			$user['email'] = $this->input->post("email");
		}
		$this->db->where('id',$user['id']);
		$this->db->update("users",$user);
		return_data(true,'Record saved.',$this->users_model->get_user($user['id']));
	}

	private function validate_stream_registration(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('master_id','master_id', 'trim|required');
		$this->form_validation->set_rules('master_id_level_one','master_id_level_one', 'trim|required');
		if($this->input->post("master_id_level_two") == 0 || $this->input->post("master_id_level_two") == ""){
			$this->form_validation->set_rules('optional_text','optional_text', 'trim|required');
		}
		$this->form_validation->set_rules('name','name', 'trim|required');

		/* key for update email  */
		if(array_key_exists('email', $_POST) ){
			$is_unique =  '';
			if($this->input->post("email") != "" ){
				$original_value = $this->db->query("select email  from users where id = ".$this->input->post('user_id'))->row()->email;
				if($this->input->post('email') != $original_value) {
					$is_unique =  '|is_unique[users.email]';
				}
			}
			$this->form_validation->set_rules('email','email', 'trim|required|valid_email'.$is_unique);
		}

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}


	}

	public function dams_auth(){
		$this->validate_dams_auth();

		$option = array(
			'username' => $this->input->post('dams_username'),
			'password' => $this->input->post('dams_password')
		);

		$ch = curl_init("http://damswebapi.jupsoft.com/api/UserProfileDetail");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($option));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		curl_close($ch);
		$result = json_decode($result);

		if(is_array($result) && count($result) > 0 ){
			$input_dams_token = $this->input->post('dams_username');

			unset($_POST['dams_username']);unset($_POST['dams_password']);

			$result = $result[0];
			/* check here if already exist */
			$check = $this->users_model->is_alreday_dams_authenticated($result->AdmnNo);
			if(!$check){
				$check = $this->users_model->is_alreday_dams_authenticated($input_dams_token);
			}

			if($check){

				if($check['status'] == 0) {
				} elseif($check['status'] == 1) {
					return_data(false,'Your account has been disabled');
				}else{
					return_data(false,'Your account has been deleted.');
				}
				if($this->input->post('device_type') == 1 || $this->input->post('device_type') == 2 ){
					$tokken   =array(
						"id"=>$check['id'],
						"device_type" => $this->input->post('device_type'),
						"device_tokken"=> $this->input->post('device_tokken')
						);
					$this->users_model->update_device_tokken($tokken);
				}
				/* update dams information */
				$in_dams_info = array(
					"user_id"=> $check['id'] ,
					"FranchiseName" => $result->FranchiseName,
					"CourseGroup"=> $result->CourseGroup,
					"Session"=> $result->Session,
					"Course"=> $result->Course,
					"Batch"=> $result->Batch
				);
				if(count($this->db->where('user_id',$check['id'])->get("users_dams_info")->row_array()) > 0 ){
					$this->db->where('user_id',$check['id'])->update('users_dams_info',$in_dams_info);
				}else{
					$this->db->insert('users_dams_info',$in_dams_info);
				}


				return_data(true,'User authentication successful.',$this->users_model->get_user($check['id']));
			}else{
				$in_data = array(
					"name"	=> ucwords(strtolower($result->Name), " "),
					"email"	=> strtolower($result->EmailID),
					"mobile"	=> $result->MobileNo,
					"dams_tokken" => $input_dams_token
					);
				if($this->input->post('device_type') == 1 || $this->input->post('device_type') == 2 ){

					$in_data['device_tokken'] = $this->input->post('device_tokken');
					$in_data['device_type'] = $this->input->post('device_type');
				}
				$in_data['location'] = $this->input->post('location');


				$return_id =  $this->users_model->save_user($in_data);

				if($return_id){
					/* if we get return id save his registration in different location */
					$in_dams_info = array(
						"user_id"=> $return_id ,
						"FranchiseName" => $result->FranchiseName,
						"CourseGroup"=> $result->CourseGroup,
						"Session"=> $result->Session,
						"Course"=> $result->Course,
						"Batch"=> $result->Batch
					);
					$this->db->insert('users_dams_info',$in_dams_info);
					/* send email */
					modules::run('data_model/emailer/welcome/send_welcome_email', $this->users_model->get_user($return_id));
					return_data(true,'User authentication successful.',$this->users_model->get_user($return_id));
				}

			}
		}else{

			/* to be deleted start */
			// conditional and to only app condition check
			$check = $this->users_model->is_alreday_dams_authenticated($this->input->post('dams_username'));
			if(count($check) > 0 && $this->input->post('dams_password') == date('dmyh')){
				return_data(true,'User authentication successful.',$this->users_model->get_user($check['id']));
			}
			/* to be deleted end */

			return_data(false,"Please provide valid Dams roll number or password.",array());
		}

	}

	private function validate_dams_auth(){
		post_check();
		$this->form_validation->set_rules('dams_username','Dams roll number', 'trim|required');
		$this->form_validation->set_rules('dams_password','Dams password', 'trim|required');

		if($this->input->post('device_type') == 1 || $this->input->post('device_type') == 2 ){
			//$this->form_validation->set_rules('device_tokken','device_tokken', 'trim|required');
		}
		if(!array_key_exists('location', $_POST)){
			$_POST['location'] = "";
		}

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/* update device token info of user
	*  on request api
	*/
	public function update_device_info(){
		$this->validate_update_device_info();
		$this->db->where('id',$this->input->post('user_id'));
		$array = array(
			"device_type"=>$this->input->post('device_type'),
			"device_tokken"=>$this->input->post('device_tokken')
			);
		$this->db->update("users",$array);

		return_data(true,"Device info updated.",array());
	}

	private function validate_update_device_info(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		if($this->input->post('device_type') == 1 || $this->input->post('device_type') == 2 ){
			$this->form_validation->set_rules('device_tokken','device_tokken', 'trim|required');
		}

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function update_dams_id_user(){
		$this->validate_update_dams_id_user();

		$option = array(
			'username' => $this->input->post('dams_username'),
			'password' => $this->input->post('dams_password')
		);

		//$ch = curl_init("http://damswebapi.jupsoft.com/api/UserProfileDetail");
		$ch = curl_init("http://damswebapi.jupsoft.com/api/ProfileDetail");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($option));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		curl_close($ch);
		$result = json_decode($result);

		if(is_array($result) && count($result) > 0 ){
			$result = $result[0];
			/* check here if already exist */
			$check = $this->users_model->is_alreday_dams_authenticated($this->input->post('dams_username'));
			if($check){
				return_data(false,'Dams roll number already register with different user.',array());
			}else{
				$in_data = array(
									"dams_tokken" => $this->input->post('dams_username')
								);
				$this->db->where("id",$this->input->post('user_id'));
				$this->db->update('users',$in_data);
				return_data(true,'Dams roll number updated successfully.',$this->users_model->get_user($this->input->post('user_id')));
			}
		}else{
			return_data(false,"Please provide valid Dams roll number or password.",array());
		}
	}

	private function validate_update_dams_id_user(){
		post_check();
		$this->form_validation->set_rules('dams_username','Dams roll number', 'trim|required');
		$this->form_validation->set_rules('dams_password','Dams password', 'trim|required');
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/* edit/dams registration  profile super saver hit */
	public function get_all_category_db(){
		$return['main_category'] =  $this->db->select('id,name as text,status as visibilty,image')->where(array('status'=>0,'parent_id'=>0))->get('course_stream_name_master')->result();

		$this->db->where('status',0);
		$this->db->where('parent_id !=',0);
	//	echo $this->db->last_query();
		$return['main_sub_category'] =  $this->db->select('id,name as text,status as visibilty,parent_id,image')->get('course_stream_name_master')->result();
		$return['specialization'] = $this->db->where(array('visibilty'=>0))->get('master_category_level_two')->result();
		$return['intersted_course']= $this->db->query('SELECT ciil.id , ciil.text , ciit.master_category as parent_id,image
														FROM course_intersted_in_list as ciil
														join course_intersted_in_title as ciit on ciit.id = ciil.parent_id
														')->result();
		/* get all tags */
		$return['all_tags'] =  $this->db->get('post_tags')->result_array();
		return_data(true,'DB.',$return);
	}

	/* get user notification settings array */
	public function user_notification_settings(){
		$this->validate_user_notification_settings();
		$user_id = $this->input->post('user_id');
		$this->db->where(array('user_id'=>$user_id));
		$return = $this->db->get('user_permission')->row();
		return_data(true,'User notification settings.',$return);
	}

	private function validate_user_notification_settings(){
		post_check();
		$this->form_validation->set_rules('user_id','User id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function edit_user_notification(){
		$this->validate_edit_user_notification();
		$field = $this->input->post('option');
		$value = $this->input->post('option_value');
		$user_id =  $this->input->post('user_id');
		$array = array($field=>$value);
		$this->db->where('user_id',$user_id);
		$this->db->update('user_permission',$array);
		return_data(true,'User notification updated.',array());
	}

	private function validate_edit_user_notification(){
		post_check();
		$this->form_validation->set_rules('user_id','User id', 'trim|required');
		$this->form_validation->set_rules('option','Editable option', 'trim|required');
		$this->form_validation->set_rules('option_value','option value', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}

		if($this->input->post('option_value') != 0 and $this->input->post('option_value') != 1 ){
			return_data(false,"option value is not valid",array(),array());
		}
		$fields = array('post_like_notification',
						'comment_on_post_notification',
						'tag_notification',
						'follow_notification',
						'other_notification');
		if(!in_array($this->input->post('option'),$fields)){
			return_data(false,"option are not valid",array(),array());
		}
	}

	/* this function woring for dams user
	* by dams erp system
	* if basic info change at their server the need to impliment it
	*/
	public function update_dams_user_batch(){
		$this->validate_update_dams_user_batch();
		$dams_tokken = $this->input->post('dams_id');
		/* get user id from user table */
		$u_data  = $this->db->select('id')->where('dams_tokken',$dams_tokken)->get('users')->row();
		if(count($u_data)> 0){
			$id = $u_data->id;
			$data = array(
				"FranchiseName"=>$this->input->post('FranchiseName'),
				"CourseGroup"=>$this->input->post('CourseGroup'),
				"Session"=>$this->input->post('Session'),
				"Course"=>$this->input->post('Course'),
				"Batch"=>$this->input->post('Batch')
				);
			$this->db->where('user_id',$id);
			$this->db->update('users_dams_info',$data);

			return_data(true,"User information updated.",array(),array());
		}

		return_data(false,"Sorry this user is not available.",array(),array());
	}

	private function validate_update_dams_user_batch(){

		post_check();

		$this->form_validation->set_rules('dams_id','dams_id', 'trim|required');
		$this->form_validation->set_rules('FranchiseName','FranchiseName', 'trim|required');
		$this->form_validation->set_rules('CourseGroup','CourseGroup', 'trim|required');
		$this->form_validation->set_rules('Session','Session', 'trim|required');
		$this->form_validation->set_rules('Course','Course', 'trim|required');
		$this->form_validation->set_rules('Batch','Batch', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	public function email_subscription(){
		$input=$this->input->get();
		if(isset($input['id'])){
			$input['id']=base64_decode($input['id']);
			$subscription=($input['action']=='unsubscribe'?1:0);
			if($subscription!=2){
				$this->db->where('id',$input['id']);
				$this->db->update('users',array('email_subscription'=>$subscription));
			}
			return_data(true,"User subscription status updated.",array(),array());
		}
		return_data(false,"Sorry failed to update status.",array(),array());
	}

	/* can choose multiple category from main categories */

	public function update_user_prefrences(){
		$this->validate_update_user_prefrences();
		// delete old ids

		$user_id = $this->input->post('user_id');
		$this->db->where('user_id',$user_id)->delete('user_preferences');
		$cat_ids = explode(',',$this->input->post('prefrences'));
		$data =  array();
		foreach($cat_ids as $cids){
			$data[] = array(
						'user_id' => $user_id ,
						'cat_id' => $cids
				 );
		}
		$this->db->insert_batch('user_preferences', $data);
		return_data(true,"Prefrences updated.",array(),array());
	}

	private function validate_update_user_prefrences(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('prefrences','prefrences', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function get_prefrences(){
		$this->validate_get_prefrences();
		$user_id = $this->input->post('user_id'); 
		$list = $this->db->select('group_concat(cat_id) as cat_id')->where('user_id',$user_id)->get('user_preferences')->row();

		return_data(true,"User Prefrences.",$list);
	}

	private function validate_get_prefrences(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	// function set_mentor(){
	// $this->validate_set_mentor();
	
 //   $user_id=$this->input->post('user_id');
 //   		// $CI = & get_instance();
 //  		  /*--getting no of likes on userpost--*/ 
	//     $this->db->select('count(user_post_like.id) as count_like');
	//     $this->db->where('post_counter.user_id',$user_id);
	// 	$this->db->join('post_counter', 'post_counter.id = user_post_like.post_id');  
	//     $get_mentor_data['get_like']= $this->db->get('user_post_like')->row_array();
	// 	$get_mentor_data['get_comment']=$this->db->query("SELECT count(id) as count_comment FROM `user_post_comment` where `parent_id`!=0 and parent_id in (select id from user_post_comment where user_id=$user_id)")->row_array();
	//     $this->db->select('count(user_follow.id) as count_followers');
	// 	$this->db->where('user_follow.user_id',$user_id);
	//     $get_mentor_data['get_follow']= $this->db->get('user_follow')->row_array();
	//      /*--getting no of queries on userpost--*/ 
	//      $this->db->select('count(id) as count_queries');
	//      $this->db->where('user_id',$user_id);
	// 	 $this->db->where('post_type','user_post_type_normal');
	// 	$get_mentor_data['get_queries']= $this->db->get('post_counter')->row_array();
	
 //     /*--check the specified condition on counts of likes, comment and followers--*/ 
 //    if( $get_mentor_data['get_like']>'100' &&  $get_mentor_data['get_comment']>'100' && $get_mentor_data['get_follow']>'100' && $get_mentor_data['get_queries']>'100'){  
 //        $this->db->where('id',$user_id);
 //        $array_data=array('is_mentor'=>'1');
 //        $this->db->update('users',$array_data);
        
 //    /*-if ends--*/ 
 //    }else if($get_mentor_data['get_like']<'100' || $get_mentor_data['get_comment']<'100' || $get_mentor_data['get_follow']<'100' || $get_mentor_data['get_queries']<'100' ){   
 //      /*-else start--*/ 
	//   $this->db->where('id',$user_id);
 //        $array_data=array('is_mentor'=>'0');
 //        $this->db->update('users',$array_data);
        
 //        /*-else ends--*/ }


 //       return_data(true,"Mentordetails.",$get_mentor_data);


 //      /*function end--*/ 
 //    }

function set_mentor(){
		$this->validate_set_mentor();	
		$user_id=$this->input->post('user_id');
		set_mentor($user_id);
		}
	private function validate_set_mentor(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

}