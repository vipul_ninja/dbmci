<div class="col-lg-12">
<section class="panel">
        <header class="panel-heading">
            Edit Stream
        </header>
        <div class="panel-body">
            <form role="form" method="POST" enctype="multipart/form-data">
              <?php if($stream['id']) { ?>
              <input type="hidden" id="id" value="<?php echo $stream['id'];?>">              
              <?php } ?>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="" name="name" value="<?php echo $stream['name']; ?>"/>
                    <span class="text-danger"><?php echo form_error('name');?></span>
                </div>                
                <div class="form-group">
                    <label for="name">Name (Hindi)</label>
                    <input type="text" class="form-control" id="" name="name_2" value="<?php echo $stream['name_2']; ?>"/>
                    <span class="text-danger"><?php echo form_error('name_2');?></span>
                </div>   
                <div>             
                     <label for="name">Image</label>
                </div>

                <div>                   
                    <img style="background:<?php echo $stream['color']; ?>" src="<?php echo $stream['image'];?>" height="100" width="100">
                </div>
                <div>                
                     <label for="name">New Image</label>
                </div>

                <div>                   
                    <input type="file" accept="image/*" name = "image" id="exampleInputFile" width="20%">
                </div> 
                <?php //print_r($stream) ; ?>  
                <div class="form-group">
                    <label for="name">Color Picker</label>
                    <input type="color" class="form-control input-sm" id="color" name="color" value="<?php echo $stream['color']; ?>" >

                </div>
                <div class="form-group" style="margin-top: 13px">
                <button type="submit" class="btn btn-sm  btn-info bold">Submit</button>

                <a href="<?php echo AUTH_PANEL_URL.'course_product/stream/stream_management'; ?>">
                <button type="button" class="btn btn-sm  btn-danger bold ">Cancel </button>  
                </a> 
                <div>
                       
            </form>
        </div>
    </section>
</div>
<div class="clearfix"></div>
