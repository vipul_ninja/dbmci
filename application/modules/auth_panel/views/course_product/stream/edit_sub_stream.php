<?php     $main_stream_result = $this->db->get('`course_subject_master`')->result_array();
//print_r($stream);
 ?>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            Edit Sub Stream
        </header>
        <div class="panel-body">
            <form role="form" method="POST" enctype="multipart/form-data">
              <?php if($stream['id']) { ?>
              <input type="hidden" name="id" value="<?php echo $stream['id'];?>">
              <input type="hidden" name="parent_id" value="<?php echo $stream['parent_id'];?>">
              <?php } ?>
                <div class="form-group">
                    <label for="name">Sub Stream Name</label>
                    <input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo $stream['name']; ?>" placeholder="Enter Sub Stream Name">
                    <span class="text-danger"><?php echo form_error('name');?></span>
                </div>
<!--                <div class="form-group">
                    <label for="name">Sub Stream Name (Hindi)</label>
                    <input type="text" class="form-control input-sm" id="name_2" name="name_2" value="<?php echo $stream['name_2']; ?>" placeholder="Enter Sub Stream Name">
                    <span class="text-danger"><?php echo form_error('name_2');?></span>
                </div>-->
              <!--  <div>               
                     <label for="name">Image</label>
                </div>

                <div>                   
                    <img  style="background:<?php echo $stream['color']; ?>" src="<?php echo $stream['image'];?>" height="20%" width="20%">
                </div>
                <div class="row">
                <div class="form-group  col-md-6">
              <label for="name">&nbsp &nbsp Upload New Image </label>
                        
              <input class="stream_css " type="file" accept="image/*" name = "image" id="exampleInputFile" >
              </div>     -->
            <!--   <div class="form-group  col-md-6" >
                    <label for="name">Color Picker</label>
                    <input type="color" class="form-control  form-inlineinput-sm" value="<?php echo $stream['color']; ?>" id="color" name="color" placeholder="Enter color code" >
                </div>
                </div>
                              <label for="title">Subjects </label>
                <div class=" ">
                    <div class="">
                    <?php

                            $all_subject_stream_ids =$this->db->select('GROUP_CONCAT(subject_id) as type_ids')->where('stream_id',$stream['id'])->get('Substream_subject_relationship')->row()->type_ids;
                            if($all_subject_stream_ids !=""){
                            $all_subject_stream_ids = explode(',',$all_subject_stream_ids);
                          }else{
                             $all_subject_stream_ids=array();
                          }
                          ?>
                            <ul class="nav nav-tabs">
                                <?php $p=1;$holder=array();
                                  foreach($main_stream_result as $res){
                                //  if($res['parent_id']==0){
                                    $holder[]=$res['id'];
                                    ?>
                                <li class=""><input <?php if(in_array($res['id'], $all_subject_stream_ids)){  echo 'checked'; } ?>  type="checkbox" name="subject_type[]" value="<?php echo($res['id'].'&nbsp');?>" ><?php echo($res['name'].'&nbsp');?></input></li>
                                <?php //} 
                                $p++;}?>
                                <div class="pull-right margin-right"><span>Select All &nbsp</span><input id="select-all" type="checkbox" value="check all"></div>
                            </ul>
                    </div>
                    </div> -->
                <button type="submit" class="btn bold btn-sm btn-info">Submit</button>
                <a href="<?php echo AUTH_PANEL_URL.'course_product/stream/add_sub_stream/'.$stream['parent_id']; ?>"><button type="button" class="btn btn-sm  btn-danger bold ">Cancel </button></a>
            </form>

        </div>
    </section>
</div>
<div class="clearfix"></div>
