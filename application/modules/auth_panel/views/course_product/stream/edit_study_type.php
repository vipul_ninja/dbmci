<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            Edit Study Type
        </header>
        <div class="panel-body">
            <form role="form" method="POST">
              <?php if($stream['id']) { ?>
              <input type="hidden" name="id" value="<?php echo $stream['id'];?>">
              <input type="hidden" name="parent_id" value="<?php echo $stream['parent_id'];?>">
              <?php } ?>
                <div class="form-group">
                    <label for="name">Sub Study Type</label>
                    <input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo $stream['name']; ?>" placeholder="Enter Sub Stream Name">
                    <span class="text-danger"><?php echo form_error('name');?></span>
                </div>
                <button type="submit" class="btn bold btn-sm btn-info">Submit</button>
                <a href="<?php echo AUTH_PANEL_URL.'course_product/stream/add_sub_stream/'.$stream['parent_id']; ?>"><button type="button" class="btn btn-sm  btn-danger bold ">Cancel </button></a>
            </form>

        </div>
    </section>
</div>
<div class="clearfix"></div>
