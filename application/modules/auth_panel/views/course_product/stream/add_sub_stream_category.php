<?php 
  $st_name = $this->db->select('name')->where('id',$stream_id)->get('course_stream_name_master')->row()->name;
?>

<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            Add Sub Stream <span class="pull-right"><?php echo $st_name; ?></span>
        </header>
        <div class="panel-body">
            <form role="form" class="form-inline"  method="POST" enctype="multipart/form-data">
              <div class="col-md-12 error bold alert-box">
              <?php echo validation_errors(); ?>
              </div>
                <div class="form-group">
                    <label for="name">Sub Stream Name</label>
                    <input type="hidden" value="<?php if(isset($stream_id)){ echo $stream_id; }?>" name="stream_id">
                    <input type="text" class="form-control  form-inline input-sm" value="<?php echo set_value('name');?>" id="name" name="name" placeholder="Enter Sub Stream">
                </div>
               
                <button type="submit" class="btn btn-xs bold btn-info">Add</button>
                <a class="btn btn-xs bold btn-danger" href="<?php echo AUTH_PANEL_URL.'course_product/stream/stream_management'; ?>">Cancel</a>
            </form>

        </div>
    </section>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		<?php // echo strtoupper($page); ?> Sub Stream(s) LIST
		</header>
		<div class="panel-body">
		<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="all-user-grid">
  		<thead>
    		<tr>
          <th>#</th>
          <th>Sub Stream Name </th>
         
          <th>Created on</th>
		      <th>Action </th>
    		</tr>
  		</thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
              <th></th>
              <th></th>
              <th></th>
          </tr>
      </thead>
		</table>
		</div>
		</div>
	</section>
</div>


<?php 
$this->db->order_by("position", "asc");
$this->db->where("parent_id", $stream_id);
$all = $this->db->get('course_stream_name_master')->result();
?>
    

<?php
$adminurl = AUTH_PANEL_URL;
//if($page == 'android') { $device_type = 1; } elseif ($page == 'ios') { $device_type = 2; } elseif ($page == 'all') { $device_type = '0'; }
$dragablejs = AUTH_ASSETS.'js/draggable-portlet.js';
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-user-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/stream/ajax_sub_sub_stream_list/"+"$stream_id", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );

						$( function() {
					$( ".dpd1" ).datetimepicker();
					$( ".dpd2" ).datetimepicker();
					 }); // datepicker closed
                   } );
               </script>
               <script src="$dragablejs"></script>
               <script>
                    jQuery(document).ready(function() {
                        DraggablePortlet.init();
                    });
                    function save_position(){
                        var position = [];
                        $('.ui-sortable-handle').each(function() {
                            position.push($(this).data('catid'));
                        });
                        $.ajax({
                            type:'POST',
                            url :"$adminurl"+"course_product/stream/save_position_stream",
                            data:{'ids':position},
                            dataType:'json',
                            success:function(data){
                              //console.log(data.errors);
                              show_toast('success', 'Position saved successfully','Updated');
                            },
                            error: function(data){

                            }
                        });
                    }
                </script>
EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
