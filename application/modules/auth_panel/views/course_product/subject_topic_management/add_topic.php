<?php //echo '<pre>'; print_r($subject_list); die;?>

                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Add Topic
                          </header>
                          <div class="panel-body">
                              <form class="form-inline"  role="form" method="POST">
                                <div class="col-md-12 error bold alert-box">
                                  <?php echo validation_errors(); ?>
                                </div>
                                <div class="form-group">
                                      <label for="name">Select Subject</label>
                                      <select class="form-control input-sm " name="subject_id">
                                        <option value="" >----Select-----</option>
                                        <?php if(isset($subject_list)){
                                          foreach($subject_list as $subject){
                                            ?>
                                            <option <?php echo (set_value('subject_id') == $subject['id'])?'selected="selected"':'';?> value="<?php echo $subject['id'];?>"><?php echo $subject['name'];?></option>
                                        <?php
                                          }
                                        }?>
                                      </select>
                                  </div>
                                  <div class="form-group">
                                      <label for="topic">Topic Name</label>
                                      <input type="text" value="<?php echo set_value('topic');?>"  class="form-control input-sm " id="topic" name="topic" placeholder="Enter Topic Name">
                                  </div>
                                  <button type="submit" class="btn btn-sm bold btn-info">Add Topic</button>
                              </form>

                          </div>
                      </section>
                  </div>
				  <div class="clearfix"></div>


			<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		<?php // echo strtoupper($page); ?> TOPIC(s) LIST
		</header>
		<div class="panel-body">
		<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="all-user-grid">
  		<thead>
    		<tr>
          <th>#</th>
      		<th>Subject Name </th>
          <th>Topic Name</th>
            <th>Status </th>
            <th>Created on</th>
            <th>Last updated</th>
		      <th>Action </th>
    		</tr>
  		</thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
              <th></th>
			         <th></th>
               <th></th>
               <th></th>
          </tr>
      </thead>
		</table>
		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
//if($page == 'android') { $device_type = 1; } elseif ($page == 'ios') { $device_type = 2; } elseif ($page == 'all') { $device_type = '0'; }
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-user-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/Subject_topics/ajax_topic_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );

						$( function() {
					$( ".dpd1" ).datetimepicker();
					$( ".dpd2" ).datetimepicker();
					 }); // datepicker closed
                   } );
               </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
