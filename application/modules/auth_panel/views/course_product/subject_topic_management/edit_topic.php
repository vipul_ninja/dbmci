
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Edit Topic
          </header>
          <div class="panel-body">
              <form role="form" class="form-inline"  method="POST">
                <div class="col-md-12 error bold alert-box">
                  <?php echo validation_errors(); ?>
                </div>
                <?php if($topics_management['id']) { ?>
                <input type="hidden" name="id" value="<?php echo $topics_management['id'];?>">
                <?php } ?>
                <div class="form-group">
                      <label for="name">Subject</label>
                      <select  class="form-control input-sm" name="subject_id">
                        <?php if(isset($subject_list)){
                          foreach($subject_list as $subject){
                            ?>
                            <option <?php if(isset($topics_management['subject_id'])){ if($topics_management['subject_id']==$subject['id']){echo 'selected';}else{echo "disabled";}  }?> value="<?php echo $subject['id'];?>"><?php echo $subject['name'];?></option>
                        <?php
                          }
                        }?>
                      </select>
                       <span class="text-danger"><?php echo form_error('subject_id');?></span>
                  </div>
                  <div class="form-group">
                      <label for="topic">Topic Name</label>
                      <input type="text" value="<?php if(isset($topics_management['topic'])){ echo $topics_management['topic']; }?>" class="form-control   input-sm" id="topic" name="topic" placeholder="Enter Topic Name">
                  </div>
                  <div class="form-group">
                      <label for="topic">Topic Name (Hindi)</label>
                      <input type="text" value="<?php if(isset($topics_management['topic_2'])){ echo $topics_management['topic_2']; }?>" class="form-control   input-sm" id="topic_2" name="topic_2" placeholder="Enter Topic Name">
                  </div>
                  <button type="submit" class="btn btn-sm bold  btn-info">Submit</button>
                    <a href="<?php echo AUTH_PANEL_URL.'course_product/Subject_topics/topics_management'; ?>"><button type="button" class="btn btn-sm btn-danger bold ">Cancel </button></a>
              </form>

          </div>
      </section>
  </div>
  <div class="clearfix"></div>
