<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            Edit Subject
        </header>
        <div class="panel-body">
            <form role="form" method="POST" enctype="multipart/form-data">
              <?php if($subject['id']) { ?>
              <input type="hidden" id="id" value="<?php echo $subject['id'];?>">
              <?php } ?>
                <div class="form-group">
                    <label for="name">Subject Name</label>
                    <input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo $subject['name']; ?>" placeholder="Enter Subject Name">
                    <span class="text-danger"><?php echo form_error('name');?></span>
                </div>
                <div class="form-group">
                    <label for="name">Subject Name (Hindi)</label>
                    <input type="text" class="form-control input-sm" id="name_2" name="name_2" value="<?php echo $subject['name_2']; ?>" placeholder="Enter Subject Name">
                    <span class="text-danger"><?php echo form_error('name_2');?></span>
                </div>
                <div>
                <img src="<?php echo $subject['image']; ?>" style="background:<?php echo $subject['color_code']; ?>" height="100" width="100" >
                </div>
                <div class="form-group">
              <label for="name">New Image </label>           
              </div>
              <div class="form-group stream_css">            
              <input type="file" accept="image/*" name = "image" id="exampleInputFile">
              </div> 
              <div class="form-group">
                    <label for="name">Color Picker</label>
                    <input type="color" class="form-control input-sm" id="color" name="color" value="<?php echo $subject['color_code']; ?>" >
                    <span class="text-danger"><?php echo form_error('color');?></span>
                </div>
                <button type="submit" class="btn bold btn-sm btn-info">Submit</button>
                <a href="<?php echo AUTH_PANEL_URL.'course_product/Subject_topics/subject_management'; ?>"><button type="button" class="btn btn-sm  btn-danger bold ">Cancel </button></a>
            </form>

        </div>
    </section>
</div>
<div class="clearfix"></div>
