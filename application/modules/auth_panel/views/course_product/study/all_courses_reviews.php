  <?php $period = $this->input->get('period'); ?>
   <div id="tabContent8" class="col-md-12 tabu ">
        <section class="panel">
            <header class="panel-heading">
                All Study Reviews
                 <span class="tools pull-right">
                   <form role="form" method="get" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  >
                    <select name="period" class="form-control input-sm m-bot15 period" onchange="this.form.submit()">
                        <option <?php if($period == "today"){ echo "selected";} ?> value="today">Today</option>
                        <option <?php if($period == "yesterday"){ echo "selected";} ?>  value="yesterday">Yesterday</option>
                        <option <?php if($period == "7days"){ echo "selected";} ?>  value="7days">Last 7 Days</option>
                        <option <?php if($period == "current_month"){ echo "selected";} ?>  value="current_month">Current Month</option>
                        <option <?php if($period == "custom"){ echo "selected";} ?>  value="custom">Custom</option>
                        <option <?php if($period == "" || $period == "all"){ echo "selected";} ?>  value="all">All</option>
                    </select>
                    <?php
                      foreach($_GET as $key=>$value){
                        if($key != 'period'){
                          echo "<input type='hidden' value='".$value."' name='".$key."'>";
                        }        
                      }
                    ?>
                  </form>
                  </span>
            </header>
            <div class="panel-body ">
                <div class="adv-table">
                                    <!-- date filter  -->

                   <?php if($period == "custom" ){  ?>
                    <div class="col-md-6 pull-right custom_search" >
                          <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
                              <div  class="input-group-addon">From</div>
                              <input type="text" id="min-date-course-transaction" class="form-control date-range-filter input-sm course_start_date"  placeholder="">
                              <div class="input-group-addon">to</div>
                              <input type="text" id="max-date-course-transaction" class="form-control date-range-filter input-sm course_end_date"  placeholder="">
                              <div class="input-group-addon btn date-range-filter-clear">Clear</div>
                          </div>
                      </div>
                    <?php } ?>

                    <table  class="display table table-bordered table-striped col-md-12" id="all-course-review-grid" style="width:100%">
                        <thead>
                            <tr>
                              <th>id</th>
                              <th>Course </th>
                              <th>User Name </th>                               
                              <th>Review </th>
                              <th>Rating</th>
                              <th>Time </th>
                              <th>Action </th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
								                <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div> 

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-course-review-grid';
                       var dataTable_review = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "pageLength": 100,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/course/ajax_all_course_review_list?period=$period", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );

                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_review.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable_review.columns(i).search(v).draw();
                        } ); 
                        // Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date-course-transaction').val() !="" && $('#max-date-course-transaction').val() != "" ){
                                var dates = $('#min-date-course-transaction').val()+','+$('#max-date-course-transaction').val();
                                dataTable_review.columns(5).search(dates).draw();
                            }
                        });
                        $('.date-range-filter-clear').on( 'click', function () {
                            // clear date filter
                            $('#min-date-course-transaction').val('');
                            $('#max-date-course-transaction').val("");
                            dataTable_review.columns(5).search('').draw();
                        });                
                   } );
				   
				   
  			        $('#min-date-course-transaction').datepicker({
                  format: 'dd-mm-yyyy',
                  autoclose: true
                });
                $('#max-date-course-transaction').datepicker({
                  format: 'dd-mm-yyyy',
                  autoclose: true
                });

               </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>