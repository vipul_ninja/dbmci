<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
 			Study List
		<a href="<?php echo AUTH_PANEL_URL."course_product/Course/add_course_product"; ?>" class="pull-right">
      <button class="btn btn-sm btn-success">Add Course</button>
    </a>
		</header>

		<div class="panel-body">
		<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="course-list-grid">
  		<thead>
    		<tr>
                <th>Title </th>
                <th>Stream</th>
                <th>Category</th>
                <th>Subject</th>
                <th>Publish</th>
                <th>Is New</th>
                <th>Rating</th>
                <th>For users</th>
                <th>Is suggested </th>
				<th>Created</th>
                <th>Edited On </th>
                <th>Action</th>
    		</tr>
  		</thead>
       <thead>
          <tr>
              <th><input type="text" data-column="0"  class="form-control search-input-text"></th>
              <!-- <th><input type="text" data-column="1"  class="form-control search-input-text"></th>
              <th><input type="text" data-column="2"  class="form-control search-input-text"></th>
              <th><input type="text" data-column="3"  class="form-control search-input-text"></th> -->
              <th>
                <select data-column="4"  class="form-control search-input-select">
                    <option value="">(All)</option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
				            <option value="2">Requested</option>
                </select>
              </th>
              <th>
                <select data-column="5"  class="form-control search-input-select">
                    <option value="">(All)</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
              </th>
             <th></th>
              <th>
                <select data-column="7"  class="form-control search-input-select">
                    <option value="">(All)</option>
                    <option value="0">DBMCI</option>
                    <option value="1">Non DBMCI</option>
                    <option value="2">Both</option>
                </select>
              </th>
              <th>
                <select data-column="8"  class="form-control search-input-select">
                    <option value="">(All)</option>
                    <option value="1">yes</option>
                    <option value="0">no</option>
                </select>
              </th>
			  <th></th>
              <th></th>
              <th></th>
          </tr>
      </thead>
		</table>
		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'course-list-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
												 "columnDefs": [
																					 {
																							 "targets": [ 1 ],
																							 "visible": false,
																							 "searchable": false
																					 },
																					 {
																							 "targets": [ 2 ],
																							 "visible": false,
																							 "searchable": false
																					 },
																					 {
																							 "targets": [ 3 ],
																							 "visible": false,
																							 "searchable": false
																					 }
																			 ],
                           "processing": true,
                           "serverSide": true,
                           "pageLength": 100,
                           "order": [[ 2, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/course/ajax_all_courses_list", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
											 $('.search-input-select').on( 'change', function () {   // for select box
												    var i =$(this).attr('data-column');
												    var v =$(this).val();
												    dataTable.columns(i).search(v).draw();
												} );
                   } );
               </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
