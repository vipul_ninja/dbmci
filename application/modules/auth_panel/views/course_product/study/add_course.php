<div class="col-md-3">
<section class="panel">
  <header class="panel-heading">
      Study menu
  </header>
  <div class="panel-body">
      <ul class="nav prod-cat">
          <li><a href="javascript:void(0)" data-div="1" ><i class=" fa fa-angle-right"></i> Basic information</a></li>
          <li class="disabled"><a href="javascript:void(0)" data-div="2" ><i class=" fa fa-angle-right"></i> Cover Image </a></li>
          <li class="disabled"><a href="javascript:void(0)" data-div="3" ><i class=" fa fa-angle-right"></i> Cover Video </a></li>
          <li class="disabled"><a href="javascript:void(0)" data-div="4" ><i class=" fa fa-angle-right"></i> Price managment </a></li>
          <li class="disabled" ><a href="javascript:void(0)" data-div="7" ><i class=" fa fa-angle-right"></i> Topic managment </a></li>
          <li class="disabled" ><a href="javascript:void(0)" data-div="6" ><i class=" fa fa-angle-right"></i> Coupon </a></li>
          <li class="disabled" ><a href="javascript:void(0)" data-div="5" ><i class=" fa fa-angle-right"></i> FAQ </a></li>
      </ul>
  </div>
</section>

</div>

<div class="col-lg-9">
    <section class="panel">
        <header class="panel-heading">
            Add Study
        </header>
        <div class="panel-body">
            <form  autocomplete="off" novalidate="novalidate"  id="add_course" action="" method="POST">

                <div class="form-group col-md-12">
                    <label for="exampleInputEmail1">Study Title</label>
                    <input type="text" placeholder="" aria-required="true" name="title" class="form-control input-sm required">
                </div>

                <div class="form-group col-md-4 hide ">
                    <label for="exampleInputEmail1">Course Category</label>
                    <select name="course_main_fk"  class="form-control input-sm course_element_select ">

                    </select>
                </div>

                <div class="form-group col-md-4 hide">
                    <label for="exampleInputEmail1">Course Stream</label>
                    <select name="course_category_fk"  class="form-control input-sm stream_element_select ">

                    </select>
                </div>


                <div class="form-group col-md-4 hide">
                    <label for="exampleInputEmail1">Subject</label>
                    <select name="subject_id"  class="form-control input-sm subject_element_select  ">
                    </select>
                </div>
                <div class="form-group col-md-6 hide">
                    <label for="exampleInputEmail1">Topic</label>
                    <select  class="form-control input-sm topic_element_select ">
                    </select>
                </div>

                <div class="form-group col-md-12 ">
                    <label for="exampleInputEmail1">Description</label>
                    <textarea name="description" placeholder="" class="form-control input-sm required "></textarea>
                </div>

                <div class="form-group col-md-12  hide">
                <span class="label label-info label-">Note-:</span> <small class="bold">If you want to make this course free please insert 0 i.e. zero in MRP input box. 					</small>
                 </div>
                <div class="form-group col-md-4 hide">
                    <label for="exampleInputEmail1">MRP</label>
                    <input name="mrp" value="0" type="text" placeholder="" class="form-control input-sm required" onkeypress="return validate_data_keypress(event)">
                </div>
                <div class="form-group col-md-4 hide">
                  <div class="col-md-6 no-padding">
                    <label for="exampleInputEmail1">(for dams)</label>
                    <input type="text" name="for_dams"   placeholder="" class="form-control col-md-2 input-sm for_dams_input required">
                  </div>
                  <div class="col-md-6 no-padding hide">
                    <label for="exampleInputEmail1">(%)</label>
                    <input type="text"  name="for_dams_percent"  placeholder="" class="form-control col-md-2  input-sm for_dams_input">
                  </div>
                </div>
                <div class="form-group col-md-4 hide">
                  <div class="col-md-6 no-padding">
                    <label for="exampleInputEmail1">(for non dams)</label>
                   <input  name="non_dams" type="text" value="0"  placeholder="" class="form-control input-sm for_dams_input required ">
                  </div>
                  <div class="col-md-6 no-padding hide">
                    <label for="exampleInputEmail1">(%)</label>
                     <input type="text"  name="non_dams_percent" placeholder="" class="form-control col-md-2  input-sm for_dams_input ">
                  </div>
                </div>
                <div class="form-group col-md-12 hide">
                    <label for="exampleInputEmail1">Instructor</label>
                    <select name="instructor_id" class="form-control input-sm instructor_element_select">
                    </select>
                </div>
                <div class="col-md-12" id="instructor_profile">

                </div>
                <div class="form-group col-md-12 hide">
                    <label for="exampleInputEmail1">GST</label>
                    <label class="checkbox-inline">
                      <input class="required" checked="checked" type="radio" value="0" name="gst_include" id="inlineCheckbox1"> Include
                      </label>
                      <label class="checkbox-inline">
                          <input class="required"  type="radio" value="1" name="gst_include"   id="inlineCheckbox1"> Exclude
                      </label>
                </div>
                <div class="form-group col-md-12 hide">
                    <label for="exampleInputEmail1">Instructor Sharing (%)</label>
                     <input name="instructor_share"  type="text" placeholder="" class="form-control input-sm ">
                </div>

                <div class="form-group col-md-12 ">
                    <label for="exampleInputEmail1">Search Tags</label>
                    <textarea  name="tags" class="form-control input-sm "></textarea>
                </div>
				<div class="form-group col-md-12 hide">
                    <label for="exampleInputEmail1">Course For</label>
                    <label class="checkbox-inline">
                      <input class="required" checked="checked" type="radio" value="0" name="course_for" id="course_for_chkbox"> Dams
                      </label>
                      <label class="checkbox-inline">
                          <input class="required"  type="radio" value="1" name="course_for"   id="course_for_chkbox"> Non Dams
                      </label>
					<label class="checkbox-inline ">
                          <input checked="checked" class="required"  type="radio" value="2" name="course_for"   id="course_for_chkbox"> Both
                      </label>
                </div>
                 <div class="form-group col-md-12 ">
                   <button class="btn btn-info" type="submit">Submit</button>
                 </div>
            </form>

        </div>
    </section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$validation_js = AUTH_ASSETS."js/jquery.validate.min.js";
$custum_js = <<<EOD
              <script src="$validation_js" type="text/javascript"></script>
                <script>
                  jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"'>"+value.name+"</option>";
                        });
                         $(".subject_element_select").html(html);
                      }
                    });

                  jQuery.ajax({
                      url: "$adminurl"+"course_product/Course_category/get_main_category?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"'>"+value.text+"</option>";
                        });
                         $(".course_element_select").html(html);
                      }
                    });


                    $( ".subject_element_select" ).change(function() {
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.topic+"</option>";
                          });
                           $(".topic_element_select").html(html);
                        }
                      });
                    });

                    $( ".course_element_select" ).change(function() {
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/Course_category/get_main_category_stream/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.name+"</option>";
                          });
                           $(".stream_element_select").html(html);
                        }
                      });
                    });
                    //instructor_element_select

                     jQuery.ajax({
                      url: "$adminurl"+"course_product/course/get_all_instructor?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {
                        var html = "<option value=''>--select--</option>";
                        console.log(data);
                        $.each( data , function( key , value ) {
                         if(value.status == 0 ){
                           html += "<option value='"+value.id+"'>"+value.name+"</option>";

                         }

                        });
                         $(".instructor_element_select").html(html);
                      }
                    });

                    $( ".instructor_element_select" ).change(function() {
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/course/get_instructor_basic_info/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {
                          console.log(data);
                          if(data){
                            html =  '<div class="panel-body bg-info "><a class="task-thumb" href="#"><img style="max-width:100px"  alt="" src="'+data.profile_picture+'"></a><div class="task-thumb-details" style="margin-left: 30px;" ><h1><a href="#">'+data.name+'</a></h1><p>'+data.email+'</p></div></div>';
                            $('#instructor_profile').html(html);
                            if(data.instuctor_info){
                                i =  data.instuctor_info;
                                $("input[name=instructor_share]").val(i.r_sharing);
                            }
                          }
                        }
                      });
                    });
               </script>
           <script type="text/javascript">
                  $(document).ready(function () {
                        $('input[name=mrp]').bind('blur keyup change ', function(e){
                            if($(this).val() < 1){
                                $('input[name=for_dams]').val(0).change();
                                $('input[name=non_dams]').val(0).change();
                            }
                        });
                      /* ****************** price jquery handler start here  **********************  */
                      $('.for_dams_input').bind('blur keyup change ', function(e){
                         name = $(this).attr('name');
                         mrp  = $('input[name=mrp]').val();
                         for_dams = $('input[name=for_dams]').val();
                         for_dams_percent = $('input[name=for_dams_percent]').val();

                         non_dams = $('input[name=non_dams]').val();
                         non_dams_percent = $('input[name=non_dams_percent]').val();
                         /* event on for dams */
                         if(name == "for_dams"){
                          percent = 100 - (for_dams/mrp)*100;
                          if(for_dams == "" || for_dams == 0 ){
                            $('input[name=for_dams_percent]').val(100);
                          }else{
                            $('input[name=for_dams_percent]').val(percent);
                          }
                         }
                         /* event on for dams percent */
                         if(name == "for_dams_percent"){
                          value = mrp- (mrp/100)*for_dams_percent;
                          $('input[name=for_dams]').val(value);

                         }
                          /* event on for non  dams */
                          if(name == "non_dams" ){
                              percent = 100 - (non_dams/mrp)*100;
                              if(non_dams == ""  || non_dams == 0){
                                $('input[name=non_dams_percent]').val(100);
                              }else{
                                $('input[name=non_dams_percent]').val(percent);
                              }
                             }
                           /* event on for dams percent */
                         if(name == "non_dams_percent"){
                          value = mrp- (mrp/100)*non_dams_percent;
                          $('input[name=non_dams]').val(value);

                         }
                      });
                      $('.for_dams_input').change();

                      var form = $("#add_course");
                        jQuery.validator.addMethod("max_price", function(value, element) {
                            var max = $('input[name=mrp]').val();
                            if(parseFloat(value) > parseFloat(max)){
                                console.log(value+"-"+max);
                                return false;
                            }else{
                            return true;
                            }
                        }, "Please provide value equal or less than mrp.");

                        form.validate({
                            errorPlacement: function errorPlacement(error, element) {
                                element.after(error);
                            },
                            rules: {
                                mrp: {
                                required: true,
                                min: 0
                                }
                            }
                        });
                    });



                    function validate_data_keypress(event){
                      if (event.which != 46 && (event.which < 47 || event.which > 59))
                      {
                          event.preventDefault();
                          if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
                              event.preventDefault();
                          }
                      }
                  }

              </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
