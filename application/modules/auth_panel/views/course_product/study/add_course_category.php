<?php  $error = validation_errors();
$display = "display:none;";
if(!empty($error)){ $display = "";} 
if($this->input->get('category')){$display = "display:none;";}
?>
<div class="col-lg-12 add_series_element" style="<?php echo $display; ?>">
   <div class="col-lg-12">
      <section class="panel">
         <header class="panel-heading">
            Add Study category
         </header>
         <div class="panel-body">
            <form autocomplete="off" role="form" method= "POST" action="<?php echo AUTH_PANEL_URL.'course_product/course_category/add_category' ?>" enctype="multipart/form-data">
               <div class="form-group col-sm-3">
                  <label for="text">Name</label>
                  <input type="text" class="form-control input-sm" name = "name" id="text" value="<?php echo set_value('name'); ?>" placeholder="Enter category Name" >
                  <span class="text-danger"><?php echo form_error('name');?></span>
               </div>
               <div class="form-group col-sm-3">
                  <label for="text">App View Type </label>
                  <select class="form-control input-sm" name="app_view_type">
                      <option value='1'>Horizontal</option>
                      <option value="2">Vertical</option>
                  </select>
               </div>
               <div class="form-group col-sm-3">
                  <label for="text">Main category </label>
                  <select class="form-control input-sm" name="parent_fk">
                    <?php if(isset($main_categories)){ foreach($main_categories as $main_cat) {?>
                      <option value="<?php echo $main_cat['id'];  ?>"><?php echo $main_cat['text'];  ?></option>
                    <?php } } ?>
                  </select>
                  <span class="text-danger"><?php echo form_error('parent_fk');?></span>
               </div>
               <div class="form-group col-sm-3">
                  <label for="text">Visibility </label>
                  <select class="form-control input-sm" name="visibilty">
                      <option value='on'>ON</option>
                      <option value="off">OFF</option>
                  </select>
                   <span class="text-danger"><?php echo form_error('visibilty');?></span>
               </div>
                <div class="form-group col-sm-3">
                  <label for="text">Show in Image List </label>
                  <select class="form-control input-sm" name="in_carousel">
                      <option value='1'>Show</option>
                      <option value="0">Hide</option>
                  </select>
                   <span class="text-danger"><?php echo form_error('in_carousel');?></span>
               </div>
               <div class="form-group col-sm-3">
                  <label for="text">Image </label>
                  <input type="file" name="image">
                   <span class="text-danger"><?php echo form_error('image');?></span>
               </div>
              
               <div class="form-group col-md-12">    
                 <input type="submit" name="add_category" class="btn btn-info btn-sm" value="Submit">
                 <button class="btn btn-danger btn-sm" onclick="$('.add_series_element').hide('slow');" type="button" >Cancel</button>
               </div>
            </form>
         </div>
      </section>
   </div>
   <div class="clearfix"></div>
</div>
<!-- end of add_currency section -->
<!--- edit currency section -->
<?php if($this->input->get('category')){ ?>
    <div class="col-lg-12">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Edit Course Category
            </header>
            <div class="panel-body">
                <form autocomplete="off" role="form" id="edit_category" method= "POST" action="<?php echo AUTH_PANEL_URL.'course_product/course_category/add_category?category='.$this->input->get('category'); ?>" enctype="multipart/form-data">
                  <?php if($this->input->get('category')){
                    ?>
                  <input type="hidden" name="category" value="<?php echo $this->input->get('category_id'); ?>">
                  <input type="hidden" name="image_url" value="<?php echo $category['image']; ?>">
                  <?php } ?>
                <div class="form-group col-sm-3">
                  <label for="text">Name</label>
                  <input type="text" class="form-control input-sm" name = "name" id="text" value="<?php if($category){ echo $category['name'];}?>" placeholder="Enter category Name" >
                  <span class="text-danger"><?php echo form_error('name');?></span>
                </div>
                <div class="form-group col-sm-3">
                  <label for="text">App View Type </label>
                  <select class="form-control input-sm" name="app_view_type">
                      <option value='1' <?php if($category['app_view_type']==1){ echo 'selected';}?>>Horizontal</option>
                      <option value="2" <?php if($category['app_view_type']==2){ echo 'selected';}?>>Vertical</option>
                  </select>
                  <span class="text-danger"><?php echo form_error('app_view_type');?></span>
                </div>
                <div class="form-group col-sm-3">
                  <label for="text">Visibility </label>
                  <select class="form-control input-sm" name="visibilty">
                      <option value='on' <?php if($category['visibilty']=='on'){ echo 'selected';}?>>ON</option>
                      <option value="off" <?php if($category['visibilty']=='off'){ echo 'selected';}?>>OFF</option>
                  </select>
                  <span class="text-danger"><?php echo form_error('visibilty');?></span>
               </div>
               <div class="form-group col-sm-3">
                  <label for="text">Main category </label>
                  <select class="form-control input-sm" name="parent_fk">
                    <?php if(isset($main_categories)){ foreach($main_categories as $main_cat) {?>
                      <option value="<?php echo $main_cat['id'];  ?>" <?php if($category['parent_fk']==$main_cat['id']){ echo 'selected';}?>><?php echo $main_cat['text'];  ?></option>
                    <?php } } ?>
                  </select>
                  <span class="text-danger"><?php echo form_error('parent_fk');?></span>
               </div>
                <div class="form-group col-sm-3">
                  <label for="text">Show in Image List </label>
                  <select class="form-control input-sm" name="in_carousel">
                      <option value='1' <?php if($category['in_carousel']==1){ echo 'selected';}?>>Show</option>
                      <option value="0" <?php if($category['in_carousel']==0){ echo 'selected';}?>>Hide</option>
                  </select>
                   <span class="text-danger"><?php echo form_error('in_carousel');?></span>
               </div>
               <div class="form-group col-sm-6">
                  <label for="text">Image </label>
                  <br>
                  <img src="<?php if($category['image']){ echo $category['image'];}?>" height="100px" with="100px">
                  <br><br>
                  <input type="file" name="image">
                   <span class="text-danger"><?php echo form_error('image');?></span>
               </div>
               <br>

                <div class="form-group col-md-12">    
                    <input type="submit" name="edit_category" class="btn btn-info btn-sm" value="submit">
                    <a class='btn btn-danger btn-sm' href="<?php echo AUTH_PANEL_URL.'course_product/course_category/add_category';?>" >Cancel</a>
                </div>
                </form>
            </div>
        </section>
    </div>
    <div class="clearfix"></div>
    </div>
<?php } ?>
<!--- and of edit  currency section -->
<div class="col-sm-12">
	<section class="panel">
    <?php
    if(!$this->input->get('category_id')){
    ?>
		<header class="panel-heading">
      Category List
       <button onclick="$('.add_series_element').show('slow');" class="btn-success btn-xs btn pull-right"><i class="fa fa-plus"></i> Add</button>
		<?php //echo strtoupper($page); ?>
		</header>
    <?php } ?>
		<div class="panel-body">
		<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="all-currency-grid">
  		<thead>
    		<tr>
          <th>#</th>
          <th>Main category</th>
      		<th>Name </th>
          <th>App View Type</th>
          <th>Visibility</th>
          <th>Image</th>
          <th>Show in image List</th>
          <th>Action</th>
    		</tr>
  		</thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
          </tr>
      </thead>
		</table>
		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-currency-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/Course_category/ajax_all_category_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );
                   } );
               </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
