<?php

$instructor_id = $_GET['transaction_id'];
$instuctor_name = urldecode($this->input->get('instuctor_name'));
$sql = "SELECT sum(`instructor_share`) as total  FROM `course_transaction_record` WHERE `instructor_id` =$instructor_id and transaction_status = 1 ";
$total_revenue =  $this->db->query($sql)->row()->total;

$sql = "SELECT sum(course_price) as total FROM `course_transaction_record` WHERE  transaction_status = 1 and `instructor_id` =".$instructor_id;
$total_sale =  $this->db->query($sql)->row()->total;

$sql = "SELECT count(id) as total FROM `course_transaction_record` WHERE `instructor_id` =".$instructor_id." AND transaction_status =1 and course_price > 0";
$total_transactions =  $this->db->query($sql)->row()->total;

$sql = "SELECT * FROM `course_instructor_information` WHERE `user_id` = $instructor_id";
$ins_data =  $this->db->query($sql)->row();
?>
<div class=" state-overview">
                  <div class="col-lg-4 col-sm-6">
                      <section class="panel">
                          <div class="symbol terques">
                              <i class="fa fa-user"></i>
                          </div>
                          <div class="value">
                              <h1 class="count"><?php echo (isset($ins_data->review_count))?$ins_data->review_count:0; ?></h1>
                              <p>Total Reviews</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-4 col-sm-6">
                      <section class="panel">
                          <div class="symbol red">
                              <i class="fa fa-tags"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count2"><?php echo $total_transactions; ?></h1>
                              <p>Total Transactions</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-4 col-sm-6">
                      <section class="panel">
                          <div class="symbol yellow">
                              <i class="fa fa-shopping-cart"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count3"><?php echo $total_sale; ?></h1>
                              <p>Total Sale</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-4 col-sm-6">
                      <section class="panel">
                          <div class="symbol blue">
                              <i class="fa fa-bar-chart-o"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count4"><?php echo $total_revenue; ?></h1>
                              <p>Total Revenue</p>
                          </div>
                      </section>
                  </div>
              </div>

<div class="col-sm-12">
  <section class="panel">
    <header class="panel-heading">
      Instructors Transaction (<?php echo $instuctor_name;?>)
      <a href="<?php echo AUTH_PANEL_URL . "course_product/course_transactions?instructor_id=$instructor_id&instructor_name=".urlencode($instuctor_name);?>" class="pull-right "><button class="btn  bold btn-sm btn-success" >View all</button></a>
    </header>
    <div class="panel-body">
    <div class="adv-table">
	<div class="col-md-6 pull-right">
        <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
            <div  class="input-group-addon">From</div>
            <input type="text" id="min-date-course-transaction" class="form-control date-range-filter input-sm course_start_date"  placeholder="">
            <div class="input-group-addon">to</div>
            <input type="text" id="max-date-course-transaction" class="form-control date-range-filter input-sm course_end_date"  placeholder="">
            <div class="input-group-addon btn date-range-filter-clear">Clear</div>
        </div>
    </div>
    <table  class="display table table-bordered table-striped" id="all-Course-transactions-grid">
      <thead>

        <tr>
        <th>#</th>
        <th>Course Name</th>
        <th>Total Earning</th>
		    <th>Instructor Revenue</th>
        <th>Transactions Count</th>
        <th>Instructor Name</th>
        <th>Action</th>
        </tr>
      </thead>
      <thead>
          <tr>
            <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
            <th></th>
            <th></th>
          </tr>
      </thead>
    </table>
    </div>
    </div>
  </section>
</div>


<?php
$adminurl = AUTH_PANEL_URL;
$assetsurl_morris_js = AUTH_ASSETS.'assets/morris.js-0.4.3/morris.min.js';
$assetsurl_morris_raphael_js = AUTH_ASSETS.'assets/morris.js-0.4.3/raphael-min.js';
$assetsurl_morris_css = AUTH_ASSETS.'assets/morris.js-0.4.3/morris.css';
$custum_js = <<<EOD


               <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {

                       var table = 'all-Course-transactions-grid';
                       var dataTable_transaction = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "pageLength": 50,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"course_product/Instructor_transactions_details/get_ajax_instructor_all_transactions_list/$instructor_id", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_transaction.columns(i).search(v).draw();
                       } );
					 $('.search-input-select').on( 'change', function () {   // for select box
						  var i =$(this).attr('data-column');
						  var v =$(this).val();
						  dataTable_transaction.columns(i).search(v).draw();
					  } );
					  // Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date-course-transaction').val() !="" && $('#max-date-course-transaction').val() != "" ){
                                var dates = $('#min-date-course-transaction').val()+','+$('#max-date-course-transaction').val();
                                dataTable_transaction.columns(9).search(dates).draw();
                            }
                        });

                        $('.date-range-filter-clear').on( 'click', function () {
                            // clear date filter
                            $('#min-date-course-transaction').val('');
                            $('#max-date-course-transaction').val("");
                            dataTable_transaction.columns(9).search('').draw();
                        });
                   } );

				   $('#min-date-course-transaction').datepicker({
				  		format: 'dd-mm-yyyy',
						autoclose: true

					});
					$('#max-date-course-transaction').datepicker({
						format: 'dd-mm-yyyy',
						autoclose: true

					});
               </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
