
<?php
$all_language_meta  = $this->db->select('id,language')->get('language_code')->result_array();


?> 
<style>
.q_name::before {
    content: ' \2023';
    font-weight: bold;
}
</style>

	<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
     Question Bank List
  <a href="<?php echo AUTH_PANEL_URL.'question_bank/Question_bank/add_question/';?>">
    <button class="btn-success btn-xs btn pull-right"><i class="fa fa-plus"></i> Add</button>
  </a>
</header>
		<div class="panel-body">
		<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="all-user-grid">
  		<thead>
    		<tr>
					<th>#</th>
					<th>Question</th>
					<th>Stream</th>
					<th>Sub-Stream</th>
					<th>Subject</th>
					<th>Topic</th>
					<th>Type</th>
					<th>Level</th>
					<th>Added-by </th>
					<th>Verified</th>
					<th>Lang</th>
					<th>Q_ID</th>
					<th>Action </th>
    		</tr>
  		</thead>
      <thead>
          <tr>
			<th><input type="text" data-column="0"  class="hide search-input-text form-control"></th>
			<th><input type="text" data-column="1"  class="search-input-text form-control"></th>
			<th><input type="text" data-column="2"  class="search-input-text form-control"></th>
			<th><input type="text" data-column="3"  class="search-input-text form-control"></th>
			<th><input type="text" data-column="4"  class="search-input-text form-control"></th>
			<th><input type="text" data-column="5"  class="search-input-text form-control"></th>
			<th><input type="text" data-column="6"  class="search-input-text form-control"></th>
			<th><select data-column="7"  class="form-control search-input-select">
							<option value="1">Easy</option>
							<option value="2">Medium</option>
							<option value="3">Hard</option>
							 </select>
		    </th>			
		 <th><input type="text" data-column="8"  class="search-input-text form-control"></th>
			<th><input type="text" data-column="9"  class="search-input-text form-control"></th>
			<th>  <select data-column="10"  class="form-control search-input-select">
                   <?php   foreach($all_language_meta as $language){  ?>
                                        <option value="<?php echo $language['id']; ?>"><?php echo $language['language']; ?></option>
                                        
                                        <?php } ?>
									</select></th>
			<th><input type="text" data-column="11"  class="search-input-text form-control"></th>

			<th></th>
          </tr>
      </thead>
		</table>
		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
				 			<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

    					      jQuery(document).ready(function() {
                           var table = 'all-user-grid';
                           var dataTable = jQuery("#"+table).DataTable( {
                               "processing": true,
                                "pageLength": 15,
                                "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                               "serverSide": true,
                               "order": [[ 0, "desc" ]],
                               "ajax":{
                                   url :"$adminurl"+"question_bank/question_bank/ajax_question_bank_list/", // json datasource
                                   type: "post",  // method  , by default get
                                   error: function(){  // error handling
                                       jQuery("."+table+"-error").html("");
                                       jQuery("#"+table+"_processing").css("display","none");
                                   }
                               }
                           } );
                           jQuery("#"+table+"_filter").css("display","none");
                           $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                               var i =$(this).attr('data-column');  // getting column index
                               var v =$(this).val();  // getting search input value
                               dataTable.columns(i).search(v).draw();
                           } );
                            $('.search-input-select').on( 'change', function () {   // for select box
                                var i =$(this).attr('data-column');
                                var v =$(this).val();
                                dataTable.columns(i).search(v).draw();
                            } );
                       } );

               </script>


EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
