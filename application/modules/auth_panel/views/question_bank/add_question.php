<?php $all_language = $this->db->select('id,language')->get('language_code')->result_array();
?>
<div class="col-lg-12 add_series_element" style="">
	  <section class="panel">
		  <header class="panel-heading">
			  Add Question
		  </header><?php 
	// print_r($_SESSION['languagei']);
		// echo $_SESSION['languagei'];
		  ?> 
		  <div class="panel-body">
			  <form  autocomplete="off" novalidate="novalidate"  id="add_question"  onSubmit="return validate_input()"  method="post" action="<?php echo AUTH_PANEL_URL.'question_bank/question_bank/add_question/'; ?>"   enctype="multipart/form-data">
			  
					 <div class="col-md-2">
					 <div class="col-md-12 dropdown ">
			   <label for="exampleInputEmail1">Language</label>
						<button class="col-md-12  btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?=$all_language[0]['language']?>
						<!-- <span class="caret"></span></button> -->
						<ul class="col-md-12 dropdown-menu">
							<?php 
									foreach($all_language as $language){ 
												?> 
							<li>
								<input 
									data-lang-name="<?php echo  $language['language']; ?>"
									name="language[]" 
									class=" language_checkbox select2-select req" 
									type="checkbox" checked
									value="<?php echo  $language['id']; ?>" 
									name="language[]" <?php  if (ISSET($_SESSION['languagei']) && in_array($language['id'], $_SESSION['languagei'])){ echo 'checked'; }?>  
									onChange="getSelectedOptions()" required >
									<?php echo  $language['language']; ?> 
							</li>
							<?php } ?>
						</ul>
					</div>
					 <div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Test Name</label>
							<input type="text" placeholder="Test Name" name = "test_name" id="test_name" class="form-control input-xs" value="<?php if(isset($_SESSION['test_name'])){ echo $_SESSION['test_name']; } ?>">
							<span class="error bold"><?php echo form_error('test_name');?></span>

							
						</div>	
						<?php
							
						?>
						<!-- <div class="form-group col-md-12">
							<label for="exampleInputEmail1">Language</label>
							<select name="language[]" id="language" class="select2-select req" style="width: 90%;" onChange="getSelectedOptions(this)" multiple>
							<?php foreach($all_language as $language){ ?> 
								<option value="<?php echo  $language['id']; ?>"><?php echo  $language['language']; ?></option>

							<?php } ?>
							</select>
							<span class="error bold"><?php echo form_error('status');?></span>
						</div> -->
					 	<?php
					 		$all_option = $this->db->where('status',0)->get('course_stream_name_master')->result(); 
					 		$main_option = $sub_option = "";
					 		foreach($all_option as $ao){
					 			if($ao->parent_id == 0 ){
									 $selected='';
									   if (isset($_SESSION['stream_id']) &&  $_SESSION['stream_id']==$ao->id  ){ 
										$selected= "selected";  }
					 				$main_option .= "<option value='".$ao->id."' $selected >".$ao->name."</option>";
					 			}else{
									if (isset($_SESSION['sub_stream_id']) &&  $_SESSION['sub_stream_id']==$ao->id  ){ 
										$selected= "selected";  }
					 				$sub_option .= "<option style='display: none;' class='substream sub".$ao->parent_id."' value='".$ao->id."' $selected >".$ao->name."</option>";
					 			}
							 }
							 
							
					 	?>
						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Main stream</label>
							<select class="form-control input-xs stream_element_select" name="stream_id">
							<option value=''>--select Stream--</option>
							<?php echo $main_option;?>
							</select>
							
						</div>

						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Sub stream</label>
							<select class="form-control input-xs sub_element_select" name="sub_stream_id">
							<option value=''>--select Sub Stream--</option>
							<?php echo $sub_option;?>
							</select>
							
						</div>

						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Subject</label>
							<select class="form-control input-xs subject_element_select" name="subject_id">

							</select>
							<span class="error bold"><?php echo form_error('subject_id');?></span>
						</div>
						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Topic</label>
							<select class="form-control input-xs topic_element_select" name="topic_id">

							</select>
							<span class="error bold"><?php echo form_error('topic_id');?></span>
						</div>
						<div class="form-group col-md-12 hide">
							<label for="exampleInputEmail1">Question Style</label>
							<select class="form-control input-xs question_screentype" name="question_screentype" onchange="show_split_div(this.value)" >
								<option value="0" <?php   if (isset($_SESSION['question_screentype']) &&  $_SESSION['question_screentype']== '0' ){ ?>
								selected= "selected" <?php } ?>>Single Screen</option>
								<option value="1" <?php   if (isset($_SESSION['question_screentype']) && $_SESSION['question_screentype']== '1' ){ ?>
								selected= "selected" <?php } ?>>Split Screen</option>
							</select>
							<span class="error bold"><?php echo form_error('question_type');?></span>
						</div>
						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Question type</label>
							<select class="form-control input-xs question_type" name="question_type" >
								<option value="SC" <?php   if (isset($_SESSION['question_type'])  && $_SESSION['question_type']== 'SC' ){ ?>
								selected= "selected" <?php } ?>   >Single choice</option>
								<option value="MC" <?php   if (isset($_SESSION['question_type'])  &&  $_SESSION['question_type']== 'MC' ){ ?>
								selected= "selected" <?php } ?>>Multiple choice</option>
								<option value="TF" <?php   if (isset($_SESSION['question_type'])  &&  $_SESSION['question_type']== 'TF' ){ ?>
								selected= "selected" <?php } ?>>True-false</option>
							</select>
							<span class="error bold"><?php echo form_error('question_type');?></span>
						</div>
							<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Difficulty level</label>
							<select class="form-control input-xs" name="difficulty_level" >
								<option value="1" <?php   if (isset($_SESSION['difficulty_level'])  && $_SESSION['difficulty_level']== '1' ){ ?>
								selected= "selected" <?php } ?>>Easy</option>
								<option value="2" <?php   if (isset($_SESSION['difficulty_level'])  && $_SESSION['difficulty_level']== '2' ){ ?>
								selected= "selected" <?php } ?>>Medium</option>
								<option value="3" <?php   if ( isset($_SESSION['difficulty_level'])  && $_SESSION['difficulty_level']== '3' ){ ?>
								selected= "selected" <?php } ?>>Hard</option>

							</select>
							<span class="error bold"><?php echo form_error('difficulty_level');?></span>
						</div>
							<div class="form-group col-md-12 hide ">
							<label>Marks</label>
							<input type="text" placeholder="Marks" name = "marks" id="marks" class="form-control input-xs">
							<span class="error bold"><?php echo form_error('marks');?></span>
						</div>
						<div class="form-group col-md-12 hide">
							<label>Negative marks</label>
							<input type="text" placeholder="Negative marks" name = "negative_marks" id="negative_marks" class="form-control input-xs">
							<span class="error bold"><?php echo form_error('negative_marks');?></span>
						</div>
						<div class="form-group col-md-12 hide">
							<label>Duration</label>
							<input type="text" placeholder="Duration" name = "duration" id="duration" class="form-control input-xs">
							<span class="error bold"><?php echo form_error('duration');?></span>
						</div>
						<div class="form-group col-md-12">
							<label for="exampleInputEmail1">Status</label>
							<select class="form-control input-xs" name="status" >
								<option value="1" <?php   if ( isset($_SESSION['status'])  && $_SESSION['status']== '1' ){ ?>
								selected= "selected" <?php } ?> >Active</option>
								<option value="0" <?php   if ( isset($_SESSION['status'])  && $_SESSION['status']== '0' ){ ?>
								selected= "selected" <?php } ?> >Inactive</option>

							</select>
							<span class="error bold"><?php echo form_error('status');?></span>
							<input type="hidden" placeholder="image_url" name = "img_url_uplod" id="img_url_uplod" class="form-control input-xs">

						</div>
						<div class="form-group col-md-12 hide">
							<label for="exampleInputEmail1">Typable</label>
							<select class="form-control input-xs typable" name="typable"  >
							
								<option value="0" <?php   if ( isset($_SESSION['typable'])  && $_SESSION['typable']== '0' ){ ?>
								selected= "selected" <?php } ?> >No</option>
								<option value="1" <?php   if ( isset($_SESSION['typable'])  && $_SESSION['typable']== '1' ){ ?>
								selected= "selected" <?php } ?>>Yes</option>

							</select>
							<span class="error bold"><?php echo form_error('typable');?></span>
						</div>
					</div>

					<div class="col-md-10" >
						<div id="populate_html_for_lang"></div>

						



		 		<div class="form-group col-md-12 hide ">
			 		<button class="btn btn-info btn-xs pull-right addmoreoption"  data-count='6'  type="button" >Add More Option</button>
		 		</div>

		 
	
				  </div>
				  <div class="form-group col-md-12">
					<button class="btn btn-primary btn-sm pull-right"  type="submit" name="save" value="save_n_next">Save & Next</button>
					<button class="btn btn-info btn-sm pull-right  margin-right"  type="submit" name="save" value="save" >Save</button>
					
				  </div>
				 
				  </div>
         
			  </form>

		  </div>
	  </section>
  </div>
<div role="dialog"  id="myModal2" class="modal fade" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<h4 class="modal-title file-modal-element-head"><strong>Upload Image</strong> </h4>
            </div>
            <div class="modal-body">
				<div class="panel-body">
			  <form role="form" method="post" id="image_upload_form" enctype="multipart/form-data">
				   <span class="error bold img_upload_form" id="image_url_error"></span>
		        <div class="form-group">
					  <input type="file" accept="image/*" name = "image_file" id="image_file" class="img_upload_form">
				  </div>

				   <div class="form-group col-md-4 img_status"    >
					   <label><strong>Uploaded Image</strong></label>
					  <input readonly type="text" id="img_status_value" class="form-control input-sm img_upload_form" data-url="" value="">
				  </div>
				  <div class="form-group col-md-4 img_status"   >
					   <img id="img_src" src="" alt="" height="42" width="42">

				  </div>
				   <div class="form-group col-md-4 img_status"   >
					  <button class="copy_url">Copy Url</button>

				  </div>


				  <button class="btn btn-info img_upload_form"  type="button" id="image_upload_btn" >Upload</button>
			  </form>
		  </div>
			</div>
			</div>
		</div>
	</div>
<?php
$adminurl = AUTH_PANEL_URL;
$validation_js = AUTH_ASSETS."js/jquery.validate.min.js";
if(isset($_SESSION['subject_id'])){

	$subject_id=$_SESSION['subject_id'];
}else{
	$subject_id='';	
}
if(isset($_SESSION['topic_id'])){
	
		$topic_id=$_SESSION['topic_id'];
	}else{
		$topic_id='';	
	}

$custum_js = <<<EOD
              <script src="$validation_js" type="text/javascript"></script>
                <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
               <script type="text/javascript" language="javascript" >
			 var subject_id="$subject_id";
			 var topic_id="$topic_id";
		//	alert(topic_id);
                  jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"' >"+value.name+"</option>";
						});
						$(".subject_element_select").html(html).val(subject_id).change();
						
						// if(subject_id=''){
						// 	alert(' not blank');
							
						//  $(".subject_element_select").html(html).val(subject_id).change();
						// }else{
						// 	alert( 'blank');
							
						// 	$(".subject_element_select").html(html);	
						// }
                      }
                    });

                    $( ".stream_element_select" ).change(function() {
                      val = $(this).val();
                      $('.sub_element_select').val('');
                      $('.substream').hide();
                      $('.sub'+val).show();
                    });

                    $( ".subject_element_select" ).change(function() {
						
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.topic+"</option>";
                          });
                           $(".topic_element_select").html(html).val(topic_id);
                        }
                      });
                    });

									
					
					
					function validate_input(){

				 						var type = $(".question_type").val();
				 						var length = $('.checkbox_class:checked').length;
										 var question  = $(".question_input").val();
										 var language  = $(".language").val();
										 var length_lng = $('.language_checkbox:checked').length;
										 var typable  = $(".typable").val();
										 
				 						
				 						if(length == 0){
				 							show_toast('error', 'Please provide atleast one answer','Answer');
				 							return false;
				 						}
										 if((type == "SC"  || type == "TF")  && length_lng=='1' && length > 1){
											show_toast('error', 'Please provide only one answer','Answer');
											return false;
										}
										if((type == "SC"  || type == "TF")   && length > 2 && length_lng=='2') {
										 show_toast('error', 'Please provide only one answer','Answer');
										 return false;
									 }

									 if(typable!=='1'){
				 						clock = 0;
										 $(".language_checkbox:checked").each(function () {
											var lang_id = $(this).val();
											
										   $(".checkbox_class:checked").each(function () {
											   var ans_id= "option_"+$(this).val();
												var check = "option_"+$(this).val()+"_"+lang_id;
												 check  =  check.trim();
												   if($('textarea[name='+check+']').val() == ""){
													  // alert('hii');
													   show_toast('error', 'The '+ans_id+' can not be blank.','Answer');
													   clock = 1 ;
												   }
										   });
										});
										if(clock == 1 ){
											return false;
										}
										
									 
										var lng_st=0;
										$(".language_checkbox:checked").each(function () {
										 var lang_id = $(this).val();
										set = "";
										set_shw = "";
										max= 5;
										for (i = 3; i <= max; i++) {
											var name_data="option_"+i+"_"+lang_id;
										 console.log(name_data);
										 console.log(set_shw);
										 if($('textarea:input[name='+name_data+']').val() == "" ){
											 set = "option_"+i+"_"+lang_id;
											 set_shw = 'option_'+i;
										 }
										 if($('textarea:input[name='+name_data+']').val() !== ""  &&  set_shw!==''){
														show_toast('error', 'The '+set_shw+' can not be blank.','Answer');
													lng_st=1;
												}
												if(i == max){
												 set_shw = "";
											 }
	 
										}	  
									 });
									 if(lng_st == 1 ){
										 return false;
									 }
								 }
									}
									 $("#populate_html_for_lang").on("click", ".upload_image",function(){
							         $('#myModal2').modal('show');
													});
													/* description magic */
													$("#populate_html_for_lang").on("click", ".upload_description",function(){
														if($(this).html()== "Add Solution" ){
															$('.question_description').show('slow');
															$(this).html('Hide Solution');
														}else{
															$('.question_description').hide('slow');
															$(this).html('Add Solution');
														}
													});
												/* ckeditor magic */
												
												$("#populate_html_for_lang").on("click", ".use_editor",function(){
												//	alert('hiii');
												// $( ".use_editor" ).click(function(){
													name = 	$(this).data('name');

													if($(this).html()== "Use Editor" ){
														CKEDITOR.replace(name);
														$(this).html('Remove Editor');
													}else{
														CKEDITOR.instances[name].destroy();
														$(this).html('Use Editor');
													}
												});

											//addmoreoption
											$( ".addmoreoption" ).click(function(){

													 num =  $(this).data('count');
														$('.moreopt_'+num).show();
														$(this).data('count',num+1);
														if(num == 10 ){
															$(this).hide();
														}
											});

											$(document).ready(function (e) {
											$('.img_status').hide();
											$("#image_upload_form").on('click',(function(e) {
											var image_data = $('#image_file').val();
												if(image_data == ''){
															$("#image_url_error").text("Please select an image first");
														}
												else{
												e.preventDefault();
												$.ajax({
													url: "$adminurl"+"question_bank/question_bank/add_image",
													type: "POST",
												 	data:new FormData(this),
													cache:false,
													dataType:'json',
													contentType: false,
													processData: false,
													success:function(data){
														var url = data.url;

														$(".img_status").show();
														$("#img_status_value").val(data.url);
														$('#img_status_value').data('url',data.url);
														$('.img_upload_form').hide();
														$('#img_src').attr('src', data.url );
														$("#img_url_uplod").val(data.url);
														
														console.log("success");
														console.log(data);
													},
													error: function(data){
														console.log("error");
														console.log(data);
													}
											   });
											   }
											}));

										});

										$("body").on("click", ".copy_url", function(event){
										var url = $('#img_status_value').val();
										var tmpInput = $('<input>');
										  tmpInput.val($('#img_status_value').data('url'));
										  $('body').append(tmpInput);
										  tmpInput.select();
										  document.execCommand('copy');
										  tmpInput.remove();
										  alert("Url copied paste it anywhere to use image url")
									});


               </script>
							 <script type="text/javascript">
		                  $(document).ready(function () {
		                      var form = $("#add_question");

		                        form.validate({
		                            errorPlacement: function errorPlacement(error, element) {
		                                element.after(error);
		                            },
		                            rules: {
										test_name: {
		                                	required: true
		                                },
										language: {
		                                	required: true
		                                },
		                               
										option_1: {
		                                	required: true
		                                },
										option_2: {
		                                	required: true
		                                },
		                                
										subject_id: {
		                                	required: true
		                                },
										topic_id: {
		                                	required: true
		                                },
		                                stream_id: {
		                                	required: true
		                                },
		                                sub_stream_id: {
		                                	required: true
		                                }
		                            }
								});
								$(".split_div").hide();
								
		                    });
		              </script>
									<!-- swithery-->
									<script type="text/javascript">
											$(document).ready(function () {
													//default
													$(".js-switch-small").each(function () {
														var elem = document.querySelector("#"+$(this).attr("id"));
														var switchery = new Switchery(elem, { size: 'small' });
										        });
												$(".split_div").hide();
											});


											function show_split_div(split_type){
											//	var split_type = $("#question_screentype").val();
											
												if(split_type=='1'){
												//	alert(split_type);
													$(".split_div").show();

												}else{
													$(".split_div").hide();
												//	alert(split_type);
												}
											
											}

										
	function getSelectedOptions(sel) {
		// select all lang 
		$("#populate_html_for_lang").html('');
		var paragraph = "";
		var question = "";
		var solution = "";
		var option1 = "";
		var option2 = "";
		var option3 = "";
		var option4 = "";
		var option5 = "";
		if($(".language_checkbox:checked").length <= 2) {
			$('.language_checkbox:checked').each(function() {
			var lang_id = $(this).val();
			var lang_text = $(this).data('lang-name');
			// append first paragraph html 
			paragraph += '  <div class="form-group col-md-6 split_div" style="display:none;"> <label >USE PARAGRAPGH <sup>('+lang_text+')</sup></label> <button type="button" class="btn-xs btn btn-warning use_editor margin-left" data-name="split_text_'+lang_id+'" >Use Editor</button> <textarea rows="10" cols="50" class="form-control input-sm editor " id="split_text_'+lang_id+'" name="split_text_'+lang_id+'" required></textarea> <span class="error bold"></span> </div>';
			
			// append question 
			question += ' <div class="form-group col-md-12"><button type="button" class="btn-xs btn btn-success pull-right upload_description margin-left hide " >Add Solution </button> <button type="button" class="btn-xs btn btn-info pull-right upload_image margin-left" >Add Image</button> <label >Question  <sup>('+lang_text+')</sup></label> <button type="button" class="btn-xs btn btn-warning use_editor margin-left" data-name="question_'+lang_id+'" >Use Editor</button> <textarea rows="1" cols="50" class="form-control input-sm question_input" name="question_'+lang_id+'" required></textarea> </div>';
		 //apend solution
		 solution += '  <div class="form-group col-md-12 question_description" style=""> <label >Solution <sup>('+lang_text+')</sup></label> <button type="button" class="btn-xs btn btn-warning use_editor margin-left" data-name="description_'+lang_id+'" >Use Editor</button> <textarea rows="1" cols="50" class="form-control input-sm editor " id="description_'+lang_id+'" name="description_'+lang_id+'" required></textarea> <span class="error bold"></span> </div>';
		 
			//append option 1
		  option1 += ' <div class="form-group col-md-12 "><div class="col-md-12"> <label class="pull-left" >Option 1<sup>('+lang_text+')</sup></label> <button class="btn-xs btn btn-warning use_editor margin-left" data-name="option_1_'+lang_id+'" type="button">Use Editor</button> <div class="pull-right"> <small class="bold">Set as right answer</small> <input type="checkbox" value="1" name="answer[]" id="cb_option_1'+lang_id+'" class="answer_meta_check checkbox_class opt_1"  /> </div> </div> <textarea rows="1" cols="50" class="form-control input-sm editor " name="option_1_'+lang_id+'" required ></textarea> <span class="error bold"></span> </div>';
		  //append option 2
		  option2 += ' <div class="form-group col-md-12 editor_opt"><div class="col-md-12 "> <label class="pull-left" >Option 2<sup>('+lang_text+')</sup></label> <button class="btn-xs btn btn-warning use_editor margin-left" data-name="option_2_'+lang_id+'" type="button">Use Editor</button> <div class="pull-right"> <small class="bold">Set as right answer</small> <input type="checkbox" value="2" name="answer[]" id="cb_option_2'+lang_id+'" class=" answer_meta_check checkbox_class opt_2" /> </div> </div> <textarea rows="1" cols="50" class="form-control input-sm editor editor_opt" name="option_2_'+lang_id+'"  required ></textarea> <span class="error bold"></span> </div>';
		  //append option 3
		  option3 += ' <div class="form-group col-md-12 editor_opt "><div class="col-md-12"> <label class="pull-left" >Option 3<sup>('+lang_text+')</sup></label> <button class="btn-xs btn btn-warning use_editor margin-left" data-name="option_3_'+lang_id+'" type="button">Use Editor</button> <div class="pull-right"> <small class="bold">Set as right answer</small> <input type="checkbox" value="3" name="answer[]" id="cb_option_3'+lang_id+'" class="answer_meta_check checkbox_class opt_3" /> </div> </div> <textarea rows="1" cols="50" class="form-control input-sm editor editor_opt" name="option_3_'+lang_id+'"  ></textarea> <span class="error bold"></span> </div>';
		  //append option 4
		  option4 += ' <div class="form-group col-md-12 editor_opt"><div class="col-md-12"> <label class="pull-left" >Option 4<sup>('+lang_text+')</sup></label> <button class="btn-xs btn btn-warning use_editor margin-left" data-name="option_4_'+lang_id+'" type="button">Use Editor</button> <div class="pull-right"> <small class="bold">Set as right answer</small> <input type="checkbox" value="4" name="answer[]" id="cb_option_4'+lang_id+'" class="answer_meta_check checkbox_class opt_4" /> </div> </div> <textarea rows="1" cols="50" class="form-control input-sm editor editor_opt " name="option_4_'+lang_id+'"  ></textarea> <span class="error bold"></span> </div>';
		  //append option 5
		  option5 += ' <div class="form-group col-md-12 editor_opt"><div class="col-md-12"> <label class="pull-left" >Option 5<sup>('+lang_text+')</sup></label> <button class="btn-xs btn btn-warning use_editor margin-left" data-name="option_5_'+lang_id+'" type="button">Use Editor</button> <div class="pull-right"> <small class="bold">Set as right answer</small> <input type="checkbox" value="5" name="answer[]" id="cb_option_5'+lang_id+'" class="answer_meta_check checkbox_class opt_5" /> </div> </div> <textarea rows="1" cols="50" class="form-control input-sm editor editor_opt " name="option_5_'+lang_id+'" ></textarea> <span class="error bold"></span> </div>';
		
		});

		// finalise html 
		paragraph = '<div class="col-md-12">'+paragraph+'</div>';
		question  = '<div class="col-md-12">'+question+'</div>';
		solution  = '<div class="col-md-12">'+solution+'</div>';
		option1  = '<div class="col-md-12">'+option1+'</div>';
		option2  = '<div class="col-md-12">'+option2+'</div>';
		option3  = '<div class="col-md-12">'+option3+'</div>';
		option4  = '<div class="col-md-12">'+option4+'</div>';
		option5  = '<div class="col-md-12">'+option5+'</div>';
		$("#populate_html_for_lang ").append(paragraph+question+solution+option1+option2+option3+option4+option5);
	
	}else{

		$(this).prop('checked', false);
		alert('You can select only 2 languages ');
	}
	}

	
	/* magic strat here */
	getSelectedOptions();
	</script>
	<script>

	$(document).on('click', '.answer_meta_check:checkbox', function(event) {
		var val = $(this).val();
		var cls = '.opt_'+val;
		if(this.checked) {
			// Iterate each checkbox
			$(cls+':checkbox').each(function() {
				this.checked = true;
			});
		} else {
			$(cls+':checkbox').each(function() {
				this.checked = false;
			});
		}
	});





	$(document).on('change', '.typable', function(event) {
		var val = $(this).val();
		var cls = '.opt_'+val;
		if(val=='1') {
			$(".editor_opt").hide();
		
		} else {
			
			$(".editor_opt").show();
			
		
		}
	});
	</script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
