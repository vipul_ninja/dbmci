<?php 
$all_language = $this->db->select('id,language')->get('language_code')->result_array();
?>
<div class="col-lg-12 add_series_element" style="">
	  <section class="panel">
		  <header class="panel-heading">
			  Add Question in Bulk
		  </header>
		  <div class="panel-body">
			  <form  autocomplete="off" novalidate="novalidate"  id="add_question"  onSubmit="return validate_input()"  method="post" action="<?php echo AUTH_PANEL_URL.'question_bank/question_bank/add_bulk_question_csv' ?>"  enctype="multipart/form-data">
				 	<div class="col-md-3">
					 	<?php
					 		$all_option = $this->db->where('status',0)->get('course_stream_name_master')->result();
					 		$main_option = $sub_option = "";
					 		foreach($all_option as $ao){
					 			if($ao->parent_id == 0 ){
					 				$main_option .= "<option value='".$ao->id."'>".$ao->name." (ID= ".$ao->id.")</option>";
					 			}else{
					 				$sub_option .= "<option style='display: none;' class='substream sub".$ao->parent_id."' value='".$ao->id."'>".$ao->name." (ID= ".$ao->id.")</option>";
					 			}
					 		}
					 	?>
						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Main stream</label>
							<select class="form-control input-xs stream_element_select" name="stream_id">
							<option value=''>--select Stream--</option>
							<?php echo $main_option;?>
							</select>

						</div>

						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Sub stream</label>
							<select class="form-control input-xs sub_element_select" name="sub_stream_id">
							<option value=''>--select Sub Stream--</option>
							<?php echo $sub_option;?>
							</select>

						</div>

						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Subject</label>
							<select class="form-control input-xs subject_element_select" name="subject_id">

							</select>
							<span class="error bold"><?php echo form_error('subject_id');?></span>
						</div>
						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Topic</label>
							<select class="form-control input-xs topic_element_select" name="topic_id">

							</select>
							<span class="error bold"><?php echo form_error('topic_id');?></span>
						</div>
						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Language</label>
							<select class="form-control input-xs language_select" name="language_select">
							<option >---select language---</option>
							<?php  foreach($all_language as $all_language){  ?> 
                    <option value="<?php echo $all_language['id'] ?>"> <?php echo $all_language['language'];  echo ' (ID='.$all_language['id'].')'; ?> </option>
							<?php  }?>
							</select>
							<span class="error bold"><?php echo form_error('topic_id');?></span>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group col-md-12">
							<section class="panel">
									<header class="panel-heading">
											Upload CSV <a href="<?php echo base_url().'auth_panel_assets/sample_csv_questions.csv'; ?>" class="pull-right btn-sm btn btn-info btn-xs">Download Sample CSV</a>
									</header>
										<div class="panel-body">
											<div class="form-group">
												<label for="exampleInputFile">File input</label>
												<input type="file" accept=".csv" id="exampleInputFile" name ="csvfile">
											</div>
										</div>
										<div class="alert alert-success">
											<?php echo $csv_out;?>
										</div>
								 <div class="alert alert-success">
										<span class="bold"> GUIDELINES :-</span>
										<ul class="margin-bottom-none padding-left-lg">
											<li><span class="bold">question : </span>This field is for question content.</li>
											<li><span class="bold">description : </span>This field is for question description.</li>
											<li><span class="bold">question_type : </span>This field is for question type :- MC - for multiple choice , SC -  for single choice , TF - for True & False</li>
											<li><span class="bold">difficulty_level : </span>This field is for questions difficulty :- valueshold be 1,2,or 3 (1 - Easy , 2 -  medium , 3 - hard) </li>
											<li><span class="bold">duration : </span>This field is for test duration in minutes</li>
											<li><span class="bold">Option_1 : </span>This field is for question's option-1 </li>
											<li><span class="bold">Option_2 : </span>This field is for question's option-2</li>
											<li><span class="bold">Option_3 : </span>This field is for question's option-3</li>
											<li><span class="bold">Option_4 : </span>This field is for question's option-4</li>
											<li><span class="bold">Option_5 : </span>This field is for question's option-5</li>
											<li><span class="bold">answer : </span>This field contains the answer of particular question and it should contain answer/answers separated by comma ex - 1 or 1,2,5</li>
											<li><span class="bold">Main stream : </span>Id for Main Stream is available in select box.</li>
											<li><span class="bold">Sub stream : </span>Id for Sub Stream is available in select box.</li>
											<li><span class="bold">Subject : </span>Id for Subject is available in select box.</li>
											<li><span class="bold">Topic : </span>Id for Subject is available in select box.</li>
											<li><span class="bold">Language : </span>Id for language is available in select box.</li>
											<li><span class="bold">Config_id : </span>Unique Question Id .It is required</li>


										</ul>
								 </div>
						</section>

						</div>

						<div class="form-group col-md-12">
						<button class="btn btn-info btn-sm"  type="submit" >Add</button>
						</div>

					</div>
	  	</section>
		</form>
  </div>
<?php
$adminurl = AUTH_PANEL_URL;
$validation_js = AUTH_ASSETS."js/jquery.validate.min.js";
$custum_js = <<<EOD
              <script src="$validation_js" type="text/javascript"></script>
               <script type="text/javascript" language="javascript" >

                  jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"'>"+value.name+" (ID = "+value.id+")</option>";
                        });
                         $(".subject_element_select").html(html);
                      }
                    });

                    $( ".stream_element_select" ).change(function() {
                      val = $(this).val();
                      $('.sub_element_select').val('');
                      $('.substream').hide();
                      $('.sub'+val).show();
                    });

                    $( ".subject_element_select" ).change(function() {
                      id = $(this).val();
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.topic+" (ID = "+value.id+")</option>";
                          });
                           $(".topic_element_select").html(html);
                        }
                      });
                    });



               </script>
							 <script type="text/javascript">
		                  $(document).ready(function () {
		                      var form = $("#add_question");

		                        form.validate({
		                            errorPlacement: function errorPlacement(error, element) {
		                                element.after(error);
		                            },
		                            rules: {
																		// subject_id: {
		                                // 	required: true
		                                // },
																		// topic_id: {
		                                // 	required: true
		                                // },
		                                // stream_id: {
		                                // 	required: true
		                                // },
		                                // sub_stream_id: {
		                                // 	required: true
		                                // },
																		csvfile: {
		                                	required: true
		                                }
		                            }
		                        });
		                    });
		              </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
