<div class="col-lg-12">
  <section class="panel">
      <header class="panel-heading">
          Add EMAIL Template
      </header>
      <div class="panel-body">
          <form method="POST" action="<?php echo AUTH_PANEL_URL.'mailer/add_email_template' ?>" role="form">
               <div class="form-group ">
					  <label >Template Name</label>
					  <input type="text" placeholder="Enter template name" name = "template_name"  class="form-control input-sm"><span class="error bold"><?php echo form_error('template_name');?></span>
					   
				  </div>
              <div class="form-group">
                    <label for="exampleInputPassword1">Type message</label>
					<textarea class="form-control" name="template_html"> </textarea>
              </div>
              <button class="btn btn-info" type="submit">Submit</button>
          </form>

      </div>
  </section>
</div>


<?php
$assetsurl = AUTH_ASSETS.'assets/ckeditor/ckeditor.js';
$custum_js = <<<EOD
              
        <script type="text/javascript" src="$assetsurl"></script>     
          <script>
            CKEDITOR.replace('template_html');
          </script>

EOD;

  echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>