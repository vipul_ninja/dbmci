<?php 
//$this->db->where('id',1);
$info = $this->db->get_where('version_control',array('id'=>1))->row_array();

?>

<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            DEVICE LATEST VERSION
        </header>
        <div class="panel-body">
            <form method="POST" action="" role="form" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2" ><i class="fa fa-android fa-2x fa-spin"></i> Android</label>
                    <input type="text" value="<?=!empty($info)?$info['android']:''?>" name="android" placeholder="android version" class="form-control">
                </div>
                <div class="form-group">
                    <label  ><i class="fa fa-apple fa-2x fa-spin"></i> IOS</label>
                    <input type="text" name="ios" value="<?=!empty($info)?$info['ios']:''?>"  placeholder="ios version" class="form-control">
                </div>
                <button class="btn btn-success" type="submit">save</button>
            </form>

        </div>
    </section>

</div>