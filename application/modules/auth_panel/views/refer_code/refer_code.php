<div>
   <div class="col-lg-6">
      <section class="panel">
         <header class="panel-heading">
            POINTS VALUE
         </header>
         <div class="panel-body">
            <form autocomplete="off" role="form" novalidate="novalidate" id="refer_points" method= "POST" enctype="multipart/form-data">
            <div class="form-group">
                  <label for="meta_value">POINTS TO RUPEE CONVERSION RATE  </label>
                  <input type="text" class="form-control  input-sm" name="RUPEE_CONVERSION_RATE" value="<?php echo get_db_meta_key('RUPEE_CONVERSION_RATE');?>"  placeholder="Enter points">
                  <span class="bold small">NOTE-: Please provide points value which will equal to 1 RUPEE.</span>
               </div>
               
               <div class="form-group">
                  <label for="meta_value">MINIMUM COIN TO REDEEM </label>
                  <input type="text" class="form-control  input-sm" name="MINIMUM_COIN_TO_REDEEM" value="<?php echo get_db_meta_key('MINIMUM_COIN_TO_REDEEM');?>"  placeholder="Enter points">
               </div>               
               <div class="form-group">
                  <label for="meta_value">REFER POINTS </label>
                  <input type="text" class="form-control  input-sm" name="REFER_POINTS" value="<?php echo get_db_meta_key('REFER_POINTS');?>"  placeholder="Enter points">
               </div>

               <div class="form-group">
                  <label for="meta_value">POST LIKE</label>
                  <input type="text" class="form-control  input-sm" name="POST_LIKE"  value="<?php echo get_db_meta_key('POST_LIKE');?>" placeholder="Enter points">
               </div>
               <div class="form-group">
                  <label for="meta_value">COMMENT_ADD</label>
                  <input type="text" class="form-control  input-sm" name="COMMENT_ADD" value="<?php echo get_db_meta_key('COMMENT_ADD');?>" placeholder="Enter points">

               </div>


               <div class="form-group">
                  <label for="meta_value">USER_FOLLOW</label>
                  <input type="text" class="form-control  input-sm" name="USER_FOLLOW" value="<?php echo get_db_meta_key('USER_FOLLOW');?>"  placeholder="Enter points">

               </div>

               <div class="form-group">
                  <label for="meta_value">COURSE_REVIEW</label>
                  <input type="text" class="form-control  input-sm" name="COURSE_REVIEW" value="<?php echo get_db_meta_key('COURSE_REVIEW');?>" placeholder="Enter points">

               </div>

               <div class="form-group">
                  <label for="meta_value">COURSE_INSTRUCTOR_REVIEW</label>
                  <input type="text" class="form-control  input-sm" name="COURSE_INSTRUCTOR_REVIEW" value="<?php echo get_db_meta_key('COURSE_INSTRUCTOR_REVIEW');?>"  placeholder="Enter points">

               </div>

               <div class="form-group">
                  <label for="meta_value">POST_ADD</label>
                  <input type="text" class="form-control  input-sm" name="POST_ADD" value="<?php echo get_db_meta_key('POST_ADD');?>"  placeholder="Enter points">

               </div>

               <div class="form-group">
                  <label for="meta_value">MCQ_ADD</label>
                  <input type="text" class="form-control  input-sm" name="SURVEY_ADD" value="<?php echo get_db_meta_key('SURVEY_ADD');?>"  placeholder="Enter points">

               </div>

               <div class="form-group">
                  <label for="meta_value">WELCOME_POINTS</label>
                  <input type="text" class="form-control  input-sm" name="WELCOME_POINTS" value="<?php echo get_db_meta_key('WELCOME_POINTS');?>" placeholder="Enter points">

                </div>
                <div class="form-group">
                  <label for="meta_value">MCQ ATTEMPT COINS</label>
                  <input type="text" class="form-control  input-sm" name="MCQ_COINS" value="<?php echo get_db_meta_key('MCQ_COINS');?>" placeholder="Enter Coins">

                </div>
               <button type="submit" class="btn btn-info">Submit</button>
            </form>
         </div>
      </section>
   </div>
   <div class="clearfix"></div>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$validation_js = AUTH_ASSETS."js/jquery.validate.min.js";
$custum_js = <<<EOD
              <script src="$validation_js" type="text/javascript"></script>
                <script>
				
				 var form = $("#refer_points");                                          
                        form.validate({
                            errorPlacement: function errorPlacement(error, element) {
                                element.after(error);
                            },
                            rules: {
                                RUPEE_CONVERSION_RATE: {
                                required: true,
                                min: 0
                                },
                                REFER_POINTS:{
                                    required: true,
                                    min: 0,
                                    max:100
                                },
                                POST_LIKE: {
                                    required: true,
                                    min: 0,                                    
                                },
                                COMMENT_ADD: {
                                    required: true,
                                    min: 0,                                   
                                },
								USER_FOLLOW: {
                                    required: true,
                                    min: 0,                                  
                                },
								COURSE_REVIEW: {
                                    required: true,
                                    min: 0,                                    
                                },
								COURSE_INSTRUCTOR_REVIEW: {
                                    required: true,
                                    min: 0,                                 
                                },
								POST_ADD: {
                                    required: true,
                                    min: 0,                                    
                                },
								SURVEY_ADD: {
                                    required: true,
                                    min: 0,                                    
                                },
								WELCOME_POINTS: {
                                    required: true,
                                    min: 0,                                  
                                }
                                MCQ_COINS: {
                                    required: true,
                                    min: 0,                                  
                                }
								
                            }
                        }); 					  					
                   
              </script>          
EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
