<?php
    $c_id         = $this->input->get('course_id');
    $main_stream_result = $this->db->get('`course_stream_name_master`')->result_array();

    $edit_post_type = $post_content['post_type'];
    $description_box = ($edit_post_type == "user_post_type_mcq")?'':$post_content['post_data']['text'];
    $owner_id = $post_content['user_id'];
    $post_headline = $post_content['post_headline'];
    if($edit_post_type == "user_post_type_video"){

    $video_id=$this->db->where('post_id',$_GET['post_id'])->get('`user_post_video_relationship`')->row()->video_id;

    }else{

      $video_id=''; 
    }


?>

<div>
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <?php echo $p_heading; ?>
      </header>
      <div class="panel-body">
        <form autocomplete="off" id="dose_form_sub" novalidate="novalidate" role="form" method="POST" action="" enctype="multipart/form-data">
        <input type = "hidden" value="<?php echo $post_content['id'];?>" name="post_id">
        <div class="col-md-12">
         <label for="title">Category </label>
                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                <?php $p=1;$holder=array();
                                  foreach($main_stream_result as $res){
                                  if($res['parent_id']==0){
                                    $holder[]=$res['id'];
                                    ?>
                                <li class="<?php echo ($p==1)?'active':''?>"><a class="cate_parent_head parent_holder<?php echo $res['id'];?>" href="#tabprimary<?php echo $res['id'];?>" data-toggle="tab"><?php echo($res['name'].'&nbsp');?><label class="badge"></label></a></li>
                                <?php } $p++;}?>
                                <div class="pull-right margin-right"><span>Select All &nbsp</span><input id="select-all" type="checkbox" value="check all"></div>
                            </ul>
                    </div>

                    <div class="panel-body">
                        <div class="tab-content">
                          <?php
                            $all_cate_ids = $post_content['sub_cate_id'];
                          if($all_cate_ids !=""){
                            $all_cate_ids = explode(',',$all_cate_ids);
                          }else{
                             $all_cate_ids=array();
                          }

                          $hold=1;
                          for($i=0;$i<count($holder);$i++){?>
                            <div class="tab-pane fade <?php echo ($hold==1)?' in active':''?>" id="tabprimary<?php echo $holder[$i];?>">
                              <?php
                                foreach($main_stream_result as $res){
                                if($holder[$i]==$res['parent_id']){
                                 echo('<div class="col-md-2"><input '.(in_array($res['id'], $all_cate_ids)?'checked':'').' data-holder_id="'.$res['parent_id'].'" type="checkbox" name="course_cat_id[]" value="'.$res['id'].'" class="cat_check">'.$res['name'].'</div>');
                                }
                              }
                              ?>
                               <div class="pull-right margin-right  "><span>Select All &nbsp</span><input class="select-sub" data-id="tabprimary<?php echo $holder[$i];?>" type="checkbox" value="check all">
                               <?PHP 
                               ?>
                               
                               </div>
                            </div>
                          <?php $hold++;} ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
            $post_id=$this->input->get('post_id');
             $all_subjects = $this->db->get('course_subject_master')->result_array();
             $this->db->where('post_id',$post_id);
             $post_edit = $this->db->get('post_subject_relationship')->row_array();

 ?>
            <div class="form-group ">
            <label for="title">Subject Type</label>
            <select id="subject_id" name="subject_id">
            <option value="">Select Subject Type</option>
            <?php foreach ($all_subjects as $value) { 
              if($value['id']==$post_edit['subject_id']){
              ?>
             <option value="<?php echo $value['id']?>" selected><?php echo $value['name']; ?></option>
            <?php }else{ ?>

              <option value="<?php echo $value['id']?>" ><?php echo $value['name']; ?></option>

         <?php   } } ?>
            </select>
            <span class="text-danger"><?php echo form_error('stude_type');?></span>
          </div>
          <div class="form-group">
            <label for="title">Post Type </label>
            <div class="col-md-12 margin-bottom">
              <button data-color="#6fa1f2" data-type="user_post_type_normal" class="btn btn-sm bold margin-right post_selection <?php echo ($edit_post_type == "user_post_type_normal")?'':'hide'; ?> " type="button"> Text </button>
              <button data-color="#c66ff1"  data-type="user_post_type_video" class="btn  btn-sm bold margin-right  post_selection <?php echo ($edit_post_type == "user_post_type_video")?'':'hide'; ?> " type="button" > Video  </button>
              <button data-color="#f06fad"  data-type="user_post_type_article" class="btn btn-sm bold margin-right  post_selection <?php echo ($edit_post_type == "user_post_type_article")?'':'hide'; ?>  " type="button" > Article </button>
              <button data-color="#ef9e6e" data-type="user_post_type_vocab" class="btn btn-sm bold margin-right  post_selection <?php echo ($edit_post_type == "user_post_type_vocab")?'':'hide'; ?>  " type="button" > Vocab </button>
              <button data-color="#946eef" data-type="user_post_type_current_affair" class="btn btn-sm bold margin-right  post_selection <?php echo ($edit_post_type == "user_post_type_current_affair")?'':'hide'; ?>  " type="button" > Current Affair </button>
              <button data-color="#76ef6e" data-type="user_post_type_quiz" class="btn btn-sm bold margin-right  post_selection <?php echo ($edit_post_type == "user_post_type_quiz")?'':'hide'; ?>  " type="button" > Quiz </button>
            </div>           
              <input type='hidden' value="<?php echo $edit_post_type;?>" name="post_type"  class="form-control required" placeholder=""/>
              <span class="text-danger"><?php echo form_error('post_type');?></span>
          </div>
            
          <div class="form-group common_input_element title_input_element">
          <label for="title">Title</label>
          <input type="text" class="form-control" value="<?php echo  $post_headline;?>" name = "title" id="title" placeholder="Enter Title">
          <span class="text-danger"><?php echo form_error('title');?></span>
          </div>

          <div class="form-group common_input_element description_input_element">
          <label for="desc">Description</label>
          <textarea class="form-control" id="description"  name = "description" rows="3" placeholder="Enter Description"><?php echo $description_box;?></textarea>
          </div>

          <div class="form-group common_input_element">
          <label for="desc">Publish Duration</label>
          <input type="text" id="datefilter" name="datefilter" value="" />
          <span class="text-danger"><?php echo form_error('datefilter');?></span>
          </div>

          <div class="form-group common_input_element quiz_input_element">
          <label for="post_type">Quiz id </label>
          <input type='text' name="quiz_id"  class="form-control" placeholder="Enter quiz Id"/>
          <span class="text-danger"><?php echo form_error('post_type');?></span>
          </div>
          <div class="form-group common_input_element video_id_element ">
            <label for="post_type">Video id </label>
              <input type='text' name="video_id"  class="form-control required" value="<?php echo $video_id; ?>" placeholder="Enter Video Id" />
            <span class="text-danger"><?php echo form_error('user_id');?></span>
          </div> 

          <div class="form-group common_input_element user_id_input_element">
          <label for="post_type">User id </label>
          <input type='text' name="user_id" value="<?php echo $owner_id;?>"  class="form-control required" placeholder="Enter user Id"/>
          <span class="text-danger"><?php echo form_error('user_id');?></span>
          </div>
          <div class="owner_view col-md-12"></div> 
           <!-- <div class="form-group">
            <label for="desc">Display Picture</label>
            <input type='file' name="display_picture" onchange="readURL(this);" id="image_input" />
            <span class="text-danger"><?php echo form_error('pic');?></span>
          </div> -->  
          <div class="form-group col-md-12 common_input_element display_pic_input_element">
          <label for="desc">Display Picture</label>
          <input type="file" name="image_file" id="image_file" onchange="fileSelectHandler()"  />  
          <img src="<?php echo $post_content['display_picture']; ?>" height="15%" width="15%">
          </div>

          <link href="<?php echo AUTH_ASSETS; ?>assets/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet"/>
          <link href="<?php echo AUTH_ASSETS; ?>css/image-crop.css" rel="stylesheet"/>

          <div class="panel-body cropper_handler " style="display:none">
            <div class="col-md-12 col-sm-12">
              <div class="row">
               <!--  <div class="col-md-6">
                  <img src="#" id="demo3" alt="Jcrop Example" width="100%" />
                  <input type="text" style="visibility:hidden;" name="display_pic_dimension">
                </div> -->
                <div class="col-md-6">
                  <div id="preview-pane">
                    <div class="preview-container">
                      <img src="#" class="jcrop-preview" alt="Preview"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- another start -->

                    <input type="hidden" id="x1" name="x1" />
                    <input type="hidden" id="y1" name="y1" />
                    <input type="hidden" id="x2" name="x2" />
                    <input type="hidden" id="y2" name="y2" />

                   <!--  <h2>Step1: Please select image file</h2>
                    <div><input type="file" name="image_file" id="image_file" onchange="fileSelectHandler()" /></div>
 -->
                    <div class="error"></div>

                    <div class="col-md-12 step2">
                        <img id="preview"/>

                  <div class="info hide">
                      <label>File size</label> <input type="text" id="filesize" name="filesize" />
                      <label>Type</label> <input type="text" id="filetype" name="filetype" />
                      <label>Image dimension</label> <input type="text" id="filedim" name="filedim" />
                      <label>W</label> <input type="text" id="w" name="w" />
                      <label>H</label> <input type="text" id="h" name="h" />
                  </div>
          <!-- another start -->
          <div class="col-md-12"> <button type="submit"  id="submit" class="btn btn-info pull-right ">Submit</button> </div>
        </div>

      </form>
    </div>
  </section>
</div>
<div class="clearfix"></div>
</div>
<!-- page end-->
</section>


<!-- Modal -->
<div id="Errors" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-body model_error ">
         <i class="text-center fa fa-spin fa-spinner fa-4x"></i>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<?php
$adminurl             = AUTH_PANEL_URL;
$jquery_color_js      = AUTH_ASSETS.'assets/jcrop/js/jquery.color.js';
$jquery_Jcrop_min_js  = AUTH_ASSETS.'assets/jcrop/js/jquery.Jcrop.min.js';
$validation_js        = AUTH_ASSETS."js/jquery.validate.min.js";
$ckeditor_js = AUTH_ASSETS.'assets/ckeditor/ckeditor.js';


$custum_js = <<<EOD
<script src="$jquery_color_js"></script>
<script src="$jquery_Jcrop_min_js"></script>
<script src="$validation_js"></script>
<script src="$ckeditor_js"></script>

 <script>

  jQuery('.cat_check').change(function() {

    $('.cate_parent_head .badge').html('');
    var arr = [];
    var c=0;
    $('.cat_check').each(function (){

      if ($(this).prop('checked'))
      {
        p_id = $(this).data('holder_id');
        arr.push(p_id);
      }
    });

    var counts = {};
    arr.forEach(function(x) {
      counts[x] = (counts[x] || 0)+1;
      if(counts[x] > 0 ){
        $('.parent_holder'+x+' .badge').html(counts[x]);
        //console.log('#tabprimary'+x+' .badge');
      }
    });
  }).change();

 /*post selection magic starts from here */
 
  $('.post_selection').click(function(event) {

    $('.post_selection').css('background','');
    $('.post_selection').css('color','');
    $(this).css('background',$(this).data('color'));
    $(this).css('color','#fff');
    $('input[name=post_type]').val($(this).data('type'));
    view = $(this).data('type');
    reset_look_of_form(view);
  });

  $('.common_input_element').hide();
  /* reset form */
  function reset_look_of_form(view){
    /* hide Quiz input element */
    $('.common_input_element').hide();
    if(view == 'user_post_type_normal'){
      $('.description_input_element,  .user_id_input_element').show();
            if(CKEDITOR.instances["description"]){
        CKEDITOR.instances["description"].destroy();
      }
    }
    if(view == 'user_post_type_article'){
      $('.description_input_element , .title_input_element , .display_pic_input_element , .user_id_input_element').show();
      if(!CKEDITOR.instances["description"]){
        CKEDITOR.replace('description');
      }
    }

    if(view == 'user_post_type_vocab'){
      $('.description_input_element , .title_input_element , .display_pic_input_element , .user_id_input_element').show();
      if(!CKEDITOR.instances["description"]){
        CKEDITOR.replace('description');
      }
    }    
    if(view == 'user_post_type_article'){
      $('.description_input_element , .title_input_element , .display_pic_input_element , .user_id_input_element').show();
      if(!CKEDITOR.instances["description"]){
        CKEDITOR.replace('description');
        
      }
    }  

    if(view == 'user_post_type_current_affair'){
       
      $('.description_input_element , .title_input_element , .display_pic_input_element ,  .user_id_input_element').show();
      if(!CKEDITOR.instances["description"]){
        CKEDITOR.replace('description');
      }
    } 
    if(view == 'user_post_type_quiz'){
      $('.description_input_element , .quiz_input_element, .title_input_element ,.display_pic_input_element ,  .user_id_input_element').show();
      if(!CKEDITOR.instances["description"]){
        CKEDITOR.replace('description');
      }
    }
    if(view == 'user_post_type_video'){
      
     $('.description_input_element , .title_input_element , .video_id_element ,  .user_id_input_element').show();
     if(!CKEDITOR.instances["description"]){
       CKEDITOR.replace('description');
     }
   }    
      
  }


 /*post selection magic end  from here */

  /* update user view */
  $('input[name=user_id]').on('keyup blur focus', function () {
    var id = $(this).val();
    user_view(id);
  });

  function user_view(id){
    $('.owner_view').html('');
    var jqxhr = $.getJSON( "$adminurl"+"daily_dose/current_affair/view_owner/"+id, function() {
      console.log( "success" );
    })
      .done(function(data) {
        if(data ){
          html =  '<div class="panel-body bg-info "><a class="task-thumb" href="#"><img style="max-width:100px"  alt="" src="'+data.profile_picture+'"></a><div class="task-thumb-details" style="margin-left: 30px;" ><h1><a href="#">'+data.name+'</a></h1><p>'+data.email+'</p></div></div>';
                  $('#instructor_profile').html(html);
          $('.owner_view').html(html);        
        }
      })
      .fail(function() {
        console.log( "error" );
      })
      .always(function() {
        console.log( "complete" );
      });
  }

  user_view('$owner_id');

$('#select-all').click(function(event) {
    if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
        });
    } else {
        $(':checkbox').each(function() {
            this.checked = false;
        });
    }

    jQuery('.cat_check').change();
});
</script>

<script>
var FormImageCrop = function () {

  var demo3 = function() {
    // Create variables (in this scope) to hold the API and image size
    var jcrop_api,
    boundx,
    boundy,
    // Grab some information about the preview pane
    preview = $('#preview-pane'),
    pcnt = $('#preview-pane .preview-container'),
    pimg = $('#preview-pane .preview-container img'),

    xsize = 250;//pcnt.width(),
    ysize = 170;//pcnt.height();

    console.log('init',[xsize,ysize]);

    $('#demo3').Jcrop({
      onChange: updatePreview,
      onSelect: updatePreview,
      aspectRatio: xsize / ysize
    },function(){
      // Use the API to get the real image size
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];
      // Store the API in the jcrop_api variable
      jcrop_api = this;
      // Move the preview into the jcrop container for css positioning
      preview.appendTo(jcrop_api.ui.holder);
    });

    function updatePreview(c){
      $('input[name=image_file]').val(JSON.stringify(c));
      console.log('update preview');
      if (parseInt(c.w) > 0){
        var rx = xsize / c.w;
        var ry = ysize / c.h;
        pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * c.x) + 'px',
          marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
      }
    };
  }
  return {
    //main function to initiate the module
    init: function () {
      if (!jQuery().Jcrop) {
        return;
      }
      demo3();
    }
  };
}();

$(document).ready(function (e) {
  $('#dose_form_sub').on('submit',(function(e) {
    e.preventDefault();
    CKEDITOR.instances['description'].updateElement();
    var formData = new FormData(this);
    $('#Errors').modal('show');
    $.ajax({
      type:'POST',
      url: $(this).attr('action'),
      data:formData,
      dataType:'json',
      cache:false,
      contentType: false,
      processData: false,
      success:function(data){
         
        if(!data.status){
          $('#Errors').modal('show');
          $('.model_error').html(data.errors);
        }
        if(data.status == true){
          console.log(data.status);
          location.reload();
        }
      },
      error: function(data){
      }
    });
  }));
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      //console.log(e.target.result);
      $('.cropper_handler').css('display','');
      $('#demo3').attr('src', e.target.result);
      $('.jcrop-preview').attr('src', e.target.result);
      $('.jcrop-holder img').attr('src', e.target.result);
      FormImageCrop.init();
    };
    reader.readAsDataURL(input.files[0]);
  }
}

/* make editable form now */

reset_look_of_form('$edit_post_type');

/////////////

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};

// check for selected crop region
function checkForm() {
    if (parseInt($('#w').val())) return true;
    $('.error').html('Please select a crop region and then press Upload').show();
    return false;
};

// update info by cropping (onChange and onSelect events handler)
function updateInfo(e) {
    $('#x1').val(e.x);
    $('#y1').val(e.y);
    $('#x2').val(e.x2);
    $('#y2').val(e.y2);
    $('#w').val(e.w);
    $('#h').val(e.h);
};

// clear info by cropping (onRelease event handler)
function clearInfo() {
    $('.info #w').val('');
    $('.info #h').val('');
};

function fileSelectHandler() {

   // // get selected file
    var oFile = $('#image_file')[0].files[0];

    // hide all errors
    $('.error').hide();

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (! rFilter.test(oFile.type)) {
        $('.error').html('Please select a valid image file (jpg and png are allowed)').show();
        return;
    }

    // check for file size
    // if (oFile.size > 250 * 1024) {
    //     $('.error').html('You have selected too big file, please select a one smaller image file').show();
    //     return;
    // }

    // preview element
    var oImage = document.getElementById('preview');

    // prepare HTML5 FileReader
    var oReader = new FileReader();
        oReader.onload = function(e) {

        // e.target.result contains the DataURL which we can use as a source of the image
        $('.step2 img').attr("src",e.target.result);
        
      //  oImage.src = e.target.result;
        oImage.onload = function () { // onload event handler

            // display step 2
            $('.step2').fadeIn(500);

            // display some basic image info
            var sResultFileSize = bytesToSize(oFile.size);
            $('#filesize').val(sResultFileSize);
            $('#filetype').val(oFile.type);
            $('#filedim').val(oImage.naturalWidth + ' x ' + oImage.naturalHeight);

            // Create variables (in this scope) to hold the Jcrop API and image size
            var jcrop_api, boundx, boundy;

            // destroy Jcrop if it is existed
            if (typeof jcrop_api != 'undefined') 
                jcrop_api.destroy();

            // initialize Jcrop
            $('#preview').Jcrop({
                minSize: [250, 170], // min crop size
                aspectRatio : 250/170, // keep aspect ratio 1:1
                bgFade: true, // use fade effect
                bgOpacity: .3, // fade opacity
                onChange: updateInfo,
                onSelect: updateInfo,
                onRelease: clearInfo
            }, function(){

                // use the Jcrop API to get the real image size
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];

                // Store the Jcrop API in the jcrop_api variable
                jcrop_api = this;
            });
        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}
</script>
<script type="text/javascript">
$(function() {

  $('input[name="datefilter"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});

$('.select-sub').click(function(event) {
  to = $(this).data('id');
  if(this.checked) {
      // Iterate each checkbox
      $('#'+to+' .cat_check:checkbox').each(function() {
          this.checked = true;
      });
  } else {
      $('#'+to+' .cat_check:checkbox').each(function() {
          this.checked = false;
      });
  }
 jQuery('.cat_check').change();
});
</script>
EOD;

echo modules::run('auth_panel/template/add_custum_js',$custum_js );
