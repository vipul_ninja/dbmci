
<?php
$post_type=$this->input->get('post_type');
 $sql = "SELECT *,count(`id`) as total FROM `post_counter` WHERE `post_type` = '".$post_type."' and status='0'";
// //echo $sql;
 $total =  $this->db->query($sql)->row()->total;
// $sql = "SELECT count(ca_id) as total
// FROM dailydose_currentaffairs  where created_at=date('Y-m-d')";
// $published =  $this->db->query($sql)->row()->total;
// $sql = "SELECT count(ca_id) as total
// FROM dailydose_currentaffairs ";
// $for_dams_video =  $this->db->query($sql)->row()->total;
// $sql = "SELECT count(ca_id) as total
// FROM dailydose_currentaffairs ";
// $for_non_dams_video =  $this->db->query($sql)->row()->total;
?>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 20px;
  width: 20px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 30px;
}

.slider.round:before {
  border-radius: 40%;
}
</style>
<div class="col-sm-12">
    <div class=" state-overview">
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol terques">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class="count">
                    <?php echo $total; ?>
                    </h1>
                   <b><?php 
                 echo $data_no;
                    ?>
                    </b>
                   
                </div>
            </section>
        </div>
      <!--   <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol red">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="value">
                    <h1 class=" count2">
                       <?php //echo $published; ?>
                    </h1>
                    <p>Today's Current Affairs </p>
                </div>
            </section>
        </div> -->
        
</div>
<div class="col-sm-12">
  <section class="panel">
    <header class="panel-heading">
    <?php echo strtoupper($page); ?>
    </header>
    <div class="panel-body">
    <div class="adv-table">
      <div class="col-md-6 pull-right">        
      </div>
    <table  class="display table table-bordered table-striped" id="all-video-grid">
      <thead>
        <tr>
        <th>Title </th>
        <!--<th>Image</th>-->      
        <th>Created Date</th>          
        <!-- <th> Publish Duration</th>           -->
          
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
              <th></th>
              <th></th>
          </tr>
      </thead>
    </table>
    </div>
    </div>
  </section>
</div>

<?php
$post_type = $this->input->get('post_type');

$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-video-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"daily_dose/Current_affair/ajax_all_current_affairs_list?post_type=$post_type", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );
            // Re-draw the table when the a date range filter changes
                        $('.date-range-filter').change(function() {
                            if($('#min-date-video-list').val() !="" && $('#max-date-video-list').val() != "" ){
                                var dates = $('#min-date-video-list').val()+','+$('#max-date-video-list').val();
                                dataTable.columns(8).search(dates).draw();
                            }
                            if($('#min-date-video-list').val() =="" || $('#max-date-video-list').val() == "" ){
                                var dates = "";
                                dataTable.columns(8).search(dates).draw();
                            }
                        });
                   } );

           $('#min-date-video-list').datepicker({
              format: 'dd-mm-yyyy',
            autoclose: true

          });
          $('#max-date-video-list').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true

          });
$(document).ready(function(){

$('.alert-status').on('click', function(e) {
    $.ajax({
        method: 'POST',
        url: '/uri/of/your/page',
        data: {
            'my_checkbox_value': e.target.checked
        },
        dataType: 'json',
        success: function(data){
            console.log(data);
        }
    });
});
});

               </script>



EOD;

  echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
