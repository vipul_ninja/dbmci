<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="DELL" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="shortcut icon" href="img/favicon.png">

        <title><?php echo CONFIG_PROJECT_NICK_NAME ; ?></title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo AUTH_ASSETS; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo AUTH_ASSETS; ?>css/bootstrap-reset.css" rel="stylesheet">
        <!--external css-->
        <link href="<?php echo AUTH_ASSETS; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

        <!--right slidebar-->
        <link href="<?php echo AUTH_ASSETS ?>css/slidebars.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo AUTH_ASSETS; ?>css/style.css" rel="stylesheet">
        <link href="<?php echo AUTH_ASSETS; ?>css/style-responsive.css" rel="stylesheet" />

        <link  rel="stylesheet" href="<?php echo AUTH_ASSETS; ?>assets/bootstrap-datepicker/css/datepicker.css">

        <!-- date time picker css -->
        <link rel="stylesheet" type="text/css" href="<?php echo AUTH_ASSETS; ?>assets/bootstrap-datetimepicker/css/datetimepicker.css" />

        <!-- file upload css -->
        <link rel="stylesheet" type="text/css" href="<?php echo AUTH_ASSETS; ?>assets/bootstrap-fileupload/bootstrap-fileupload.css" />

        <!-- Time picker css -->
        <link rel="stylesheet" type="text/css" href="<?php echo AUTH_ASSETS; ?>assets/bootstrap-timepicker/compiled/timepicker.css" />

        <!--toastr-->
        <link href="<?php echo AUTH_ASSETS; ?>assets/toastr-master/toastr.css" rel="stylesheet" type="text/css" />

        <!--dynamic table-->
        <!--<link href="<?php echo AUTH_ASSETS; ?>assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
        <link href="<?php echo AUTH_ASSETS; ?>assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo AUTH_ASSETS; ?>assets/data-tables/DT_bootstrap.css" />-->
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="<?php echo AUTH_ASSETS; ?>js/html5shiv.js"></script>
          <script src="<?php echo AUTH_ASSETS; ?>js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

        <link rel="stylesheet" type="text/css" href="<?php echo AUTH_ASSETS; ?>assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo AUTH_ASSETS; ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo AUTH_ASSETS; ?>assets/bootstrap-daterangepicker/daterangepicker.css" />


        <!--bootstrap switcher-->
        <link rel="stylesheet" type="text/css" href="<?php echo AUTH_ASSETS; ?>assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css" />

        <!-- switchery-->
        <link rel="stylesheet" type="text/css" href="<?php echo AUTH_ASSETS; ?>assets/switchery/switchery.css" />
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

    </head>

    <body>


        <section id="container">
            <!--header start-->
            <header class="header white-bg">
                <div class="sidebar-toggle-box">
                    <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
                </div>
                <!--logo start-->
                <a href="" class="logo" ><?php echo CONFIG_PROJECT_NICK_NAME ; ?></a>
                <!--logo end-->
                <div class="nav notify-row" id="top_menu">
                </div>
                <div class="top-nav ">
                    <ul class="nav pull-right top-menu">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <?php
                                $user_data = $this->session->userdata('active_user_data');
                                if ($user_data->profile_picture != "") {
                                    echo "<img class='img-thumbnail' width='30px' src='" . $user_data->profile_picture . "' />";
                                } else {
                                    echo '<i class ="fa fa-user"></i>';
                                }
                                ?>
                                <span class="username">ACCOUNT</span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li><a class=""  href="<?php echo site_url('auth_panel/profile/profile_edit'); ?>"><i class=" fa fa-suitcase "></i>Profile</a></li>
                                <li><a  class=""  href="<?php echo site_url('auth_panel/login/logout'); ?>"><i class="fa fa-key"></i>  Log Out</a></li>
                                <li><a  class=""  href="#"><i class="fa fa-bell-o "></i>
                                        <?php
                                        echo $user_data->username . '</br>' . $user_data->email;
                                        ?>
                                    </a></li>
                            </ul>
                        </li>

                        <!-- user login dropdown end -->
                    </ul>
                </div>
            </header>
            <!--header end-->
            <!--sidebar start-->
            <?php
            /*             * ***** Sidebar  Check For Instructor/SuperAdmin User Using Session ****** */
            $side_bar = "SIDEBAR_SUPER_USER";
            if ($user_data->instructor_id != 0) {
                $side_bar = "SIDEBAR_INSTRUCTOR_USER";
            }
            $this->load->view($side_bar);
            /*             * ***** Sidebar  Check For Instructor/SuperAdmin User Using Session Ends ****** */
            ?>

            <!--sidebar end-->
            <!--main content start-->
            <section id="main-content" >

                <section class="wrapper site-min-height">

                    <div class="row">
                        <div class="col-lg-12 hide">
                            <ul class="breadcrumb">
                                <li class="active capitalize"><?php echo isset($page_title) ? $page_title : ""; ?></li>
                            </ul>
                        </div>

                        <div class="col-lg-12">

                            <?php echo isset($page_data) ? $page_data : ""; ?>
                        </div>

                    </div>
                </section>
            </section>
            <!--main content end-->

            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                <?php echo CONFIG_PROJECT_NICK_NAME ; ?>© Backend
                    <a href="#" class="go-top">
                        <i class="fa fa-angle-up"></i>
                    </a>
                </div>
            </footer>
            <!--footer end-->
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="<?php echo AUTH_ASSETS; ?>js/jquery.js"></script>
        <script src="<?php echo AUTH_ASSETS; ?>js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo AUTH_ASSETS; ?>js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="<?php echo AUTH_ASSETS; ?>js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo AUTH_ASSETS; ?>js/slidebars.min.js"></script>
        <script src="<?php echo AUTH_ASSETS; ?>js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="<?php echo AUTH_ASSETS; ?>js/respond.min.js" ></script>

        <!-- file upload button -->
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>

        <!-- Date time  picker -->
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

        <!--toastr-->
        <script src="<?php echo AUTH_ASSETS; ?>assets/toastr-master/toastr.js"></script>

        <!--common script for all pages-->
        <script src="<?php echo AUTH_ASSETS; ?>js/common-scripts.js"></script>



        <!--custom switch-->
        <script src="<?php echo AUTH_ASSETS; ?>js/bootstrap-switch.js"></script>
        <!--custom tagsinput-->
        <script src="<?php echo AUTH_ASSETS; ?>js/jquery.tagsinput.js"></script>

        <!--date picker-------->
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>assets/bootstrap-daterangepicker/date.js"></script>
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!--time picker-------->
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <!--jquery knob for charts-------->
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>assets/jquery-knob/js/jquery.knob.js"></script>

        <!--custom switch-->
        <script src="<?php echo AUTH_ASSETS; ?>js/bootstrap-switch.js"></script>
        <!--custom tagsinput-->
        <script src="<?php echo AUTH_ASSETS; ?>js/jquery.tagsinput.js"></script>
        <!--custom checkbox & radio-->
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>js/ga.js"></script>
        <!--bootstrap-switch-->
        <script src="<?php echo AUTH_ASSETS; ?>assets/bootstrap-switch/static/js/bootstrap-switch.js"></script>

        <!--bootstrap-switch-->
        <script src="<?php echo AUTH_ASSETS; ?>assets/switchery/switchery.js"></script>
        <!--script for this page-->
        <script src="<?php echo AUTH_ASSETS; ?>js/form-component.js"></script>

        <!--bootstrap swither-->
        <script type="text/javascript">
            $(document).ready(function () {
                // Resets to the regular style
                $('#dimension-switch').bootstrapSwitch('setSizeClass', '');
                // Sets a mini switch
                $('#dimension-switch').bootstrapSwitch('setSizeClass', 'switch-mini');
                // Sets a small switch
                $('#dimension-switch').bootstrapSwitch('setSizeClass', 'switch-small');
                // Sets a large switch
                $('#dimension-switch').bootstrapSwitch('setSizeClass', 'switch-large');


                $('#change-color-switch').bootstrapSwitch('setOnClass', 'success');
                $('#change-color-switch').bootstrapSwitch('setOffClass', 'danger');
            });
        </script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>

        <?php /* global ajax handler if authentication failure server will return a code and  it will catch that
         *   end here
         */
        ?>
        <?php echo $javascript; ?>
        <script type="text/javascript">
            var i = -1;
            var toastCount = 0;
            var $toastlast;
            function show_toast(type, text, title) {
                var shortCutFunction = type;
                var msg = text;
                var toastIndex = toastCount++;

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "3000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "10000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }

                if ($('#addBehaviorOnToastClick').prop('checked')) {
                    toastr.options.onclick = function () {
                        alert('You can perform some custom action after a toast goes away');
                    };
                }


                if (!msg) {
                    msg = getMessage();
                }

                $("#toastrOptions").text("Command: toastr["
                        + shortCutFunction
                        + "](\""
                        + msg
                        + (title ? "\", \"" + title : '')
                        + "\")\n\ntoastr.options = "
                        + JSON.stringify(toastr.options, null, 2)
                        );

                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;
                if ($toast.find('#okBtn').length) {
                    $toast.delegate('#okBtn', 'click', function () {
                        alert('you clicked me. i was toast #' + toastIndex + '. goodbye!');
                        $toast.remove();
                    });
                }
                if ($toast.find('#surpriseBtn').length) {
                    $toast.delegate('#surpriseBtn', 'click', function () {
                        alert('Surprise! you clicked me. i was toast #' + toastIndex + '. You could perform an action here.');
                    });
                }
            }



            $('#clearlasttoast').click(function () {
                toastr.clear(getLastToast());
            });
            $('#cleartoasts').click(function () {
                toastr.clear();
            });
<?php
//$page_toast_type = "error"; $page_toast = "toast"; $page_toast_title = "title";
if ($page_toast_type != "" && $page_toast != "") {
    ?>
                $('#toast-container').css("width", "100%");
                show_toast('<?php echo $page_toast_type; ?>', '<?php echo $page_toast; ?>', '<?php echo $page_toast_title; ?>');

    <?php
} elseif (isset($_SESSION['page_alert_box_type']) && isset($_SESSION['page_alert_box_title']) && isset($_SESSION['page_alert_box_message'])) {
    ?>
                $('#toast-container').css("width", "100%");
                show_toast('<?php echo $_SESSION['page_alert_box_type']; ?>', '<?php echo $_SESSION['page_alert_box_message']; ?>', '<?php echo $_SESSION['page_alert_box_title']; ?>');
    <?php
    //setcookie('page_alert_box_type', "", time() , "/");
    //setcookie('page_alert_box_title', "", time() , "/");
    //setcookie('page_alert_box_message', "", time(), "/");
    unset($_SESSION['page_alert_box_type']);
    unset($_SESSION['page_alert_box_title']);
    unset($_SESSION['page_alert_box_message']);
}
?>
        </script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script src="<?php echo AUTH_ASSETS; ?>js/tasks.js" type="text/javascript"></script>
        <script>
            jQuery(document).ready(function () {
                TaskList.initTaskWidget();
            });

            $(function () {
                $("#sortable").sortable();
                $("#sortable").disableSelection();
            });

            $(function () {
                $("#sortable_topic").sortable();
                $("#sortable_topic").disableSelection();
            });
        </script>

<!-- <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script> -->

        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>/new/jquery-menu-editor.min.js"></script>
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>/new/bootstrap-iconpicker/js/iconset/fontawesome5-3-1.min.js"></script>
        <script type="text/javascript" src="<?php echo AUTH_ASSETS; ?>/new/bootstrap-iconpicker/js/bootstrap-iconpicker.min.js"></script>


    </body>
</html>
