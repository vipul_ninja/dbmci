<?php
$page = (!isset($page)) ? "" : $page;

$dashboard = '';
$web_user = '';
$all_user = '';
$android = '';
$ios = '';
$instructor = '';
$location = '';
$user_query = '';
$setting = '';
$backend_user = '';
$create_backend_user = '';
$backend_user_list = '';
$permission_group = '';
$category = '';
$category_list = '';
$course = '';
$fanwall = '';
$all_fanwall = '';
$fan_wall_banner = '';
$fan_wall_abuse_post = '';
$go_live = '';
$tags = '';
$tag_add = '';
$tag_group = '';
$email_template = '';
$email = '';
$sms = '';
$bulk_email = '';
$notification = '';
if ($page == 'dashboard') {
    $dashboard = 'active';
} elseif ($page == 'all') {
    $all_user = 'active';
    $web_user = 'active';
} elseif ($page == 'android') {
    $android = 'active';
    $web_user = 'active';
} elseif ($page == 'ios') {
    $ios = 'active';
    $web_user = 'active';
} elseif ($page == 'instructor') {
    $instructor = 'active';
    $web_user = 'active';
} elseif ($page == 'location') {
    $location = 'active';
    $web_user = 'active';
} elseif ($page == 'user_query') {
    $user_query = 'active';
} elseif ($page == 'create_backend_user') {
    $create_backend_user = 'active';
    $setting = 'active';
    $backend_user = 'active';
} elseif ($page == 'backend_user_list') {
    $backend_user_list = 'active';
    $setting = 'active';
    $backend_user = 'active';
} elseif ($page == 'permission_group') {
    $permission_group = 'active';
    $setting = 'active';
} elseif ($page == 'category_list') {
    $category_list = 'active';
    $category = 'active';
} elseif ($page == 'course') {
    $course = 'active';
    $category = 'active';
} elseif ($page == 'all_fanwall') {
    $all_fanwall = 'active';
    $fanwall = 'active';
} elseif ($page == 'fan_wall_banner') {
    $fan_wall_banner = 'active';
} elseif ($page == 'go_live') {
    $go_live = 'active';
} elseif ($page == 'fan_wall_abuse_post') {
    $fan_wall_abuse_post = 'active';
    $fanwall = 'active';
} elseif ($page == 'tag_add') {
    $tag_add = 'active';
    $tags = 'active';
} elseif ($page == 'tag_group') {
    $tag_group = 'active';
    $tags = 'active';
} elseif ($page == 'send_email') {
    $email = 'active';
    $email_template = 'active';
} elseif ($page == 'send_sms') {
    $sms = 'active';
    $email_template = 'active';
} elseif ($page == 'bulk_email') {
    $bulk_email = 'active';
    $email_template = 'active';
} elseif ($page == 'push_notification') {
    $notification = 'active';
}


$sidebar_url = array('web_user/all_user_list');
$sidebar_url[] = 'admin/index';
$sidebar_url[] = 'web_user/all_user_list';
$sidebar_url[] = 'admin/create_backend_user';
$sidebar_url[] = 'admin/backend_user_list';
$sidebar_url[] = 'category/index';
$sidebar_url[] = 'course/index';
$sidebar_url[] = 'fan_wall/all_post';
$sidebar_url[] = 'add_tag/index';
$sidebar_url[] = 'fan_wall/fan_wall_abuse_post';
$sidebar_url[] = 'fan_wall/fan_wall_banner';
$sidebar_url[] = 'live_manager/live';
$sidebar_url[] = 'mailer/send_email';
$sidebar_url[] = 'bulk_message/send_bulk_message';
$sidebar_url[] = 'bulk_email/send_bulk_email';
$sidebar_url[] = 'user_query/index';
$sidebar_url[] = 'Version/versioning';

if (is_array($sidebar_url)) {
    $sidebar_url = implode("','", $sidebar_url);
    $sidebar_url = "'" . $sidebar_url . "'";
}
$session_userdata = $this->session->userdata();

$user_permsn = $this->db->query("SELECT pg.permission_fk_id FROM backend_user_role_permissions as burps left join permission_group as pg on pg.id = burps.permission_group_id where burps.user_id = '" . $session_userdata['active_backend_user_id'] . "' ")->row_array();
//print_r($user_permsn);die;

$user_permsn = ($user_permsn['permission_fk_id']) ? $user_permsn['permission_fk_id'] : '0';

$query = $this->db->query("SELECT * from backend_user_permission where id IN ($user_permsn)")->result_array();
$result_side_bar = $query;


//$query = $this->db->query(("SELECT bup.*, (SELECT permission_id FROM `backend_user_role_permission` WHERE user_id = '".$session_userdata['active_backend_user_id']."' AND permission_id = bup.id ) as user_permission FROM `backend_user_permission` as bup WHERE bup.permission_perm in ($sidebar_url) "));
//$result_side_bar = $query->result_array();

$temp = array();
foreach ($result_side_bar as $value) {

    $temp[$value['permission_perm']] = $value['id'];
    /* if($value['id'] == $value['user_permission']) {
      echo "<pre>";print_r($value);
      } */
}
//echo "<pre>";print_r($temp);die;
?>
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">


            <!-- ################################ DASHBOARD MENU  #################################################-->

            <?php if (array_key_exists("admin/index", $temp) && $temp['admin/index'] != '') { ?>
                <li>
                    <a class="<?php echo $dashboard; ?>" href="<?php echo AUTH_PANEL_URL . 'admin/index'; ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            <?php } ?>

            <!-- ################################ MOBILE USER MENU  #################################################-->

            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:;" class="<?php
                echo $web_user;
                ;
                ?>">
                    <i class="fa fa-mobile"></i>
                    <span>Mobile app user</span>
                    <span class="dcjq-icon"></span></a>

                <ul class="sub" style="display: block;">

                    <li class="sub-menu">
                        <a href="javascript:;" class="<?php echo $web_user; ?>">

                            <span>View User</span>
                            <span class="dcjq-icon"></span>
                        </a>
                        <ul class="sub" style="display: block;">
                            <?php
                            if ((array_key_exists("web_user/all_user_list", $temp) && $temp['web_user/all_user_list'] != '')) {
                                ?>
                                <li class="<?php echo $all_user; ?>"><a href="<?php echo AUTH_PANEL_URL . 'web_user/all_user_list'; ?>"><i class="fa fa-users"></i>All</a></li>
                                <?php //if(array_key_exists("admin/create_backend_user",$temp) && $temp['admin/create_backend_user'] != '') {    ?>
                                <li class="<?php echo $android; ?>"><a href="<?php echo AUTH_PANEL_URL . 'web_user/all_user_list?user=android'; ?>"><i class="fa fa-android"></i>Android</a></li>
                                <?php //}  if(array_key_exists("admin/backend_user_list",$temp) && $temp['admin/backend_user_list'] != '') {    ?>
                                <li class="<?php echo $ios; ?>"><a href="<?php echo AUTH_PANEL_URL . 'web_user/all_user_list?user=ios'; ?>"><i class="fa fa-apple"></i>Ios</a></li>
                                <li class="<?php echo $instructor; ?> hide "><a href="<?php echo AUTH_PANEL_URL . 'instructor_user_details/all_instructors_list?user=instructor'; ?>"><i class="fa fa-user"></i>Instructor</a></li>
                                <li class="<?php echo $instructor; ?>"><a href="<?php echo AUTH_PANEL_URL . 'web_user/all_user_list?user=expert'; ?>"><i class="fa fa-user"></i>Expert</a></li>

                                <?php //}    ?>

                            <?php } ?>
                            <li class="<?php echo $location; ?> hide"><a href="<?php echo AUTH_PANEL_URL . 'web_user/all_user_location'; ?>"><i class="fa fa-map-marker"></i>Location</a></li>

                        </ul>
                    </li>
                </ul>
            </li>


            <!-- ################################ FEEDS MENU  ####################################################-->

            <?php
            if ((array_key_exists("fan_wall/all_post", $temp) && $temp['fan_wall/all_post'] != '') || (array_key_exists("add_tag/index", $temp) && $temp['add_tag/index'] != '') || (array_key_exists("fan_wall/fan_wall_abuse_post", $temp) && $temp['fan_wall/fan_wall_abuse_post'] != '')
            ) {
                ?>
                <li class="sub-menu">
                    <a class="<?php echo $fanwall; ?>" href="javascript:;">
                        <i class="fa fa-list"></i>
                        <span>Feeds </span>
                    </a>
                    <ul class="sub">
                        <li class="" ><a href="<?php echo AUTH_PANEL_URL . 'daily_dose/current_affair/add_currentaffairs'; ?>">Add Feeds</a></li>
                        <?php if (array_key_exists("fan_wall/all_post", $temp) && $temp['fan_wall/all_post'] != '') { ?>
                            <li class="<?php echo $all_fanwall; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'fan_wall/all_post'; ?>">All Feeds</a></li>
                        <?php } if (array_key_exists("fan_wall/fan_wall_abuse_post", $temp) && $temp['fan_wall/fan_wall_abuse_post'] != '') { ?>
                            <li class="<?php echo $fan_wall_abuse_post; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'fan_wall/fan_wall_abuse_post'; ?>">Abused Feeds</a></li>
                        <?php } ?>

<!--                        <li class=""><a href="<?php echo AUTH_PANEL_URL . 'daily_dose/current_affair/currentaffairs_list?post_type=user_post_type_current_affair'; ?>">Current  Affairs</a></li>
                        <li class=""><a href="<?php echo AUTH_PANEL_URL . 'daily_dose/current_affair/quiz_list?post_type=user_post_type_quiz'; ?>">Quiz</a></li>
                        <li class=""><a href="<?php echo AUTH_PANEL_URL . 'daily_dose/current_affair/vocab_list?post_type=user_post_type_vocab'; ?>">Vocabs</a></li>
                        <li class=""> <a href="<?php echo AUTH_PANEL_URL . 'daily_dose/current_affair/article_list?post_type=user_post_type_article'; ?>">View Article</a></li>
                        <li class=""> <a href="<?php echo AUTH_PANEL_URL . 'daily_dose/current_affair/video_list?post_type=user_post_type_video'; ?>">View Video</a></li>-->

                    </ul>
                </li>
            <?php } ?>

            <!-- ################################ COLLEGE MENU  ####################################################-->

            <li>
                    <a class="<?php echo $dashboard; ?>" href="javascript:;">
                        <i class="fa fa-dashboard"></i>
                        <span>State & College</span>
                    </a>
                    <ul class="sub">
                       
                       <li class=""><a href="<?php echo AUTH_PANEL_URL . 'Add_college/manage_states'; ?>">Add State</a></li>
                        <li class=""><a href="<?php echo AUTH_PANEL_URL . 'Add_college'; ?>">Add College</a></li>
                        

                    </ul>
                </li>

            <!-- ################################ CATEGORIES MENU  ################################################-->
            <?php
            if ((array_key_exists("category/index", $temp) && $temp['category/index'] != '') || (array_key_exists("course/index", $temp) && $temp['course/index'] != '')
            ) {
                ?>

                <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/stream/stream_management'; ?>"><i class="fa fa-list"></i><span>Categories</span></a></li>

                <!-- <li class="sub-menu">
                    <a class="<?php echo $category; ?>" href="javascript:;">
                        <i class="fa fa-list-ol "></i>
                        <span>Categories </span>
                    </a>
                    <ul class="sub">
                <li class="sub-menu dcjq-parent-li">
                <a href="<?php echo AUTH_PANEL_URL . 'category/list_master_category'; ?>"> Master Category</a></li>
                <?php if (array_key_exists("category/index", $temp) && $temp['category/index'] != '') { ?>
                                                                                                            <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'category'; ?>">Category List</a></li>
                <?php } if (array_key_exists("course/index", $temp) && $temp['course/index'] != '') { ?>
                                                                                                            <li class="<?php echo $course; ?>"><a href="<?php echo AUTH_PANEL_URL . 'course'; ?>">Course Interested List</a></li>
                <?php } ?>

                    </ul>
                </li> -->
            <?php } ?>
            <!-- ################################ TAGS MENU ##################################################-->

           <li class="sub-menu  ">
                <a class="<?php echo $tags; ?>" href="javascript:;">
                    <i class="fa fa-envelope"></i>
                    <span>Speciality</span>
                </a>
                <ul class="sub">
                    <?php if (array_key_exists("add_tag/index", $temp) && $temp['add_tag/index'] != '') { ?>
                        <li class="<?php echo $tag_add; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'add_tag'; ?>">Add Speciality</a></li>
                    <?php } ?>
                    <!-- <li class="<?php echo $tag_group; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'add_tag/make_tag_group'; ?>">Speciality group</a></li> -->
                </ul>
            </li>



            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:;" class="dcjq-parent">
                    <i class="fa fa-sitemap"></i>
                    <span>Test Management</span>
                    <span class="dcjq-icon"></span></a>
                <ul class="sub" style="display: none;">
                    <li class="sub-menu dcjq-parent-li">
                        <a href="boxed_page.html" class="dcjq-parent">Question Bank <span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li><a href="<?php echo AUTH_PANEL_URL . 'question_bank/question_bank/add_question'; ?>">Add Questions</a></li>
                            <!-- <li><a href="<?php echo AUTH_PANEL_URL . 'question_bank/question_bank/add_bulk_question_csv'; ?>">Add Questions(Bulk)</a></li> -->
                            <li><a href="<?php echo AUTH_PANEL_URL . 'question_bank/question_bank/view_question_list'; ?>">View List</a></li>
                            <!--<li><a href="<?php //echo AUTH_PANEL_URL . 'course_product/stream/stream_management';                      ?>">Q Stream</a></li>-->
                        </ul>
                    </li>
                    <li class="sub-menu dcjq-parent-li">
                        <a href="boxed_page.html" class="dcjq-parent">Test Series <span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li><a href="<?php echo AUTH_PANEL_URL . 'test_series/test_series'; ?>">Add/Edit Test</a></li>
                        </ul>
                    </li>
                    <!-- ########################## Quiz ################################################-->
                    <li class="sub-menu dcjq-parent-li">
                        <a href="boxed_page.html" class="dcjq-parent">Quiz<span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li><a href="<?php echo AUTH_PANEL_URL . 'quiz/Quiz'; ?>">Add/Edit Quiz</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <!-- ########################## Daily Dose  ################################################-->
            <!-- <li class="sub-menu dcjq-parent-li">
                        <a href="javascript:;" class="dcjq-parent">
                            <i class="fa fa-sitemap"></i>
                            <span>Daily Dose </span>
                        <span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li class="sub-menu dcjq-parent-li">
                                <a href="boxed_page.html" class="dcjq-parent">Current Affairs<span class="dcjq-icon"></span></a>
                                <ul class="sub" style="display: none;">
                                    <li class=""><a href="<?php //echo AUTH_PANEL_URL . 'daily_dose/current_affair/add_currentaffairs';                      ?>">Add Current Affairs</a></li>
                                <li class=""><a href="<?php //echo AUTH_PANEL_URL . 'daily_dose/current_affair/currentaffairs_list?post_type=user_post_type_current_affair';                      ?>">View Current  Affairs</a></li> </ul>
                            </li>
                            <li class="sub-menu dcjq-parent-li">
                                <a href="boxed_page.html" class="dcjq-parent">Quiz<span class="dcjq-icon"></span></a>
                                <ul class="sub" style="display: none;">
                                <li class="" ><a href="<?php //echo AUTH_PANEL_URL . 'daily_dose/current_affair/add_quiz';                      ?>">Add Quiz</a></li>
                        <li class=""><a href="<?php //echo AUTH_PANEL_URL . 'daily_dose/current_affair/quiz_list?post_type=user_post_type_quiz';                      ?>">View Quiz</a></li>
             </ul>
                            </li>
            
                            <li class="sub-menu dcjq-parent-li">
                                <a href="boxed_page.html" class="dcjq-parent">Vocabulary<span class="dcjq-icon"></span></a>
                                <ul class="sub" style="display: none;">
                                     <li class="" ><a href="<?php //echo AUTH_PANEL_URL . 'daily_dose/current_affair/add_vocab';                      ?>">Add Vocabulary</a></li>
                               <li class=""><a href="<?php //echo AUTH_PANEL_URL . 'daily_dose/current_affair/vocab_list?post_type=user_post_type_vocab';                      ?>">View Vocabs</a></li>
                                </ul>
                            </li>
            
                            <li class="sub-menu dcjq-parent-li">
                                <a href="boxed_page.html" class="dcjq-parent"> Article <span class="dcjq-icon"></span></a>
                                <ul class="sub" style="display: none;">
                                   <li class=""> <a href="<?php //echo AUTH_PANEL_URL . 'daily_dose/current_affair/add_article';                      ?>">Add Article</a>
            </li>
                                 <li class=""> <a href="<?php // echo AUTH_PANEL_URL . 'daily_dose/current_affair/article_list?post_type=user_post_type_article';                     ?>">View Article</a></li>
                                </ul>
                            </li>
            
            
                        </ul>
                    </li>
             ########################## Course managemnet ################################################-->
            <li class="sub-menu dcjq-parent-li">
                <a href="javascript:;" class="dcjq-parent">
                    <i class="fa fa-sitemap"></i>
                    <span>Course Management </span>
                    <span class="dcjq-icon"></span></a>
                <ul class="sub" style="display: none;">
                    <li class="sub-menu dcjq-parent-li">
                        <a href="boxed_page.html" class="dcjq-parent">Subject & Topics<span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li><a href="<?php echo AUTH_PANEL_URL . "course_product/Subject_topics/subject_management"; ?>">Subject</a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . "course_product/Subject_topics/topics_management"; ?>">Topic </a></li>
                        </ul>
                    </li>
                    <li class="sub-menu dcjq-parent-li">
                        <a href="boxed_page.html" class="dcjq-parent">Course<span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li><a href="<?php echo AUTH_PANEL_URL . "course_product/Course/add_course_product"; ?>">Add Course</a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . "course_product/Course/all_Courses_list"; ?>">View Course</a></li>
                            <li ><a href="<?php echo AUTH_PANEL_URL . "course_product/Course_category/add_course_stream"; ?>">Course Category</a></li>
                            <li ><a href="<?php echo AUTH_PANEL_URL . "course_product/Course_category/add_course_group"; ?>">Course Group Type</a></li>
                        </ul>
                    </li>

                    <li class="sub-menu dcjq-parent-li">
                        <a href="boxed_page.html" class="dcjq-parent">Coupon<span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li><a href="<?php echo AUTH_PANEL_URL . "coupon_master/coupon/add_coupon"; ?>">Add/View Coupon List</a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . "course_product/Coupon_course_relation/course_coupon/"; ?>">Course <> coupon relation</a></li>
                        </ul>
                    </li>

                    <li class="sub-menu dcjq-parent-li">
                        <a href="boxed_page.html" class="dcjq-parent"> File Library <span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li class="hide" ><a href="<?php echo AUTH_PANEL_URL . 'library/library/index'; ?>">Add/View pdf </a></li>
                            <li class="hide"><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_ppt'; ?>">Add/View ppt </a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_video'; ?>">Add/View video </a></li>
                            <li class="hide" ><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_epub'; ?>">Add/View epub </a></li>
                            <li class="hide" ><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_doc'; ?>">Add/View doc </a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_image'; ?>">Add/View image </a></li>
                            <li class="hide"><a href="<?php echo AUTH_PANEL_URL . 'library/library/add_concept'; ?>">Add/View concept </a></li>
                        </ul>
                    </li>


                </ul>
            </li>

            <!-- //---------------------------------------------------------------------------------////////////////////// -->
            <li class="sub-menu dcjq-parent-li">
                <a href="#" class="dcjq-parent"><i class="fa fa-file"></i> Reports <span class="dcjq-icon"></span></a>
                <ul class="sub" style="display: none;">
                    <li class=" sub-menu dcjq-parent-li">
                        <a href="#" class="dcjq-parent"> Account Section <span class="dcjq-icon"></span></a>
                        <ul class="sub" style="display: none;">
                            <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions'; ?>">All transactions</a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=pending'; ?>">Pending</a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=complete'; ?>">Complete</a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=cancel'; ?>">Cancel</a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=refund_req'; ?>">Refund Req.</a></li>
                            <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=refunded'; ?>">Refunded</a></li>
                        </ul>
                    </li>

                    <li class="hide"><a href="<?php echo AUTH_PANEL_URL . 'course_product/instructor_transactions/index'; ?>">Instructor transaction</a></li>
                    <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_wise_transactions'; ?>">Course transaction</a></li>

                    <li  class="hide" ><a href="<?php echo AUTH_PANEL_URL . "course_product/course/all_courses_reviews_list"; ?>">Course Reviews Reports</a></li>
                    <li  class="hide" ><a title="Instructor reviews Report" href="<?php echo AUTH_PANEL_URL . "course_product/course/all_instructor_reviews_list"; ?>">Inst. Reviews Reports</a></li>
                    <li  class="hide" ><a title="Instructor reviews Report" href="<?php echo AUTH_PANEL_URL . "course_product/course/all_test_given_report"; ?>">Test Taken Reports</a></li>
                    <li><a title="Points earn Report" href="<?php echo AUTH_PANEL_URL . "points_earns/report"; ?>">User points report</a></li>
                </ul>
            </li>

            <!-- ################################ SETTINGS MENU  ####################################################-->

            <?php
            if ((array_key_exists("admin/create_backend_user", $temp) && $temp['admin/create_backend_user'] != '') || (array_key_exists("admin/backend_user_list", $temp) && $temp['admin/backend_user_list'] != '')
            ) {
                ?>

                <li class="sub-menu dcjq-parent-li">
                    <a href="javascript:;" class="dcjq-parent <?php echo $setting; ?>">
                        <i class="fa fa-cogs"></i>
                        <span>Settings</span>
                        <span class="dcjq-icon"></span></a>

                    <ul class="sub" style="display: block;">
                        <?php
                        if ((array_key_exists("admin/create_backend_user", $temp) && $temp['admin/create_backend_user'] != '') || (array_key_exists("admin/backend_user_list", $temp) && $temp['admin/backend_user_list'] != '')
                        ) {
                            ?>
                            <li class="sub-menu dcjq-parent-li">
                                <a href="javascript:;" class="dcjq-parent <?php echo $backend_user; ?>">
                                    <i class="fa fa-users"></i>
                                    <span>Backend Users</span>
                                    <span class="dcjq-icon"></span>
                                </a>
                                <ul class="sub" style="display: block;">
                                    <?php if (array_key_exists("admin/create_backend_user", $temp) && $temp['admin/create_backend_user'] != '') { ?>
                                        <li class="<?php echo $create_backend_user; ?>"><a href="<?php echo AUTH_PANEL_URL . 'admin/create_backend_user'; ?>">Add New</a></li>
                                    <?php } if (array_key_exists("admin/backend_user_list", $temp) && $temp['admin/backend_user_list'] != '') { ?>
                                        <li class="<?php echo $backend_user_list; ?>"><a href="<?php echo AUTH_PANEL_URL . 'admin/backend_user_list'; ?>">View List</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>

                        <li class="<?php echo $permission_group; ?>"><a href="<?php echo AUTH_PANEL_URL . 'admin/make_permission_group'; ?>">Role management</a></li>
                        <li class="<?php echo $permission_group; ?>"><a href="<?php echo AUTH_PANEL_URL . 'refer_code/refer_code'; ?>">Points</a></li>
                        
                    </ul>
                </li>

            <?php } ?>


            <!-- ######################## PUSH NOTIFICATIONS (TEMPORARY HIDDEN)#####################################-->
                        <li ><a class="<?php echo $notification; ?>" href="<?php echo AUTH_PANEL_URL . 'bulk_messenger/push_notification/send_push_notification'; ?>"><i class="fa fa-bell"></i>
                                <span>Push Notification</span></a></li>

            <!-- ################################ HELP DESK MENU  ####################################################-->
            <?php if (array_key_exists("user_query/index", $temp) && $temp['user_query/index'] != '') { ?>
                <li ><a class="<?php echo $user_query; ?>" href="<?php echo AUTH_PANEL_URL . 'user_query'; ?>"><i class="fa fa-dashboard"></i>
                        <span>Help Desk</span></a></li>
            <?php } ?>

            <!-- ########################## VERSION CONTROL MENU  ################################################-->
            <?php if (array_key_exists("Version/versioning", $temp) && $temp['Version/versioning'] != '') { ?>
                <li>
                    <a class="<?php //echo $dashboard; ?>" href="<?php echo AUTH_PANEL_URL . 'version_control/Version/versioning'; ?>">
                        <i class="fa fa-code"></i>
                        <span>Version control</span>
                    </a>
                </li>
            <?php } ?>

            <!-- ########################## Video managemnet ################################################-->
            <?php
            if ((array_key_exists("video_control/add_video", $temp) && $temp['video_control/add_video'] != '') || (array_key_exists("video_control/edit_video", $temp) && $temp['video_control/edit_video'] != '') || (array_key_exists("video_control/video_analytics", $temp) && $temp['video_control/video_analytics'] != '')
            ) {
                ?>
<!--                <li class="sub-menu">
                    <a class="" href="javascript:;">
                        <i class="fa fa-video-camera"></i>
                        <span> Video Channel</span>
                    </a>
                    <ul class="sub">
                        <?php if (array_key_exists("video_control/add_video", $temp) && $temp['video_control/add_video'] != '') { ?>
                            <li class="" ><a href="<?php echo AUTH_PANEL_URL . 'video_channel/video_control/add_video'; ?>">Add Videos</a></li>
                        <?php } if (array_key_exists("video_control/edit_video", $temp) && $temp['video_control/edit_video'] != '') { ?>
                            <li class="" ><a href="<?php echo AUTH_PANEL_URL . 'video_channel/video_control/video_list'; ?>">All Videos</a></li>
                        <?php } if (array_key_exists("video_control/video_analytics", $temp) && $temp['video_control/video_analytics'] != '') { ?>
                            <li class="" ><a href="<?php echo AUTH_PANEL_URL . 'video_channel/video_control/video_analytics'; ?>">Videos Analytics</a></li>
                        <?php } ?>
                        <li class="hide " ><a href="<?php echo AUTH_PANEL_URL . 'video_channel/video_control/video_search_list'; ?>">Video Searching Tags</a></li>
                    </ul>
                </li>-->
            <?php } ?>

            <li class="sub-menu">
                <a class="" href="<?php echo AUTH_PANEL_URL . 'user_loger/index'; ?>">
                    <i class="fa fa-file-text"></i>
                    <span> Backend Activity Log</span>
                </a>
            </li>



        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
