<?php //echo "<pre>";print_r($group_detail);die; ?>
<div class="row">
<div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Edit tags group <a href="<?php echo AUTH_PANEL_URL.'add_tag/make_tag_group'; ?>" ><button class="btn btn-success pull-right btn-sm">Back</button></a>
                          </header>
                          <div class="panel-body">
                              <form role="form" method="post" action="">
                                  <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">Tag group Name</label>
                                      <input type="text" placeholder="Enter group name" name="tag_group_name" value="<?php echo $group_detail['tag_group_name']; ?>" id="exampleInputEmail1" class="form-control input-sm ">
                                      <span style="color:red"><?php echo form_error('tag_group_name'); ?></span>
                                  </div>
                                  </div>
                                  <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Tag Category</label>
                                    <select class="form-control input-sm" id="tag_category" name="tag_category">
                                        <?php foreach ($tag_categories as  $value) {
                                              if($value['id'] ==  $group_detail['master_id']) {
                                          echo '<option selected value="'.$value['id'].'">'.$value['text'].'</option>';
                                          } else {
                                            echo '<option  value="'.$value['id'].'">'.$value['text'].'</option>';
                                          }
                                        } ?>
                                    </select>
                                  </div>
                                  </div>
                                
                                <div class="col-md-12 ">
                                  <div class="form-group checkbox_list">
                                      
                                  </div>
                                  <span style="color:red"><?php echo form_error('tags_id[]'); ?></span>
                                </div><br><br>

                                <div class="col-md-12 "> 
                                    <button class="btn btn-info pull-right btn-sm" type="submit">Submit</button>
                                </div>
                                
                              </form>
                          </div>
                        </section>
                        </div>
</div>    




<?php
$adminurl = AUTH_PANEL_URL;
$checked_id = $group_detail['tag_fk_id'];
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'backend-user-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "pageLength": 50,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"add_tag/ajax_get_tag_group_list", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );


                      jQuery("select").bind("change", function(){
                        var cat_id = $('#tag_category').val();
                        var checked_id = "$checked_id";
                        jQuery.ajax({
                            url: "$adminurl"+"add_tag/ajax_get_subcategory_by_id_and_checked",
                            method: 'POST',
                            dataType: 'json',
                            data: {
                               "cat_id":  cat_id,
                               "checked_id" : checked_id
                            },
                            success: function (data) {
                                console.log(data);
                                if(data.status) {
                                    $('.checkbox_list').html(data.data);
                                 }
                            }
                        });
                      }).trigger("change");
                                            
                   } );
               </script>

EOD;

    echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
