
    <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Add Speciality 
                          </header>
                          <div class="panel-body">
                              <form role="form" class="form-inline"  method="post">
                                  <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="exampleInputEmail2" >Main Category</label>
                                      <select class="form-control input-sm" name="category_id">
                                        <option disabled="disabled" selected="selected" >Select Category</option>
                                        <?php foreach ($main_category as $key => $value) {
                                        echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                                        } ?>
                                      </select>
                                      <span style="color:red"><?php  echo form_error('category_id');  ?></span>
                                  </div>
                                  </div>
                                  <div class="col-md-3">
                                  <div class="form-group">
                                      <label for="exampleInputPassword2">Speciality</label>
                                      <input type="text" placeholder="Write tag" name="tag"  class=" input-sm form-control">
                                      <span style="color:red"><?php  echo form_error('tag');  ?></span>
                                  </div>
                                  </div>
                                  <div class="col-md-3">
                                  <div class="form-group">
                                      <label for="exampleInputPassword2">Status</label>
                                      <select class="form-control input-sm" name="status">
                                        <option disabled="disabled" >Select Status</option>
                                        <option value="1" selected="selected">Enable</option>
                                        <option value="0">Disable</option>
                                      </select>
                                  </div>
                                  </div>
                                  <button class="btn btn-success btn-sm" type="submit">Submit</button>
                              </form>

                          </div>
                      </section>

                  </div>

<!-- all post show list -->

<div class="col-sm-12">
  <section class="panel">
    <header class="panel-heading">
    ALL TAG(s) LIST
    <span class="tools pull-right">
    <a href="javascript:;" class="fa fa-chevron-down"></a>
    <!--<a href="javascript:;" class="fa fa-times"></a>-->
    </span>
    </header>
    <div class="panel-body">
    <div class="adv-table">
    <table  class="display table table-bordered table-striped" id="all-user-grid">
      <thead>
        <tr>

          <th>Category </th>
          <th>Speciality </th>
          <th>View</th>
          <th>Action</th>
          <th>Position</th>
        </tr>
      </thead>
      <thead>
          <tr>

              <th>
                <select class="form-control search-input-select" data-column="0">

                  <?php
                    foreach($main_category as $key=>$cat) {
                      if($cat['name'] == 'Medical') {
                        echo '<option value="'.$cat['name'].'" selected>'.$cat['name'].'</option>';
                      } else {
                        echo '<option value="'.$cat['name'].'">'.$cat['name'].'</option>';
                      }
                     ?>
                  <?php } ?>
                </select>
              </th>
              <th><input type="text" data-column="1"  class="search-input-text hide form-control"></th>
              <th></th>
              <th></th>
              <th></th>
          </tr>
      </thead>
    </table>
    </div>
    </div>
  </section>
</div>



<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-user-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "pageLength": 50,
                           "serverSide": true,
                           "order": [[ 4, "asc" ]],
                           "ajax":{
                               url :"$adminurl"+"add_tag/ajax_all_tag_list", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                       $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );

                        //change position of post tags..

                        $(document).on('click','.position',function() {
                          var value = $(this).attr('data-value');
                          var tag_id = $(this).attr('data-id');
                          var position = $(this).attr('data-position');
                          jQuery.ajax({
                            url: "$adminurl"+"add_tag/ajax_change_post_tag_position",
                            method: 'POST',
                            dataType: 'json',
                            data: {
                              value: value,
                              tag_id: tag_id,
                              position: position
                            },
                            success: function (data) {
                              console.log(data);
                              if (data.status) {
                                  jQuery("#all-user-grid").DataTable().draw();
                                 //window.location.href = "$adminurl"+"add_tag";
                              }
                            }
                          });
                        });
                  });



               </script>

EOD;

  echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
