<?php //echo "<pre>";print_r($all_tags);die; ?>
<div class="row">
<div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Make tags group
                          </header>
                          <div class="panel-body">
                              <form role="form" method="post" action="">
                                  <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">Tag group Name</label>
                                      <input type="text" placeholder="Enter group name" name="tag_group_name" id="exampleInputEmail1" class="form-control input-sm ">
                                      <span style="color:red"><?php echo form_error('tag_group_name'); ?></span>
                                  </div>
                                  </div>
                                  <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Tag Category</label>
                                    <select class="form-control input-sm" id="tag_category" name="tag_category">
                                        <?php foreach ($tag_categories as  $value) {
                                          echo '<option value="'.$value['id'].'">'.$value['text'].'</option>';
                                        } ?>
                                    </select>
                                  </div>
                                  </div>
                                
                                <div class="col-md-12 ">
                                  <div class="form-group checkbox_list">
                                      
                                  </div>
                                  <span style="color:red"><?php echo form_error('tags_id[]'); ?></span>
                                </div><br><br>

                                <div class="col-md-12 "> 

                                    <button class="btn btn-info pull-right btn-sm" type="submit">Submit</button>
                                </div>
                                
                              </form>
                          </div>
                        </section>
                        </div>
</div>    


<div class="col-sm-12">
    <section class="panel">
        <header class="panel-heading">
            GROUP TAG(s) LIST
        <span class="tools pull-right">
        <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span>
        </header>
        <div class="panel-body">
        <div class="adv-table">
        <table  class="display table table-bordered table-striped" id="backend-user-grid">
        <thead>
            <tr>
            <th># </th>
            <th>Group Name </th>
            <th>Master category </th>
            
                    <th>Action</th>
            </tr>
        </thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="form-control search-input-text"></th>
              <th><input type="text" data-column="1"  class="form-control search-input-text"></th>
              <th><select class="form-control search-input-select" data-column="2">
                   
                  <?php 
                    $i = 0;
                    foreach($tag_categories as $key=>$cat) {
                        $selected = "";
                        if($i == 0 ){
                          $selected = "selected=selected";
                        }
                        echo '<option '.$selected.' value="'.$cat['id'].'" >'.$cat['text'].'</option>';
                        $i++;
                      }  
                     ?>
                  
                </select></th>
              <th></th>
          </tr>
      </thead>
        </table>
        </div>
        </div>
    </section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'backend-user-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "pageLength": 50,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"add_tag/ajax_get_tag_group_list", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );

                       $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );


                      jQuery("#tag_category").bind("change", function(){
                        var cat_id = $('#tag_category').val();
                        jQuery.ajax({
                            url: "$adminurl"+"add_tag/ajax_get_subcategory_by_id",
                            method: 'POST',
                            dataType: 'json',
                            data: {
                               "cat_id":  cat_id
                            },
                            success: function (data) {
                                var html = '';
                                if(data.status) {
                                  if(data.data.length > 0) {
                                    html = '<div class="col-lg-12">';

                                    $.each( data.data , function( key, value ) {
                                      html += '<div class=col-md-4 checkbox><label><input name=tags_id[] type=checkbox value="'+value.id+'">&nbsp' + value.text + '</label></div>';
                                    });

                                    html += '</div>';
                                  }
                                  $('.checkbox_list').html(html);
                                }
                            }
                        });
                      }).trigger("change");
                                            
                   } );
               </script>

EOD;

    echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>




