
    <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Edit Speciality

                <a href="<?php echo AUTH_PANEL_URL."add_tag"; ?>"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
          </header>
          <div class="panel-body">
              <form role="form" class="form-inline"  method="post">
                  <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputEmail2" >Main Category</label>
                      
                      <select class="form-control input-sm" name="category_id">
                        <option disabled="disabled" selected="selected" >Select Category</option>
                        <?php foreach ($main_category as $key => $value) {
                                if($edit_data['master_id'] == $value['id']) {
                                  echo '<option selected="selected" value="'.$value['id'].'">'.$value['name'].'</option>';
                                } else {
                          echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                            } 
                        } ?>
                      </select>
                      <span style="color:red"><?php  echo form_error('category_id');  ?></span>
                  </div>
                  </div>
                  <div class="col-md-3">
                  <div class="form-group">
                      <label for="exampleInputPassword2">Speciality</label>
                      <input type="text" placeholder="Write tag" name="tag"  value="<?php echo $edit_data['text']; ?>" class="form-control input-sm ">
                      <span style="color:red"><?php  echo form_error('tag');  ?></span>
                  </div>
                  </div>
                  <div class="col-md-3">
                  <div class="form-group">
                      <label for="exampleInputPassword2">Status</label>
                      <select class="form-control input-sm" name="status">
                        <option disabled="disabled" >Select Status</option>
                        <?php if($edit_data['status'] == 1) {
                              echo '<option value="1" selected="selected">Enable</option><option value="0">Disable</option>';
                        } elseif($edit_data['status'] == 0) { 
                              echo '<option value="1" >Enable</option><option value="0" selected="selected">Disable</option>';
                        } ?>
                      </select>
                  </div>
                  </div>
                  <button class="btn btn-success btn-sm" type="submit">Submit</button>
              </form>

          </div>
      </section>
  </div>
