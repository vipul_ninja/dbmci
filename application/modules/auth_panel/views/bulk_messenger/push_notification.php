<div class="col-lg-6">
  <section class="panel">
      <header class="panel-heading">
          Push Notification to Users
      </header>
      <div class="panel-body">
          <form id="bulk_push" method="" action=""  role="form">
              <div class="form-group">
                  <label for="exampleInputEmail1">Type of user</label>
                  <select  name="user_type"class="form-control m-bot15 bulk_user_type">
                      <option value="ALL">ALL</option>
                      <option value="DAMS">DBMCI</option>
                      <option value="NON_DAMS">NON DBMCI</option>
                  </select>
              </div>
              <div class="form-group">
                  <label >Device</label>
                  <div>
                  <select name="device_type"class=" device_type form-control m-bot15 ">
                    <option value="ALL">ALL</option>
                    <option value="ANDROID">ANDROID</option>
                    <option value="IOS">IOS</option>
                </select>                 
              </div>
               <div class="form-group">
                  <label for="notification_type">Notification Type</label>
                  <select class="form-control input-sm notification_type " name = "notification_type" id="notification_type">
                    <option value="">--Select Type--</option>
                     <option value="GENERAL">General</option>
                     <option value="FEED" <?php if($this->input->get('feed_id') ){echo "selected"; }else{ echo "disabled";} ?> >Feed</option>
                     <option value="URL">URL</option>
                     <option value="VIDEO" disabled="">Video</option>
                  </select>
               </div>
               <div class="form-group" id="notification_type_text" >
                  <label for="">Extra (Paste url link for notification type URL )</label>
                  <input type="text" class="form-control notification_content" value="<?php if($this->input->get('feed_id') ){echo $this->input->get('feed_id');} ?>" accept="" name = "" id="">
               </div>
              <div class="form-group">
                    <label for="exampleInputPassword1">Type message</label>
				        	<textarea class="form-control  bulk_user_message" name="message"><?php if($this->input->get('text') ){echo base64_decode($this->input->get('text'));} ?></textarea>
                    <span style="color: red"><?php echo ($this->session->flashdata('error') && $this->session->flashdata('error') == 'raw_error') ?form_error('message'):''; ?></span>
              </div>
              <!--<input type="hidden" value="raw" name="notification_type">-->
              <button class="btn btn-info bulk_button hide" type="submit" >Submit</button>
              <div id="show_socket_state" class="bold col-md-12"> <i class="fa fa-spinner fa-spin bold " aria-hidden="true"></i>  Please Wait while we connecting you to server. </div>
          </form>

      </div>
  </section>
</div>


<!-- Custome User Send Message -->
<?php 

if($this->input->get('q')) {
      $query_data = (array)json_decode(base64_decode($this->input->get('q')));
      //echo "<pre>";print_r($query_data);die;
    
?>
<div class="col-lg-6">
  <section class="panel">
      <header class="panel-heading">
          Push Notification to custom User(s)
      </header>
      <div class="panel-body">
          <form method="POST" action="" role="form">
              <div class="form-group">
                  <label for="exampleInputEmail1">User Name</label>
                  <p for="exampleInputEmail1"><?php echo  $query_data['name'];  ?></p>
              </div>
              <div class="form-group">
                    <label for="exampleInputPassword1">Type message</label>
                    <textarea class="form-control" name="message"></textarea>
                    <span style="color: red"><?php echo ($this->session->flashdata('error') && $this->session->flashdata('error') == 'custom_error') ?  form_error('message'):''; ?></span>
              </div>
              <input type="hidden" value="<?php echo ($query_data) ? $query_data['device_type'] :'';  ?>" name="device_type">
              <input type="hidden" value="<?php echo ($query_data) ? $query_data['device_tokken'] :'';  ?>" name="device_token">
              <input type="hidden" value="custom" name="notification_type">
              <button  class="btn btn-info" type="submit">Submit</button>
          </form>

      </div>
  </section>
</div>
<?php } ?>

<?php
$adminurl = AUTH_PANEL_URL;
$socketjs = 'http://18.223.244.127/web_socket/examples/js/socket.js';
$custum_js = <<<EOD
  <script src="$socketjs"></script>
  <script type="text/javascript" language="javascript" > 
    var socket=$.websocket('ws://18.223.244.127:2000');
  
      $('#bulk_push').submit(function() { 
            var users_type = $('.bulk_user_type').val();
            var users_message = $('.bulk_user_message').val();
            var device_type = $('.device_type').val();
            var notification_type = $('.notification_type').val();
            var notification_text =  $('.notification_content').val();
        
            if(notification_type == "" ){
                alert('Please choose notification type');
                return false;
            }
            if(notification_text =="" && notification_type == "URL"){
                alert('URL is required for notification type url.');
                return false;
            }             
            if(users_message == "" ){
                alert('Please type message for users.');
                return false;
            }
           
            if(users_message){
                $('.bulk_button').hide();
                json_var = {};
                json_var.users_type =  users_type;
                json_var.users_message =  users_message;
                json_var.device_type = device_type;
                json_var.notification_type = notification_type;
                json_var.notification_text = notification_text;
                console.log(json_var);
                socket.emit('sync_process', json_var);
            }
            $('.bulk_user_message').val('');
            return false;
      });

      socket.on('sync_process', function(msg){
        $('#show_socket_state').text(msg);
        //console.log(msg);
      });
      socket.on('connect', function(user){
          $('.bulk_button').removeClass('hide');
          $('#show_socket_state').html('<i class="fa fa-check" aria-hidden="true"></i> You are connected to server.');
        console.log('web_socket connected');
      });
    
    socket.listen();
  </script>    
  <script type="text/javascript" charset="utf8">
     $('#notification_type').change(function(){
        if($(this).val() == 'GENERAL' || $(this).val() == 'FEED' || $(this).val() == "" ){
            $('#notification_type_text').hide();
        }
        if($(this).val() == 'URL'){

             $('#notification_type_text').show();
        }
        
    }).change();
    </script>          

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );

