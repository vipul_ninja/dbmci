<style>
.box.box-danger {
    border-top-color: #dd4b39;
    /*margin-top:34px;*/
    min-height: 193px;
}
.box.box-primary {
    border-top-color: #3c8dbc;
	/*margin-top:34px;*/
        min-height: 193px;
}
.box.box-warning {
    border-top-color: #f39c12;
	/*margin-top:34px;*/
        min-height: 193px;
}
.box.box-success {
    border-top-color: #00a65a;
	/*margin-top:34px;*/
        min-height: 193px;
}
.box.box-info {
    border-top-color: #00c0ef;
	/*margin-top:34px;*/
        min-height: 193px;
}
.box {
    position: relative;
    border-radius: 3px;
    background: #ffffff;
    border-top: 3px solid #d2d6de;
    margin-bottom: 20px;
    width: 100%;
}
bootstrap.min.css:5
* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.box-header {
    color: #444;
    display: block;
    padding: 4px 10px;
    position: relative;
    margin-top:15px;
}
.box-body ul{
  list-style:none;
  width:100%;
  display:inline-block;
  padding:10px 27px !important;
}
.box-body h5{
    margin-left: 24px;
}
/* .box-body ul li{
  width:50%;
  float:left;
} */
.icons_position{
  position:absolute;
  margin: -14px 22px;
  width: 42px;
  overflow: hidden;
  height: 42px;
  background:#fff;
  xborder-radius:100px;
}
.icons_position img{
  padding: 2px 5px;
}
.box-body ul.widthfull li{
  width: 100%;  
}
</style>

<div class="container" style="width:100%;">
  <div class="row">
    <div class="col-md-6">
        <div class="box box-warning">
    	 <div class="icons_position">
    	     <img src="<?php echo base_url(); ?>auth_panel_assets/img/count-img/student.png">
         </div>		 
                    <div class="box-header">
                      <h4 class="box-title bold">Student Section</h4>
                    </div>
                    <div class="box-body">
                      <div class="row">
                        <ul>
                          <li><span class="col-md-10 bold">Total Students</span><span class="col-md-2"><?php echo $student_count_details['total_students']; ?></span></li>
                          <li class="hide"><span class="col-md-10 bold">Total DBMCI Students</span><span class="col-md-2"><?php echo $student_count_details['dams_students']; ?></span></li>
                          <li class="hide"><span class="col-md-10 bold">Total Non-DBMCI Students</span><span class="col-md-2"><?php echo $student_count_details['non_dams_students']; ?></span></li>                    
                         </ul>
                      </div>
                    </div><!-- /.box-body -->
          </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
    	   <div class="icons_position">
    	     <img src="<?php echo base_url(); ?>auth_panel_assets/img/count-img/rss.png">
         </div>
                    <div class="box-header">
                      <h4 class="box-title bold">Feeds Section</h4>
                    </div>
                    <div class="box-body">
                      <div class="row">
                        <ul>
    					            <li class="hide" ><span class="col-md-10 bold">Total Feeds By Dams</span><span class="col-md-2"><?php echo $feed_count_details['total_feeds_dams']; ?></span></li>
                          <li class="hide"><span class="col-md-10 bold">Total Feeds By Non-Dams</span><span class="col-md-2"><?php echo $feed_count_details['total_feeds_non_dams']; ?></span></li>
                          <li><span class="col-md-10 bold">Total Report Abuse</span><span class="col-md-2"><?php echo $feed_count_details['total_report_abuse']; ?></span></li>  
                          <li><span class="col-md-10 bold">Total Likes</span><span class="col-md-2"><?php echo $feed_count_details['total_likes']; ?></span></li>  
                          <li><span class="col-md-10 bold">Total Comments</span><span class="col-md-2"><?php echo $feed_count_details['total_comments']; ?></span></li>  
                        </ul>
                      </div>
                    </div><!-- /.box-body -->
             </div>
    </div>
    <div class="col-md-4 hide">
        <div class="box box-danger">
    	   <div class="icons_position">
    	     <img src="<?php echo base_url(); ?>auth_panel_assets/img/count-img/video-camera.png">
           </div>
                    <div class="box-header">
                      <h4 class="box-title bold ">Video Section</h4>
                    </div>
                    <div class="box-body">
                      <div class="row">
                        <ul>
    						<li><span class="col-md-10 bold">Total Video</span><span class="col-md-2"><?php echo $video_count_details['total_videos']; ?></span></li>
    						<li><span class="col-md-10 bold">Total Videos Views</span><span class="col-md-2"><?php echo $video_count_details['videos_view_count']; ?></span></li>
    						<li class="hide"><span class="col-md-10 bold">Total Non Dams Videos</span><span class="col-md-2"><?php echo $video_count_details['non_dams_videos']; ?></span></li>
    						<li class="hide"><span class="col-md-10 bold">Total Dams Videos</span><span class="col-md-2"><?php echo $video_count_details['dams_videos']; ?></span></li>
                        </ul>
                      </div>
                    </div><!-- /.box-body -->
             </div>
    </div>
  </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="col-lg-8">
                              <!--work progress start-->
            <section class="panel">
                <div class="panel-body progress-panel">
                    <div class="task-progress">
                        <h1>Course Purchase</h1>
                        <p>Last 5 purchase</p>
                    </div>
                    <div class="task-option">
                       <a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions'; ?>"><button class="btn btn-success">See all </button></a>
                    </div>
                </div>
                <?php
                   $query= "SELECT ctr.* , u.name as user_name , bu.username as instructor_name ,cm.title as course_name,DATE_FORMAT(FROM_UNIXTIME(SUBSTR(ctr.creation_time,1,10)), '%d-%m-%Y %h:%i:%s') as creation_time, ctr.pay_via FROM course_transaction_record as ctr left join users as u on u.id = ctr.user_id left join backend_user as bu on bu.id = ctr.instructor_id left join course_master as cm on cm.id = ctr.course_id WHERE 1=1 order by ctr.id desc limit 0 ,5";
                   $last_five_records = $this->db->query($query)->result(); 

                ?>
                <div class="table-responsive">
                  <table class="table table-hover personal-task">
                    <tbody>
                     <?php
                        $i = 1;
                        foreach($last_five_records as $lfr){
                          ?>
                              <tr>
                                  <td><?php echo $i ; ?></td>
                                  <td><?php echo $lfr->user_name; ?></td>
                                  <td><?php echo $lfr->course_name; ?></td>
                                  <td>
                                      <?php echo ($lfr->transaction_status == 1 )?'complete':' -- '; ?>
                                  </td>
                                  <td><?php echo $lfr->creation_time; ?></td>
                              </tr>

                          <?php
                          $i++;
                        }
                     ?>
                    </tbody>
                  </table>

                </div>
                
            </section>
                              <!--work progress end-->
        </div>
        <div class="col-md-4">
          <div class="box box-success">
           <div class="icons_position">
             <img src="<?php echo base_url(); ?>auth_panel_assets/img/count-img/language.png">
           </div>
                      <div class="box-header">
                        <h4 class="box-title bold ">Top 5 Purchased Courses</h4>
                      </div>
                      <div class="box-body">
                        <div class="row">
                          <ul class="widthfull">
                  <?php foreach($top_5_purchased_courses_details as $child_top_5_purchased_courses_details){  ?>
                                <li class="bold"><?php echo $child_top_5_purchased_courses_details['course_name']; ?> &nbsp;&nbsp;&nbsp; 
                                    <i class="fa fa-share-square-o" aria-hidden="true"></i> Price - <?php echo $child_top_5_purchased_courses_details['course_price']; ?> &nbsp;&nbsp; 
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i> Rating - <?php echo $child_top_5_purchased_courses_details['course_rating']; ?> &nbsp;&nbsp;&nbsp;
                                </li>
                  <?php } ?>
                  
                          </ul>
                        </div>
                      </div><!-- /.box-body -->
               </div>
      </div>
      <script>
      //  function sms(){
      //    $.ajax({
      //      url:'<?=base_url('index.php/auth_panel/Admin/send_sms_message')?>',
      //      method:'POST',
      //      data:{
      //        'c_code':'+91',
      //        'mobile':'7781913376',
      //        'message':'test message'
      //      },
      //      success:function(){

      //      }
      //    });
      //  }
      </script>
      </div>  
      <div class="col-lg-12">
        <div class="col-lg-8">
                              <!--work progress start-->
            <section class="panel">
                <div class="panel-body progress-panel">
                    <div class="task-progress">
                        <h1>Course Reviews</h1>
                        <p>Last 5 Reviews</p>
                    </div>
                    <div class="task-option">
                       <a href="<?php echo AUTH_PANEL_URL."course_product/course/all_courses_reviews_list"; ?>"><button class="btn btn-success">See all </button></a>
                    </div>
                </div>
                <?php
                   $query= "SELECT cur.id ,cm.title, cur.course_fk_id, cur.user_id , cur.rating ,cur.text, DATE_FORMAT(FROM_UNIXTIME(cur.creation_time/1000), '%d-%m-%Y %h:%i:%s') as ctime , u.name as name , u.profile_picture
                                          FROM course_user_rating AS cur
                                          Left JOIN users AS u ON u.id = cur.user_id
                        JOIN course_master cm ON cm.id=cur.course_fk_id  order by cur.id desc limit 0, 5 ";
                   $last_five_records = $this->db->query($query)->result(); 

                ?>
                <div class="table-responsive">
                <table class="table table-hover personal-task">
                    <tbody>
                     <?php
                        $i = 1;
                        foreach($last_five_records as $lfr){
                          ?>
                              <tr>
                                  <td><?php echo $i ; ?></td>
                                  <td><?php echo $lfr->name; ?></td>
                                  <td><?php echo $lfr->title; ?></td>
                                  <td>
                                      <?php echo $lfr->text; ?>
                                  </td>
                                  <td>
                                      <?php echo $lfr->rating; ?>
                                  </td>
                                  <td><?php echo $lfr->ctime; ?></td>
                              </tr>

                          <?php
                          $i++;
                        }
                     ?>
                    </tbody>
                </table>
                </div>
            </section>
                              <!--work progress end-->
        </div>
        <div class="col-md-4">
            <div class="box box-success">
             <div class="icons_position">
               <img src="<?php echo base_url(); ?>auth_panel_assets/img/count-img/Course.png">
             </div>
                        <div class="box-header">
                          <h4 class="box-title bold ">Course Section</h4>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <ul>
                    <li><span class="col-md-10 bold">Total Course Published</span><span class="col-md-2"><?php echo $course_count_details['total_published']; ?></span></li>
                    <li><span class="col-md-10 bold">Total Course Purchased</span><span class="col-md-2"><?php echo $course_count_details['total_purchased']; ?></span></li>
                    <li><span class="col-md-10 bold">Total Free Course</span><span class="col-md-2"><?php echo $course_count_details['total_paid_course']; ?></span></li>
                    <li><span class="col-md-10 bold">Total Paid Course</span><span class="col-md-2"><?php echo $course_count_details['total_free_course']; ?></span></li>
                            </ul>
                          </div>
                        </div><!-- /.box-body -->
                 </div>
        </div>
      </div>

    </div>
  <div class="row">

    <div class="col-md-4 hide">
        <div class="box box-success">
         <div class="icons_position">
           <img src="<?php echo base_url(); ?>auth_panel_assets/img/count-img/faculty.PNG">
         </div>
                    <div class="box-header">
                      <h4 class="box-title bold ">Faculty Section</h4>
                    </div>
                    <div class="box-body">
                      <div class="row">
                        <ul>
                <li><span class="col-md-10 bold">Total Faculty/Expert</span><span class="col-md-2"><?php echo $faculty_count_details['total_faculty']; ?></span></li>
                <li><span class="col-md-10 bold">Total Dams Faculty</span><span class="col-md-2"><?php echo $faculty_count_details['total_dams_faculty']; ?></span></li>
                <li><span class="col-md-10 bold">Total Non-Dams Faculty</span><span class="col-md-2"><?php echo $faculty_count_details['total_non_dams_faculty']; ?></span></li>                                      
                        </ul>
                      </div>
                    </div><!-- /.box-body -->
             </div>
    </div>
    <div class="col-md-4 hide">
        <div class="box box-success">
         <div class="icons_position">
           <img src="<?php echo base_url(); ?>auth_panel_assets/img/count-img/Instructor.PNG">
         </div>
                    <div class="box-header">
                      <h4 class="box-title bold ">Instructor Section</h4>
                    </div>
                    <div class="box-body">
                      <div class="row">
                        <ul>
                <li><span class="col-md-10 bold">Total Instructor</span><span class="col-md-2"><?php echo $instructor_count_details['total_instructor']; ?></span></li> 
                <li><span class="col-md-10 bold">Total Dams Instructor</span><span class="col-md-2"><?php echo $instructor_count_details['total_dams_instructor']; ?></span></li> 
                <li><span class="col-md-10 bold">Total Non-Dams Instructor</span><span class="col-md-2"><?php echo $instructor_count_details['total_non_dams_instructor']; ?></span></li>                                        
              </ul>
                      </div>
                    </div><!-- /.box-body -->
             </div>
    </div>
      <div class="col-md-4">
          <div class="box box-success">
           <div class="icons_position">
             <img src="<?php echo base_url(); ?>auth_panel_assets/img/count-img/language.png">
           </div>
                      <div class="box-header">
                        <h4 class="box-title bold ">Ads Section</h4>
                      </div>
                      <div class="box-body">
                        <div class="row">
                            <h5>Over all click (Till Now)</h5>
                          <ul class="widthfull">
                  <?php foreach($adv_count_details as $child_adv_count_details){ ?>
                                <li class="bold" ><?php echo $child_adv_count_details['banner_title']; ?> - <?php echo $child_adv_count_details['hit_count']; ?> &nbsp;&nbsp;&nbsp; (Active from - <?php echo $child_adv_count_details['from_date']; ?>)</li>   
                <?php } ?>
                          </ul>
                        </div>
                      </div><!-- /.box-body -->
               </div>
      </div>

      <div class="col-md-4">
          <div class="box box-success">
           <div class="icons_position">
             <img src="<?php echo base_url(); ?>auth_panel_assets/img/count-img/language.png">
           </div>
                      <div class="box-header">
                        <h4 class="box-title bold ">Top 5 Active User</h4>
                      </div>
                      <div class="box-body">
                        <div class="row">
                            <ul class="widthfull">
                    <?php foreach($top_5_user_details as $child_top_5_user_details){ ?>
                                <li class="bold" ><?php echo $child_top_5_user_details['name']; ?> 
                    <span class="pull-right">
                                     <i class="fa fa-share-square-o" aria-hidden="true" title="Post"></i> <span title="Post"><?php echo $child_top_5_user_details['total']; ?> &nbsp;&nbsp;</span> 
                                     <i class="fa fa-thumbs-o-up" aria-hidden="true" title="Likes"></i><span title="Likes"> <?php echo $child_top_5_user_details['likes']; ?> &nbsp;&nbsp;&nbsp;</span>  
                                     <i class="fa fa-rss" aria-hidden="true" title="follow"></i> <span title="follow"><?php echo $child_top_5_user_details['followers_count']; ?></span> 
                    </span>
                    </li>
                              <?php } ?>
                          </ul>
                        </div>
                      </div><!-- /.box-body -->
               </div>
      </div>      
  </div>
</div>