
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Emedicoz Registration</title>

    <!-- Bootstrap core CSS auth_panel_assets -->
    <link href="<?php echo base_url();?>auth_panel_assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>auth_panel_assets/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>auth_panel_assets/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>auth_panel_assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>auth_panel_assets/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>auth_panel_assets/js/html5shiv.js"></script>
    <script src="<?php echo base_url();?>auth_panel_assets/js/respond.min.js"></script>
    <![endif]-->
    <style>
    body {
        background: url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvr40dB95zEeLKDXz4Pea1cPo039mLl9kpc0OMmRdy8v6jca_9) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
      }
    </style>
</head>
<body class="login-body">
   <div class="container">
      <form class="form-signin" method="POST" action="<?php echo site_url('auth_panel/registration/index');?>" enctype="multipart/form-data">
         <h2 class="form-signin-heading">registration </h2>
         <div class="login-wrap ">
            <span class="success bold"><?php if(isset($status)){echo $status;}?></span>
            <div >
            <input type="text" value="<?php echo set_value('username');?>" class=" input-sm col-md-12" name="username" placeholder="Full Name" autofocus>
            <span class="error bold"><?php echo form_error('username');?></span>
          </div>
            <div >
            <input type="text" value="<?php echo set_value('email');?>"  class="input-sm col-md-12" name="email" placeholder="Email" autofocus>
            <span class="error bold"><?php echo form_error('email');?></span>
          </div>
          <div >
            <input type="password" value="<?php echo set_value('password');?>"  class="input-sm col-md-12"  name="password" placeholder="Password">
            <span class="error bold"><?php echo form_error('password');?></span>
          </div >
          <div >
            <input type="password" value="<?php echo set_value('confirm_password');?>"  class="input-sm col-md-12" name="confirm_password" placeholder="Re-type Password">
            <span class="error bold"><?php echo form_error('confirm_password');?></span>
          </div >
            <div class="form-inline">
               <select class="  input-sm col-md-3" name="country_code">
                  <option value ="+91">+91</option>
               </select>
               <input type="text" value="<?php echo set_value('mobile');?>"  class=" input-sm col-md-9" name="mobile" placeholder="Mobile" autofocus>
            </div>
            <span class="error bold"><?php echo form_error('mobile');?></span>
            <button class="btn btn-sm btn-login btn-block" type="submit">Submit</button>
            <div class="registration">
               Already Registered.
               <a class="" href="<?php echo base_url(). 'index.php/auth_panel/login/index'; ?>">
               Login
               </a>
            </div>
         </div>
      </form>
   </div>
</body>
    <script src="<?php echo base_url();?>auth_panel_assets/js/jquery.js"></script>
    <script src="<?php echo base_url();?>auth_panel_assets/js/bootstrap.min.js"></script>
</html>
