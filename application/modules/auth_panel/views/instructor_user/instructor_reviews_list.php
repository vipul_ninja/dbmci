

		<div class="col-sm-12">
		<section class="panel">
			<header class="panel-heading">
		 Instructor's reviews
	</header>
			<div class="panel-body">
			<div class="adv-table">
			<table  class="display table table-bordered table-striped" id="all-instructor-user-reviews-grid">
			<thead>
				<tr>

			  <th>#</th>
			  <th>User name</th>
			  <th>Rating</th>
			  <th>Review</th>
			  <th>Created at</th> 

			 <th>Action </th> 	 
				</tr>
			</thead>
		  <thead>
			  <tr>
				  <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="4"  class="search-input-text form-control"></th>


				 <th></th> 
			  </tr>
		  </thead>
			</table>
			</div>
			</div>
		</section>
	</div>

<?php 
$is_instructor = 0;
if(isset($user_instructor_data)){
	$is_instructor = 1; }

	
$instructor_id = $user_data['id'];
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
                <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
				<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
                <script type="text/javascript" language="javascript" > 
					//var is_instructor = '$is_instructor';
				//	if(is_instructor == 1){
						jQuery(document).ready(function() {
						   var table = 'all-instructor-user-reviews-grid';
						   var dataTable_instructor = jQuery("#"+table).DataTable( {
							   "processing": true,
								"pageLength": 15,
								"lengthMenu": [[15, 25, 50], [15, 25, 50]],
							   "serverSide": true,
							   "order": [[ 4, "desc" ]],
							   "ajax":{
								   url :"$adminurl"+"instructor_user/ajax_instructor_user_ratings_list/$instructor_id", // json datasource
								   type: "post",  // method  , by default get
								   error: function(){  // error handling
									   jQuery("."+table+"-error").html("");
									   jQuery("#"+table+"_processing").css("display","none");
								   }
							   }
						   } );
						   jQuery("#"+table+"_filter").css("display","none");
						   $('.search-input-text').on( 'keyup click', function () {   // for text boxes
							   var i =$(this).attr('data-column');  // getting column index
							   var v =$(this).val();  // getting search input value
							   dataTable_instructor.columns(i).search(v).draw();
						   } );
							$('.search-input-select').on( 'change', function () {   // for select box
								var i =$(this).attr('data-column');
								var v =$(this).val();
								dataTable_instructor.columns(i).search(v).draw();
							} );											
					   } );			   
				 //  }
				    
               </script>              

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );





