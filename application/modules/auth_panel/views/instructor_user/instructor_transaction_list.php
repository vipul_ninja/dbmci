<?php

$ins_id =  $user_data['id'] ;
$total_earn_amount =  $this->db->select('sum(instructor_share) as total')->where(array('transaction_status'=>1,'instructor_id'=>$ins_id))->get('course_transaction_record')->row()->total;
$total_paid_amount =  $this->db->select('sum(amount) as total')->where(array('instructor_id'=>$ins_id))->get('instructor_pay_record')->row()->total;


$ins_data =  $this->db->where('id',$ins_id)->get('backend_user')->row();
?>

<section class="panel">

    <div class="panel-body">

          <div class="col-md-2">
            <img class="img-responsive img-thumbnail"  style="max-height:200px;max-width:100px" src="<?php echo $ins_data->profile_picture; ?>">
          </div>
          <div class="col-md-5 bold">
            Name -: <?php echo $ins_data->username;?> </br>
            Email -: <?php echo $ins_data->email;?> </br>
            Mobile -: <?php echo $ins_data->mobile;?> </br>
          </div>
          <div class="col-md-5">
            <span class="bold" >Total amount of earning -: <?php echo $total_earn_amount ;?> </span></br>
            <span class="bold">Total credited amount from admin -: <?php echo $total_paid_amount ;?></span></br>
            <span class="bold" >Total due amount -: <?php echo $total_earn_amount -  $total_paid_amount ;?></span>
          </div>


    </div>
</section>

		<div class="col-sm-12">
		<section class="panel">
			<header class="panel-heading">
		 All Transactions
	</header>
			<div class="panel-body">
			<div class="adv-table">
			<table  class="display table table-bordered table-striped" id="all-instructor-user-reviews-grid">
			<thead>
				<tr>

			  <th>Invoice id</th>
			  <th>Course name</th>
			  <th>Share</th>
			  <th>Created at</th>

				</tr>
			</thead>
		  <thead>
			  <tr>
				  <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
				  <th><input type="text" data-column="3"  class="search-input-text form-control"></th>

			  </tr>
		  </thead>
			</table>
			</div>
			</div>
		</section>
	</div>


  <section class="panel">
      <header class="panel-heading">
          Instructor Payment Records
      </header>
      <div class="panel-body ">
          <div class="adv-table">
              <table  class="display table table-bordered table-striped col-md-12" id="all-ins_transaction-grid">
                  <thead>
                      <tr>
                          <th>Transaction id</th>
                          <th>Amount</th>
                          <th>Message </th>
                          <th>Creation time</th>
                      </tr>
                  </thead>
                  <thead>
                      <tr>
                          <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                          <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                          <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                          <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
                      </tr>
                  </thead>
              </table>
          </div>
      </div>
  </section>

<?php
$is_instructor = 0;
if(isset($user_instructor_data)){
	$is_instructor = 1; }


$instructor_id = $user_data['id'];
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
                <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
				<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
                <script type="text/javascript" language="javascript" >
					//var is_instructor = '$is_instructor';
				//	if(is_instructor == 1){
						jQuery(document).ready(function() {
						   var table = 'all-instructor-user-reviews-grid';
						   var dataTable_instructor = jQuery("#"+table).DataTable( {
							   "processing": true,

							   "serverSide": true,
							   "order": [[ 0, "desc" ]],
							   "ajax":{
								   url :"$adminurl"+"instructor_user/ajax_instructor_transactions_list/$instructor_id", // json datasource
								   type: "post",  // method  , by default get
								   error: function(){  // error handling
									   jQuery("."+table+"-error").html("");
									   jQuery("#"+table+"_processing").css("display","none");
								   }
							   }
						   } );
						   jQuery("#"+table+"_filter").css("display","none");
						   $('.search-input-text').on( 'keyup click', function () {   // for text boxes
							   var i =$(this).attr('data-column');  // getting column index
							   var v =$(this).val();  // getting search input value
							   dataTable_instructor.columns(i).search(v).draw();
						   } );
							$('.search-input-select').on( 'change', function () {   // for select box
								var i =$(this).attr('data-column');
								var v =$(this).val();
								dataTable_instructor.columns(i).search(v).draw();
							} );
					   } );
				 //  }

         jQuery(document).ready(function() {
            var table = 'all-ins_transaction-grid';
            var dataTable_review = jQuery("#"+table).DataTable( {
                "processing": true,
                "serverSide": true,
                "order": [[ 0, "desc" ]],
                "ajax":{
                    url :"$adminurl"+"instructor_user/ajax_single_instructor_list/"+"$instructor_id", // json datasource
                    type: "post",  // method  , by default get
                    error: function(){  // error handling
                        jQuery("."+table+"-error").html("");
                        jQuery("#"+table+"_processing").css("display","none");
                    }
                }
            } );

            jQuery("#"+table+"_filter").css("display","none");
            $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                var i =$(this).attr('data-column');  // getting column index
                var v =$(this).val();  // getting search input value
                dataTable_review.columns(i).search(v).draw();
            } );
        } );

               </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
