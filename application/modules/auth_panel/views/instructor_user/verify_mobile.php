<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Emedicoz </title>

    <!-- Bootstrap core CSS auth_panel_assets -->
    <link href="<?php echo base_url();?>auth_panel_assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>auth_panel_assets/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>auth_panel_assets/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>auth_panel_assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>auth_panel_assets/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>auth_panel_assets/js/html5shiv.js"></script>
    <script src="<?php echo base_url();?>auth_panel_assets/js/respond.min.js"></script>
    <![endif]-->

</head>
  <body class="login-body">

    <div class="container">
      <form class="form-signin" method="POST" action="<?php echo site_url('auth_panel/registration/verify_mobile');?>" enctype="multipart/form-data">
       <h2 class="form-signin-heading">Verify Mobile </h2>
        <div class="login-wrap ">
			<span class="success bold"><?php if(isset($status)){echo $status;}?></span>
			<div class="form-group">
			  <label for="exampleInputPassword2" class="sr-only">OTP</label>
			  <input type="password" placeholder="Enter OTP" name="otp" class="form-control">
				 <span class="error bold"><?php echo form_error('otp');?></span>
			</div>

            <button class="btn btn-sm btn-login btn-block" type="submit">Submit</button>

            <div class="registration">
                <a class="" href="<?php echo base_url(). 'index.php/auth_panel/login/index'; ?>">
                    Login
                </a>
            </div>


        </div>

      </form>

    </div>


  </body>

    <script src="<?php echo base_url();?>auth_panel_assets/js/jquery.js"></script>
    <script src="<?php echo base_url();?>auth_panel_assets/js/bootstrap.min.js"></script>
