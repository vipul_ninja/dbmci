
        <div class="container">
            <div class="row">
                <div class="col-md-12"><h2>Menu Management</h2>
                <!-- <p>Click the Load Button to execute the method <code>setData(Array data)</code></p> -->
                </div>
            </div>
            <span id="success_msg" align="center"></span>
            <div class="row">
                <div class="col-md-6">
                    <div class="card mb-3">
                        <div class="card-header"><h5 class="float-left">Menu</h5>
                            <div class="float-right" style="display: none;">
                                <button id="btnReload" type="button" class="btn btn-outline-secondary">
                                    <i class="fa fa-play"></i> Load Data</button>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul id="myEditor" class="sortableLists list-group">
                            </ul>
                        </div>
                        <div class="card-footer">
                            <div>
                                <button id="btnOutput" type="button" class="btn btn-success"><i class="fas fa-check-square"></i> Save Menu</button>
                            </div>
                        </div>
                    </div>
                  <!--   <p>Click the Output button to execute the function <code>getString();</code></p> -->
                    <div class="card">
                        <!-- <div class="card-header">JSON Output
                        <div class="float-right">
                        <button id="btnOutput" type="button" class="btn btn-success"><i class="fas fa-check-square"></i> Output</button>
                        </div>
                        </div> -->
                        <div class="card-body" style="display: none;">
                            <div class="form-group">
                                <textarea id="out" class="form-control" cols="50" rows="10"><?=$menu_json; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-primary mb-3">
                        <div class="card-header bg-primary text-white">Add/Edit menu</div>
                        <div class="card-body">
                            <form id="frmEdit" class="form-horizontal">
                                <div class="form-group">
                                    <label for="text">Text</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control item-menu" name="text" id="text" placeholder="Text">
                                        <div class="input-group-append">
                                            <button type="button" id="myEditor_icon" class="btn btn-outline-secondary"></button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="icon" class="item-menu">
                                </div>
                                <div class="form-group">
                                    <label for="href">URL</label>
                                    <input type="text" class="form-control item-menu" id="href" name="href" placeholder="URL">
                                </div>
                                <div class="form-group">
                                    <label for="target">Target</label>
                                    <select name="target" id="target" class="form-control item-menu">
                                        <option value="_self">Self</option>
                                        <option value="_blank">Blank</option>
                                        <option value="_top">Top</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">Tooltip</label>
                                    <input type="text" name="title" class="form-control item-menu" id="title" placeholder="Tooltip">
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">
                            <button type="button" id="btnUpdate" class="btn btn-primary" disabled><i class="fas fa-sync-alt"></i> Update Menu</button>
                            <button type="button" id="btnAdd" class="btn btn-success"><i class="fas fa-plus"></i> Add Menu</button>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>     
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"/>
<link rel="stylesheet" href="<?php echo AUTH_ASSETS; ?>/new/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css">

<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>


<script>
// var menu = $menu_json;
jQuery(document).ready(function () {
    /* =============== DEMO =============== */
    // menu items
    var arrayjson = '<?php echo $menu_json;?>';
    // icon picker options
    var iconPickerOptions = {searchText: "Search...", labelHeader: "{0}/{1}"};
    // sortable list options
    var sortableListOptions = {
        placeholderCss: {'background-color': "#cccccc"}
    };

    var editor = new MenuEditor('myEditor', {listOptions: sortableListOptions, iconPicker: iconPickerOptions});
    editor.setForm($('#frmEdit'));
    editor.setUpdateButton($('#btnUpdate'));
    $('#btnReload').on('click', function () {
        editor.setData(arrayjson);
    });
    $( "#btnReload" ).trigger( "click" );
    $('#btnOutput').on('click', function () {
        var str = editor.getString();
        $("#out").text(str);
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url("/auth_panel/web_handler/create_menu/save_menu"); ?>',
            data: {menu_json: str},
            success: function (data) {
                if(data == '1'){
                    $('#success_msg').html('');
                    $("html, body").animate({scrollTop: 0}, 600);
                    $('#success_msg').css({"color":"green","font-size":"15px"}).html('Your menu is successfully saved...!').fadeIn(500).fadeOut(8000);
                }
            },
            error: function (err) {
                console.log("ERR" + err);
            },
        });
    });

    $("#btnUpdate").click(function(){
        editor.update();
    });

    $('#btnAdd').click(function(){
        editor.add();
    });
    /* ====================================== */
});
</script>