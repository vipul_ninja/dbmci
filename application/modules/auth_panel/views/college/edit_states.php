
    <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Edit States

                <a href="<?php echo AUTH_PANEL_URL."add_college/manage_states"; ?>"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
          </header>
          <div class="panel-body">
              <form role="form" class="form-inline"  method="post">
                  <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputPassword2">State Name</label>
                        <input type="text" placeholder="State Name" name="name"  value="<?php echo $state_data['name']; ?>" class="form-control input-sm ">
                        <span style="color:red"><?php  echo form_error('name');  ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                  
                  </div>
                  <div class="col-md-3">
                  <div class="form-group">
                      <label for="exampleInputPassword2">Status</label>
                      <select class="form-control input-sm" name="status">
                        <option disabled="disabled" >Select Status</option>
                        <?php if($state_data['status'] == 1) {
                              echo '<option value="1" selected="selected">Enable</option><option value="0">Disable</option>';
                        } elseif($state_data['status'] == 0) { 
                              echo '<option value="1" >Enable</option><option value="0" selected="selected">Disable</option>';
                        } ?>
                      </select>
                  </div>
                  </div>
                  <button class="btn btn-success btn-sm" type="submit">Submit</button>
              </form>

          </div>
      </section>
  </div>
