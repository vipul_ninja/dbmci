<?php
$q_string = "";
if(validation_errors() == ""){
  $q_string =  http_build_query($_POST);
}

?>
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
        INSTRUCTOR REPORTS
        </header>
        <div class="panel-body">
            <form role="form" method="post" class="">
              <div class="col-md-12 error bold"><?php echo validation_errors(); ?></div>

                <div class="form-group col-md-12 ">
                  <label for="exampleInputEmail2" class="">Course Instructor </label>
                  <select name="instructor_id" class="form-control input-xs">
                    <option value="">--Select--</option>
                    <?php
                    $query  =  $this->db->query("SELECT ctr.instructor_id , bu.username
                                from course_transaction_record as ctr
                                join backend_user as bu on bu.id = ctr.instructor_id
                                group by ctr.instructor_id ");
                    $all = $query->result();
                    foreach($all as $ins){
                      echo "<option ".set_select('instructor_id', $ins->instructor_id)." value='".$ins->instructor_id."'>".$ins->username."</option>";
                    }
                    ?>
                  </select>
                </div>

                <div class="form-group col-md-12">
                  <label for="exampleInputEmail2" class="">Choose filter </label>
                  <select  name="filter"class="form-control input-xs">
                    <option value="INVOICENO" <?php echo  set_select('filter', 'INVOICENO'); ?>  >--INVOICE NO.--</option>
                    <option value="TODAY" <?php echo  set_select('filter', 'TODAY'); ?> >Today</option>
                    <option value="YESTERDAY" <?php echo  set_select('filter', 'YESTERDAY'); ?> >Yesterday</option>
                    <option value="MONTH" <?php echo  set_select('filter', 'MONTH'); ?> >Current Month</option>
                    <option value="YEAR" <?php echo  set_select('filter', 'YEAR'); ?> >Current Year</option>
                    <option value="DATE_RANGE" <?php echo  set_select('filter', 'DATE_RANGE'); ?> >Date Range</option>
                  </select>
                </div>

                <div class="clearfix margin-top"></div>
                <div class="col-md-12 DATE_RANGE " style="display:none">
                  <div data-date-format="dd-mm-yyyy" data-date="13/07/2013">
                    <div class="form-group margin-top">
                      <label for="exampleInputEmail2" class="">From date </label>
                      <input type="text" value="<?php echo set_value('from_date');?>" name="from_date" placeholder="dd-mm-yyyy"  id="min-date-course-transaction" class="form-control date-range-filter input-xs  "  placeholder="">
                    </div>
                    <div class="form-group margin-top">
                      <label for="exampleInputPassword2" class="">To date </label>
                      <input type="text" value="<?php echo set_value('to_date');?>"  name="to_date" placeholder="dd-mm-yyyy" id="max-date-course-transaction" class="form-control date-range-filter input-xs  "  placeholder="">
                    </div>
                  </div>
                </div>

                <div class="clearfix margin-top"></div>
                <div class="form-group col-md-12 INVOICENO " style="display:none" >
                  <label for="exampleInputEmail2" class="">Invoice no. </label>
                    <input type="text" value="<?php echo set_value('id');?>" name="id" placeholder="Invoice number " class="form-control input-xs">
                </div>

                <div class="col-md-12 margin-top" >
                  <div class="form-group ">
                    <label for="exampleInputEmail2" class="">Transaction Status </label>
                    <select name="transaction_status" class="form-control input-xs">
                      <option value="">--Select--</option>
                      <option value="ALL" <?php echo  set_select('transaction_status', 'ALL'); ?> >All</option>
                      <option value="2" <?php echo  set_select('transaction_status', '2'); ?>  >Pending</option>
                      <option value="1" <?php echo  set_select('transaction_status', '1'); ?>  >Complete</option>
                    </select>
                  </div>
                  <div class="form-group ">
                    <label for="exampleInputEmail2" class="">Course Type </label>
                    <select name="course_type" class="form-control input-xs">
                      <option value="">--Select--</option>
                      <option value="ALL" <?php echo  set_select('course_type', 'ALL'); ?> >All</option>
                      <option value="FREE"  <?php echo  set_select('course_type', 'FREE'); ?> >Free</option>
                      <option value="PAID" <?php echo  set_select('course_type', 'PAID'); ?> >Paid</option>
                    </select>
                  </div>

                </div>

                <div class="clearfix margin-top"></div>
                <div class="col-md-12 margin-top">
                  <button class="btn btn-success btn-xs bold" type="submit">Search</button>
                  <input class="btn btn-success btn-xs bold pull-right" name="csv" type="submit" value="Download CSV">
                </div>
            </form>

        </div>
    </section>
</div>
<?php if($q_string != ""){ ?>
<section class="panel">
  <div class="panel-body ">
  <div class="adv-table ">
    <div class="table-responsive col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
      <table  class="table display table-bordered table-striped" id="all-reports-grid">
          <thead>
              <tr>
                  <th>Invoice no.</th>
                  <th>Course price </th>
                  <th>User name </th>
                  <th>Email </th>
                  <th>Mobile </th>
                  <th>Instructor </th>
                  <th>Course name </th>
                  <th>Date</th>
              </tr>
          </thead>
          <thead class="hide">
              <tr>
                  <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="5"  class="search-input-text form-control"></th>
                  <th><input type="text" data-column="6"  class="search-input-text form-control"></th>
              </tr>
          </thead>
      </table>
    </div>
  </div>
</div>
</section>
<?php } ?>
<?php


$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
               <script type="text/javascript" language="javascript" >
               if('$q_string' != "" ){
                 jQuery(document).ready(function() {
                     var table = 'all-reports-grid';
                     var dataTable_user = jQuery("#"+table).DataTable( {
                          "processing": true,
                          "pageLength": 15,
                          "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                          "serverSide": true,
                          "order": [[ 0, "desc" ]],
                          "ajax":{
                             url :"$adminurl"+"reports/all_payment_reports/ajax_all_report_list?"+"$q_string", // json datasource
                             type: "post",  // method  , by default get
                             error: function(){  // error handling
                                 jQuery("."+table+"-error").html("");
                                 jQuery("#"+table+"_processing").css("display","none");
                             }
                         }
                     } );
                     jQuery("#"+table+"_filter").css("display","none");
                     $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                         var i =$(this).attr('data-column');  // getting column index
                         var v =$(this).val();  // getting search input value
                         dataTable_user.columns(i).search(v).draw();
                     } );
                 } );
               }


               $('#min-date-course-transaction').datepicker({
                  format: 'dd-mm-yyyy',
                autoclose: true

              });
              $('#max-date-course-transaction').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true

              });

              $(".form-group input,select").focusin(function() {
                $('.error').hide('slow')
              });

              $("select[name=filter]").change(function() {
                $(".DATE_RANGE ,.INVOICENO").hide();
                var cls =  $(this).val();
                if(cls == "INVOICENO"){
                  $('input[name=from_date] , input[name=to_date]' ).val('');
                }else if(cls == "DATE_RANGE"){
                  $('input[name=id]').val('');
                }else{
                  $('input[name=from_date] , input[name=to_date] , input[name=id] ' ).val('');
                }
                $("."+cls).show();

              }).change();
               </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
