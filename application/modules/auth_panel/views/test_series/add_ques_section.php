<style>
.stream_css {

    display: inline !important;
    width: 17% !important;
}
</style> 



<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		<?php // echo strtoupper($page); ?> Stream(s) LIST
		</header>
		<div class="panel-body">
		<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="subject_questions_list">
		<thead>
		<tr>
		<th>#</th>
		<th>Question</th>
		<th>Subject</th>
		<th>Topic</th>
		<th>Question type</th>
		<th>Difficulty level</th>
		<th>Marks </th>
		<th>Negative marks</th>
		<th>Added by </th>
		<th>Action </th>
<th><input name="select_all" value="1" id="example-select-all" type="checkbox"></th>
		</tr>
	</thead>
  <thead>
	  <tr>
		  <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
		  <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
		  <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
		  <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
		  <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
		  <th><input type="text" data-column="5"  class="search-input-text form-control"></th>
		  <th><input type="text" data-column="6"  class="search-input-text form-control"></th>
		  <th><input type="text" data-column="7"  class="search-input-text form-control"></th>
		  <th><input type="text" data-column="8"  class="search-input-text form-control"></th>
		  <th></th>
<th></th>
	  </tr>
  </thead>
		</table>
		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
//if($page == 'android') { $device_type = 1; } elseif ($page == 'ios') { $device_type = 2; } elseif ($page == 'all') { $device_type = '0'; }
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
					jQuery(document).ready(function() {
						var subject_id = '$subject_id';
					 var table = 'subject_questions_list';
					 var dataTable_q_list = jQuery("#"+table).DataTable( {
						 "processing": true,
						 "serverSide": true,
						 "order": [[ 0, "desc" ]],
						 "ajax":{
							 url :"$adminurl"+"test_series/test_series/ajax_subject_wise_question_list/?subject_id="+subject_id+"&test_series_id=$test_series_id", // json datasource
							 type: "post",  // method  , by default get
							 error: function(){  // error handling
								 jQuery("."+table+"-error").html("");
								 jQuery("#"+table+"_processing").css("display","none");
							 }
						 },
						 "pageLength": 100,
						 'columnDefs': [{
							 'targets': 10,
							 'searchable': false,
							 'orderable': false,
							 'className': 'dt-body-center',
							 /*'render': function (data, type, full, meta){
								 return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
							 }*/
						  }]
					 } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );

                   } );
               </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
