

<style>
.stream_css {

    display: inline !important;
    width: 17% !important;
}
</style> 

<div class="col-lg-12">
<section class="panel">
        <header class="panel-heading">
           Edit Slider
        </header>
<div class="container">
<form method="POST" enctype="multipart/form-data">
    <div class="form-group col-xs-11">
        <label for="">Slider Name:</label>
        <input type="text" class="form-control" width="90%" id="edited_name" name="name" value="<?php if(isset($slider_data)){echo $slider_data['name'];}?>">
    </div>

    <div class="form-group col-xs-11">
    <button type="submit" class="btn btn-md bold btn-info">Edit</button>
    </div>
</form>
</div>
    </section>
</div>
<div class="clearfix"></div>
<? php>
