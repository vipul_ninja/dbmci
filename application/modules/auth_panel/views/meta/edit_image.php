<style>
.stream_css {

    display: inline !important;
    width: 17% !important;
}
</style> 

<div class="col-lg-12">
    <section class="panel">
    <header class="panel-heading">
        Edit Image
    </header>    
    <div class="container">
    <form method="POST" enctype="multipart/form-data">
        <div class="form-group col-xs-11">
            <label for="pwd">Description:</label>
            <textarea name="text" class="form-control"><?php echo $data_image['text'];?></textarea>
        </div>

        <div class="form-group col-xs-11">
            <label>Image</label>
         </div> 

        <div class="form-group col-xs-11">           
            <img src="<?php echo $data_image['image_url'];?>" height="90px" width="90px">
        </div>

        <div class="form-group col-xs-11">
            <label>Change Images</label>            
            <input class="form-group" type="file" accept="image/*" name = "image_url" id="exampleInputFile">
        </div>
        <div class="form-group col-xs-11">
        <button type="submit" class="btn btn-md bold btn-info">Add</button>
        </div>
    </form>
    </div>
    </section>
</div>

<div class="clearfix"></div>

