<div style="width:100%; margin-left:0px;margin-top:-70px;" class="modal fade" id="modal_queries_sales" role="dialog">   
    <div class="modal-dialog modal-lg" style="width:60%;height:80%;" > 
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><center><b>Details</center></b></h4>
            </div>
			<div class="modal-body">
			<fieldset>
			<div class="setting col-md-6">
					<label>	<h5>Name:&nbsp&nbsp&nbsp</h5></label>								
					<label style="display:inline"><?php if (isset($details)) { echo $details['name']; } ?></label>
					<br>
			</div>
			<div class="setting col-md-6">
					<label>	<h5>Email:&nbsp&nbsp</h5></label>
					 <label style="display:inline"><?php if (isset($details)) {
                        echo $details['email'];
                       } ?></label>
					<br>
					</div>		
			</fieldset>
			<fieldset>
			<div class="setting col-md-6">
					<label>	<h5>Mobile:&nbsp&nbsp&nbsp</h5></label>
					<label style="display:inline"><?php if (isset($details)) {
							echo $details['code'].'-'.$details['mobile'];
					} ?></label>
			</div>				
			
			<div class="setting col-md-6">
					<label>	<h5>Budget:&nbsp&nbsp&nbsp</h5></label>
					<label style="display:inline"><?php if (isset($details)) {
                        echo $details['budget'];
                       } ?></label>				
			</div>									
			</fieldset>
			
			<fieldset>
			<div class="setting col-md-6">
							<label>	<h5>User:&nbsp&nbsp&nbsp&nbsp</h5></label>
							<label style="display:inline"><?php 
								if (isset($details)) 
								{
									echo $details['sale_user_name'];
								} 
							?></label>							
			</div>
			 <div class="setting col-md-6">
							<label>	<h5>Status:&nbsp&nbsp&nbsp</h5></label>
							<label style="display:inline"><?php if (isset($details)) {
							echo $details['status_name'];
							} ?></label>							
			</div>				
			</fieldset>			
			<fieldset>			
			<div class="setting col-md-6">
			<label>	<h5>Country:&nbsp&nbsp&nbsp</h5></label>
			<label style="display:inline"><?php if (isset($details)) {
			echo $details['country'];
			} ?>
			</label>   
			</div>
			<div class="setting col-md-6">
							<label>	<h5>Date/Time:&nbsp&nbsp&nbsp</h5></label>
							<label style="display:inline"><?php if (isset($details)) {
								echo $details['create_date'];
								} ?>
							</label>	
			</div>
			</fieldset>
			
			<fieldset>
			<div class="setting col-md-6">
					<label>	<h5>NDA:&nbsp&nbsp&nbsp&nbsp&nbsp</h5></label>
					<label style="display:inline"><?php if (isset($details)) {
                                    if($details['send_nda']==1||$details['send_nda']=='on'){
										echo "Yes";
									}else
									{									
										echo "No";
									}								 
                              } ?>
					</label>
			</div>	
			<div class="setting col-md-6">
						<label>	<h5>Skype:&nbsp&nbsp&nbsp</h5></label>
						<label style="display:inline"><?php if (isset($details)) {
						echo $details['skype_whatsapp'];
						} ?>
						</label>
			</div>			
			</fieldset>
			
			<fieldset>
						<div class="setting col-md-6">
							<label>	<h5>By Page:&nbsp&nbsp</h5></label>
							<label style="display:inline"><?php if (isset($details)) {
								echo $details['page'];
								} ?></label>
							</p>
						</div>
						<div class="setting col-md-6">
							<label>	<h5>By Website:</h5></label>
							<label style="display:inline"><?php if (isset($details)) {
							echo $details['by_website'];
							} ?></label>
						</div>
				
						
			</fieldset>			
			<fieldset>
			    <div class="setting col-md-6">
					<label>	<h5>Category:&nbsp&nbsp&nbsp</h5></label>
					<label style="display:inline"><?php if (isset($details)) {
                        echo $details['category'];
                       } ?>
					</label>
			</div>
			<div class="setting col-md-6">
					<label>	<h5>When to Start:&nbsp&nbsp&nbsp</h5></label>
					<label style="display:inline"><?php if (isset($details)) {
					echo $details['preferences'];
					} ?></label>
			</div>
			</fieldset>
			
			<fieldset>
					<div class="setting col-md-12">
					<label>	<h5>Message :&nbsp</h5><br></label>
						<!--<textarea readonly  rows="5" name="message" class="form-control"><?php if (isset($details)){echo $details['message'];}?></textarea>-->
						<?php if (isset($details)){echo $details['message'];}?>
					<br> 
					</div>
			</fieldset>	
						
				<h5 style="margin-left:15px";>Follow Up :</h5>
				<br>			
				<?php
				foreach($comments as $comment){				 
				?>
				<div class="direct-chat-msg right">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left" style="margin-left:15px"><?php echo $comment['comment_by'];?></span>
                        <span class="direct-chat-timestamp pull-right" style="margin-right:15px"><?php echo $comment['create_date'];?></span>
                      </div>                     
                      <div class="direct-chat-text" style="margin-left:15px"> 
                       <?php echo $comment['comment'];?>
                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
					
				<?php }
				 $a =$this->session->userdata('sale_user_name');
				 $sales_id =$this->session->userdata('id');
				 
				 
				?>		
				<br>				
				<fieldset>
				<div>
						<form method='POST' action="comments" name="asa">							
							<input type="hidden" name="lead_id" value="<?php if (isset($details)) {   echo $details['id'];  } ?> ">
							<textarea style="margin-left:15px" id="comment" name="comment" placeholder="Write Your Comment Heress....." cols="141" rows="4"></textarea>
							<input style="margin-left:15px" name="comment_by" value="<?php echo $a;?>" class="btn btn-success" type="hidden" >
							<input style="margin-left:15px" name="commenter_id" value="<?php echo $sales_id;?>" class="btn btn-success" type="hidden" >
							<br>
                            <p style="margin-left:15px;margin-top:10px">Follow up Date: <input type="text"  id="datepicker" placeholder="click to open calendar" name="folloup_date"></p>							
							<!--<p>Follow up Time: <input type="text" style="margin-left:15px;margin-top:10px"  placeholder="Enter time" name="follow_up_time"></p>-->
							<input style="margin-left:15px" class="btn btn-primary" type="submit" value="send" >							
						</form>
				</div>	
							 	
				<div class="setting col-md-6" style="display:inline">					
				<?php if($details['file'])
				{          
				$path = " http://www.new.appsquadz.com/uploads/query_documents/";?>												
				<a download  target="_blank" class ="btn btn-primary" href="<?php echo $path.$details['file']; ?>">Download file   <i class="fa fa-arrow-circle-down fa-1x" aria-hidden="true"></i></a>
				<?php }
				else{echo '<h5 style="color:red">Not Available</h5>';}
				?>	
				<br>
				</div>	
				
				<div class="setting col-md-6" style="display:inline">					
				<label style="display:inline">
				<?php 
				if (isset($details))
				{ 
				$a = $details['id'];
				//$mail = $details['email'];
				//echo $a;
				//echo $mail;
				}
				?>
				<button class="button btn btn-primary">Sendmail</button>
				</label>
				</div>						
				</fieldset>						
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
$('.button').click(function() {	
	var id=<?php echo $a ;?>;	
		$.ajax({
				type: "POST",
				url: "<?php echo base_url('index.php/admin_panel/') ?>Queries/modalmail",		
				data: { 
				id:id
				}
				}).done(function( msg ) {
				alert("Mail sent from modal");
			});    

});
	
$( function()
	{
			$( "#datepicker" ).datepicker();
			$('#datepicker').datepicker({
			dateFormat: 'dd-mm-yy'
		});  
	});
</script>
<?php
function  select()
{
	die('hello');
}
?>