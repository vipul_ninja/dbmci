<div>
   <div class="col-lg-6">
      <section class="panel">
         <header class="panel-heading">
            GST VALUE
         </header>
         <div class="panel-body">
            <form autocomplete="off" role="form" novalidate="novalidate" id="gst_points" method= "POST" enctype="multipart/form-data">
            <div class="form-group">
                  <label for="meta_value">Gst value  </label>
                  <input type="text" class="form-control  input-sm" name="GST_VALUE" value="<?php echo get_db_meta_key('GST_VALUE');?>"  placeholder="Enter value">
                  <span class="bold small">NOTE-: Do not append % sign in value.</span>
               </div>

               <button type="submit" class="btn btn-info btn-sm">Submit</button>
            </form>
         </div>
      </section>
   </div>
   <div class="clearfix"></div>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$validation_js = AUTH_ASSETS."js/jquery.validate.min.js";
$custum_js = <<<EOD
              <script src="$validation_js" type="text/javascript"></script>
                <script>
				
				 var form = $("#gst_points");                                          
                        form.validate({
                            errorPlacement: function errorPlacement(error, element) {
                                element.after(error);
                            },
                            rules: {
                                GST_VALUE: {
                                required: true,
                                min: 0,
                                max:30
                                }
                            }
                        }); 					  					
                   
              </script>          
EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
