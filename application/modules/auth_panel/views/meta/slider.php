<style>
.stream_css {

    display: inline !important;
    width: 17% !important;
}
</style> 

<div class="col-lg-12">
<section class="panel">
        <header class="panel-heading">
            Add Slider
        </header>
<div class="container">
<form method="POST" enctype="multipart/form-data">
    <div class="form-group col-xs-11">
        <label for="">Slider Name:</label>
        <input type="text" class="form-control" width="90%" value="" id="name" name="name" placeholder="Enter Stream Name">
    </div>

    <div class="form-group col-xs-11">
    <button type="submit" class="btn btn-md bold btn-info">Add</button>
    </div>
</form>
</div>
    </section>
</div>
<div class="clearfix"></div>

<div class="col-sm-12">

	<section class="panel">
		<header class="panel-heading">
		<?php // echo strtoupper($page); ?> Names LIST
		</header>
		<div class="panel-body">
		<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="all-user-grid">
  		<thead>
    		<tr>
          <th>#</th>
            <th>Name </th>            
            <th>Created on</th>
            <th>Last updated</th>
            <th>Action </th>
    		</tr>
  		</thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th></th>
              <th></th>
              <th></th>			        
          </tr>
      </thead>
		</table>
		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
//if($page == 'android') { $device_type = 1; } elseif ($page == 'ios') { $device_type = 2; } elseif ($page == 'all') { $device_type = '0'; }
$custum_js = <<<EOD
              <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >
                   jQuery(document).ready(function() {
                       var table = 'all-user-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"meta/Slider/ajax_slider_name_list", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );

						$( function() {
					$( ".dpd1" ).datetimepicker();
					$( ".dpd2" ).datetimepicker();
					 }); // datepicker closed
                   } );
               </script>
            //    <script>
            //    function enable_disable(id)
            //     {
                    
            //     }
            //    </script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
