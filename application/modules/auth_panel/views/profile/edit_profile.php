<div>
   <div class="col-lg-4">
      <section class="panel">
         <header class="panel-heading">
            Update Password
         </header>
         <div class="panel-body">
            <form autocomplete="off" role="form" novalidate="novalidate" id="Update_password" method= "POST" enctype="multipart/form-data">
              <div class="form-group">
                <label for="">Current password  </label>
                <input type="password" class="form-control  input-sm" name="current_password" value=""  placeholder="Enter current password">
                <span class="error bold"><?php echo form_error('current_password'); ?></span>
              </div>
              <div class="form-group">
                <label for="">New password  </label>
                <input type="password" id="new_password" class="form-control  input-sm" name="new_password" value=""  placeholder="Enter new password">
                <span class="error bold"><?php echo form_error('new_password'); ?></span>
              </div>
              <div class="form-group">
                <label for="">Re-enter new  password  </label>
                <input type="password" class="form-control  input-sm" name="renew_password" value=""  placeholder="Enter new password again">
                <span class="error bold"><?php echo form_error('renew_password'); ?></span>
              </div>
               <input type="submit" name="change_password" value="Update" class="btn btn-info btn-sm">
            </form>
         </div>
      </section>
   </div>
   <?php
   /* user_data*/
   $userd = $this->db->where('id',$this->session->userdata('active_backend_user_id'))->get('backend_user')->row();
   ?>
   <div class="col-lg-4">
      <section class="panel">
         <header class="panel-heading">
            Update Profile
         </header>
         <div class="panel-body">
            <form autocomplete="off" role="form" novalidate="novalidate" id="Update_profile" method= "POST" enctype="multipart/form-data">
              <div class="form-group">
                <label for="">Name  </label>
                <input type="text" class="form-control  input-sm" name="name" value="<?php echo $userd->username ;?>"  placeholder="Enter name">
                <span class="error bold"><?php echo form_error('name'); ?></span>
              </div>
              <div class="form-group">
                <label for="">Email  </label>
                <input type="email"  class="form-control  input-sm" name="email" value="<?php echo $userd->email ;?>"   placeholder="Enter valid email">
                <span class="error bold"><?php echo form_error('email'); ?></span>
              </div>
              <div class="form-group">
                <label for="">Mobile Number   </label>
                <input type="" class="form-control  input-sm" name="mobile" value="<?php echo $userd->mobile ;?>"   placeholder="Enter 10 digit mobile number ">
                <span class="error bold"><?php echo form_error('mobile'); ?></span>
              </div>
               <input type="submit" name="change_profile" value="Update" class="btn btn-info btn-sm">
            </form>
         </div>
      </section>
   </div>


	<div class="col-lg-4">
      <section class="panel">
         <header class="panel-heading">
            Update Image
         </header>
         <div class="panel-body">
            <form autocomplete="off" role="form" novalidate="novalidate" id="Update_picture" method= "POST" enctype="multipart/form-data">
                <div class="form-group">
                  <?php
                    if($userd->profile_picture != "" ){
                      echo '<img  class="col-md-12"   src="'.$userd->profile_picture.'">';
                    }
                  ?>
					          <label for="exampleInputFile">Upload Image</label>
					               <input type="file" accept="image/*" name = "profile_picture" id="exampleInputFile">
					            <span class="error bold"><?php echo form_error('profile_picture');?></span>
		             </div>
               <input type="submit" name="change_image" value="Update" class="btn btn-info btn-sm">
            </form>
         </div>
      </section>
   </div>
   <div class="clearfix"></div>

</div>
<?php
$adminurl = AUTH_PANEL_URL;
$validation_js = AUTH_ASSETS."js/jquery.validate.min.js";
$custum_js = <<<EOD
              <script src="$validation_js" type="text/javascript"></script>
                <script>

				 var form = $("#Update_password");
                        form.validate({
                            errorPlacement: function errorPlacement(error, element) {
                                element.after(error);
                            },
                            rules: {
                                current_password: {
                                required: true
                              },
                                new_password: {
                                required: true,
                                maxlength:12,
                                minlength:8
                              },
                                renew_password: {
                                required: true,
                                maxlength:12,
                                minlength:8,
                                equalTo:"#new_password"
                                }
                            }
                        });
                        var form = $("#Update_picture");
                             form.validate({
                                 errorPlacement: function errorPlacement(error, element) {
                                     element.after(error);
                                 },
                                 rules: {
                                     profile_picture: {
                                     required: true
                                   }
                                 }
                             });
                             var form = $("#Update_profile");
                                  form.validate({
                                      errorPlacement: function errorPlacement(error, element) {
                                          element.after(error);
                                      },
                                      rules: {
                                          name: {
                                          required: true
                                        },
                                          email: {
                                          required: true,
                                          maxlength:50
                                        },
                                          mobile: {
                                          required: true,
                                          maxlength:10,
                                          minlength:10
                                          }
                                      }
                                  });
              </script>
EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
