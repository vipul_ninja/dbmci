<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		FAN WALL POST
		<span class="tools pull-right">
		<a href="javascript:;" class="fa fa-chevron-down"></a>
		<a href="javascript:;" class="fa fa-times"></a>
		</span>
		</header>
		<div class="panel-body">
		<div class="adv-table">		   

		<table  class="display table table-bordered table-striped" id="all-post-grid">
  		<thead>
    		<tr>
				<th>#</th>
				<th>Name</th>
        <th>Post_type</th>
				<th>likes</th>
        <th>comments</th>
        <th>share</th>
        <th>report_abuse</th>       
    		</tr>
  		</thead>
		</table>

		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD

               <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {

                       var table = 'all-post-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"fan_wall/ajax_all_post/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
						 $('.search-input-select').on( 'change', function () {   // for select box
							    var i =$(this).attr('data-column');
							    var v =$(this).val();
							    dataTable.columns(i).search(v).draw();
							} );
                   } );
               </script>              

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
