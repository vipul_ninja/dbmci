<style>
.panel-body {
    background: white none repeat scroll 0 0;
    margin-bottom: 6px;
}

.modal-body img{
    max-height: 400px;
}

.fb-user-status {
    line-height: 15px;
    padding: 3px 0;
}
.fb-user-thumb {
    float: left;
    margin-right: 10px;
    width: 50px;
}
.fb-user-thumb img {
    border-radius: 50%;
    height: 50px;
    width: 50px;
}
.fb-time-action {
    padding: 5px 0;
}
.fb-user-details p {
  color:inherit;      
}
/*
.fb-comments li {
    border-top: 1px solid #ebeef5;
    padding: 5px 15px 0px 15px;
}
*/
</style>
<section class=" live_feeder" data-element_id = "">

</section>



<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
            <script type="text/javascript" src="//player.wowza.com/player/latest/wowzaplayer.min.js"></script>
            
             <script>

                function get_feed(){
                  $('.load_spinner').show();
                  jQuery.ajax({
                      url: "$adminurl"+"post_controller/ajax_get_feed",
                      method: 'POST',
                      dataType: 'json',
                      data: {
                          "id":  '922',
                      },
                      success: function (data) {
                        console.log(data);
                        
                        var result = data.data;
                        //console.log(result);
                        
                        $.each( result, function( key, value ) {
                          result = value.data;
                         // console.log(result);
                          var shared_text = "";
                          if(result.is_shared == 1){
                            
                            shared_text =  '<p class="col-md-12 row panel"> <strong class="text-primary">'+result.post_owner_info.name+'</strong> shared <strong class="text-info">'+ result.post_shared_of.name+'</strong>\'s post.</p>';
                          }
                          if(result.post_type == "user_post_type_normal"){
                            var element_post_text = result.post_data.text;
                            
                          }else{
                            var element_post_text = '<span class="badge badge-sm label-warning">Question </span>'+result.post_data.question;
                            if(result.post_data.answer_one != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> A </span> <span class='small'> "+result.post_data.answer_one+'</span>';
                            }
                            if(result.post_data.answer_two != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> B </span> <span class='small'> "+result.post_data.answer_two+'</span>';
                            }
                            if(result.post_data.answer_three != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> C </span> <span class='small'> "+result.post_data.answer_three+'</span>';
                            }
                            if(result.post_data.answer_four != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> D </span> <span class='small' > "+result.post_data.answer_four+'</span>';
                            }
                            if(result.post_data.answer_five != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> E </span> <span class='small' > "+result.post_data.answer_five+'</span>';
                            }                                                        
                          }
                          /* code for images */                              
                          file_array = result.post_data.post_file;
                          var images = "";
                          var videos = "";
                          var document = "";
                          $.each( file_array , function( key, value ) {
                            if(value.file_type == "image"){
                              images = images+'<a href="#myModal9" class="model_popup"  data-toggle="modal"><img style="height:100px" class="img-thumbnail"  src="'+value.link+'"> </a>';

                            }

                            if(value.file_type == "video"){
                              videos = videos+ '<div class="col-md-12 text-center"><video class="col-md-4" controls><source src="'+value.link+'" ></video><!--<div id="playerElement_'+ value.id +'" style="width:50%; height:200px;margin:0 auto"></div>--></div><div class="clearfix"></div>';
                            }

                            if(value.file_type == "doc" || value.file_type == "xls" || value.file_type == "pdf" ){
                              document = document + '<p class="small bold" ><a target="_blank" href="'+value.link +'"><i class="fa fa-file  text-info"></i> '+value.file_info+'</a></p>';
                            }
                          });

                          if(result.post_owner_info.profile_picture != '') { var profile_image = '<img alt=""src="'+ result.post_owner_info.profile_picture +'">'; } else { var profile_image =  '<i class="fa fa-user fa-4x"></i>'; }
                            
                            
                          var post_action = '<div class="btn-group pull-right"><button type="button" class="btn btn-success dropdown-toggle btn-xs" data-toggle="dropdown" ria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="$adminurl'+'post_controller/view_full_post_'+result.post_type +'/'+ result.id +'" >View full post</a></li><li><a href="$adminurl'+'post_controller/delete_post/'+ result.id +'" onclick="return confirm_message()">Delete</a></li>';
                            if(result.is_shared == 0) { 
                           post_action +='<li><a href="$adminurl'+'post_controller/edit_post_'+ result.post_type +'/'+ result.id +'">Edit</a></li>';
                           }
                           post_action +='</ul></div>';
                          if(result.post_tag == '') { var post_tag = ""; } else { var post_tag = " - "+ result.post_tag; }
                          var html ='<div  class=panel-body><div class=fb-user-thumb>'+profile_image+'</div>'+ post_action +'<div class=fb-user-details><h3><a href=# class="bold">'+ result.post_owner_info.name +'</a><p class="small"> '+ result.post_owner_info.speciality + post_tag +'</p></h3><p class="small bold">'+ timeSince(new Date(parseInt(result.creation_time))) +' ago</div><div class=clearfix></div>'+shared_text+'<p class="col-md-12">'+ element_post_text +'</p>';
                         
                         if(videos != ""){
                          html += videos;
                         }

                          if(images != '') {
                            html += '<div class=panel-body><div class=album>'+images+'</div></div>';
                          }

                          if(document != ""){
                             html += document;
                          }

                          html += '<div style="clear:both" class="fb-border fb-status-container"><div class="fb-time-action small" ><a href=# >'+ result.likes +' likes</a> <span>-</span> <a data-post_id="'+result.id+'" style="cursor:pointer" class="comment_tab">'+ result.comments +' Comments</a> <span>-</span> <a href=# >'+ result.share +' Share</a></div> <div class="comment_element"></div>   </div></div>';
                          
                          $(".live_feeder").append(html);

                          /*$.each( file_array , function( key, value ) {
                            if(value.file_type == "video"){
                                var thumb = value.link.split('.').reverse();
                                thumb[0] = 'jpg';
                                thumb = thumb.reverse().join('.');

                                 WowzaPlayer.create('playerElement_'+value.id,
                                  {
                                  "license":"PLAY1-mTEKv-3cFTk-TMjeZ-vvreV-PMnuE",
                                  "sourceURL":"http://ec2-13-58-238-45.us-east-2.compute.amazonaws.com:1935/vod/"+value.link+"/playlist.m3u8",
                                  "autoPlay":false,
                                  "volume":"75",
                                  "mute":false,
                                  "loop":false,
                                  "audioOnly":false,
                                  "uiShowQuickRewind":true,
                                  "uiQuickRewindSeconds":"30",
                                  "posterFrameURL":"https://s3.ap-south-1.amazonaws.com/dams-apps-production/video_thumbnails/"+thumb,
                                  "uiPosterFrameFillMode":"fit"
                                  }
                                );
                            }

                          });*/

                          $(".live_feeder").attr('data-element_id',result.id);
                           $('.load_spinner').hide();
                        });
                        
                      }
                  });
                 
                }
                 get_feed();

         $(document).on('click','.comment_tab',function() {
              var this_click = $(this);
              var comment_html = '';
              jQuery.ajax({
                      url: "$adminurl"+"fan_wall/ajax_get_comment_feed",
                      method: 'POST',
                      dataType: 'json',
                      data: {
                          "post_id":  $(this).data('post_id')
                      },
                      success: function (data) {
                          var data = data.data;
                          if(data.status) {
                            var cmnt_data = data.data;

                          
                          $.each( cmnt_data , function( key, value ) {
                                if(value.profile_picture != '') { var profile_image = '<img alt=""src="'+ value.profile_picture +'">'; } else { var profile_image =  '<i class="fa fa-user fa-4x"></i>'; }
                              
                              comment_html += '<div class="fb-border fb-gray-bg  fb-status-container"><ul class=fb-comments><li><a href=# class=cmt-thumb>'+profile_image+'</a><div class="cmt-details small"><a href=#>'+value.name+'</a> <span>'+value.comment+'</span><p>'+ timeSince(new Date(parseInt(value.time))) +' ago </div></ul><div class=clearfix></div></div>';  
                          });
                          $(this_click).parent().siblings('.comment_element').html(comment_html);
                      }
                    }

                  });
              
         })


          /* date convert to human readable format */
          function timeSince(date) {
            var seconds = Math.floor((new Date() - date) / 1000);
            var interval = Math.floor(seconds / 31536000);
            if (interval > 1) {
              return interval + " years";
            }
            interval = Math.floor(seconds / 2592000);
            if (interval > 1) {
              return interval + " months";
            }
            interval = Math.floor(seconds / 86400);
            if (interval > 1) {
              return interval + " days";
            }
            interval = Math.floor(seconds / 3600);
            if (interval > 1) {
              return interval + " hours";
            }
            interval = Math.floor(seconds / 60);
            if (interval > 1) {
              return interval + " minutes";
            }
              return Math.floor(seconds) + " seconds";
          }
          /* date convert to human readable format */

              $(document).on('click','.model_popup',function() {
                  var image_src = $('img',this).attr('src');
                  var another_image = $('img',this).parent().parent().html();
                  var html = '<img  class="img-thumbnail"  src="'+image_src+'">';
                  $('.modal-body').html(html);
                  $('.modal-footer').html(another_image);
                  $('.modal-footer a').removeAttr("class");
                  $('.modal-footer a').removeAttr("data-toggle");
                  $('.modal-footer a').removeAttr('href');
                  $('.modal-footer a').css('cursor','pointer');

              }); 

              $(document).on('click','.modal-footer a',function() {
                  
                  var image_src = $('img',this).attr('src');
                  var html = '<img  class="img-thumbnail"  src="'+image_src+'">';
                  $('.modal-body').html(html);
                  
              })

            /* confirm windows box message*/
            function confirm_message() {
              return confirm('are you sure to delete?');
            }

             </script>            

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );