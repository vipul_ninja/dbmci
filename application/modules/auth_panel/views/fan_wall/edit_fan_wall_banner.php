

<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            Edit Banners
            <a href="<?php echo AUTH_PANEL_URL.'fan_wall/fan_wall_banner'; ?>"><button class="pull-right btn btn-info btn-xs bold">Back</button></a>
        </header>
        <div class="panel-body">
            <form role="form" method="post" enctype="multipart/form-data" >
                <p style="color:red;font-size:100%"><?php echo $this->session->flashdata('message'); ?></p>

                <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Banner Title<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter Banner Title" id="exampleInputEmail1" class="form-control" name="banner_title" value="<?php echo $banner_detail['banner_title']; ?>">
                    <span style="color:red"><?php echo form_error('banner_title'); ?></span>
                </div>
                    <?php if (!empty($banner_detail['web_link'])){?>
                <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Web Link<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter Web Link" id="exampleInputEmail1" class="form-control" name="web_link" value="<?php echo $banner_detail['web_link']; ?>">
                    <span style="color:red"><?php echo form_error('web_link'); ?></span>
                </div> 
                    <?php }?>
                <?php if (!empty($banner_detail['course_link'])){?>   
                 <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Course Link<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter course Link" id="exampleInputEmail1" class="form-control" name="course_link" value="<?php echo $banner_detail['course_link']; ?>">
                    <span style="color:red"><?php echo form_error('course_link'); ?></span>
                </div>
                <?php }?>
                <?php if (!empty($banner_detail['study_type'])){?>   
                 <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Study Type<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter study type" id="exampleInputEmail1" class="form-control" name="study_type" value="<?php echo $banner_detail['study_type']; ?>">
                    <span style="color:red"><?php echo form_error('study_type'); ?></span>
                </div>
                <?php }?>

                <div class="form-group col-md-6">
                    <label for="exampleInputPassword1">Text<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter Text" id="exampleInputPassword1" class="form-control" name="text" value="<?php echo $banner_detail['text']; ?>">
                    <span style="color:red"><?php echo form_error('text'); ?></span>
                </div>

                <div class="form-group col-md-6">
                    <label for="exampleInputPassword1">Date Range<sup class="error">* required</sup></label>
                        <div data-date-format="mm/dd/yyyy" data-date="13/07/2013" class="input-group input-large">
                            <input type="text" name="from" class="form-control dpd1" value="<?php echo date("Y-m-d H:m", ($banner_detail['from_date'] / 1000) ); ?>">
                            <span style="color:red"><?php echo form_error('from'); ?></span>
                            <span class="input-group-addon">To</span>
                            <input type="text" name="to" class="form-control dpd2" value="<?php echo date("Y-m-d H:m", ($banner_detail['to_date'] / 1000) ); ?>">
                            <span style="color:red"><?php echo form_error('to'); ?></span>
                        </div>
                        <span class="help-block text-center">Select date range</span>
                </div>

                  <div class="form-group col-md-6">
                        <label for="exampleInputPassword1">Choose File </br>Please upload image with width 640px and height 360px</label><br>
                        <img style="max-height: 150px;" id="img_id" src="<?php echo $banner_detail['image_link']; ?>">
                      
                      
                          <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden">
                            <span class="btn btn-white btn-file">
                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>
                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                            <input type="file" class="default" name="file" id="file">
                            </span>
                              <span style="margin-left:5px;" class="fileupload-preview"></span>
                              <a style="float: none; margin-left:5px;" data-dismiss="fileupload" class="close fileupload-exists" href="#"></a>
                          </div>
                          <span style="color:red"><?php echo form_error('file'); ?></span>
                      
                  </div>

                <div class="form-group col-md-3">
                    <label for="exampleInputPassword1">Priority<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter number" id="exampleInputPassword1" class="form-control" name="priority" value="<?php echo $banner_detail['priority']; ?>">
                    <span style="color:red"><?php echo form_error('priority'); ?></span>
                </div>
				<div class="form-group col-md-3">
                    <label for="exampleInputPassword1">Status</label>
					<?php 
						   $this->db->select('status');						   
					$res = $this->db->get('fan_wall_banner')->result_array();
					// echo('<pre>');
					// print_r($res);					
					?>
                    <select class="form-control" name='status'>
					<?php foreach($res as $sts)?>											
					<option value="0" <?php if($sts['status']==0){echo 'selected';}?> >Enable</option>			
					<option value="1" <?php if($sts['status']==1){echo 'selected';}?> >Disable</option>                    
					</select>
                </div>
                <div class="form-group col-md-6">
                    <label for="exampleInputPassword1">Button Text<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter number" id="exampleInputPassword1" class="form-control" name="button_text" value="<?php echo $banner_detail['button_text']; ?>">
                    <span style="color:red"><?php echo form_error('button_text'); ?></span>
                </div>
				
                <input type="hidden" name="file_upload" value="<?php echo $banner_detail['image_link']; ?>">
                <div class="form-group col-md-12">
                    <button class="btn btn-info" type="submit">Submit</button>
                </div>
                
            </form>

        </div>
    </section>
</div>


<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD

                <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                          $( function() {
                            $( ".dpd1" ).datetimepicker();
                            $( ".dpd2" ).datetimepicker();
                          }); // datepicker closed

                        $('#file').change(function (event) {

                             $("#img_id").attr('src', URL.createObjectURL(event.target.files[0]));
                        });

                    }); //Main document closed

                </script>              

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );