<?php //echo "<pre>";print_r($record);echo "</pre>"; ?>


<div class="col-lg-12">
  <section class="panel">
      <header class="panel-heading">
          Edit Post
      </header>
      <div class="panel-body">
          <form role="form" method="post" enctype="multipart/form-data">
              <div class="form-group">
                  <label for="exampleInputPassword1">Post Text</label>
                  <textarea name="text" class="col-md-12"><?php echo $record['post_data']['text']; ?></textarea>
              </div>
              <div class="form-group">
                  <label for="exampleInputFile">Post Tag</label>
                    <select name="post_tag" class="form-control input-sm m-bot15">
                      <option value="">Select Tag</option>
                      <?php $post_tags = $this->db->get('post_tags')->result_array();
                        foreach ($post_tags as $key => $value) {
                            if($value['text'] == $record['post_tag']) {
                                echo '<option value="'.$value['id'].'" selected="selected">'.$value['text'].'</option>';
                            } else {
                                echo '<option value="'.$value['id'].'">'.$value['text'].'</option>';
                            }
                        }

                      ?>                         
                    </select>
              </div>
              <?php 
                      $img_count = 1;$img_html='';$video_html='';$doc_html='';$pdf_html='';
                      if(!empty($record['post_data']['post_file'])) { 
                      foreach ($record['post_data']['post_file'] as $key => $value) {
                          if($value['file_type'] == 'image') {

                              $img_html .= '<div class="col-lg-3 "><label for="exampleInputFile">Image '.$img_count.'</label><span class="close">&times;</span><img alt="Preview" id="image'.$img_count.'"class="img-responsive img-thumbnail" src="'.$value['link'].'"><input type="file" class="image" name="image'.$img_count.'" value="select file"></div>';
                              $img_count = $img_count+1;

                          } elseif($value['file_type'] == 'video') {
                               $video_html .= '<div class="col-lg-3"><label for="exampleInputFile">Video</label><span class="close">&times;</span><video src="'.$value['link'].'"  width="234" height="140" controls></video><input type="file" name="video" value="select file"></div>';
                          } elseif($value['file_type'] == 'doc') {
                               $doc_html .= '<div class="col-lg-3"><label for="exampleInputFile">Doc</label><span class="close">&times;</span><p class="small bold"><a href="'.$value['link'].'" target="_blank"><i class="fa fa-file  text-info"></i> </a></p><input type="file" name="image1" value="select file"></div>';
                          } elseif($value['file_type'] == 'pdf') {
                               $pdf_html .= '<div class="col-lg-3"><label for="exampleInputFile">Pdf</label><span class="close">&times;</span><p class="small bold"><a href="'.$value['link'].'" target="_blank"><i class="fa fa-file  text-info"></i><b>'.$value['file_info'].'</b> </a></p><input type="file" name="image1" value="select file"></div>';
                          } 
                      }
              }

              /*for($img_count = $img_count; $img_count<=5; $img_count++) {
                $img_html .= '<div class="col-lg-3 "><label for="exampleInputFile">Image '.$img_count.'</label><span class="close">&times;</span><img alt="" id="image'.$img_count.'"class="img-responsive img-thumbnail" src=""><input type="file" class="image" name="image'.$img_count.'" value="select file"></div>';
              }*/

              ?>
              <div class="row image">
                  <?php echo $img_html; ?>
              </div> 
              <div class="row video">
                  <?php echo $video_html; ?>
              </div>
              <div class="row doc">
                  <?php echo $doc_html; ?>
              </div>
              <div class="row pdf">
                  <?php echo $pdf_html; ?>
              </div>
              
              </br>
              <div class="form-group">
                <input type="hidden" name="user_id" value="<?php echo $record['user_id']; ?>">
                <input type="hidden" name="post_id" value="<?php echo $record['id']; ?>">
                <button class="btn btn-info" type="submit">Submit</button>
                <a href="" class="btn btn-warning">Cancel</a>
              </div>
          </form>

      </div>

  </section>
</div>

<?php

$custum_js = <<<EOD
          <script>
              $('.close').on('click',function() {
                  $(this).parent().find('img').attr('src','');
                  $(this).parent().find('img').attr('alt','');
                  $(this).parent().find('img').attr('class','');
                  $(this).parent().find('img').after('<input type="hidden" id="'+ $(this).parent().find('img').attr('id')+'" value="'+ $(this).parent().find('img').attr('id')+'" name="file_remove[]">')
                  $(this).parent().find('span').remove();
                  //$(this).parent().append('<input type="hidden" name="image1">');
              })

              $('.image').on('change',function (event) {
                //alert($(this).siblings('img').attr('id'));
                  var id = $(this).siblings('img').attr('id');
                  $('#'+id).attr('class','img-responsive img-thumbnail');
                  $('#'+id).attr('src', URL.createObjectURL(event.target.files[0]));

              });


              $('.close').on('click',function() {
                  $(this).parent().find('video').attr('src','');
                  $(this).parent().find('video').after('<input type="hidden" id="'+ $(this).parent().find('video').attr('id')+'" value="'+ $(this).parent().find('video').attr('id')+'" name="file_remove[]">')
                  $(this).parent().find('span').remove();
                  
              })


          </script>

EOD;

  echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>