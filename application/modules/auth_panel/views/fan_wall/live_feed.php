 <link href="https://vjs.zencdn.net/7.0.3/video-js.css" rel="stylesheet">
							   <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="reportAbusePop" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Reported By Users</h4>
                                          </div>
										  
                                          <div class="modal-body" id = "report_abuse">
											
                                          </div>
										  
                                      </div>
                                  </div>
                              </div>
                  <div class="">
                  <div class="">
                      <section class="panel">
                          <div class="panel-body">
                              <form  class="form-inline" method="GET" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                  <div class="form-group hide">
                                      <label for="exampleInputEmail2" class="">Tag</label>
                                      <select id="element-tag" name="tag" value="" class="form-control input-xs ">
                                      <?php
                                        
                                        $tags = $this->db->get("post_tags")->result();
                                         echo '<option value="">--Select Tag--</option>';  
                                        foreach($tags as $t){
                                          $selected = '';
                                          if($this->input->get('tag') == $t->id) {
                                            $selected = 'selected';
                                          } 
                                          echo '<option '.$selected.' value="'.$t->id.'" >'.$t->text.'</option>';  
                                          
                                        }
                                      ?>
                                      </select>
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputPassword2" class="">Search </label>
                                      <input id="element-text" name="element-text" type="text" placeholder="Type here " id="exampleInputPassword2" value="<?php echo $this->input->get('element-text'); ?>" class=" input-xs form-control">
                                  </div>
                                  <input type="hidden" name="search_user_post_id" value="<?php echo $this->input->get('search_user_post_id'); ?>">
                                  <input type="hidden" name="page" value="<?php echo $this->input->get('page'); ?>">
                                  <button class="btn btn-success btn-xs" type="submit">Search</button>
                                  <a href="<?php echo AUTH_PANEL_URL.'fan_wall/all_post'; ?>"><button class="btn btn-danger btn-xs" type="button">Clear</button></a>

                                  <?php if($this->input->get('page') == "report_abuse"){ ?>  
                                    <div class="form-group pull-right <?php if($_GET['page'] != "report_abuse"){ echo "hide";}?> ">
                                      <select name="abused_order" onchange="this.form.submit()" class="form-control input-xs" >
                                        <option <?php if(ISSET($_GET['abused_order']) && $_GET['abused_order'] == "latest"){ echo "selected";} ?> value="latest">Lastest Abused</option>
                                        <option <?php if(ISSET($_GET['abused_order']) && $_GET['abused_order'] == "most_abused" ){ echo "selected";} ?> value="most_abused">Most Abused</option>
                                      </select>
                                      <input name="last_abused_segment" type="hidden" value=""/>
                                    </div> 
                                  <?php } ?>
                              </form>

                          </div>
                      </section>

                  </div>
              </div>
<style>
.panel-body {
    background: white none repeat scroll 0 0;
    margin-bottom: 6px;
}

.modal-body img{
    max-height: 400px;
}

.fb-user-status {
    line-height: 15px;
    padding: 3px 0;
}
.fb-user-thumb {
    float: left;
    margin-right: 10px;
    width: 50px;
}
.fb-user-thumb img {
    border-radius: 50%;
    height: 50px;
    width: 50px;
}
.fb-time-action {
    padding: 5px 0;
}
.fb-user-details p {
  color:inherit;      
}
/*
.fb-comments li {
    border-top: 1px solid #ebeef5;
    padding: 5px 15px 0px 15px;
}
*/
.fb-comments .cmt-thumb {
    float: left;
    margin-right: 0px;
    width: 30px;
}
.fb-comments .cmt-details a {
    font-size: 12px;
    font-weight: bold;
}
</style>
<section class=" live_feeder" data-element_id = "">

</section>
<div class="text-center load_spinner " style="display:none" ><i class="text-center fa fa-spinner fa-spin fa-4x"></i></div>

<!--pop up for image show-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal9" class="modal fade modal-dialog-center top-modal-with-space in" style="display: none;">
  <div class="modal-dialog">
      <div class="">
          <div class="modal-content">
              <div class=" hide modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <!--<h4 class="modal-title">Modal1 Tittle</h4>-->
              </div>
              <div class="modal-body text-center">
              
              </div>
              <div class="modal-footer text-center">
                
              </div>
          </div>
      </div>
  </div>
</div>
<!--pop up for image show ends here html pop up model-->


<!-- pop up for show liked users -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade" style="display: none;overflow: auto;">
  <div class="modal-dialog modal-sm">
      <div class="modal-content-wrap">
          <div class="modal-content" >
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h5 class="modal-title bold">Liked User(s)</h5>
              </div>
              <div class="modal-body" style="padding: 0 15px;position: relative;">
                <div class="fb-status-container fb-border fb-gray-bg">
                    <ul data-last_id="0" class="fb-comments all_like_list" style="">
                        


                    </ul>
                </div>
              </div>
              <div class="modal-footer hide">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-warning"> Confirm</button>
              </div>
          </div>

      </div>
  </div>
</div>
<!-- pop up for show liked users -->




<?php
$adminurl = AUTH_PANEL_URL;
$auth_assets = AUTH_ASSETS;
$page = ($this->input->get('page') == "report_abuse")?$this->input->get('page'):"";
$user_id = ($this->input->get('search_user_post_id') != "")?$this->input->get('search_user_post_id'):"";
$tag = ($this->input->get('tag') != "")?$this->input->get('tag'):"";
$element_text = ($this->input->get('element-text') != "")?$this->input->get('element-text'):"";

$abused_order = ($this->input->get('abused_order') != "")?$this->input->get('abused_order'):"";
$last_abused_segment = ($this->input->get('last_abused_segment') != "")?$this->input->get('last_abused_segment'):"";

$custum_js = <<<EOD
            <script type="text/javascript" src="//player.wowza.com/player/latest/wowzaplayer.min.js"></script>
            <script src="https://vjs.zencdn.net/7.0.3/video.js"></script>
             <script>
              $(document).on('click','.like_list , .show_more_like_list',function() {
                if($(this).hasClass( "show_more_like_list" )){
                  var likes_count = $(this).data('likes_count');
                  $(this).hide('slow').remove();
                }
                if($(this).hasClass( "like_list" )){
                 var likes_count = $(this).text();
                 $('.all_like_list').html('');
                }
                var post_id = $(this).data('post_id');
                  
                  likes_count = likes_count.split(' ');
                  if(likes_count[0] > 0) {
                  $('#myModal').modal('show'); 
                  jQuery.ajax({
                        url: "$adminurl"+"fan_wall/ajax_get_user_like_list",
                        method: 'POST',
                        dataType: 'json',
                        data: {

                            "post_id":  $(this).data('post_id'),
                            "last_id": $(this).data('last_like_id'),

                        },
                        success: function (data) {
                          
                          var data = data.data;

                          if(data.status) {

                          var like_data = data.data;
                          var last_like_id =  "";
                          var liked_html = '';
                          $.each( like_data , function( key, value ) {

                                if(value.profile_picture != '') { var profile_image = '<img style="width:30px;height:30px" alt=""src="'+ value.profile_picture +'">'; } else { var profile_image =  '<img style="width:30px;height:30px" alt="" src="$auth_assets'+'img/sample_user.jpg'+'">'; }

                              

                              liked_html += '<li style="padding: 5px;position: relative;"><a href="$adminurl'+'web_user/user_profile/'+value.user_id+'" class="fb-user-thumb">'+ profile_image +'</a><div class=""><a href="$adminurl'+'web_user/user_profile/'+value.user_id+'" class="bold">'+value.name+'</a> <p style="margin: 0;" ><small style="font-size:10px">'+ timeSince(new Date(parseInt(value.creation_time))) +' - ago</small></div>';  
                              post_id = value.post_id;
                              last_like_id = value.id;
                             
                          }); // for each closed here.
                          var button ="";
                          if(data.data.length == 10 && likes_count[0] > 10){
                          var button = '<li style="padding: 5px;"><div class="text-center"><button data-post_id="'+post_id+'" data-last_like_id="'+ last_like_id +'" data-likes_count="'+ likes_count[0] +' likes" type="button" class="btn btn-info btn-xs bold show_more_like_list">Show more</button></li>';
                          }

                          liked_html += button;

                          $('.all_like_list').data('last_id',last_like_id);
                          $('.all_like_list').append(liked_html);

                      } // status if closed here.
                          

                        } // ajax success closed here.
                  }); // ajax call closed here.
                } // like check greater than 0 closed here.
                }); // like list document close here.


                var page = "$page"; 
                var user_id = "$user_id"; 
                var element_text = "$element_text";
                var tag = "$tag";
                $(window).scroll(function() {
                  if($(window).scrollTop() + $(window).height() == $(document).height()) {
                    get_feed();
                  }
                });

                function search_feed(){
                  $('.live_feeder').html('');
                  $(".live_feeder").attr('data-element_id',"");
                  get_feed();
                }

                function get_feed(){
                  abused_filter = '$abused_order';
                  last_abused_segment = $('input[name=last_abused_segment]').val();

                  $('.load_spinner').show();
                  jQuery.ajax({
                      url: "$adminurl"+"fan_wall/ajax_get_feed",
                      method: 'POST',
                      dataType: 'json',
                      data: {
                          "last_id":  $(".live_feeder").attr('data-element_id'),
                          "search_text": element_text,
                          "tag_id": tag,
                          "page":page,
                          "user_id":user_id,
                          "last_abused_segment": last_abused_segment,
                          "abused_filter":abused_filter

                      },
                      success: function (data) {
                        console.log(data.last_chunk);  
                        $('input[name=last_abused_segment]').val(data.last_chunk); 

                        if(!data.status){
                          $('.load_spinner').hide();
                        }
                        var result = data.data;
                        //console.log(result);
                        
                        $.each( result, function( key, value ) {
                          result = value.data;
                          //console.log(result);
                          var shared_text = "";
                          if(result.is_shared == 1){
                            
                            shared_text =  '<p class="col-md-12 row panel"> <strong class="text-primary">'+result.post_owner_info.name+'</strong> shared <strong class="text-info">'+ result.post_shared_of.name+'</strong>\'s post.</p>';
                          }

                          post_type_specification = "";
                          /* by default */
                          var element_post_text = result.post_data.text;

                          if(result.post_type == "user_post_type_current_affair"){
                            post_type_specification = "Current Affair";
                          }
                          if( result.post_type =="user_post_type_article" ){
                            post_type_specification = "Article";
                          }
                          if( result.post_type =="user_post_type_vocab" ){
                            post_type_specification = "Vocab";
                          }       

                          if( result.post_type =="user_post_type_quiz" ){
                            post_type_specification = "Quiz";
                          }   

                          if( result.post_type =="user_post_type_normal" ){
                            post_type_specification = "Feed";
                          }

                          if( result.post_type == "user_post_type_video" ){
                            post_type_specification = "Video";
                          } 
                          if( result.post_type == "user_post_type_mcq" ){
                            post_type_specification = "Mcq";
                          } 
                          if(result.post_type == "user_post_type_current_affair" || result.post_type == "user_post_type_quiz" || result.post_type == "user_post_type_vocab"  || result.post_type == "user_post_type_article"){
                            element_post_text = "<h3 class='bold capitalize'>"+result.post_headline+"</h3>";
                            display_picture = '</br><div class="col-md-3"><img class="thumbnail img-responsive" src="'+result.display_picture+'"></div>';
                            element_post_text = element_post_text +display_picture +  '</br><div class="col-md-7">'+result.post_data.text+'</div>';
                          }

                          if(result.post_type == "user_post_type_normal"){
                            
                            var youtube_link = '';
                            //you tube videos search according matching text in post text.
                            youtube_link = get_youtube_url(element_post_text);

                            if(typeof youtube_link !== "undefined" ) {
                              youtube_link = '<iframe width="400" height="200"src="https://www.youtube.com/embed/'+ youtube_link +'"></iframe>';
                            } else {
                              youtube_link = '';
                            }
                            
                          }else if(result.post_type == "user_post_type_mcq"){
                            var element_post_text = '<span class="badge badge-sm label-warning">Question </span>'+result.post_data.question;
                            if(result.post_data.answer_one != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> A </span> <span class='small'> "+result.post_data.answer_one+'</span>';
                            }
                            if(result.post_data.answer_two != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> B </span> <span class='small'> "+result.post_data.answer_two+'</span>';
                            }
                            if(result.post_data.answer_three != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> C </span> <span class='small'> "+result.post_data.answer_three+'</span>';
                            }
                            if(result.post_data.answer_four != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> D </span> <span class='small' > "+result.post_data.answer_four+'</span>';
                            }
                            if(result.post_data.answer_five != ""){
                              element_post_text = element_post_text+"</br><span class='badge badge-sm label-info'> E </span> <span class='small' > "+result.post_data.answer_five+'</span>';
                            }                                                        
                          }
                          /* code for images */                              
                          file_array = result.post_data.post_file;
                          var images = "";
                          var videos = "";
                          var document = "";
                          $.each( file_array , function( key, value ) {
                            if(value.file_type == "image"){
                              images = images+'<a href="#myModal9" class="model_popup"  data-toggle="modal"><img style="height:100px" class="img-thumbnail"  src="'+value.link+'"> </a>';

                            }

                            if(value.file_type == "video"){
                              videos = videos+ '<div class="col-md-12 text-center"> <video id="my-video" class="video-js" controls preload="auto" width="640" height="264" poster="" data-setup="{}"> <source src="'+value.link+'" type="video/mp4">  <p class="vjs-no-js"> To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="#" >supports HTML5 video</a> </p> </video><!--<video class="col-md-4" controls><source src="'+value.link+'" ></video>--></div><div class="clearfix"></div>';
                            }

                            if(value.file_type == "doc" || value.file_type == "xls" || value.file_type == "pdf" ){
                              document = document + '<p class="small bold" ><a target="_blank" href="'+value.link +'"><i class="fa fa-file  text-info"></i> '+value.file_info+'</a></p>';
                            }
                          });

/* -------------------------------------PROFILE IMAGE ------------------------------------------ */
                          if(result.post_owner_info.profile_picture != '') { var profile_image = '<a href="$adminurl'+'web_user/user_profile/'+result.post_owner_info.id+'"><img alt=""src="'+ result.post_owner_info.profile_picture +'"></a>'; } else { var profile_image =  '<a href="$adminurl'+'web_user/user_profile/'+result.user_id+'"><i class="fa fa-user fa-4x"></i></a>'; }
                            
/* -------------------------------------POST ACTION ------------------------------------------ */    
                          if(result.post_type == "user_post_type_normal"){
                            var send_for_push = btoa(encodeURIComponent(result.post_data.text));
                          }else{
                            var send_for_push = btoa(encodeURIComponent(result.post_data.question));
                          }
                          var post_action = '<div class="btn-group pull-right"><button type="button" class="btn btn-success dropdown-toggle btn-xs" data-toggle="dropdown" ria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="$adminurl'+'post_controller/delete_post/'+ result.id +'" onclick=" return confirm(\'Are you really want to delete this feed? \'); ">Delete</a></li><li><a target="_blank" href="$adminurl'+'bulk_messenger/push_notification/send_push_notification?text='+send_for_push+'&feed_id='+ result.id +'" onclick=" return confirm(\'Do you really want to send push notification for this feed? \'); ">Push Notification</a></li>';
                            if(result.is_shared == 0) { 
                           post_action +='<li><a href="$adminurl'+'daily_dose/current_affair/edit_post?post_id='+ result.id +'">Edit</a></li>';
                           }
                           post_action +='</ul></div>';
/* -------------------------------------POST TAG NAME ------------------------------------------ */
                          if(result.post_tag == '') { var post_tag = ""; } else { var post_tag = " - "+ result.post_tag; }

/* -------------------------------TAGS PEOPLE IN POST ------------------------------------------ */
                          var tags_people = '';
                          if(result.tagged_people) {
                            $.each( result.tagged_people, function( key, value ) {
                              tags_people += '<a href="$adminurl'+'web_user/user_profile/'+value.id+'"><span class="badge bg-info">'+ value.name +'</span> </a>';
                            })
                            if(tags_people != ""){
                              tags_people = '<i class="fa fa-users bg-info"></i> '+ tags_people;
                            }
                          }
                          /************************** set which type of post it is  ***********************/

                          var html ='<div  class=panel-body><div class=fb-user-thumb>'+profile_image+'</div>'+ post_action +'<div class=fb-user-details><h3><a href="$adminurl'+'web_user/user_profile/'+result.user_id+'" class="bold">'+ result.post_owner_info.name +'</a></h3><p class="small bold"> '+ post_type_specification +'-'+ timeSince(new Date(parseInt(result.creation_time))) +' ago</div><div class=clearfix></div>'+shared_text+'<p class="col-md-12">'+ element_post_text +'</p>';+ youtube_link + tags_people;


                         
                         if(videos != ""){
                          html += videos;
                         }

                          if(images != '') {
                            html += '<div class=panel-body><div class=album>'+images+'</div></div>';
                          }

                          if(document != ""){
                             html += document;
                          }

                          html += '<div style="clear:both" class="fb-border fb-status-container"><div class="fb-time-action small" ><a style="cursor:pointer"  class="bold bg-info like_list" data-post_id="'+result.id+'" data-last_like_id="0">'+ result.likes +' likes</a> <span>-</span> <a data-post_id="'+result.id+'" data-last_comment_id="" style="cursor:pointer" class="bold bg-success comment_tab">'+ result.comments +' Comments</a> <span>-</span> <a href=# class="hide">'+ result.share +' Share</a><a class="bold bg-danger" onclick = "report_abuse('+result.id+')" data-toggle="modal" href="#reportAbusePop" >'+ result.report_abuse +' Report abused</a></div> <div class="comment_element_'+result.id+'"></div>   </div></div>';
                          
                          $(".live_feeder").append(html);

                          $(".live_feeder").attr('data-element_id',result.id);
                           $('.load_spinner').hide();
                        });
                        
                      }
                  });
                 
                }
                 get_feed();

/* -------------------------------------DELETE COMMENT ------------------------------------------ */
          $(document).on('click','.delete_comment',function() {
            var cnfrm = confirm('Are you really want to delete this comment?');
            if(cnfrm) {
                var this_click = $(this);
                jQuery.ajax({
                      url: "$adminurl"+"fan_wall/ajax_delete_comment_feed",
                      method: 'POST',
                      dataType: 'json',
                      data: {
                          "id":  $(this).data('comment_id'),
                          "post_id":  $(this).data('post_id'),
                          "user_id":  $(this).data('user_id')
                      },
                      success: function (data) {
                        if(data.data.status) {
                          $('.comment_box_'+$(this_click).data('comment_id')).hide('slow');
                        }
                      }
                });
            }
          })
/* ---------------------------------------  COMMENT ------------------------------------------ */
         $(document).on('click','.comment_tab , .show_more_comment',function() {
              if($(this).hasClass( "show_more_comment" )){
                $(this).hide('slow').remove();
              }
              if($(this).hasClass( "comment_tab" )) {
                $(this).removeClass('comment_tab');
              }
              var this_click = $(this);
              var comment_html = '';
              jQuery.ajax({
                      url: "$adminurl"+"fan_wall/ajax_get_comment_feed",
                      method: 'POST',
                      dataType: 'json',
                      data: {
                          "post_id":  $(this).data('post_id'),
                          "last_comment_id":  $(this).data('last_comment_id'),
                      },
                      success: function (data) {
                          var data = data.data;
                          if(data.status) {

                          var cmnt_data = data.data;
                          var last_post_id = last_comment_id= "";
                          
                          $.each( cmnt_data , function( key, value ) {
                                
                                if(value.profile_picture != '') { var profile_image = '<img  style="width:30px;height:30px;border-radius:50%" alt=""src="'+ value.profile_picture +'">'; } else { var profile_image =  '<img  style="width:30px;height:30px;border-radius:50%" alt="" src="$auth_assets'+'img/sample_user.jpg'+'">'; }

/*------------------------------- PEOPLE TAGGED IN COMMENT -------------------------------------*/

                                  var cmnt_tag_poeple = value.tagged_people;
                                  var cmnt_tag_people_html = '';
                                  if(cmnt_tag_poeple != '') {
                                        cmnt_tag_people_html = '<i class="fa fa-users bg-info"></i> ';
                                    $.each(cmnt_tag_poeple , function(key, value){
                                        cmnt_tag_people_html += '<a href="$adminurl'+'web_user/user_profile/'+value.id+'"><span class="badge bg-info">'+ value.name +'</span> </a>';
                                    }) 
                                  }
/* ---------------------------------------  COMMENT HTML ------------------------------------------ */

                              comment_html += '<div class="fb-border fb-gray-bg  fb-status-container comment_box_'+value.id+'"><ul class=fb-comments><li><a href="$adminurl'+'web_user/user_profile/'+value.user_id+'" class=cmt-thumb>'+profile_image+'</a><div class="cmt-details small col-md-9"><a href="$adminurl'+'web_user/user_profile/'+value.user_id+'">'+value.name+'</a> <span class="comment_value"  id = "comment_span_'+value.id+'" data-state="">'+value.comment+'</span><p>'+ timeSince(new Date(parseInt(value.time))) +' ago </p>'+ cmnt_tag_people_html +'</div><div class="cmt-details small col-md-2 pull-right"><button ria-expanded="false" data-toggle="dropdown" class="btn btn-danger  btn-xs pull-right delete_comment" type="button" data-comment_id = "'+value.id+'" data-post_id="'+value.post_id+'" data-user_id = "'+value.user_id+'"><i class="fa fa-trash-o"></i></button><button style="position:relative;right:5%" ria-expanded="false" data-toggle="dropdown" class="btn btn-success  btn-xs pull-right edit_comment" id="edit_comment_'+value.id+'" type="button" data-comment_id = "'+value.id+'" data-post_id="'+value.post_id+'" data-user_id = "'+value.user_id+'"><i class="fa fa-edit" ></i></button></div></ul><div class=clearfix></div></div>';  
                              last_post_id = value.post_id;
                              last_comment_id = value.id;
                          });
/* --------------------------------  COMMENT SHOW MORE BUTTON HTML -----------------------------------*/

                          var button ="";
                          if(comment_html != ''){
                            button ='<div class="col-md-12 text-center"><button data-post_id = "'+last_post_id+'" data-last_comment_id = "'+last_comment_id+'" class="btn btn-xs btn-info show_more_comment">Show more</button></div>';
                          }
                          comment_html += button;
                          $('.comment_element_'+last_post_id).append(comment_html);
                      }
                    }
                  });



              $(document).on('click','.edit_comment',function() {
                
                $(this).hide();
                var id = $(this).data('comment_id');
                var user_id = $(this).data('user_id');
                var post_id = $(this).data('post_id');
                var val = $('#comment_span_'+id).text();
                var state = $('#comment_span_'+id).data('state');
                
                if(state != "active" ){
                   
                  $('#comment_span_'+id).data('state','active');
                  $('#comment_span_'+id).html('<input  class="form-control" type="text" name="commnet_text" value="'+val+'"><button data-user_id='+user_id+' data-post_id='+post_id+' data-comment_id='+id+' class="save_edited_comment btn btn-success btn-sm bold">Save</button>');                  
                }
              })


              $(document).on('click','.save_edited_comment',function() {
                var id = $(this).data('comment_id');
                var user_id = $(this).data('user_id');
                var post_id = $(this).data('post_id');
                var text = $(this).prev('input').val();
                if($('#comment_span_'+id).data('state') == 'active') {
                jQuery.ajax({
                      url: "$adminurl"+"fan_wall/ajax_edit_comment_feed",
                      method: 'POST',
                      dataType: 'json',
                      async:false,
                      data: {
                          "id":  id,
                          "post_id":  post_id,
                          "user_id":  user_id,
                          "text":text
                      },
                      success: function (data) {
                        console.log(data);
                        if(data.data.status) {
                         $('#comment_span_'+id).html(text);
                         $('#edit_comment_'+id).show('slow');
                          $('#comment_span_'+id).data('state','');
                        }
                      }
                });
              }
              })
              
              
         })


          /* date convert to human readable format */
          function timeSince(date) {
            var seconds = Math.floor((new Date() - date) / 1000);
            var interval = Math.floor(seconds / 31536000);
            if (interval > 1) {
              return interval + " years";
            }
            interval = Math.floor(seconds / 2592000);
            if (interval > 1) {
              return interval + " months";
            }
            interval = Math.floor(seconds / 86400);
            if (interval > 1) {
              return interval + " days";
            }
            interval = Math.floor(seconds / 3600);
            if (interval > 1) {
              return interval + " hours";
            }
            interval = Math.floor(seconds / 60);
            if (interval > 1) {
              return interval + " minutes";
            }
              return Math.floor(seconds) + " seconds";
          }
          /* date convert to human readable format */

              $(document).on('click','.model_popup',function() {
                  var image_src = $('img',this).attr('src');
                  var another_image = $('img',this).parent().parent().html();
                  var html = '<img  class="img-thumbnail"  src="'+image_src+'">';
                  $('.modal-body').html(html);
                  $('.modal-footer').html(another_image);
                  $('.modal-footer a').removeAttr("class");
                  $('.modal-footer a').removeAttr("data-toggle");
                  $('.modal-footer a').removeAttr('href');
                  $('.modal-footer a').css('cursor','pointer');

              }); 

              $(document).on('click','.modal-footer a',function() {
                  
                  var image_src = $('img',this).attr('src');
                  var html = '<img  class="img-thumbnail"  src="'+image_src+'">';
                  $('.modal-body').html(html);
                  
              })

  function get_youtube_url(str){
    const regex = /(?:https?:\/\/)?(?:www.)?(?:youtube.com|youtu.be)\/(?:watch\?)?v=([^\s]+)/m;
                if ((m = regex.exec(str)) !== null) {
                // The result can be accessed through the `m`-variable.
                return  m[1];
                }
              }
			  
			  function report_abuse(id){
			  
			  	  jQuery.ajax({
                      url: "$adminurl"+"fan_wall/get_post_report_abused_by_id/"+id,
                      method: 'POST',
                      dataType: 'json',
                      async:false,
                      data: {
                                           
                      },
                      success: function (data) {
                        console.log(data);
                        if(data) {
                         $('#report_abuse').html('');
						 $.each( data , function( key, value ) {
						 	var report_abuse_data = '<div class="room-box" ><h5 class="text-primary"><a href="#"><strong>'+value.name+'</strong></a></h5><p><strong>Reason :</strong> '+value.reason+'</p><p><strong>Comment : </strong>'+value.comment+'</p></div>';
											  $('#report_abuse').append(report_abuse_data);
						 })
                        
                        }
                      }
                });
			  }
			  
			  
             </script>            

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
