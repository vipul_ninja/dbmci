
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            Add Banners
        </header>
        <div class="panel-body">
            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo AUTH_PANEL_URL.'fan_wall/fan_wall_banner'; ?>">
                <p style="color:red;font-size:100%"><?php echo $this->session->flashdata('message'); ?></p>
                <div class="col-md-12">
                <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Banner Title<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter Banner Title" id="exampleInputEmail1" class="form-control" name="banner_title" value="<?php echo set_value('banner_title'); ?>">
                    <span style="color:red"><?php echo form_error('banner_title'); ?></span>
                </div>
                <div class="form-group col-md-3 ">
                    <label for="exampleInputEmail1">Link type<sup class="error">* required</sup></label>
                    <select class="form-control" name="link_type">
                         <option value="0" <?php echo set_select('link_type', '0', TRUE); ?> >web link </option>
                         <option value="1" <?php echo set_select('link_type', '1', TRUE); ?> >course link </option>
                         <option value="2" <?php echo set_select('link_type', '2', TRUE); ?> >Study Type </option>
                    </select>
                </div>

                <div class="form-group col-md-3 web_link_element " id="web_link">
                    <label for="exampleInputEmail1">Web Link<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter Web Link" id="exampleInputEmail1" class="form-control" name="web_link" value="<?php echo set_value('web_link'); ?>" >
                    <span style="color:red"><?php echo form_error('web_link'); ?></span>
                </div>
                <div class="form-group col-md-3 course_link_element hide" id="course_link" >
                    <label for="exampleInputEmail1">course link<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter course link" id="exampleInputEmail1" class="form-control" name="course_link" value="<?php echo set_value('course_link'); ?>">
                    <span style="color:red"><?php echo form_error('course_link'); ?></span>
                </div>
                <div class="form-group col-md-3 study_type_element " id="study_type" >
                    <label for="exampleInputEmail1">Study Type<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter study type" id="exampleInputEmail1" class="form-control" name="study_type" value="<?php echo set_value('study_type'); ?>">
                    <span style="color:red"><?php echo form_error('study_type'); ?></span>
                </div>

                </div>
                <div class="form-group col-md-6">
                    <label for="exampleInputPassword1">Text<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter Text" id="exampleInputPassword1" class="form-control" name="text" value="<?php echo set_value('text'); ?>">
                    <span style="color:red"><?php echo form_error('text'); ?></span>
                </div>

                <div class="form-group col-md-6">
                    <label for="exampleInputPassword1">Date Range<sup class="error">* required</sup></label>
                        <div data-date-format="mm/dd/yyyy" data-date="13/07/2013" class="input-group input-large">
                            <input type="text" name="from" class="form-control dpd1" value="<?php echo set_value('from'); ?>">
                            <span style="color:red"><?php echo form_error('from'); ?></span>
                            <span class="input-group-addon">To</span>
                            <input type="text" name="to" class="form-control dpd2" value="<?php echo set_value('to'); ?>">
                            <span style="color:red"><?php echo form_error('to'); ?></span>
                        </div>
                        <span class="help-block text-center">Select date range</span>
                </div>

                  <div class="form-group  col-md-6">
                      <label for="exampleInputPassword1">Choose File <sup class="error">* required</sup> </br>Please upload image with width 640px and height 360px</label>
                      
                          <div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden">
                            <span class="btn btn-white btn-file">
                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>
                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                            <input type="file" class="default" name="file">
                            </span>
                              <span style="margin-left:5px;" class="fileupload-preview"></span>
                              <a style="float: none; margin-left:5px;" data-dismiss="fileupload" class="close fileupload-exists" href="#"></a>
                          </div>
                          <span style="color:red"><?php echo form_error('file'); ?></span>
                      
                  </div>
                <div class="form-group col-md-3">
                    <label for="exampleInputPassword1">Priority<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter number" id="exampleInputPassword1" class="form-control" name="priority" value="<?php echo set_value('priority'); ?>">
                    <span style="color:red"><?php echo form_error('priority'); ?></span>
                </div>
                <div class="form-group col-md-3">
                    <label for="exampleInputPassword1">Button Text<sup class="error">* required</sup></label>
                    <input type="text" placeholder="Enter number" id="exampleInputPassword1" class="form-control" name="button_text" value="<?php echo set_value('button_text'); ?>">
                    <span style="color:red"><?php echo form_error('button_text'); ?></span>
                </div>
                   <div class="col-md-12">
                    <button class="btn btn-info" type="submit">Submit</button>
                 </div>
            </form>

        </div>
    </section>
</div>


              
<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		FAN WALL BANNER
		</header>
		<div class="panel-body">
		<div class="adv-table">		   

		<table  class="display table table-bordered table-striped" id="all-post-grid">
  		<thead>
    		<tr>
				<th>#</th>
        <th>Title</th>
        <th >Text</th>
		<th>Clicks</th>
        <th>Banner</th>
		<th>Priority</th>
		<th>Status</th>
        <th>Action</th>       
    		</tr>
  		</thead>
		</table>
		</div>
		</div>
	</section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD

               <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                       var table = 'all-post-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"fan_wall/ajax_all_fan_wall_banner/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       }); //data table closed

                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       });
              				 $('.search-input-select').on( 'change', function () {   // for select box
              					    var i =$(this).attr('data-column');
              					    var v =$(this).val();
              					    dataTable.columns(i).search(v).draw();
              					});


              $( function() {
                $( ".dpd1" ).datetimepicker();
                $( ".dpd2" ).datetimepicker();
              }); // datepicker closed


          }); //Main document closed
               </script>     
               
               <script>
                    $('select[name=link_type]').on('change', function() {
                        var link= $(this).val();
                        
                        if(link =='1'){
                            $("#web_link").hide(); 
                            $("#study_type").hide();
                            $("#course_link").removeClass("hide");
                            $("#course_link").show();  
                        }
                        if(link =='0'){
                            $("#course_link").hide(); 
                            $("#study_type").hide();
                            //$("#course_link").removeClass("hide");
                            $("#web_link").show();  
                        }
                        if(link =='2'){
                           
                            $("#web_link").hide(); 
                            $("#course_link").hide(); 
                            $("#study_type").show();  
                        }
                    }).change();
               </script>       

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );
