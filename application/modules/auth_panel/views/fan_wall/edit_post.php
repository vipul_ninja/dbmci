<?php //echo "<pre>";print_r($record);echo "</pre>"; ?>
<div class="col-lg-12">
  <section class="panel">
      <header class="panel-heading">
          Edit Post
      </header>
      <?php if($record['post_type'] == 'user_post_type_normal') { ?>
      <div class="panel-body">
          <form role="form" method="post" enctype="multipart/form-data">
              <div class="form-group">
                  <label for="exampleInputEmail1">Post Type</label>
                    <select class="form-control input-sm m-bot15" name="post_type"  id="post_type">
                      <option value="normal_post" selected>Normal text</option>
                      <option value="mcq_post">Post MCQ</option>
                    </select>
              </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">Post Text</label>
                  <textarea name="text" class="col-md-12"><?php echo $record['post_data']['text']; ?></textarea>
              </div>
              <div class="form-group">
                  <label for="exampleInputFile">Post Tag</label>
                    <select name="post_tag" class="form-control input-sm m-bot15">
                      <option value="">Select Tag</option>
                      <?php $post_tags = $this->db->get('post_tags')->result_array(); 
                        foreach ($post_tags as $key => $value) {
                            if($value['id'] == $result['post_tag']) {
                                echo '<option value="'.$value['id'].'" selected>'.$value['text'].'</option>';
                            } else {
                                echo '<option value="'.$value['id'].'">'.$value['text'].'</option>';
                            }
                        }

                      ?>                         
                    </select>
              </div>
              <?php 
                      $img_count = 1;$img_html='';$video_html='';$doc_html='';$pdf_html='';
                      if(!empty($record['post_data']['post_file'])) { 
                      foreach ($record['post_data']['post_file'] as $key => $value) {
                          if($value['file_type'] == 'image') {

                              $img_html .= '<div class="col-lg-3"><label for="exampleInputFile">Image '.$img_count.'</label><img alt="Preview" class="img-responsive img-thumbnail" src="'.$value['link'].'"><input type="file" name="image'.$img_count.'" value="select file"></div>';
                              $img_count = $img_count+1;

                          } elseif($value['file_type'] == 'video') {
                               $video_html .= '<div class="col-lg-3"><label for="exampleInputFile">Video</label><video src="'.$value['link'].'"  width="320" height="240" controls></video><input type="file" name="image1" value="select file"></div>';
                          } elseif($value['file_type'] == 'doc') {
                               $doc_html .= '<div class="col-lg-3"><label for="exampleInputFile">Doc</label><p class="small bold"><a href="'.$value['link'].'" target="_blank"><i class="fa fa-file  text-info"></i> </a></p><input type="file" name="image1" value="select file"></div>';
                          } elseif($value['file_type'] == 'pdf') {
                               $pdf_html .= '<div class="col-lg-3"><label for="exampleInputFile">Pdf</label><p class="small bold"><a href="'.$value['link'].'" target="_blank"><i class="fa fa-file  text-info"></i><b>'.$value['file_info'].'</b> </a></p><input type="file" name="image1" value="select file"></div>';
                          } 
                      }
              }

              ?>
              <div class="row image">
                  <?php echo $img_html; ?>
              </div> 
              <div class="row video">
                  <?php echo $video_html; ?>
              </div>
              <div class="row doc">
                  <?php echo $doc_html; ?>
              </div>
              <div class="row pdf">
                  <?php echo $pdf_html; ?>
              </div>
              
              </br>
              <div class="form-group">
                <button class="btn btn-info" type="submit">Submit</button>
              </div>
          </form>

      </div>



      <!-- Post McQ Start from here  -->
      <?php } elseif($record['post_type'] == 'user_post_type_mcq') { ?>
        <div class="panel-body">
          <form role="form" method="post" enctype="multipart/form-data">
              <div class="form-group">
                  <label for="exampleInputEmail1">Post Type</label>
                    <select class="form-control input-sm m-bot15" name="post_type"  id="post_type">
                           <option value="normal_post">Normal text</option>
                           <option value="mcq_post" selected>Post MCQ</option>
                    </select>
              </div>
              
              <div class="form-group">
                  <label for="exampleInputFile">Post Tag</label>
                    <select name="post_tag" class="form-control input-sm m-bot15">
                      <option value="">Select Tag</option>
                      <?php $post_tags = $this->db->get('post_tags')->result_array(); 
                        foreach ($post_tags as $key => $value) {
                            if($value['id'] == $result['post_tag']) {
                                echo '<option value="'.$value['id'].'" selected>'.$value['text'].'</option>';
                            } else {
                                echo '<option value="'.$value['id'].'">'.$value['text'].'</option>';
                            }
                        }

                      ?>                         
                    </select>
              </div>

              <div class="form-group">
                  <label for="exampleInputPassword1">Question</label>
                  <textarea name="text" class="col-md-12"><?php echo $record['post_data']['question']; ?></textarea>
              </div>

              <div class="form-group has-success">
                <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">A</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputSuccess" name="answer_one" value="<?php echo $record['post_data']['answer_one']; ?>">
                </div>
              </div>
              <div class="form-group has-warning">
                <label class="col-sm-2 control-label col-lg-2" for="inputWarning">B</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputWarning" name="answer_two" value="<?php echo $record['post_data']['answer_two']; ?>">
                </div>
              </div>
              <div class="form-group has-error">
                <label class="col-sm-2 control-label col-lg-2" for="inputError">C</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputError" name="answer_three" value="<?php echo $record['post_data']['answer_three']; ?>">
                </div>
              </div>  
              <div class="form-group has-error">
                <label class="col-sm-2 control-label col-lg-2" for="inputError">D</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputError" name="answer_four" value="<?php echo $record['post_data']['answer_four']; ?>">
                </div>
              </div>  
              <div class="form-group has-error">
                <label class="col-sm-2 control-label col-lg-2" for="inputError">E</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputError" name="answer_five" value="<?php echo $record['post_data']['answer_five']; ?>">
                </div>
              </div>



              <?php 
                      $img_count = 1;$img_html='';$video_html='';$doc_html='';$pdf_html='';
                      if(!empty($record['post_data']['post_file'])) { 
                      foreach ($record['post_data']['post_file'] as $key => $value) {
                          if($value['file_type'] == 'image') {

                              $img_html .= '<div class="col-lg-3"><label for="exampleInputFile">Image '.$img_count.'</label><img alt="Preview" class="img-responsive img-thumbnail" src="'.$value['link'].'"><input type="file" name="image'.$img_count.'" value="select file"></div>';
                              $img_count = $img_count+1;

                          } elseif($value['file_type'] == 'video') {
                               $video_html .= '<div class="col-lg-3"><label for="exampleInputFile">Video</label><video src="'.$value['link'].'"  width="320" height="240" controls></video><input type="file" name="image1" value="select file"></div>';
                          } elseif($value['file_type'] == 'doc') {
                               $doc_html .= '<div class="col-lg-3"><label for="exampleInputFile">Doc</label><p class="small bold"><a href="'.$value['link'].'" target="_blank"><i class="fa fa-file  text-info"></i> </a></p><input type="file" name="image1" value="select file"></div>';
                          } elseif($value['file_type'] == 'pdf') {
                               $pdf_html .= '<div class="col-lg-3"><label for="exampleInputFile">Pdf</label><p class="small bold"><a href="'.$value['link'].'" target="_blank"><i class="fa fa-file  text-info"></i><b>'.$value['file_info'].'</b> </a></p><input type="file" name="image1" value="select file"></div>';
                          } 
                      }
              }

              ?>
              <div class="row image">
                  <?php echo $img_html; ?>
              </div> 
              <div class="row video">
                  <?php echo $video_html; ?>
              </div>
              <div class="row doc">
                  <?php echo $doc_html; ?>
              </div>
              <div class="row pdf">
                  <?php echo $pdf_html; ?>
              </div>
              
              </br>
              <div class="form-group">
                <button class="btn btn-info" type="submit">Submit</button>
              </div>
          </form>

      </div>

      <?php } ?>
  </section>
</div>