<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Refer_code extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation');
		$this->load->model("refer_code_model");
	}
	public function index() {
		error_reporting(0);
		if($this->input->post()) { 
			
			foreach($_POST as $k=>$v){
				set_db_meta_key($k,$v);
			}

			page_alert_box('success','Points Updated','Points updated successfully');
			backend_log_genration($this->session->userdata('active_backend_user_id'),'Updated the points.',
							'POINTS');
		}
		$data['page_data'] = $this->load->view('refer_code/refer_code', array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	
}
