<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class All_payment_reports extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation');
	}

  private function validation(){
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

			if($_POST['from_date'] == "" &&  $_POST['to_date'] == "" ){
				$this->form_validation->set_rules('filter', 'Filter', 'required');
			}

			if($_POST['filter'] != "" and $_POST['filter'] == "DATE_RANGE" ){
				$this->form_validation->set_rules('from_date', 'From date', 'required');
				$this->form_validation->set_rules('to_date', 'To date', 'required');
			}elseif($_POST['filter'] != "" and $_POST['filter'] == "INVOICENO" ){
				$this->form_validation->set_rules('id', 'Invoice no.', 'required');
			}

    if($this->form_validation->run($this) == FALSE ){
      return false;
    }
    return true;
  }


	public function index() {
    if($_POST){

      if($this->validation() != false && $this->input->post('csv')){
        $this->csv();
      }

    }
		$data['page_data'] = $this->load->view('reports/all_transaction', array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

  function csv(){
    //print_r($_POST);die;
    $this->load->dbutil();
    $this->load->helper('file');
    $this->load->helper('download');
    $where = "";
    /* the time filers starts here */
    $filter = $this->input->post('filter');
    if($filter == "TODAY"){
      $date = date('Y-m-d');
      $where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) = UNIX_TIMESTAMP('$date') " ;
    }elseif($filter == "YESTERDAY"){
      $date = date('Y-m-d', strtotime('-1 days', strtotime(date('Y-m-d'))));;
      $where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) = UNIX_TIMESTAMP('$date') " ;
    }elseif($filter == "MONTH"){
      $from_date = date('Y-m-01');
      $to_date = date('Y-m-t');
      $where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) >= UNIX_TIMESTAMP('$from_date') AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) <= (UNIX_TIMESTAMP('$to_date')+86399 ) " ;
    }elseif($filter == "YEAR"){
      $from_date = date('Y-01-01');
      $to_date = date('Y-12-31');
      $where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) >= UNIX_TIMESTAMP('$from_date') AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) <= (UNIX_TIMESTAMP('$to_date')+86399 ) " ;
    }elseif($this->input->post('from_date') != "" && $this->input->post('to_date') != "" ){
      $from_date = DateTime::createFromFormat('d-m-Y', $this->input->post('from_date'));
      $from_date = $from_date->format('Y-m-d');

      $to_date = DateTime::createFromFormat('d-m-Y', $this->input->post('to_date'));
      $to_date = $to_date->format('Y-m-d');

      $where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) >= UNIX_TIMESTAMP('$from_date') AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) <= (UNIX_TIMESTAMP('$to_date')+86399 ) " ;
    }
    /* the time filers end  here */
    /* transaction type filter starts here */
    if($this->input->post('transaction_status') == 1 ){
      $where .=  " and ctr.transaction_status = 1 ";
    }elseif($this->input->post('transaction_status') == 2 ){
      $where .=  " and ctr.transaction_status = 2 ";
    }
    /* transaction type filter end  here */

    /* course price filter start  here */
    if($this->input->post('course_type') == "FREE"){
      $where .=  " and ctr.course_price <= 0 ";
    }elseif($this->input->post('course_price') == "PAID" ){
      $where .=  " and ctr.course_price > 0 ";
    }
    /* course price filter end  here */
		/* direct with invoice */
		if($this->input->post('id') != "" ){
			$where .=  " and ctr.id = ".$this->input->post('id')." ";
		}
    $query = "SELECT ctr.id as invoice_no , course_price ,
              u.name as user_name , u.email as email ,CONCAT (u.c_code, u.mobile) as mobile ,
              bu.username as instructor_name ,
              cm.title as course_name ,
              DATE_FORMAT(FROM_UNIXTIME(SUBSTR(ctr.creation_time,1,10)), '%d-%m-%Y %H:%i:%s') as date
              from course_transaction_record as ctr
              left join users as u on u.id = ctr.user_id
              left join backend_user as bu on bu.id = ctr.instructor_id
              left join course_master as cm on cm.id = ctr.course_id
              where 1 =1
              $where
              ";

    $query = $this->db->query($query);
    $delimiter = ",";
    $newline = "\r\n";
    $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
    force_download('CSV_Report'.date('d-m-Y_H-i-s').'.csv', $data);
   }

	 public function ajax_all_report_list(){
		 		// storing  request (ie, get/post) global array to a variable
		 		$requestData = $_REQUEST;

		 		$columns = array(
		 			// datatable column index  => database column name
		 			0 => 'invoice_no',
		 			1 => 'course_price',
					1 => 'user_name',
		 			3 => 'email',
		 			4 => 'mobile',
		 			5 => 'instructor_name',
		 			6 => 'course_name',
					7 => 'date'
		 		);

				$where = "where 1=1 ";

				$filter = $this->input->get('filter');
				if($filter == "TODAY"){
					$date = date('Y-m-d');
					$where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) = UNIX_TIMESTAMP('$date') " ;
				}elseif($filter == "YESTERDAY"){
					$date = date('Y-m-d', strtotime('-1 days', strtotime(date('Y-m-d'))));;
					$where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) = UNIX_TIMESTAMP('$date') " ;
				}elseif($filter == "MONTH"){
					$from_date = date('Y-m-01');
					$to_date = date('Y-m-t');
					$where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) >= UNIX_TIMESTAMP('$from_date') AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) <= (UNIX_TIMESTAMP('$to_date')+86399 ) " ;
				}elseif($filter == "YEAR"){
					$from_date = date('Y-01-01');
					$to_date = date('Y-12-31');
					$where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) >= UNIX_TIMESTAMP('$from_date') AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) <= (UNIX_TIMESTAMP('$to_date')+86399 ) " ;
				}elseif($this->input->get('from_date') != "" && $this->input->get('to_date') != "" ){
					$from_date = DateTime::createFromFormat('d-m-Y', $this->input->get('from_date'));
					$from_date = $from_date->format('Y-m-d');

					$to_date = DateTime::createFromFormat('d-m-Y', $this->input->get('to_date'));
					$to_date = $to_date->format('Y-m-d');

					$where .=" AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) >= UNIX_TIMESTAMP('$from_date') AND  CAST(SUBSTR(ctr.creation_time,1,10) AS UNSIGNED ) <= (UNIX_TIMESTAMP('$to_date')+86399 ) " ;
				}
				/* the time filers end  here */
				/* transaction type filter starts here */
				if($this->input->get('transaction_status') == 1 ){
					$where .=  " and ctr.transaction_status = 1 ";
				}elseif($this->input->get('transaction_status') == 2 ){
					$where .=  " and ctr.transaction_status = 2 ";
				}
				/* transaction type filter end  here */

				/* course price filter start  here */
				if($this->input->get('course_type') == "FREE"){
					$where .=  " and ctr.course_price <= 0 ";
				}elseif($this->input->get('course_price') == "PAID" ){
					$where .=  " and ctr.course_price > 0 ";
				}
				/* course price filter end  here */
				/* direct with invoice */
				if($this->input->get('id') != "" ){
					$where .=  " and ctr.id = ".$this->input->get('id')." ";
				}

		 		$query = "SELECT ctr.id as total
		              from course_transaction_record as ctr
		              left join users as u on u.id = ctr.user_id
		              left join backend_user as bu on bu.id = ctr.instructor_id
		              left join course_master as cm on cm.id = ctr.course_id
		              $where
		 								";
		 		$query = $this->db->query($query);
		 		$query = $query->row_array();
		 		$totalData = (count($query) > 0) ? $query['total'] : 0;
		 		$totalFiltered = $totalData;

		 		$sql = "SELECT ctr.id as invoice_no , course_price ,
		              u.name as user_name , u.email as email ,CONCAT (u.c_code, u.mobile) as mobile ,
		              bu.username as instructor_name ,
		              cm.title as course_name ,
		              DATE_FORMAT(FROM_UNIXTIME(SUBSTR(ctr.creation_time,1,10)), '%d-%m-%Y %H:%i:%s') as date
		              from course_transaction_record as ctr
		              left join users as u on u.id = ctr.user_id
		              left join backend_user as bu on bu.id = ctr.instructor_id
		              left join course_master as cm on cm.id = ctr.course_id
		              $where  ";

		 		// getting records as per search parameters
		 		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
		 			$sql.=" AND ctr.id  LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		 		}
		 		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
		 			$sql.=" AND course_price LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		 		}
				if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
		 			$sql.=" AND u.name LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		 		}
		 		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
		 			$sql.=" AND u.email LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		 		}
		 		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
		 			$sql.=" AND CONCAT (u.c_code, u.mobile) LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		 		}
		 		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
		 			$sql.=" AND bu.username  LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		 		}
		 		if (!empty($requestData['columns'][6]['search']['value']) ) {  //salary
		 			$sql.=" AND cm.title LIKE '" . $requestData['columns'][6]['search']['value'] . "%' ";
		 		}

		 		$query = $this->db->query($sql)->result();

		 		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		 		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		 		$result = $this->db->query($sql)->result();
		 		$data = array();
		 		foreach ($result as $r) {  // preparing an array
		 			$nestedData = array();
		 			$nestedData[] = $r->invoice_no;
		 			$nestedData[] = $r->course_price;
					$nestedData[] = $r->user_name;
					$nestedData[] = $r->email;
		 			$nestedData[] = $r->mobile;
		 			$nestedData[] = $r->instructor_name;
					$nestedData[] = $r->course_name;
		 			$nestedData[] = $r->date;
		 			$data[] = $nestedData;
		 		}
		 		$json_data = array(
		 			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
		 			"recordsTotal" => intval($totalData), // total number of records
		 			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
		 			"data" => $data   // total data array
		 		);
		 		echo json_encode($json_data);  // send data as json format
	 }



}
