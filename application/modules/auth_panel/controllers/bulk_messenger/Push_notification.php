<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Push_notification extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation');
		$this->load->helper("push_helper");
	}


	public function send_push_notification(){
		if($_POST && $_POST['notification_type'] == 'custom') {
			$this->form_validation->set_rules('message', 'Message', 'required');
			if ($this->form_validation->run() != FALSE) {
				
				if($this->input->post('device_type') == 1){
					/* android */
					$token = $this->input->post('device_token');
					$device = "android";
					generatePush($device, $token, $this->input->post('message'));
				}
				if($this->input->post('device_type') == 2){
					/* ios */
					$token = $this->input->post('device_token');
					$device = "ios";
					generatePush($device, $token, $this->input->post('message'));
				}

				$data['page_toast'] = 'Message sent successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}
			$this->session->set_flashdata('error', 'custom_error');
		}

		$view_data['page'] = 'push_notification';
		$data['page_data'] = $this->load->view('bulk_messenger/push_notification', $view_data , TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

}