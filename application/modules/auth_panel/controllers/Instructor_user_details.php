<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Instructor_user_details extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->model('instructor_model');
		$this->load->library('form_validation', 'uploads');
		$this->load->helper('message_sender_helper');
	}

	public function all_instructors_list() {
		
		($this->input->get('user') == 'instructor') ? $view_data['page'] = 'instructor' : '';		
		$data['page_data'] = $this->load->view('instructor_user_details/all_instructors', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_all_instructors_list($device_type) {
		$where = "";
		
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'username',
			2 => 'email',
			3 => 'mobile',
			4 => 'status',
			5 => 'creation_time',
		);

		$query = "SELECT count(id) as total
				  FROM backend_user where status != 2  and instructor_id = 1
				  $where
				 ";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT id,username,email,mobile,DATE_FORMAT(FROM_UNIXTIME(creation_time), '%d-%m-%Y %h:%i:%s') as creation_time,status FROM  backend_user  where  instructor_id = 1 and status != 2 $where";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND username LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND email LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND mobile LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		$status = $requestData['columns'][4]['search']['value'] ; 
		if ($status ==  1 ) {
			$sql.=" AND status = '0' ";
		}
		if($status > 1 ){
			$sql.=" AND status > 0 ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])  ) {  //salary
			$date = explode(',',$requestData['columns'][5]['search']['value']);
			$start = strtotime($date[0])*1000;
			$end = (strtotime($date[1])*1000)+86400000;
			$sql.="  AND  creation_time >= '$start' and creation_time <= '$end'";
		}
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();
		//echo $this->db->last_query();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->username;
			$nestedData[] = $r->email;
			$nestedData[] = $r->mobile;

			$nestedData[] = ($r->status == 0 )?'<span class="btn btn-xs bold btn-success">Active</span>':'<span class="btn btn-xs bold btn-danger">Disabled</span>';
			$nestedData[] = $r->creation_time;
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "instructor_user_details/instructor_profile/" . $r->id . "'>View</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function instructor_profile($id) {
		if($this->input->post()) {

			$this->form_validation->set_rules('u_name', 'Beneficiary name', 'required');
			$this->form_validation->set_rules('b_account', 'Beneficiary account', 'required');
			$this->form_validation->set_rules('b_ifsc', 'Bank IFSC', 'required');
			$this->form_validation->set_rules('b_name', 'Bank name', 'required');
			$this->form_validation->set_rules('b_address', 'Bank address', 'required');
			$this->form_validation->set_rules('r_sharing', 'Resource sharing', 'required');

			if ($this->form_validation->run() == FALSE) {

            }

			$update = array('u_name' => $this->input->post('u_name'),'b_account' => $this->input->post('b_account'),'b_ifsc' => $this->input->post('b_ifsc'),'b_name' => $this->input->post('b_name'),'b_address' => $this->input->post('b_address'),'r_sharing' => $this->input->post('r_sharing'));
			//print_r($insert_data); die;

			$this->db->where('user_id',$id);
            $this->db->update('course_instructor_information',$update);

			$data['page_toast'] = 'Information updated successfully.';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
		}

		$view_data['user_data'] = $this->instructor_model->get_instructor_profile($id);
		$view_data['user_instructor_data'] = $this->instructor_model->get_user_instructor_details($id);
		$view_data['page'] = 'all_user';
        $data['page_data'] = $this->load->view('instructor_user_details/instructor_profile', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}


		public function is_instructor($id) {
		if($id > 0) {
			$is_instructor = $_POST['instructor'];

			$this->db->select("*");
			$this->db->where('id',$id);
			$user_details = $this->db->get('backend_user')->row_array();

			if($_POST['instructor'] == 1){ // activate instrutor
				$data = array('user_id'=>$id);
				$this->db->where('user_id',$id);
				$checkStatus = $this->db->get('course_instructor_information')->row_array();
				if(empty($checkStatus)){
					$result = $this->db->insert("course_instructor_information",$data);
				}

				$this->db->where('id',$id);
			  $instructor_details_backend_user = $this->db->get('backend_user')->row_array();

				if($instructor_details_backend_user['status'] != 0){

					$this->db->where('id',$id);
					$update = $this->db->update('backend_user',array('status'=>0));
					$input =  array(
					"SENDER"=> "support@dbmci.com",
					"RECIPIENT"=> $user_details['email'] ,
					"SUBJECT"=> "Welcome to DBMCI Instructor" ,
					"HTMLBODY"=> " Dear ".$user_details['username'].",Instructor account has been activated on ".CONFIG_PROJECT_NICK_NAME."." ,
					"TEXTBODY"=> "Password For Instructor Login"
				 );
				//$this->aws_emailer->send_aws_email($input);
					send_message_global($user_details['country_code'],$user_details['mobile'],'Instructor account has been activated on '.CONFIG_PROJECT_NICK_NAME.'.');
				}

			}elseif($_POST['instructor'] == 0){
				$this->db->where('id',$id);
				$update = $this->db->update('backend_user',array('status'=>1));
				send_message_global($user_details['country_code'],$user_details['mobile'],'Instructor account has been de-activated on '.CONFIG_PROJECT_NICK_NAME.'.');
			}
		}

		redirect('auth_panel/instructor_user_details/instructor_profile/'.$id);
	}



	public function ajax_instructor_ratings_list($id) {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'rating',
			3 => 'text',
			4 => 'creation_time'

		);

		$query = "SELECT count(id) as total FROM course_instructor_rating";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT cir.*,u.name,DATE_FORMAT(FROM_UNIXTIME(cir.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time
								FROM course_instructor_rating as  cir
								 join users as u
								on cir.user_id = u.id where cir.instructor_id = $id";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND rating LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND text LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND creation_time LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}



		$query = $this->db->query($sql)->result();
		//echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$star="";
			if($r->rating > 0 ){
				for($i=0;$i<$r->rating;$i++){
					$star .='<i class =  "fa fa-star text-danger"></i>';
				}
				if($i<5){$j = 5 - $i;for($i=0;$i<$j;$i++){$star .='<i class =  "fa fa-star-o text-danger"></i>';}}
			}

			$nestedData[] = $r->id;
			$nestedData[] = $r->name;
			$nestedData[] = $star;
			$nestedData[] = substr($r->text, 0, 30);
			$nestedData[] = $r->creation_time;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "web_user/edit_instructor_rating/" . $r->id . "'>Edit</a>&nbsp" ;
			$action .= "<a class='btn-sm btn btn-danger btn-xs bold' href='" . AUTH_PANEL_URL . "web_user/delete_review/" . $r->id ."?instructor_id=".$r->instructor_id. "'>delete</a>";
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function edit_instructor_rating($id){
		$view_data['page']  = 'edit_instructor_rating';
		$data['page_title'] = "Edit Instructor Rating";
		if($this->input->post('update_instructor_review')) {
			/* handle submission */
			$this->update_instructor_review();
		}
		$view_data['instructor_rating_detail'] = $this->web_user_model->get_instructor_rating_details_by_id($id);
		$data['page_data'] = $this->load->view('web_user/edit_instructor_details', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	private function update_instructor_review(){
		if($this->input->post()) {
			$this->form_validation->set_rules('rating', 'Rating', 'required');
			$this->form_validation->set_rules('text', 'Review', 'required');
			$review_id = $this->input->post('id');
			$user_id = $this->input->post('instructor_id');
			if ($this->form_validation->run() == FALSE) {
             $error = validation_errors();
            }else{
				$update = array(
					'rating' => $this->input->post('rating'),
					'text' => $this->input->post('text'),

				);
				$this->db->where('id',$review_id);
				$this->db->update('course_instructor_rating',$update);
				page_alert_box('success','Action performed','Review updated successfully');
			}
		}

	}

	public function delete_review($id) {
		$user_id = $_GET['instructor_id'];
		$status = $this->web_user_model->delete_review($id);
		page_alert_box('success','Action performed','Review deleted successfully');
		if($status) {
			redirect('auth_panel/web_user/user_profile/'.$user_id);
		}
	}


}
