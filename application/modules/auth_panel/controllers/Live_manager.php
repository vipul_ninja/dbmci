<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Live_manager extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->helper("push_helper");
	}

	public function live(){

		if($this->input->post()){
			$user_detail = $this->db->query("SELECT * from users where id='".$_POST['user_id']."'")->row_array();;
			
			$stream = 0;
			if($this->input->post('state') == 0){
				$stream = 1;
			}
			$info = array(
				"state"=>$stream ,
				"user_id" =>$user_detail['id'],
				"stream_for"=>$this->input->post('stream_for')
				);
			$this->db->where('id',$this->input->post('id'));
			$this->db->update('live_manager',$info);

			if($this->input->post('state') == 0 && $this->input->post('send_notify') == 1){
				$this->send_push_notification($this->input->post('stream_for'),$user_detail);
			}
		}

		$this->db->where("id",1);
		$view_data['live_row'] = $this->db->get('live_manager')->row_array();
		$view_data['page'] = 'go_live';
		$data['page_title'] = "Go Live";
		
		$data['page_data'] = $this->load->view('live_manager/live', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function send_push_notification($for,$user_detail){
		$this->db->where("id",1);
		$live_row = $this->db->get('live_manager')->row_array();

			$push_data = json_encode(
				array(
					'notification_code' => 401,
					'message' => $user_detail['name'] ." is live.",
				)
			);
			if($for =="ALL"){
						$users = $this->db->get('users')->result_array();
			}else{
			$this->db->where('dams_tokken != ""');
			$users = $this->db->get('users')->result_array();
			}

			foreach($users as $u ){
				if( $u['device_type'] == 1){
					/* android */
					$token =  $u['device_tokken'];
					$device = "android";
					$result = generatePush($device, $token, $push_data);
					log_message('error', "android push notification . token is $token and data is ".json_encode($push_data).$result);
				}
				
				if( $u['device_type'] == 2){
					/* ios */
					$token =  $u['device_tokken'];
					$device = "ios";
					generatePush($device, $token, $push_data);
				}

			}
	}
}	