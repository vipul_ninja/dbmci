<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		$this->load->model("Course_list_model");
		$this->load->model("Category_list_model");

	}


	public function index() {
		$view_data['category_id'] = $this->input->get('cat');
		$view_data['page']  = 'course';

		$data['page_title'] = "course list";
		$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('course/course_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_course_list_level_one($master_id) {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'text',
			
		);

		$query = "SELECT count(id) as total
								FROM course_intersted_in_title 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ciil.id as id,ciit.text as title , ciil.text as text 
								FROM course_intersted_in_list as  ciil
								join course_intersted_in_title as ciit 
								on ciil.parent_id = ciit.id
								where  ciit.master_category = $master_id
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having text LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}	
			
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->title;
			$nestedData[] = $r->text;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "course/edit_sub_course/" . $r->id . "'>Edit</a>";
			
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function add_course() {
		if($this->input->post()) { 

			$this->form_validation->set_rules('main_cat', 'Main Category', 'required');

			$this->form_validation->set_rules('text', 'Course Name', 'required|is_unique[course_intersted_in_title.text]');
			

			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$insert_data = array('master_category' => $this->input->post('main_cat'),'text' => $this->input->post('text'),);
            	
            	$this->db->insert('course_intersted_in_title',$insert_data);
            	
 
				$data['page_toast'] = 'Information Inserted successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }


		}
		$view_data['page']  = '';
		$data['page_title'] = "Add course";
		$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('course/add_course', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}



	public function edit_sub_course($id) {
		
		if($this->input->post()) { 
			$this->form_validation->set_rules('text', 'Sub Category', 'required');
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	/*$visible = $this->input->post('visible');
            	if(isset($visible)) {$visible=0;} else {$visible=1;}*/
            	$update = array('text' => $this->input->post('text'));
            	$this->db->where('id',$id);
            	$this->db->update('course_intersted_in_list',$update);
            	//$this->
 
				$data['page_toast'] = 'Information Updated  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }
		}
		$view_data['page']  = '';
		$data['page_title'] = "Sub course edit";
		$view_data['sub_course'] = $this->Course_list_model->get_sub_course_list_by_id($id);
		$data['page_data'] = $this->load->view('course/edit_sub_course_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function add_sub_course() {
		if($this->input->post()) { 
			$this->form_validation->set_rules('main_cat', 'Main Category', 'required');
			$this->form_validation->set_rules('main_course', 'Main Course', 'required');
			$this->form_validation->set_rules('text', 'Course Name', 'required|is_unique[course_intersted_in_list.text]');
			
			if ($this->form_validation->run() == FALSE) {
            } else {
            	$insert_data = array('parent_id' => $this->input->post('main_course'),'text' => $this->input->post('text'),);
            	$this->db->insert('course_intersted_in_list',$insert_data);
 
				$data['page_toast'] = 'Information Inserted successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }
		}
		$view_data['page']  = '';
		$data['page_title'] = "Add sub course";
		$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('course/add_sub_course', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function get_sub_course() {
		$id = $_POST['id'];
		$course = $this->Course_list_model->get_course_list_by_id($id);
		$html = '<option value="" >Select course</option>';
		    foreach($course as $crs) {
		$html .= '<option value='.$crs['id'].'>'.$crs['text'].'</option>';
			}

		echo json_encode(array('status'=>true,'html'=>$html));die;
	}


}