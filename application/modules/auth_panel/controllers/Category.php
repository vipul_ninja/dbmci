<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends MX_Controller {
	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		$this->load->model("Category_list_model");

	}


	public function index() {
		$data['page_title'] = "category list";
		$view_data['page']  = 'category_list';
		$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('category/category_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_subcategory_list_level_one() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'master_text',
			2 => 'text',
		);

		$query = "SELECT count(id) as total
								FROM master_category_level_one 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT mclo.id as id , mclo.text as text , mclo.visibilty as visibilty , mc.text as master_text
								FROM master_category_level_one as  mclo 
								join master_category as mc 
								on mc.id = mclo.parent_id
								where 1=1
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having text LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having master_text LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}	
			
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->master_text;
			$nestedData[] = $r->text;
			$nestedData[] = ($r->visibilty == 0 )?'<i class="text-info">visible</i>':'<i class="text-danger">hidden</i>';
			$action = "<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "category/edit_sub_category/" . $r->id . "'>Edit</a>";
			
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}
	

	public function edit_sub_category($id) {
		if($this->input->post()) { 
			$this->form_validation->set_rules('text', 'Sub Category', 'required');
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$visible = $this->input->post('visible');
            	if(isset($visible)) {$visible=0;} else {$visible=1;}
            	$update = array('text' => $this->input->post('text'),'visibilty'=>$visible);
            	$this->db->where('id',$id);
            	$this->db->update('master_category_level_one',$update);
            	//$this->
 
				$data['page_toast'] = 'Information Updated  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }


		}
		$view_data['page']  = '';
		$data['page_title'] = "category list";
		$view_data['sub_category_id'] = $id;
		$view_data['sub_category_list'] = $this->Category_list_model->get_sub_category_list();
		$view_data['sub_category'] = $this->Category_list_model->get_sub_category_list_by_id($id);
		$data['page_data'] = $this->load->view('category/edit_sub_category_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}


	public function add_sub_category() {
		if($this->input->post()) { 
			$this->form_validation->set_rules('main_cat', 'Main Category', 'required');

			$this->form_validation->set_rules('text', 'Sub Category', 'required');
			

			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$insert_data = array('parent_id' => $this->input->post('main_cat'),'text' => $this->input->post('text'),);
            	
            	$this->db->insert('master_category_level_one',$insert_data);
            	
 				
				$data['page_toast'] = 'Information Inserted successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }


		}
		$view_data['page']  = '';
		$data['page_title'] = "category list";
		$view_data['sub_category'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('category/add_sub_category_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_subcategory_list_level_two($id) {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'sub_text',
			2 => 'text',
		);

		$query = "SELECT count(id) as total
								FROM master_category_level_two 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT mclt.id as id , mclt.text as sub_text , mclo.text as text
								FROM master_category_level_two as  mclt 
								join master_category_level_one as mclo 
								on mclo.id = mclt.parent_id
								where mclt.parent_id = $id
								And 1=1
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND text LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}	
			
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND sub_text LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->text;
			$nestedData[] = $r->sub_text;
			$action = "<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "category/edit_sub_sub_category/" . $r->id . "'>Edit</a>";
			
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function edit_sub_sub_category($id) {
		if($this->input->post()) { 
			$this->form_validation->set_rules('text', 'Sub Category', 'required');
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$visible = $this->input->post('visible');
            	if(isset($visible)) {$visible=0;} else {$visible=1;}
            	$update = array('text' => $this->input->post('text'),'visibilty'=>$visible);
            	$this->db->where('id',$id);
            	$this->db->update('master_category_level_two',$update);
            	//$this->
 
				$data['page_toast'] = 'Information Updated  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }
		}
		$view_data['page']  = '';
		$data['page_title'] = "Sub category list";
		$view_data['sub_category'] = $this->Category_list_model->get_sub_sub_category_list_by_id($id);
		$data['page_data'] = $this->load->view('category/edit_sub_sub_category_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function add_sub_sub_category($id) {
		if($this->input->post()) { 

			$this->form_validation->set_rules('text', 'Sub Category', 'required');
			

			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$insert_data = array('parent_id' => $id,'text' => $this->input->post('text'),);
            	
            	$this->db->insert('master_category_level_two',$insert_data);
            	
 				
				$data['page_toast'] = 'Information Inserted successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }


		}
		$data['page_title'] = "Add Sub category list";
		$view_data['page']  = '';
		$view_data['sub_category'] = $this->Category_list_model->get_sub_category_list_by_id($id);
		$data['page_data'] = $this->load->view('category/add_sub_sub_category_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function add_master_category()
	{
		die('add');
	}

	public function list_master_category()
	{
			if($this->input->post()) { //print_r($_POST); die;
				$this->form_validation->set_rules('text', 'Category', 'required|is_unique[master_category.text]');
				if ($this->form_validation->run() == FALSE) {

		  }else{
					$insert_data = array('text' => ucfirst($this->input->post('text')));
					$insert_data['created_on'] = milliseconds();
					$insert_data['updated_on'] = milliseconds();
					$this->db->insert('master_category',$insert_data);
					page_alert_box('success','Category Added','Category has been added successfully');
		  }
			}
			$data['page_data'] = $this->load->view('category/master_category/add_master_category', array(), TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
		
	}
	
	public function ajax_master_category_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'text',
			2 => 'visibilty',
			3 => 'created_on',
			4 => 'updated_on'
		);
		$query = "SELECT count(id) as total FROM master_category";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT csm.id, csm.text ,csm.visibilty,
										DATE_FORMAT(FROM_UNIXTIME(csm.created_on/1000), '%d-%m-%Y %h:%i:%s') as created_on ,
										DATE_FORMAT(FROM_UNIXTIME(csm.updated_on/1000), '%d-%m-%Y %h:%i:%s') as updated_on
										FROM master_category as csm ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND text LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND visibilty LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND created_on LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND updated_on LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->text;
			$nestedData[] = ($r->visibilty == 0)?'Enabled':'Disabled'; 
			$nestedData[] = $r->created_on;
			$nestedData[] = $r->updated_on;
			if($r->visibilty == 0 ){
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to disable?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "Category/disable_subject/" . $r->id . "'>Disable</a>";
			}else{
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to enable?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "Category/enable_subject/" . $r->id . "'> Enable</a>";
			}

			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "category/edit_subject/" . $r->id . "'><i class='fa fa-pencil'></i>&nbsp Edit</a>&nbsp;$control";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


public function disable_subject($subject_id){	
		$this->db->where('id',$subject_id);
	  $this->db->update('master_category',array('visibilty'=>1));
		page_alert_box('success','Category','Category has been disabled successfully');
		redirect(AUTH_PANEL_URL.'category/list_master_category');
	}

	public function enable_subject($subject_id){
		$this->db->where('id',$subject_id);
	   $this->db->update('master_category',array('visibilty'=>0));
		page_alert_box('success','Category','Category has been enabled successfully');
		redirect(AUTH_PANEL_URL.'category/list_master_category');
	}

	public function edit_subject($id) 
	{		
			if($this->input->post()) 
			{
			
				$this->form_validation->set_message('edit_unique', 'Sorry, This Category name already exist.');
				$this->form_validation->set_rules('text', 'Category Name', 'trim|required|edit_unique[master_category.text.'.$id.']');
				if ($this->form_validation->run() == TRUE)
				 {
	             	$update_data = array(
									'text' => ucfirst($this->input->post('text')),
									'updated_on' => milliseconds()
					);
					$this->db->where('id',$id);
					$this->db->update('master_category',$update_data);
					page_alert_box('success','Subject Updated','Subject has been updated successfully');
					redirect(AUTH_PANEL_URL.'Category/list_master_category');
	            }
			}
			$view_data['page']  = 'edit_subject';
			$data['page_title'] = "Subject edit";
			$this->db->where('id',$id);
			$view_data['subject'] = $this->db->get('master_category')->row_array();
			$data['page_data'] = $this->load->view('category/master_category/edit', $view_data, TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}



}