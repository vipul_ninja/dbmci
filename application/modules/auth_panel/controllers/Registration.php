<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation', 'uploads');
    $this->load->helper('message_sender_helper');
	}

	public function index(){
		$status = "";
		if ($this->input->post()) {
				$this->form_validation->set_error_delimiters('','');
				$this->form_validation->set_rules('username', 'User Name', 'required|trim|is_unique[backend_user.username]');
				$this->form_validation->set_rules('password', 'Password', 'required|trim');
				$this->form_validation->set_rules('confirm_password', 'Confirm password', 'required|trim|matches[password]');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim|is_unique[backend_user.email]');
				$this->form_validation->set_rules('mobile', 'Mobile', 'required|trim|is_unique[backend_user.mobile]|regex_match[/^[0-9]{10}$/]');
				$this->form_validation->set_rules('country_code', 'Country Code', 'required');


				if ($this->form_validation->run() == True) {
					$insert_array = array(
						'username' => $this->input->post('username'),
						'email' => $this->input->post('email'),
						'password' => md5($this->input->post('password')),
						'mobile' => $this->input->post('mobile'),
						'country_code' => $this->input->post('country_code'),
						'creation_time' => time(),
						'instructor_id' => 1,
						'status' => 1,
						'otp' => rand(1000,999999)
					);
					send_message_global($insert_array['country_code'],$insert_array['mobile'],'your otp for instrutor registration is '.$insert_array['otp']);

					$this->session->set_userdata($insert_array);
					redirect(site_url('auth_panel/registration/verify_mobile'));

				}
		}
		$this->load->view('instructor_user/instructor_registration', $status);
	}

	public function verify_mobile(){
		$data = array();
		$session_userdata = $this->session->userdata();
		$data['session_data'] = $session_userdata;

		$status = "";
		if ($this->input->post()) {
				$this->form_validation->set_rules('otp', 'OTP', 'required');


				if ($this->form_validation->run() == True) {
					$insert_array = array(
						'username' => $data['session_data']['username'],
						'email' => $data['session_data']['email'],
						'password' => $data['session_data']['password'],
						'mobile' => $data['session_data']['mobile'],
						'country_code' => $data['session_data']['country_code']	,
						'creation_time' => $data['session_data']['creation_time'],
						'instructor_id' => $data['session_data']['instructor_id'],
						'status' => $data['session_data']['status']

					);

					if($session_userdata['otp'] == $this->input->post('otp') ){


						$save_result = $this->db->insert("backend_user",$insert_array);
						if($save_result){
							$this->session->sess_destroy();
							send_message_global($insert_array['country_code'],$insert_array['mobile'],"You have successfully registered and you can login once you get approval by admin.");
							$data['status'] = "You have successfully registered and you can login once you get approval by admin.";
						}
					}
					else{
						$data['status'] = "Wrong Input !!";
					}

				}
		}
		$this->load->view('instructor_user/verify_mobile', $data);


	}



}
