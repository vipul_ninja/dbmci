<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Record extends MX_Controller {

	function __construct() {
		parent::__construct();
  		/* !!!!!! Warning !!!!!!!11
  		 *  admin panel initialization
  		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
  		 */
  		modules::run('auth_panel/auth_panel_ini/auth_ini');
  		$this->load->library('form_validation');
      $this->load->model('instructor_pay_model');
    }

	public function index() {
		if($this->input->post()) {
      $rec['amount'] = $this->input->post('amount');
      $rec['instructor_id'] = $this->input->post('instructor_id');
      $rec['message'] = $this->input->post('message');
      $this->instructor_pay_model->add_record($rec);
		}

		$data['page_data'] = $this->load->view('instructor_pay/add_record', array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

  public function ajax_single_ins_list($ins_id){

    		// storing  request (ie, get/post) global array to a variable
    		$requestData = $_REQUEST;

    		$columns = array(
    			// datatable column index  => database column name
    			0 => 'id',
    			1 => 'amount',
    			2 => 'message',
    			3 => 'creation_time',
    		);

    		$query = "SELECT count(id) as total
    								FROM instructor_pay_record where instructor_id = $ins_id
    								";
    		$query = $this->db->query($query);
    		$query = $query->row_array();
    		$totalData = (count($query) > 0) ? $query['total'] : 0;
    		$totalFiltered = $totalData;

    		$sql = "SELECT * , DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y %h:%i:%s') as on_time
    								FROM instructor_pay_record where instructor_id = $ins_id ";

    		// getting records as per search parameters
    		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
    			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
    		}
    		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
    			$sql.=" AND amount LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
    		}
    		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
    			$sql.=" AND message LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
    		}
    		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
    			$sql.=" having on_time LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
    		}

    		$query = $this->db->query($sql)->result();

    		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

    		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

    		$result = $this->db->query($sql)->result();
    		$data = array();
    		foreach ($result as $r) {  // preparing an array
    			$nestedData = array();
    			$nestedData[] = $r->id;
    			$nestedData[] = $r->amount;
    			$nestedData[] = $r->message;
    			$nestedData[] = $r->on_time;
    			$data[] = $nestedData;
    		}

    		$json_data = array(
    			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
    			"recordsTotal" => intval($totalData), // total number of records
    			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
    			"data" => $data   // total data array
    		);

    		echo json_encode($json_data);  // send data as json format
  }

  public function view_all_paid_transactions(){
    $data['page_data'] = $this->load->view('instructor_pay/view_paid_transactions', array(), TRUE);
    echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
  }

}
