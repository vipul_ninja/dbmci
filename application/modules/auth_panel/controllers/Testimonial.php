<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Testimonial extends MX_Controller 
{
	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->model('web_user_model');
		$this->load->library('form_validation', 'uploads');
	}



public function add_testimonial(){	
		
    if($_POST && $_POST['user_id']!=='' && $_POST['description']!==''){				
        $this->submit_post();
    }
    $view_data['p_heading'] = "Add Testimonial";
  //  $view_data['post_type'] = "user_post_type_current_affair";
    $data['page_data'] = $this->load->view('testimonial/add_testimonial', $view_data, TRUE);
    echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
}
public function edit_testimonial(){	
	
	
if(isset($_GET['post_id']) && $_GET['post_id']!==''){
	
	  $this->db->where('id',$this->input->get('post_id'));
	  $view_data['test_data']=$this->db->get('user_testimonial')->row_array();
	
	 if($_POST && $_POST['user_id']!=='' && $_POST['description']!==''){				
        $this->update_post();
    }
	}
	
   
    $view_data['p_heading'] = "Edit Testimonial";
    $data['page_data'] = $this->load->view('testimonial/edit_testimonial', $view_data, TRUE);
    echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
}


function submit_post(){
    $data=array('user_id'=>$this->input->post('user_id'),
    'review'=>$this->input->post('description')
                );
        $this->db->insert('user_testimonial', $data); 
        page_alert_box('success','Post','Testimonial has been added successfully');
        redirect(AUTH_PANEL_URL."testimonial/testimonial_list");
        
    }

	function update_post(){
		$data=array('user_id'=>$this->input->post('user_id'),
		'review'=>$this->input->post('description')
					);
			$this->db->where('id',$this->input->get('post_id'));
			$this->db->update('user_testimonial', $data); 
			page_alert_box('success','Post','Testimonial has been updated successfully');
			redirect(AUTH_PANEL_URL."testimonial/testimonial_list");
			
		}
    public function testimonial_list() {	
		$data['page_title'] = "Testimonial List";
		

		//$view_data['videos'] = $this->currentaffairs_model->get_currentaffair_list();
		$data['page_data'] = $this->load->view('testimonial/testiminial_list', $data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	} 
    public function ajax_all_testimonial_list() {	
		//echo $video_id; die;
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'post_headline',		
			2 => 'display_picture'
		);

		$query = "SELECT count(id) as total
				  FROM user_testimonial ";
//echo $query;
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;


        $sql = "SELECT ut.*,users.name  from user_testimonial as ut inner join users on ut.user_id= users.id  where   1=1 ";

		
		// getting records as per search parameters
		// if (!empty($requestData['columns'][0]['search']['value'])) {   //name
		// 	$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		// }
		if (!empty($requestData['columns'][0]['search']['value'])) {  //salary
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
        if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND review LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}		
		//echo $sql;
		$query = $this->db->query($sql)->result();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length		
		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->review;
			$nestedData[] = $r->name;

        $action = "<a class='btn-xs bold btn btn-info edit_video_comment' href='".AUTH_PANEL_URL."testimonial/edit_testimonial?post_id=".$r->id."' data-trigger='edit_comment' >Edit</a>";
			$action1 = "<a class='btn-xs bold btn btn-danger ' onclick=\"return confirm('Warning !!!!  Do you really want to delete this data?');\" href='".AUTH_PANEL_URL."testimonial/delete_testimonial/".trim($r->id)."' data-trigger='' >delete</a>";		

			$nestedData[] = $action. '    ' .$action1;
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
    }
    
    public function delete_testimonial($id){
           $this->db->where('id',$id);
		   $this->db->delete('user_testimonial');
		    page_alert_box('success','Testimonial','Testimonial has been deleted successfully.');
          
	   redirect(AUTH_PANEL_URL.'Testimonial/testimonial_list');
   }

}