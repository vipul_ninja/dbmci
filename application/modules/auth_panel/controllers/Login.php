<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation', 'uploads');
	}

	public function index(){
		$error['error'] = "";
		if($this->session->userdata('active_backend_user_flag') && $this->session->userdata('active_backend_user_flag') ){
			redirect(site_url('auth_panel/admin/index'));
			die;
		}
		if ($this->input->post()) {
				$this->form_validation->set_error_delimiters(' ', ' ');
				$this->form_validation->set_rules('password', 'Password', 'required|trim');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');

				if ($this->form_validation->run() == FALSE) { }

				$this->db->Where("email", $this->input->post('email'));
				$this->db->Where("password", md5($this->input->post('password')));
				//$this->db->Where("status", '0');
				$result = $this->db->get('backend_user')->row();
				if (!empty($result) && $result->status == 0) {
						/*
						* setting session according to auth_panle_ini file in controller master file of panel
						*/

						$newdata = array(
						        'active_backend_user_flag'  => True,
						        'active_backend_user_id'    => $result->id,
								'active_user_data'			=>$result
						);
						/******* Check For Instructor User *******/
						if($result->instructor_id == 0){
							$this->session->set_userdata($newdata);
							redirect(site_url('auth_panel/admin/index'));
							die;
						}
					    elseif($result->instructor_id != 0){

							$this->session->set_userdata($newdata);
							redirect(site_url('auth_panel/course_product/Instructor_transactions_details?transaction_id='.$result->id));
							die;
						}
						/*******  Check For Instructor User Ends *******/
				}
			elseif(!empty($result) && $result->status == 1){
				$error['error'] = "Blocked Account !! Please contact the admin.";
			}
			elseif(empty($result) && $this->input->post('email') != '' && $this->input->post('password') != ''){
				$error['error'] = "Invalid Credentials !!";
			}
		}


		$this->load->view('login/login', $error);

	}

	public function logout(){
			$this->session->sess_destroy();
			redirect(site_url('auth_panel/login/index'));
	}


	public function forget_password() {
		//echo "<pre>";print_r($_POST);die;

		if(!isset($_POST['post_type'])) {

		if($_POST['email'] == '') {
			echo json_encode(array('status' => false, 'message'=>'Please enter email address.' ));die;
		}
		if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			echo json_encode(array('status' => false, 'message'=>'Please enter valide email address.' ));die;
		}
		$this->db->Where("email", $_POST['email']);
		$result = $this->db->get('backend_user')->row();
		if(empty($result)) {
			echo json_encode(array('status' => false, 'message'=>'Email address does not exist.' ));die;
		} else {
			$tokken = rand(1000,999999);
			/* ####### Update tokken of backend user ###########*/
			$this->db->where('email',$_POST['email'])
			->update('backend_user',array('tokken'=>$tokken));
				$input =  array(
					"SENDER"=> "support@dbmci.com",
					"RECIPIENT"=> $_POST['email'] ,
					"SUBJECT"=> "DBMCI! Otp for change password!" ,
					"HTMLBODY"=> "Hello! </br> We have received a request to recover the password to your dashboard .</br>
												So we generated a OTP for your request .</br>
												Your OTP for change password is  $tokken </br></br>
												If you did not request to recover your password, please disregard this message, and no changes will be made to your current sign-in details.</br></br>
Sincerely,</br>
<bold>Your eMedicoz Team</bold>" ,
					"TEXTBODY"=> "Your tokken for change password is  $tokken"
				);
			 $this->aws_emailer->send_aws_email($input);

			echo json_encode(array('status' => true, 'post_type'=>'', 'message'=>'OTP Has been sent on your email and mobile number' ));die;

		}
	} else {
		if($_POST['tokken'] == '') {
			echo json_encode(array('status' => false,  'message'=>'Please enter tokken.' ));die;
		}
		if($_POST['new_pwd'] == '') {
			echo json_encode(array('status' => false, 'message'=>'Please enter new password.' ));die;
		}
		if($_POST['cnf_pwd'] == '') {
			echo json_encode(array('status' => false, 'message'=>'Please enter confirm password.' ));die;
		}
		if($_POST['new_pwd'] != $_POST['cnf_pwd']) {
			echo json_encode(array('status' => false, 'message'=>'Password does not match.' ));die;
		}

		$result = $this->db->where('email',$_POST['email'])
				->where('tokken',$_POST['tokken'])
				->update('backend_user',array('password'=>md5($_POST['new_pwd']),'tokken'=>''));
		if($this->db->affected_rows()) {
			echo json_encode(array('status' => true, 'post_type'=>$_POST['post_type'], 'message'=>'Password change successfully please <a href="">Click here to login</a>.' ));die;
		} else {
			echo json_encode(array('status' => false,  'message'=>'Tokken does not correct' ));die;
		}
	}

	}

}
