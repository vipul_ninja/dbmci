<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stream extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		$this->load->helper('cookie');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation');
				/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
		$this->load->library('form_validation', 'uploads');
		$this->load->library('upload');
		/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
	}
	// public function get_all_subject(){
	// 	$return = $this->db->get("course_subject_master")->result_array();
	// 	if($this->input->get("return") == "json"){
	// 		echo json_encode($return);die;
	// 	}
	// 	return $return;
	// }
	// public function get_topic_from_subject($subject_id){
	// 	$this->db->where('subject_id',$subject_id);
	// 	$return = $this->db->get("course_subject_topic_master")->result_array();
	// 	if($this->input->get("return") == "json"){
	// 		echo json_encode($return);die;
	// 	}
	// 	return $return;
	// }
	public function amazon_s3_upload($name,$aws_path) {

		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).str_replace([':', ' ', '/', '*','#','@','%'],"_",$_FILES["file"]['name']),
							'SourceFile' => $_FILES["file"]['tmp_name'],
							'ContentType' => 'image',
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array('param1' => 'value 1','param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	}


	public function add_stream(){
		if($this->input->post()) {
			$this->form_validation->set_rules('name', 'name', 'required|is_unique[]');
		}
		$view_data['page']  = "add_stream";
		$data['page_data'] = $this->load->view('',$view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function add_sub_stream($id){
		if($this->input->post()) {
			$this->form_validation->set_message('stream_add_unique', 'Sorry, This Sub stream name already exist in this stream.');
			$this->form_validation->set_rules('name', 'Stream Name', 'trim|required|stream_add_unique[course_stream_name_master.name.'.$id.']');
			if ($this->form_validation->run() == FALSE) {

		     }else{
				$insert_data = array('name' => ucfirst($this->input->post('name')));
				$insert_data['creation_time'] = milliseconds();
				$insert_data['parent_id'] = $id;
				$insert_data['last_updated'] = milliseconds();
				if($_FILES && $_FILES['image']['name']){		
					$file  = $this->amazon_s3_upload($_FILES['image'],'course_file_meta');
					}else{
					$file='Not Selected';
					}						
					//$insert_data['image'] =$file;
					//$insert_data['color'] =$this->input->post('color');
				$this->db->insert('course_stream_name_master',$insert_data);
				page_alert_box('success','Stream Added','Stream has been added successfully');
	      	}

		}
		$view_data['page']  = "add_sub_stream";
		$view_data['stream_id'] = $id;
		$data['page_data'] = $this->load->view('course_product/stream/add_sub_stream',$view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
////ADD STUDY TYPE

public function add_study_sub_type($id){
		if($this->input->post()){
		$type=1;
		$this->form_validation->set_message('stream_add_unique', 'Sorry, This Sub stream name already exist in this stream.');
		$this->form_validation->set_rules('name', 'Study Name', 'trim|required|stream_add_unique[course_stream_name_master.name.'.$id.']');
			if ($this->form_validation->run() == FALSE) {
		     }else{

				$insert_data = array('name' => ucfirst($this->input->post('name')));
				$insert_data['creation_time'] = milliseconds();
				$insert_data['parent_id'] = $id;
				$insert_data['type'] = $type;
				$insert_data['last_updated'] = milliseconds();
				$this->db->insert('course_stream_name_master',$insert_data);
				page_alert_box('success','Study Added','Study has been added successfully');
	      	}

		}
		$view_data['page']  = "add_sub_stream";
		$view_data['stream_id'] = $id;
		$data['page_data'] = $this->load->view('course_product/stream/add_study_type',$view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


public function stream_management(){
	if($this->input->post()) { 
		// print_r($this->input->post()); di ;
		  // print_r($_FILES['image']);
		  //  die;
		$this->form_validation->set_message('stream_add_unique', 'Sorry, This stream name already exist.');
		$this->form_validation->set_rules('name', 'Stream Name', 'trim|required|stream_add_unique[course_stream_name_master.name.0]');
		//$this->form_validation->set_rules('color', 'color ', 'trim|required');
		//$this->form_validation->set_rules('exampleInputFile', 'Image', 'required');
		if ($this->form_validation->run() == FALSE) {

		 }else{
			$insert_data = array('name' => ucfirst($this->input->post('name')));
			$insert_data['creation_time'] = milliseconds();
			$insert_data['last_updated'] = milliseconds();

			if($_FILES && $_FILES['image']['name']){		
			$file  = $this->amazon_s3_upload($_FILES['image'],'course_file_meta');
			}else{
			$file='Not Selected';
			}						
			$insert_data['image'] =$file;
			//$insert_data['color'] =$this->input->post('color');
			$this->db->insert('course_stream_name_master',$insert_data);
			//echo $this->db->last_query();
			page_alert_box('success','Stream Added','Stream has been added successfully');
		  }
	}
	$data['page_data'] = $this->load->view('course_product/stream/add_stream', array(), TRUE);
	echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
}

	public function disable_stream($stream_id){
		$this->db->where('id',$stream_id);
	  $this->db->update('course_stream_name_master',array('status'=>1));
		page_alert_box('success','Stream Disabled','Stream has been disabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/stream_management');
	}

	public function delete_stream($stream_id){
		$this->db->where('id',$stream_id);
		$this->db->or_where('parent_id',$stream_id);
	  	$this->db->delete('course_stream_name_master');
		page_alert_box('success','Stream Deleted','Stream has been deleted successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/stream_management');
	}

	public function enable_stream($stream_id){
		$this->db->where('id',$stream_id);
	   $this->db->update('course_stream_name_master',array('status'=>0));
		page_alert_box('success','Stream Enabled','Stream has been enabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/stream_management');
	}

	public function disable_sub_stream($stream_id){
		$this->db->where('id',$stream_id);
		$this->db->update('course_stream_name_master',array('status'=>1));
		page_alert_box('success','Sub Stream Disabled','Sub Stream has been disabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/add_sub_stream/'.$this->input->get('parent_id'));
	}


	public function delete_sub_stream($stream_id){
		$this->db->where('id',$stream_id);
		$this->db->or_where('parent_id',$stream_id);
	  	$this->db->delete('course_stream_name_master');
		page_alert_box('success','Stream Deleted','Stream has been deleted successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/add_sub_stream/'.$this->input->get('parent_id'));
	}
	
	public function enable_sub_stream($stream_id){
		$this->db->where('id',$stream_id);
		$this->db->update('course_stream_name_master',array('status'=>0));
		page_alert_box('success','Sub Stream Enabled','Sub Stream has been enabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/add_sub_stream/'.$this->input->get('parent_id'));
	}

	public function enable_study($stream_id){
		$this->db->where('id',$stream_id);
	    $this->db->update('course_stream_name_master',array('status'=>0));
		page_alert_box('success','Stream Enabled','Study has been enabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/add_study_sub_type/'.$this->input->get('parent_id'));
	}

	public function disable_study($stream_id){
		$this->db->where('id',$stream_id);
		$this->db->update('course_stream_name_master',array('status'=>1));
		page_alert_box('success','Sub Stream Disabled','Study has been disabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/add_study_sub_type/'.$this->input->get('parent_id'));
	}
	public function ajax_stream_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'last_updated'
		);
		$query = "SELECT count(id) as total FROM course_stream_name_master where parent_id = 0";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT csm.id, csm.name ,csm.parent_id,csm.status,
										DATE_FORMAT(FROM_UNIXTIME(csm.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time ,
										DATE_FORMAT(FROM_UNIXTIME(csm.last_updated/1000), '%d-%m-%Y %h:%i:%s') as last_updated
										FROM course_stream_name_master as csm where parent_id = 0 ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		$start  = 0 ;
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = ++$start;
			$nestedData[] = $r->name;
			// $nestedData[] = '<img width="30" height="30"  src="'.$r->image.'">';
			
			// $nestedData[] = ($r->status == 1 )?'Disabled':'Enabled';
			// $nestedData[] = $r->creation_time;
			$nestedData[] = $r->last_updated;
			if($r->status == 0 ){
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to disable?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/stream/disable_stream/" . $r->id . "'>Disable</a>";
			}else{
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to enable?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "course_product/stream/enable_stream/" . $r->id . "'> Enable&nbsp</a>";
			}

			$nestedData[] = " <a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "course_product/stream/add_sub_stream/" . $r->id . "?name=".$r->name."'><i class='fa fa-pencil'></i>&nbsp Manage Sub Stream</a>
			<a class='btn-xs bold btn btn-info hide' href='".AUTH_PANEL_URL."course_product/stream/add_study_sub_type/".$r->id."'><i class='fa fa-pencil'></i>&nbspManage Study Type</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function ajax_sub_stream_list($id) {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'creation_time',
			
		);
		$query = "SELECT count(id) as total FROM course_stream_name_master WHERE parent_id = $id";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT csm.id,csm.parent_id,csm.name ,csm.status ,
						DATE_FORMAT(FROM_UNIXTIME(csm.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time ,
						DATE_FORMAT(FROM_UNIXTIME(csm.last_updated/1000), '%d-%m-%Y %h:%i:%s') as last_updated
						FROM course_stream_name_master as csm WHERE parent_id = $id ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		$start  = 0 ;
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = ++$start;
			$nestedData[] = $r->name;
			// $nestedData[] = '<img width="30" height="30" style="background:'.$r->color.'" src="'.$r->image.'">';
			
			// $nestedData[] = ($r->status == 1 )?'Disabled':'Enabled';
			$nestedData[] = $r->creation_time;
			// $nestedData[] = $r->last_updated;
			if($r->status == 0 ){
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to disable?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/stream/disable_sub_stream/" . $r->id . "?parent_id=".$r->parent_id."'>Disable</a>";
			}else{
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to enable?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "course_product/stream/enable_sub_stream/" . $r->id . "?parent_id=".$r->parent_id."'> Enable</a>";
			}

			$nestedData[] = "
			&nbsp; <a onClick=\"return confirm('Are you sure you want to delete it? You may lose respective records. Action can not be undo.')\" class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/stream/delete_sub_stream/" . $r->id . "?parent_id=".$r->parent_id."'><i class='fa fa-times'></i>&nbsp Delete</a>
			<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "course_product/stream/edit_sub_stream?id=". $r->id. "&parent_id=".$r->parent_id."'><i class='fa fa-pencil'></i>&nbsp Edit</a>&nbsp; $control
			&nbsp;&nbsp
                                <a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "course_product/stream/add_sub_stream_category?id=". $r->id. "&parent_id=".$r->parent_id."'><i class='fa fa-pencil'></i>&nbsp View</a>&nbsp;
			&nbsp;&nbsp;";
                        
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function ajax_sub_sub_stream_list($id) {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name', 
			2 => 'creation_time',
			
		);
		$query = "SELECT count(id) as total FROM course_stream_name_master WHERE parent_id = $id";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT csm.id,csm.parent_id,csm.name ,csm.status ,
						DATE_FORMAT(FROM_UNIXTIME(csm.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time ,
						DATE_FORMAT(FROM_UNIXTIME(csm.last_updated/1000), '%d-%m-%Y %h:%i:%s') as last_updated
						FROM course_stream_name_master as csm WHERE parent_id = $id ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		$start  = 0 ;
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = ++$start;
			$nestedData[] = $r->name;
			// $nestedData[] = '<img width="30" height="30" style="background:'.$r->color.'" src="'.$r->image.'">';
			
			// $nestedData[] = ($r->status == 1 )?'Disabled':'Enabled';
			$nestedData[] = $r->creation_time;
			// $nestedData[] = $r->last_updated;
			if($r->status == 0 ){
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to disable?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/stream/disable_sub_stream/" . $r->id . "?parent_id=".$r->parent_id."'>Disable</a>";
			}else{
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to enable?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "course_product/stream/enable_sub_stream/" . $r->id . "?parent_id=".$r->parent_id."'> Enable</a>";
			}

			$nestedData[] = "
			&nbsp; <a onClick=\"return confirm('Are you sure you want to delete it? You may lose respective records. Action can not be undo.')\" class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/stream/delete_sub_stream/" . $r->id . "?parent_id=".$r->parent_id."'><i class='fa fa-times'></i>&nbsp Delete</a>
			<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "course_product/stream/edit_sub_stream?id=". $r->id. "&parent_id=".$r->parent_id."'><i class='fa fa-pencil'></i>&nbsp Edit</a>&nbsp; $control
			&nbsp;&nbsp
			&nbsp;&nbsp;";
                        
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}
        
        public function add_sub_stream_category(){
           $id=$this->input->get('id');
           $pid=$this->input->get('parent_id');
           if($this->input->post()) {
			$this->form_validation->set_message('stream_add_unique', 'Sorry, This Sub stream name already exist in this stream.');
			$this->form_validation->set_rules('name', 'Stream Name', 'trim|required|stream_add_unique[course_stream_name_master.name.'.$id.']');
			if ($this->form_validation->run() == FALSE) {

		     }else{
				$insert_data = array('name' => ucfirst($this->input->post('name')));
				$insert_data['creation_time'] = milliseconds();
				$insert_data['parent_id'] = $id;
				$insert_data['last_updated'] = milliseconds();
				if($_FILES && $_FILES['image']['name']){		
					$file  = $this->amazon_s3_upload($_FILES['image'],'course_file_meta');
					}else{
					$file='Not Selected';
					}						
					//$insert_data['image'] =$file;
					//$insert_data['color'] =$this->input->post('color');
				$this->db->insert('course_stream_name_master',$insert_data);
				page_alert_box('success','Stream Added','Stream has been added successfully');
	      	}

		}
		$view_data['page']  = "add_sub_stream_category";
		$view_data['stream_id'] = $id;
		$data['page_data'] = $this->load->view('course_product/stream/add_sub_stream_category',$view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
        }

///AJAX STUDY SHOW
	//study sub type show
	public function ajax_study_list($id) {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'status',
			3 => 'creation_time',
			4 => 'last_updated'
		);
		$query = "SELECT count(id) as total FROM course_stream_name_master WHERE parent_id = $id  and type=1";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT csm.id,csm.parent_id,csm.name ,csm.status ,
						DATE_FORMAT(FROM_UNIXTIME(csm.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time ,
						DATE_FORMAT(FROM_UNIXTIME(csm.last_updated/1000), '%d-%m-%Y %h:%i:%s') as last_updated
						FROM course_stream_name_master as csm WHERE parent_id = $id AND type=1";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		$start  = 0 ;
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = ++$start;
			$nestedData[] = $r->name;
			$nestedData[] = ($r->status == 1 )?'Disabled':'Enabled';
			$nestedData[] = $r->creation_time;
			$nestedData[] = $r->last_updated;
			if($r->status == 0 ){
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to disable?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/stream/disable_study/" . $r->id . "?parent_id=".$r->parent_id."'>Disable</a>";
			}else{
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to enable?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "course_product/stream/enable_study/" . $r->id . "?parent_id=".$r->parent_id."'> Enable</a>";
			}

			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "course_product/stream/edit_study_type?id=". $r->id. "&parent_id=".$r->parent_id."'><i class='fa fa-pencil'></i>&nbsp Edit</a>&nbsp; $control
			&nbsp;";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function edit_stream($id)
	{

	   if($this->input->post()){				
		   $this->form_validation->set_message('stream_edit_unique', 'Sorry, This stream name already exist.');
		   $this->form_validation->set_rules('name', 'Stream Name', 'trim|required|stream_edit_unique[course_stream_name_master.name.'.$id.'.0]');
		   $this->form_validation->set_rules('name_2', 'Stream (hindi) Name', 'trim|required|stream_edit_unique[course_stream_name_master.name_2.'.$id.'.0]');
		   
		   if($_FILES && $_FILES['image']['name']){					
				$file = $this->amazon_s3_upload($_FILES['image'],'course_file_meta');	
			}
		   if ($this->form_validation->run() == TRUE)
			 {
				$update_data = array(
							   'name' => ucfirst($this->input->post('name')),
							   'name_2' => ucfirst($this->input->post('name_2')),
							   'last_updated' => milliseconds()

			   );
				if($_FILES && $_FILES['image']['name']){	 
				$update_data['image']=$file;             
			}
			$update_data['color'] =$this->input->post('color');
			

			   $this->db->where('id',$id);
			   $this->db->update('course_stream_name_master',$update_data);
			   page_alert_box('success','Stream Updated','Stream has been updated successfully');
			   redirect(AUTH_PANEL_URL.'course_product/stream/stream_management');
	 }


		
	   }
	   $view_data['page']  = 'edit_stream';
	   $data['page_title'] = "Stream edit";
	   $this->db->where('id',$id);
	   $view_data['stream'] = $this->db->get('course_stream_name_master')->row_array();
	   $data['page_data'] = $this->load->view('course_product/stream/edit_stream', $view_data, TRUE);
	   echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
}

	public function edit_sub_stream() {
	//	print_r($_POST);
		$id= $this->input->get('id');
		$parent_id= $this->input->get('parent_id');
		if($this->input->post()) {
			$this->form_validation->set_message('stream_edit_unique', 'Sorry, This Sub Stream name already exist.');
			$this->form_validation->set_rules('name', 'Sub Stream Name', 'trim|required|stream_edit_unique[course_stream_name_master.name.'.$id.'.'.$parent_id.']');
		//$this->form_validation->set_rules('name_2', 'Sub Stream Name (HINDI)', 'trim|required|stream_edit_unique[course_stream_name_master.name_2.'.$id.'.'.$parent_id.']');
			
			
			if ($this->form_validation->run() == TRUE) {
             	$update_data = array(
								'name' => ucfirst($this->input->post('name')),
								'name_2' => ucfirst($this->input->post('name_2')),
								'last_updated' => milliseconds()
				);

				if($_FILES && $_FILES['image']['name']){		
					$file  = $this->amazon_s3_upload($_FILES['image'],'course_file_meta');
					$update_data['image'] =$file;
					}else{
					$file='Not Selected';
					}						
				
					$update_data['color'] =$this->input->post('color');
				$this->db->where('id',$id);
				$this->db->update('course_stream_name_master',$update_data);
			$subject_study_relation	= $this->input->post('subject_type'); 
				$this->db->where('stream_id',$this->input->get('id'))->delete('Substream_subject_relationship');
		   
				if($subject_study_relation !== NULL){
					foreach($subject_study_relation as $st_id){
						//echo $st_id;
						$array_data = array("subject_id"=>$st_id,"stream_id"=>$this->input->get('id'));	
						$this->db->insert('Substream_subject_relationship',$array_data);
					}
				}
				page_alert_box('success','Sub Stream Updated','Sub Stream has been updated successfully');

				redirect(AUTH_PANEL_URL.'course_product/stream/add_sub_stream/'.$this->input->post('parent_id'));
			}
		
		}
		$view_data['page']  = 'edit_sub_stream';
		$data['page_title'] = "Sub Stream edit";
		$this->db->where('id',$id);
		$view_data['stream'] = $this->db->get('course_stream_name_master')->row_array();
		$data['page_data'] = $this->load->view('course_product/stream/edit_sub_stream', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	//edit sub type//
	public function edit_study_type() {
		$id= $this->input->get('id');
		$parent_id= $this->input->get('parent_id');
		if($this->input->post()) {
			$this->form_validation->set_message('stream_edit_unique', 'Sorry, This Sub Stream name already exist.');
			$this->form_validation->set_rules('name', 'Sub Stream Name', 'trim|required|stream_edit_unique[course_stream_name_master.name.'.$id.'.'.$parent_id.']');
			if($this->form_validation->run() == TRUE) {
             	$update_data = array(
								'name' => ucfirst($this->input->post('name')),
								'last_updated' => milliseconds()
				);
				$this->db->where('id',$id);
				$this->db->update('course_stream_name_master',$update_data);
				page_alert_box('success','Study Updated','Sub Stream has been updated successfully');

				redirect(AUTH_PANEL_URL.'course_product/stream/add_study_sub_type/'.$this->input->post('parent_id'));
			}
		}
		$view_data['page']  = 'edit_sub_stream';
		$data['page_title'] = "Sub Stream edit";
		$this->db->where('id',$id);
		$view_data['stream'] = $this->db->get('course_stream_name_master')->row_array();
		$data['page_data'] = $this->load->view('course_product/stream/edit_study_type', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function save_position_stream(){
		$ids = $_POST['ids'];
		$counter = 1;
		foreach($ids as $id){
			$this->db->where('id',$id);
			$array = array('position'=>$counter);
			$this->db->update('course_stream_name_master',$array);
			$counter++;
		}
		echo json_encode(array('status'=>true,'message'=>'position saved'));
		die;
	}




}
