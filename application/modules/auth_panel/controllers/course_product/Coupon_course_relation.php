<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Coupon_course_relation extends MX_Controller {

    public function __construct() {
        parent::__construct();
        /* !!!!!! Warning !!!!!!!11
         *  admin panel initialization
         *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
         */
        $this->load->helper('aul');
        modules::run('auth_panel/auth_panel_ini/auth_ini');
        /* do not remove helper and grocey_crud
         * It will put you in danger
         */
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
    }

    public function _example_output($output = null) {
        $this->load->view(AUTH_TEMPLATE . 'grocery_crud_template', (array) $output);
    }

    public function course_coupon() {
        $crud = new grocery_CRUD();
        $crud->unset_export();
        $crud->unset_print();
        $crud->set_subject('Coupon');
        $crud->set_table('coupon_course_relation_master');
        $crud->set_relation('course_id', 'course_master', 'title');
        $crud->set_relation('coupon_id', 'course_coupon_master', 'coupon_tilte ');
        $crud->display_as('course_id', 'Course')->display_as('coupon_id', 'Coupon');
        $crud->required_fields(array('course_id', 'coupon_id'));
        $output = $crud->render();
        $this->_example_output($output);
    }

}
