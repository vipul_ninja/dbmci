<?php 
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course_category extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->model("category_list_model");
		$this->load->model("Add_college_model");

	}

	public function _example_output($output = null)
	{
		$this->load->view(AUTH_TEMPLATE.'grocery_crud_template',(array)$output);
	}

	public function amazon_s3_upload($name,$aws_path) {
		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');
				
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).$_FILES["file"]["name"], 
							'SourceFile' => $_FILES["file"]["tmp_name"], 
							'ContentType' => 'image', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	
	}

	// public function add_course_stream()
	// {
	// 	$crud = new grocery_CRUD();
	// 	$crud->unset_export();
	// 	$crud->unset_print();
	// 	$crud->set_subject("Course category");
	// 	$crud->set_table('course_category');
	// 	$crud->columns(array('parent_fk','name','visibilty'));
	// 	$crud->display_as('parent_fk','Main category');
	// 	$crud->required_fields(array('name','parent_fk','app_view_type','visibilty'));
	// 	$crud->set_relation('parent_fk','master_category','text');
	// 	$crud->field_type('app_view_type','dropdown', array('1' => 'vertical', '2' => 'horizontal'));
	// 	$crud->unset_read();
	// 	$crud->unset_edit_fields('position');
	// 	$crud->unset_fields('position');
	// 	$output = $crud->render();
	// 	$this->_example_output($output);
	// }

	public function add_course_stream()
	{
		$view_data['main_categories']=$this->get_main_category();
		$view_data['page']  = 'add_course_category';
		$data['page_data']=$this->load->view('course_product/course/add_course_category',$view_data,TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function add_category(){ 
		if($this->input->post('add_category')) { //print_r($_FILES); die;

			$this->form_validation->set_rules('name', 'Category Name', 'trim|required');
			if ($this->form_validation->run() == TRUE) { 
             	$insert_data = array(
					'name' => $this->input->post('name'),
					'app_view_type' => $this->input->post('app_view_type'),
					'parent_fk' => $this->input->post('parent_fk'),
					'visibilty' => $this->input->post('visibilty'),
					'in_carousel' => $this->input->post('in_carousel'),
					'image' => $this->amazon_s3_upload($_FILES['image'],"course_file_meta")

				);
				$this->category_list_model->insert_category($insert_data);
				page_alert_box('success','Category Added','Category Added successfully.');
            } 
        }	
        if($this->input->post('edit_category')) { //print_r($_FILES); die;
			$this->form_validation->set_rules('name', 'Category name', 'trim|required');
			
			if ($this->form_validation->run() == TRUE) { 
				if($_FILES['image']['name']!=''){
					$image=$this->amazon_s3_upload($_FILES['image'],"course_file_meta");
				}
				else{
					$image=$this->input->post('image_url');
				}
             	$update_data = array(
             		'id' => $this->input->get('category'),
             		'name' => $this->input->post('name'),
					'app_view_type' => $this->input->post('app_view_type'),
					'parent_fk' => $this->input->post('parent_fk'),
					'visibilty' => $this->input->post('visibilty'),
					'in_carousel' => $this->input->post('in_carousel'),
					'image' => $image
				);
				$this->category_list_model->update_category($update_data);
				page_alert_box('success','Category Updated','Category Updated successfully.');
				redirect(AUTH_PANEL_URL."course_product/course_category/add_category");
            } 
        }
        if($this->input->get('category')) {
        $this->db->where('id',$this->input->get('category'));
		$data_category['category'] =   $this->db->get("course_category")->row_array();
	//	echo $this->db->last_query();
        } 
        $data_category['main_categories']=$this->get_main_category();
		$data['page_data'] = $this->load->view('course_product/course/add_course_category',@$data_category,TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function category_list() {
		$data['page_title'] = "category List";
		$view_data['page']  = 'category List';
		$view_data['currency'] = $this->category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('course_product/course_category/category_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function get_main_category(){
		$this->db->where('parent_id',0);
		$return = $this->db->get("course_stream_name_master")->result_array();
		if($this->input->get("return") == "json"){
			echo json_encode($return);die;
		}
		return $return;
	}

	public function get_main_category_stream($id){
		$this->db->where('parent_fk',$id);
		$return = $this->db->get("course_category")->result_array();
		if($this->input->get("return") == "json"){
			echo json_encode($return);die;
		}
		return $return;
	}


	public function ajax_all_category_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'app_view_type',
			3 => 'parent_fk',
			4 => 'visibilty',
			5 => 'image',
			6 => 'in_carousel'
		);

		$query = "SELECT count(id) as total
				  FROM course_category";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT cc.*,mc.name as main_cat
				FROM   course_category as cc  
				join course_stream_name_master as mc on mc.id=cc.parent_fk where 1=1 ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
	
		$data = array();

		foreach ($result as $r) {  // preparing an array
			if($r->app_view_type==1){ $app_view_type='Horizontal'; } 
			if($r->app_view_type==2){ $app_view_type='Vertical'; }
			if($r->in_carousel==0){ $in_carousel='Hide'; } 
			if($r->in_carousel==1){ $in_carousel='Show'; }
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->main_cat;
			$nestedData[] = $r->name;
			$nestedData[] = $app_view_type;
			$nestedData[] = $r->visibilty;
			$nestedData[] = "<img src='".$r->image."' height='50px' width='50px'>";
			$nestedData[] = $in_carousel;
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='".AUTH_PANEL_URL."course_product/course_category/add_category?category=".$r->id."'>Edit</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function add_course_group(){
		if($this->input->get('tag_cache_cleared') != "" && 2 == base64_decode($this->input->get('tag_cache_cleared'))) {
			$data['page_toast'] = 'Cache cleared successfully';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
		}

		if(isset($_POST)) { 
			$this->form_validation->set_rules('name', 'Group Name', "required|is_unique[course_group_type.name]");
			
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$insert = array(
            		'name' => $this->input->post('name'),	
            		'status' => $this->input->post('status'),
					'created'=>milliseconds(),
					'modified'=>milliseconds()
            		);
            	$this->db->insert('course_group_type',$insert);
 
				$data['page_toast'] = 'Information Inserted  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }

        }
		//die('hell');
		$view_data['page']  = 'add_course_group';
		$data['page_data']=$this->load->view('course_product/course/add_course_group',$view_data,TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_all_group_list(){
		$confirm_alert='return confirm("Are you sure?")';
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name		
			0 => 'name',
			1 => 'status',
			2 => 'created',
			2 => 'modified'
		);

		$query = "SELECT count(id) as total
								FROM course_group_type 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT * FROM course_group_type";

		// getting records as per search parameters
		
		if (!empty($requestData['columns'][0]['search']['value'])) {  //salary
			$sql.=" where name LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		} 
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND status LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND status LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND status LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		
		$query = $this->db->query($sql)->result();
        //echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
		$sql .= " order by id asc";
		$result = $this->db->query($sql)->result();
		//echo "<pre>";print_r($result); echo count($result)-1;die;

		$data = array();
		//print_r($result);
		foreach ($result as $key => $r) {  // preparing an array
			$nestedData = array();

			
			$nestedData[] = $r->name;
			$nestedData[] = $r->status==1?'<span style="color:green">Enabled</span>':'<span style="color:red">Disabled</span>';
			$nestedData[] = date('d-M-y',($r->created/1000));
			$nestedData[] = date('d-M-y',($r->modified/1000));
			$action = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "course_product/Course_category/edit_course_group/" . $r->id . "'>Edit</a>&nbsp;
			<a class='btn-xs bold btn btn-danger' onclick='$confirm_alert' href='" . AUTH_PANEL_URL . "course_product/Course_category/delete_course_group/" . $r->id . "'>Delete</a>&nbsp;"
				;
			
			$nestedData[] = $action;
			
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function delete_course_group($id){
		$this->db->where('id',$id);
		$this->db->delete('course_group_type');
		if($this->db->affected_rows()==1){
			
			$data['page_toast'] = 'Information Updated  successfully.';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
			redirect(AUTH_PANEL_URL.'course_product/Course_category/add_course_group');
		}else{
			
			$data['page_toast'] = 'Information could not be Updated.';
			$data['page_toast_type'] = 'failed';
			$data['page_toast_title'] = 'Action performed.';
			redirect(AUTH_PANEL_URL.'course_product/Course_category/add_course_group');
		}
	}

	public function edit_course_group($id){
		if($this->input->post()) {
			
			$this->form_validation->set_rules('name', 'Group Name', 'required');
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	
            	$update = array(           		
            		'name'  => $this->input->post('name'),
					'status' => $this->input->post('status'),
					'modified'=>milliseconds()
            		);
            	$this->db->where('id',$id);
            	$this->db->update('course_group_type',$update);
            	//$this->
 
				$data['page_toast'] = 'Information Updated  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }
		}
		$view_data['page'] = 'edit_course_group';
		
		$view_data['group_data'] = $this->category_list_model->get_course_group_by_id($id);
		
		$data['page_data'] = $this->load->view('course_product/course/edit_course_group', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


}
