<?php
class Course_wise_transactions extends MX_Controller {

		public function __construct()
	{
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
		$this->load->model('Course_transactions_model');
		$this->load->library('grocery_CRUD');
	}

	public function index(){

		$view_data['page']  = 'course_wise_transactions';
		$data['page_title'] = "Course Transactions Details";

		$data['page_data'] = $this->load->view('course_product/course/course_wise_transactions', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function get_ajax_course_all_transactions_list(){

		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'course_id',
			1 => 'course_name',
			2 => 'total',
			3 => 'revenue',
			4 => 'trans_count',

		);

		$query = "SELECT count(distinct(instructor_id)) as total
								FROM course_transaction_record
								WHERE transaction_status = 1 and course_price > 0" ;
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT DISTINCT (
							ctr.course_id
							) AS course_id, sum( ctr.course_price ) AS total, count( DISTINCT (
							ctr.id
							) ) AS trans_count, sum( ctr.instructor_share ) AS revenue,cm.title as course_name
							FROM course_transaction_record as ctr
							 JOIN course_master cm on cm.id= ctr.course_id
							 where ctr.transaction_status = 1 and ctr.course_price > 0 
							GROUP BY ctr.course_id
					 ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND ctr.course_id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND cm.title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND total LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND revenue LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value']) ) {  //salary
			$sql.=" AND trans_count LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

	/*	if (!empty($requestData['columns'][9]['search']['value'])) {  //salary
			$date = explode(',',$requestData['columns'][9]['search']['value']);
			$start = strtotime($date[0])*1000;
			$end = (strtotime($date[1])*1000)+86400000;
			$sql.="  and  ctr.creation_time >= '$start' and ctr.creation_time <= '$end'"; // BETWEEN $start and $end";
		}                                                                                                                 */
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->course_id;
			$nestedData[] = $r->course_name;
			$nestedData[] = $r->total;
			$nestedData[] = $r->revenue;
			$nestedData[] = $r->trans_count;

			$action = "<a class='btn-xs bold  btn btn-info' href='" . AUTH_PANEL_URL . "course_product/course_transactions?course_id=$r->course_id&course_name=".urlencode($r->course_name)."'>View</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}




}
