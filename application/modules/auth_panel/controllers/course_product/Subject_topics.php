<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subject_topics extends MX_Controller {
	public function __construct()
		{
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
		$this->load->helper('url');
		$this->load->library('form_validation');

	}
	public function amazon_s3_upload($name,$aws_path) {

		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).str_replace([':', ' ', '/', '*','#','@','%'],"_",$_FILES["file"]['name']),
							'SourceFile' => $_FILES["file"]['tmp_name'],
							'ContentType' => 'image',
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array('param1' => 'value 1','param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	}
	public function get_all_subject(){

		$return = $this->db->get("course_subject_master")->result_array();
		if($this->input->get("return") == "json"){
			echo json_encode($return);die;
		}
		return $return;
	}

	public function get_topic_from_subject($subject_id){
		$this->db->where('subject_id',$subject_id);
		$return = $this->db->get("course_subject_topic_master")->result_array();

		if($this->input->get("return") == "json"){
			echo json_encode($return);die;
		}
		return $return;
	}

	public function add_subject(){
		if($this->input->post()) {
			$this->form_validation->set_rules('subject', 'Subject', 'required|is_unique[]');
		}
		$view_data['page']  = "add_subject";
		$data['page_data'] = $this->load->view('',$view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function subject_management(){
		if($this->input->post()) { //print_r($_POST); die;			
			$this->form_validation->set_rules('name', 'Subject Name', 'required|is_unique[course_subject_master.name]');
			if ($this->form_validation->run() == FALSE) {

      }else{
				$insert_data = array('name' => ucfirst($this->input->post('name')));
				$insert_data['creation_time'] = milliseconds();
				$insert_data['last_updated'] = milliseconds();
				if($_FILES && $_FILES['image']['name']){					
					$file  = $this->amazon_s3_upload($_FILES['image'],'course_file_meta');
					}else{					
					$file='Not Selected';
					}	
				$insert_data['image'] = $file;
				$this->db->insert('course_subject_master',$insert_data);
				page_alert_box('success','Subject Added','Subject has been added successfully');
      }
		}
		$data['page_data'] = $this->load->view('course_product/subject_topic_management/add_subject', array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function disable_subject($subject_id){
		$this->db->where('id',$subject_id);
	  $this->db->update('course_subject_master',array('status'=>1));
		page_alert_box('success','Subject Disabled','Subject has been disabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/subject_management');
	}


	public function delete_subject($subject_id){
		$this->db->where('id',$subject_id);
		$this->db->delete('course_subject_master');
		$this->db->where('subject_id',$topic_id);
		$this->db->delete('course_subject_topic_master');
		page_alert_box('success','Subject Deleted','Subject has been deleted successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/subject_management');
	}


	public function enable_subject($subject_id){
		$this->db->where('id',$subject_id);
	   $this->db->update('course_subject_master',array('status'=>0));
		page_alert_box('success','Subject Enabled','Subject has been enabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/subject_management');
	}

	public function ajax_subject_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'status',
			3 => 'creation',
			4 => 'updated'
		);
		$query = "SELECT count(id) as total FROM course_subject_master";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT csm.id, csm.name ,csm.status ,csm.color_code, csm.name_2,csm.image,
										DATE_FORMAT(FROM_UNIXTIME(csm.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation ,
										DATE_FORMAT(FROM_UNIXTIME(csm.last_updated/1000), '%d-%m-%Y %h:%i:%s') as updated
										FROM course_subject_master as csm ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND name_2 LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name;
			$nestedData[] = ($r->status == 1 )?'<b><i class="text-danger">Disabled</i></b>':'<b><i class="text-success">Enabled</i></b>';
			//$nestedData[] = '<img width="30" height="30" style="background:'.$r->color_code.'" src="'.$r->image.'">';
			$nestedData[] = $r->creation;
			$nestedData[] = $r->updated;
			if($r->status == 0 ){
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to disable?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/subject_topics/disable_subject/" . $r->id . "'>Disable</a>";
			}else{
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to enable?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "course_product/subject_topics/enable_subject/" . $r->id . "'> Enable</a>";
			}

			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "course_product/subject_topics/edit_subject/" . $r->id . "'><i class='fa fa-pencil'></i>&nbsp Edit</a>&nbsp;$control <a  onclick=\"return confirm('Warning !!!!  Do you really want to delete?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/subject_topics/delete_subject/" . $r->id . "'> Delete</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


		public function edit_subject($id) {
			if($this->input->post()) {
				$this->form_validation->set_message('edit_unique', 'Sorry, This subject name already exist.');
				$this->form_validation->set_rules('name', 'Subject Name', 'trim|required|edit_unique[course_subject_master.name.'.$id.']');
				$this->form_validation->set_rules('name_2', 'Subject Name(Hindi)', 'trim|required|edit_unique[course_subject_master.name_2.'.$id.']');
				
				if ($this->form_validation->run() == TRUE) {
	             	$update_data = array(
									'name' => ucfirst($this->input->post('name')),
									'name_2' => ucfirst($this->input->post('name_2')),
									'color_code' => ucfirst($this->input->post('color')),
									'last_updated' => milliseconds()
					);
					if($_FILES && $_FILES['image']['name']){					
						$file  = $this->amazon_s3_upload($_FILES['image'],'course_file_meta');
						$update_data['image'] = $file;
						}else{	

						$file='Not Selected';
						}	
				

					
					$this->db->where('id',$id);
					$this->db->update('course_subject_master',$update_data);
					page_alert_box('success','Subject Updated','Subject has been updated successfully');
					redirect(AUTH_PANEL_URL.'course_product/Subject_topics/subject_management');
	      }
			}
			$view_data['page']  = 'edit_subject';
			$data['page_title'] = "Subject edit";
			$this->db->where('id',$id);
			$view_data['subject'] = $this->db->get('course_subject_master')->row_array();
			$data['page_data'] = $this->load->view('course_product/subject_topic_management/edit_subject', $view_data, TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function is_topic_unique(){

		$subject_id=$this->input->post('subject_id');
		$topic=$this->input->post('topic');
		$query = $this->db->query("SELECT * FROM course_subject_topic_master where subject_id='$subject_id' and topic='$topic'");
		$query->num_rows();
		if ($query->num_rows()>0){
			$this->form_validation->set_message('is_topic_unique', 'This topic is already available in this subject');
			return FALSE;
		}
			return TRUE;
	}


	public function topics_management(){
		if($this->input->post()) { //print_r($_POST); die;
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic', 'Topic Name', 'required');
			if($this->input->post('topic') != ""){
				$this->form_validation->set_rules('topic', 'Topic Name', 'callback_is_topic_unique');
			}
			if ($this->form_validation->run($this) == FALSE) {

      }else{
				$insert_data = array('subject_id' => $this->input->post('subject_id'),
									'topic' => ucfirst($this->input->post('topic'))
			    				);
				$insert_data['creation_time'] = milliseconds();
				$insert_data['last_updated'] = milliseconds();

				$this->db->insert('course_subject_topic_master',$insert_data);
				page_alert_box('success','Topic Added','Topic has been added successfully');

    	}

		}
		$view_data['subject_list'] = $this->db->where('status',0)->get('course_subject_master')->result_array();
		$data['page_data'] = $this->load->view('course_product/subject_topic_management/add_topic', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function disable_topic($topic_id){
		$this->db->where('id',$topic_id);
	  $this->db->update('course_subject_topic_master',array('status'=>1));
		page_alert_box('success','Topic Disabled','Topic has been disabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/topics_management');
	}

	public function delete_topic($topic_id){
		$this->db->where('id',$topic_id);
	  $this->db->delete('course_subject_topic_master');
		page_alert_box('success','Topic Deleted','Topic has been deleted successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/topics_management');
	}

	public function enable_topic($topic_id){
		$this->db->where('id',$topic_id);
	   $this->db->update('course_subject_topic_master',array('status'=>0));
		page_alert_box('success','Topic Enabled','SubjTopicect has been enabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/topics_management');
	}
	public function ajax_topic_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'subject_id',
			2 => 'topic',
		
		);
		$query = "SELECT count(id) as total FROM course_subject_topic_master";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT course_subject_topic_master.*,course_subject_master.name ,topic_2,
						DATE_FORMAT(FROM_UNIXTIME(course_subject_topic_master.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation ,
						DATE_FORMAT(FROM_UNIXTIME(course_subject_topic_master.last_updated/1000), '%d-%m-%Y %h:%i:%s') as updated
						FROM course_subject_topic_master
						JOIN course_subject_master on course_subject_master.id=course_subject_topic_master.subject_id";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {
			$sql.=" AND course_subject_topic_master.id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			$sql.=" AND course_subject_topic_master.topic LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name;

			$nestedData[] = $r->topic;
			$nestedData[] = ($r->status == 1 )?'Disabled':'Enabled';
			$nestedData[] = $r->creation;
			$nestedData[] = $r->updated;
			if($r->status == 0 ){
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to disable?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/subject_topics/disable_topic/" . $r->id . "'>Disable</a>";
			}else{
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to enable?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "course_product/subject_topics/enable_topic/" . $r->id . "'> Enable</a>";
			}
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "course_product/subject_topics/edit_topic/" . $r->id . "'>Edit</a>&nbsp;$control <a  onclick=\"return confirm('Warning !!!!  Do you really want to delete?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/subject_topics/delete_topic/" . $r->id . "'> Delete</a> ";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function edit_topic_unique(){

		$subject_id=$this->input->post('subject_id');
		$topic=$this->input->post('topic');
		$id =$this->input->post('id');
		$query = $this->db->query("SELECT * FROM course_subject_topic_master where subject_id='$subject_id' and topic ='$topic' and id !='$id'");
		if ($query->num_rows()>0){
			$this->form_validation->set_message('edit_topic_unique', 'This topic is already available in this subject.');
			return false;
		}
			return true;
	}

	public function edit_topic($id) {
		if($this->input->post()) {

			$this->form_validation->set_rules('topic', 'Topic ', 'trim|required');
			if($this->input->post('topic') != ""){
				$this->form_validation->set_rules('topic', 'Topic Name', 'callback_edit_topic_unique');
			}
			if ($this->form_validation->run($this) == TRUE) {
             	$update_data = array(
             		'id' => $id,
								'topic' => ucfirst($this->input->post('topic')),
								'topic_2' => ucfirst($this->input->post('topic_2'))
				);
				$update_data['last_updated'] = milliseconds();

				$this->db->where('id',$id);
				$this->db->update('course_subject_topic_master',$update_data);
				page_alert_box('success','Topic Updated','Topic has been updated successfully');
				redirect(AUTH_PANEL_URL.'course_product/Subject_topics/topics_management');
				
        }
		}

		$view_data['page']  = 'edit_topic';
		$data['page_title'] = "Topic edit";
		$view_data['subject_list'] = $this->db->get('course_subject_master')->result_array();
		$view_data['topics_management'] = $this->db->where('id',$id)->get('course_subject_topic_master')->row_array();
		$data['page_data'] = $this->load->view('course_product/subject_topic_management/edit_topic', $view_data, TRUE);
		
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
		
	}



}
