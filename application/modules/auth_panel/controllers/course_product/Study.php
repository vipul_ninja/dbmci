<?php 
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Study extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
		$this->load->model('Course_list_model');
		$this->load->helper('url');
		$this->load->library('form_validation');

	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public function study_add_course_product(){	
		if($this->input->post()){
			$data = array(
				"title"=>$this->input->post('title'),
				// "course_main_fk"=>$this->input->post('course_main_fk'),
				// "course_category_fk"=>$this->input->post('course_category_fk'),
				// "subject_id"=>$this->input->post('subject_id'),
				"description"=>$this->input->post('description'),
				"course_for"=>$this->input->post('course_for'),
				"tags"=>$this->input->post('tags'),
				"mrp"=>$this->input->post('mrp'),
				"for_dams"=>$this->input->post('for_dams'),
				"non_dams"=>$this->input->post('non_dams'),
				"instructor_id"=>$this->input->post('instructor_id'),
				"instructor_share"=>$this->input->post('instructor_share'),
				'created_by'=>$this->session->userdata('active_backend_user_id'),
				'creation_time'=>milliseconds(),
				'last_updated'=>milliseconds(),
				"course_type"=>'1',
				);
			$this->db->insert('course_master',$data);
			$id = $this->db->insert_id();

			/* price manager we have course id here */
			//get all mcc_code available round(520.34345,2)
			$cm = $this->db->get('currency_value_master')->result_array();
			foreach($cm as $c){
				$p_row = array(
					"course_id"=> $id,
					"mcc_code"=> $c['mcc_code'],
					"mrp"=> round($this->input->post('mrp')/$c['value'],2),
					"for_dams"=> round($this->input->post('for_dams')/$c['value'],2),
					"non_dams"=> round($this->input->post('non_dams')/$c['value'],2),
					);
				$this->db->insert('course_price_master',$p_row);
			}
			page_alert_box('success','Course Added','Course Added successfully.');
			/* put a log */
			backend_log_genration($this->session->userdata('active_backend_user_id'),'Added new course -: '.$this->input->post('title'),'COURSE');
		//	redirect(AUTH_PANEL_URL."course_product/course/edit_course_page?course_id=".$id);
			redirect(AUTH_PANEL_URL."course_product/Study/study_all_Courses_list");

		}
		$data['page_data'] = $this->load->view('course_product/study/add_course', $view_data=array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

public function study_all_Courses_list(){
	
		$data['page_data'] = $this->load->view('course_product/course/all_courses_list', $view_data=array('study_type'=>'1'), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	public function get_all_subject(){

		$return = $this->db->get("course_subject_master")->result_array();
		if($this->input->get("return") == "json"){
			echo json_encode($return);die;
		}
		return $return;
	}

	public function get_topic_from_subject($subject_id){
		$this->db->where('subject_id',$subject_id);
		$return = $this->db->get("course_subject_topic_master")->result_array();

		if($this->input->get("return") == "json"){
			echo json_encode($return);die;
		}
		return $return;
	}

	public function add_subject(){
		if($this->input->post()) {
			$this->form_validation->set_rules('subject', 'Subject', 'required|is_unique[]');
		}
		$view_data['page']  = "add_subject";
		$data['page_data'] = $this->load->view('',$view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	

	public function disable_subject($subject_id){
		$this->db->where('id',$subject_id);
	  $this->db->update('course_subject_master',array('status'=>1));
		page_alert_box('success','Subject Disabled','Subject has been disabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/subject_management');
	}

	public function enable_subject($subject_id){
		$this->db->where('id',$subject_id);
	   $this->db->update('course_subject_master',array('status'=>0));
		page_alert_box('success','Subject Enabled','Subject has been enabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/subject_management');
	}

	public function ajax_subject_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'creation',
			3 => 'updated'
		);
		$query = "SELECT count(id) as total FROM course_subject_master";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT csm.id, csm.name ,csm.status ,
										DATE_FORMAT(FROM_UNIXTIME(csm.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation ,
										DATE_FORMAT(FROM_UNIXTIME(csm.last_updated/1000), '%d-%m-%Y %h:%i:%s') as updated
										FROM course_subject_master as csm ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name;
			$nestedData[] = ($r->status == 1 )?'Disabled':'Enabled';
			$nestedData[] = $r->creation;
			$nestedData[] = $r->updated;
			if($r->status == 0 ){
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to disable?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/subject_topics/disable_subject/" . $r->id . "'>Disable</a>";
			}else{
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to enable?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "course_product/subject_topics/enable_subject/" . $r->id . "'> Enable</a>";
			}

			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "course_product/subject_topics/edit_subject/" . $r->id . "'><i class='fa fa-pencil'></i>&nbsp Edit</a>&nbsp;$control";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


		public function edit_subject($id) {
			if($this->input->post()) {
				$this->form_validation->set_message('edit_unique', 'Sorry, This subject name already exist.');
				$this->form_validation->set_rules('name', 'Subject Name', 'trim|required|edit_unique[course_subject_master.name.'.$id.']');
				if ($this->form_validation->run() == TRUE) {
	             	$update_data = array(
									'name' => ucfirst($this->input->post('name')),
									'last_updated' => milliseconds()
					);
					$this->db->where('id',$id);
					$this->db->update('course_subject_master',$update_data);
					page_alert_box('success','Subject Updated','Subject has been updated successfully');
					redirect(AUTH_PANEL_URL.'course_product/Subject_topics/subject_management');
	      }
			}
			$view_data['page']  = 'edit_subject';
			$data['page_title'] = "Subject edit";
			$this->db->where('id',$id);
			$view_data['subject'] = $this->db->get('course_subject_master')->row_array();
			$data['page_data'] = $this->load->view('course_product/subject_topic_management/edit_subject', $view_data, TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function is_topic_unique(){

		$subject_id=$this->input->post('subject_id');
		$topic=$this->input->post('topic');
		$query = $this->db->query("SELECT * FROM course_subject_topic_master where subject_id='$subject_id' and topic='$topic'");
		$query->num_rows();
		if ($query->num_rows()>0){
			$this->form_validation->set_message('is_topic_unique', 'This topic is already available in this subject');
			return FALSE;
		}
			return TRUE;
	}


	
	public function disable_topic($topic_id){
		$this->db->where('id',$topic_id);
	  $this->db->update('course_subject_topic_master',array('status'=>1));
		page_alert_box('success','Topic Disabled','Topic has been disabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/topics_management');
	}

	public function enable_topic($topic_id){
		$this->db->where('id',$topic_id);
	   $this->db->update('course_subject_topic_master',array('status'=>0));
		page_alert_box('success','Topic Enabled','SubjTopicect has been enabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/Subject_topics/topics_management');
	}
	public function ajax_topic_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'subject_id',
			2 => 'topic'

		);
		$query = "SELECT count(id) as total FROM course_subject_topic_master";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT course_subject_topic_master.*,course_subject_master.name ,
						DATE_FORMAT(FROM_UNIXTIME(course_subject_topic_master.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation ,
						DATE_FORMAT(FROM_UNIXTIME(course_subject_topic_master.last_updated/1000), '%d-%m-%Y %h:%i:%s') as updated
						FROM course_subject_topic_master
						JOIN course_subject_master on course_subject_master.id=course_subject_topic_master.subject_id";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {
			$sql.=" AND course_subject_topic_master.id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			$sql.=" AND course_subject_topic_master.topic LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name;

			$nestedData[] = $r->topic;
			$nestedData[] = ($r->status == 1 )?'Disabled':'Enabled';
			$nestedData[] = $r->creation;
			$nestedData[] = $r->updated;
			if($r->status == 0 ){
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to disable?');\" class=' btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "course_product/subject_topics/disable_topic/" . $r->id . "'>Disable</a>";
			}else{
				$control = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to enable?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "course_product/subject_topics/enable_topic/" . $r->id . "'> Enable</a>";
			}
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "course_product/subject_topics/edit_topic/" . $r->id . "'>Edit</a>&nbsp;$control";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function edit_topic_unique(){

		$subject_id=$this->input->post('subject_id');
		$topic=$this->input->post('topic');
		$id =$this->input->post('id');
		$query = $this->db->query("SELECT * FROM course_subject_topic_master where subject_id='$subject_id' and topic ='$topic' and id !='$id'");
		if ($query->num_rows()>0){
			$this->form_validation->set_message('edit_topic_unique', 'This topic is already available in this subject.');
			return false;
		}
			return true;
	}

	public function edit_topic($id) {
		if($this->input->post()) {

			$this->form_validation->set_rules('topic', 'Topic ', 'trim|required');
			if($this->input->post('topic') != ""){
				$this->form_validation->set_rules('topic', 'Topic Name', 'callback_edit_topic_unique');
			}
			if ($this->form_validation->run($this) == TRUE) {
             	$update_data = array(
             		'id' => $id,
								'topic' => ucfirst($this->input->post('topic'))
				);
				$update_data['last_updated'] = milliseconds();

				$this->db->where('id',$id);
				$this->db->update('course_subject_topic_master',$update_data);
				page_alert_box('success','Topic Updated','Topic has been updated successfully');
        }
		}

		$view_data['page']  = 'edit_topic';
		$data['page_title'] = "Topic edit";
		$view_data['subject_list'] = $this->db->get('course_subject_master')->result_array();
		$view_data['topics_management'] = $this->db->where('id',$id)->get('course_subject_topic_master')->row_array();
		$data['page_data'] = $this->load->view('course_product/subject_topic_management/edit_topic', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	

	public function edit_course_basic_info_study(){
		if($this->input->post()){
			$data = array(
				"title"=>$this->input->post('title'),
				// "course_main_fk"=>$this->input->post('course_main_fk'),
				// "course_category_fk"=>$this->input->post('course_category_fk'),
				// "subject_id"=>$this->input->post('subject_id'),
				"description"=>$this->input->post('description'),
				"course_for"=>$this->input->post('course_for'),
				"course_attribute"=>$this->input->post('course_attribute'),
				"course_sp"=>$this->input->post('course_sp'),
				"tags"=>$this->input->post('tags'),
				"practice_id"=>$this->input->post('practice_id'),
				"mrp"=>$this->input->post('mrp'),
				"for_dams"=>$this->input->post('for_dams'),
				"non_dams"=>$this->input->post('non_dams'),
				"left_text"=>$this->input->post('left_text'),
				"right_text"=>$this->input->post('right_text'),
			
				//"instructor_id"=>$this->input->post('instructor_id'),
				//"instructor_share"=>$this->input->post('instructor_share'),
				"gst_include"=>$this->input->post('gst_include'),
				"is_new"=>$this->input->post('is_new'),
				"is_locked"=>$this->input->post('is_locked'),
				"in_suggested_list"=>$this->input->post('in_suggested_list'),
				"fake_learner"=>$this->input->post('fake_learner'),
				"free_ids"=>$this->input->post('free_ids'),
				"validity"=>$this->input->post('validity'),
				'last_updated'=>milliseconds()
				);

			if($this->input->post('start_date') != "" && $this->input->post('end_date') != "" ){
				$data["start_date"] = date("Y-m-d", strtotime($this->input->post('start_date')));
				$data["end_date"] =  date("Y-m-d", strtotime($this->input->post('end_date')));
			}elseif($this->input->post('start_date') == "" || $this->input->post('end_date') == ""){
				$data["start_date"] = "";
				$data["end_date"] =  "";
			}

			$id = $this->input->get('course_id');
			$this->db->where('id',$id);
			$this->db->update('course_master',$data);

			page_alert_box('success','Course Edited','Course basic information updated successfully.');
			/* put a log */
			backend_log_genration($this->session->userdata('active_backend_user_id'),
			'Edited the basic info. of course -: '.$this->input->post('title'),
			'COURSE');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$id);
			die;
			/* price manager we have course id here */
			//get all mcc_code available round(520.34345,2)
			$cm = $this->db->get('currency_value_master')->result_array();
			foreach($cm as $c){
				$p_row = array(
					"course_id"=> $id,
					"mcc_code"=> $c['mcc_code'],
					"mrp"=> round($this->input->post('mrp')/$c['value'],2),
					"for_dams"=> round($this->input->post('for_dams')/$c['value'],2),
					"non_dams"=> round($this->input->post('non_dams')/$c['value'],2),
					);
				$this->db->insert('course_price_master',$p_row);
			}
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$id);
		}
	}


	public function _example_output($output = null)
	{
		$this->load->view(AUTH_TEMPLATE.'grocery_crud_template',(array)$output);
	}

	public function amazon_s3_upload($name,$aws_path) {
		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).$_FILES["file"]["name"],
							'SourceFile' => $_FILES["file"]["tmp_name"],
							'ContentType' => 'image',
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];

	}


	public function add_course_product(){		
		if($this->input->post()){
			$data = array(
				"title"=>$this->input->post('title'),
				// "course_main_fk"=>$this->input->post('course_main_fk'),
				// "course_category_fk"=>$this->input->post('course_category_fk'),
				// "subject_id"=>$this->input->post('subject_id'),
				"course_attribute"=>$this->input->post('course_attribute'),
				"course_sp"=>$this->input->post('course_sp'),
				
				"description"=>$this->input->post('description'),
				"course_for"=>$this->input->post('course_for'),
				"tags"=>$this->input->post('tags'),
				"mrp"=>$this->input->post('mrp'),
				"left_text"=>$this->input->post('left_text'),
				"right_text"=>$this->input->post('right_text'),
				"for_dams"=>$this->input->post('for_dams'),
				"non_dams"=>$this->input->post('non_dams'),
				"instructor_id"=>$this->input->post('instructor_id'),
				"instructor_share"=>$this->input->post('instructor_share'),
				'created_by'=>$this->session->userdata('active_backend_user_id'),
				'creation_time'=>milliseconds(),
				'last_updated'=>milliseconds()
				);
			$this->db->insert('course_master',$data);
			$id = $this->db->insert_id();

			/* price manager we have course id here */
			//get all mcc_code available round(520.34345,2)
			$cm = $this->db->get('currency_value_master')->result_array();
			foreach($cm as $c){
				$p_row = array(
					"course_id"=> $id,
					"mcc_code"=> $c['mcc_code'],
					"mrp"=> round($this->input->post('mrp')/$c['value'],2),
					"for_dams"=> round($this->input->post('for_dams')/$c['value'],2),
					"non_dams"=> round($this->input->post('non_dams')/$c['value'],2),
					);
				$this->db->insert('course_price_master',$p_row);
			}
			page_alert_box('success','Course Added','Course Added successfully.');
			/* put a log */
			backend_log_genration($this->session->userdata('active_backend_user_id'),
			'Added new course -: '.$this->input->post('title'),
			'COURSE');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page?course_id=".$id);

		}
		$data['page_data'] = $this->load->view('course_product/course/add_course', $view_data=array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function edit_course_basic_info(){
		if($this->input->post()){
			$data = array(
				"title"=>$this->input->post('title'),
				// "course_main_fk"=>$this->input->post('course_main_fk'),
				// "course_category_fk"=>$this->input->post('course_category_fk'),
				// "subject_id"=>$this->input->post('subject_id'),
				"description"=>$this->input->post('description'),
				"course_for"=>$this->input->post('course_for'),
				"course_attribute"=>$this->input->post('course_attribute'),
				"course_sp"=>$this->input->post('course_sp'),
				"tags"=>$this->input->post('tags'),
				"mrp"=>$this->input->post('mrp'),
				"for_dams"=>$this->input->post('for_dams'),
				"non_dams"=>$this->input->post('non_dams'),
				"left_text"=>$this->input->post('left_text'),
				"right_text"=>$this->input->post('right_text'),
			
				//"instructor_id"=>$this->input->post('instructor_id'),
				//"instructor_share"=>$this->input->post('instructor_share'),
				"gst_include"=>$this->input->post('gst_include'),
				"is_new"=>$this->input->post('is_new'),
				"is_locked"=>$this->input->post('is_locked'),
				"in_suggested_list"=>$this->input->post('in_suggested_list'),
				"fake_learner"=>$this->input->post('fake_learner'),
				"free_ids"=>$this->input->post('free_ids'),
				"validity"=>$this->input->post('validity'),
				'last_updated'=>milliseconds()
				);

			if($this->input->post('start_date') != "" && $this->input->post('end_date') != "" ){
				$data["start_date"] = date("Y-m-d", strtotime($this->input->post('start_date')));
				$data["end_date"] =  date("Y-m-d", strtotime($this->input->post('end_date')));
			}elseif($this->input->post('start_date') == "" || $this->input->post('end_date') == ""){
				$data["start_date"] = "";
				$data["end_date"] =  "";
			}

			$id = $this->input->get('course_id');
			$this->db->where('id',$id);
			$this->db->update('course_master',$data);

			page_alert_box('success','Course Edited','Course basic information updated successfully.');
			/* put a log */
			backend_log_genration($this->session->userdata('active_backend_user_id'),
			'Edited the basic info. of course -: '.$this->input->post('title'),
			'COURSE');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page?course_id=".$id);
			die;
			/* price manager we have course id here */
			//get all mcc_code available round(520.34345,2)
			$cm = $this->db->get('currency_value_master')->result_array();
			foreach($cm as $c){
				$p_row = array(
					"course_id"=> $id,
					"mcc_code"=> $c['mcc_code'],
					"mrp"=> round($this->input->post('mrp')/$c['value'],2),
					"for_dams"=> round($this->input->post('for_dams')/$c['value'],2),
					"non_dams"=> round($this->input->post('non_dams')/$c['value'],2),
					);
				$this->db->insert('course_price_master',$p_row);
			}
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page?course_id=".$id);
		}
	}

	public function all_Course(){

			$crud = new grocery_CRUD();
			$crud->unset_export();
			$crud->unset_print();
			$crud->set_table('course_master');
			$crud->columns(array('title','course_main_fk','course_category_fk'));
			$crud->set_relation('course_main_fk','master_category','text');
			$crud->set_relation('course_category_fk','course_category','name');
			$crud->display_as('course_main_fk','Main category')->display_as('course_category_fk','Sub category');
			$crud->add_action('View', 'heelo', 'hello','btn btn-xs btn-success',array($this,'just_a_test'));
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_read();
			$crud->unset_delete();
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function just_a_test($primary_key , $row)
	{
		return AUTH_PANEL_URL."course_product/course/edit_course_page?course_id=".$primary_key;
	}

	public function all_courses_list(){
		$data['page_data'] = $this->load->view('course_product/course/all_courses_list', $view_data=array('study_type'=>'0'), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_all_courses_list()
	{

		$user_data = $this->session->userdata('active_user_data');
		$is_instructor = $user_data->instructor_id;
		$id = $user_data->id;
		$where = " cm.state = 0";
		if($is_instructor == 1){
			$where = " cm.instructor_id = $id AND cm.state = 0";
		}

	$study_type=	$this->input->get('study_type');
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'title',
			1 => 'stream',
			2 => 'category',
			3 => 'Subject',
			4 => 'publish',
			5 => 'is_new',
			6 => 'course_rating_count',
			7 => 'course_for',
			8 => 'creation_time',
			9 => 'last_updated'
		);
		$query = "SELECT count(cm.id) as total
								FROM course_master cm where course_type=	$study_type  and $where
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT cm.* , mc.text as stream ,cc.name as category , csm.name as subject ,
					 DATE_FORMAT(FROM_UNIXTIME(SUBSTR(cm.creation_time,1,10)), '%d-%m-%Y') as creation_time ,
					 DATE_FORMAT(FROM_UNIXTIME(SUBSTR(cm.last_updated,1,10)), '%d-%m-%Y') as  last_updated
					 FROM course_master as cm
					 left join master_category as mc on mc.id = cm.course_main_fk
					 left join course_category as cc on cc.id = cm.course_category_fk
					 left join course_subject_master as csm on csm.id = cm.subject_id where course_type=$study_type
					 and 
					 $where ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND title LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND mc.text LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND cc.name LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND csm.name LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value']) || $requestData['columns'][4]['search']['value'] == 0) {  //salary
			$sql.=" AND publish LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value']) || $requestData['columns'][5]['search']['value'] == 0 ) {  //salary
			$sql.=" AND is_new LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}
		if ($requestData['columns'][7]['search']['value'] >= 0 ) {  //salary
			$sql.=" AND cm.course_for LIKE '" . $requestData['columns'][7]['search']['value'] . "%' ";
		}
		if ($requestData['columns'][8]['search']['value'] >= 0 ) {  //salary
			$sql.=" AND cm.creation_time LIKE '" . $requestData['columns'][7]['search']['value'] . "%' ";
		}
		if ($requestData['columns'][9]['search']['value'] >= 0 ) {  //salary
			$sql.=" AND cm.last_updated LIKE '" . $requestData['columns'][7]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			 if($r->publish == 1 ){$publish_status = '<span class="badge btn-success">Published</span>';}elseif($r->publish == 2){$publish_status ='<span class="badge btn-danger ">Requested</span>';}else{$publish_status ="--";}
			$nestedData[] = substr($r->title,0,50).(strlen($r->title)>50?' ...':'');

			$nestedData[] = $r->stream;
			//$nestedData[] = $r->category;
			$nestedData[] = substr($r->category,0,10).(strlen($r->category)>10?'...':'');
			$nestedData[] = $r->subject;
			//$nestedData[] = ($r->publish == 1 )?'<span class="badge ">Published</span>':"--";
			$nestedData[] = $publish_status;
			$nestedData[] = ($r->is_new == 1 )?'<span class="badge bg-important">New</span>':"--";
			$star="";
			for($i=0;$i<$r->course_rating_count;$i++){
				$star .='<i class="fa fa-star text-danger" aria-hidden="true"> </i> ';
			}
			if($i<5){
				$j = 5 - $i;
				for($i=0;$i<$j;$i++){
				$star .='<i class="fa fa-star-o text-danger" aria-hidden="true"> </i> ';
				}
			}
			$nestedData[] = $star.' <small class="bold">('.$r->course_rating_count.')</small>';

			if($r->course_for == 2 ){
				$nestedData[] = 'Both';
			}else{
				$nestedData[] = ($r->course_for == 0 )?'dams':'non-dams';
			}

			$nestedData[] = ($r->in_suggested_list ==1 )?'yes':'no';
			$nestedData[] = $r->creation_time;
			$nestedData[] = $r->last_updated;
			if($study_type=='1'){
				$action = "<a class='btn-xs bold  btn btn-info' href='" . AUTH_PANEL_URL . "course_product/course/edit_course_page_study?course_id=" . $r->id . "'>Edit</a>";
				
			}else{
				$action = "<a class='btn-xs bold  btn btn-info' href='" . AUTH_PANEL_URL . "course_product/course/edit_course_page?course_id=" . $r->id . "'>Edit</a>";
				
			}

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function get_all_instructor(){
		$this->db->select('id');
		$this->db->select('username as name');
		$this->db->select('status');
		$this->db->where('instructor_id',1);
		$r = $this->db->get('backend_user')->result_array();

		if($this->input->get('return') == "json" ){
			echo json_encode($r);die;
		}
		return $r;
	}

	public function get_instructor_basic_info($id){
		$this->db->where('id',$id);
		$this->db->select('username as name');
		$this->db->select('email');
		$this->db->select('profile_picture');
		$r = $this->db->get('backend_user')->row_array();

		$this->db->where('user_id',$id);
		$r['instuctor_info'] = $this->db->get('course_instructor_information')->row_array();

		if($this->input->get('return') == "json" ){
			echo json_encode($r);die;
		}
		return $r;
	}

	public function edit_course_page(){
		$c_id = $this->input->get('course_id');
		if(!$c_id){show_404();}
		if(isset($_POST['filter_save'])){
			$this->save_filter();
		}
		if(isset($_POST['make_course_category'])){
			$this->add_category_check();
		}
		if(isset($_POST['make_save_Type'])){
			$this->add_save_check();
		}
		if(isset($_POST['make_content_Type'])){
			$this->add_content_check();
		}
		$view_data['course_data'] =  $this->get_course_basic_information($c_id);
		$view_data['subject_data'] = $this->db->get('course_subject_master')->result_array();
		$view_data['topic_data_for_json'] = $this->db->get('course_subject_topic_master')->result_array();

		$data['page_data'] = $this->load->view('course_product/course/edit_course', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	private function save_filter(){
		//echo "<pre>";print_r($_POST);die;
		if(count($this->input->post('Session')) < 1 || count($this->input->post('Course')) < 1 || count($this->input->post('CourseGroup')) < 1 ||
			count($this->input->post('Course')) < 1 || count($this->input->post('Batch')) < 1 ){
			page_alert_box('error','Filter','Invalid input parameter for filter.');
		    return ;
		}

		$out = $temp =array();

		if($_POST['Session'][0] == '*'){
			$_POST['Session'] = array('0'=>'*');
		}
		if($_POST['Course'][0] == '*'){
			$_POST['Course'] = array('0'=>'*');
		}
		if($_POST['CourseGroup'][0] == '*'){
			$_POST['CourseGroup'] = array('0'=>'*');
		}
		if($_POST['Course'][0] == '*'){
			$_POST['Course'] = array('0'=>'*');
		}
		if($_POST['Batch'][0] == '*'){
			$_POST['Batch'] = array('0'=>'*');
		}

		$f_name = "f_".time();
		$added_by = $this->session->userdata('active_backend_user_id');
		$price = $_POST['price'];
		$creation_time = milliseconds();
		foreach($_POST['Session'] as $y){
			foreach($_POST['FranchiseName'] as $f){
				foreach($_POST['CourseGroup'] as $cg){
					foreach($_POST['Course'] as $c){
						foreach($_POST['Batch'] as $b){
							$temp =array();
							$temp['Session'] = $y;
							$temp['FranchiseName'] = $f;
							$temp['CourseGroup'] = $cg;
							$temp['Course'] = $c;
							$temp['Batch'] = $b;

							$query = "INSERT IGNORE INTO course_filtration (course_id , FranchiseName, CourseGroup, Session, Course, Batch , f_name , price , creation_time , added_by  )
										VALUES ('".$_POST['course_id']."',
												'".$temp['FranchiseName']."',
												'".$temp['CourseGroup']."',
												'".$temp['Session']."',
												'".$temp['Course']."',
												'".$temp['Batch']."' ,
												'".$f_name."' ,
												'".$price."' ,
												'".$creation_time."' ,
												'".$added_by."' )
												";
							$this->db->query($query);
						}
					}
				}
			}
		}
		page_alert_box('success','Filter','Filter for course set successfully.');

	}

	public function update_cover_image(){
		if($_POST && $_FILES['cover_image']['name']!==''){
			$image_info = getimagesize($_FILES["cover_image"]["tmp_name"]);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
			if($image_width == $image_height){
				$file  = $this->amazon_s3_upload($_FILES['cover_image'],"course_file_meta");
				$this->db->where('id',$this->input->post('course_id'));
				$this->db->update('course_master',array('cover_image'=>$file));
				page_alert_box('success','Course Image','Course cover image updated successfully.');
			}
			else{
				page_alert_box('error','Course Image','Course cover image should have same height and width');
			}

		}
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$this->input->post('course_id'));
	}

	public function update_color(){
		if($_POST ){
		
		$color=	$this->input->post('color');
		if($color!==''){
				$this->db->where('id',$this->input->post('course_id'));
				$this->db->update('course_master',array('color_code'=>$color));
				page_alert_box('success','Course Image','Color code  updated successfully.');
			}
			else{
				page_alert_box('error','Course Image','Choose color to submit the form');
			}

		}
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$this->input->post('course_id'));
	}
	public function update_color_header_image(){
		if($_POST ){
		
			$color=	$this->input->post('color_code');
			if($color!==''){
				$this->db->where('id',$this->input->post('course_id'));
				$this->db->update('course_master',array('color_header'=>$color));
				page_alert_box('success','Course Wall Image','Color code  updated successfully.');
			}
			else{
				page_alert_box('error','Course Wall Image','Choose color to submit the form');
			}

		}
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$this->input->post('course_id'));
	}


	public function update_desc_header_image(){
		if($_POST && $_FILES['desc_header_image']['name']!==''){
			$image_info = getimagesize($_FILES["desc_header_image"]["tmp_name"]);
			$image_width = $image_info[0];
			$image_height = $image_info[1];

			if($image_width != $image_height){
				$file  = $this->amazon_s3_upload($_FILES['desc_header_image'],"course_file_meta");
				$this->db->where('id',$this->input->post('course_id'));
				$this->db->update('course_master',array('desc_header_image'=>$file));
				page_alert_box('success','Course Header Image','Course header image updated successfully.');
			}
			else{
				page_alert_box('error','Course Header Image','Course header image should not have same height and width');
			}

		}
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$this->input->post('course_id'));
	}

	public function update_cover_video(){
		if($_POST && $_FILES['cover_video']['name']!==''){
			$file  = $this->amazon_s3_upload($_FILES['cover_video'],"course_file_meta");
			$this->db->where('id',$this->input->post('course_id'));
			$this->db->update('course_master',array('cover_video'=>$file));
			page_alert_box('success','Course Picture','Course cover picture updated successfully.');
		}
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$this->input->post('course_id'));
	}

	public function get_course_basic_information($id){
		$this->db->where('id',$id);
		$r = $this->db->get('course_master')->row_array();
		if($this->input->get('return') == "json"){
			echo json_encode($r);die;
		}
		return $r;
	}

	public function request_course_publish($id){
		$cover_image = $this->Course_list_model->get_course_image_exist_by_id($id);
		$topic_exist = $this->Course_list_model->get_topic_exist_in_course($id);
		$error_message='';
		if(!$cover_image['image']){
			$error_message.='You can not request to publish course without uploading cover image.<br>';
		}
		if(!$topic_exist){
			$error_message.='You can not request to publish course without having topic and topic content in it respectively also you need to set a topic as preview from action menu available in topic.';
		}
		if($error_message){
			$this->session->set_flashdata('image_not_exist', $error_message);
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$id);
			die;
		}
		$this->db->where('id',$id);
		$this->db->update('course_master',array('publish'=>2));
		page_alert_box('success','Requested Successfully','Request to publish Course has been done successfully.');
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$id);
	}

	public function set_course_publish($id){
		$cover_image = $this->Course_list_model->get_course_image_exist_by_id($id);
		$topic_exist = $this->Course_list_model->get_topic_exist_in_course($id);
		$error_message='';

		if(!$cover_image['image']){
			$error_message.='You can not publish course without uploading cover image.<br>';
		}
		if(!$topic_exist){
			$error_message.='You can not publish course without having topic and topic content in it respectively also you need to set a topic as preview from action menu available in topic.';
		}
		if($error_message){
			$this->session->set_flashdata('image_not_exist', $error_message);
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$id);
			die;
		}

		$this->db->where('id',$id);
		$this->db->update('course_master',array('publish'=>1));
		page_alert_box('success','Course published','Course added to publish list successfully.');
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$id);
	}

	public function set_course_unpublish($id){
		$this->db->where('id',$id);
		$this->db->update('course_master',array('publish'=>0));
		page_alert_box('warning','Course Unpublished','Course removed from publish list successfully.');
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$id);
	}

	public function update_faq_order(){
		$count = 1 ;
		foreach($_POST as $value){
			$this->db->where('id',$value);
			$this->db->update("course_faq_master",array('position'=>$count));
			$count++;
		}
		echo json_encode(array('status'=>true));
	}

	public function update_topic_order($course_id){
		$count = 1 ;
		foreach($_POST as $value){
			$this->db->where('id',$value);
			$this->db->where('course_fk',$course_id);
			$this->db->update("course_segment_master",array('position'=>$count));
			$count++;
		}
		echo json_encode(array('status'=>true));
	}

	public function save_course_topic(){
		if($_POST){
			$course_id = $this->input->post('course_id');
			$subject_id = $this->input->post('subject_id');
			$topic = $this->input->post('topic');
			$data = array("subject_id"=>$subject_id,"topic"=>$topic);
			$this->db->insert("course_subject_topic_master",$data);
			page_alert_box('success','Topic Added','New topic added successfully.');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		}

	}

	public function add_topic_to_course(){
		if($_POST){
			$course_id = $this->input->post('course_id');
			$topic = $this->input->post('topic_id');
			$data = array("course_fk"=>$course_id,"topic_id"=>$topic);
			$this->db->insert("course_segment_master",$data);
			page_alert_box('success','Topic Added','Topic added to course successfully.');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		}
	}


	public function edit_course_topic(){
		if($_POST){
			$course_id = $this->input->post('course_id');
			$topic = $this->input->post('edit_topic');
			$data = array("topic"=>$topic);
			$this->db->where('id',$this->input->post('topic_id'));
			$this->db->update("course_subject_topic_master",$data);
			page_alert_box('success','Topic Edited','Topic edited successfully.');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		}

	}

	public function delete_topic_from_course($course_id){
		$topic_id = $this->input->get('topic_id');
		$this->db->where('topic_id',$topic_id);
		$this->db->where('course_fk',$course_id);
		$this->db->delete("course_segment_master");
		page_alert_box('warning','Topic removed','Topic from course removed successfully.');
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
	}

	public function ajax_file_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			5 => 'page_count',
			6 => 'file_type'

		);

		$query = "SELECT count(id) as total
								FROM course_topic_file_meta_master where file_type = 1
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.file_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic ,file_type
								FROM course_topic_file_meta_master as  ctfmm
								left join course_subject_master as csm on ctfmm.subject_id = csm.id
								left join course_subject_topic_master as cstm on ctfmm.topic_id = cstm.id
								where  1= 1
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND  title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND  csm.URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value']) && $requestData['columns'][3]['search']['value'] != 'null' ) {  //salary
			$sql.=" AND ctfmm.subject_id in (" . $requestData['columns'][3]['search']['value'] . ") ";
		}
		$search_topic =  $requestData['columns'][4]['search']['value'];
		if ($search_topic !== "" and $search_topic != 'null' ) {  
			$sql.=" AND ctfmm.topic_id in (" . $search_topic . ") ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][6]['search']['value'])) {  //salary
			$sql.=" having file_type LIKE '" . $requestData['columns'][6]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->title;
			$nestedData[] = "link";
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			$nestedData[] = $r->file_type;
			$action = "<a onClick='add_file_to_topic(\" ".$r->title."\",\"".$r->id."\")'  class='btn-sm btn btn-success btn-xs bold' href='javascript:void(0)'>Add</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data ,   // total data array
			"query"=> $this->db->last_query()
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function ajax_test_list(){
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'test_type', 
			2 => 'test_series_name',
		);

		$query = "SELECT count(id) as total FROM course_test_series_master where publish =1 ";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctsm.* , ctta.name as test_type_name , csnm.name as exam_type , csnmm.name as sub_type
					FROM course_test_series_master as  ctsm 
					join course_test_type_attribute as ctta on ctsm.test_type = ctta.id 
					join course_stream_name_master as csnm on csnm.id = ctsm.stream
					join course_stream_name_master as csnmm on csnmm.id = ctsm.sub_stream
					where  publish =1
					";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND test_type in (" . $requestData['columns'][1]['search']['value'] . ") ";
		}				
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND csnm.id in (" . $requestData['columns'][2]['search']['value'] . ") ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND csnmm.id in (" . $requestData['columns'][3]['search']['value'] . ") ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND test_series_name LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}


		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->test_type_name;
			$nestedData[] = $r->exam_type;
			$nestedData[] = $r->sub_type;
			$nestedData[] = $r->test_series_name;
			$action = "<a onClick='add_test_to_topic(\" ".$r->test_series_name."\",\"".$r->id."\")'   class='btn-sm btn btn-success btn-xs bold' href='javascript:void(0)'>Add</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data 
		);

		echo json_encode($json_data);  // send data as json format


	}


	public function ajax_quiz_list(){
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'test_type', 
			2 => 'test_series_name',
		);

		$query = "SELECT count(id) as total FROM course_test_series_master where publish =1 ";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctsm.*  , csm.name as subject_name  , cstm.topic as topic_name
					FROM course_test_series_master as  ctsm 
					join course_subject_master as csm  on csm.id = ctsm.subject
					join course_subject_topic_master as cstm on cstm.id = ctsm.topic_id
					where  ctsm.publish =1 and ctsm.set_type = 1
					";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND test_type in (" . $requestData['columns'][1]['search']['value'] . ") ";
		}				
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND csnm.id in (" . $requestData['columns'][2]['search']['value'] . ") ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND csnmm.id in (" . $requestData['columns'][3]['search']['value'] . ") ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND test_series_name LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}


		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->subject_name;
			$nestedData[] = $r->topic_name;
			$nestedData[] = $r->test_series_name;
			$action = "<a onClick='add_test_to_topic(\" ".$r->test_series_name."\",\"".$r->id."\")'   class='btn-sm btn btn-success btn-xs bold' href='javascript:void(0)'>Add</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data 
		);

		echo json_encode($json_data);  // send data as json format


	}


	public function add_file_to_topic(){
		if($this->input->post()){
			$course_id = $this->input->post('course_id');
			$topic_id = $this->input->post('topic_id');
			$file_id = $this->input->post('file_id');
		    $file_type = $this->input->post('file_type');

			$data = array("segment_fk"=>$topic_id,"element_fk"=>$file_id);

			if($file_type == "test"){
				$data['type'] = "test";
			}
			$this->db->insert('course_segment_element',$data);
			/* update segment information  */
			$this->update_segment_information($course_id);

			page_alert_box('success','File Added.','File to course topic added successfully.');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		}

	}

	public function ajax_add_file_to_topic(){
		if($this->input->post()){
			$course_id = $this->input->post('course_id');
			$topic_id = $this->input->post('topic_id');
			$file_id = $this->input->post('file_id');
		   $file_type = $this->input->post('file_type');

			$data = array("segment_fk"=>$topic_id,"element_fk"=>$file_id);

			if($file_type == "test"){
				$data['type'] = "test";
			}elseif($file_type == "practice"){
				$data['type'] = "practice";
			}
			$data['tag'] = 0;
			$this->db->insert('course_segment_element',$data);
			/* update segment information  */
			$this->update_segment_information($course_id);
			page_alert_box('success','File Added.','File to course topic added successfully.');
		}
		echo json_encode(array('status'=>TRUE));
	}



	public function remove_file_from_topic($id){
		$data = array("id"=>$id);
		$this->db->delete('course_segment_element',$data);
		/*update segment info */
		$this->update_segment_information($this->input->get('course_id'));
		
		page_alert_box('warning','File removed.','File from course topic removed successfully.');
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$this->input->get('course_id'));
	}

	public function change_content_view($id,$state){
		$data = array("tag"=>(($state==1)?0:1));
		$this->db->where('id',$id);
		$this->db->update('course_segment_element',$data);
		page_alert_box('success','Seting saved.','Setting for this element set successfully.');
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$this->input->get('course_id'));
	}


	public function old_add_topic_to_preview($id){
		/* reset all postion */
		$this->db->where('course_fk',$id);
		$this->db->update('course_segment_master',array('is_preview'=>0));
		/* set main postion */
		$this->db->where('topic_id',$this->input->get('topic_id'));
		$this->db->where('course_fk',$id);
		$this->db->update('course_segment_master',array('is_preview'=>1));
		page_alert_box('success','Course preview.','Topic for course preview set successfully.');
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$id);

	}

	public function add_topic_to_preview($id){
		/* reset all postion */
		$this->db->where('course_fk',$id);
		$this->db->update('course_topic_master',array('is_preview'=>0));
		/* set main postion */
		$this->db->where('id',$this->input->get('topic_id'));
		$this->db->where('course_fk',$id);
		$this->db->update('course_topic_master',array('is_preview'=>1));
		page_alert_box('success','Course preview.','Topic for course preview set successfully.');
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$id);

	}

	public function ajax_course_review_list($id=1){
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'user_id',
			2 => 'rating',
			3 => 'text',
			4 => 'ctime',
		);

		$query = "SELECT count(id) as total FROM course_user_rating where course_fk_id = $id";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT cur.id , cur.course_fk_id, cur.user_id , cur.rating ,cur.text, DATE_FORMAT(FROM_UNIXTIME(cur.creation_time/1000), '%d-%m-%Y %h:%i:%s') as ctime , u.name as name , u.profile_picture
                                    FROM course_user_rating AS cur
                                    Left JOIN users AS u ON u.id = cur.user_id
                                    WHERE cur.course_fk_id = '$id' ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND u.name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND rating LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.="  AND text LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$date = explode(',',$requestData['columns'][4]['search']['value']);
			$start = strtotime($date[0])*1000;
			$end = (strtotime($date[1])*1000)+86400000;
			$sql.="  and  cur.creation_time >= '$start' and cur.creation_time <= '$end'"; // BETWEEN $start and $end";
		}
		$query = $this->db->query($sql)->result();
		//echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name;
			$star="";
			if($r->rating > 0 ){
				for($i=0;$i<$r->rating;$i++){
					$star .='<i class="fa fa-star text-danger" aria-hidden="true"> </i> ';
				}
				if($i<5){
					$j = 5 - $i;
					for($i=0;$i<$j;$i++){
					$star .='<i class="fa fa-star-o text-danger" aria-hidden="true"> </i> ';
					}
				}
			}
			$nestedData[] = $star;
			$nestedData[] = substr($r->text, 0, 30);
			$nestedData[] = $r->ctime;
			$action = "<a class='btn-xs bold  btn btn-info' href='" . AUTH_PANEL_URL . "course_product/course/edit_course_review/" . $r->id . "'>View</a>" ;
			$action .= " <a class='btn-sm btn btn-danger btn-xs bold' href='" . AUTH_PANEL_URL . "course_product/course/delete_course_review/" . $r->id ."?course_fk_id=".$r->course_fk_id. "' >delete</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format

	}

	public function edit_course_review($id){
		//$course_review_id = $_GET['course_review_id'];
		$view_data['page']  = 'edit_course_review';
		$data['page_title'] = "Edit Course Review";
		if($this->input->post('update_course_review')) {
			/* handle submission */
			$this->update_course_review();
		}
		$view_data['course_review_detail'] = $this->Course_list_model->get_course_review_details_by_id($id);
		$data['page_data'] = $this->load->view('course_product/course/edit_course_review', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	private function update_course_review(){
		if($this->input->post()) {
			$this->form_validation->set_rules('rating', 'Rating', 'required');
			$this->form_validation->set_rules('text', 'Review', 'required');
			$review_id = $this->input->post('id');
			//$user_id = $this->input->post('course_fk_id');
			if ($this->form_validation->run() == FALSE) {
             $error = validation_errors();
            }else{
				$update = array(
					'rating' => $this->input->post('rating'),
					'text' => $this->input->post('text'),

				);
				$this->db->where('id',$review_id);
				$this->db->update('course_user_rating',$update);
				page_alert_box('success','Action performed','Review updated successfully');
			}
		}

	}

 	public function delete_course_review($id) {
		$course_id = $_GET['course_fk_id'];
		$status = $this->Course_list_model->delete_course_review($id);
		page_alert_box('success','Action performed','Review deleted successfully');
		if($status) {
			redirect('auth_panel/course_product/course/edit_course_page_study?course_id='.$course_id);
		}
	}

	public function delete_course($id) {
		$course_id = $id;
		$status = $this->Course_list_model->delete_course($id);
		page_alert_box('success','Action performed','Course deleted successfully');
		if($status) {
			redirect('auth_panel/course_product/course/all_courses_list');
		}
	}

	/* */

	public function ajax_applied_filter_list($course_id){
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'Session',
			1 => 'FranchiseName',
			2 => 'CourseGroup',
			3 => 'Course',
			4 => 'Batch'
		);

		$query = "SELECT count(id) as total
				FROM course_filtration where course_id = $course_id";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT `id`,`course_id`, group_concat(FranchiseName SEPARATOR ',') as FranchiseName , group_concat(CourseGroup SEPARATOR ',' ) as CourseGroup , group_concat(Session SEPARATOR ',' ) as Session , group_concat(Course SEPARATOR ',' ) as Course , group_concat(Batch SEPARATOR ',' ) as Batch ,f_name ,price FROM `course_filtration` where course_id = $course_id  group by f_name ";

		// getting records as per search parameters

		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND Session LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND FranchiseName LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND CourseGroup LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND Course LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND Batch LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->f_name;
			$nestedData[] = implode(', ', array_unique(explode(',',$r->Session)));
			$nestedData[] = implode(', ', array_unique(explode(',',$r->FranchiseName)));
			$nestedData[] = implode(', ', array_unique(explode(',',$r->CourseGroup)));
			$nestedData[] = implode(', ', array_unique(explode(',',$r->Course)));
			$nestedData[] = implode(', ', array_unique(explode(',',$r->Batch)));
			$nestedData[] = $r->price;
			$nestedData[] = "<a onClick='return confirm(\"Do you really want to delete?\")' class='btn-xs bold btn btn-info' href='".AUTH_PANEL_URL."course_product/course/delete_filter/".$r->course_id."?filter_name=".urlencode($r->f_name)."'>Delete</a><a class=' hide btn-xs bold btn btn-success' href='".AUTH_PANEL_URL."course_product/course/edit_course_page?course_id=".$r->course_id."&filter_name=".urlencode($r->f_name)."&edit_filter=edit'>Edit</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
			,"query"=>$this->db->last_query()
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function delete_filter($id) {
		$status = $this->Course_list_model->delete_filter($id,urldecode($_GET['filter_name']));
		page_alert_box('success','Action performed','Filter deleted successfully');
		if($status) {
			redirect(AUTH_PANEL_URL . "course_product/course/edit_course_page_study?course_id=".$id);
		}
	}

	public function years_list_for_filtration() {
		$data = $this->db->query('select distinct(Session) from users_dams_info order by Session ')->result_array();
		echo json_encode($data);
		die;
	}

	public function franchise_with_year($year){
		$data = $this->db->query("select distinct(FranchiseName) from users_dams_info where Session = $year")->result_array();
		echo json_encode($data);
		die;
	}

	public function courses_group_with_year_and_franchise($franchise,$year){
		$franchise = urldecode ($franchise);
		$data = $this->db->query("select distinct(CourseGroup) from users_dams_info where Session = $year and FranchiseName = '$franchise'")->result_array();

		echo json_encode($data);
		die;
	}

	public function courses_with_year_and_franchise_and_course_group($franchise,$year,$course_group){
		$franchise = urldecode ($franchise);
		$course_group = urldecode ($course_group);
		$data = $this->db->query("select distinct(Course) from users_dams_info where Session = $year and FranchiseName = '$franchise' and  CourseGroup = '$course_group'")->result_array();

		echo json_encode($data);
		die;
	}


	public function batches_with_year_and_franchise_and_course_group_and_course($franchise,$year,$course_group){
		$franchise = urldecode ($franchise);
		$course_group = urldecode ($course_group);
		$course = base64_decode($_GET['course']);
		$data = $this->db->query("select distinct(Batch) from users_dams_info where Session = $year and FranchiseName = '$franchise' and  CourseGroup = '$course_group' and Course = '$course'")->result_array();
		//echo "select distinct(Batch) from users_dams_info where Session = $year and FranchiseName = '$franchise' and  CourseGroup = '$course_group' and Course = '$course'";
		echo json_encode($data);
		die;
	}

	public function save_batches_record(){
		if($_POST){
			$array = $_POST['data'];

			foreach($array as $a){
				$query = "INSERT IGNORE INTO course_filtration (course_id , FranchiseName, CourseGroup, Session, Course, Batch)
							VALUES ('".$a['course_id']."',
									'".$a['FranchiseName']."',
									'".$a['CourseGroup']."',
									'".$a['Session']."',
									'".$a['Course']."',
									'".$a['Batch']."' )";
				$this->db->query($query);
			}
			echo json_encode(array("status"=>true));die;
		}
	}


	public function add_faq(){
		if($this->input->post()){	//print_r($_POST); die;

			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('question', 'Question', 'required');


			if ($this->form_validation->run() == FALSE) {
             	$error = validation_errors();
				$data  = $error;
				echo json_encode(array("data_value"=>$data,"status"=>false,"code"=>0));

            } else{
				$course_id = $this->input->post('course_id');
				$question = $this->input->post('question');
				$description = $this->input->post('description');
				if($this->input->post('faq_id') != ''){
					$faq_id = $this->input->post('faq_id');

					$data = array("question"=>$question,"description"=>$description,"course_id"=>$course_id);
					$this->db->where('id',$faq_id);
					$this->db->update('course_faq_master',$data);
					$status_data  = "FAQ updated successfully !!";
					echo json_encode(array("data_value"=>$status_data,"status"=>true,"code"=>2));
				}else{

				$data = array("course_id"=>$course_id,"question"=>$question,"description"=>$description);
				$this->db->insert('course_faq_master',$data);
				$status_data  = "FAQ added successfully !!";
				echo json_encode(array("data_value"=>$status_data,"status"=>true,"code"=>1));
				}
			}
		}

	}

	public function delete_faq($faq_id,$course_id){
		$this->db->where('id',$faq_id);
		$result = $this->db->delete('course_faq_master');
		if($result){
			page_alert_box('success','Action performed','FAQ deleted successfully');
			if($result) {
				redirect(AUTH_PANEL_URL . "course_product/course/edit_course_page_study?course_id=".$course_id);
			}
	}

	}



	public function all_courses_reviews_list(){
		$data['page_data'] = $this->load->view('course_product/course/all_courses_reviews', $view_data=array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}



	public function ajax_all_course_review_list(){
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
		    1 => 'title',
			2 => 'name',
			3 => 'text',
			4 => 'rating',
			5 => 'ctime',
		);
		$where = "";
		if($_GET['period'] != ""){
			$period =$_GET['period'];

			$today = date('d-m-Y');
			$currentWeekDate = date('d-m-Y', strtotime('-7 days', strtotime($today)));
			$currentMonth = date('m');
			$currentYear = date('Y');
			$current_millisecond = strtotime(date('01-m-Y 00:00:00'))*1000;

			if($period == "today"){
				$where .= " AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(cur.creation_time,1,10)), '%d-%m-%Y')='$today' ";
			}elseif($period == "yesterday"){
				$yesterday = date('d-m-Y', strtotime($today. ' - 1 days'));
				$where .= " AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(cur.creation_time,1,10)), '%d-%m-%Y')='$yesterday' ";
			}elseif($period == "7days"){
				//$yesterday = date('d-m-Y', strtotime($today. ' - 7 days'));
				$yesterday = strtotime("-1 week")."000";
				$where .= " AND cur.creation_time >= '$yesterday'  ";
			}elseif($period == "current_month"){
				$current_month = date('m-Y');
				$where .= " AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(cur.creation_time,1,10)), '%m-%Y') = '$current_month'  ";
			}elseif($period == "all"){
				$where .= "";
			}

		}

		$query = "SELECT count(id) as total FROM course_user_rating as cur ";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT cur.id ,cm.title, cur.course_fk_id, cur.user_id , cur.rating ,cur.text, DATE_FORMAT(FROM_UNIXTIME(cur.creation_time/1000), '%d-%m-%Y %h:%i:%s') as ctime , u.name as name , u.profile_picture
                                    FROM course_user_rating AS cur
                                    Left JOIN users AS u ON u.id = cur.user_id
									JOIN course_master cm ON cm.id=cur.course_fk_id
									where 1=1 $where
                                    ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND cm.title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND u.name LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND rating LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.="  AND text LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$date = explode(',',$requestData['columns'][5]['search']['value']);
			$start = strtotime($date[0])*1000;
			$end = (strtotime($date[1])*1000)+86400000;
			$sql.="  and  cur.creation_time >= '$start' and cur.creation_time <= '$end'"; // BETWEEN $start and $end";
		}
		$query = $this->db->query($sql)->result();
		//echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			//$nestedData[] = $r->id;
			$nestedData[] = ++$requestData['start'];
			$nestedData[] = $r->title;
			$nestedData[] = $r->name;
			$nestedData[] = substr($r->text, 0, 30);
			$star="";
			if($r->rating > 0 ){
				for($i=0;$i<$r->rating;$i++){
					$star .='<i class="fa fa-star text-danger" aria-hidden="true"> </i> ';
				}
				if($i<5){
					$j = 5 - $i;
					for($i=0;$i<$j;$i++){
					$star .='<i class="fa fa-star-o text-danger" aria-hidden="true"> </i> ';
					}
				}
			}
			$nestedData[] = $star;
			$nestedData[] = $r->ctime;
			$action = "<a class='btn-xs bold  btn btn-info' href='" . AUTH_PANEL_URL . "course_product/study/edit_course_review/" . $r->id . "'>View</a>" ;
			$action .= " <a class='btn-sm btn btn-danger btn-xs bold' href='" . AUTH_PANEL_URL . "course_product/study/delete_course_review/" . $r->id ."?course_fk_id=".$r->course_fk_id. "' >delete</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format

	}


	public function all_instructor_reviews_list(){
		$data['page_data'] = $this->load->view('course_product/study/all_ins_reviews', $view_data=array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}



	public function ajax_all_ins_review_list(){
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
		    1 => 'title',
			2 => 'name',
			3 => 'text',
			4 => 'rating',
			5 => 'ctime',
		);

		$query = "SELECT count(id) as total FROM course_instructor_rating";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT cir.id ,bu.username, cir.instructor_id, cir.user_id , cir.rating ,cir.text, DATE_FORMAT(FROM_UNIXTIME(cir.creation_time/1000), '%d-%m-%Y %h:%i:%s') as ctime , u.name as name , u.profile_picture
                                    FROM course_instructor_rating AS cir
                                    Left JOIN users AS u ON u.id = cir.user_id
									JOIN backend_user bu ON bu.id=cir.instructor_id
                                    ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND bu.username LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND u.name LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND rating LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.="  AND text LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$date = explode(',',$requestData['columns'][5]['search']['value']);
			$start = strtotime($date[0])*1000;
			$end = (strtotime($date[1])*1000)+86400000;
			$sql.="  and  cir.creation_time >= '$start' and cir.creation_time <= '$end'"; // BETWEEN $start and $end";
		}
		$query = $this->db->query($sql)->result();
		//echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->username;
			$nestedData[] = $r->name;
			$nestedData[] = substr($r->text, 0, 30);
			$star="";
			if($r->rating > 0 ){
				for($i=0;$i<$r->rating;$i++){
					$star .='<i class="fa fa-star text-danger" aria-hidden="true"> </i> ';
				}
				if($i<5){
					$j = 5 - $i;
					for($i=0;$i<$j;$i++){
					$star .='<i class="fa fa-star-o text-danger" aria-hidden="true"> </i> ';
					}
				}
			}
			$nestedData[] = $star;
			$nestedData[] = $r->ctime;
			$action = "";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format

	}

	//all_test_given_report

	public function all_test_given_report(){
		$data['page_data'] = $this->load->view('course_product/study/all_test_given_report', $view_data=array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}



	public function ajax_all_test_given_report(){
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
		    1 => 'test_series_name',
			2 => 'name',
			3 => 'time_spent',
			4 => 'total_test_series_time',
			5 => 'reward_points',
			6 => 'creation_time',
		);

		$query = "SELECT count(id) as total FROM course_test_series_report";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

        $sql = "SELECT ctsr.id  , ctsm.test_series_name , u.name , ctsr.time_spent ,
				ctsr.total_test_series_time , ctsr.reward_points,
				DATE_FORMAT(FROM_UNIXTIME(ctsr.creation_time/1000), '%d-%m-%Y %h:%i:%s') as ctime
				from course_test_series_report as ctsr
				left join users as u on u.id = ctsr.user_id
				left join course_test_series_master as ctsm on ctsm.id = ctsr.test_series_id
				";
		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND ctsm.id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND ctsm.test_series_name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND u.name LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND ctsr.time_spent LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.="  AND ctsr.total_test_series_time LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.="  AND ctsr.reward_points LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][6]['search']['value'])) {  //salary
			$sql.="  AND DATE_FORMAT(FROM_UNIXTIME(ctsr.creation_time/1000), '%d-%m-%Y %h:%i:%s') LIKE '" . $requestData['columns'][6]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();
		//echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->test_series_name;
			$nestedData[] = $r->name;
			$nestedData[] = $r->time_spent;
			$nestedData[] = $r->total_test_series_time;
			$nestedData[] = $r->reward_points;
			$nestedData[] = $r->ctime;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format

	}




  /* Add topic to course latest */

	public function add_topic_name_to_course(){
		if($_POST){
			$course_id = $this->input->post('course_id');
			$topic = $this->input->post('topic_name');
			$topic_type = $this->input->post('topic_type');
			if($_FILES && $_FILES['imagetopic']['name'] != ""){
				$file  = $this->amazon_s3_upload($_FILES['imagetopic'],'course_topic_image');
			}else{
				$file  ="";
			}

			$data = array("course_fk"=>$course_id,"topic_name"=>$topic,"image"=>$file,'topic_type'=>$topic_type);
			$this->db->insert("course_topic_master",$data);
			page_alert_box('success','Topic Added','New topic added successfully.');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		}
	}

	public function add_practice_to_course(){
		if($_POST){
			print_r($_POST);
			
			$practice_id = $this->input->post('practice_id');
		//	$topic = $this->input->post('topic_name');
		$course_id = $this->input->post('course_id');
		
			$data = array("practice_id"=>$practice_id);
			$this->db->where('id',$this->input->post('active_topic_id'));
			$this->db->update("course_topic_master",$data);
			page_alert_box('success','Practice Id','New Practice Id added successfully.');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		}
	}

	public function add_course_type(){
		if($_POST){
			//print_r($_POST);
			
			$course_id = $this->input->post('course_id');
		//	$topic = $this->input->post('topic_name');
		$course_type = $this->input->post('course_type');
		
			$data = array("name"=>$course_type);
			$this->db->insert("course_type_master",$data);
			page_alert_box('success','Course Type','New Course type added successfully.');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		}
	}
	public function add_content_type(){
		if($_POST){
			//print_r($_POST);
			
			$course_id = $this->input->post('course_id');
		//	$topic = $this->input->post('topic_name');
		$content_type = $this->input->post('content_type');
		
			$data = array("name"=>$content_type);
			$this->db->insert("course_content_type",$data);
			page_alert_box('success','Course Type','New Content type added successfully.');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		}
	}
	public function delete_topic_id_from_course($course_id){
		$topic_id = $this->input->get('topic_id');
		$this->db->where('id',$topic_id);
		$this->db->where('course_fk',$course_id);
		$this->db->delete("course_topic_master");

		$this->db->where('segment_fk',$topic_id);
		$this->db->delete("course_segment_element");
		/* update segment information  */
		$this->update_segment_information($course_id);
		page_alert_box('warning','Topic removed','Topic from course removed successfully.');
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
	}

	public function edit_course_topic_name(){
		if($_POST){
			print_r($_FILES['imagetpc_udt']);
			$course_id = $this->input->post('course_id');
			$topic = $this->input->post('edit_topic');
			if($_FILES['imagetpc_udt'][name]!=''){
			$file  = $this->amazon_s3_upload($_FILES['imagetpc_udt'],'course_topic_image');
			
			$data = array("topic_name"=>$topic,"image"=>$file);
			}else{
				$data = array("topic_name"=>$topic);	
			}
			$this->db->where('id',$this->input->post('topic_id'));
			$this->db->update("course_topic_master",$data);

			page_alert_box('success','Topic Edited','Topic edited successfully.');
			redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		}

	}

	public function update_topic_name_order($course_id){
		$count = 1 ;
		foreach($_POST as $value){
			$this->db->where('id',$value);
			$this->db->where('course_fk',$course_id);
			$this->db->update("course_topic_master",array('position'=>$count));
			$count++;
		}
		echo json_encode(array('status'=>true));
	}

	private function add_category_check(){
		$this->db->where('course_id', $_POST['course_id']);
		$this->db->delete('course_category_relationship');
			if($this->input->post('course_cat_id')){
				foreach($_POST['course_cat_id'] as $a){
					$data = array(
					'course_id' => $_POST['course_id'],
					'cate_id' => $a,
					);
					$this->db->insert('course_category_relationship', $data);
				}//edn of ofr each
			}
	}

	private function add_save_check(){
		$this->db->where('course_id', $_POST['course_id']);
		$this->db->delete('course_type_relationship');
			foreach($_POST['save_type_check'] as $a){
				$data=array(
				'course_id'=>$_POST['course_id'],
				'type_id'=>$a,
				);
				$this->db->insert('course_type_relationship',$data);
			}

	}
	private function add_content_check(){
		$this->db->where('course_id', $_POST['course_id']);
		$this->db->delete('course_content_type_relationship');
			foreach($_POST['save_type_content'] as $a){
				$data=array(
				'course_id'=>$_POST['course_id'],
				'content_type_id'=>$a,
				);
				$this->db->insert('course_content_type_relationship',$data);
			}

	}
	

	private function update_segment_information($c_id){
		$query =  $this->db->query("SELECT ctm.* , ctm.topic_name as title
		FROM course_topic_master as ctm
		where ctm.course_fk = '$c_id' order by position asc ");
		$topic = $query->result_array();

		$f_pdf = $f_ppt =  $f_video = $f_epub =  $f_doc = $f_flash_card  = $f_concepts = $f_test = 0; 
		$pdf_text = $ppt_text  = $video_text = $epub_text = $doc_text = $flashcard_text = $concept_text = $test_text = "";

		foreach($topic as $tp){ 
			$this->db->where('segment_fk',  $tp['id']);
			$files =  $this->db->get('course_segment_element')->result_array();

			foreach($files as $f){
				$this->db->where('id',  $f['element_fk']);
				$file_id =  $this->db->get('course_topic_file_meta_master')->row_array();
				$file_type_id = $file_id['file_type'];

				if($file_type_id == 1 && $f['type'] !="test"){ $f_pdf++; $pdf_text = "$f_pdf pdf";  }
				if($file_type_id == 2 && $f['type'] !="test"){ $f_ppt++; $ppt_text = "$f_ppt ppt"; }
				if($file_type_id == 3 && $f['type'] !="test"){ $f_video++; $video_text = "$f_video video";  }
				if($file_type_id == 4 && $f['type'] !="test"){ $f_epub++; $epub_text = "$f_epub epub";  }
				if($file_type_id == 5 && $f['type'] !="test"){ $f_doc++; $doc_text = "$f_doc doc file";}
				if($file_type_id == 6 && $f['type'] !="test"){ $f_flash_card++; $flashcard_text = "$f_flash_card flash card";}
				if($file_type_id == 7 && $f['type'] !="test"){ $f_concepts++; $concept_text = "$f_concepts concept";}
				if($f['type'] =="test"){ $f_test++; $test_text = "$f_test test-series";}
			}
		} 

		$txt = array();
		if($concept_text != ""){ $txt[] = $concept_text; }
		if($test_text != ""){ $txt[] = $test_text; }
		if($video_text != ""){ $txt[] = $video_text; }
		if($pdf_text != ""){ $txt[] = $pdf_text; }
		if($ppt_text != ""){ $txt[] = $ppt_text; }
		if($epub_text != ""){ $txt[] = $epub_text; }
		if($doc_text != ""){ $txt[] = $doc_text; }
		if($flashcard_text != ""){ $txt[] = $flashcard_text; }
		$txt =  implode(',',$txt);
		$this->db->where('id',$c_id)->update('course_master',array('segment_information'=>$txt));
	}



	public function change_order($id) {

		/* complicated handle with CARE!!!!!!!!!!*/
		$course_id=$this->input->get('course_id');
		/* current id */
		$swap_order = $this->input->get('swap_action');
		/* topic id or holder */
		$tid = $this->input->get('tid');
		$main_id_position = $this->db->select('position')->where('id',$id)->get('course_segment_element')->row()->position;
		if($this->input->get('swap_action')=='up'){
	
			$this->db->where('position <', $main_id_position);
			$this->db->where('segment_fk',  $tid);
			$this->db->order_by('position','desc');
			$coursegmnt =  $this->db->get('course_segment_element')->row_array();
		}elseif($this->input->get('swap_action')=='down'){
			$this->db->where('position > ', $main_id_position);
			$this->db->where('segment_fk  ',  $tid);	
			$this->db->order_by('position','ASC');
            $coursegmnt =  $this->db->get('course_segment_element')->row_array();	
		}
		/* update current */
		$array_data=array('position'=>$coursegmnt['position']);
		$this->db->where('id',$id);
		$this->db->update('course_segment_element',$array_data); 
		/* update relative id */
		$array_data_swap=array('position'=>$main_id_position);
		$this->db->where('id',$coursegmnt['id']);
		$this->db->update('course_segment_element',$array_data_swap);
		/* :) */
		redirect(AUTH_PANEL_URL."course_product/course/edit_course_page_study?course_id=".$course_id);
		
	}




}
