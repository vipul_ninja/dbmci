<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class Course_transactions extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
		$this->load->model('Course_transactions_model');
		$this->load->library('grocery_CRUD');
	}

	public function index(){

		$view_data['page']  = 'course_transactions';
		$data['page_title'] = "Course Transactions";
		//$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('course_product/course/course_transactions', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function get_ajax_course_transactions_list(){
		$output_csv = $output_pdf =  false; 
		/******* Check For Instructor User *******/
		$user_data = $this->session->userdata('active_user_data');
		$where = "";
		if($user_data->instructor_id == 1){
			$instructor_id = $user_data->id;
			$where = " AND ctr.instructor_id =$instructor_id ";
		}
		/*------------------------------------------*/
		if($_GET['period'] != ""){
			$period =$_GET['period'];

			$today = date('d-m-Y');
			$currentWeekDate = date('d-m-Y', strtotime('-7 days', strtotime($today)));
			$currentMonth = date('m');
			$currentYear = date('Y');
			$current_millisecond = strtotime(date('01-m-Y 00:00:00'))*1000;
			if($period == "today"){
				$where .= " AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(ctr.creation_time,1,10)), '%d-%m-%Y')='$today' ";
			}elseif($period == "yesterday"){
				$yesterday = date('d-m-Y', strtotime($today. ' - 1 days'));
				$where .= " AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(ctr.creation_time,1,10)), '%d-%m-%Y')='$yesterday' ";
			}elseif($period == "7days"){
				//$yesterday = date('d-m-Y', strtotime($today. ' - 7 days'));
				$yesterday = strtotime("-1 week")."000";
				$where .= " AND ctr.creation_time >= '$yesterday'  ";
			}elseif($period == "current_month"){
				$current_month = date('m-Y');
				$where .= " AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(ctr.creation_time,1,10)), '%m-%Y') = '$current_month'  ";
			}elseif($period == "all"){
				$where .= "";
			}

		}

		/*------------------------------------------*/
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		/*******  Check For Instructor User Ends *******/
		
		if(isset($_POST['input_json'])){
			if(ISSET($_POST['download_pdf'])){
				$output_pdf =  true;
			}else{
				$output_csv = true;
			}
			//$_POST = json_decode($_POST['input_json'],true);
			$requestData = json_decode($_POST['input_json'],true);

		}


		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'user_name',
			2 => 'course_name',
			3 => 'pay_via',
			4 => 'coupon_applied',
			5 => 'instructor_name',
			6 => 'transaction_status',
			7 => 'creation_time',
			8 => 'instructor_share',
			9 => 'course_price',
			10 =>'points_used'

		);

		$query = "SELECT count(id) as total
								FROM course_transaction_record as ctr where 1 = 1 $where
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctr.* , u.name as user_name , bu.username as instructor_name ,
						cm.title as course_name,
						DATE_FORMAT(FROM_UNIXTIME(SUBSTR(ctr.creation_time,1,10)), '%d-%m-%Y') as creation_time, 
						ctr.pay_via
				FROM course_transaction_record as ctr
				left join users as u on u.id = ctr.user_id
				left join backend_user as bu on bu.id = ctr.instructor_id
				left join course_master as cm on cm.id = ctr.course_id
				WHERE  1 = 1 $where";
		$for_client_filters = array();	

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND u.name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND cm.title LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
			$for_client_filters['course_name'] = $requestData['columns'][2]['search']['value'];
		}

		if (!empty($requestData['columns'][3]['search']['value']) ) {  //salary
			$sql.=" AND pay_via LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value']) ) {  //salary
			$sql.=" AND coupon_applied LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value']) ) {  //salary
			$sql.=" AND bu.username LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
			$for_client_filters['instructor_name'] = $requestData['columns'][5]['search']['value'];
		}
		if ($requestData['columns'][6]['search']['value'] >= 0)  {  //salary
			$sql.=" AND ctr.transaction_status = '" . $requestData['columns'][6]['search']['value'] . "' ";
		}
		if (!empty($requestData['columns'][7]['search']['value'])) {  //salary
			$sql.=" AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(ctr.creation_time,1,10)), '%d-%m-%Y') like '" . $requestData['columns'][7]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][8]['search']['value']) ) {  //salary
			if($requestData['columns'][8]['search']['value'] == 2 ){
				$sql.=" AND ctr.course_price = 0 ";

			}
			if($requestData['columns'][8]['search']['value'] == 1){
				$sql.=" AND ctr.course_price > 0 ";
			}	
		}

		if (!empty($requestData['columns'][12]['search']['value']))  {  //salary
			$sql.=" AND ctr.course_price = '" . $requestData['columns'][12]['search']['value'] . "' ";
		}
		// if (!empty($requestData['columns'][8]['search']['value']) ) {  //salary
		// 	$sql.=" AND instructor_share LIKE '" . $requestData['columns'][8]['search']['value'] . "%' ";
		// }
		// if (!empty($requestData['columns'][9]['search']['value'])) {  //salary
		// 	$date = explode(',',$requestData['columns'][9]['search']['value']);
		// 	$start = strtotime($date[0])*1000;
		// 	$end = (strtotime($date[1])*1000)+86400000;
		// 	$sql.="  and  ctr.creation_time >= '$start' and ctr.creation_time <= '$end'"; // BETWEEN $start and $end";
		// }
		

		$query = $this->db->query($sql)->result();
		//echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		if($output_csv == false && $output_pdf == false){
			$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
		}else{
			$sql.=" ORDER BY ctr.creation_time desc ";
		}
		$result = $this->db->query($sql)->result();

		$data = array();

//csv ravi
		if($output_csv == true || $output_pdf == true){

			// for csv loop
			$head = array(
				's.no',
				'name',
				'course name',
				'course price',
				'pay via',
				'coupon applied',
				'status',
				'Instructor name',				
				'on',
				'type',
				'Price',
				'Gst',
				'Net Amt.',
				'Ins Share',
				'Profit'
				);
			$start = 0;
			foreach($result as $r ){
				if($r->transaction_status == 0){
					$transaction_status = "Pending";
				}elseif($r->transaction_status == 1){
					$transaction_status = "Complete";
				}elseif($r->transaction_status == 2){
					$transaction_status = "Cancel";
				}elseif($r->transaction_status == 3){
					$transaction_status = "Refund Req.";
				}
				elseif($r->transaction_status == 4){
					$transaction_status = "Refunded";
				}				
				$nestedData = array();
				$nestedData[] = ++$start;//$r->id;
				$nestedData[] = $r->user_name;
				$nestedData[] = $r->course_name;
				$nestedData[] = $r->course_price;
				$nestedData[] = $r->pay_via;
				$nestedData[] = $r->coupon_applied;
				$nestedData[] = $transaction_status;
				$nestedData[] = $r->instructor_name;
				//$nestedData[] = $r->instructor_share;
				$nestedData[] = $r->creation_time;
				$nestedData[] = ($r->course_price > 0 )?'Paid':'Free';
				//$fa_inr = '';		
				$tax = $r->course_price - (($r->course_price*$r->tax)/100);
				$nestedData[] = bcadd( $tax, 0, 2) ; 
				$nestedData[] = bcadd( $r->course_price -$tax , 0, 2)  ; 
				$nestedData[] = bcadd($r->course_price, 0, 2); 
				$nestedData[] = $r->instructor_share;
				$nestedData[] = bcadd(($r->course_price - $r->instructor_share) , 0, 2); 
				$data[] = $nestedData;
			}

			if($output_csv == true){
				$this->all_transaction_to_csv_download($data, $filename = "export.csv", $delimiter=";",$head);
				die;				
			}
			if($output_pdf == true){
				$this->all_transaction_to_pdf_download($data);

				die;
			}
			
		}
//csv end ravi		
			function pull_text_right($str){
				return '<span class="pull-right">'.$str.'</span>';
			} 
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			if($r->transaction_status == 0){
				$transaction_status = "<span class='bold btn btn-warning btn-xs'>Pending</span>";
			}elseif($r->transaction_status == 1){
				$transaction_status = "<span class=' bold btn btn-success btn-xs'>Complete</span>";
			}elseif($r->transaction_status == 2){
				$transaction_status = "<span class=' bold btn btn-danger btn-xs'>Cancel</span>";
			}elseif($r->transaction_status == 3){
				$transaction_status = "<span class=' bold btn btn-info btn-xs'>Refund Req.</span>";
			}
			elseif($r->transaction_status == 4){
				$transaction_status = "<span class=' bold btn btn-success btn-xs'>Refunded</span>";
			}

			$nestedData[] = ++$requestData['start'];//$r->id;
			$nestedData[] = $r->user_name;
			$fa_inr = '<i class="fa fa-inr"></i> ';			
			$nestedData[] = substr($r->course_name, 0, 25).'...'; 			
			$nestedData[] = $r->pay_via;
			$nestedData[] = $r->coupon_applied;
			$nestedData[] = $r->instructor_name;
			$nestedData[] = $transaction_status;

			$nestedData[] = $r->creation_time;
			$nestedData[] = ($r->course_price > 0 )?'Paid':'Free';
			$nestedData[] = $r->points_used;
			$tax = $r->course_price - (($r->course_price*$r->tax)/100);
			$nestedData[] = pull_text_right( $fa_inr.bcadd( $tax, 0, 2) ); 
			$nestedData[] =  pull_text_right($fa_inr.bcadd( $r->course_price -$tax , 0, 2))  ; 
			$nestedData[] = pull_text_right( $fa_inr.bcadd($r->course_price, 0, 2)); 
			$nestedData[] = pull_text_right($fa_inr.$r->instructor_share);
			$nestedData[] = pull_text_right( $fa_inr.bcadd(($r->course_price - $r->instructor_share) , 0, 2)); 

			$action = "<a class='btn-xs bold  btn btn-info' href='" . AUTH_PANEL_URL . "course_product/course_transactions/course_transaction_details?transaction_id=" . $r->id . "'>View</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;


		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data,   // total data array
			"client_filters" => $for_client_filters ,
			"posted_data" => $_POST,
			//"query"=> $this->db->last_query()
		);

		echo json_encode($json_data);  // send data as json format
	}
//end of ajax ravi

	/*
	* this function is to generated csv w.r.t. to user inout in ajax datatable 
	* why i use it ? 
	* it is realted with authetication machenism 
	* Warning !! please check ajax data table view and other related function before changing it 
	* happy coding :)
	*/
	public function get_ajax_course_transactions_download(){
		if(!$_POST){
			show_404();
		}
		$this->get_ajax_course_transactions_list();
	}

	public function all_transaction_to_csv_download($array, $filename = "export.csv", $delimiter=";",$header) {
	    header('Content-Type: application/csv');
	    header('Content-Disposition: attachment; filename="'.$filename.'";');
	    $f = fopen('php://output', 'w');

	    fputcsv($f, $header);

	    foreach ($array as $line) {
	        fputcsv($f, $line, $delimiter);
	    }
	} 

	public function all_transaction_to_pdf_download($data){
		$this->load->library('fpdf_course_transaction');
		$this->fpdf_course_transaction->pdf_out($data);
	}


	public function course_transaction_details(){
		$transaction_id = $_GET['transaction_id'];
		$view_data['page']  = 'course_transactions_details';
		$data['page_title'] = "Course Transactions Details";
		$view_data['course_transactions_details'] = $this->Course_transactions_model->get_course_transactions_details($transaction_id);
		//print_r($view_data['course_transactions_details']); die;
		$data['page_data'] = $this->load->view('course_product/course/course_transactions_details', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function update_course_transaction_status(){
		if($this->input->post()) {
			$this->form_validation->set_rules('transaction_status', 'Transaction Status', 'required');
			$this->form_validation->set_rules('post_transaction_id', 'Post Transaction Id', 'required');
			$this->form_validation->set_rules('pay_via', 'Pay Via', 'required');

			$id     = $this->input->post('id');
			if ($this->form_validation->run() == FALSE) {
             	$error = validation_errors();
				echo $error; die;
            }
			else {
			$update_data =
				array(
					  'transaction_status'  => $this->input->post('transaction_status'),
					  'post_transaction_id' => $this->input->post('post_transaction_id'),
					  'pay_via'             => $this->input->post('pay_via')

						  );
				//print_r($update_data); die;
				          $this->db->where('id',$id);
			$add_series = $this->db->update('course_transaction_record',$update_data);
			page_alert_box('success','Action performed','Status updated successfully');
            }
            /* put a log */
			backend_log_genration($this->session->userdata('active_backend_user_id'),
			'Update Transaction status for transaction id  -: '.$this->input->post('pre_transaction_id'),
			'COURSE TRANSACTION');
            redirect(AUTH_PANEL_URL."course_product/course_transactions/course_transaction_details?transaction_id=".$id);
		}

	}

	/* api to use refund amount */

	public function refund_amount($id){
		$ORDERID = $_GET['ORDERID'];
		$TXNID = $_GET['TXNID'];
		$REFUNDAMOUNT = $_GET['REFUNDAMOUNT'];
		$refund_url = base_url()."Paytm/Refund.php?MID=DAMSDe47047643996320&ORDERID=".$ORDERID."&TXNID=".$TXNID."&REFUNDAMOUNT=".$REFUNDAMOUNT."&SERVER=LIVE";

		$json  = file_get_contents($refund_url);
    	$array =  json_decode($json);

    	if(count($array) > 0 ){
    		$array = array(
    					"transaction_status"=> 3,
    					"refund_id"=> $array->REFID,
    			);
    	 $this->db->where('pre_transaction_id',$ORDERID);
    	 $this->db->update('course_transaction_record',$array);
    	}

    	/* put a log */
			backend_log_genration($this->session->userdata('active_backend_user_id'),
			'Generate a refund request for transaction id  -: '.$ORDERID,
			'COURSE TRANSACTION');

    	redirect(AUTH_PANEL_URL."course_product/course_transactions/course_transaction_details?transaction_id=".$id);
	}

	public function genrate_receipt($transaction_id){

		$course_transactions_details = $this->Course_transactions_model->get_course_transactions_details($transaction_id);

		$data['receipt_number'] = $course_transactions_details['id'];
		$data['user_name'] =  $course_transactions_details['user_name'];
		$data['user_email'] =  $course_transactions_details['user_email'];
		$data['user_mobile'] =  $course_transactions_details['user_mobile'];

		$data['course_name'] = $course_transactions_details['course_name'];
		$data['pay_via'] = $course_transactions_details['pay_via'];
		$data['amount'] =  $course_transactions_details['course_price'];
		$data['transaction_id']  = $course_transactions_details['post_transaction_id'];

		$data['date'] =  $course_transactions_details['creation_time'];

		$this->load->library('fpdf_genrate_receipt');
		$this->fpdf_genrate_receipt->pdf_out($data);
	}


}
