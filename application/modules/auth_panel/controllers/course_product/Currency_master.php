<?php 

class Currency_master extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view(AUTH_TEMPLATE.'grocery_crud_template',(array)$output);
	}

	public function Currency_master_view(){
		
			$crud = new grocery_CRUD();
			$crud->unset_export();
			$crud->unset_print();
			$crud->set_table('currency_value_master');
			$output = $crud->render();
			$this->_example_output($output);
	}
}