<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Add_tag extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->model('add_tag_model');
		$this->load->library('form_validation');
		
	}

	public function index() {
		if($this->input->get('tag_cache_cleared') != "" && 2 == base64_decode($this->input->get('tag_cache_cleared'))) {
			$data['page_toast'] = 'Cache cleared successfully';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
		}

		if(isset($_POST)) { 
			$this->form_validation->set_rules('category_id', 'Category Id', "required");
			$this->form_validation->set_rules('tag', 'Tag', "required|is_unique[post_tags.text]");
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$insert = array(
            		'master_id' => $this->input->post('category_id'),
            		'text'  => $this->input->post('tag'),
            		'status' => $this->input->post('status'),
            		'position'=>($this->db->query('select max(id) as max from post_tags')->row()->max)+1
            		);
            	$this->db->insert('post_tags',$insert);
 
				$data['page_toast'] = 'Information Inserted  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }

		}
 		$view_data['page'] = 'tag_add';
		$view_data['main_category'] = $this->add_tag_model->get_main_category();
		$data['page_data'] = $this->load->view('tag/add_tag', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_all_tag_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			
			0 => 'master_id',
			1 => 'text',
			4 => 'position',
		);

		$query = "SELECT count(id) as total
								FROM post_tags 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT post.id , post.text , post.status , post.position,
				 master1.name as master_id FROM post_tags  post
				 LEFT JOIN course_stream_name_master  master1
				 ON post.master_id = master1.id ";

		// getting records as per search parameters
		
		if (!empty($requestData['columns'][0]['search']['value'])) {  //salary
			$sql.=" where master1.name LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		} 
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND post.text LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		$query = $this->db->query($sql)->result();
        //echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
		$sql .= "order by post.position asc";
		$result = $this->db->query($sql)->result();
		//echo "<pre>";print_r($result); echo count($result)-1;die;

		$data = array();
		//print_r($result);
		foreach ($result as $key => $r) {  // preparing an array
			$nestedData = array();

			
			$nestedData[] = $r->master_id;
			$nestedData[] = $r->text;
			$nestedData[] = ($r->status == 1 )?'visible':'hidden';
			$action = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "add_tag/edit_tag/" . $r->id . "'>Edit</a>&nbsp;"
				;
			
			$nestedData[] = $action;
			$position = "";
			if($key != intval($totalFiltered)-1){
				$position = '<a style="cursor:pointer" title="level down" class="position" data-value="0" data-id="'.$r->id.'" data-position="'.$r->position.'"><i class="btn btn-info btn-sm fa fa-level-down"></i></a>'; 		
			}

			
			if($key != 0) {
			$position .= '<a style="cursor:pointer" title="level up" class="pull-right position" data-value="1" data-id="'.$r->id.'" data-position="'.$r->position.'"><i class="btn btn-info btn-sm  fa fa-level-up"></i></a>';
			}

			$nestedData[] = $position;
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function  ajax_change_post_tag_position() {
		if($_POST['value'] == 0) {
			$v = $this->db->where('position' , $_POST['position'] + 1)->get('post_tags')->row_array();
			$this->db->where('id' , $v['id']);
			$this->db->update('post_tags',array('position'=> $v['position']-1));

			$this->db->where('id' , $_POST['tag_id']);
			$this->db->update('post_tags',array('position'=> $_POST['position']+1));

		} else if($_POST['value'] == 1) {
			$v = $this->db->where('position' , $_POST['position'] - 1)->get('post_tags')->row_array();
			$this->db->where('id' , $v['id']);
			$this->db->update('post_tags',array('position'=> $v['position']+1));

			$this->db->where('id' , $_POST['tag_id']);
			$this->db->update('post_tags',array('position'=> $_POST['position']-1));
		}
		echo json_encode(array('status'=>true));die;
	}


	public function edit_tag($id) {
		if($this->input->post()) {
			$this->form_validation->set_rules('category_id', 'Category Id', 'required');
			$this->form_validation->set_rules('tag', 'Tag', 'required');
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	
            	$update = array(
            		'master_id' => $this->input->post('category_id'),
            		'text'  => $this->input->post('tag'),
            		'status' => $this->input->post('status')
            		);
            	$this->db->where('id',$id);
            	$this->db->update('post_tags',$update);
            	//$this->
 
				$data['page_toast'] = 'Information Updated  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }
		}
		$view_data['page'] = 'tag_add';
		$view_data['main_category'] = $this->add_tag_model->get_main_category();
		$view_data['edit_data'] = $this->add_tag_model->get_tag_by_id($id);
		$data['page_data'] = $this->load->view('tag/edit_tag', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function make_tag_group() {
		if($_POST) {
			//echo "<pre>";print_r($_POST);die;

			$this->form_validation->set_rules('tags_id[]', 'Tag', 'required');
			$this->form_validation->set_rules('tag_group_name', 'Tag group name', 'required');
			
			if ($this->form_validation->run() != FALSE) {
			if(!empty($_POST['tags_id'])) { $tags_ids = implode(',',$_POST['tags_id']); } else { $tags_ids = ''; }
			$insert_data = array('tag_group_name'=>$_POST['tag_group_name'],'master_fk'=>$_POST['tag_category'],'tag_fk_id'=>$tags_ids);
			$this->db->insert('post_tag_group',$insert_data);

			$data['page_toast'] = 'Information Inserted  successfully.';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
			} 
		}
		$view_data['page'] = 'tag_group';
		$view_data['tag_categories'] = $this->add_tag_model->get_main_category();
		$data['page_data'] = $this->load->view('tag/make_tag_group', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_get_tag_group_list() {
			// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			
			0 => 'id',
			1 => 'tag_group_name',
			2 => 'master_fk'
		);

		$query = "SELECT count(id) as total
								FROM post_tag_group 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT mc.text,ptg.* FROM post_tag_group ptg left JOIN master_category mc ON mc.id = ptg.master_fk where 1=1 ";
		

		// getting records as per search parameters
		
		if (!empty($requestData['columns'][0]['search']['value'])) {  //salary
			$sql.=" AND ptg.id  =  '" . $requestData['columns'][0]['search']['value'] . "' ";
		} 
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND ptg.tag_group_name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND ptg.master_fk = '" . $requestData['columns'][2]['search']['value'] . "' ";
		}
		
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			
			$nestedData[] = $r->id;
			$nestedData[] = $r->tag_group_name;
			$nestedData[] = $r->text;
			$action = "<a  class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "add_tag/edit_tag_group/" . $r->id . "'>Edit</a>&nbsp;<a onclick=\"return confirm('Are you really want to delete this group?')\" class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "add_tag/delete_tag_group/" . $r->id . "'>Delete</a>&nbsp;"
				;
			/*if ($r->user_state == 'Active') {
				$action .= "<a class='btn-sm btn btn-warning' href='" . AUTH_PANEL_URL . "admin/block_backend_user/" . $r->id . "/1'>Block</a>";
			} else {
				$action .= "<a class='btn-sm btn btn-success' href='" . AUTH_PANEL_URL . "admin/block_backend_user/" . $r->id . "/0'>Unblock</a>";
			}*/
			$nestedData[] = $action;
			
			
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function delete_tag_group($id) {
		$this->db->where('id',$id)
		->delete('post_tag_group');

		redirect('auth_panel/add_tag/make_tag_group');
	}

	public function ajax_get_subcategory_by_id() {
		$id = $this->input->post('cat_id');
		$sub_cat = $this->add_tag_model->get_sub_category_by_master_cat_id($id);
		$json_data = array(
			"status" => true,
			"data" => $sub_cat   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}



	public function edit_tag_group($id) {
		if($_POST) {
			
			$this->form_validation->set_rules('tags_id[]', 'Tag', 'required');
			$this->form_validation->set_rules('tag_group_name', 'Tag group name', 'required');
			
			if ($this->form_validation->run() != FALSE) {
			if(isset($_POST['tags_id'])) { $tags_ids = implode(',',$_POST['tags_id']); } else { $tags_ids = ''; }
			$updated_data = array('tag_group_name'=>$_POST['tag_group_name'],'master_fk'=>$_POST['tag_category'],'tag_fk_id'=>$tags_ids);
			$this->db->where('id',$id);
			$this->db->update('post_tag_group',$updated_data);

			$data['page_toast'] = 'Information Updated  successfully.';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
			} 
		}
		$view_data['page'] = 'tag_group';
		$view_data['tag_categories'] = $this->add_tag_model->get_main_category();
		$view_data['group_detail'] = $this->add_tag_model->get_tag_group_detail_by_id($id);
		$data['page_data'] = $this->load->view('tag/edit_tag_group', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}



	public function ajax_get_subcategory_by_id_and_checked() {
		$id = $this->input->post('cat_id');
		$checked_id = $this->input->post('checked_id');
		$sub_cat = $this->add_tag_model->get_sub_category_by_master_cat_id($id);
		//echo "<pre>";print_r($sub_cat);die;
		$html = '<div class="col-lg-12">';
		foreach ($sub_cat as $key => $value) {
			$is_cheked = (strpos($checked_id, $value['id']) !== false)?'checked':'';
			 $html .= '<div class=col-md-4><label><input ' . $is_cheked .'  name="tags_id[]" type="checkbox" value="'.$value['id'].'"  >&nbsp' . $value['text'] . '</label></div>';
		}
		$html .= '</div>';

		
		$json_data = array(
			"status" => true,
			"data" => $html   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function clear_server_cache($content) {
		$this->db->where('content_type', $content);
		$this->db->set('version', 'version+1', FALSE);
		$this->db->update('content_version');
		redirect('auth_panel/add_tag?tag_cache_cleared='.base64_encode(2));
	}



}