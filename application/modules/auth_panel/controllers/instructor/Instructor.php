<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Instructor extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');			
		$this->load->library('grocery_CRUD');

	}

	public function _example_output($output = null)
	{
		$this->load->view(AUTH_TEMPLATE.'grocery_crud_template',(array)$output);
	}
	
	public function view_instructors(){
		
			$crud = new grocery_CRUD(); 
			$crud->unset_export();
			$crud->unset_print();
		    $crud->unset_add();
		    $crud->unset_delete();
			$crud->columns(array('user_id'));
			$crud->set_table('course_instructor_information');
			$crud->edit_fields('u_name','b_account','b_ifsc','b_address','b_name','r_sharing');
			$crud->set_relation('user_id','users','name');			
			$crud->unset_texteditor('b_address','full_text');
			$crud->unset_texteditor('b_name','full_text');
			$crud->display_as('b_address','Bank Address')->display_as('b_name','Bank Name')->display_as('u_name','Beneficiary Name')->display_as('b_account','Beneficiary Account No.')->display_as('b_ifsc','Bank IFSC')->display_as('r_sharing','Revenue Sharing');
			$output = $crud->render();
			$this->_example_output($output);
	
	}
	

	



}