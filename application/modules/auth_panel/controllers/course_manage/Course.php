<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		
	}

	public function index() {
		$user_data = $this->session->userdata('active_user_data');
		$view_data['page'] = 'course_management';
		
		$data['page_data']  = $this->load->view('course_management/course' ,$view_data, TRUE);
		$data['page_title'] = "course management";
		echo modules::run('auth_panel/template/call_default_template', $data);
	}

}