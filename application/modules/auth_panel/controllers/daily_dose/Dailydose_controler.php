<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Dailydose_controler extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->model('add_tag_model');
		$this->load->library('form_validation');
		$this->load->model("Video_control_model");
		$this->load->helper('set_mentor');
	}

	public function video_list() {
		$data['page_title'] = "Videos List";
		$view_data['page']  = 'Videos List';
		$view_data['videos'] = $this->Video_control_model->get_video_list();
		$data['page_data'] = $this->load->view('video_channel/video_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function video_analytics() {
		$data['page_title'] = "Videos List";
		$view_data['page']  = 'Videos List';
		$data['page_data'] = $this->load->view('video_channel/video_analytics',array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function amazon_s3_upload($name,$aws_path) {
		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');
				
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).$_FILES["file"]["name"], 
							'SourceFile' => $_FILES["file"]["tmp_name"], 
							'ContentType' => 'image', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	
	}
	public function amazon_s3_upload_videos($name,$aws_path) {
		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');
				
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => rand(0,7896756).str_replace(" ","-",$_FILES["file"]["name"]), 
							'SourceFile' => $_FILES["file"]["tmp_name"], 
							'ContentType' => 'video', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));
				$data=$result->toArray();
				//echo "<pre>";print_r($data);die;
				$video = explode('/',$data['ObjectURL']);
				return end($video);
	}

	public function add_video(){ 
		if($this->input->post()) { 
			$this->form_validation->set_rules('video_title', 'Video title', 'required');
			$this->form_validation->set_rules('video_desc', 'Video description', 'required');
			$this->form_validation->set_rules('main_cat', 'Main category', 'required');
			$this->form_validation->set_rules('sub_cat', 'Sub category', 'required');
	
			if (empty($_FILES['video_file']['name']) and empty($this->input->post('video_url_youtube')) and empty($this->input->post('video_url_vimeo')))
			{ 
				$this->form_validation->set_rules('video_file', 'File', 'required');
			}

			if (empty($_FILES['thumbnail_file']['name']) and empty($this->input->post('thumbnail_url')) )
			{ 
				$this->form_validation->set_rules('thumbnail_file', 'Thumb_File', 'required');
			}

			if($this->input->post('video_url_youtube')){				
				$this->form_validation->set_rules('video_url_youtube', 'Youtube video', 'required|is_unique[video_master.URL]');
				$this->form_validation->set_message('is_unique', 'The video already exists');
			}

			if ($this->form_validation->run() == FALSE) { 
             
            } 
			else { 	
				//video file	  					
				if(!empty($_FILES['video_file']['name'])){
					$file  = $this->amazon_s3_upload_videos($_FILES['video_file'],FANWALL_IMAGES_S3);
				}
				else if($this->input->post('video_url_youtube')){
					$file = $this->input->post('video_url_youtube');
				}else if($this->input->post('video_url_vimeo')){
					$file = $this->input->post('video_url_vimeo');
				}else{ 
						$file = '';
				}
				// thumnail file
				if(!empty($_FILES['thumbnail_file']['name'])){
					$thumb_file  = $this->amazon_s3_upload($_FILES['thumbnail_file'],"course_file_meta");
				}
				else if($this->input->post('thumbnail_url')){
					$thumb_file = $this->input->post('thumbnail_url');
				}else{ 
						$thumb_file = '';
				}

				if($this->input->post('for_dams')==1){
					$for_dams=1;
				}else{
					$for_dams=0;
				}
				if($this->input->post('for_non_dams')==1){
					$for_non_dams=1;
				}else{
					$for_non_dams=0;
				}
				$screen_tag = "";
				if(array_key_exists('screen_tag',$_POST)){
					$screen_tag = implode($_POST['screen_tag'],',');
				}
				$insert_data = array(
					'video_title' => $this->input->post('video_title'),
					'video_type' => $this->input->post('video_type'),
					'URL' => $file,
					'video_desc' => $this->input->post('video_desc'),
					'author_name' => $this->input->post('author_name'),
					'thumbnail_url' => $thumb_file,
					'main_cat' => $this->input->post('main_cat'),
					'sub_cat' => $this->input->post('sub_cat'),
					'start_date' => "",
					'end_date' => "",
					'initial_view' => "",
					'featured' => $this->input->post('featured'),
					'allow_comments' => $this->input->post('allow_comments'),
					'is_new' => $this->input->post('is_new'),
					'for_dams' => $for_dams,
					'for_non_dams' => $for_non_dams,
					'creation_time' => milliseconds(),
					'screen_tag' => $screen_tag
				);
				
				$id =  $this->Video_control_model->insert_video($insert_data);
				page_alert_box('success','Video Added','New video added successfully');
				redirect(AUTH_PANEL_URL.'video_channel/video_control/edit_video?id='.$id);
            }

		}
		
		$data['page_data'] = $this->load->view('video_channel/add_video', array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function edit_video(){ 
		if($this->input->post()) { //print_r($_POST); die;

			$this->form_validation->set_rules('video_title', 'Video title', 'required');
			$this->form_validation->set_rules('video_desc', 'Video description', 'required');
			$this->form_validation->set_rules('main_cat', 'Main category', 'required');
			$this->form_validation->set_rules('sub_cat', 'Sub category', 'required');
			//$this->form_validation->set_rules('featured', 'featured');
			
			
			$is_featured = $this->input->post('featured');
			$is_new = $this->input->post('is_new');
			$video_id = $this->input->get('id');
			$subcat_id = $this->input->post('sub_cat');
			if($is_featured  == 1 || $is_new  == 1){
				$check_featured_and_new = $this->Video_control_model->check_featured_and_new($video_id,$subcat_id,$is_featured,$is_new);
				
				if($check_featured_and_new['total'] > 4){
					page_alert_box('error','Video Not Updated','Already more than 5 are featured* or new*');
					redirect(AUTH_PANEL_URL.'video_channel/video_control/edit_video?id='.$this->input->get('id'));
					
				}
			}
			
			if ($this->form_validation->run() == FALSE) { 
             			echo validation_errors();
            } 
			else { 		  					
				if(!empty($_FILES['video_file']['name'])){
					$file  = $this->amazon_s3_upload_videos($_FILES['video_file'],FANWALL_IMAGES_S3);
					//$file = "test.mp4";
					}
				elseif($this->input->post('video_url_youtube')){
					$file = $this->input->post('video_url_youtube');
				}
				else if($this->input->post('video_url_vimeo')){
					$file = $this->input->post('video_url_vimeo');
				}else{
					$file = "";
				}

				if(!empty($_FILES['thumbnail_file']['name'])){
					$thumb_file  = $this->amazon_s3_upload($_FILES['thumbnail_file'],"course_file_meta");
				}
				else if($this->input->post('thumbnail_url')){
					$thumb_file = $this->input->post('thumbnail_url');
				}else{ 
						$thumb_file = '';
				}
				if($this->input->post('for_dams')==1){
					$for_dams=1;
				}else{
					$for_dams=0;
				}
				if($this->input->post('for_non_dams')==1){
					$for_non_dams=1;
				}else{
					$for_non_dams=0;
				}
				$screen_tag = "";
				if(array_key_exists('screen_tag',$_POST)){
					$screen_tag = implode($_POST['screen_tag'],',');
				}
				$update_data = array(
					'id'		=>$this->input->get('id'),
					'video_title' => $this->input->post('video_title'),
					'video_type' => $this->input->post('video_type'),
					'video_desc' => $this->input->post('video_desc'),
					'author_name' => $this->input->post('author_name'),
					'thumbnail_url' => $thumb_file,
					'main_cat' => $this->input->post('main_cat'),
					'sub_cat' => $this->input->post('sub_cat'),
					'start_date' => "",
					'end_date' => "",
					'initial_view' => "",
					'featured' => $this->input->post('featured'),
					'allow_comments' => $this->input->post('allow_comments'),
					'is_new' => $this->input->post('is_new'),
					'for_dams' => $for_dams,
					'for_non_dams' => $for_non_dams,
					'screen_tag' => $screen_tag
				);
				if($file != '' ){
					$update_data['URL'] = $file;
				}
				$updated = $this->Video_control_model->update_video($update_data);
				$video=$this->Video_control_model->get_video($this->input->get('id'));
				page_alert_box('success','Video Updated','Video updated successfully');
				backend_log_genration($this->session->userdata('active_backend_user_id'),
										$video['title'].' has been updated',
										'VIDEOS');
				if($updated){
					redirect(AUTH_PANEL_URL.'video_channel/video_control/edit_video?id='.$this->input->get('id'));
				}
            }

		}
		if($this->input->get('id')){
			$view_data['video_detail'] = $this->Video_control_model->get_video(['id'=>$this->input->get('id')]);
		}
		$data['page_data'] = $this->load->view('video_channel/edit_video', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function get_youtube_details() {
	    if($this->input->post()){
	      $url=$this->input->post('youtube_url');
	    }
	    echo $details=file_get_contents('https://www.youtube.com/oembed?url='.$url.'&format=json');

	}

	public function get_vimeo_details(){
		$url = $this->input->post('vimeo_url');
	    $host = explode('.', str_replace('www.', '', strtolower(parse_url($url, PHP_URL_HOST))));
	    $host = isset($host[0]) ? $host[0] : $host;
        $video_id = substr(parse_url($url, PHP_URL_PATH), 1);
        $hash = json_decode(file_get_contents("http://vimeo.com/api/v2/video/{$video_id}.json"));
      	$return = array(
                'provider'          => 'Vimeo',
                'title'             => $hash[0]->title,
                'description'       => str_replace(array("<br>", "<br/>", "<br />"), NULL, $hash[0]->description),
                'description_nl2br' => str_replace(array("\n", "\r", "\r\n", "\n\r"), NULL, $hash[0]->description),
                'thumbnail'         => $hash[0]->thumbnail_large,
                'author_name'         => $hash[0]->user_name,
                'video'             => "https://vimeo.com/" . $hash[0]->id,
                'embed_video'       => "https://player.vimeo.com/video/" . $hash[0]->id,
        		);
        echo json_encode($return);
           
		}

	public function ajax_all_video_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'video_title',
			2 => 'video_type',
			3 => 'main_category',
			4 => 'sub_category',
			5 => 'author_name',
			6 => 'thumbnail_url',
			7 => 'thumbnail_url',
			8 => 'creation_time',
			
		);

		$query = "SELECT count(id) as total
				  FROM video_master";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT vm.id,DATE_FORMAT(FROM_UNIXTIME(vm.creation_time/1000), '%d-%m-%Y') as creation_time,vm.video_title,
					   (CASE WHEN vm.video_type = 0 THEN 'Normal'
            				 WHEN vm.video_type = 1 THEN 'Youtube'
             				 WHEN vm.video_type = 2 THEN 'Vimeo'
       					END) AS video_type_name,
					   vm.author_name,vm.thumbnail_url,vm.views ,vm.for_dams ,vm.for_non_dams,vm.is_new ,vm.featured,
					   vm.video_desc,vm.start_date,vm.end_date,mc.text as main_category,pt.text as sub_category
				FROM   video_master as vm 
				join master_category as mc on vm.main_cat=mc.id 
				join post_tags as pt on vm.sub_cat=pt.id ";

		// getting records as per search parameters
	
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND vm.id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND vm.video_title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		if ($requestData['columns'][2]['search']['value'] >= 0 ) {  //salary
			$sql.=" AND vm.video_type LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND mc.text LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND pt.text LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" AND vm.author_name LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}

		if (isset($requestData['columns'][6]['search']['value']) && $requestData['columns'][6]['search']['value'] == 2) { 
			$sql.=" AND vm.thumbnail_url = '' ";
		}elseif(isset($requestData['columns'][6]['search']['value']) && $requestData['columns'][6]['search']['value'] == 1){
			$sql.=" AND vm.thumbnail_url != '' ";
		}

		if (!empty($requestData['columns'][7]['search']['value'])) {  //salary
			$sql.=" AND vm.author_name LIKE '" . $requestData['columns'][7]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][8]['search']['value']) ) {  //salary
			$date = explode(',',$requestData['columns'][8]['search']['value']);
			$start = strtotime($date[0])*1000; 
			$end = (strtotime($date[1])*1000)+86400000; 
			$sql.="  and  vm.creation_time >= '$start' and vm.creation_time <= '$end'"; // BETWEEN $start and $end";
		}

		
		
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
	
		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$extra_info = '<sup class="text-danger bold">';
			$extra_info .= ($r->is_new == 1 )?' New':"";
			$extra_info .= ($r->is_new == 1 && $r->featured == 1)?' & ':"";
			$extra_info .= ($r->featured == 1 )?'Featured':"";
			$extra_info .= '</sup>';
			$nestedData[] = $r->video_title.$extra_info;

			$nestedData[] = $r->video_type_name;
			$nestedData[] = $r->main_category;
			$nestedData[] = $r->sub_category;
			$nestedData[] = $r->author_name;
			$nestedData[] = '<img src="'.$r->thumbnail_url.'" height="50px" width="50px">';
			$nestedData[] = $r->views;
			$nestedData[] = $r->creation_time;
			$nestedData[] = ($r->for_dams == 1 || $r->for_non_dams == 1 )?'<span class="badge bg-success">Published</span>':'<span class="badge bg-important">Unublished</span>';
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='".AUTH_PANEL_URL."video_channel/Video_control/edit_video?id=".$r->id."'>Edit</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function ajax_applied_filter_list($video_id){
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'Session',
			1 => 'FranchiseName',
			2 => 'CourseGroup',
			3 => 'Course',
			4 => 'Batch'	
		);

		$query = "SELECT count(id) as total
				  FROM video_filtration where video_id = $video_id";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT `id`,`video_id`, `FranchiseName`, `CourseGroup`, `Session`, `Course`, `Batch` FROM `video_filtration` where video_id = $video_id  ";

		// getting records as per search parameters
	
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND Session LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND FranchiseName LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		if ($requestData['columns'][2]['search']['value'] >= 0 ) {  //salary
			$sql.=" AND CourseGroup LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND Course LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND Batch LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
	
		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->Session;
			$nestedData[] = $r->FranchiseName;
			$nestedData[] = $r->CourseGroup;
			$nestedData[] = $r->Course;
			$nestedData[] = $r->Batch;
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='".AUTH_PANEL_URL."video_channel/video_control/delete_filter/".$r->id."/".$r->video_id."'>Delete</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function get_video_subcategory($main_cat_id){
	
		$this->db->where('master_id',$main_cat_id);
		$return = $this->db->get("post_tags")->result_array();

		if($this->input->get("return") == "json"){
			echo json_encode($return);die;
		}
		return $return;
	}
	
	public function ajax_video_comments_list($video_id) { 
		//echo $video_id; die;
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		
		
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'user_name',
			2 => 'comment',
			3 => 'time'		
		);

		$query = "SELECT count(id) as total
				  FROM video_master_comment where video_id = $video_id ";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT vmc.id,vmc.video_id,vmc.comment,DATE_FORMAT(FROM_UNIXTIME(vmc.time/1000), '%d-%m-%Y %h:%i:%s') as time,u.name as user_name FROM   video_master_comment as vmc 
				join users as u on vmc.user_id=u.id  
				where video_id = $video_id ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND u.name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND vmc.comment LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$date = explode(',',$requestData['columns'][3]['search']['value']);
			$start = strtotime($date[0])*1000; 
			$end = (strtotime($date[1])*1000)+86400000; 
			$sql.="  and  vmc.time >= '$start' and vmc.time <= '$end'";
		}		
				
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
	
		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->user_name;
			$nestedData[] = $r->comment;
			$nestedData[] = $r->time;
			
			$action = "<a class='btn-xs bold btn btn-info edit_video_comment' onclick='update_video_comment($r->id)' href='javascript:void(0)' data-comment_id='".$r->id."' data-trigger='edit_comment' >Edit</a>";
			//$action .= " <a class='btn-sm btn btn-danger btn-xs bold' href='" . AUTH_PANEL_URL . "video_channel/video_control/delete_video_comment/"  .$r->id."?video_id=".$r->video_id. "'      onclick='\"return confirm('Are you sure to delete this comment');\"' >delete</a>";
			$action .= " <a class='btn-sm btn btn-danger btn-xs bold' href='" . AUTH_PANEL_URL . "video_channel/video_control/delete_video_comment/"  .$r->id."?video_id=".$r->video_id. "'    onclick=\"return confirm('Are you sure to delete this comment');\" >delete</a>";
			
			$nestedData[] = $action;
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}
	
	
	public function get_video_comment_details_by_id($id){
		$this->db->select('video_master_comment.*,users.name as user_name');
		$this->db->where('video_master_comment.id',$id);
		$this->db->join('users','users.id=video_master_comment.user_id');
		$r = $this->db->get('video_master_comment')->row_array();

		if($this->input->get('return') == "json" ){
			echo json_encode($r);die;
		}
		return $r;
	}
	
	public function update_video_comment(){
		if($_POST){ 
			
			$this->db->where('id',$this->input->post('comment_id'));
			$this->db->update('video_master_comment',array('comment'=>$this->input->post('user_comment')));
			//page_alert_box('success','Video Comment','Video comment updated successfully.');	
			
			echo json_encode(array('status'=>true));die;
		}
		//redirect(AUTH_PANEL_URL."video_channel/video_control/ajax_video_comments_list/".$this->input->post('video_id'));
	}
	
	public function delete_video_comment($id) { 
		$video_id = $_GET['video_id']; 
		$status = $this->Video_control_model->delete_video_comment($id,$video_id);
		page_alert_box('success','Action performed','Comment deleted successfully');
		if($status) {
			redirect('auth_panel/video_channel/video_control/edit_video?id='.$video_id);
		}
	} 
	/* Video Filtration */

	public function delete_filter($id,$video_id) { 
		$status = $this->Video_control_model->delete_filter($id);
		page_alert_box('success','Action performed','Filter deleted successfully');
		if($status) {
			redirect(AUTH_PANEL_URL . "video_channel/video_control/edit_video?id=".$video_id);
		}
	} 

	public function years_list_for_filtration() { 
		$data = $this->db->query('select distinct(Session) from users_dams_info order by Session ')->result_array();
		echo json_encode($data);
		die;
	} 

	public function franchise_with_year($year){
		$data = $this->db->query("select distinct(FranchiseName) from users_dams_info where Session = $year")->result_array();
		echo json_encode($data);
		die;
	}
	
	public function courses_group_with_year_and_franchise($franchise,$year){
		$franchise = urldecode ($franchise);
		$data = $this->db->query("select distinct(CourseGroup) from users_dams_info where Session = $year and FranchiseName = '$franchise'")->result_array();
		
		echo json_encode($data);
		die;
	}
	
	public function courses_with_year_and_franchise_and_course_group($franchise,$year,$course_group){
		$franchise = urldecode ($franchise);
		$course_group = urldecode ($course_group);
		$data = $this->db->query("select distinct(Course) from users_dams_info where Session = $year and FranchiseName = '$franchise' and  CourseGroup = '$course_group'")->result_array();
		
		echo json_encode($data);
		die;
	}

	
	public function batches_with_year_and_franchise_and_course_group_and_course($franchise,$year,$course_group,$course){
		$franchise = urldecode ($franchise);
		$course_group = urldecode ($course_group);
		$course = urldecode ($course);
		$data = $this->db->query("select distinct(Batch) from users_dams_info where Session = $year and FranchiseName = '$franchise' and  CourseGroup = '$course_group' and Course = '$course'")->result_array();
		
		echo json_encode($data);
		die;
	}

	public function save_batches_record(){
		if($_POST){
			$array = $_POST['data'];

			foreach($array as $a){
				$query = "INSERT IGNORE INTO video_filtration (video_id , FranchiseName, CourseGroup, Session, Course, Batch) 
							VALUES ('".$a['video_id']."',
									'".$a['FranchiseName']."',
									'".$a['CourseGroup']."',
									'".$a['Session']."',
									'".$a['Course']."',
									'".$a['Batch']."' )";
				$this->db->query($query);					
			}
			echo json_encode(array("status"=>true));die;
		}
	}

	/***
 *     __      __ _      _                 _____                          _       _______            
 *     \ \    / /(_)    | |               / ____|                        | |     |__   __|           
 *      \ \  / /  _   __| |  ___   ___   | (___    ___   __ _  _ __  ___ | |__      | |  __ _   __ _ 
 *       \ \/ /  | | / _` | / _ \ / _ \   \___ \  / _ \ / _` || '__|/ __|| '_ \     | | / _` | / _` |
 *        \  /   | || (_| ||  __/| (_) |  ____) ||  __/| (_| || |  | (__ | | | |    | || (_| || (_| |
 *         \/    |_| \__,_| \___| \___/  |_____/  \___| \__,_||_|   \___||_| |_|    |_| \__,_| \__, |
 *                                                                                              __/ |
 *                                                                                             |___/ 
 */

	public function video_search_list() {
		$data['page_title'] = "Videos Search Tag List";
		$view_data['page']  = 'Videos Search Tag List';
		$view_data['tags'] = $this->Video_control_model->get_tag_list();
		$data['page_data'] = $this->load->view('video_channel/video_tag_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function add_video_tag(){ 
		if($this->input->post('add_tag')) { //print_r($_FILES); die;

			$this->form_validation->set_rules('tag_name', 'Tag Name', 'trim|required|is_unique[video_search_tag_list.tag_name]');
			if ($this->form_validation->run() == TRUE) { 
             	$insert_data = array(
					'tag_name' => $this->input->post('tag_name')
				);
				$this->Video_control_model->insert_video_tag($insert_data);
				page_alert_box('success','Video Tag Added','Video tag has been added successfully.');
            } 
        }	
        if($this->input->post('edit_tag')) { //print_r($_FILES); die;
        	$original_tag=$this->Video_control_model->check_unique_tag(['id'=>$this->input->post('tag_id')]);
        	//print_r($original_value);   	 die;
        	if($this->input->post('tag_name') != $original_tag) {
		       $is_unique_tag =  '|is_unique[video_search_tag_list.tag_name]';
		    } else {
		       $is_unique_tag =  '';
		    }
			$this->form_validation->set_rules('tag_name', 'Tag Name', 'trim|required'.$is_unique_tag.'');
			if ($this->form_validation->run() == TRUE) { 
             	$update_data = array(
             		'id' => $this->input->get('tag_id'),
					'tag_name' => $this->input->post('tag_name')
				);
				$this->Video_control_model->update_tag($update_data);
				page_alert_box('success','Video Tag Updated','Video tag has been updated successfully.');
            } 
            //redirect(AUTH_PANEL_URL.'currency_master/Currency_master/add_currency?currency_id='.$this->input->post('currency_id'));
        }
        if($this->input->get('tag_id')) {
         	$data_currency['tags'] = $this->Video_control_model->get_tag(['id'=>$_GET['tag_id']]);
        } 
		$data['page_data'] = $this->load->view('video_channel/video_tag_list', @$data_currency, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_all_tag_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'tag_name'
		);

		$query = "SELECT count(id) as total
				  FROM video_search_tag_list";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT * 
				FROM   video_search_tag_list where 1=1 ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND tag_name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
	
		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->tag_name;
			
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='".AUTH_PANEL_URL."video_channel/Video_control/add_video_tag?tag_id=".$r->id."'>Edit</a> <a class='btn-xs bold btn btn-danger' href='".AUTH_PANEL_URL."video_channel/Video_control/delete_video_tag?tag_id=".$r->id."'>Delete</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function delete_video_tag(){
		if($this->input->get('tag_id')){
			$result = $this->Video_control_model->delete_video_tag($this->input->get('tag_id'));
		} 
		if($result){
			page_alert_box('success','Video Tag Deleted','Video tag has been deleted successfully.');
		}
		redirect(AUTH_PANEL_URL."video_channel/Video_control/add_video_tag");
	}

	public function download_video_from_channel(){
		$file = urldecode($_GET['file']);
		$file = "https://s3.ap-south-1.amazonaws.com/dams-apps-production/$file";
	    header ('Content-type: octet/stream');
	    header ('Content-disposition: attachment; filename='.rand(999999,999999999).'.mp4;');
	    //header('Content-Length: '.filesize($file));
	    readfile($file);
	    exit;
	}


	public function update_mentor($user_id){

//$user_id=$this->input->get('user_id');

set_mentor($user_id);


	}

}
