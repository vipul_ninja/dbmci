<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Current_affair extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation');
		$this->load->model("currentaffairs_model");
	}

	public function view_owner($id){
		$this->db->where('id',$id);
		$user = $this->db->get('users')->row();
		echo json_encode($user);
	}

	public function add_currentaffairs(){		
		if($_POST){				
			$this->submit_post();
		}
		$view_data['p_heading'] = "Add Feed";
		$view_data['post_type'] = "user_post_type_current_affair";
		$data['page_data'] = $this->load->view('current_affairs/add_currentaffairs', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function add_quiz(){
		if($_POST){
			$this->submit_post();
		}
		$view_data['p_heading'] = "Quiz";
		$view_data['post_type'] = "user_post_type_quiz";
		$data['page_data'] = $this->load->view('current_affairs/add_currentaffairs', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function add_vocab(){
		if($_POST){
			$this->submit_post();
		}
		$view_data['p_heading'] = "Vocab";
		$view_data['post_type'] = "user_post_type_vocab";
		$data['page_data'] = $this->load->view('current_affairs/add_currentaffairs', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function add_article(){
		if($_POST){
			$this->submit_post();
		}
		$view_data['p_heading'] = "Article";
		$view_data['post_type'] = "user_post_type_article";
		$data['page_data'] = $this->load->view('current_affairs/add_currentaffairs', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}



	public function submit_post(){
		// echo "<pre>";		
		// print_r($_POST);
		// echo "</pre>";
		$errors = $image_error = ""; 
		$display_picture = false;

		$return = array('status'=>false,"errors"=>array()); 
		$this->form_validation->set_rules('course_cat_id[]','Subject type', 'trim|required');
		$this->form_validation->set_rules('post_type','Post type', 'trim|required');
		$this->form_validation->set_rules('user_id','User id', 'trim|required');
		$this->form_validation->set_rules('subject_id','Subject Type', 'trim|required');
		
		if($_POST['post_type'] == "user_post_type_normal"){
			$this->form_validation->set_rules('description','Description', 'trim|required');
		}
		if($_POST['post_type'] == "user_post_type_article" 
			|| $_POST['post_type'] == "user_post_type_article"
			|| $_POST['post_type'] == "user_post_type_current_affair"
			|| $_POST['post_type'] == "user_post_type_vocab"
			|| $_POST['post_type'] == "user_post_type_quiz" ){

			$this->form_validation->set_rules('title','Title', 'trim|required');
			//custum validation 
			if($_POST['x1'] == "" && $_POST['y1'] == "" && $_POST['w'] == "" && $_POST['h'] == "" ){
				$image_error = "Please upload and crop image.";
			}
			$display_picture = true;
		}

		if($_POST['post_type'] == "user_post_type_quiz"){
			$this->form_validation->set_rules('quiz_id','Quiz id', 'trim|required');
		}

		if($_POST['post_type'] == "user_post_type_video"){
			$this->form_validation->set_rules('video_id','Video id', 'trim|required');
		}

		$this->form_validation->run();
		$all_errors = $this->form_validation->get_all_errors();
		
		
		if(is_array($all_errors)){
			$return['errors'] = array_values($all_errors);
		}

		if($image_error !=""){
			$return['errors'][] = $image_error;
		}

		$return['errors'] = implode('</br>',$return['errors']);
		if($return['errors'] != ""){
			echo json_encode($return);	die;
		}

		$data_post_counter = array();
		$source_base_64 = $source_base_64_100 = "";
		if($display_picture == true ){
			$_POST['x1'];
			$_POST['y1'];
			$_POST['w'];
			$_POST['h'];
			$iWidth =700;
			$iHeight = 540; 
			$iJpgQuality = 100;
			$sTempFileName = $_FILES["image_file"]["tmp_name"];
			$aSize = getimagesize($sTempFileName);
			switch($aSize[2]) {
				case IMAGETYPE_JPEG:
				$sExt = '.jpg';
				$vImg = @imagecreatefromjpeg($sTempFileName);
				$info_source_base_64 = "data:image/jpeg;base64,";
				break;
				case IMAGETYPE_PNG:
				$sExt = '.png';
				$vImg = @imagecreatefrompng($sTempFileName);
				$info_source_base_64 = "data:image/png;base64,";
				break;
			}
			// create a new truee color image
			$vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
			// copy and resize part of an image with resampling
			$data =    imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);
			ob_start ();
			imagejpeg($vDstImg,null, $iJpgQuality);
			$image_data = ob_get_contents ();
			ob_end_clean ();
			$source_base_64 = $info_source_base_64.base64_encode($image_data);
			$iWidth =100; $iHeight = 62;
			$vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
			// copy and resize part of an image with resampling
			$data =    imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);
			ob_start ();
			imagejpeg($vDstImg,null, $iJpgQuality);
			$image_data = ob_get_contents ();
			ob_end_clean ();
			$source_base_64_100 = $info_source_base_64.base64_encode($image_data);
		}	
		/* start saving post */
		$creation_time = milliseconds();
		$data_post_counter['creation_time'] = $creation_time;
		$data_post_counter['user_id'] = $_POST['user_id'];
		$data_post_counter['post_type'] = $_POST['post_type'];
		$data_post_counter['post_headline'] = $_POST['title'];
		$data_post_counter['post_tag'] = 1;
		if($_POST['datefilter'] != "" ){
			$date_range = explode('-',$_POST['datefilter']);
			$date_start = explode('/',$date_range[0]);
			$date_end = explode('/',$date_range[1]);
			$finalDate1 = trim($date_start[2]).'-'.trim($date_start[0]).'-'.trim($date_start[1]);
			$finalDate2 = trim($date_end[2]).'-'.trim($date_end[0]).'-'.trim($date_end[1]); 

			$data_post_counter['start_publish_date'] = $finalDate1  ;
			$data_post_counter['end_publish_date'] = $finalDate2;
		}


		$file_name = rand(0,7896756).$_FILES["image_file"]["name"];
		if($source_base_64 != ""){
			$data_post_counter['display_picture'] = $this->amazon_s3_upload_with_base64('post_display',$file_name,$source_base_64);
			$this->amazon_s3_upload_with_base64('post_display_100',$file_name,$source_base_64_100);
		}

		$this->db->insert('post_counter', $data_post_counter);
		//echo $this->db->last_query(); die;
		$record['text'] = $_POST['description'];
		$record['post_id'] = $this->db->insert_id();
		$record['post_text_type'] = 'text';
		$this->db->insert('user_post_type_text', $record);

		$course_cate	= $this->input->post('course_cat_id');
		if($course_cate !== NULL){
			foreach($course_cate as $c){
				$array_ins = array("sub_cate_id"=>$c,"post_id"=>$record['post_id']);	
				$this->db->insert('post_category_relationship',$array_ins);
			}	
		}	

		$this->db->where('id',$data_post_counter['user_id']);
		$this->db->set('post_count', 'post_count+1', FALSE);
		$this->db->update("users");
		if($this->input->post('quiz_id')){
			$in=array();
			$in['post_id'] = $record['post_id'];
			$in['quiz_id'] = $_POST['quiz_id'];
			$in['creation_time'] = milliseconds();
			$this->db->insert('user_post_quiz_relationship',$in); 
		}
		if($this->input->post('video_id')){
			$in=array();
			$in['post_id'] = $record['post_id'];
			$in['video_id'] = $_POST['video_id'];
			$in['creation_time'] = milliseconds();
			$this->db->insert('user_post_video_relationship',$in); 
		}
				
		if($this->input->post('subject_id')){
			$in=array();
			$this->db->where('post_id',$record['post_id']);
			$this->db->delete('post_subject_relationship');
			
			$in['post_id'] = $record['post_id'];
			$in['subject_id'] = $_POST['subject_id'];
			$this->db->insert('post_subject_relationship',$in); 
		}		

		if( isset($record['post_id']) &&  $record['post_id']!=''){
			page_alert_box('success','Post','Post has been added successfully');
			$return['status'] = true;
			echo json_encode($return);
			die;
		}
		
	}
		

	public function currentaffairs_list() {	
		$data['page_title'] = "Current affairs List";
		$view_data['page']  = 'Current affairs List';
		$view_data['data_no'] = "Total Current Affairs";

		//$view_data['videos'] = $this->currentaffairs_model->get_currentaffair_list();
		$data['page_data'] = $this->load->view('current_affairs/currentaffairs_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function video_list() {	
		$data['page_title'] = "Video List";
		$view_data['page']  = 'Video List';
		$view_data['data_no'] = "Total  Video's";

		//$view_data['videos'] = $this->currentaffairs_model->get_currentaffair_list();
		$data['page_data'] = $this->load->view('current_affairs/currentaffairs_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
public function article_list() {	
		$data['page_title'] = "Article List";
		$view_data['page']  = 'Article List';
		$view_data['data_no'] = "Total Articles";

		//$view_data['videos'] = $this->currentaffairs_model->get_currentaffair_list();
		$data['page_data'] = $this->load->view('current_affairs/currentaffairs_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

		public function vocab_list() {
		$data['page_title'] = "Vocab Dose";
		$view_data['page']  = 'Vocab Dose';
	    $view_data['data_no'] = "Total Vocab Dose";

		//$view_data['videos'] = $this->currentaffairs_model->get_currentaffair_list();
		$data['page_data'] = $this->load->view('current_affairs/currentaffairs_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function quiz_list() {
		$data['page_title'] = "Quiz";
		$view_data['page']  = 'Quiz List';
		 $view_data['data_no'] = "Total Quiz";

		//$view_data['videos'] = $this->currentaffairs_model->get_currentaffair_list();
		$data['page_data'] = $this->load->view('current_affairs/currentaffairs_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
///---------------image store on aws server---start-------//
private function amazon_s3_upload_with_base64($aws_path,$file_name,$source_base_64) {
		$image_parts = explode(";base64,", $source_base_64);
    $image_type_aux = explode("image/", $image_parts[0]);
    $image_type = $image_type_aux[1];
    $image_base64 = base64_decode($image_parts[1]);
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.$file_name,
							'ContentType'=> 'image/' . $image_type,
							'Body' => $image_base64,
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array('param1' => 'value 1','param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	}
///---------------image store on aws server---start-------//


public function ajax_all_current_affairs_list() {	
		//echo $video_id; die;
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'post_headline',		
			2 => 'display_picture'
		);

		$query = "SELECT count(id) as total
				  FROM post_counter  where post_type='".$requestData['post_type']."' and status='0'";
//echo $query;
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;


        $sql = "SELECT *,DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_timee from post_counter  where  status='0' and  1=1 ";

		if(!empty($requestData['post_type']) && isset($requestData['post_type'])) {   //name
		$sql.=" AND post_type='".$requestData['post_type']."'";
		}

		// getting records as per search parameters
		// if (!empty($requestData['columns'][0]['search']['value'])) {   //name
		// 	$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		// }
		if (!empty($requestData['columns'][0]['search']['value'])) {  //salary
			$sql.=" AND post_headline LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND creation_timee LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}		
		//echo $sql;
		$query = $this->db->query($sql)->result();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length		
		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			// $nestedData[] = $r->id;
			$nestedData[] = $r->post_headline;
			$nestedData[] = $r->creation_timee;
		//	z$nestedData[] = $r->start_publish_date .'-'. $r->end_publish_date;
			if($r->publish == '0' ){
				// $nestedData[] = "<label class='switch'><input type='checkbox'   id='switch' class='alert-status' data-size='normal' name='my-checkbox' data-on-text='Android' data-checkbox='VALUE3' value=''><span class='slider round'></span></label>";
$nestedData[] = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to Unpublish this data?');\" class=' btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "daily_dose/Current_affair/disable_current_affair/" . $r->id . "/".$r->post_type."'>Published</a>";


			}else if($r->publish =='1'){
				$nestedData[] = "<a  onclick=\"return confirm('Warning !!!!  Do you really want to publish this data?');\" class=' btn-xs bold btn btn-warning' href='" . AUTH_PANEL_URL . "daily_dose/current_affair/enable_current_affair/" . $r->id . "/".$r->post_type."'> Unpublish</a>";

			}
			// $nestedData[] = "<img src='".$r->display_picture."' height='50' width='50' >" ;
		//	if($r->publish == 0 ){
			$action = "<a class='btn-xs bold btn btn-info edit_video_comment' href='".AUTH_PANEL_URL."daily_dose/current_affair/edit_post?post_id=".$r->id."' data-trigger='edit_comment' >Edit</a>";
		//	}else{
			//	$action = "<a class='btn-xs bold btn btn-info edit_video_comment' style='visibility:hidden' href='".AUTH_PANEL_URL."daily_dose/current_affair/edit_post?post_id=".$r->id."' data-trigger='edit_comment' >Edit</a>";
				
		//	}
			$action1 = "<a class='btn-xs bold btn btn-danger ' onclick=\"return confirm('Warning !!!!  Do you really want to delete this data?');\" href='".AUTH_PANEL_URL."daily_dose/current_affair/delete__current_affair/".trim($r->id) . "/".trim($r->post_type)."' data-trigger='' >delete</a>";		

			$nestedData[] = $action. '    ' .$action1;
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


public function disable_current_affair($stream_id,$posttype){
		$this->db->where('id',$stream_id);
	  $this->db->update('post_counter',array('publish'=>1));
		page_alert_box('success',' Disabled','Data  has been unpublished successfully');
		$post_type= $this->input->get('post_type');
	redirect(AUTH_PANEL_URL.'daily_dose/current_affair/currentaffairs_list?&post_type='.$posttype);


		}
//	daily_dose/current_affair/currentaffairs_list?post_type=user_post_type_current_affair
	

	public function enable_current_affair($stream_id,$posttype){
		$this->db->where('id',$stream_id);
	   $this->db->update('post_counter',array('publish'=>0));
		page_alert_box('success',' Enabled',' Data has been published successfully');
	redirect(AUTH_PANEL_URL.'daily_dose/current_affair/currentaffairs_list?&post_type='.$posttype);
//	currentaffairs_list?post_type=user_post_type_current_affair
	}

	public function delete__current_affair($id,$posttype){
		
		   $result = $this->currentaffairs_model->delete_current_affair($id,$posttype);
		  // echo $this->db->last_query();
	   if($result){
		   page_alert_box('success','Current Affair','Current Affair has been deleted successfully.');
	   }

	   redirect(AUTH_PANEL_URL.'daily_dose/current_affair/currentaffairs_list?&post_type='.$posttype);
   }

//EDIT

	public function edit_post(){
		
		if($this->input->post()){	
			$this->edit_post_handler();
		}	

		$id = $this->input->get('post_id');
		$view_data['p_heading']  = 'Edit Feed';
		$data['page_title'] = "Edit Feed";

		$option = array(
			'is_json' => true,
			'data' => array("user_id"=>"0","post_id"=>$id)
		);

		$ch = curl_init(site_url('data_model/fanwall/fan_wall/get_single_post_data_for_user'));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		curl_close($ch);
		$post = json_decode($result,true);
		$view_data['post_content']  = $post['data'];
		$data['page_data'] = $this->load->view('current_affairs/edit_post', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	
	public function edit_post_handler(){
		
		$edit_id = $this->input->post('post_id');
		$errors = $image_error = ""; 
		$display_picture = false;

		$return = array('status'=>false,"errors"=>array()); 
		$this->form_validation->set_rules('course_cat_id[]','Exam type', 'trim|required');
		$this->form_validation->set_rules('post_type','Post type', 'trim|required');
		$this->form_validation->set_rules('user_id','User id', 'trim|required');

		if($_POST['post_type'] == "user_post_type_normal"){
			$this->form_validation->set_rules('description','Description', 'trim|required');
		}
		if($_POST['post_type'] == "user_post_type_article" 
			|| $_POST['post_type'] == "user_post_type_article"
			|| $_POST['post_type'] == "user_post_type_current_affair"
			|| $_POST['post_type'] == "user_post_type_vocab"
			|| $_POST['post_type'] == "user_post_type_quiz" ){

			$this->form_validation->set_rules('title','Title', 'trim|required');
			//custum validation 

			if($_FILES && $_FILES['image_file']['name']!=='' &&  ($_POST['x1'] == "" && $_POST['y1'] == "" && $_POST['w'] == "" && $_POST['h'] == "") ){
				$image_error = "Please upload and crop image.";
			}
			$display_picture = true;
		}


		$this->form_validation->run();
		$all_errors = $this->form_validation->get_all_errors();
		
		
		if(is_array($all_errors)){
			$return['errors'] = array_values($all_errors);
		}

		if($image_error !=""){
			$return['errors'][] = $image_error;
		}

		$return['errors'] = implode('</br>',$return['errors']);
		if($return['errors'] != ""){
			echo json_encode($return);	die;
		}

		$data_post_counter = array();
		if($_FILES['image_file']['name']!==''){
		$source_base_64 = $source_base_64_100 = "";
		if($display_picture == true ){
			$_POST['x1'];
			$_POST['y1'];
			$_POST['w'];
			$_POST['h'];
			$iWidth =700;
			$iHeight = 540; 
			$iJpgQuality = 100;
			$sTempFileName = $_FILES["image_file"]["tmp_name"];
			$aSize = getimagesize($sTempFileName);
			switch($aSize[2]) {
				case IMAGETYPE_JPEG:
				$sExt = '.jpg';
				$vImg = @imagecreatefromjpeg($sTempFileName);
				$info_source_base_64 = "data:image/jpeg;base64,";
				break;
				case IMAGETYPE_PNG:
				$sExt = '.png';
				$vImg = @imagecreatefrompng($sTempFileName);
				$info_source_base_64 = "data:image/png;base64,";
				break;
			}
			// create a new truee color image
			$vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
			// copy and resize part of an image with resampling
			$data =    imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);
			ob_start ();
			imagejpeg($vDstImg,null, $iJpgQuality);
			$image_data = ob_get_contents ();
			ob_end_clean ();
			$source_base_64 = $info_source_base_64.base64_encode($image_data);
			$iWidth =100; $iHeight = 62;
			$vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
			// copy and resize part of an image with resampling
			$data =    imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);
			ob_start ();
			imagejpeg($vDstImg,null, $iJpgQuality);
			$image_data = ob_get_contents ();
			ob_end_clean ();
			$source_base_64_100 = $info_source_base_64.base64_encode($image_data);
		}	
		/* start saving post */	
       
		$file_name = rand(0,7896756).$_FILES["image_file"]["name"];
		if($source_base_64 != ""){
			$data_post_counter['display_picture'] = $this->amazon_s3_upload_with_base64('post_display',$file_name,$source_base_64);
			$this->amazon_s3_upload_with_base64('post_display_100',$file_name,$source_base_64_100);
		}
	}
	$data_post_counter['post_headline'] = $_POST['title'];
	$data_post_counter['post_tag'] = 1;
		$this->db->where('id',$edit_id)->update('post_counter', $data_post_counter);

		$record['text'] = $_POST['description'];
		$record['post_id'] = $edit_id;
		$record['post_text_type'] = 'text';
		$this->db->where('post_id',$edit_id)->update('user_post_type_text', $record);

		$course_cate	= $this->input->post('course_cat_id');
		if($course_cate !== NULL){
			$this->db->where('post_id',$edit_id)->delete('post_category_relationship');
			foreach($course_cate as $c){
				$array_ins = array("sub_cate_id"=>$c,"post_id"=>$record['post_id']);	
				$this->db->insert('post_category_relationship',$array_ins);
			}	
		}	

		if($this->input->post('quiz_id')){
			$in['post_id'] = $edit_id;
			$in['quiz_id'] = $_POST['quiz_id'];
			$this->db->where('post_id',$edit_id)->update('user_post_quiz_relationship',$in); 
		}

		if($this->input->post('subject_id')){
			$indata=array();
			
			$indata['post_id'] = $record['post_id'];
			$this->db->where('post_id',$edit_id);
			$indata['subject_id'] = $_POST['subject_id'];
			$this->db->update('post_subject_relationship',$indata); 
		}	

		page_alert_box('success','Feed','Feed has been update successfully');
		$return['status'] = true;
		echo json_encode($return);
		die;
		
	}
}
