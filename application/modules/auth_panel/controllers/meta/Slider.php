<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation');
	}
	
    public function amazon_s3_upload($name,$aws_path) {

		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).str_replace([':', ' ', '/', '*','#','@','%'],"_",$_FILES["file"]['name']),
							'SourceFile' => $_FILES["file"]['tmp_name'],
							'ContentType' => 'image',
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array('param1' => 'value 1','param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	}

	public function amazon_s3_upload_custom($name,$tmp_name,$aws_path) {		
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).str_replace([':', ' ', '/', '*','#','@','%'],"_",$name),
							'SourceFile' => $tmp_name,
							'ContentType' => 'image',
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array('param1' => 'value 1','param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	}
/////////////////////////////////////////////////////Add  Slider Name////////////////////////////////////////////
	public function add_slider() 
	{	
		if($this->input->post())	
		{		
			$this->form_validation->set_rules('name', 'Username', 'required');			
			if ($this->form_validation->run() == FALSE)
                {
                      
                }else{
					$data =	array('name'=>ucfirst($this->input->post('name')),
					'created_on' => milliseconds(),'created_by' => milliseconds(),
					'created_by'=>$this->session->userdata('active_backend_user_id'));				   
					$this->db->insert('slider_master',$data);	
					page_alert_box('success','Name','Name has been added successfully');																	
				}				
		}
		 $data['page_data'] = $this->load->view('meta/slider', array(), TRUE);
		 echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
		}
/////////////////////////////////////////////////////Edit Slider Name///////////////////////////////////////////////////////////////
public function edit_slider_name($id)
{	
   if($this->input->post()){
	  //print_r($this->input->post());die;$this->form_validation->set_message('stream_edit_unique', 'Sorry, This Sub Stream name already exist.');
	   			
			$update_data = array(
						   'name' => ucfirst($this->input->post('name')),
						   'last_updated' => milliseconds()
		   );					    
		   $this->db->where('id',$id);
		   $this->db->update('slider_master',$update_data);
		   page_alert_box('success','Name Updated','Name has been updated successfully');
		   redirect(AUTH_PANEL_URL.'meta/Slider/add_slider');
   
   }
   $view_data['page']  = 'edit_stream';
   $data['page_title'] = "Stream edit";
   $this->db->where('id',$id);
   $view_data['slider_data'] = $this->db->get('slider_master')->row_array();   
   $data['page_data'] = $this->load->view('meta/edit_slider',$view_data, TRUE);
   echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
}
/////////////////////////////////////////////////////getting slider name through AjAX//////////////////////////////////////////////////////////
		public function ajax_slider_name_list(){		
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'created_on',
			3 => 'last_updated'			
		);
		$query = "SELECT count(id) as total FROM slider_master";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;
		$sql = "SELECT id,name,last_updated,created_by,DATE_FORMAT(FROM_UNIXTIME(last_updated/1000) , '%d-%m-%Y %h:%i:%s') as last_updated ,DATE_FORMAT(FROM_UNIXTIME(created_on/1000), '%d-%m-%Y %h:%i:%s') as created_on from slider_master";
		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		$query = $this->db->query($sql)->result();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
		$result = $this->db->query($sql)->result();
		$data = array();
		$start  = 0 ;
		foreach ($result as $r) {  // preparing an array

	    $this->db->select('status'); 
        $this->db->from('slider_master');   
        $this->db->where('id', $r->id);
	    $status= $this->db->get()->result();
	
	    $status_final = $status[0]->status;

	   if($status_final==1){ 
		$enable="<a class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "meta/Slider/disable/" . $r->id ."?stat=2'>&nbsp Disable</a>";
	     }else{ 
		$enable="<a class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "meta/Slider/enable/" . $r->id ."?stat=1'>&nbsp Enable</a>&nbsp";
		 }
		$nestedData = array();
		
	     	//$nestedData[] = ++$start;
		    $nestedData[] = $r->id;
			$nestedData[] = $r->name;			
			$nestedData[] = $r->created_on;
			$nestedData[] = $r->last_updated;
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "meta/Slider/edit_slider_name/" . $r->id . "'><i class='fa fa-pencil'>
			</i>&nbsp Edit</a>&nbsp
			<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "meta/Slider/add_image/" . $r->id . "?name=".$r->name."'><i class='fa fa-plus'></i>&nbsp Add Image</a>
		
			".$enable;
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data ,  // total data array
			"query" =>$this->db->last_query()
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function add_image($id){			
		if($this->input->post())	
		{		
			if($_FILES && $_FILES['image_url']['name']){		
						$file  = $this->amazon_s3_upload($_FILES['image_url'],'course_file_meta');
						}
				$data=array('slider_id'=>$id,
							'image_url'=>$file,
							'text'=>$this->input->post('text'),
							'created_by'=>$this->session->userdata('active_backend_user_id'));	
				 $this->db->insert('slider_image_master',$data);							
				 page_alert_box('success','Image','Image has been added successfully');																	
					
		}
		 $data['id']=$id;
		 $data['page_data'] = $this->load->view('meta/add_image', $data, TRUE);
		 echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
		}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
public function enable($id){	
$stat=	$_GET['stat'];	
// echo $id;
$data=array('status'=>$stat);
$this->db->where('id',$id);
$this->db->update('slider_master',$data);
page_alert_box('success','Enable','Enable Successfully');
redirect(AUTH_PANEL_URL.'meta/Slider/add_slider');
}

public function disable($id){	
	$stat=	$_GET['stat'];
	$data=array('status'=>$stat);
	$this->db->where('id',$id);
	$this->db->update('slider_master',$data);
	echo($this->db->last_query());
	page_alert_box('success','Disable','Disabled Successfully');
	redirect(AUTH_PANEL_URL.'meta/Slider/add_slider');
}
/////////////////////////////////////////////////////getting slider image  AjAX//////////////////////////////////////////////////////////
		public function ajax_slider_image_list($id){					
			$requestData = $_REQUEST;
			$columns = array(
				// datatable column index  => database column name
				0 => 'id',
				1 => 'image_url',
				2 => 'text'
				
			);					
			$query = "SELECT count(id) as total FROM slider_image_master";
			$query = $this->db->query($query);
			$query = $query->row_array();
			$totalData = (count($query) > 0) ? $query['total'] : 0;
			$totalFiltered = $totalData;
			$sql = "SELECT id,image_url,text from slider_image_master where slider_id=$id";									
			$query = $this->db->query($sql)->result();
			$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
			$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
			$result = $this->db->query($sql)->result();
			$data = array();
			$start  = 0 ;
			foreach ($result as $r) {  // preparing an array
				$nestedData = array();
				 //$nestedData[] = ++$start;
				$nestedData[] = $r->id;
				$nestedData[] = "<img src='".$r->image_url."' height='60px' width='60px'>";			
				$nestedData[] = $r->text;				
				$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "meta/Slider/edit_image/" . $r->id ."'><i class='fa fa-pencil'>
				</i>&nbsp Edit</a>&nbsp	
				<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "meta/Slider/delete_image/" . $r->id ."'
				onclick='return confirm('Are you sure you want to delete this item?');'><i class='fa fa-pencil'>
				</i>&nbsp Delete</a>&nbsp				
				"; 
				$data[] = $nestedData;
			}
	
			$json_data = array(
				"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
				"recordsTotal" => intval($totalData), // total number of records
				"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data" => $data ,  // total data array
				"query" =>$this->db->last_query()
			);	
			echo json_encode($json_data);  // send data as json format
		}
		public function edit_image($id){					
			if($this->input->post())	
			{		
				if($_FILES && $_FILES['image_url']['name']){		
							$file  = $this->amazon_s3_upload($_FILES['image_url'],'course_file_meta');
							$data=array(
								'image_url'=>$file,
								'text'=>$this->input->post('text'),
								'created_by'=>$this->session->userdata('active_backend_user_id'));		
						}else{
					        $data=array(								
								'text'=>$this->input->post('text'),
								'created_by'=>$this->session->userdata('active_backend_user_id'));								
					}
					$this->db->where('id',$id);	
					 $this->db->update('slider_image_master',$data);							
					 page_alert_box('success','Data','Data has been Updated successfully');				
			}
								$this->db->where('id',$id);
			 $data['data_image']=$this->db->get('slider_image_master')->row_array();
			 $data['page_data'] = $this->load->view('meta/edit_image', $data, TRUE);
			 echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
			}
			public function delete_image($id){							
				if($this->input->post())	
				{		
					
						$this->db->where('id',$id);	
						 $this->db->delete('slider_image_master');							
						 page_alert_box('success','Data','Data has been Updated successfully');				
				}
				
				}
	
}