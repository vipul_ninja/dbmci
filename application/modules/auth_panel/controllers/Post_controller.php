<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
class Post_controller extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		//$this->load->helper("push_helper");
		$this->load->model("post_controller_model");
	}

	public function delete_post($id) {
		$res = $this->post_controller_model->delete_post($id);
			redirect(AUTH_PANEL_URL.'Fan_wall/all_post');
	}


	public function amazon_s3_upload($name,$aws_path) {
		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');
				
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).$_FILES["file"]["name"], 
							'SourceFile' => $_FILES["file"]["tmp_name"], 
							'ContentType' => 'image', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	
}


	public function edit_post_user_post_type_normal($id) {
			if($_POST) 
			{
				//echo "<pre>";print_r($_POST);echo "</pre>";die;
				
				$option = array(
					'is_json' => true,
					'data' 	=> 	array("user_id"=>$_POST['user_id'],
									"post_id"=>$_POST['post_id'],
									"post_type"=>'text',
									"text" => $_POST['text'],
									"post_tag" => $_POST['post_tag'],
									"file" => ""
								)
				);
				$ch = curl_init(site_url('data_model/user/post_text/edit_post'));
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$result = curl_exec($ch);
				curl_close($ch);
				$post = json_decode($result,true);
				if($post['status'] == 1) {
						$data['page_toast'] = 'Information Updated  successfully.';
						$data['page_toast_type'] = 'success';
						$data['page_toast_title'] = 'Action performed.';
				}
			}

			$option = array(
			   'is_json' => true,
			   'data' => array("user_id"=>"0","post_id"=>$id)
			);

			$ch = curl_init(site_url('data_model/fanwall/fan_wall/get_single_post_data_for_user'));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$result = curl_exec($ch);
			curl_close($ch);
			$post = json_decode($result,true);

			$view_data['']  = '';/* please be sure page should be there blank.  */
			$view_data['record'] = $post['data'];
			$data['page_data'] = $this->load->view('fan_wall/edit_post_user_post_type_normal', $view_data, TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function edit_post_user_post_type_mcq($id) {
			if($_POST) {
				//echo "<pre>";print_r($_POST);die;
				$option = array(
					'is_json' => true,
					'data' 	=> 	array("user_id"=>$_POST['user_id'],
									"post_id"=>$_POST['post_id'],
									"post_tag" => $_POST['post_tag'],
									"question"=>$_POST['text'],
									"answer_one" => $_POST['answer_one'],
									"answer_two" => $_POST['answer_two'],
									"answer_three" => $_POST['answer_three'],
									"answer_four" => $_POST['answer_four'],
									"answer_five" => $_POST['answer_five'],
									"right_answer" => $_POST['right_answer']
								)
				);
				$ch = curl_init(site_url('data_model/user/post_mcq/edit_mcq'));
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$result = curl_exec($ch);
				curl_close($ch);
				$post = json_decode($result,true);
				
				if($post['status'] == 1) {
						$data['page_toast'] = 'Information Updated  successfully.';
						$data['page_toast_type'] = 'success';
						$data['page_toast_title'] = 'Action performed.';
				}
				
			}
			$option = array(
			   'is_json' => true,
			   'data' => array("user_id"=>"0","post_id"=>$id)
			);

			$ch = curl_init(site_url('data_model/fanwall/fan_wall/get_single_post_data_for_user'));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$result = curl_exec($ch);
			curl_close($ch);
			$post = json_decode($result,true);
//echo "<pre>";print_r($post['data']);die;
			$view_data['']  = '';/* please be sure page should be there blank.  */
			$view_data['record'] = $post['data'];
			$data['page_data'] = $this->load->view('fan_wall/edit_post_user_post_type_mcq', $view_data, TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function view_full_post_user_post_type_normal($id) {
			$view_data['post_id']   = $id;
			$data['page_data'] = $this->load->view('fan_wall/view_post_user_post_type_normal', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_get_feed() {

		$option = array(
			   'is_json' => true,
			   'data' => array("user_id"=>"0","post_id"=>$_POST['id'])
			  );

			  $ch = curl_init(site_url('data_model/fanwall/fan_wall/get_single_post_data_for_user'));
			  curl_setopt($ch, CURLOPT_POST, 1);
			  curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
			  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			  $result = curl_exec($ch);
			  curl_close($ch);
			  $post = json_decode($result,true);
		
		echo json_encode(array("status"=>true,"data"=>$post));die;
		
	}


	public function view_full_post_user_post_type_mcq($id) {
		die($id);
	}
}