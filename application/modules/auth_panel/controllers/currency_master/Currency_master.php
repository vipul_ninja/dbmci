<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Currency_master extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->model('add_tag_model');
		$this->load->library('form_validation');
		$this->load->model("currency_master_model");
	}

	public function currency_list() {
		$data['page_title'] = "Currency List";
		$view_data['page']  = 'Currency List';
		$view_data['currency'] = $this->currency_master_model->get_currency_list();
		$data['page_data'] = $this->load->view('currency_master/currency_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function add_currency(){ 
		if($this->input->post('add_currency')) { //print_r($_FILES); die;

			$this->form_validation->set_rules('mcc_code', 'MCC Code', 'trim|required|is_unique[currency_value_master.mcc_code]|is_numeric|greater_than[0]');
			$this->form_validation->set_rules('value', 'Currency Value', 'trim|required|is_numeric|greater_than[0]|less_than[200]');
			$this->form_validation->set_rules('utf_value', 'UTF Value', 'trim|required');
			$this->form_validation->set_rules('name', 'Currency Name', 'trim|is_unique[currency_value_master.name]|required');
			if ($this->form_validation->run() == TRUE) { 
             	$insert_data = array(
					'mcc_code' => $this->input->post('mcc_code'),
					'value' => $this->input->post('value'),
					'utf_value' => $this->input->post('utf_value'),
					'name' => $this->input->post('name')
				);
				$this->currency_master_model->insert_currency($insert_data);
            } 
        }	
        if($this->input->post('edit_currency')) { //print_r($_FILES); die;
        	$original_mcc=$this->currency_master_model->check_unique_mcc(['id'=>$this->input->post('currency_id')]);
        	//print_r($original_value);   	 die;
        	if($this->input->post('mcc_code') != $original_mcc) {
		       $is_unique_mcc =  '|is_unique[currency_value_master.mcc_code]';
		    } else {
		       $is_unique_mcc =  '';
		    }
			$this->form_validation->set_rules('mcc_code', 'MCC Code', 'trim|required'.$is_unique_mcc.'|is_numeric|greater_than[0]');
			$this->form_validation->set_rules('value', 'Currency Value', 'trim|required|is_numeric|greater_than[0]|less_than[200]');
			$this->form_validation->set_rules('utf_value', 'UTF Value', 'trim|required');
			$original_name=$this->currency_master_model->check_unique_name(['id'=>$this->input->post('currency_id')]);
        	if($this->input->post('name') != $original_name) {
		       $is_unique_name =  '|is_unique[currency_value_master.name]';
		    } else {
		       $is_unique_name =  '';
		    }
			$this->form_validation->set_rules('name', 'Name', 'trim|required'.$is_unique_name);
			if ($this->form_validation->run() == TRUE) { 
             	$update_data = array(
             		'id' => $this->input->get('currency_id'),
					'mcc_code' => $this->input->post('mcc_code'),
					'value' => $this->input->post('value'),
					'utf_value' => $this->input->post('utf_value'),
					'name' => $this->input->post('name')
				);
				$this->currency_master_model->update_currency($update_data);
            } 
            //redirect(AUTH_PANEL_URL.'currency_master/Currency_master/add_currency?currency_id='.$this->input->post('currency_id'));
        }
        if($this->input->get('currency_id')) {
         	$data_currency['currency'] = $this->currency_master_model->get_currency(['id'=>$_GET['currency_id']]);
        } 
		$data['page_data'] = $this->load->view('currency_master/currency_list', @$data_currency, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_all_currency_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'mcc_code',
			2 => 'value',
			3 => 'utf_value',
			4 => 'name',
		);

		$query = "SELECT count(id) as total
				  FROM currency_value_master";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT * 
				FROM   currency_value_master where 1=1 ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND mcc_code LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND value LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND utf_value LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
	
		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->mcc_code;
			$nestedData[] = $r->value;
			$nestedData[] = $r->utf_value;
			$nestedData[] = $r->name;
			
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='".AUTH_PANEL_URL."currency_master/Currency_master/add_currency?currency_id=".$r->id."'>Edit</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

}
