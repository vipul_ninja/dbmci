<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Instructor_user extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->model('instructor_user_model');
		$this->load->library('form_validation', 'uploads');
	}

	public function instructor_dashboard(){

		$view_data['page'] = 'instructor_dashboard';
        $data['page_data'] = $this->load->view('instructor_user/instructor_dashboard', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
		}

	public function instructor_profile($id) {
		if($this->input->post()) {

			//$this->form_validation->set_rules('user_id', '', 'required');
			$this->form_validation->set_rules('u_name', 'Beneficiary name', 'required');
			$this->form_validation->set_rules('b_account', 'Beneficiary account', 'required');
			$this->form_validation->set_rules('b_ifsc', 'Bank IFSC', 'required');
			$this->form_validation->set_rules('b_name', 'Bank name', 'required');
			$this->form_validation->set_rules('b_address', 'Bank address', 'required');
			$this->form_validation->set_rules('r_sharing', 'Resource sharing', 'required');



			if ($this->form_validation->run() == FALSE) {

            }

			$update = array('u_name' => $this->input->post('u_name'),'b_account' => $this->input->post('b_account'),'b_ifsc' => $this->input->post('b_ifsc'),'b_name' => $this->input->post('b_name'),'b_address' => $this->input->post('b_address'),'r_sharing' => $this->input->post('r_sharing'));
			//print_r($insert_data); die;

			$this->db->where('user_id',$id);
            $this->db->update('course_instructor_information',$update);

			$data['page_toast'] = 'File added successfully.';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
		}



		$view_data['user_data'] = $this->instructor_user_model->get_user_profile($id);
		$view_data['user_instructor_data'] = $this->instructor_user_model->get_user_instructor_details($id);
		$view_data['page'] = 'all_user';
        $data['page_data'] = $this->load->view('instructor_user/instructor_profile', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}


	public function instructor_account_details($id) {
		if($this->input->post()) {

			//$this->form_validation->set_rules('user_id', '', 'required');
			$this->form_validation->set_rules('u_name', 'Beneficiary name', 'required');
			$this->form_validation->set_rules('b_account', 'Beneficiary account', 'required');
			$this->form_validation->set_rules('b_ifsc', 'Bank IFSC', 'required');
			$this->form_validation->set_rules('b_name', 'Bank name', 'required');
			$this->form_validation->set_rules('b_address', 'Bank address', 'required');
			$this->form_validation->set_rules('r_sharing', 'Resource sharing', 'required');
			$this->form_validation->set_rules('about_me', 'About yourself', 'required');



			if ($this->form_validation->run() == FALSE) {

            }

			$update = array('u_name' => $this->input->post('u_name'),'b_account' => $this->input->post('b_account'),'b_ifsc' => $this->input->post('b_ifsc'),'b_name' => $this->input->post('b_name'),'b_address' => $this->input->post('b_address'),'r_sharing' => $this->input->post('r_sharing'),'about_me' => $this->input->post('about_me'));
			//print_r($insert_data); die;

			$this->db->where('user_id',$id);
            $this->db->update('course_instructor_information',$update);

			$data['page_toast'] = 'File added successfully.';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
		}



		$view_data['user_data'] = $this->instructor_user_model->get_user_profile($id);
		$view_data['user_instructor_data'] = $this->instructor_user_model->get_user_instructor_details($id);
		$view_data['page'] = 'all_user';
        $data['page_data'] = $this->load->view('instructor_user/instructor_account_details', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}


	public function instructor_reviews_list($id) {

		$view_data['user_data'] = $this->instructor_user_model->get_user_profile($id);
		$view_data['user_instructor_data'] = $this->instructor_user_model->get_user_instructor_details($id);
		$view_data['page'] = 'all_user';
        $data['page_data'] = $this->load->view('instructor_user/instructor_reviews_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}

	public function ajax_instructor_user_ratings_list($id) {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'rating',
			3 => 'text',
			4 => 'creation_time'

		);

		$query = "SELECT count(id) as total FROM course_instructor_rating";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT cir.*,u.name,DATE_FORMAT(FROM_UNIXTIME(cir.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time
								FROM course_instructor_rating as  cir
								 join users as u
								on cir.user_id = u.id where instructor_id = $id";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND rating LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND text LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND creation_time LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}



		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$star="";
			if($r->rating > 0 ){
				for($i=0;$i<$r->rating;$i++){
					$star .='<i class =  "fa fa-star text-danger"></i>';
				}
				if($i<5){$j = 5 - $i;for($i=0;$i<$j;$i++){$star .='<i class =  "fa fa-star-o text-danger"></i>';}}
			}

			$nestedData[] = $r->id;
			$nestedData[] = $r->name;
			$nestedData[] = $star;
			$nestedData[] = substr($r->text, 0, 30);
			$nestedData[] = $r->creation_time;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "instructor_user/edit_instructor_user_rating/" . $r->id . "'>Edit</a>";
			$action .= " <a class='btn-sm btn btn-danger btn-xs bold' href='" . AUTH_PANEL_URL . "instructor_user/delete_instructor_review/" . $r->id ."?instructor_id=".$r->instructor_id. "'>Delete</a>";
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function edit_instructor_user_rating($id){
		$view_data['page']  = 'edit_instructor_rating';
		$data['page_title'] = "Edit Instructor Rating";
		if($this->input->post('update_instructor_review')) {
			/* handle submission */
			$this->update_instructor_user_review();
		}
		$view_data['instructor_rating_detail'] = $this->instructor_user_model->get_instructor_rating_details_by_id($id);
		$data['page_data'] = $this->load->view('instructor_user/edit_instructor_user_details', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	private function update_instructor_user_review(){
		if($this->input->post()) {
			$this->form_validation->set_rules('rating', 'Rating', 'required');
			$this->form_validation->set_rules('text', 'Review', 'required');
			$review_id = $this->input->post('id');
			$user_id = $this->input->post('instructor_id');
			if ($this->form_validation->run() == FALSE) {
             $error = validation_errors();
            }else{
				$update = array(
					'rating' => $this->input->post('rating'),
					'text' => $this->input->post('text'),

				);
				$this->db->where('id',$review_id);
				$this->db->update('course_instructor_rating',$update);
				page_alert_box('success','Action performed','Review updated successfully');
			}
		}

	}

	public function delete_instructor_review($id) {
		$user_id = $_GET['instructor_id'];
		$status = $this->instructor_user_model->delete_review($id);
		page_alert_box('success','Action performed','Review deleted successfully');
		if($status) {
			redirect('auth_panel/instructor_user/instructor_reviews_list/'.$user_id);
		}
	}

	public function instructor_transaction_list($id) {

		$view_data['user_data'] = $this->instructor_user_model->get_user_profile($id);
		$view_data['page'] = 'all_instructor_transactions';
        $data['page_data'] = $this->load->view('instructor_user/instructor_transaction_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}

	public function ajax_instructor_transactions_list($id) {

		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'course_name',
			2 => 'instructor_share',
			3 => 'creation_time'

		);

		$query = "SELECT count(id) as total
								FROM course_transaction_record as ctr where 1 = 1 AND ctr.instructor_id = $id AND ctr.transaction_status = 1 AND ctr.instructor_share != 0";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctr.* ,cm.title as course_name,DATE_FORMAT(FROM_UNIXTIME(SUBSTR(ctr.creation_time,1,10)), '%d-%m-%Y %h:%i:%s') as creation_time
								FROM course_transaction_record as ctr
								left join course_master as cm on cm.id=ctr.course_id
								where 1 = 1 AND ctr.instructor_id = $id AND ctr.transaction_status = 1 AND ctr.instructor_share != 0";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND ctr.id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND cm.title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND ctr.instructor_share LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND ctr.creation_time LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}


		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		//echo $this->db->last_query(); die;
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->course_name;
			$nestedData[] = $r->instructor_share;
			$nestedData[] = $r->creation_time;
			//$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "instructor_user/edit_instructor_user_rating/" . $r->id . "'>Edit</a>";

			//$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function ajax_single_instructor_list($ins_id){

    		// storing  request (ie, get/post) global array to a variable
    		$requestData = $_REQUEST;

    		$columns = array(
    			// datatable column index  => database column name
    			0 => 'id',
    			1 => 'amount',
    			2 => 'message',
    			3 => 'creation_time',
    		);

    		$query = "SELECT count(id) as total
    								FROM instructor_pay_record where instructor_id = $ins_id
    								";
    		$query = $this->db->query($query);
    		$query = $query->row_array();
    		$totalData = (count($query) > 0) ? $query['total'] : 0;
    		$totalFiltered = $totalData;

    		$sql = "SELECT * , DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y %h:%i:%s') as on_time
    								FROM instructor_pay_record where instructor_id = $ins_id ";

    		// getting records as per search parameters
    		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
    			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
    		}
    		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
    			$sql.=" AND amount LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
    		}
    		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
    			$sql.=" AND message LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
    		}
    		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
    			$sql.=" having on_time LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
    		}

    		$query = $this->db->query($sql)->result();

    		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

    		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

    		$result = $this->db->query($sql)->result();
    		$data = array();
    		foreach ($result as $r) {  // preparing an array
    			$nestedData = array();
    			$nestedData[] = $r->id;
    			$nestedData[] = $r->amount;
    			$nestedData[] = $r->message;
    			$nestedData[] = $r->on_time;
    			$data[] = $nestedData;
    		}

    		$json_data = array(
    			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
    			"recordsTotal" => intval($totalData), // total number of records
    			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
    			"data" => $data   // total data array
    		);

    		echo json_encode($json_data);  // send data as json format
  }


}
