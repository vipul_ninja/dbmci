<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class Go_live extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->library('form_validation');
		
		modules::run('auth_panel/auth_panel_ini/auth_ini');
	}


	public function index() {
		//$user_data = $this->session->userdata('active_user_data');
		$view_data['page'] = 'go_live';
		//$view_data['roles'] = $user_data->roles;
		//$view_data['student']    = $this->backend_user->getStudentRecords();
		//$view_data['student_join'] = $this->backend_user->getJoinedStudent();
		
		$data['page_title'] = "go live";
		$data['page_data'] = $this->load->view('go_live/golive', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

}

