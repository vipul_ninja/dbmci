<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu_master extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		$this->load->helper('cookie');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation');
				/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
		$this->load->library('form_validation', 'uploads');
		$this->load->library('upload');
		/* do not remove helper and grocey_crud
		* It will put you in danger
		*/
	}

	public function amazon_s3_upload($name,$aws_path) {

		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).str_replace([':', ' ', '/', '*','#','@','%'],"_",$_FILES["file"]['name']),
							'SourceFile' => $_FILES["file"]['tmp_name'],
							'ContentType' => 'image',
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array('param1' => 'value 1','param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	}


	public function menu_master_management(){
		//print_r($_POST);
		if($this->input->post()) { 
			// print_r($this->input->post()); di ;
			// print_r($_FILES['image']);
			//  die;
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('name_2', 'Name_2', 'trim|required');
			//$this->form_validation->set_rules('exampleInputFile', 'Image', 'required');
			if ($this->form_validation->run() == FALSE) {

			}else{
				$insert_data = array('name' => ucfirst($this->input->post('name')));
				$insert_data['name_2'] =$this->input->post('name_2');
				$insert_data['app_view'] =$this->input->post('app_view');
				$insert_data['web_view'] =$this->input->post('web_view');
				$insert_data['position'] =$this->input->post('position');
				$insert_data['visibility'] =$this->input->post('visibility');
				$insert_data['child_type'] =$this->input->post('child_type');
				if($_FILES && $_FILES['image']['name']){		
					$file  = $this->amazon_s3_upload($_FILES['image'],'course_file_meta');
				}else{
					$file='Not Selected';
				}						
				$insert_data['image'] =$file;
				
				$this->db->insert('menu_master',$insert_data);
				page_alert_box('success','Menu Added','Menu has been added successfully');
			}
		}
		$data['page_data'] = $this->load->view('menu_master/menu/add_menu', array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function disable_stream($stream_id){
		$this->db->where('id',$stream_id);
	  $this->db->update('course_stream_name_master',array('status'=>1));
		page_alert_box('success','Stream Disabled','Stream has been disabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/stream_management');
	}

	public function enable_stream($stream_id){
		$this->db->where('id',$stream_id);
	   $this->db->update('course_stream_name_master',array('status'=>0));
		page_alert_box('success','Stream Enabled','Stream has been enabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/stream_management');
	}

	public function enable_study($stream_id){
		$this->db->where('id',$stream_id);
	    $this->db->update('course_stream_name_master',array('status'=>0));
		page_alert_box('success','Stream Enabled','Study has been enabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/add_study_sub_type/'.$this->input->get('parent_id'));
	}

	public function disable_study($stream_id){
		$this->db->where('id',$stream_id);
		$this->db->update('course_stream_name_master',array('status'=>1));
		page_alert_box('success','Sub Stream Disabled','Study has been disabled successfully');
		redirect(AUTH_PANEL_URL.'course_product/stream/add_study_sub_type/'.$this->input->get('parent_id'));
	}
	public function ajax_menu_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2=> 'name_2',
			3 => 'view_type',
			4 => 'app-view',
			5 => 'web-view',
			5 => 'image'
		);
		$query = "SELECT count(id) as total FROM menu_master";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "select * from menu_master ";

		
		
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
         
		$result = $this->db->query($sql)->result();
		//print_r($result);
		$data = array();
		$start  = 0 ;
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = ++$start;
			$nestedData[] = $r->name;
			$nestedData[] = $r->name_2;
			$nestedData[] = $r->view_type;
			$nestedData[] = $r->app_view;
			$nestedData[] = $r->web_view;
			
			$nestedData[] = '<img width="30" height="30" src="'.$r->image.'">';
			$nestedData[] = $r->child_type;
			
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "menu_master/menu_master/edit_menu/" . $r->id . "'><i class='fa fa-pencil'></i>&nbsp Edit</a>&nbsp; 
			&nbsp; <a onClick=\"return confirm('Are you sure you want to delete it? You may lose respective records. Action can not be undo.')\" class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "menu_master/menu_master/delete_menu/" . $r->id . "'><i class='fa fa-times'></i>&nbsp Delete</a>
			&nbsp; ";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function edit_menu($id)
	{
      
	   if($this->input->post()){				
		
		   $this->form_validation->set_rules('name', 'Stream Name', 'trim|required');
		   $this->form_validation->set_rules('name_2', 'Stream (hindi) Name', 'trim|required');
		   
		   if($_FILES && $_FILES['image']['name']){					
				$file = $this->amazon_s3_upload($_FILES['image'],'course_file_meta');	
			}
		   if ($this->form_validation->run() == TRUE)
			 {
				$update_data = array(
							   'name' => ucfirst($this->input->post('name')),
							   'name_2' => ucfirst($this->input->post('name_2')),
							    'app_view' => $this->input->post('app_view'),
				                'web_view' => $this->input->post('web_view'),
				                 'position' => $this->input->post('position'),
				                'visibility'=> $this->input->post('visibility'),
				                'child_type' => $this->input->post('child_type')

			   );
				if($_FILES && $_FILES['image']['name']){	 
				$update_data['image']=$file;             
			}
			
			

			   $this->db->where('id',$id);
			   $this->db->update('menu_master',$update_data);
			   page_alert_box('success','Menu Updated','Menu has been updated successfully');
			   redirect(AUTH_PANEL_URL.'menu_master/menu_master/menu_master_management');
	 }


		
	   }
	   $view_data['page']  = 'edit_stream';
	   $data['page_title'] = "Stream edit";
	   $this->db->where('id',$id);
	   $view_data['stream'] = $this->db->get('menu_master')->row_array();
	   $data['page_data'] = $this->load->view('menu_master/menu/edit_menu', $view_data, TRUE);
	   echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
}

	public function save_position_stream(){
		$ids = $_POST['ids'];
		$counter = 1;
		foreach($ids as $id){
			$this->db->where('id',$id);
			$array = array('position'=>$counter);
			$this->db->update('menu_master',$array);
			$counter++;
		}
		echo json_encode(array('status'=>true,'message'=>'position saved'));
		die;
	}
	public function delete_menu($stream_id){
		
		$this->db->where('id',$stream_id);
	  	$this->db->delete('menu_master');
		page_alert_box('success','Menu Deleted','Menu has been deleted successfully');
		redirect(AUTH_PANEL_URL.'menu_master/menu_master/menu_master_management');
	}




}
