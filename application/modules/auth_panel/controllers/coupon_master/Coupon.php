<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->model('Coupon_model');
		$this->load->library('form_validation');
	}

	public function add_coupon(){
		if($this->input->post()) { //print_r($_POST); die;

			$this->form_validation->set_rules('couponname', 'Coupon Name', 'required|is_unique[course_coupon_master.coupon_tilte]');
			$this->form_validation->set_rules('validto', 'Valid To Date', 'required');
			$this->form_validation->set_rules('validfrom', 'Valid From Date', 'required');
			$this->form_validation->set_rules('coupontype', 'Coupon Type', 'required');
			$this->form_validation->set_rules('coupovalue', 'Coupon Value', 'required|is_natural_no_zero');
			

			if ($this->form_validation->run() == FALSE) {
             
            } 
			else { 								
				$insert_data = array('coupon_tilte' => $this->input->post('couponname'),
									'end' => $this->input->post('validto'),
									'start' => $this->input->post('validfrom'),
									'coupon_type' => $this->input->post('coupontype'),
									'coupon_value' => $this->input->post('coupovalue'),
									'coupon_for'=> $this->input->post('coupon_for')
									);
				
				$this->db->insert('course_coupon_master',$insert_data);
				$data['page_toast'] = 'File added successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }

		}
		$data['page_data'] = $this->load->view('coupon_master/add_coupon', array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	
	public function ajax_coupon_list() {

		
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'coupon_tilte',
			2 => 'start',
			3 => 'end',
			4 => 'coupon_type',
			5 => 'coupon_value',
			6 => 'state',
			
		);

		$query = "SELECT count(id) as total FROM course_coupon_master where state = 0";
									
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT id,coupon_tilte,start,end,coupon_type,coupon_value,state FROM  course_coupon_master where state = 0";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND coupon_tilte LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND start LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND end LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND coupon_type LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}	

		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" AND coupon_value LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}

		if (isset($requestData['columns'][6]['search']['value']) && $requestData['columns'][6]['search']['value'] != ""  ) {  //salary
			$sql.=" AND state = ". $requestData['columns'][6]['search']['value'];
		}

		
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
	
		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->coupon_tilte;
			$nestedData[] = $r->start;
			$nestedData[] = $r->end;
			$nestedData[] = ($r->coupon_type == "2")?"In Percentage %":"In Value";
			$nestedData[] =$r->coupon_value;
			$nestedData[] = ($r->state == 0)?"Active":"Deactive";
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "coupon_master/coupon/edit_coupon/" . $r->id . "'>Edit</a>&nbsp;"
				. "<a class='btn-xs bold btn btn-danger' onclick=\"return confirm('Are you sure you want to delete?')\" href='" . AUTH_PANEL_URL . "coupon_master/coupon/delete_coupon/" . $r->id . "'>Delete</a>&nbsp;";					
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they 			first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}
	
	
		public function edit_coupon($id) {
		
		if($this->input->post()) { 
			$this->form_validation->set_rules('couponname', 'Coupon Name', 'required|edit_unique[course_coupon_master.coupon_tilte.'.$id.']');
			$this->form_validation->set_rules('validto', 'Valid To Date', 'required');
			$this->form_validation->set_rules('validfrom', 'Valid From Date', 'required');
			$this->form_validation->set_rules('coupontype', 'Coupon Type', 'required');
			$this->form_validation->set_rules('coupovalue', 'Coupon Value', 'required|is_natural_no_zero');
			

			if ($this->form_validation->run() == FALSE) {
             
            } 
			else { 								
			
			$insert_data = array('coupon_tilte' => $this->input->post('couponname'),
								'end' => $this->input->post('validto'),
								 'start' => $this->input->post('validfrom'),
								 'coupon_type' => $this->input->post('coupontype'),
								 'coupon_value' => $this->input->post('coupovalue'),
								 'coupon_for'=> $this->input->post('coupon_for')
								 );
				$update_coupon = $this->Coupon_model->update_coupon($id,$insert_data);
				if($update_coupon){
					$data['page_toast'] = 'Information Updated  successfully.';
					$data['page_toast_type'] = 'success';
					$data['page_toast_title'] = 'Action performed.';
				}
            }
		}
			
			
		$view_data['page']  = '';
		$data['page_title'] = "Coupon edit";
		$view_data['coupon'] = $this->Coupon_model->get_coupon_by_id($id);

		$view_data['added_user']  = $this->db->query("SELECT u.* 
										  from users as u 
										  Join coupon_user_relation as cur on u.id = cur.user_id
										  where cur.coupon_id = $id 
										  ")->result_array();


		$data['page_data'] = $this->load->view('coupon_master/edit_coupon', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	
	
	public function delete_coupon($id) {
		$delete_user = $this->Coupon_model->delete_coupon($id);
		if ($update_data == true) {
			$this->session->set_flashdata('success_message', 'Coupon has been Deleted succssfully');
		} else {
			$this->session->set_flashdata('error_message', 'Coupon not Deleted');
		}
		redirect(AUTH_PANEL_URL . 'coupon_master/coupon/add_coupon');
	}
	
	public function user_list($str=""){
		$str = urlencode($str);
		if($str !=""){
			$result = $this->db->query("select * from users where name like '$str%' or mobile   like  '$str%' or dams_tokken  like '$str%'  or email like  '$str%'  limit 0,5")->result_array();
			echo json_encode($result);			
		}
	}	

	public function add_user(){
		$u_id = $this->input->get('user_id');
		$c_id = $this->input->get('coupon_id');

		$this->db->where('user_id',$u_id);
		$this->db->where('coupon_id',$c_id);
		$exist =  $this->db->get('coupon_user_relation')->result_array();
		if(count($exist) == 0 ){
			$insert_data = array(
			"user_id"=>$u_id,
			"coupon_id"=>$c_id,
			"added_by"=> $this->session->userdata('active_user_data')->id,
			"created_on"=>milliseconds()
			);
			$this->db->insert('coupon_user_relation',$insert_data);	
		}
		
		redirect(AUTH_PANEL_URL . 'coupon_master/coupon/edit_coupon/'.$c_id);
	}	

	public function remove_user(){
		$u_id = $this->input->get('user_id');
		$c_id = $this->input->get('coupon_id');
		$this->db->where('user_id',$u_id);
		$this->db->where('coupon_id',$c_id);
		$this->db->delete('coupon_user_relation');
		redirect(AUTH_PANEL_URL . 'coupon_master/coupon/edit_coupon/'.$c_id);
	}
}



