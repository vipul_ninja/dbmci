<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
 use Aws\CloudFront\CloudFrontClient;
class Fan_wall extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->library('form_validation');
		
		modules::run('auth_panel/auth_panel_ini/auth_ini');
	}

	public function all_post() {
		($this->input->get('page') == 'report_abuse') ? $view_data['page'] = 'fan_wall_abuse_post': $view_data['page'] = 'all_fanwall';
		
		//$data['page_data'] = $this->load->view('fan_wall/index', $view_data, TRUE);
		$data['page_data'] = $this->load->view('fan_wall/live_feed', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_all_post() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'user_id',
			2 => 'post_type',
			3 => 'likes',
			4 => "comments",
			5 => "share",
			6 => "report_abuse"
		);

		$query = "SELECT count(id) as total
									FROM post_counter
									";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT  pc.id as id , `user_id`, `post_type`, `likes`, `comments`, `share`, `report_abuse`  , u.name as name , u.profile_picture as profile_picture
		FROM  post_counter as pc  
		join users as u
		On u.id = pc.user_id
		where 1=1";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND email LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND mobile LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name."<img class='col-md-3' src='".$r->profile_picture."' />";
			$nestedData[] = $r->post_type;
			$nestedData[] = $r->likes;
			$nestedData[] = $r->comments;
			$nestedData[] = $r->share;
			$nestedData[] = $r->report_abuse;
			
			$nestedData[] = "<a class='btn-sm btn btn-info hide' href='" . AUTH_PANEL_URL . "web_user/user_profile/" . $r->id . "'>View</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}
	
	public function get_post_report_abused_by_id($id) {
		
    	$query = "SELECT upr.id , u.name , u.profile_picture  , CASE
		    WHEN uprr.reason IS NULL THEN ' '
		    ELSE uprr.reason
		  END AS reason, upr.comment
		from user_post_report as upr join users as u on upr.user_id = u.id 
		left join user_post_report_reasons as uprr on upr.reason_id = uprr.id 
		where upr.post_id = $id order by upr.creation_time desc";    	
		$data = $this->db->query($query)->result_array();
		//echo $this->db->last_query(); 
		echo $report_abuse_data = json_encode($data);
		

    }


	public function fan_wall_banner() {
		$link_type = $this->input->post('link_type');
		if($_POST) {
				$image_error = false;
				$this->form_validation->set_rules('banner_title', 'Banner Title', 'required');
				$this->form_validation->set_rules('text', 'Text', 'required');
				if($link_type==0)
				 {
		
				$this->form_validation->set_rules('web_link', 'Web link', 'required');
				 }
				 if($link_type==1)
				 {
				
				$this->form_validation->set_rules('course_link', 'course link', 'required');
				 }
				 if($link_type==2)
				 {
				
				$this->form_validation->set_rules('study_type', 'study type', 'required');
				 }
				$this->form_validation->set_rules('from', 'From date', 'required');
				$this->form_validation->set_rules('to', 'To date', 'required');
				$this->form_validation->set_rules('priority', 'Priority', 'required');
				$this->form_validation->set_rules('button_text', 'button text', 'required');
				if(strtotime($_POST['from']) > strtotime($_POST['to'])) {
					$this->form_validation->set_rules('to', 'To','rule',array('rule' => 'To date should be greater than From date.'));
				}
				if (empty($_FILES['file']['name'])){
				    $this->form_validation->set_rules('file', 'File', 'required');
				}else{
					$imagedetails = getimagesize($_FILES['file']['tmp_name']);
					$width = $imagedetails[0];
					$height = $imagedetails[1];
					if($width != "640" && $height != "360"){
						$data['page_toast'] = 'Image size is not valid.';
						$data['page_toast_type'] = 'error';
						$data['page_toast_title'] = 'Error';
						$image_error = true;
					}
				}
				if ($this->form_validation->run() == true && $image_error == false){    
				require_once(FCPATH.'aws/aws-autoloader.php');
				$allowedExts = array("png", "gif", "jpeg", "jpg");
				$c = explode(".", $_FILES["file"]["name"]);
				
				if ((($_FILES["file"]["type"] == "image/gif")
				|| ($_FILES["file"]["type"] == "image/jpeg")
				|| ($_FILES["file"]["type"] == "image/jpg")
				|| ($_FILES["file"]["type"] == "image/png")))
					 {
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => BANNER_IMAGES_S3.'/'.rand(0,7896756).$_FILES["file"]["name"], 
							'SourceFile' => $_FILES["file"]["tmp_name"], 
							'ContentType' => 'image', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));

						$data=$result->toArray();
						if($link_type==0)
						{
					     $_POST['link_type']=1;
						}
						if($link_type==1)
						{
					     $_POST['link_type']=2;
						}
						if($link_type==2)
						{
					     $_POST['link_type']=3;
						}
						$this->db->insert( "fan_wall_banner",array("priority"=>$_POST['priority'],"banner_title"=>$_POST['banner_title'],"image_link"=>$data['ObjectURL'],"web_link"=>$_POST['web_link'],"course_link"=>$_POST['course_link'],"study_type"=>$_POST['study_type'],"type_link"=>$_POST['link_type'],"button_text"=>$_POST['button_text'],"text"=>$_POST['text'],"from_date"=>strtotime($_POST['from']) * 1000,"to_date"=>strtotime($_POST['to']) * 1000));	
						//creating backlog
						backend_log_genration($this->session->userdata('active_backend_user_id'),'Added new advertisement '.$_POST['banner_title'],'ADVERTISEMENT');	

						$data['page_toast'] = 'Banner added successfully.';
						$data['page_toast_type'] = 'success';
						$data['page_toast_title'] = 'Action performed.';
					}else {
						$this->session->set_flashdata('message', 'File type not correct please choose correct file type.');
						redirect('auth_panel/fan_wall/fan_wall_banner');
					} 
				} 
			}
			$view_data['page']  = 'fan_wall_banner';
			$data['page_data'] = $this->load->view('fan_wall/fan_wall_banner', $view_data, TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	


	public function ajax_all_fan_wall_banner()
	{
		$requestData = $_REQUEST;
		$columns = array(
			//datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'text',
			3 => 'hit_count',
			4 => 'image_link',
			5 => 'priority',
			6 => 'status'
		);
		$query = "SELECT count(id) as total	FROM fan_wall_banner";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT  * 
		FROM  fan_wall_banner 
		where 1=1";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		
		$query = $this->db->query($sql)->result();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  //preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = substr($r->banner_title,0,25).'...';
			$nestedData[] = substr($r->text,0,25).'...';
			$nestedData[] = $r->hit_count;
			$nestedData[] = "<img width='160' height='90'  src='".$r->image_link."' />";
			$nestedData[] =$r->priority;
		    if($r->status==1){
				$nestedData[]='Deactive';
				}else{
				$nestedData[]='Active';
				};
			$nestedData[] = "<a class='btn-xs bold  btn btn-success' href='" . AUTH_PANEL_URL . "fan_wall/edit_banner/" . $r->id . "'>Edit</a><a class='btn-xs bold margin-left btn btn-danger' href='" . AUTH_PANEL_URL . "fan_wall/delete_banner/" . $r->id . "' onclick=\" return confirm('Are you really want to delete this banner? ') \">Delete</a><a class='btn-xs bold margin-left btn btn-info' href='".$r->image_link."' download='download'>Download</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function delete_banner($id) {
		$this->db->where('id',$id);
		$this->db->delete('fan_wall_banner');

		$view_data['page']  = 'fan_wall_banner';
		$data['page_toast'] = 'Banner deleted successfully.';
		$data['page_toast_type'] = 'success';
		$data['page_toast_title'] = 'Action performed.';
		$data['page_data'] = $this->load->view('fan_wall/fan_wall_banner', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function edit_banner($id) {
		//print_r($_POST);
		$this->db->select('type_link');
		$this->db->from('fan_wall_banner');
		$this->db->where('id',$id);
		$linktypes= $this->db->get()->row()->type_link;
		
		$course_link = $this->input->post('course_link');
		$web_link = $this->input->post('web_link');
		$study_link = $this->input->post('study_type');
		if($_POST) {
			
			$image_error = false;
			$this->form_validation->set_rules('banner_title', 'Banner Title', 'required');
			$this->form_validation->set_rules('text', 'Text', 'required');
			if($linktypes==1 && empty($web_link))
			{
			$this->form_validation->set_rules('web_link', 'web link', 'required');
			}
			if($linktypes==2 && empty($course_link))
			{
			$this->form_validation->set_rules('course_link', 'course link', 'required');
			}
			if($linktypes==3 && empty($study_link))
			{
			$this->form_validation->set_rules('study_type', 'study link', 'required');
			}
			
			$this->form_validation->set_rules('from', 'From date', 'required');
			$this->form_validation->set_rules('to', 'To date', 'required');
			$this->form_validation->set_rules('priority', 'Priority', 'required');
			$this->form_validation->set_rules('button_text', 'button', 'required');
			if(strtotime($_POST['from']) > strtotime($_POST['to'])) {
			$this->form_validation->set_rules('to', 'To','rule',array('rule' => 'To date should be greater than From date.'));
			}

			if ($_FILES['file']['name']){
					$imagedetails = getimagesize($_FILES['file']['tmp_name']);
					$width = $imagedetails[0];
					$height = $imagedetails[1];
					if($width != "640" && $height != "360"){
						$data['page_toast'] = 'Image size is not valid.';
						$data['page_toast_type'] = 'error';
						$data['page_toast_title'] = 'Error';
						$image_error = true;
					}
				}
			if ($this->form_validation->run() == true && $image_error == false){
				

					$file_upload = $_POST['file_upload'];

					if ($_FILES['file']['error'] == 0)
						{
						    require_once(FCPATH.'aws/aws-autoloader.php');
							$allowedExts = array("png", "gif", "jpeg", "jpg");
							$c = explode(".", $_FILES["file"]["name"]);
				
							if ((($_FILES["file"]["type"] == "image/gif")
							|| ($_FILES["file"]["type"] == "image/jpeg")
							|| ($_FILES["file"]["type"] == "image/jpg")
							|| ($_FILES["file"]["type"] == "image/png")))
					 			{
									$s3Client = new S3Client([  
									'version'     => 'latest',
									'region'      => 'ap-south-1',
									'credentials' => [        
									'key'    => AMS_S3_KEY,
									'secret' => AMS_SECRET,  
									],
									]);
									$result = $s3Client->putObject(array(   
										'Bucket' => AMS_BUCKET_NAME,
										'Key' => BANNER_IMAGES_S3.'/'.rand(0,7896756).$_FILES["file"]["name"], 
										'SourceFile' => $_FILES["file"]["tmp_name"], 
										'ContentType' => 'image', 
										'ACL' => 'public-read',
										'StorageClass' => 'REDUCED_REDUNDANCY', 
										'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
									));

						$data=$result->toArray();
						$file_upload = $data['ObjectURL'];
						}

				}
			
			if(!isset($_POST['web_link']))
			{
				$_POST['web_link']='';
			}
			if(!isset($_POST['course_link']))
			{
				$_POST['course_link']='';
			}
			if(!isset($_POST['study_type']))
			{
				$_POST['study_type']='';
			}
			$this->db->where('id',$id)
			->update('fan_wall_banner',array("status"=>$_POST['status'],"priority"=>$_POST['priority'],"banner_title"=>$_POST['banner_title'],"image_link"=>$file_upload , "web_link"=>$_POST['web_link'],"course_link"=>$_POST['course_link'],"study_type"=>$_POST['study_type'],"button_text"=>$_POST['button_text'],"text"=>$_POST['text'],"from_date"=>strtotime($_POST['from']) * 1000,"to_date"=>strtotime($_POST['to']) * 1000));
				
			$data['page_toast'] = 'Banner Updated successfully.';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';

			}
			
		}
		$view_data['banner_detail']  = $this->db->where('id',$id)->get('fan_wall_banner')->row_array();
		$view_data['page']  = 'fan_wall_banner';
		$data['page_data'] = $this->load->view('fan_wall/edit_fan_wall_banner', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function fan_wall_abuse_post() {
		redirect('auth_panel/fan_wall/all_post?page=report_abuse');
		/*$view_data['page']  = 'fan_wall_abuse_post';
		$data['page_data'] = $this->load->view('fan_wall/fan_wall_abuse_post', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);*/
	}


	public function ajax_all_abuse_post() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'user_id',
			2 => 'post_type',
			3 => 'likes',
			4 => "comments",
			5 => "share",
			6 => 'report_abuse'
		);

		$query = "SELECT count(id) as total
									FROM post_counter
									";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT  pc.id as id , `user_id`, `post_type`, `likes`, `comments`, `share`, `report_abuse`  , u.name as name , u.profile_picture as profile_picture
		FROM  post_counter as pc  
		join users as u
		On u.id = pc.user_id
		where pc.report_abuse >= 1
		AND  1=1";


		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND email LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND mobile LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name."<img class='col-md-3' src='".$r->profile_picture."' />";
			$nestedData[] = $r->post_type;
			$nestedData[] = $r->likes;
			$nestedData[] = $r->comments;
			$nestedData[] = $r->share;
			$nestedData[] = $r->report_abuse;
			
			
			$nestedData[] = "<a class='btn-sm btn btn-info hide' href='" . AUTH_PANEL_URL . "web_user/user_profile/" . $r->id . "'>View</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function ajax_get_feed() {

		$where = "";
		

		if($_POST['tag_id'] != ""){ $where .= " And pc.post_tag = ".$_POST['tag_id'] ; }
		if($_POST['page'] == "report_abuse"){ $where .= " And pc.report_abuse > 0 "; }
		if($_POST['user_id'] != ""){ $where .= " And pc.user_id =  ".$_POST['user_id']; }
		$joins_search_table =  "";
		if($_POST['search_text'] != ""){ 
			$where .= " And ( post_tags.text like '%".$_POST['search_text']."%' or u.name like '%".$_POST['search_text']."%' or uptt.text LIKE '%".$_POST['search_text']."%' or uptm.question LIKE '%".$_POST['search_text']."%') "  ;
			$joins_search_table = " LEFT JOIN user_post_type_text as uptt on pc.id = uptt.post_id LEFT JOIN user_post_type_mcq  as uptm on pc.id = uptm.post_id ";
		}
        if($_POST['page'] == "report_abuse" && $_POST['abused_filter'] == "most_abused"){
        	$order_by = "ORDER BY pc.report_abuse desc";
        }else{
        	$order_by = "ORDER BY pc.creation_time DESC";
        }
        $initial = 0 ;
        $limit = " limit $initial ,10";

        if($_POST['page'] == "report_abuse" && $_POST['abused_filter'] == "most_abused" && $_POST['last_abused_segment'] !=""){

        	$initial = ($_POST['last_abused_segment']+10);
        	$limit = " limit $initial ,10";
        }

        if($_POST['abused_filter'] !== "most_abused"){
        	if($_POST['last_id'] != ""){ $where .= " And pc.id < ".$_POST['last_id'] ; }
        }


		$query = $this->db->query(" SELECT pc.id 
									FROM post_counter AS pc
									JOIN user_registration_data AS urd ON pc.user_id = urd.user_id
									LEFT JOIN post_tags ON pc.post_tag = post_tags.id
									$joins_search_table
									JOIN users AS u ON u.id = pc.user_id
									WHERE pc.status = 0 
									and	pc.is_shared = 0
									$where 						
									$order_by
									$limit
									");
		$result  =  $query->result_array();

		$post = array();
		foreach($result as $re) {
			$option = array(
			   'is_json' => true,
			   'data' => array("user_id"=>"0","post_id"=>$re['id'])
			  );

			  $ch = curl_init(site_url('data_model/fanwall/fan_wall/get_single_post_data_for_user'));
			  curl_setopt($ch, CURLOPT_POST, 1);
			  curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
			  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			  $result = curl_exec($ch);
			  curl_close($ch);
			  $post[] = json_decode($result,true);

			}
		if(!$post){
			echo json_encode(array("status"=>false,"data"=>$post,'last_chunk'=>$initial));die;
		}
		
		echo json_encode(array("status"=>true,"data"=>$post,'last_chunk'=>$initial));die;
		
	}


	function ajax_get_comment_feed() {
			$option = array(
			   'is_json' => true,
			   'data' => array("user_id"=>"0","post_id"=>$_POST['post_id'],"last_comment_id"=>$_POST['last_comment_id'])
			 );

			  $ch = curl_init(site_url('data_model/user/post_comment/get_post_comment'));
			  curl_setopt($ch, CURLOPT_POST, 1);
			  curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
			  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			  $result = curl_exec($ch);
			  curl_close($ch);
			  $post = json_decode($result,true);
			  echo json_encode(array("status"=>true,"data"=>$post));die;
	}


	public function ajax_delete_comment_feed() {
		$option = array(
			   'is_json' => true,
			   'data' => array("user_id"=>$_POST['user_id'],"post_id"=>$_POST['post_id'],"id"=>$_POST['id'])
			 );

			  $ch = curl_init(site_url('data_model/user/post_comment/delete_comment'));
			  curl_setopt($ch, CURLOPT_POST, 1);
			  curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
			  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			  $result = curl_exec($ch);
			  curl_close($ch);
			  $post = json_decode($result,true);
			  echo json_encode(array("status"=>true,"data"=>$post));die;
	}


	public function ajax_edit_comment_feed() {
		$option = array(
			   'is_json' => true,
			   'data' => array("user_id"=>$_POST['user_id'],"post_id"=>$_POST['post_id'],"id"=>$_POST['id'],"comment"=>$_POST['text'])
			 );

			  $ch = curl_init(site_url('data_model/user/post_comment/update_comment'));
			  curl_setopt($ch, CURLOPT_POST, 1);
			  curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
			  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			  $result = curl_exec($ch);
			  curl_close($ch);
			  $post = json_decode($result,true);
			  echo json_encode(array("status"=>true,"data"=>$post));die;
	}


	public function test_config() {
		return [
		's3' => [
			'key' => 'AKIAJOX5DIUSM6P6AIYA',
			'secret' => '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',
			'bucket' => 'dams-apps-production'
		],
		'cloudfront' => [
			'url' => 'https://d3qn7x9z1pnfz8.cloudfront.net'
		]
	];
	}

	public function test() {
		require_once(FCPATH.'aws/aws-autoloader.php');
		$config = $this->test_config();
		$s3 = S3Client::factory([
		'version'     => 'latest',
        'region'      => 'us-east-2',
		'key' => $config['s3']['key'],
		'secret' => $config['s3']['secret']
 		]);
 		$cloudfront = CloudFrontClient::factory([
		'version'     => 'latest',
        'region'      => 'us-east-2',
		'private_key' => FCPATH.'pk-APKAINLPC4YDYYIC6ZOA.pem',
		'key_pair_id' => 'APKAINLPC4YDYYIC6ZOA'
 		]);
 		$object = '202781SampleVideo_1280x720_1mb.mp4';
		$expiry = new DateTime('+100 minute');
	
		$url = $cloudfront->getSignedUrl([
				'url' => "{$config['cloudfront']['url']}/{$object}",
				'expires' => $expiry->getTimestamp(),
				'private_key' => 'pk-APKAINLPC4YDYYIC6ZOA.pem',
				'key_pair_id' => 'APKAINLPC4YDYYIC6ZOA'
		]);

	echo $url;die;
	}


	public function ajax_get_user_like_list() {
		$option = array(
		   'is_json' => true,
		   'data' => array("post_id"=>$_POST['post_id'],"last_id"=>$_POST['last_id'])
		 );

		  $ch = curl_init(site_url('data_model/user/post_like/get_post_like_users'));
		  curl_setopt($ch, CURLOPT_POST, 1);
		  curl_setopt($ch, CURLOPT_POSTFIELDS, $option['data']);
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		  $result = curl_exec($ch);
		  curl_close($ch);
		  $post = json_decode($result,true);
		  echo json_encode(array("status"=>true,"data"=>$post));die;
	}

}	
