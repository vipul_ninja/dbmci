<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create_menu extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		
	}

	public function index() {
		$user_data = $this->session->userdata('active_user_data');
		$results = $this->db->select('*')
				 ->from('web_menu')
				 ->get()
				 ->row_array();
		$view_data['menu_json'] = $results['menu_json'];
		$view_data['page'] = 'create_menu';
		$data['page_data']  = $this->load->view('web_handler/create_menu' ,$view_data, TRUE);
		$data['page_title'] = "Create Menu";
		echo modules::run('auth_panel/template/call_default_template', $data);
	}

	public function save_menu(){
		$data['menu_json'] = $this->input->post('menu_json');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['status'] = 1;
		$menu_data = $this->db->get_where('web_menu')->row_array();
		if(empty($menu_data)){
			$this->db->insert('web_menu', $data);
			echo "1";
		}else{
			$this->db->where('id', $menu_data['id']);
			$this->db->update('web_menu', $data);
			echo "1";
		}
	}

}