<?php

class Points_earns extends MX_Controller {

	public function __construct(){
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
	}

	public function report(){
		$data['page_data'] = $this->load->view('points_earns/report_list', $view_data=array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_all_points_list(){

		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'email',
			3 => 'area',
			4 => 'reward',
			5=>'creation_time'
		);

		$query = "SELECT count(id) as total
				  FROM user_activity_rewards";

		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT uar.id , u.name, u.email , uar.area, CAST(uar.reward AS SIGNED) as reward ,DATE_FORMAT(FROM_UNIXTIME(uar.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time
				FROM   user_activity_rewards as uar 
				join   users  as u on uar.user_id = u.id
				where 1=1 ";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			//$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND  u.name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND u.email LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND  uar.area LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" AND reward LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
	
		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = ++$requestData['start'];
			$nestedData[] = $r->name;
			$nestedData[] = $r->email;
			$nestedData[] = $r->area;
			$nestedData[] = $r->reward;
			$nestedData[] = $r->creation_time;
			
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='".AUTH_PANEL_URL."currency_master/Currency_master/add_currency?currency_id=".$r->id."'>Edit</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format

	}

}