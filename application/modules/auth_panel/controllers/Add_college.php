<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Add_college extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->model('Add_college_model');
		$this->load->library('form_validation');
		
	}

	public function index() {
		if($this->input->get('tag_cache_cleared') != "" && 2 == base64_decode($this->input->get('tag_cache_cleared'))) {
			$data['page_toast'] = 'Cache cleared successfully';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
		}

		if(isset($_POST)) { 
			$this->form_validation->set_rules('name', 'College Name', "required|is_unique[college_list.name]");
			$this->form_validation->set_rules('state_id', 'State', "required");
			
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$insert = array(
					'state_id'=>$this->input->post('state_id'),
            		'name' => $this->input->post('name'),	
            		'status' => $this->input->post('status'),
					'created'=>milliseconds(),
					'modified'=>milliseconds()					
            		);
            	$this->db->insert('college_list',$insert);
 
				$data['page_toast'] = 'Information Inserted  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }

        }
        //die('hell');
 		$view_data['page'] = 'add_college';
		$view_data['states'] = $this->Add_college_model->get_states();
		$data['page_data'] = $this->load->view('college/add_college', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_all_college_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name		
			0 => 'state_id',
			1 => 'name',
			2 => 'status',
		);

		$query = "SELECT count(id) as total
								FROM college_list 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT college_list.id , college_list.name , college_list.status ,college_list.created,college_list.modified,
				 state.name as state_name FROM college_list
				 JOIN state
				 ON college_list.state_id = state.id ";

		// getting records as per search parameters
		
		if (!empty($requestData['columns'][0]['search']['value'])) {  //salary
			$sql.=" where college_list.state_id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		} 
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND college_list.name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND college_list.status LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		
		$query = $this->db->query($sql)->result();
        //echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
		$sql .= "order by college_list.id asc";
		$result = $this->db->query($sql)->result();
		//echo "<pre>";print_r($result); echo count($result)-1;die;

		$data = array();
		//print_r($result);
		foreach ($result as $key => $r) {  // preparing an array
			$nestedData = array();

			
			$nestedData[] = $r->state_name;
			$nestedData[] = $r->name;
			$nestedData[] = ($r->status==1)?'<span style="color:green">Enabled</span>':'<span style="color:red">Disabled</span>';
			$nestedData[] = date('d-M-y',($r->created/1000));
			$nestedData[] = date('d-M-y',($r->modified/1000));
			$confirm_alert='return confirm("Are you sure?");';
			$action = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "add_college/edit_college/" . $r->id . "'>Edit</a>&nbsp;
			<a class='btn-xs bold btn btn-danger' onclick='$confirm_alert' href='" . AUTH_PANEL_URL . "add_college/delete_college/" . $r->id . "'>Delete</a>&nbsp;"
				;
			
			$nestedData[] = $action;
			
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function delete_college($id){
		$this->db->where('id',$id);
		$this->db->delete('college_list');
		if($this->db->affected_rows()==1){
			page_alert_box('success', 'Action performed.', 'Information Deleted  successfully.');
			redirect(AUTH_PANEL_URL.'Add_college');
		}else{
			page_alert_box('success', 'Action performed.', 'Information could not be deleted.');
			redirect(AUTH_PANEL_URL.'Add_college');
		}
	}

	public function edit_college($id) {
		if($this->input->post()) {
			$this->form_validation->set_rules('state_id', 'State', 'required');
			$this->form_validation->set_rules('name', 'College Name', 'required');
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	
            	$update = array(
            		'state_id' => $this->input->post('state_id'),
            		'name'  => $this->input->post('name'),
					'status' => $this->input->post('status'),
					'modified'=>milliseconds()
            		);
            	$this->db->where('id',$id);
				$this->db->update('college_list',$update);
				
				$data['page_toast'] = 'Information Updated  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }
		}
		$view_data['page'] = 'edit_college';
		$view_data['states'] = $this->Add_college_model->get_states();
		$view_data['college_data'] = $this->Add_college_model->get_college_by_id($id);
		
		$data['page_data'] = $this->load->view('college/edit_college', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function manage_states(){
		if($this->input->get('tag_cache_cleared') != "" && 2 == base64_decode($this->input->get('tag_cache_cleared'))) {
			$data['page_toast'] = 'Cache cleared successfully';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
		}

		if(isset($_POST)) { 
			$this->form_validation->set_rules('name', 'State Name', "required|is_unique[state.name]");
			
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$insert = array(
            		'name' => $this->input->post('name'),	
            		'status' => $this->input->post('status'),
					'created'=>milliseconds(),
					'modified'=>milliseconds()
            		);
            	$this->db->insert('state',$insert);
 
				$data['page_toast'] = 'Information Inserted  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }

        }
        //die('hell');
 		$view_data['page'] = 'add_state';
		$view_data['main_category'] = $this->Add_college_model->get_states();
		$data['page_data'] = $this->load->view('college/add_states', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function edit_states($id) {
		if($this->input->post()) {
			
			$this->form_validation->set_rules('name', 'State Name', 'required');
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	
            	$update = array(           		
            		'name'  => $this->input->post('name'),
					'status' => $this->input->post('status'),
					'modified'=>milliseconds()
            		);
            	$this->db->where('id',$id);
            	$this->db->update('state',$update);
 
				page_alert_box('success', 'Action performed.', 'Information Updated  successfully.');
            }
		}
		$view_data['page'] = 'edit_state';
		
		$view_data['state_data'] = $this->Add_college_model->get_state_by_id($id);
		
		$data['page_data'] = $this->load->view('college/edit_states', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function delete_states($id){
		$this->db->where('id',$id);
		$this->db->delete('state');
		if($this->db->affected_rows()==1){
			page_alert_box('success', 'Action performed.', 'Information Deleted  successfully.');
			redirect(AUTH_PANEL_URL.'Add_college/manage_states');
		}else{
			page_alert_box('success', 'Action performed.', 'Information could not be deleted.');
			redirect(AUTH_PANEL_URL.'Add_college/manage_states');
		}
	}

	public function ajax_all_state_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name		
			0 => 'name',
			1 => 'status',
			2 => 'created',
			3 => 'modified'
		);

		$query = "SELECT count(id) as total
								FROM state 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT * FROM state";

		// getting records as per search parameters
		
		if (!empty($requestData['columns'][0]['search']['value'])) {  //salary
			$sql.=" where name LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		} 
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" AND status LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" AND created LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" AND modified LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		
		$query = $this->db->query($sql)->result();
        //echo $this->db->last_query();
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
		$sql .= " order by id asc";
		$result = $this->db->query($sql)->result();
		//echo "<pre>";print_r($result); echo count($result)-1;die;

		$data = array();
		//print_r($result);
		foreach ($result as $key => $r) {  // preparing an array
			$nestedData = array();

			
			$nestedData[] = $r->name;
			$nestedData[] = $r->status==1?'<span style="color:green">Enabled</span>':'<span style="color:red">Disabled</span>';
			$nestedData[] = date('d-M-y',($r->created/1000));
			$nestedData[] = date('d-M-y',($r->modified/1000));
			$confirm_atert='return confirm("Are you sure?")';
			$action = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "add_college/edit_states/" . $r->id . "'>Edit</a>&nbsp;
			"
				;
			
			$nestedData[] = $action;
			
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	function read_docx($file='test.doc'){
		$userDoc='./'.$file;

		$fileHandle = fopen($userDoc, "r");
		$word_text = @fread($fileHandle, filesize($userDoc));
		$line = "";
		$tam = filesize($userDoc);
		$nulos = 0;
		$caracteres = 0;
		for($i=1536; $i<$tam; $i++)
		{
			$line .= $word_text[$i];

			if( $word_text[$i] == 0)
			{
				$nulos++;
			}
			else
			{
				$nulos=0;
				$caracteres++;
			}

			if( $nulos>1996)
			{   
				break;  
			}
		}

		//echo $caracteres;

		$lines = explode(chr(0x0D),$line);
		//$outtext = "<pre>";

		$outtext = "";
		foreach($lines as $thisline)
		{
			$tam = strlen($thisline);
			if( !$tam )
			{
				continue;
			}

			$new_line = ""; 
			for($i=0; $i<$tam; $i++)
			{
				$onechar = $thisline[$i];
				if( $onechar > chr(240) )
				{
					continue;
				}

				if( $onechar >= chr(0x20) )
				{
					$caracteres++;
					$new_line .= $onechar;
				}

				if( $onechar == chr(0x14) )
				{
					$new_line .= "</a>";
				}

				if( $onechar == chr(0x07) )
				{
					$new_line .= "\t";
					if( isset($thisline[$i+1]) )
					{
						if( $thisline[$i+1] == chr(0x07) )
						{
							$new_line .= "\n";
						}
					}
				}
			}
			//troca por hiperlink
			$new_line = str_replace("HYPERLINK" ,"<a href=",$new_line); 
			$new_line = str_replace("\o" ,">",$new_line); 
			$new_line .= "\n";

			//link de imagens
			$new_line = str_replace("INCLUDEPICTURE" ,"<br><img src=",$new_line); 
			$new_line = str_replace("\*" ,"><br>",$new_line); 
			$new_line = str_replace("MERGEFORMATINET" ,"",$new_line); 


			$outtext .= nl2br($new_line);
		}		
		print_r($outtext);
		$olines=explode("<br />", $outtext);

		$slines=array();
		foreach( $olines as $orow ){
			$ovalue=explode("	", $orow);
			if(count($ovalue)>5)$slines[]=$ovalue;
		}
		echo "<pre>";
        print_r($slines);
        echo "</pre>";
	}

	public function clear_server_cache($content) {
		$this->db->where('content_type', $content);
		$this->db->set('version', 'version+1', FALSE);
		$this->db->update('content_version');
		redirect('auth_panel/add_tag?tag_cache_cleared='.base64_encode(2));
	}

}