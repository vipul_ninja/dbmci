<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instructor_registration extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation', 'uploads');

	}

	public function index(){
		$data['message'] = "";
		if ($this->input->post()) {
			$this->form_validation->set_error_delimiters('<span>','</span>');
			$this->form_validation->set_rules('username', 'User Name', 'required|trim|is_unique[backend_user.username]');
			$this->form_validation->set_rules('password', 'Password', 'required|trim');
			$this->form_validation->set_rules('confirm_password', 'Confirm password', 'required|trim|matches[password]');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim|is_unique[backend_user.email]');

				if ($this->form_validation->run() == True) {
					$insert_array = array(
						'username' => $this->input->post('username'),
						'email' => $this->input->post('email'),
						'password' => md5($this->input->post('password')),
						'creation_time' => time(),
						'instructor_id' => 1,
						'status' => 1
					);
					$result = $this->db->insert("backend_user",$insert_array);
					$data['message'] = "You have successfully registered and you can login once you get approval by admin.";
				}
		}
		$this->load->view('instructor_registration/instructor_registration',$data);
	}
}
