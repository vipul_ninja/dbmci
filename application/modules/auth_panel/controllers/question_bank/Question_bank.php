<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
defined('BASEPATH') OR exit('No direct script access allowed');
class Question_bank extends MX_Controller {
	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('grocery_CRUD');
		$this->load->library('form_validation', 'uploads');
		$this->load->model("Question_bank_model");
		$this->load->library('upload');

	}

	public function amazon_s3_upload($name,$aws_path) {
		$_FILES['file'] = $name;
		//print_r($_FILES['file']);
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).$_FILES["file"]["name"],
							'SourceFile' => $_FILES["file"]["tmp_name"],
							'ContentType' => 'image',
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];

	}
	/* verify question */
	public function verify_question($config_id){

		$lang_code =  $this->input->get('lang_code');
		$this->db->where('config_id',$config_id);
		$this->db->where('lang_code',$lang_code);
		$array = array('is_verified'=>1);
		$this->db->update('course_question_bank_master',$array);
		page_alert_box('success','Action performed','Question verified successfully');
		redirect(AUTH_PANEL_URL.'question_bank/Question_bank/edit_question/'.$config_id.'?lang_code='.$lang_code);

	}

	public function add_image() {
		        error_reporting(0);
				if($_FILES['image_file']['name']!=''){
					$file  = $this->amazon_s3_upload($_FILES['image_file'],"course_file_meta");
					$data['url']= $file;
					echo json_encode($data); die;

				}else{
					$file = '';
					$data['url']= $file;
				    echo json_encode($data); die;
				}
	}

	public function _example_output($output = null)
	{
		$this->load->view(AUTH_TEMPLATE.'grocery_crud_template',(array)$output);
	}


	public function index(){

		$view_data['page']  = 'add_question';
		$data['page_title'] = "Add Question";
		//$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('question_bank/add_question', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}


	public function add_question() {
		
		if($this->input->post()) {
			if($this->input->post('save')=='save_n_next'){
				
				
				$i = 0;
				foreach($_POST['language'] as $language){
					$_SESSION['languagei'][$i] = $language;
			$i++;
					}
					$_SESSION['test_name'] = $this->input->post('test_name');
					$_SESSION['stream_id'] = $this->input->post('stream_id');
					$_SESSION['sub_stream_id'] = $this->input->post('sub_stream_id');
					$_SESSION['subject_id'] = $this->input->post('subject_id');
					$_SESSION['topic_id'] = $this->input->post('topic_id');
					$_SESSION['question_screentype'] = $this->input->post('question_screentype');
					$_SESSION['question_type'] = $this->input->post('question_type');
					$_SESSION['difficulty_level'] = $this->input->post('difficulty_level');
					$_SESSION['status'] = $this->input->post('status');
					$_SESSION['typable'] = $this->input->post('typable');
			//  setcookie( 'test_name', $this->input->post('test_name'), time()+3600);
			//   setcookie( 'stream_id', $this->input->post('stream_id'), time()+3600);
			//   setcookie( 'sub_stream_id', $this->input->post('sub_stream_id'), time()+3600);
			//   setcookie( 'subject_id', $this->input->post('subject_id'), time()+3600);
			//   setcookie( 'topic_id', $this->input->post('topic_id'), time()+3600);
			//   setcookie( 'question_screentype', $this->input->post('question_screentype'), time()+3600);
			//   setcookie( 'question_type', $this->input->post('question_type'), time()+3600);
			  
			//   setcookie( 'difficulty_level', $this->input->post('difficulty_level'), time()+3600);
			//   setcookie( 'status', $this->input->post('status'), time()+3600);
			//   setcookie( 'typable', $this->input->post('typable'), time()+3600);
			  			
				$lng_count=	count($this->input->post('language'));
				$language=$this->input->post('language');
				$config_id=rand(100 , 999);
				
				foreach($language as $lng){
					$lng_fixer = '_'.$lng; 
				
					$aray_answer=array_unique($this->input->post('answer'));
					$answer = implode(',',$aray_answer);

					$insert_data =
					array('question' => $this->input->post('question'.$lng_fixer),
							//'paragraph_text'   => $this->input->post('split_text'.$lng_fixer),
							'test_name' => $this->input->post('test_name'),
							'description' => $this->input->post('description'.$lng_fixer),
							'subject_id'    => $this->input->post('subject_id'),
							'lang_code'    => $lng,
							'config_id'    => $config_id,
							'topic_id'  => $this->input->post('topic_id'),
							'question_type'    => $this->input->post('question_type'),
							'difficulty_level'   => $this->input->post('difficulty_level'),
							'marks'   => $this->input->post('marks'),
							//'screen_type'   => $this->input->post('question_screentype'),
							'negative_marks'   => $this->input->post('negative_marks'),
							'option_1'   => $this->input->post('option_1'.$lng_fixer),
							'option_2'   => $this->input->post('option_2'.$lng_fixer),
							'option_3'   => $this->input->post('option_3'.$lng_fixer),
							'option_4'   => $this->input->post('option_4'.$lng_fixer),
							'option_5'   => $this->input->post('option_5'.$lng_fixer),
							'answer'   => $answer,
							'status'  => $this->input->post('status'),
							//'typable'  => $this->input->post('typable'),
							'duration'  => $this->input->post('duration'),
							'stream_id'  => $this->input->post('stream_id'),
							'sub_stream_id'  => $this->input->post('sub_stream_id'),
							'uploaded_by'=>$this->session->userdata('active_user_data')->id
						);
				
							if($this->input->post('img_url_uplod')!==''){

							$insert_data['image']  = $this->input->post('img_url_uplod');
							}
					$add_series = $this->db->insert('course_question_bank_master',$insert_data);
					}
				if($add_series){

			page_alert_box('success','Action performed','Question added successfully');
					
		
	}
			}else if($this->input->post('save')=='save'){
				unset($_SESSION['test_name']);
				unset($_SESSION['stream_id']);
				unset($_SESSION['sub_stream_id']);
				unset($_SESSION['subject_id']);
				unset($_SESSION['topic_id']);
				unset($_SESSION['question_screentype']);
				unset($_SESSION['question_type']);
				unset($_SESSION['difficulty_level']);
				unset($_SESSION['status']);
				unset($_SESSION['typable']);
				unset($_SESSION['languagei']); 
							
				$lng_count=	count($this->input->post('language'));
				$language=$this->input->post('language');
				$config_id=rand(100 , 999);
				
				foreach($language as $lng){
					$lng_fixer = '_'.$lng; 
				
					$aray_answer=array_unique($this->input->post('answer'));
					$answer = implode(',',$aray_answer);

					$insert_data =
					array('question' => $this->input->post('question'.$lng_fixer),
							//'paragraph_text'   => $this->input->post('split_text'.$lng_fixer),
							'test_name' => $this->input->post('test_name'),
							'description' => $this->input->post('description'.$lng_fixer),
							'subject_id'    => $this->input->post('subject_id'),
							'lang_code'    => $lng,
							'config_id'    => $config_id,
							'topic_id'  => $this->input->post('topic_id'),
							'question_type'    => $this->input->post('question_type'),
							'difficulty_level'   => $this->input->post('difficulty_level'),
							'marks'   => $this->input->post('marks'),
							//'screen_type'   => $this->input->post('question_screentype'),
							'negative_marks'   => $this->input->post('negative_marks'),
							'option_1'   => $this->input->post('option_1'.$lng_fixer),
							'option_2'   => $this->input->post('option_2'.$lng_fixer),
							'option_3'   => $this->input->post('option_3'.$lng_fixer),
							'option_4'   => $this->input->post('option_4'.$lng_fixer),
							'option_5'   => $this->input->post('option_5'.$lng_fixer),
							'answer'   => $answer,
							'status'  => $this->input->post('status'),
							//'typable'  => $this->input->post('typable'),
							'duration'  => $this->input->post('duration'),
							'stream_id'  => $this->input->post('stream_id'),
							'sub_stream_id'  => $this->input->post('sub_stream_id'),
							'uploaded_by'=>$this->session->userdata('active_user_data')->id
						);
				
							if($this->input->post('img_url_uplod')!==''){

							$insert_data['image']  = $this->input->post('img_url_uplod');
							}
					$add_series = $this->db->insert('course_question_bank_master',$insert_data);
					}
				if($add_series){

			page_alert_box('success','Action performed','Question added successfully');
					
		}

		redirect(AUTH_PANEL_URL.'question_bank/Question_bank/view_question_list');
		
			}
			


      

		}
//	}
		$view_data['page']  = 'add_question';
		$data['page_title'] = "Add Question";
		$data['page_data'] = $this->load->view('question_bank/add_question', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function view_question_list() {
		$view_data['page']  = 'view_question_list';
		$data['page_title'] = "Add Question";
		$data['page_data'] = $this->load->view('question_bank/view_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_question_bank_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'config_id',
			1 => 'question',
			2 => 'stream_name',
			3 => 'sub_stream_name',
			4 => 'subject_name',
			5 => 'topic_id',
			6 => 'question_type',
			7 => 'difficulty_level',
			8 => 'uploader_name',
			9 => 'is_verified',
			10 => 'lang_code',
			11 => 'config_id',
		);

		$query = "SELECT count(id) as total FROM course_question_bank_master";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT cqbm.*,csm.name as subject_name , cstm.topic as topic_name ,
						bu.username as uploader_name , csnm.name as stream_name,
						csnmv.name as sub_stream_name,language_code.language
						FROM course_question_bank_master as  cqbm
						left join course_subject_master as csm on cqbm.subject_id = csm.id
						left join course_subject_topic_master as cstm  on cstm.id = cqbm.topic_id
						left join backend_user as bu on bu.id = cqbm.uploaded_by
						Left join course_stream_name_master as csnm on csnm.id = cqbm.stream_id
						Left join course_stream_name_master as csnmv on csnmv.id = cqbm.sub_stream_id
						Left join language_code on language_code.id=cqbm.lang_code
						where 1=1
						";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" and  question LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" and csnm.name LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" and csnmv.name LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" and csm.name LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" and cstm.topic  LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][6]['search']['value'])) {  //salary
			$sql.=" and question_type LIKE '" . $requestData['columns'][6]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][7]['search']['value'])) {  //salary
			$sql.=" and difficulty_level LIKE '" . $requestData['columns'][7]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][8]['search']['value'])) {  //salary
			$sql.=" and uploader_name LIKE '" . $requestData['columns'][8]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][9]['search']['value'])) {  //salary
			$sql.=" and is_verified LIKE '" . $requestData['columns'][9]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][10]['search']['value'])) {  //salary
			$sql.=" and lang_code LIKE '" . $requestData['columns'][10]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][11]['search']['value'])) {  //salary
			$sql.=" and config_id LIKE '" . $requestData['columns'][11]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY id   desc  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
		//$sql.="order by cqbm.config_id ";
		$result = $this->db->query($sql)->result();
        //echo $this->db->last_query();
		$data = array();
		$color = array('#800080','#C0C0C0','#808080','#000000','#FF0000','#800000','#FFFF00','#808000','#00FF00','#008000','#53141D');
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$difficulty_level = "";
			if($r->difficulty_level == 1){ $difficulty_level = "Easy"; }elseif($r->difficulty_level == 2){$difficulty_level = "Medium";}elseif($r->difficulty_level == 3){$difficulty_level = "Hard";}
			if($r->is_verified == 1){ $status = "<span class='text-success bold'>Yes</span>";}else{$status = "<span class='text-danger bold'>No</span>";}

			//$nestedData[] = $r->id;
			$nestedData[]  =  ++$requestData['start'];
			$q = strip_tags($r->question);
			//echo substr($q,0,20).(strlen($q)>20?' ...':'');die;
			$nestedData[] = /*strip_tags($r->question);*/ '<span style="color: '.$color[substr($r->config_id, -1)].';" class="q_name"></span>'.mb_substr($q,0,25).(strlen($q)>25?' ...':'').' <small><sup class="hide text-danger  bold">C_ID '.$r->config_id.'</sup></small>';
			$nestedData[] = $r->stream_name;
			$nestedData[] = $r->sub_stream_name;
			$nestedData[] = ($r->subject_name)?$r->subject_name : "--NA--";
			$nestedData[] = ($r->topic_name)?$r->topic_name : "--NA--";
			$nestedData[] = $r->question_type;
			$nestedData[] = $difficulty_level;
			$nestedData[] = $r->uploader_name;
			$nestedData[] = $status;
			$nestedData[] = $r->language;
			$nestedData[] = $r->config_id;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "question_bank/question_bank/edit_question?config_id=".$r->config_id ."'>Edit</a>";
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function edit_question(){
		$config_id =  $this->input->get('config_id');
		
		if($this->input->post('update_question_button')){
			$this->edit_question_details();
			$lang_code =  $this->input->post('lang_code');
		}

		$view_data['page']  = 'edit_question';
		$data['page_title'] = "Edit Question";

		if($_GET['config_id'] ){
			$view_data['question_detail'] = $this->Question_bank_model->get_question_by_id($config_id);
		$data['page_data'] = $this->load->view('question_bank/edit_question', $view_data, TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
		}
	}

	private function edit_question_details(){  //print_r($_POST); die;
		if($this->input->post()) {
			
			$id     = $this->input->post('id');
			$lng_count=	count($this->input->post('language'));
			$language=$this->input->post('language');
			$aray_answer=array_unique($this->input->post('answer'));
			$answer = implode(',',$aray_answer);
			foreach($language as $lng){	
				$lng_fixer = '_'.$lng; 
            $this->form_validation->set_rules('question'.$lng_fixer, 'Question', 'required');
			$this->form_validation->set_rules('question_type', 'Question type', 'required');
			$this->form_validation->set_rules('difficulty_level', 'Difficulty level', 'required');
			$this->form_validation->set_rules('option_1'.$lng_fixer, 'Option 1', 'required');
			$this->form_validation->set_rules('option_2'.$lng_fixer, 'Option 2', 'required');
			$this->form_validation->set_rules('status', 'Status', 'required');
			$this->form_validation->set_rules('subject_id', 'subject_id', 'required');
			$this->form_validation->set_rules('topic_id', 'topic_id', 'required');
				if ($this->form_validation->run() == FALSE) {
					//$error = validation_errors();
		   //echo $error; die;
	   }{
				
				$update_data =
				array('question' => $this->input->post('question'.$lng_fixer),
						'paragraph_text'   => $this->input->post('split_text'.$lng_fixer),
						'test_name' => $this->input->post('test_name'),
						'description' => $this->input->post('description'.$lng_fixer),
						'subject_id'    => $this->input->post('subject_id'),
						'lang_code'    => $lng,
						'topic_id'  => $this->input->post('topic_id'),
						'question_type'    => $this->input->post('question_type'),
						'difficulty_level'   => $this->input->post('difficulty_level'),
						'marks'   => $this->input->post('marks'),
						'negative_marks'   => $this->input->post('negative_marks'),
						'option_1'   => $this->input->post('option_1'.$lng_fixer),
						'option_2'   => $this->input->post('option_2'.$lng_fixer),
						'option_3'   => $this->input->post('option_3'.$lng_fixer),
						'option_4'   => $this->input->post('option_4'.$lng_fixer),
						'option_5'   => $this->input->post('option_5'.$lng_fixer),
						'answer'   => $answer,
						'screen_type'  => $this->input->post('question_screentype'),
						'typable'  => $this->input->post('typable'),			
						'status'  => $this->input->post('status'),
						'duration'  => $this->input->post('duration'),
						'stream_id'  => $this->input->post('stream_id'),
						'sub_stream_id'  => $this->input->post('sub_stream_id'),
						'uploaded_by'=>$this->session->userdata('active_user_data')->id
					);
									
					if($this->input->post('img_url_uplod')!==''){

						$update_data['image']  = $this->input->post('img_url_uplod');
					}
					$this->db->where('lang_code',$update_data['lang_code']);
					$this->db->where('config_id',$id);
					$check_exstence = 	$this->db->get('course_question_bank_master')->row();
					if($update_data['lang_code'] != 1 && count($check_exstence) < 1  ){
						//check if already exist
						$update_data['config_id']  = $id;
						$update_data['uploaded_by'] = $this->session->userdata('active_user_data')->id;
						$add_series = $this->db->insert('course_question_bank_master',$update_data);
						page_alert_box('success','Action performed','Question updated successfully');
						
			//	redirect(AUTH_PANEL_URL.'question_bank/Question_bank/edit_question?config_id='.$id);
					}else{
						// all language type editing here

		
						if($update_data['lang_code'] == 1 ){
							$update_else = array();
							$update_else['stream_id'] = $update_data['stream_id'];
							$update_else['sub_stream_id'] = $update_data['sub_stream_id'];
							$update_else['subject_id'] = $update_data['subject_id'];
							$update_else['question_type'] = $update_data['question_type'];
							$update_else['difficulty_level'] = $update_data['difficulty_level'];
							$this->db->where('config_id',$id);
							$this->db->update('course_question_bank_master',$update_else);
						}else{
							unset($update_data['stream_id']);
							unset($update_data['sub_stream_id']);
							unset($update_data['subject_id']);
							unset($update_data['question_type']);
							unset($update_data['difficulty_level']);
						}

						$this->db->where('config_id',$id);
						$this->db->where('lang_code',$update_data['lang_code']);
						$add_series = $this->db->update('course_question_bank_master',$update_data);
                       // echo $this->db->last_query();die;
						page_alert_box('success','Action performed','Question updated successfully');
					//	redirect(AUTH_PANEL_URL.'question_bank/Question_bank/edit_question?config_id='.$id);
					}
}


//page_alert_box('success','Action performed','Question updated successfully');
	//redirect(AUTH_PANEL_URL.'question_bank/Question_bank/edit_question?config_id='.$id);
				}
			/////////vpppppppppp////////////////////
		
			}

					
 	 	}
	


		public function add_bulk_question_csv(){
			$view_data['csv_out'] = "";
			if($_POST){
				$view_data['csv_out'] = $this->uploadCSV();
			}

			$view_data['page']  = 'add_question';
			$data['page_title'] = "Add Question in Bulk";
			$data['page_data'] = $this->load->view('question_bank/add_bulk_question', $view_data, TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

		}

		private function uploadCSV(){

	    $count = 0;
	    $fp = fopen($_FILES['csvfile']['tmp_name'],'r') or die("can't open file");

			/*----------------------  question error check start here -------------------------------------*/
			$error=  ""; $row_error = "";
			while($csv_line = fgetcsv($fp,1024)){

				$count++;
				if($count == 1){
				    continue;
				}//keep this if condition if you want to remove the first row

				for($i = 0, $j = count($csv_line); $i < $j; $i++){
					$insert_csv = array();
					$insert_csv['question'] = trim($csv_line[0]);
					$insert_csv['description'] = $csv_line[1];
					$insert_csv['question_type'] = strtoupper(trim($csv_line[2]));
					$insert_csv['difficulty_level'] = $csv_line[3];
					$insert_csv['option_1'] = trim($csv_line[4]);
					$insert_csv['option_2'] = trim($csv_line[5]);
					$insert_csv['option_3'] = trim($csv_line[6]);
					$insert_csv['option_4'] = trim($csv_line[7]);
					$insert_csv['option_5'] = trim($csv_line[8]);
					$insert_csv['answer'] = trim($csv_line[9]);
					$insert_csv['stream_id'] = trim($csv_line[10]);
					$insert_csv['sub_stream_id'] = trim($csv_line[11]);
					$insert_csv['subject_id'] = trim($csv_line[12]);
					$insert_csv['topic_id'] = trim($csv_line[13]);
					$insert_csv['config_id'] = trim($csv_line[14]);
					$insert_csv['lang_code'] = trim($csv_line[15]);
				}
				if($insert_csv['question'] == "" ){
					   $row_error .= '"question" field is required. ';
				}
				if($insert_csv['question_type'] == "" ){
					   $row_error .= '"question_type" field is required. ';
				}
				if($insert_csv['difficulty_level'] == "" ){
					   $row_error .= '"difficulty_level" field is required. ';
				}
				if($insert_csv['option_1'] == "" ){
					   $row_error .= '"option_1" field is required. ';
				}
				if($insert_csv['option_2'] == "" ){
					   $row_error .= '"option_2" field is required. ';
				}
				if($insert_csv['answer'] == "" ){
					   $row_error .= '"answer" field is required. ';
				}
				if($insert_csv['question_type'] != "MC" && $insert_csv['question_type'] != "SC" && $insert_csv['question_type'] != "TF" ){
					   $row_error .= '"question_type" field is not valid. ';
				}
				if($insert_csv['stream_id'] == "" ){
					   $row_error .= '"stream_id" field is required. ';
				}
				if($insert_csv['sub_stream_id'] == "" ){
					   $row_error .= '"sub_stream_id" field is required. ';
				}
				if($insert_csv['subject_id'] == "" ){
					   $row_error .= '"subject_id" field is required. ';
				}
				if($insert_csv['topic_id'] == "" ){
					   $row_error .= '"topic_id" field is required. ';
				}
				if($insert_csv['config_id'] == "" ){
					$row_error .= '"config_id" field is required. ';
			 }
			 if($insert_csv['lang_code'] == "" ){
				$row_error .= '"lang_code" field is required. ';
		 }
				$defaul_answer_array = array(1,2,3,4,5);
				$answer = explode(',',$insert_csv['answer']); //print_r($defaul_answer_array);die;
				foreach($answer as $answerChild){
					if(in_array($answerChild,$defaul_answer_array) == false){
					   $row_error .= '"answer" field should contain valid integer values only like 1,2,3,4. ';
					}
				}
	      $i++;

				if($row_error != "" ){
					$error .= '<span class="bold">Error found in row number '.$count.' </span> -: '.$row_error.'</br>';
					$row_error = "";
				}
			 }
		   /* while loop end here */
				if($row_error != ""){
					 page_alert_box('error','Action not performed','Invalid CSV Uploaded');
					 return $row_error;
				}

			/*----------------------  question error check start here -------------------------------------*/
			 $count=0;
			   $fp = fopen($_FILES['csvfile']['tmp_name'],'r') or die("can't open file");
	        while($csv_line = fgetcsv($fp,1024)){
	            $count++;
	            if($count == 1)
	            {
	                continue;
	            }//keep this if condition if you want to remove the first row
	            for($i = 0, $j = count($csv_line); $i < $j; $i++){
								$insert_csv['question'] = trim($csv_line[0]);
								$insert_csv['description'] = $csv_line[1];
								$insert_csv['question_type'] = strtoupper(trim($csv_line[2]));
								$insert_csv['difficulty_level'] = $csv_line[3];
								$insert_csv['option_1'] = $csv_line[4];
								$insert_csv['option_2'] = $csv_line[5];
								$insert_csv['option_3'] = $csv_line[6];
								$insert_csv['option_4'] = $csv_line[7];
								$insert_csv['option_5'] = $csv_line[8];
								$insert_csv['answer'] = $csv_line[9];
								$insert_csv['stream_id'] = $csv_line[10];
								$insert_csv['sub_stream_id'] = $csv_line[11];
								$insert_csv['subject_id'] = $csv_line[12];
								$insert_csv['topic_id'] = $csv_line[13];
								$insert_csv['config_id'] = trim($csv_line[14]);
								$insert_csv['lang_code'] = trim($csv_line[15]);
								$insert_csv['uploaded_by'] = $this->session->userdata('active_user_data')->id;
	            }
				$i++;
				$this->db->where('config_id',$insert_csv['config_id']);
				$select_alreeady_exist_data=$this->db->get('course_question_bank_master')->num_rows();
			//	echo count($insert_csv['lang_code']);
				if($select_alreeady_exist_data<2){
	            $data = array(
										'question' => $insert_csv['question'],
										'description' => $insert_csv['description'],
										'question_type' => $insert_csv['question_type'],
										'difficulty_level' => $insert_csv['difficulty_level'],
										'option_1' => $insert_csv['option_1'],
										'option_2' => $insert_csv['option_2'],
										'option_3' => $insert_csv['option_3'],
										'option_4' => $insert_csv['option_4'],
										'option_5' => $insert_csv['option_5'],
										'answer' => $insert_csv['answer'],
										'uploaded_by' => $insert_csv['uploaded_by'],
										'stream_id' => $insert_csv['stream_id'],
										'sub_stream_id' => $insert_csv['sub_stream_id'],
										'subject_id' => $insert_csv['subject_id'],
										'topic_id' => $insert_csv['topic_id'],
										'lang_code' => $insert_csv['lang_code'],
										'config_id' =>$insert_csv['config_id']
										
									);
	      		$this->db->insert('course_question_bank_master', $data);
						page_alert_box('success','Action performed','CSV uploaded successfully');
					}else{
						
										page_alert_box('error','Action Not performed','CSV already Uploaded');
					}
	    	}
				
			}

		}



