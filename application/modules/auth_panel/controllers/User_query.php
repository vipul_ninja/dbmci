<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_query extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->model('user_query_model');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
	}

	public function index() {
		$view_data['page'] = 'user_query';
		$data['page_data'] = $this->load->view('user_query/user_query', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_all_user_query_list() {
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'user_id',
			2 => 'category',
			3 => 'title',
			
		);

		$query = "SELECT count(id) as total
									FROM user_queries
									";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT  u.name as name ,
		uq.* from user_queries uq 
		INNER JOIN users u ON 
		uq.user_id = u.id 
		where 1=1";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {   //name
			$sql.=" AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {   //name
			$sql.=" AND category LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {   //name
			$sql.=" AND title LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		
		
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name;
			$nestedData[] = $r->category;
			$nestedData[] = $r->title;
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "user_query/view_query/" . $r->id . "'>View</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function view_query($id) {
			$userQueryDetail = $this->user_query_model->getUserQueryDetailById($id);
			$view_data['page'] = 'user_query';
			$view_data['userQueryDetail'] =  $userQueryDetail;
			$data['page_data'] = $this->load->view('user_query/user_query_view', $view_data, TRUE);
			echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}



	public function user_query_admin_reply() {
		$this->db->query('INSERT INTO user_query_admin_reply (`backend_user_id`,`query_id`,`text`) VALUES ("'.$_POST['backend_user_id'].'","'.$_POST['query_id'].'","'.$_POST['text'].'")');

		$to = $_POST['queried_user_email'];
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_port' => 587,
			'smtp_user' => 'support@dbmci.com',
			'smtp_pass' => 'emed@321',
		);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from("support@dbmci.com", 'eMedicoz!');
		$this->email->to($to);
		$this->email->subject('eMedicoz support');
		//$this->email->message($this->load->view('emailer/otp_for_sign_up', $data, True));
		$this->email->message($_POST['text']);
		$this->email->set_mailtype("html");
		$this->email->send();
	
		redirect('auth_panel/user_query/view_query/'.$_POST['query_id']);
	}

}