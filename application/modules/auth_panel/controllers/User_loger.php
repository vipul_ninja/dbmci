<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_loger extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->model('user_query_model');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
	}

	public function index() {
		$view_data['page'] = 'user_logger';
		$data['page_data'] = $this->load->view('user_loger/logs', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_user_loger_list() {
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'name',
			1 => 'comment',
			2 => 'segment',
			3 => 'creation_time',
			
		);
//SELECT `id`, `user_id`, `comment`, `segment`, `creation_time` FROM `backend_user_activity_log` WHERE 1
		$query = "SELECT count(id) as total
									FROM backend_user_activity_log
									";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT  bu.username as name ,
		buag.* , DATE_FORMAT(FROM_UNIXTIME(buag.creation_time/1000), '%d-%m-%Y %h:%i:%s') as ctime from backend_user_activity_log buag 
		JOIN backend_user bu ON buag.user_id = bu.id 
		where 1=1 ";
        	
		//getting records as per search parameters
				if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND username LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {   //name
			$sql.=" AND comment LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {   //name
			$sql.=" AND segment LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {   //name
			$sql.=" AND creation_time LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		
		
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {  // preparing an array
			$nestedData = array();
			$nestedData[] = $r->name;
			$nestedData[] = $r->comment;
			$nestedData[] = $r->segment;
			$nestedData[] = $r->ctime;
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
    }
}