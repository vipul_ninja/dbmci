<?php


if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/*
# sample 

if (!function_exists('pre')) {

	function pre($array) {
		
	}

}

*/


if (!function_exists('pre')) {
	function pre($array) {
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}
}


if (!function_exists('return_data')) {
	function return_data($status=false,$message="",$data = array(),$error=array()){
		echo json_encode(array('status'=>$status,'message'=>$message,'data' => $data ,'error'=>$error)); 
		die;	
	}
}


if (!function_exists('post_check')){
	function post_check() {
		if ($_SERVER['REQUEST_METHOD'] != 'POST'){
			echo json_encode(array('status'=>false,'message'=>"Invalid input parameter.Please use post method.",'data' => array() ,'error'=>array())); 
			die;	
		}
		// put in redis channel 
		// $CI = & get_instance();
		// $array = array('api_info'=>'demo'); 
		// $array['url'] = current_url();
		// $array['hash'] = md5(current_url());
		// $CI->redis_magic->PUBLISH('API_LIVE_VIEW',json_encode($array));
		
	}
}

if (!function_exists('milliseconds')){
	function milliseconds() {
	    $mt = explode(' ', microtime());
	    return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
	}
}

if(!function_exists('is_comma_seprated')){
	function is_comma_seprated($string = "" , $return = "" ){
		
		if ($string != "" && count(explode(",",$string)) > 0) {
			if($return === True){
				return explode(',',$string);
			}
  			return true;
		}else{
			if($return === false){
				return array();
			}
			return false;			
		}
	}
}

//get_user_basic_data
if (!function_exists('services_helper_user_basic')) {
	function services_helper_user_basic($user_id) {
		$CI = & get_instance();
		$CI->db->where('id', $user_id);
		return $CI->db->get('users')->row_array();
	}
}

//User activity rewards manipulation
if(!function_exists('activity_rewards')){
	function activity_rewards($user_id , $area,$do="+"){
		if ($user_id != "" && $area != ""){	
			$data['area'] = $area;
			// fetch points w.r.t. area 
			$points = get_db_meta_key($area); 
			/* on post like */
			if($area == "POST_LIKE" && $do == "-"){ $data['area'] = "POST_DISLIKE"; }
			//COMMENT_ADD
			if($area == "COMMENT_ADD" && $do == "-"){ $data['area'] = "COMMENT_DELETE"; }
			//USER_FOLLOW
			if($area == "USER_FOLLOW" && $do == "-"){ $data['area'] = "USER_UNFOLLOW"; }
			//COURSE_REVIEW_POINTS
			if($area == "COURSE_REVIEW" && $do == "-"){ $data['area'] = "COURSE_REVIEW_REMOVE"; }
			//COURSE_INSTRUTOR_REVIEW
			if($area == "COURSE_INSTRUCTOR_REVIEW" && $do == "-"){ $data['area'] = "COURSE_INSTRUCTOR_REVIEW_REMOVE"; }
			
			if($points > 0 ){
				$data['user_id'] = $user_id;
				$data['reward']  = ($do=="-")?'-'.$points:$points;
				$data['creation_time'] = milliseconds();
				$CI = & get_instance();
				$CI->db->insert('user_activity_rewards',$data);
				$CI->db->query("Update users set reward_points = (SELECT sum(reward) as total FROM `user_activity_rewards` WHERE user_id = $user_id) where id = $user_id");
			}
		}
	}
}
