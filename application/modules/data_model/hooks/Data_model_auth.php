<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_model_auth {
	function __construct()
    {
        $this->CI =& get_instance();
    }
	
	public function index() {
		$header = getallheaders();
		/* do not repeat or over write */
		define('API_REQUEST_LANG' , ((array_key_exists('lang', $header))?$header['lang']:1));

		$perm =  $this->CI->router->fetch_class().'/'.$this->CI->router->fetch_method();

		if($this->CI->router->fetch_module() == 'data_model') {
			
			if($this->CI->router->fetch_class() != "registration"
				&& $this->CI->router->fetch_class() != "version"
				&& $this->CI->router->fetch_class() != "welcome"
				&& $this->CI->router->fetch_class() != "send_otp_on_email_sign_up"
				&& $this->CI->router->fetch_class() != "mobile_auth"
				){
				
				/* check if user_id and device_tokken both are not empty and keys are available 
				* else ignore the authentication 
				*/
				if(array_key_exists('user_id', $header) 
					&& array_key_exists('device_tokken', $header)
					&& $header['user_id'] != "" 
					&& $header['device_tokken'] != "" ){
					 
					$this->CI->db->select(array('device_tokken'));
				    $this->CI->db->where('id',$header['user_id']);
					$out = $this->CI->db->get('users')->row_array();
					/* Check if device tokken mismatch */
				    if($out && $out['device_tokken'] != "" && $header['device_tokken'] != $out['device_tokken'] ){
						echo json_encode(array("status"=>false,
												"message"=>"User Session Expire.",
												"data"=>array(),
												"error"=>array(),
												"auth_code"=>"100100"
												)
						);
				    	die;
					}
				}
			}

			/* controll language */
			$this->CI->load->helper('language');
			$set_lang = "english";
			$set_lang = (API_REQUEST_LANG == 2)?'hindi':$set_lang;
			$this->CI->lang->load('data_model_language',$set_lang);
		}


	}
}