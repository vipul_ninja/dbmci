<?php
$this->db->where("template_name",'payment_done');
$html =  $this->db->get("mailer")->row()->template_html;


$html = str_replace('{$name}', $name , $html);

// get transaction data
$this->db->where("pre_transaction_id",$pre_transaction_id);
$this->db->select('*');
$this->db->select("DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y %H:%i:%s') as c_date ");

$t_data =  $this->db->get("course_transaction_record")->row();

/* course title */
$this->db->select('title');
$this->db->where("id",$t_data->course_id);
$course_name =  $this->db->get("course_master")->row();

$html = str_replace('{$tax}', $t_data->tax , $html);
$html = str_replace('{$sno}', $t_data->id , $html);
$html = str_replace('{$receipt}', $t_data->post_transaction_id , $html);
$html = str_replace('{$receipt_date}', $t_data->c_date , $html);
$html = str_replace('{$fee_amount}', $t_data->course_price , $html);
$html = str_replace('{$course_name}', $course_name->title , $html);
echo $html;
