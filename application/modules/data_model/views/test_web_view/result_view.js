function cl(p) {
   console.log(p);
}

var app = angular.module('myApp', []);
var answer_meta = {
};
var fly_direction = 'left';


//var base_url = "http://13.232.24.221/";
var base_url = "http://localhost/ibt_latest/";

app.controller('body_controller', function ($scope, $http, $timeout) {
   $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8;";

   var url = base_url+"index.php/data_model/courses/test_series/get_test_series_result";
   var FormData = {
       'user_id': config_user_id,
       'test_segment_id': config_test_segment_id
   };

   $http({
       url: url,
       method: "Post",
       data: $.param(FormData),
       dataType: 'json'
   }).then(function (response) {
       //cl(response.data);
       /* make usable variable form json */
       var basic_info = response.data.data.basic_info;
       var question_bank = response.data.data;
       var over_all_time = 0;

    });

   var url = base_url+"index.php/data_model/courses/test_series/get_test_series_basic_result";
   var FormData = {
       'user_id': config_user_id,
       'test_segment_id': config_test_segment_id
   };

   $http({
       url: url,
       method: "Post",
       data: $.param(FormData),
       dataType: 'json'
   }).then(function (response) {
      res = response.data.data;
      $scope.test_series_name = res.test_series_name;
      $scope.user_rank = res.user_rank;
      $scope.total_attempt = res.total_user_attempt;
      $scope.score = res.marks;
      $scope.total_score = res.test_series_marks;
      $scope.reattempt_url = base_url+"index.php/data_model/courses/test_web/view?test_series_id="+res.test_series_id+"&user_id="+config_user_id;
      $scope.view_solution_url = base_url+"index.php/data_model/courses/test_web/view_solution?test_segment_id="+res.id+"&user_id="+config_user_id;
    });

   var url = base_url+"index.php/data_model/courses/test_series/get_question_dump";
   var FormData = {
       'user_id': config_user_id,
       'test_segment_id': config_test_segment_id
   };

   $http({
       url: url,
       method: "Post",
       data: $.param(FormData),
       dataType: 'json'
   }).then(function (response) {
      res = response.data.data.basic;
      $scope.total_unanswered = res.total_unanswered;
      $scope.total_answered = res.total_answered;
      $scope.total_correct = res.total_correct;
      $scope.total_incorrect = res.total_incorrect;
      qd = response.data.data.dump;
      var html = cls =  "";
      var counter = 1;
      //bullet_for_view
        $.each(qd, function (i, v) {
            if(v.answer ==""){ cls = "unattempt";}
            if(v.is_correct == "1"){ cls = "right-answer";}
            if(v.is_correct == "0"){ cls = "wrong-answer";}
            html +='<li class="icons-width"><a href="#" class="'+cls+'"><p>'+counter+'</p></a></li>';
            counter++;
        });
        $('.bullet_for_view').html(html);

        // states for subject 
        stat = response.data.data.subject_dump;

        stat_html = "";
        $.each(stat, function (i, v) {
            stat_html +='<div class="margin-bottom">';
            stat_html +='<div>';
            stat_html +='<a href="#demo_'+i+'" class="btn btn-primary btn1" data-toggle="collapse">'+v.name+' <span class="subject_counter">'+v.correct_question+'/'+v.total_question+'</span> </a>';
            stat_html +='';
            stat_html +='</div>';
            stat_html +='<div id="demo_'+i+'" class="collapse">';
            stat_html +='<div class="row">';
            stat_html +='<div class="col-12">';
            stat_html +='<ul  class="all-subject-ques margin-top">';
            li = ""; 
            $.each(v.topic_data , function (key, val) {
                /* create li  */
                t_name = val.name;
                if(val.name === null){
                    t_name = '-Na-';
                }
                li +="<li>"+t_name+" <span class='float-right margin-right'>"+ val.correct_question+"/"+ val.total_question+"</span></li>";
            });
            stat_html += li;
            stat_html +='</ul>';
            stat_html +='</div>';
            stat_html +='</div>';
            stat_html +=' </div></div>';
        });
        $('.stat_holder').html(stat_html);
   });

});

