function cl(p) {
   console.log(p);
}

var app = angular.module('myApp', []);
var answer_meta = {
};
var fly_direction = 'left';

app.controller('body_controller', function ($scope, $http, $timeout) {
   $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8;";

   var url = base_url+"index.php/data_model/courses/test_series/get_test_series_result";
   var FormData = {
       'user_id': config_user_id,
       'test_segment_id': config_test_segment_id
   };


   $http({
       url: url,
       method: "Post",
       data: $.param(FormData),
       dataType: 'json'
   }).then(function (response) {
       cl(response.data.data);
       /* make usable variable form json */
       var basic_info = response.data.data.basic_info;
       var question_bank = response.data.data;
       var over_all_time = 0;
       $scope.countdown = over_all_time;
       $scope.countdown_control = true;

       var updateCounter = function () {
           if ($scope.countdown_control === true) {
               $scope.countdown--;
               if ($scope.countdown < 11 && $scope.countdown % 2 === 0) {
                   $('.timer_div').css({"color": "#dc3545 !important", "font-weight": "bold"});
               } else {
                   $('.timer_div').css({"color": "#138496 !important", "font-weight": ""});
               }
           }
           if ($scope.countdown > 0) {
               $timeout(updateCounter, 1000);
           }
       };
       updateCounter();

       $scope.question_bank = question_bank;
       $scope.test_name = "";
       $scope.all_question_counter = question_bank.length;
       $scope.active_question_counter = 1;
       // Ready a answer meta in this
       $.each(question_bank, function (i, v) {
           id = v.id;
           answer_meta[id] = [];
           answer_meta[id].push(v.answer);
       });
       cl(answer_meta);
       /* put current question in global scope */
       $scope.current_question = question_bank[0];
       // pause timer
       $scope.pause_test = function () {
           if ($scope.countdown_control === false) {
               $scope.countdown_control = true;
               $('.pause_test').html('Pause <i class="fa fa-pause"></i>');
               $('.main-holder').show();
           } else {
               $scope.countdown_control = false;
               $('.pause_test').html('Start <i class="fa fa-play"></i>');
               $('.main-holder').hide();
           }
       }
       $scope.question_view = function (q) {
           /* put current question in global scope */
           $scope.current_question = q;
           question_meta_view(q);
           $scope.active_question_counter = $('#bullet_' + q.id).data('counter');

       }
       /* on cliclikng any answer it will work */
       $scope.validate_answer = function (opt) {
           q = $scope.current_question;
           exact_answer_array = (q.answer).split(',');
           exact_answer_array_length = parseInt(exact_answer_array.length);

           current_question_answer = answer_meta[q.id];
           current_question_answer_length = parseInt(current_question_answer.length);
           if (exact_answer_array_length !== current_question_answer_length) {
               // add answer to list
               answer_meta[q.id].push(parseInt(opt));
               // cl('code will be here');
               // cl(answer_meta[q.id]);
               if (parseInt(q.answer) === parseInt(opt)) {
                   $('#option_' + opt + ' p').removeClass('alert-dark').addClass('alert-success');
                   $('#bullet_' + parseInt(q.id)).removeClass('bullet-default').addClass('bullet-success');
               } else {
                   $('#option_' + opt + ' p').removeClass('alert-dark').addClass('alert-danger').effect("shake", {direction: "left", times: 2, distance: 2}, 200);
                   $('#bullet_' + parseInt(q.id)).removeClass('bullet-default').addClass('bullet-danger');
               }
           }

           current_question_answer = answer_meta[q.id];
           current_question_answer_length = parseInt(current_question_answer.length);

           if (exact_answer_array_length === current_question_answer_length) {
               // show possible answers as green
               $.each(exact_answer_array, function (i, v) {
                   $('#option_' + v + ' p').removeClass('alert-dark').addClass('alert-success').effect('highlight');
               });
               $('.people_element').show();//.effect('slide',{ direction: "right"});
           }
           $('.navigator').show();
       }
       $scope.finish_test = function () {
           var question_dump = [];
           $.each(answer_meta, function (i, v) {
               question_dump.push({'question_id': i, 'answer': v.join(",")});
           })

           final = {'time_spent': over_all_time, 'question_dump': JSON.stringify(question_dump), 'user_id': config_user_id, 'test_series_id': config_test_series_id}
           clsoe_terminal(final);
       }
       question_meta_view(question_bank[0]);
   });
});

app.filter('secondsToDateTime', function () {
   return function (countdown) {
       var d = new Date(0, 0, 0, 0, 0, 0, 0);
       d.setSeconds(countdown);
       return d;
   };
});


function question_meta_view(q) {
   exact_answer_array = (q.answer).split(',');

   html = '<div class="mg-btm">' + q.question + '</div>';
   html += '<div class="container">';
   question_head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

   // check if question is answered
   var is_answered = false;
   var next = 'hide';
   if ((answer_meta[q.id]).length > 0) {
       is_answered = true;
       next = '';
   }

   for (i = 1; i < 10; i++) {

       option_default = 'alert-dark';
       if ($.inArray(i, answer_meta[q.id]) === 0) {
           option_default = 'alert-danger';
           if ($.inArray(i, exact_answer_array) === 0 || i === parseInt(exact_answer_array[0])) {
               option_default = 'alert-success';
           }
       }
       if (is_answered) {
           if ($.inArray(i, exact_answer_array) === 0 || i === parseInt(exact_answer_array[0])) {
               option_default = 'alert-success';
           }
       }

       ind = 'option_' + i;
       if (q[ind] !== "" && typeof(q[ind]) !== "undefined" ) {
           html += '<div id="' + ind + '" onclick="angular.element(this).scope().validate_answer(' + i + ')" class="row"><div class="alert ' + option_default + ' col-md-12 answers">' + question_head[i - 1] + '. <span>' + q[ind] + '</span></div></div>';
       }
   }
   html += '</div>';
   html += '<div class="' + next + ' people_element "><sup><i class="fa fa-users"></i> ' + Math.round((q.total_right / q.total_attempt) * 100) + '%  people got this right.</sup></div>';
    html += '<div class="row description_element"><h2 class="explaination_head">Explaination</h2>' + q.description + '</div>';
   html += '<div class=""><button onclick ="back_event(' + q.id + ')" class=" col-xs-3 btn btn-info btn-sm float-left"><i class="fa fa-arrow-left"></i> Prev.</button></div>';
   html += '<div class=""><button onclick ="next_event(' + q.id + ')" class=" col-xs-3 btn btn-info btn-sm  navigator ' + next + ' float-right">Next <i class="fa fa-arrow-right"></i> </button></div>';

   $('#question_meta').effect('drop', {'direction': fly_direction}, 250, callback(html));

   $('.bullet').removeClass('bullet-active');
   $('#bullet_' + q.id).addClass('bullet-active');
}

function callback() {
   setTimeout(function () {
       $("#question_meta").removeAttr("style").fadeIn();
       $('#question_meta').html(html);
   }, 200);
}


function next_event(id) {
   fly_direction = 'left';
   $('#bullet_' + id).next('li').click();
}

function back_event(id) {
   fly_direction = 'right';
   $('#bullet_' + id).prev('li').click();
}
