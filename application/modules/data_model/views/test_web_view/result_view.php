<!DOCTYPE html>
<html lang=en ng-app="myApp">
<head>
    <meta charset=utf-8 />
    <title>Result</title>
    <meta name=viewport content="width=device-width; initial-scale=1.0" />
    <link type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
    <link type=text/css href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" rel=stylesheet />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link type=text/css href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" rel=stylesheet />
    
    <style>
     <?php $this->load->view('test_web_view/result_view.css'); ?>
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular-route.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>

<body ng-controller="body_controller">
    <div class="header">
      <div class="header-title float-left col-xs-4">
        <h3 clasa="text-capitalize">{{test_series_name}}</h3>
        <p>Keep practising</p>
      </div>  
      <div class="race-number float-right col-xs-4">
        <i class="fa fa-circle" aria-hidden="true"></i><span> 6</span>
      </div>  
    </div>
    <div class="score-board-one">  
    <div class="score-board">
      <div class="float-left col-xs-6">
        <h2>Score</h2>
        <h1>{{score}}</h1>
        <h3>out of {{total_score}}</h3>
      </div>
      <div class="float-right">
        <h2>Rank</h2>
        <h1>{{user_rank}}</h1>
        <h3>out of {{total_attempt}}</h3>
      </div> 
      <div class="clearfix"></div>  
       
    </div>
    <div class="share-btn">
        <a href="" class="btn btn-primary margin-bottom10">Share Your Score</a>
      </div>
      </div>
    <div class="question-board">
        <div class="question-title">
          <h1></h1>
        </div>
        <ul class="question-number">
          <li class="attempt-r">&#9679 <span>Attempt({{total_answered}})</span></li>
          <li class="right-answer-r">&#9679 <span>Correct({{total_correct}})</span></li>
          <li class="wrong-answer-r">&#9679 <span>Wrong({{total_incorrect}})</span></li>
          <li class="unattempt-r">&#9679 <span>Not attempted({{total_unanswered}})</span></li>
    </ul>
        <div class="questionciler listview">
    <ul class="bullet_for_view">

    </ul>
   </div>
   <div class="clearfix"></div>
        <div class="action-question">  
          <div class="leader-board">
            <a href="" class="btn btn-success btn-sm">Leader Board</a>
          </div>
          <div class="viewsolution">
            <a href="{{view_solution_url}}" class="btn btn-success btn-sm">View Solution</a>
          </div>
           <div class="reattampt">
            <a href="{{reattempt_url}}" class="btn btn-success btn-sm">Reattempt</a>
          </div>   
        </div>
        <div class="clearfix"></div>
        <div class="topic-ana">
          <h2>Topic Analysis</h2>
            <div class="stat_holder ">
            </div>
      </div>
    </div>  
  </body>
  <script>
      var base_url = '<?php echo base_url(); ?>';
      var config_user_id = '<?php echo $_GET['user_id'];?>';            
      var config_test_segment_id = '<?php echo $_GET['test_segment_id'];?>';
   </script>
  <script><?php $this->load->view('test_web_view/result_view.js'); ?></script>
</html>