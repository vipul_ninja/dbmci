<!DOCTYPE html>
<html lang=en ng-app="myApp">
    <head>
        <meta charset=utf-8 />
        <title>
            MME
        </title>
        <meta name=viewport content="width=device-width; initial-scale=1.0" />
        <link type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
        <link type=text/css href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" rel=stylesheet />
        <link type=text/css href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" rel=stylesheet />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js">
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js">
        </script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
        </script>
         <style> <?php $this->load->view('test_web_view/style.css'); ?></style>
    </head>
    <style>
        .timer_div{
            margin-top: 5px;
            color: #17a2b8;
            transition-timing-function: linear;
        }
        
    </style>
    <body ng-controller="body_controller">
        <div class="container-fluid">

        <div class="col-12">
            <h4 class="logo">
                <small class="number float-right">
                    {{active_question_counter}}/{{all_question_counter}}
                </small>
            </h4>

        </div>
        
        <div class="main-holder" style="margin-bottom: 20px;">
            <ul class="question-number" style="display: none;"  >
                <li class="bullet-default bullet" data-counter="{{$index +1}}" ng-repeat="x in question_bank" id="bullet_{{x.id}}" data-u="{{x}}" ng-click="question_view(x)" >
                    &#9679
                </li>
            </ul>

            <div id="question_meta">

            </div>
        </div>
        <script>
            var base_url = '<?php echo base_url(); ?>';
            var config_user_id = '<?php echo $_GET['user_id'];?>';            
            var config_test_segment_id = '<?php echo $_GET['test_segment_id'];?>';
        </script>
         <script>
            <?php $this->load->view('test_web_view/solution.js'); ?>
         </script>
        </div>
    </body>
</html>
