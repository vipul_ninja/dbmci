<!DOCTYPE html>
<html lang=en ng-app="myApp">
    <head>
        <meta charset=utf-8 />
        <title>IBT</title>
        <meta name=viewport content="width=device-width; initial-scale=1.0" />
        <link type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
        <!-- <link type=text/css href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" rel=stylesheet /> -->
        <!-- <link type=text/css href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" rel=stylesheet /> -->
        <!-- <link type=text/css href="assets/css/style.css" rel=stylesheet /> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js">
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js">
        </script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css">
     
    <!-- Default Theme -->
    <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css">
     
    <!--  jQuery 1.7+  -->
    <!-- <script src="jquery-1.9.1.min.js"></script> -->
     
    <!-- Include js plugin -->
    <script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>


    </head>

    <style>

      <?php $this->load->view('test_web_view/style.css'); ?>

    </style>
    <body ng-controller="body_controller">

    <div class="owl_view">    
        <data-owl-carousel class="owl-carousel question-number" data-options="{responsive:{0:{items:5},320:{items:7},500:{items:10}},navigation: true, pagination: true, rewindNav : false}">

            <li  owl-carousel-item=""  data-counter="{{$index + 1}}" ng-repeat="x in ::question_bank" id="bullet_{{x.id}}" data-u="{{x}}" ng-click="question_view(x)" >
            <!-- &#9679 -->{{$index + 1}}
            </li>
        </data-owl-carousel>
    </div>

        <div class="loader"><i class="fa fa-spin fa-spinner fa-4x"></i></div>
        <div id="mySidenav" data-view="0"  class="sidenav">
            <a  class="closebtn float-left" onclick="togger()"><i class="fa fa-angle-left fa-2x  fa-st " aria-hidden="true"></i></a>
            <a  class="float-right gridbtn" ><i class="fa fa-2x fa-th-large fa-st " aria-hidden="true"></i></a>
            <a  class="float-right listbtn" ><i class="fa fa-2x  fa-bars fa-st " aria-hidden="true"></i></a>

            <div class="questionciler">
                <a href="#"  style="background-color: #ccc" class=" cirlce_index_{{x.id}}"  ng-repeat="x in question_bank"  data-u="{{x}}" ng-click="question_view(x)" onclick="togger()" >
                    {{$index + 1}}
                </a>
            </div> 
            <div class="questionciler listview">
                <ul> 
                    <li class="icons-width " ng-repeat="x in question_bank"  data-u="{{x}}" ng-click="question_view(x)" onclick="togger()"  ><a href="#" style="background-color: #ccc" class=" cirlce_index_{{x.id}}">{{$index + 1}}</a><p class="mrtop">{{x.question|htmlToPlaintext}}</p></li>                   
                </ul>
            </div> 

            <div class="statusofquestion">
                <ul>
                    <li><span class="green">&#9679;</span> Answered</li>
                    <li><span class="red">&#9679;</span> Unanswered</li>
                    <li><span class="gray">&#9679;</span> Not Visited</li>
<!--                    <li><span class="yellow">&#9679;</span> Marked for Review</li>-->
                </ul>
            </div>
            <a href="#" class="btn btn-success btn-submit-section" ng-click="finish_test()" >SUBMIT SECTION</a>
        </div>

        <div class="container-fluid" >
    
            <div class="row timer_view ">

               <div class="col-4">
                  <!-- <h4 class="logo"> -->
                     <span class="togllemenu float-left" onclick="togger()"><i class="fa fa-st fa-2x fa-th" aria-hidden="true"></i></span> 
                  <!-- </h4> -->
               </div>

               <div class="timer_div col-4 " ng-click="pause_test()" >
                  <p class="text-center">
                     <span class="timu"><i class='fa fa-pause'> </i></span>
                     {{countdown| secondsToDateTime | date:'HH:mm:ss'}}
                  </p>   
               </div>

               <div class="col-4 ">
                  <h6 class="score">
                     <small class="number float-right"> {{active_question_counter}}/{{all_question_counter}} </small>
                  </h6>
               </div>
            </div>

            <div class="main-holder" style="margin-bottom: 50px;">

                <!-- <ul  class="question-number " >
                     <li class="bullet-default bullet" data-counter="{{$index + 1}}" ng-repeat="x in question_bank" id="bullet_{{x.id}}" data-u="{{x}}" ng-click="question_view(x)" >
                          &#9679 {{$index + 1}}
                     </li>
                </ul>-->

                <div id="question_meta">

                </div>
            </div>
        </div>
        <!-- model box -->
        <!-- Modal -->
        <!-- The Modal -->
        <div class="modal" id="report_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">        
                        <div class="error">
                            <i aria-hidden="true" class="fa fa-exclamation-circle"></i>
                        </div>


                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>



                    <div class="modal-body">
                        <div>     
                            <h4 class="modal-title text-center">Feedback/Report error</h4>
                        </div>  
                        <div class="">
                            <h6 class="modal-sub-title text-center">What are you reporting?</h6>
                        </div>   
                        <div class="col-md-12 msg text-success"></div>
                        <br>

                        <form class="report-form ng-untouched ng-pristine ng-valid" novalidate="" role="form">
                            <input type="hidden" name="question_id" value="">
                            <input type="hidden" name="category" value="Question ISSUE">
                            <div class="checkbox">
                                <label><input name="title" type="radio"  value="Factual error" class="ng-untouched ng-pristine ng-valid">
                                    Factual error
                                </label>
                            </div>
                            <div class="checkbox">
                                <label><input name="title" type="radio" value="Confusing question" class="ng-untouched ng-pristine ng-valid">
                                    Confusing question
                                </label>
                            </div>
                            <div class="checkbox">
                                <label><input name="title" type="radio" value="Inadequate explanation"  class="ng-untouched ng-pristine ng-valid">
                                    Inadequate explanation
                                </label>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control ng-untouched ng-pristine ng-valid" name="description" placeholder="Your feedback here" rows="4"></textarea>
                            </div>
                            <!---->
                            <div class="form-group submit-button">
                                <button class="btn btn-marrow-2" onClick="feed_back();" type="button">Submit
                                </button>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>

        <script>
            function togger() {
                var state = $('#mySidenav').data('view');
                if (state == 1) {
                    $('#mySidenav').hide("drop", {direction: "left"}, 400);
                    $('#mySidenav').data('view', 0);
                } else {
                    $('#mySidenav').show("drop", {direction: "left"}, 400);
                    $('#mySidenav').data('view', 1);
                }
            }
            
            $(".gridbtn").click(function () {
                $(".questionciler").show();
                $(".listview").hide();
            });
            $(".listbtn").click(function () {
                $(".questionciler").hide();
                $(".listview").show();
            });
        </script>

         <script>
            var base_url = '<?php echo base_url(); ?>';
            var config_user_id = '<?php echo $_GET['user_id']; ?>';
            var config_test_series_id = '<?php echo $_GET['test_series_id']; ?>';
            var config_text_type = '<?php echo $this->input->get('test_type'); ?>';
         </script>
         <script>
            <?php $this->load->view('test_web_view/app.js'); ?>
            $(document).ready(function() {
                //$("#owl-example").owlCarousel();
            });
         </script>

    </body>
</html>
