function cl(p) {
   console.log(p);
}

var app = angular.module('myApp', []);
var answer_meta = {
};
var fly_direction = 'left';
var text_type = "";

var current_for_swipe = '';
var all_question_counter = '';
var is_last = false;

if(config_text_type === 'practice'){
    $('.timer_view').hide();
}else{
    $('.owl_view').hide();
}

app.controller('body_controller', function ($scope, $http, $timeout) {
   $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8;";

   var url = base_url+"index.php/data_model/courses/test_series/get_test_series_with_id";
   var FormData = {
       'user_id': config_user_id,
       'test_series_id': config_test_series_id
   };

   $http({
       url: base_url+"index.php/data_model/courses/test_series/get_test_series_with_id",
       method: "Post",
       data: $.param(FormData),
       dataType: 'json'
   }).then(function (response) {
       /* make usable variable form json */
       var basic_info = response.data.data.basic_info;
       var question_bank = response.data.data.question_bank;
       var over_all_time = basic_info.time_in_mins * 60;
       $scope.countdown = over_all_time;
       $scope.countdown_control = true;

       if(config_text_type =="practice"){
           text_type = config_text_type; 
       }else{
           text_type = (basic_info.set_type == 1) ? 'quiz' : 'test';
       }
       
       if (text_type === 'quiz' ) {
           $('#finisher').hide();
       }
       /* timer code starts from here */
       var updateCounter = function () {
           if ($scope.countdown_control === true) {
               $scope.countdown--;
               if ($scope.countdown < 11 && $scope.countdown % 2 === 0) {
                   $('.timer_div').css({"color": "#dc3545 !important", "font-weight": "bold"});
               } else {
                   $('.timer_div').css({"color": "#138496 !important", "font-weight": ""});
               }
           }
           if ($scope.countdown > 0) {
               $timeout(updateCounter, 1000);
           }
       };
       updateCounter();
       /* timer code end  here */ 
       
       $scope.question_bank = question_bank;
       $scope.test_name = basic_info.test_series_name;
       $scope.all_question_counter = all_question_counter = question_bank.length;
       $scope.active_question_counter = 1;
       // Ready a answer meta in this
       $.each(question_bank, function (i, v) {
           id = v.id;
           answer_meta[id] = [];
       });
       /* put current question in global scope */
       $scope.current_question = question_bank[0];
       // pause timer
       $scope.pause_test = function () {
           if ($scope.countdown_control === false) {
               $scope.countdown_control = true;
               $('.timu').html('<i class="fa fa-pause"></i>');
               $('.main-holder').show();
           } else {
               $scope.countdown_control = false;
               $('.timu').html('<i class="fa fa-play"></i>');
               $('.main-holder').hide();
           }
       }
       $scope.question_view = function (q) {
           /* put current question in global scope */
           $scope.current_question = q;
           question_meta_view(q);
           $scope.active_question_counter = $('#bullet_' + q.id).data('counter');
           if ($scope.active_question_counter == $scope.all_question_counter) {
               $('#finisher').show();
           } else {
               $('#finisher').hide();
           }

       }
       /* on cliclikng any answer it will work */
       $scope.validate_answer = function (opt) {
           q = $scope.current_question;
           exact_answer_array = (q.answer).split(',');
           exact_answer_array_length = parseInt(exact_answer_array.length);
           current_question_answer = answer_meta[q.id];
           current_question_answer_length = parseInt(current_question_answer.length);

           // if single choice and already answered 
           // than set current as attempt and rest as unattempt 
           if (q.question_type === "SC" && current_question_answer_length > 0 && text_type === 'test') {
               $('.answers').removeClass('alert-info').addClass('alert-dark');
               answer_meta[q.id] = [];
           }

           current_question_answer = answer_meta[q.id];
           current_question_answer_length = parseInt(current_question_answer.length);

           if (exact_answer_array_length !== current_question_answer_length) {
               cl('u gone here ');
               // add answer to list
               answer_meta[q.id].push(parseInt(opt));
               var highlighter = 'alert-info';
               if (parseInt(q.answer) === parseInt(opt)) {
                   if (text_type !== 'test') {
                       highlighter = "alert-success";
                   }
                   $('#option_' + opt + ' p').removeClass('alert-dark').addClass(highlighter);
                   $('#bullet_' + parseInt(q.id)).removeClass('bullet-default').addClass('bullet-success');
               } else {
                   if (text_type !== 'test') {
                       highlighter = "alert-danger";
                   }
                   $('#option_' + opt + ' p').removeClass('alert-dark').addClass(highlighter).effect("shake", {direction: "left", times: 2, distance: 2}, 200);
                   if (text_type !== 'test') {
                       $('#option_' + opt + ' p .ticker').html('<i class="fa fa-times-circle"></i>');
                   }
                   $('#bullet_' + parseInt(q.id)).removeClass('bullet-default').addClass('bullet-danger');
               }
           } else if ($.inArray(parseInt(opt), current_question_answer) === 0 && text_type === 'test') {
               var arr = $.unique(answer_meta[q.id]);
               var itemtoRemove = parseInt(opt);
               arr.splice($.inArray(itemtoRemove, arr), 1);
               answer_meta[q.id] = arr;
               $('#option_' + opt + ' p').removeClass('alert-info').addClass('alert-dark');
           }

           current_question_answer = answer_meta[q.id];
           current_question_answer_length = parseInt(current_question_answer.length);

           if (exact_answer_array_length === current_question_answer_length && text_type != 'test') {
               // show possible answers as green
               $.each(exact_answer_array, function (i, v) {
                   $('#option_' + v + ' p').removeClass('alert-dark').addClass('alert-success').effect('highlight');
                   $('#option_' + v + ' p .ticker').html('<i class="fa fa-check-circle"></i>');
               });
               if (text_type !== 'test') {
                   $('.people_element , .description_element , .per_percent ').show();
               }
               //.effect('slide',{ direction: "right"});

           }
           $('.navigator').show();
           if (text_type === "test" && answer_meta_with_id_length(q.id) < 1) {
               $('#bullet_' + parseInt(q.id)).removeClass('bullet-danger');
               $('#bullet_' + parseInt(q.id)).removeClass('bullet-succsess');
               $('#bullet_' + parseInt(q.id)).addClass('bullet-default');
           } else if (text_type === "test" && answer_meta_with_id_length(q.id) > 0) {
               $('#bullet_' + parseInt(q.id)).removeClass('bullet-default');
               $('#bullet_' + parseInt(q.id)).removeClass('bullet-danger');
               $('#bullet_' + parseInt(q.id)).addClass('bullet-success');
           }
           $('.cirlce_index_' + q.id).css('background-color', '#28a745');
       }
       $scope.finish_test = function () {
           var question_dump = [];
           $.each(answer_meta, function (i, v) {
               question_dump.push({'question_id': i, 'answer': v.join(",")});
           })

           final = {'time_spent': over_all_time - $scope.countdown, 'question_dump': JSON.stringify(question_dump), 'user_id': config_user_id, 'test_series_id': config_test_series_id}
           clsoe_terminal(final);
       }
       question_meta_view(question_bank[0]);
   });

}).directive("owlCarousel", function() {
	return {
		restrict: 'E',
		transclude: false,
		link: function (scope) {
			scope.initCarousel = function(element) {
			  // provide any default options you want
				var defaultOptions = {
				};
				var customOptions = scope.$eval($(element).attr('data-options'));
				// combine the two options objects
				for(var key in customOptions) {
					defaultOptions[key] = customOptions[key];
				}
				// init carousel
				$(element).owlCarousel(defaultOptions);
			};
		}
	};
})
.directive('owlCarouselItem', [function() {
	return {
		restrict: 'A',
		transclude: false,
		link: function(scope, element) {
		  // wait for the last item in the ng-repeat then call init
			if(scope.$last) {
				scope.initCarousel(element.parent());
			}
		}
	};
}]);


app.filter('secondsToDateTime', function () {
   return function (countdown) {
       var d = new Date(0, 0, 0, 0, 0, 0, 0);
       d.setSeconds(countdown);
       return d;
   };
});

app.filter('htmlToPlaintext', function (){
   return function (text){
       return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
   };
});


function answer_meta_with_id(id) {
   return answer_meta[id];
}

function answer_meta_with_id_length(id) {
   var current_question_answer = answer_meta[id];
   return parseInt(current_question_answer.length);
}

function question_meta_view(q) {
    //cl($('.question-number li:last-child').last().data('counter'));
    if(all_question_counter == $('.question-number li:last-child').last().data('counter')){
        $('#finisher').show();
    }
   /* handle with care variable used for swipe */
   current_for_swipe = q.id;

   exact_answer_array = (q.answer).split(',');
   $('input[name=question_id]').val(q.id);
   html = '<div class="mg-btm">' + q.question + '</div>';
   html += '<div class="container">';
   question_head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

   // check if question is answered
   var is_answered = false;
   var next = 'hide';
   if ((answer_meta[q.id]).length > 0) {
       is_answered = true;
       next = '';
   }

   for (i = 1; i < 10; i++) {

       option_default = 'alert-dark';
       var ticker_html = "";
       if ($.inArray(i, answer_meta[q.id]) === 0) {

           option_default = 'alert-danger';
           if (text_type === "quiz" || config_text_type === 'practice') {
               ticker_html = '<i class="fa fa-times-circle"></i>';
           }
           // for right answer element 
           if ($.inArray(i, exact_answer_array) === 0 || i === parseInt(exact_answer_array[0])) {
               option_default = 'alert-success';
               if (text_type === "quiz" || config_text_type === 'practice' ) {
                   ticker_html = '<i class="fa fa-check-circle"></i>';
               }
           }
           if (text_type === 'test') {
               option_default = "alert-info";
           }
       }
       if (is_answered) {
           if ($.inArray(i, exact_answer_array) === 0 || i === parseInt(exact_answer_array[0])) {
               //option_default = 'alert-success';
               if (text_type === "quiz" || config_text_type === 'practice' ) {
                   option_default = 'alert-success';
                   ticker_html = '<i class="fa fa-check-circle"></i>';
               }
           }
           // if(text_type == 'test'){ option_default = "alert-info";  }
       }

       ind = 'option_' + i;
       if (q[ind] !== "" && typeof (q[ind]) !== "undefined") {
           opt_percent = q['option_' + i + '_attempt'];
           if (text_type === 'quiz' && next === "") {
               var show_percent = "hide"; // remove hide for live 
           } else {
               var show_percent = "hide";
           }
           html += '<div id="' + ind + '" onclick="angular.element(this).scope().validate_answer(' + i + ')" class="row"><p class="alert ' + option_default + ' col-md-12 answers"><span class="ticker">' + ticker_html + '</span> ' + question_head[i - 1] + '. <span>' + q[ind] + '</span><!--<span class="float-right per_percent ' + show_percent + '">' + opt_percent + '%</span>--></p></div>';
       }
   }
   html += '<div class="row c_right_text"> ©IBT • Qbank  • QUID ' + encode_hash(123) + ' </div>';
   if (text_type === 'quiz' || config_text_type === 'practice' ) {
       total_percent = Math.round((q.total_right / q.total_attempt) * 100);
       html += '<div class="row description_element ' + next + ' "><h2 class="explanation_head">Explaination</h2>' + q.description + '</div>';
   }

   html += '</div>';

   html += '<div><p class="report_text" onClick="$(\'.msg\').html(\'\');"><a href="#" data-toggle="modal" data-target="#report_modal" ><i class="fa fa-exclamation-circle"></i> Report error</a></p></div>';
   html += '<div class=""><button onclick ="back_event(' + q.id + ')" class=" col-xs-3 btn btn-info btn-sm float-left prev_button"><i class="fa fa-arrow-left"></i> Prev.</button></div>';
   $('.bullet').removeClass('bullet-active');
   $('#bullet_' + q.id).addClass('bullet-active');
   //cl('here in before');
   if($('#bullet_' + q.id).data('counter') === all_question_counter){
       cl('here in ');
       html += '<div class="next_button_element"><button onclick ="angular.element(this).scope().finish_test()" class=" col-xs-3 btn btn-success btn-sm  navigator next_button ' + next + ' float-right">Finish <i class="fa fa-arrow-right"></i> </button></div>';
   }else{
    html += '<div class="next_button_element"><button onclick ="next_event(' + q.id + ')" class=" col-xs-3 btn btn-info btn-sm  navigator next_button ' + next + ' float-right">Next <i class="fa fa-arrow-right"></i> </button></div>';
   }

  
   
 
    // if(all_question_counter == $('.question-number li:last-child').last().data('counter')){
    //     html += '<div class=""><button onclick ="angular.element(this).scope().finish_test()" class=" col-xs-3 btn btn-info btn-sm  navigator next_button ' + next + ' float-right">Finish <i class="fa fa-arrow-right"></i> </button></div>';
    // }else{
    
    //}
   

   $('#question_meta').effect('drop', {'direction': fly_direction}, 250, callback(html));

   /* in the side bar index circle make update the  color */
   $('.cirlce_index_' + q.id).css('background-color', '#c82333');
}

function callback() {
   setTimeout(function () {
       $("#question_meta").removeAttr("style").fadeIn();
       $('#question_meta').html(html);
   }, 200);
}

function encode_hash(str) {
   return btoa(btoa(str));
}

function next_event(id) {
   fly_direction = 'left';
   $('#bullet_' + id).parent("div").next('div').children('li').click();
}

function back_event(id) {
   fly_direction = 'right';
   $('#bullet_' + id).parent("div").prev('div').children('li').click();
}

function clsoe_terminal(final) {
   $('.loader').show();
   cl(final);
   $.ajax({
       url: base_url+"index.php/data_model/courses/test_series/save_test",
       type: "POST",
       data: final,
       dataType: 'json',
       success: function (data) {
           var d = data.data;
           window.location.href = base_url+'index.php/data_model/courses/test_web/result?user_id=' + d.user_id + '&test_segment_id=' + d.id;
       }
   });
}

function feed_back() {
   title = $('input[name=title]').val() + ' -QID- ' + $('input[name=question_id]').val();
   category = $('input[name=category]').val();
   description = $('textarea[name=description]').val();

   final = {'title': title, 'user_id': config_user_id, 'category': category, 'description': description}
   cl(final);
   $('.loader').show();
   $.ajax({
       url: base_url+"index.php/data_model/user/user_queries/submit_query",
       type: "POST",
       data: final,
       dataType: 'json',
       success: function (data) {
           $('.msg').html(data.message);
           $('input[name=title]').val('');
           $('input[name=category]').val('');
           $('textarea[name=description]').val('');
           $('.loader').hide();
       }
   });
}