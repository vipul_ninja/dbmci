<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Course_rating_reviews_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function add_course_review($info){
        $insert = array(
            "course_fk_id"=>$info['course_id'],
            "user_id"=>$info['user_id'],
            "rating"=>$info['rating'],
            "text"=>$info['text'],
            "creation_time"=>milliseconds()
        );
        $this->db->insert('course_user_rating',$insert);
        /* update review count here */
        $count = $this->db->query('select count(id) as total , TRUNCATE(avg(rating),1) as rating  from course_user_rating where course_fk_id  ='.$info['course_id'])->row_array();
		$review = ($count && $count['total'] > 0 )?$count['total']:0;
        $rating = ($count && $count['rating'] > 0 )?sprintf("%.1f", $count['rating']):0;
		$this->db->where('id',$info['course_id']);
        $array = array('course_review_count'=>$review ,"course_rating_count"=>$rating);
        $this->db->update("course_master",$array);

        return true;
	}

    public function delete_course_review($data){
        $this->db->where('course_fk_id', $data['course_id']);
		$this->db->where('user_id', $data['user_id']);
        $this->db->delete("course_user_rating");

        /* update review count here */
        $count = $this->db->query('select count(id) as total , TRUNCATE(avg(rating),1) as rating  from course_user_rating where course_fk_id  ='.$data['course_id'])->row_array();
        $review = ($count && $count['total'] > 0 )?$count['total']:0;
        $rating = ($count && $count['rating'] > 0 )?sprintf("%.1f", $count['rating']):0;
        $this->db->where('id',$data['course_id']);
        $array = array('course_review_count'=>$review ,"course_rating_count"=>$rating);
        $this->db->update("course_master",$array);

        return true;
    }

    public function edit_course_review($info){
         $insert = array(
            "course_fk_id"=>$info['course_id'],
            "user_id"=>$info['user_id'],
            "rating"=>$info['rating'],
            "text"=>$info['text']
        );
        $this->db->where('course_fk_id',$info['course_id']);
        $this->db->where('user_id',$info['user_id']);
        $this->db->update('course_user_rating',$insert);

        /* update review count here */
        $count = $this->db->query('select count(id) as total , TRUNCATE(avg(rating),1) as rating  from course_user_rating where course_fk_id  ='.$info['course_id'])->row_array();
        $review = ($count && $count['total'] > 0 )?$count['total']:0;
        $rating = ($count && $count['rating'] > 0 )?sprintf("%.1f", $count['rating']):0;
        $this->db->where('id',$info['course_id']);
        $array = array('course_review_count'=>$review ,"course_rating_count"=>$rating);
        $this->db->update("course_master",$array);


        return true;       
    }

    public function if_review_already_given($course_fk_id,$user_id){
        $this->db->where('course_fk_id',$course_fk_id);
        $this->db->where('user_id',$user_id);
        return $this->db->get('course_user_rating')->row_array();
    }
	
    public function get_reviews_list($info){
        $where = "";
        if($info['last_review_id'] != "" && $info['last_review_id'] != 0){ $where = " And cur.id < ".$info['last_review_id'] ; }
        $query = $this->db->query(" SELECT cur.* , u.name , u.profile_picture
                                    FROM course_user_rating AS cur
                                    Left JOIN users AS u ON u.id = cur.user_id
                                    WHERE cur.course_fk_id = '".$info['course_id']."' 
                                    $where 						
                                    ORDER BY cur.creation_time DESC
                                    LIMIT 0 , 10
									");
		return $query->result_array();
    }
}