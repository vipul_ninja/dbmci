<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_comment_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	

	public function add_comment($data){
		
		$data['time'] = milliseconds();

		$this->db->insert('user_post_comment', $data);
		$comment_id = $this->db->insert_id();

		$count = $this->db->query('select count(id) as total from user_post_comment where post_id ='.$data['post_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['post_id']);
		$this->db->set('comments', $count);
		$this->db->update("post_counter");

		/* check if comment is child of another comment */
		if($data['parent_id'] != 0){
			/* update comment count of parent */
			$count = $this->db->query('select count(id) as total from user_post_comment where parent_id ='.$data['parent_id'])->row_array();
			$count = ($count && $count['total'] > 0 )?$count['total']:0;
			$this->db->where('id',$data['parent_id']);
			$this->db->set('sub_comment_count', $count);
			$this->db->update("user_post_comment");
		}
		return $comment_id;
	} 
	
	
	public function update_comment($data){
		
		$this->db->where('id', $data['id']);
		$this->db->update('user_post_comment', $data); 
	} 
	
	public function delete_comment($data){
		/* get parent id of this comment */
		$parent_id = $this->db->query('select parent_id from user_post_comment where id ='.$data['id'])->row()->parent_id;

		$this->db->where('id', $data['id']);
		$this->db->where('user_id', $data['user_id']);
		$this->db->delete('user_post_comment'); 

		$count = $this->db->query('select count(id) as total from user_post_comment where post_id ='.$data['post_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['post_id']);
		$this->db->set('comments', $count);
		$this->db->update("post_counter");
		
		/* check if comment is child of another comment */
		if($parent_id != 0){
			/* update comment count of parent */
			$count = $this->db->query('select count(id) as total from user_post_comment where parent_id ='.$parent_id)->row_array();
			$count = ($count && $count['total'] > 0 )?$count['total']:0;
			$this->db->where('id',$parent_id);
			$this->db->set('sub_comment_count', $count);
			$this->db->update("user_post_comment");
		}
      
	} 
	
	public function get_post_comment($data){
		$where = "";
		if($data['last_comment_id'] != ""){ $where .= " And user_post_comment.id < ".$data['last_comment_id'] ; }
		if($data['parent_id'] != 0){
			$where .= " And user_post_comment.parent_id = ".$data['parent_id']." ";
		}else{
			$where .= " And user_post_comment.parent_id = 0 ";
		}
		$query = $this->db->query(" SELECT user_post_comment.id AS id,
									post_id, user_id, parent_id , sub_comment_count , 
									comment , time, image ,
									users.name AS name, users.profile_picture AS profile_picture , users.is_expert
							FROM `user_post_comment`
							JOIN users ON user_post_comment.user_id = users.id
							WHERE `post_id` = ".$data['post_id']."
							$where 						
							ORDER BY user_post_comment.id DESC
							LIMIT 0 , 10"
							);
		return $query->result_array();
  
	} 
	
	public function get_tag_people_in_comment($id){
		$query = $this->db->query(" SELECT u.id, u.name, u.profile_picture
									FROM user_comment_tag_people AS uctp
									JOIN users AS u ON uctp.tagged_user_id = u.id
									WHERE uctp.comment_id = $id"
							);
		return $query->result_array(); 	
	}


	public function get_single_comment_data($id){
		$query = $this->db->query(" SELECT user_post_comment.id AS id,
									post_id, user_id, parent_id , sub_comment_count  , 
									comment , time, image , 
									users.name AS name, users.profile_picture AS profile_picture , users.is_expert
									FROM `user_post_comment`
									JOIN users ON user_post_comment.user_id = users.id
									WHERE user_post_comment.id = $id"
								);
		return $query->row_array();

	}
}