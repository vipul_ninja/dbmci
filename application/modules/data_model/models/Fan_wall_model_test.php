<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Fan_wall_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	public function request_user_details_by_id($id){
		$query = $this->db->query("SELECT users.id,users.dams_tokken
									FROM users 
									WHERE id = ".$id."									
									");
		return $query->row_array();		
	}
	

	public function all_posts($data){
		$where = "";
		$where1 = "";
		$where2 = "";
		if($data['last_post_id'] != ""){ $where = " And pc.id < ".$data['last_post_id'] ; }
		if($data['tag_id'] != ""){ $where .= " And pc.post_tag = ".$data['tag_id'] ; }

		$joins_search_table = "";
		if($data['search_text'] != ""){ 
			$where .= " And ( post_tags.text like '%".$data['search_text']."%' or u.name like '%".$data['search_text']."%' or uptt.text LIKE '%".$data['search_text']."%' or uptm.question LIKE '%".$data['search_text']."%') ";

			$joins_search_table = " LEFT JOIN user_post_type_text as uptt on pc.id = uptt.post_id LEFT JOIN user_post_type_mcq  as uptm on pc.id = uptm.post_id ";
		}

		$u_data = $this->db->query("select dams_tokken from users where id = ".$data['user_id']." limit 0,1 ")->row_array();
		//echo $this->db->last_query(); die;

		if(count($u_data) > 0 && $u_data['dams_tokken'] == ""){
			$where1 .= " And pc.post_for = 2 ";
			$where2 .= " And u.is_live_agent != 1 ";  
		}
		else{
			$where1 .= " And pc.post_for = 1 ";
		}
		/*, pc.user_id, pc.post_type,  post_tags.text as post_tag , pc.is_shared, pc.parent_id, pc.likes, pc.comments, pc.share, pc.report_abuse, pc.creation_time , urd.master_id */ 
		$query = $this->db->query(" SELECT pc.id , pc.user_id, pc.post_type, 
									u.name as post_owner_name , u.profile_picture as post_owner_profile_picture ,
									urd.optional_text , mclt.text as speciality ,
									CASE
								    WHEN post_tags.text IS NULL THEN ''
								    ELSE post_tags.text
								  	END AS post_tag , 
								  	post_tags.id as post_tag_id ,
								  	pc.is_shared, pc.parent_id, pc.likes, pc.comments, pc.share, 
								  	pc.report_abuse, pc.creation_time 
									FROM post_counter AS pc
									JOIN user_registration_data AS urd ON pc.user_id = urd.user_id
									LEFT JOIN post_tags ON pc.post_tag = post_tags.id
									JOIN users AS u ON u.id = pc.user_id
									left JOIN master_category_level_two as mclt on urd.master_id_level_two = mclt.id
									LEFT JOIN user_registration_data as virtual_user on virtual_user.user_id = ".$data['user_id']."
									$joins_search_table
									WHERE pc.status = 0 
									and pc.is_shared = 0 
									$where1
									and urd.master_id = virtual_user.master_id
									$where2 						
									ORDER BY pc.creation_time DESC
									LIMIT 0 , 10
									");
		return $query->result_array();
		//echo $this->db->last_query(); die;
	}

	public function all_bookmaked($data){
		$where = "";
		if($data['last_post_id'] != ""){ $where = " And pc.id < ".$data['last_post_id'] ; }
		if($data['search_text'] != ""){ 
			$where .= " And ( post_tags.text like '%".$data['search_text']."%' or u.name like '%".$data['search_text']."%') "  ; 
		}
		$query = $this->db->query("SELECT pc.* 
									FROM post_counter as pc
									right join user_post_bookmark as upb on pc.id = upb.post_id
									LEFT JOIN post_tags ON pc.post_tag = post_tags.id
									JOIN users AS u ON u.id = pc.user_id
									WHERE pc.status = 0 
									and pc.is_shared = 0 
									and upb.user_id = ".$data['user_id']."
									$where 						
									ORDER BY upb.time DESC
									LIMIT 0 , 10
									");
		return $query->result_array();
	}

	public function all_user_post($data){
		$where = "";
		if($data['last_post_id'] != ""){ $where = " And pc.id < ".$data['last_post_id'] ; }

		$query = $this->db->query("SELECT pc.id , pc.user_id, pc.post_type, 
									u.name as post_owner_name , u.profile_picture as post_owner_profile_picture ,
									urd.optional_text , mclt.text as speciality ,
									CASE
								    WHEN post_tags.text IS NULL THEN ''
								    ELSE post_tags.text
								  	END AS post_tag , 
								  	post_tags.id as post_tag_id ,
								  	pc.is_shared, pc.parent_id, pc.likes, pc.comments, pc.share, 
								  	pc.report_abuse, pc.creation_time 
									FROM post_counter AS pc
									JOIN user_registration_data AS urd ON pc.user_id = urd.user_id
									LEFT JOIN post_tags ON pc.post_tag = post_tags.id
									JOIN users AS u ON u.id = pc.user_id
									left JOIN master_category_level_two as mclt on urd.master_id_level_two = mclt.id
									WHERE pc.status = 0 
									and pc.is_shared = 0 
									and pc.user_id = ".$data['user_id']."
									$where 						
									ORDER BY pc.creation_time DESC
									LIMIT 0 , 10
									");
		return $query->result_array();
	}

	public function get_user_post_type_mcq($id){
		$this->db->where("post_id",$id);
		$this->db->limit(1,0);
		return $this->db->get('user_post_type_mcq')->row_array();
	}

	public function get_user_post_type_mcq_multiple($ids){
		$this->db->where_in("post_id",$ids);
		return $this->db->get('user_post_type_mcq')->result_array();
	}

	public function get_basic_post_data($id){
				$query = $this->db->query(" SELECT pc.id, pc.user_id, pc.post_type, CASE
						    WHEN post_tags.text IS NULL THEN ''
						    ELSE post_tags.text
						  	END AS post_tag , 
						  	post_tags.id as post_tag_id , 
						   	pc.is_shared, pc.parent_id, pc.likes, pc.comments, pc.share, pc.report_abuse, pc.creation_time
									FROM post_counter AS pc
									LEFT JOIN post_tags ON pc.post_tag = post_tags.id
									WHERE pc.status = 0  
									and pc.id = '$id'					
									ORDER BY pc.creation_time DESC 
									LIMIT 0 ,1 
									");
		return $query->row_array();

	}

	public function get_user_post_type_normal($id){
		$this->db->where("post_id",$id);
		$this->db->limit(1,0);
		return $this->db->get('user_post_type_text')->row_array();
	}

	public function get_user_post_type_normal_multiple($ids){
		$this->db->where_in("post_id",$ids);
		return $this->db->get('user_post_type_text')->result_array();
	}

	public function get_user_post_file_meta($post_id){
		$this->db->where("post_id",$post_id);
		return $this->db->get('post_file_meta')->result_array();
	}

	public function get_post_owner_info($user_id){
		$this->db->select(array("id","name","profile_picture"));
		$this->db->where("id",$user_id);
		$this->db->limit(1,0);
		$data =  $this->db->get("users")->row_array();

		$query = $this->db->query("SELECT mclt.text, urd.optional_text
									FROM user_registration_data AS urd
									Left JOIN master_category_level_two AS mclt ON urd.master_id_level_two = mclt.id
									WHERE urd.user_id = $user_id 
									LIMIT 0 ,1 
									");
		$special =  $query->row_array();
		$data['speciality'] = ($special['text'] != "" )?$special['text']:$special['optional_text'];
		return $data; 
	}

	public function is_liked_post($post_id,$user_id){
		$this->db->where("post_id",$post_id);
		$this->db->where("user_id",$user_id);
		$this->db->limit(1,0);
		$result = $this->db->get("user_post_like")->row_array();
		if(count($result)> 0 ){
			return true; 
		}
		return false;
	}

	public function is_liked_post_multiple($ids,$request_user_id){
		$this->db->select('group_concat(post_id) as post_id');
		$this->db->where_in('post_id', $ids);
		$this->db->where("user_id",$request_user_id);
		return $this->db->get("user_post_like")->row()->post_id;
	}



	public function is_bookmarked_post($post_id,$user_id){
		$this->db->where("user_id",$user_id);
		$this->db->where("post_id",$post_id);
		$this->db->limit(1,0);
		$result =  $this->db->get('user_post_bookmark')->row_array();
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function is_bookmarked_post_multiple($ids,$request_user_id){
		$this->db->select('group_concat(post_id) as post_id');
		$this->db->where_in('post_id', $ids);
		$this->db->where("user_id",$request_user_id);
		return $this->db->get("user_post_bookmark")->row()->post_id;
	}

	public function get_info_of_shared_post_owner($post_id){
		
		$query = $this->db->query(" SELECT u.id, u.name, u.profile_picture
									FROM post_counter AS pc
									JOIN users AS u ON pc.user_id = u.id
									WHERE pc.id = $post_id"
							);
		return $query->row_array(); 
	}

	public function get_user_answer_for_mcq($data){
		$this->db->select("answer");
		$this->db->where("user_id",$data['user_id']);
		$this->db->where("mcq_id",$data['mcq_id']);
		if($data = $this->db->get('user_post_type_mcq_answers')->row_array()){
			return $data['answer'];
		}
		return "";
	}

    public function get_post_tagged_people($post_id){
		$query = $this->db->query(" SELECT u.id, u.name, u.profile_picture
									FROM user_post_tag_people AS uptp
									JOIN users AS u ON uptp.tagged_user_id = u.id
									WHERE uptp.post_id = $post_id"
							);
		return $query->result_array(); 
    }

}