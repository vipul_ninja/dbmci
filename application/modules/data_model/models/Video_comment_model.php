<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Video_comment_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	

	public function add_comment($data){
		
		$data['time'] = milliseconds();

		$this->db->insert('video_master_comment', $data);
		$comment_id = $this->db->insert_id();

		$count = $this->db->query('select count(id) as total from video_master_comment where video_id ='.$data['video_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['video_id']);
		$this->db->set('comments', $count);
		$this->db->update("video_master");
		/* check if comment is child of another comment */
		if($data['parent_id'] != 0){
			/* update comment count of parent */
			$count = $this->db->query('select count(id) as total from video_master_comment where parent_id ='.$data['parent_id'])->row_array();
			$count = ($count && $count['total'] > 0 )?$count['total']:0;
			$this->db->where('id',$data['parent_id']);
			$this->db->set('sub_comment_count', $count);
			$this->db->update("video_master_comment");
		}
		return $comment_id;
	} 
	
	
	public function update_comment($data){
		
		$this->db->where('id', $data['id']);
		$this->db->update('video_master_comment', $data); 
		return $this->get_single_comment_data($data['id']);
	} 
	
	public function delete_comment($data){
		/* get parent id of this comment */
		$parent_id = $this->db->query('select parent_id from video_master_comment where id ='.$data['id'])->row()->parent_id;
		
		$this->db->where('id', $data['id']);
		$this->db->where('user_id', $data['user_id']);
		$this->db->delete('video_master_comment'); 

		$count = $this->db->query('select count(id) as total from video_master_comment where video_id ='.$data['video_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['video_id']);
		$this->db->set('comments', $count);
		$this->db->update("video_master");
		/* check if comment is child of another comment */
		if($parent_id != 0){
			/* update comment count of parent */
			$count = $this->db->query('select count(id) as total from video_master_comment where parent_id ='.$parent_id)->row_array();
			$count = ($count && $count['total'] > 0 )?$count['total']:0;
			$this->db->where('id',$parent_id);
			$this->db->set('sub_comment_count', $count);
			$this->db->update("video_master_comment");
		}
	} 
	
	public function get_post_comment($data){
		$where = "";
		if($data['last_comment_id'] != ""){ $where = " And video_master_comment.id < ".$data['last_comment_id'] ; }
		if($data['parent_id'] != 0){
			$where = " And video_master_comment.parent_id = ".$data['parent_id']." ";
		}else{
			$where .= " And video_master_comment.parent_id = 0 ";
		}

		$query = $this->db->query(" SELECT video_master_comment.id AS id,
									video_id, user_id, parent_id , sub_comment_count ,
									comment , time, 
									users.name AS name, users.profile_picture AS profile_picture
							FROM `video_master_comment`
							JOIN users ON video_master_comment.user_id = users.id
							WHERE video_master_comment.video_id = ".$data['video_id']."
							$where 						
							ORDER BY video_master_comment.id DESC
							LIMIT 0 , 10"
							);
		return $query->result_array();
  
	} 
	


	public function get_single_comment_data($id){
		$query = $this->db->query(" SELECT video_master_comment.id AS id,
									video_id, user_id,parent_id , sub_comment_count ,
									comment , time, 
									users.name AS name, users.profile_picture AS profile_picture
									FROM `video_master_comment`
									JOIN users ON video_master_comment.user_id = users.id
									WHERE video_master_comment.id = $id"
								);
		return $query->row_array();
	}
}