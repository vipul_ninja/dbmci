<?php

class Exam_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /* test */
    public function get_exam_basic_data($in){
        $date = date("y-m-d");
        $query = "SELECT cm.id, cm.title, cm.course_attribute, cm.left_text as l_text ,
                         cm.right_text as r_text ,  cm.color_code as color_code ,
                         cm.cover_image as image_icon , cm.desc_header_image ,
                         cm.mrp ,cm.course_sp , cm.color_code ,  cm.validity , cm.description
                    From course_master as cm
                    left join course_category as cct on cm.course_category_fk = cct.id
                    Where cm.id = ".$in['course_id']."
                    and cm.publish = 1
                    and cm.state = 0
                    and (cm.start_date = '0000-00-00' or ('$date' >= cm.start_date and '$date' <= cm.end_date ))
                    order by cm.creation_time desc
                    limit 0 ,1 ";
        $query = $this->db->query($query)->row_array();
        return $query;
    }

    public function test_main_category_list($course_id){

        $query = "SELECT distinct(csnm.id) as id  ,  csnm.name as title ,  csnm.image as image_icon , color as c_code 
                  FROM course_topic_master as ctm 
                  JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                  JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                  JOIN course_stream_name_master  as csnm on csnm.id = ctsm.stream
                  WHERE ctm.course_fk = $course_id and ctm.topic_type = 'test'";
        return $data = $this->db->query($query)->result();
    }

    public function test_sub_category_list($in){
        $main_id =  $in['main_id'];
        $course_id = $in['course_id'];
        $query = "SELECT distinct(csnmm.id) as id  ,  csnmm.name as title ,  csnmm.image as image_icon , csnmm.color as c_code 
                  FROM course_topic_master as ctm 
                  JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                  JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                  JOIN course_stream_name_master  as csnm on csnm.id = ctsm.stream
                  JOIN course_stream_name_master   as csnmm on csnmm.id = ctsm.sub_stream
                  WHERE csnm.id = $main_id and ctm.course_fk = $course_id and ctm.topic_type = 'test'";

        $data = $this->db->query($query)->result();
        return $data;
    }

    public function get_test_list($in){
        $course_id = $in['course_id'];
        $sub_id = $in['sub_id'];
        $test_type = $in['test_type'];
        $query = "SELECT ctsm.* , cse.tag as is_locked
                  FROM course_topic_master as ctm 
                  JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                  JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                  JOIN course_stream_name_master  as csnm on csnm.id = ctsm.stream
                  JOIN course_stream_name_master   as csnmm on csnmm.id = ctsm.sub_stream
                  WHERE csnmm.id = $sub_id and ctm.course_fk = $course_id and ctm.topic_type = 'test' and ctsm.test_type = $test_type";
        $return = $this->db->query($query)->result();
        return $return;
    }

    public function get_test_list_count($in){
        $course_id = $in['course_id'];
        $query  = "SELECT count(ctsm.id)  as total 
                  FROM course_topic_master as ctm 
                  JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                  JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                  WHERE ctm.course_fk = $course_id and ctm.topic_type = 'test'";
        $return = $this->db->query($query)->row()->total;
        return $return;
    }


    public function test_type_count($in){
        $course_id = $in['course_id'];
        if(array_key_exists('main_id',$in)){
            $main_id = $in['main_id'];
            $query = "SELECT  ctta.id ,  ctta.name as text , count(ctsm.test_type)  as count 
                    FROM course_topic_master as ctm 
                    JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                    JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                    JOIN course_test_type_attribute as ctta on ctta.id = ctsm.test_type
                    WHERE ctsm.stream = $main_id and ctm.course_fk = $course_id and ctm.topic_type = 'test' group by ctsm.test_type ";
        }
        if(array_key_exists('sub_id',$in)){
            $sub_id = $in['sub_id'];
            $query = "SELECT  ctta.id ,  ctta.name as text , count(ctsm.test_type)  as count 
                    FROM course_topic_master as ctm 
                    JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                    JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                    JOIN course_test_type_attribute as ctta on ctta.id = ctsm.test_type
                    WHERE ctsm.sub_stream = $sub_id and ctm.course_fk = $course_id and ctm.topic_type = 'test' group by ctsm.test_type ";
        }
        $return = $this->db->query($query)->result();
        return $return;     

    }    

    public function get_course_test_subject(){
        return $this->db->select('id , name as title ,image as image_icon ,color_code as c_code ')->get('course_subject_master')->result();
    }

    /* concept */

    public function get_course_concept_subject($in){
        $course_id = $in['course_id'];
        $query = "SELECT  csm.id , csm.name as title , csm.image as image_icon ,csm.color_code as c_code  , count(ctfmm.subject_id ) as total 
        FROM course_topic_master as ctm 
        JOIN course_segment_element as cse on cse.segment_fk = ctm.id
        JOIN course_topic_file_meta_master as ctfmm on ctfmm.id = cse.element_fk
        JOIN course_subject_master as csm on csm.id = ctfmm.subject_id
        WHERE ctm.course_fk = $course_id and cse.type != 'test' and ctfmm.file_type = 7 group by  ctfmm.subject_id"; 
        
        return $this->db->query($query)->result();
    }

    public function get_course_concept_subject_topic($in){
        $course_id = $in['course_id'];
        $subject_id  = $in['subject_id'];
        $query = "SELECT  cstm.id , cstm.topic as title , cstm.image as image_icon ,cstm.color_code as c_code  , count(ctfmm.topic_id ) as total 
        FROM course_topic_master as ctm 
        JOIN course_segment_element as cse on cse.segment_fk = ctm.id
        JOIN course_topic_file_meta_master as ctfmm on ctfmm.id = cse.element_fk
        JOIN course_subject_topic_master as cstm on cstm.id = ctfmm.topic_id
        WHERE ctm.course_fk = $course_id and cse.type != 'test' and ctfmm.file_type = 7 and  ctfmm.subject_id =  $subject_id  
        group by  ctfmm.topic_id"; 
        
        return $this->db->query($query)->result();
    }

    public function get_concept_list($in){
        $course_id = $in['course_id'];
        $topic_id  = $in['topic_id'];
        $user_id = $in['user_id'];
        $query = "SELECT  ctfmm.* , cse.tag as is_locked , if(upb.id IS NULL ,'0','1') as is_bookmarked
        FROM course_topic_master as ctm  
        JOIN course_segment_element as cse on cse.segment_fk = ctm.id
        JOIN course_topic_file_meta_master as ctfmm on ctfmm.id = cse.element_fk
        left join user_post_bookmark as upb on upb.post_id  = ctfmm.id  and upb.user_id = $user_id and  upb.area = 2
        WHERE  ctm.course_fk = $course_id and cse.type != 'test' and ctfmm.file_type = 7 and  ctfmm.topic_id =  $topic_id "; 
        
        return $this->db->query($query)->result();

    }

    public function get_concept_list_count($in){
        $course_id = $in['course_id'];
        $query = "SELECT  count(ctfmm.id) as total 
        FROM course_topic_master as ctm  
        JOIN course_segment_element as cse on cse.segment_fk = ctm.id
        JOIN course_topic_file_meta_master as ctfmm on ctfmm.id = cse.element_fk
        WHERE  ctm.course_fk = $course_id and ctm.topic_type = 'concept' and cse.type != 'test' and ctfmm.file_type = 7 "; 
        
        return $this->db->query($query)->row()->total;
    }

    /* video */
    public function get_course_video_subject($in){
        $course_id = $in['course_id'];
        $query = "SELECT  csm.id , csm.name as title , csm.image as image_icon ,csm.color_code as c_code  , count(ctfmm.subject_id ) as total 
                    FROM course_topic_master as ctm 
                    JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                    JOIN course_topic_file_meta_master as ctfmm on ctfmm.id = cse.element_fk
                    JOIN course_subject_master as csm on csm.id = ctfmm.subject_id
                    WHERE ctm.course_fk = $course_id and cse.type != 'test' and ctfmm.file_type = 3 group by  ctfmm.subject_id"; 
        
        return $this->db->query($query)->result();
    }
    public function get_course_video_subject_topic($in){
        $course_id = $in['course_id'];
        $subject_id  = $in['subject_id'];
        $query = "SELECT  cstm.id , cstm.topic as title , cstm.image as image_icon ,cstm.color_code as c_code  , count(ctfmm.topic_id ) as total 
                    FROM course_topic_master as ctm 
                    JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                    JOIN course_topic_file_meta_master as ctfmm on ctfmm.id = cse.element_fk
                    JOIN course_subject_topic_master as cstm on cstm.id = ctfmm.topic_id
                    WHERE ctm.course_fk = $course_id and cse.type != 'test' and ctfmm.file_type = 3 and  ctfmm.subject_id =  $subject_id  
                    group by  ctfmm.topic_id"; 
        
        return $this->db->query($query)->result();
    }

    public function get_video_list($in){
        $course_id = $in['course_id'];
        $topic_id  = $in['topic_id'];
        $query = "SELECT  ctfmm.* ,cse.tag as is_locked 
        FROM course_topic_master as ctm 
        JOIN course_segment_element as cse on cse.segment_fk = ctm.id
        JOIN course_topic_file_meta_master as ctfmm on ctfmm.id = cse.element_fk
        WHERE ctm.course_fk = $course_id and cse.type != 'test' and ctfmm.file_type = 3 and  ctfmm.topic_id =  $topic_id "; 
        
        return $this->db->query($query)->result();

    }

    public function get_video_list_count($in){
        $course_id = $in['course_id'];
        $query = "SELECT  count(ctfmm.id) as total  
        FROM course_topic_master as ctm 
        JOIN course_segment_element as cse on cse.segment_fk = ctm.id
        JOIN course_topic_file_meta_master as ctfmm on ctfmm.id = cse.element_fk
        WHERE ctm.course_fk = $course_id and ctm.topic_type = 'video' and cse.type != 'test' and ctfmm.file_type = 3  "; 
        return $this->db->query($query)->row()->total;
    }

    /* practice */
    public function get_course_practice_subject($in){
        $course_id = $in['course_id'];
        $query = "SELECT csm.id , csm.name as title , csm.image as image_icon ,csm.color_code as c_code  , count(csm.id ) as total 
                  FROM course_topic_master as ctm 
                  JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                  JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                  JOIN course_subject_master as csm on csm.id = ctsm.subject
                  WHERE ctm.course_fk = $course_id and ctm.topic_type = 'practice' group by csm.id";
        return $return = $this->db->query($query)->result();

    }

    public function get_course_practice_subject_topic($in){
        $course_id = $in['course_id'];
        $subject_id  = $in['subject_id'];
        $query = "SELECT cstm.id , cstm.topic as title , cstm.image as image_icon ,cstm.color_code as c_code  , count(ctsm.topic_id ) as total 
                  FROM course_topic_master as ctm 
                  JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                  JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                  JOIN course_subject_topic_master as cstm on cstm.id = ctsm.topic_id
                  WHERE ctm.course_fk = $course_id and  ctsm.subject = $subject_id  and  ctm.topic_type = 'practice' group by ctsm.topic_id ";
        $return = $this->db->query($query)->result();
        return  $return = $this->db->query($query)->result();
    }

    public function get_course_practice_list($in){
        $course_id = $in['course_id'];
        $topic_id  = $in['topic_id'];
        $query = "SELECT ctsm.* , cse.tag as is_locked 
                  FROM course_topic_master as ctm 
                  JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                  JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                  WHERE ctm.course_fk = $course_id and ctm.topic_type = 'practice' and ctsm.topic_id = $topic_id";
       return  $return = $this->db->query($query)->result();

    }

    public function get_course_practice_list_count($in){
        $course_id = $in['course_id'];
        $query = "SELECT count(ctsm.id) as total  
                  FROM course_topic_master as ctm 
                  JOIN course_segment_element as cse on cse.segment_fk = ctm.id
                  JOIN course_test_series_master as ctsm on ctsm.id = cse.element_fk
                  WHERE ctm.course_fk = $course_id and ctm.topic_type = 'practice'  and ctm.topic_type = 'practice'";
       return  $return = $this->db->query($query)->row()->total;

    }

    public function provide_feedback_for_video($in){
        $d = $this->db->where(array('user_id'=> $in['user_id'], 'video_id'=>$in['video_id']))->get('course_video_feedback_master')->row();
        if(count($d) == 0 ){
            $array = array('user_id'=>$in['user_id'],
            'video_id'=>$in['video_id'],
            'point'=>$in['point'],
            'text'=>$in['text'],
            'created_on'=> milliseconds()
            );  
            $this->db->insert('course_video_feedback_master',$array);
        }

    }    
}
