<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class User_category_handling_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	public function get_basic_list(){
		return $this->db->get('master_category')->result_array();
	}

	public function get_basic_list_level_one($id){
		$this->db->where("parent_id",$id);
		$this->db->where("visibilty",0);
		return $this->db->get('master_category_level_one')->result_array();
	}	

	public function get_basic_list_level_two($id){
		$this->db->where("parent_id",$id);
		return $this->db->get('master_category_level_two')->result_array();
	}	

}	