<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Video_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
/***
 *       _____                          _      _               
 *      / ____|                        | |    (_)              
 *     | (___    ___   __ _  _ __  ___ | |__   _  _ __    __ _ 
 *      \___ \  / _ \ / _` || '__|/ __|| '_ \ | || '_ \  / _` |
 *      ____) ||  __/| (_| || |  | (__ | | | || || | | || (_| |
 *     |_____/  \___| \__,_||_|   \___||_| |_||_||_| |_| \__, |
 *                                                        __/ |
 *                                                       |___/ 
 */    
    public function get_videos_for_cat($user_id,$sub_id,$sort_by,$last_video_id,$page_segment,$search_content,$related_video_id = ""  ){
        $search_content = addslashes($search_content);
        /* order condition */
        $max = 10 ; 
        $order_by =  " order by id desc limit 0,$max ";
        
        if($sort_by != "" && $sort_by == "views"){
            $page_segment = $page_segment * $max;
            $order_by =  " order by views desc limit $page_segment , $max  ";
        }
        if($sort_by != "" && $sort_by == "likes"){
            $page_segment = $page_segment * $max ;
            $order_by =  " order by likes desc limit  $page_segment , $max ";
        }
        /* lets make a where for all result or conditional result  */
        /* if study filter is asked */
        $where = "";
        $join  = ""; 
        if($sub_id > 0 ){
            $where .=  " And vsr.study_id = $sub_id  ";
            $join  .= " join video_study_relationship as  vsr on vm.id = vsr.video_id " ;
        }
        /* limit condition  with id 
        * do it only if you are sorting with time 
        */

        if($related_video_id != "" ){ 
            $where .= " And vm.id != $related_video_id ";
        }

        if($sort_by == "time" || $sort_by == "" ){
            $where .= ($last_video_id != "" && $last_video_id > 0 )?' And vm.id < '.$last_video_id:"";
        }

        $where_having ="" ; 
        if($search_content != ""){
            $where_having  = " having  ( match(video_title) Against('$search_content' IN BOOLEAN MODE ) or screen_tag like '%$search_content%')";
        }
        $sql =  "SELECT vm.* , 
                        IFNULL((SELECT GROUP_CONCAT(tag_name) from video_search_tag_list where find_in_set (id , (SELECT screen_tag FROM video_master WHERE id = vm.id))),'') as screen_tag ,
                        (select count(id) from video_like_meta as vlm where vlm.video_id = vm.id and vlm.user_id = $user_id  limit 0,1) as is_like,
                        (select count(id) from video_view_meta as vvm where vvm.video_id = vm.id and vvm.user_id = $user_id limit 0,1) as is_viewed, 
                        (select count(id) from video_favourite_list as vfl where vfl.video_id = vm.id and vfl.user_id = $user_id limit 0,1) as is_favourite
                        from video_master as vm
                        $join
                        where 1 =1 
                        $where  $where_having group by (vm.id)  $order_by  ";	

		return $data = $this->db->query($sql)->result_array();
    }  

/***
 *                   _      _  __      __ _                 
 *         /\       | |    | | \ \    / /(_)                
 *        /  \    __| |  __| |  \ \  / /  _   ___ __      __
 *       / /\ \  / _` | / _` |   \ \/ /  | | / _ \\ \ /\ / /
 *      / ____ \| (_| || (_| |    \  /   | ||  __/ \ V  V / 
 *     /_/    \_\\__,_| \__,_|     \/    |_| \___|  \_/\_/  
 *                                                          
 *                                                          
 */
    public function add_view($user_id,$video_id){
        $this->db->where('user_id',$user_id);
        $this->db->where('video_id',$video_id);
        $isexist = $this->db->get('video_view_meta')->num_rows();

        if($isexist == 0 ){
            $info = array(
                "time"=>milliseconds(),
                "user_id"=>$user_id,
                "video_id"=>$video_id
            );
            $this->db->insert("video_view_meta",$info );
            
            $this->db->where('id',$video_id);
            $this->db->set('views', 'views + 1', FALSE);
            $this->db->update("video_master");
        }
    }

    public function add_favourite_video($user_id,$video_id){
        $this->db->where('user_id',$user_id);
        $this->db->where('video_id',$video_id);
        $isexist = $this->db->get('video_favourite_list')->num_rows();
        if($isexist == 0 ){
            $info = array(
                "user_id"=>$user_id,
                "video_id"=>$video_id
            );
        $this->db->insert("video_favourite_list",$info );
        
        }
    }

    public function remove_favourite_video($user_id,$video_id){
       
        $info = array(
            "user_id"=>$user_id,
            "video_id"=>$video_id
        );
        $this->db->where($info);  
        $this->db->delete("video_favourite_list");
       
    }
    /* add fav*/
    public function favourite_video_list($user_id,$sort_by,$last_video_id){
        $max = 10 ; 
        $order_by =  " order by vm.id desc limit 0,$max ";
        if($user_id !== '' ){
            $where = " where vfl.user_id = '$user_id' ";
        }

        /* limit condition  with id 
        * do it only if you are sorting with time 
        */
        
        if($sort_by == "time" || $sort_by == "" ){
            $where .= ($last_video_id != "" && $last_video_id > 0 )?' And vm.id < '.$last_video_id:"";
        }
        $sql =  "SELECT vm.* , 
                        IFNULL((SELECT GROUP_CONCAT(tag_name) from video_search_tag_list where find_in_set (id , (SELECT screen_tag FROM video_master WHERE id = vm.id))),'') as screen_tag ,
                        (select count(id) from video_like_meta as vlm where vlm.video_id = vm.id and vlm.user_id = $user_id  limit 0,1) as is_like,
                        (select count(id) from video_view_meta as vvm where vvm.video_id = vm.id and vvm.user_id = $user_id limit 0,1) as is_viewed ,
                        '1' as is_favourite  
                        from video_master as vm  join video_favourite_list as vfl on vm.id=vfl.video_id $where    $order_by ";	

		return $data = $this->db->query($sql)->result_array();
    }  
}    
