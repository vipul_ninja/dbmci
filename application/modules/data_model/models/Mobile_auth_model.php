<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Mobile_auth_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	Public function add_otp($mobile,$otp){
		$this->db->where("mobile",$mobile);
		$this->db->where("is_social",0);
		$this->db->update("users",array("otp"=>$otp));
	}

	Public function get_user_with_mobile($mobile){
		$this->db->where("mobile",$mobile);
		$this->db->or_where('email', $mobile); 	
		$this->db->where("is_social",0);
		return $this->db->get("users")->row_array();
	}
	Public function get_user_with_email($mobile){
		$this->db->where("email",$email);
		$this->db->where("is_social",0);
		return $this->db->get("users")->row_array();
	}

}	