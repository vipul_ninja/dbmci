<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_share_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}


	public function share_post($data){

		$da['creation_time'] = milliseconds();
		$da['post_type'] = "user_post_type_share";
		$da['is_shared'] = 1;
		$da['parent_id'] = $data['post_id'];
		$da['user_id'] = $data['user_id'];
		/* check if post id already have type iof share */
		$d =  $this->get_basic_post_data($data['post_id']);
		if(count($d) > 0 && $d["post_type"] == "user_post_type_share"){
			$da['parent_id'] = $d['parent_id'];
		}

		$this->db->insert('post_counter', $da);
		$this->db->insert_id();

		if(count($d) > 0 && $d["post_type"] == "user_post_type_share"){
			$data['post_id'] = $d['parent_id'];
		}

		$this->db->where('id',$data['post_id']);
		$this->db->set('share', 'share+1', FALSE);
		$this->db->update("post_counter");
	}

	public function get_basic_post_data($id){
		$this->db->where("id",$id);
		return $this->db->get('post_counter')->row_array();
	}

	public function is_already_share_post($data){

		$this->db->where('parent_id',$data['post_id']);
		$this->db->where('user_id',$data['user_id']);
		$result = $this->db->get("post_counter")->row_array();
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}

	}

}
