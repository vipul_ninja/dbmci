<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class User_queries_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	

	public function submit_query($data){
		
		$data['time'] = milliseconds();

		$this->db->insert('user_queries', $data);
		$this->db->insert_id();	
	} 
	
	

}