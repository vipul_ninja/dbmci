<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Course_instructor_rating_reviews_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function add_instructor_review($info){
        $insert = array(
            "instructor_id"=>$info['instructor_id'],
            "user_id"=>$info['user_id'],
            "rating"=>$info['rating'],
            "text"=>$info['text'],
            "creation_time"=>milliseconds()
        );
        $this->db->insert('course_instructor_rating',$insert);

        /* update review count here */
        $count = $this->db->query('select count(id) as total , TRUNCATE(avg(rating),1) as rating  from course_instructor_rating where instructor_id  ='.$info['instructor_id'])->row_array();
		$review = ($count && $count['total'] > 0 )?$count['total']:0;
        $rating = ($count && $count['rating'] > 0 )?sprintf("%.1f", $count['rating']):0.0;
		$this->db->where('user_id',$info['instructor_id']);
        $array = array('review_count'=>$review ,"rating_count"=>$rating);
        $this->db->update("course_instructor_information",$array);
        return true;
	}

    public function if_review_already_given($instructor_id,$user_id){
        $this->db->where('instructor_id',$instructor_id);
        $this->db->where('user_id',$user_id);
        return $this->db->get('course_instructor_rating')->row_array();
    }

    public function delete_instructor_review($data){
        $this->db->where('instructor_id', $data['instructor_id']);
				$this->db->where('user_id', $data['user_id']);
        $this->db->delete("course_instructor_rating");

        /* update review count here */
        $count = $this->db->query('select count(id) as total , TRUNCATE(avg(rating),1) as rating  from course_instructor_rating where instructor_id  ='.$data['instructor_id'])->row_array();
        $review = ($count && $count['total'] > 0 )?$count['total']:0;
        $rating = ($count && $count['rating'] > 0 )?sprintf("%.1f", $count['rating']):0;
        $this->db->where('user_id',$data['instructor_id']);
        $array = array('review_count'=>$review ,"rating_count"=>$rating);
        $this->db->update("course_instructor_information",$array);
        return true;
    }

    public function edit_instructor_review($info){
        $insert = array(
           "instructor_id"=>$info['instructor_id'],
           "user_id"=>$info['user_id'],
           "rating"=>$info['rating'],
           "text"=>$info['text']
       );
       $this->db->where('instructor_id',$info['instructor_id']);
       $this->db->where('user_id',$info['user_id']);
       $this->db->update('course_instructor_rating',$insert);

       /* update review count here */
       $count = $this->db->query('select count(id) as total , TRUNCATE(avg(rating),1) as rating  from course_instructor_rating where instructor_id  ='.$info['instructor_id'])->row_array();
       $review = ($count && $count['total'] > 0 )?$count['total']:0;
       $rating = ($count && $count['rating'] > 0 )?sprintf("%.1f", $count['rating']):0.0;
       $this->db->where('user_id',$info['instructor_id']);
       $array = array('review_count'=>$review ,"rating_count"=>$rating);
       $this->db->update("course_instructor_information",$array);
       return true;
    }

    public function get_reviews_list($info){
        $where = "";
        if($info['last_review_id'] != "" && $info['last_review_id'] != 0){ $where = " And cir.id < ".$info['last_review_id'] ; }
        $query = $this->db->query(" SELECT cir.* , u.name , u.profile_picture
                                    FROM course_instructor_rating AS cir
                                    Left JOIN users AS u ON u.id = cir.user_id
                                    WHERE cir.instructor_id = '".$info['instructor_id']."'
                                    $where
                                    ORDER BY cir.creation_time DESC
                                    LIMIT 0 , 10
									");
		return $query->result_array();
    }
}
