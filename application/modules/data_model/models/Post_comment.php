<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_comment extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("post_comment_model");
	}
	/***
	 *                   _      _    _____                                           _   
	 *         /\       | |    | |  / ____|                                         | |  
	 *        /  \    __| |  __| | | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_ 
	 *       / /\ \  / _` | / _` | | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
	 *      / ____ \| (_| || (_| | | |____| (_) || | | | | || | | | | ||  __/| | | || |_ 
	 *     /_/    \_\\__,_| \__,_|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
	 *                                                                                   
	 *                                                                                   
	 */
	public function add_comment(){

		$this->validate_add_comment();
		$comment_data['user_id'] = $this->input->post('user_id');
		$comment_data['post_id'] = $this->input->post('post_id');
		$comment_data['comment'] = $this->input->post('comment');
		$comment_data['parent_id'] = $this->input->post('parent_id');
		if(array_key_exists('image', $_POST)){
			$comment_data['image'] = $this->input->post('image');
		}
		/* save and get comment id */
		$comment_id = $this->post_comment_model->add_comment($comment_data);

		$user_id = $this->input->post('user_id');
		$post_id = $this->input->post('post_id');
		
		/* add tagged people */
		$to_tag_people = is_comma_seprated($_POST['tag_people'],True);
		if(is_array($to_tag_people) && count($to_tag_people) > 0 ){
			foreach($to_tag_people as $v ){
				if($v > 0 ){
					/* check if already tagged */
					$this->db->select("id");
					$this->db->where("comment_id",$comment_id);
					$this->db->where("tagged_user_id",$v);
					if(!ISSET($this->db->get("user_comment_tag_people")->row()->id)){
						$this->db->insert("user_comment_tag_people",array('comment_id'=> $comment_id ,
																	"tagged_user_id"=>$v ,
																	"creation_time"=>milliseconds()
																));
						/*	Generate activity log 
						*   HIGHLY SENSITIVE CODE
						*/
						modules::run('data_model/notification_genrator/activity_logger/generate_activity_whlie_people_tagged_on_comment', 
													array( "action_for_user_id"=>$v,"action_element_id"=>$comment_id,"action_performed_by_user_id"=>$this->input->post('user_id'))
												);	
						/*
						* send push notification to tagged user 
						*/
						modules::run('data_model/pusher/push_tag_on_comment/push_on_comment_tag', 
									array( "comment_id"=>$comment_id,"user_id"=>$this->input->post('user_id'),"tagged_user_id"=>$v)
								);
					}	
				}	
			}
		}
		

		
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
									array( "post_id"=>$post_id,"user_id"=>$user_id)
								);

		/*	Generate activity log 
		*   HIGHLY SENSITIVE CODE
		*/
		modules::run('data_model/notification_genrator/activity_logger/generate_activity_whlie_comment_on_post', 
									array( "action_for_user_id"=>$post_data['user_id'],"action_element_id"=>$post_id,"action_performed_by_user_id"=>$user_id)
								);		
		/* send push */
		modules::run('data_model/pusher/push_post_comment/push_on_comment', 
									array( "post_id"=>$post_id,"user_id"=>$user_id)
								);

		activity_rewards($this->input->post('user_id'),'COMMENT_ADD',"+");								
		return_data(true,'Comment added.',$post_data);
	}

	private function validate_add_comment(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->set_rules('comment','comment', 'trim|required');

		/* parent comment id  if key not found */
		if(!array_key_exists('parent_id', $_POST) ){ $_POST['parent_id'] = 0 ; }

		/* people tagging if key not found */
		if(!array_key_exists('tag_people', $_POST) ){ $_POST['tag_people'] = ""; }
        /* if found */
		if($_POST['tag_people'] != "" && is_comma_seprated($_POST['tag_people']) === false){
			return_data('false',"Please provide valid input for tagging people.");
		}
		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *      _    _             _         _           _____                                           _   
 *     | |  | |           | |       | |         / ____|                                         | |  
 *     | |  | | _ __    __| |  __ _ | |_  ___  | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_ 
 *     | |  | || '_ \  / _` | / _` || __|/ _ \ | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
 *     | |__| || |_) || (_| || (_| || |_|  __/ | |____| (_) || | | | | || | | | | ||  __/| | | || |_ 
 *      \____/ | .__/  \__,_| \__,_| \__|\___|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
 *             | |                                                                                   
 *             |_|                                                                                   
 */
	public function update_comment(){

		$this->validate_update_comment();
		$comment_data['id'] = $this->input->post('id');
		$comment_data['user_id'] = $this->input->post('user_id');
		$comment_data['post_id'] = $this->input->post('post_id');
		$comment_data['comment'] = $this->input->post('comment');
		if(array_key_exists('image', $_POST)){
			$comment_data['image'] = $this->input->post('image');
		}
		/* save and get comment id */
		$this->post_comment_model->update_comment($comment_data);
		$comment_id = $this->input->post('id');
		/* add tagged people */
		$to_tag_people = is_comma_seprated($_POST['tag_people'],True);
		if(is_array($to_tag_people) && count($to_tag_people) > 0 ){
			foreach($to_tag_people as $v ){
				if($v > 0 ){
					/* check if already tagged */
					$this->db->select("id");
					$this->db->where("comment_id",$comment_id);
					$this->db->where("tagged_user_id",$v);
					if(!ISSET($this->db->get("user_comment_tag_people")->row()->id)){
						$this->db->insert("user_comment_tag_people",array('comment_id'=> $comment_id ,
																	"tagged_user_id"=>$v ,
																	"creation_time"=>milliseconds()
																));
						/*	Generate activity log 
						*   HIGHLY SENSITIVE CODE
						*/
						modules::run('data_model/notification_genrator/activity_logger/generate_activity_whlie_people_tagged_on_comment', 
													array( "action_for_user_id"=>$v,"action_element_id"=>$comment_id,"action_performed_by_user_id"=>$this->input->post('user_id'))
												);	
						/*
						* send push notification to tagged user 
						*/
						modules::run('data_model/pusher/push_tag_on_comment/push_on_comment_tag', 
									array( "comment_id"=>$comment_id,"user_id"=>$this->input->post('user_id'),"tagged_user_id"=>$v)
								);

					}	
				}	
			}
		}
		
		/* remove tagged people */
		$remove_tag_people = is_comma_seprated($_POST['remove_tag_people'],True);
		if(is_array($remove_tag_people) && count($remove_tag_people) > 0 ){
			foreach($remove_tag_people as $v ){
				if($v > 0 ){
					$this->db->where('comment_id',$comment_id);
					$this->db->where('tagged_user_id', $v);
  					$this->db->delete('user_comment_tag_people');
				}	
			}
		}	

		$comment = $this->post_comment_model->get_single_comment_data($comment_id); 
		$comment['tagged_people'] = $this->post_comment_model->get_tag_people_in_comment($comment['id']);

		return_data(true,'Comment updated.',$comment);
	}

	private function validate_update_comment(){
		
		post_check();
		
		$this->form_validation->set_rules('id','id', 'trim|required');
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->set_rules('comment','comment', 'trim|required');
		
		/* people tagging if key not found */
		if(!array_key_exists('tag_people', $_POST) ){ $_POST['tag_people'] = ""; }
        /* if found */
		if($_POST['tag_people'] != "" && is_comma_seprated($_POST['tag_people']) === false){
			return_data('false',"Please provide valid input for tagging people.");
		}
		
		/* people tagging if key not found */
		if(!array_key_exists('remove_tag_people', $_POST) ){ $_POST['remove_tag_people'] = ""; }
        /* if found */
		if($_POST['remove_tag_people'] != "" && is_comma_seprated($_POST['remove_tag_people']) === false){
			return_data('false',"Please provide valid input for removing tagging people.");
		}		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *      _____         _        _           _____                                           _   
 *     |  __ \       | |      | |         / ____|                                         | |  
 *     | |  | |  ___ | |  ___ | |_  ___  | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_ 
 *     | |  | | / _ \| | / _ \| __|/ _ \ | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
 *     | |__| ||  __/| ||  __/| |_|  __/ | |____| (_) || | | | | || | | | | ||  __/| | | || |_ 
 *     |_____/  \___||_| \___| \__|\___|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
 *                                                                                             
 *                                                                                             
 */	
	
	public function delete_comment(){

		$this->validate_delete_comment();
		$this->post_comment_model->delete_comment($this->input->post());
		activity_rewards($this->input->post('user_id'),'COMMENT_ADD',"-");
		return_data(true,'Comment deleted.');
	}

	private function validate_delete_comment(){
		
		post_check();
		
		$this->form_validation->set_rules('id','id', 'trim|required');
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *       _____                                           _     _       _       _   
 *      / ____|                                         | |   | |     (_)     | |  
 *     | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_  | |      _  ___ | |_ 
 *     | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __| | |     | |/ __|| __|
 *     | |____| (_) || | | | | || | | | | ||  __/| | | || |_  | |____ | |\__ \| |_ 
 *      \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__| |______||_||___/ \__|
 *                                                                                 
 *                                                                                 
 */	
	
	public function get_post_comment(){

		$this->validate_get_post_comment();
		/* for pagination purpose */
		if(!array_key_exists('last_comment_id', $_POST)){
			$last_comment_id = "";
		}else{
			$last_comment_id = $this->input->post('last_comment_id');
		}
		
		$info =  array(
						"post_id" =>$this->input->post('post_id'),
						'last_comment_id'=>$last_comment_id,
						'parent_id'=>$this->input->post('parent_id')
					);

		$comments = $this->post_comment_model->get_post_comment($info);
		
		if(count($comments) > 0 ){
			/* get people inn the tags */
			$returning = array();
			foreach($comments as $c){
				$c['tagged_people'] = $this->post_comment_model->get_tag_people_in_comment($c['id']);
				$returning[] = $c;
			}
			
			return_data(true,'Comment details.',$returning);	
		}else{
			return_data(false,'No comment found.');
		}
		
	}

	private function validate_get_post_comment(){
		
		post_check();
		
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		/* parent comment id  if key not found */
		if(!array_key_exists('parent_id', $_POST) ){ $_POST['parent_id'] = 0 ; }

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/***
	 *       _____  _                _          _____                                           _   
	 *      / ____|(_)              | |        / ____|                                         | |  
	 *     | (___   _  _ __    __ _ | |  ___  | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_ 
	 *      \___ \ | || '_ \  / _` || | / _ \ | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
	 *      ____) || || | | || (_| || ||  __/ | |____| (_) || | | | | || | | | | ||  __/| | | || |_ 
	 *     |_____/ |_||_| |_| \__, ||_| \___|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
	 *                         __/ |                                                                
	 *                        |___/                                                                 
	 */   

    public function get_single_comment_data(){
    	$this->validate_get_single_comment_data();

    	$comment = $this->post_comment_model->get_single_comment_data($this->input->post('comment_id'));

    	if(count($comment) > 0 ){
			/* get people inn the tags */
			$comment['tagged_people'] = $this->post_comment_model->get_tag_people_in_comment($comment['id']);
			return_data(true,'Comment details.',$comment);	
		}else{
			return_data(false,'No comment found.');
		}

    }

    private function validate_get_single_comment_data(){
    	post_check();
    	$this->form_validation->set_rules('comment_id','comment_id', 'trim|required');		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
    }



}