<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class People_you_may_know_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	public function get_list($user_id){
		$rand =  rand(1498634128235 , milliseconds());
		$query = $this->db->query(" SELECT u.id, u.name, u.profile_picture 
									FROM users AS u 
									JOIN user_registration_data AS urd ON u.id = urd.user_id 
									JOIN user_registration_data AS urdv ON $user_id = urdv.user_id 
									LEFT JOIN user_follow as uf on $user_id = uf.follower_id and u.id = uf.user_id 
									WHERE u.is_course_register = 1 
									AND u.id != $user_id 
									AND urd.master_id = urdv.master_id
									AND uf.id IS NULL
									AND u.creation_time < $rand
									order by u.id desc LIMIT 0 , 20 ");							 			
		return $query->result_array();
	}
}	