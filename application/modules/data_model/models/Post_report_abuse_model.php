<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_report_abuse_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	public function report($data){
		
		$data['creation_time'] = milliseconds();
		$this->db->insert('user_post_report', $data);
		$this->db->insert_id();

		$this->db->where('id',$data['post_id']);
		$this->db->set('report_abuse', 'report_abuse+1', FALSE);
		$this->db->update("post_counter");
	}

	public function is_already_reported($data){
		$this->db->where("user_id",$data['user_id']);
		$this->db->where("post_id",$data['post_id']);
		$result =  $this->db->get('user_post_report')->row_array();
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function report_reasons($lang){		
		$reason = " reason";
		if($lang == 2 ){
			$reason = ' reason_2 as reason ';
		}
		$result =  $this->db->select("id,$reason")->get('user_post_report_reasons')->result_array();
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/* public function remove_from_bookmarks($data){
		$this->db->where("user_id",$data['user_id']);
		$this->db->where("post_id",$data['post_id']);
		$this->db->delete("user_post_bookmark");		
	}*/

}