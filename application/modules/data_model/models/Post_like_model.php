<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_like_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	

	public function like_post($data){
		
		$data['creation_time'] = milliseconds();

		$this->db->insert('user_post_like', $data);
		$this->db->insert_id();

		$count = $this->db->query('select count(id) as total from user_post_like where post_id ='.$data['post_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;		

		$this->db->where('id',$data['post_id']);
		$this->db->set('likes', $count);
		$this->db->update("post_counter");
	}


	public function dislike_post($data){
		
		$this->db->where('user_id', $data['user_id']);
		$this->db->where('post_id', $data['post_id']);
		$this->db->delete("user_post_like");

		$count = $this->db->query('select count(id) as total from user_post_like where post_id ='.$data['post_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['post_id']);
		$this->db->set('likes', $count);
		$this->db->update("post_counter");
	}

	public function is_already_like_post($data){
		$this->db->where('post_id',$data['post_id']);
		$this->db->where('user_id',$data['user_id']);
		$result = $this->db->get("user_post_like")->row_array();
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function user_liked_list($post_id , $last_id){
		$where = "";
		if($last_id >  0 ){
			$where .= " And upl.id > $last_id"; 
		}
		return  $this->db->query("select upl.* , 
										u.profile_picture , u.name 
										from user_post_like as upl 
										left join users as u on u.id = upl.user_id 
										where upl.post_id = $post_id $where limit 0,10")->result_array();
	}


	public function user_liked_list_with_watcher($post_id , $last_id , $is_watcher){
		$where = "";
		if($last_id >  0 ){
			$where .= " And upl.id > $last_id"; 
		}
		return  $this->db->query("select upl.* , u.profile_picture , u.name , 
      									 (CASE WHEN uf.id IS NULL THEN  0 ELSE 1 END) as is_follow
										from user_post_like as upl 
										left join users as u on u.id = upl.user_id 
										left join user_follow as uf on uf.follower_id = $is_watcher and uf.user_id = u.id 
										where upl.post_id = $post_id $where limit 0,10")->result_array();

	}
}