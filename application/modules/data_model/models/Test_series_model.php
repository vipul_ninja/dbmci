<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Test_series_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

    public function get_test_series($id){
        $this->db->where('id',$id);
        return $return =  $this->db->get('course_test_series_master')->row_array();
    }

    public function get_test_series_question($id){
        $sql = "SELECT cqbm.*,csm.name as subject_name
								FROM course_testseries_question_relation as  ctqr
								left join course_question_bank_master as cqbm on cqbm.config_id = ctqr.question_id
								left join course_subject_master as csm on cqbm.subject_id = csm.id
								where ctqr.test_series_id = $id and cqbm.lang_code = 1
								";
        return $query = $this->db->query($sql)->result();
    }

	public function save_test_series($data){
		$save = array(
			"test_series_id"=>$data['test_series_id'],
			"question_dump"=>$data['question_dump'],
			"user_id"=>$data['user_id'],
			"time_spent"=>$data['time_spent'],
			"creation_time"=> milliseconds()
		);
		$this->db->insert('course_test_series_report',$save);
		return $this->db->insert_id();
	}

	public function get_questions_answer($list){
		$sql = "SELECT id , option_1 , option_2 , option_3 , option_4 , option_5 ,  answer
		FROM course_question_bank_master
		where id in ($list)
		";
		return $query = $this->db->query($sql)->result();
	}

	/* get test series details for marks calculations */
	public function test_series_details($test_series_id){
		$sql = "SELECT id ,  total_marks,marks_per_question,negative_marking,reward_points,time_in_mins,total_questions
		FROM course_test_series_master
		where id = $test_series_id
		";
		return $query = $this->db->query($sql)->row();
	}
	/* get user test rank */
	public function user_test_rank($test_series_id,$user_id){
		$sql = "SELECT id, marks, FIND_IN_SET( marks, ( SELECT GROUP_CONCAT( marks ORDER BY marks DESC )
														FROM course_test_series_report
														WHERE test_series_id = $test_series_id AND first_attempt = 1 )
											) AS rank
				FROM course_test_series_report
				WHERE test_series_id = $test_series_id AND  first_attempt = 1 and user_id = $user_id
		";
		$query = $this->db->query($sql)->row();
		return (isset($query->rank))?$query->rank:"";
	}

	/* check user's test attempt */
	public function check_attempt($user_id,$test_series_id){
        $this->db->where('user_id',$user_id);
		$this->db->where('test_series_id',$test_series_id);
		$this->db->where('first_attempt',1);
        return $return =  $this->db->get('course_test_series_report')->row_array();
    }

	public function save_result($data){
		$this->db->where("id",$data['id']);
		$this->db->update('course_test_series_report',$data);
	}

	/* get user's reward_points from here */

	public function get_user_reward_points($user_id){
		$sql = "SELECT id,reward_points
				FROM users
				where id = $user_id
		";
		return $query = $this->db->query($sql)->row();
	}

	public function test_series_metainformation($id,$user_id){
		$sql = "SELECT ctsr.* , ctsm.test_series_name  , ctsm.test_type ,ctsm.skip_rank
				FROM course_test_series_report as ctsr
                Join course_test_series_master as ctsm on ctsm.id = ctsr.test_series_id
				where ctsr.id = $id and ctsr.user_id =   $user_id
		";

		return $query = $this->db->query($sql)->row();
	}

	public function total_user_test_attempt($test_id){
		$sql = "select count(user_id) as total from (SELECT Distinct user_id
		FROM course_test_series_report
		where test_series_id = $test_id ) as record
		";
		return $this->db->query($sql)->row()->total;
	}

	public function question_report_of_segment($user,$test_segment_id){
		/* get question dump with result_id from result row */
		$this->db->select('question_dump');
		$this->db->where('user_id',$user);
		$this->db->where('id',$test_segment_id);

		$db_out = $this->db->get('course_test_series_report')->row();
		$attempt_chunk = json_decode($db_out->question_dump);

		$qid = array();
		foreach($attempt_chunk as $ac){
			$qid[] = $ac->question_id;
		}
		$qid =  implode(',',$qid);
		/* get actual question array */
		$sql="SELECT `id`, `question`, `description`, `question_type`,
					  `option_1`, `option_2`, `option_3`, `option_4`, `option_5`, `answer` , total_attempt , total_right ,total_wrong 
				 FROM `course_question_bank_master` where id in ($qid)";
		$actual_question = $this->db->query($sql)->result();
		$final_out = array();
		foreach($attempt_chunk as $ac){

			foreach($actual_question as $aq){

				if($aq->id == $ac->question_id){
					//$aq->is_correct =  $ac->is_correct;
					$aq->user_answer =  $ac->answer;
					
					$temp =  $aq;
				}
			}
			$final_out[] = $temp;
		}

		return $final_out ;
	}
	public function top_ten_list($test_series_id){
		$sql="SELECT ctsr.user_id , ctsr.creation_time , u.name , u.profile_picture FROM `course_test_series_report` as  ctsr
		Join users as u on u.id= ctsr.user_id
        WHERE ctsr.`test_series_id` = $test_series_id and ctsr.`first_attempt`= 1
		group by ctsr.user_id
		order by ctsr.marks desc , ctsr.id limit 0,10";
		return $this->db->query($sql)->result_array();
	}

	public function top_list($test_series_id){
		$sql="SELECT ctsr.user_id , ctsr.creation_time , u.name , u.profile_picture FROM `course_test_series_report` as  ctsr
		Join users as u on u.id= ctsr.user_id
        WHERE ctsr.`test_series_id` = $test_series_id and ctsr.`first_attempt`= 1
		group by ctsr.user_id
		order by ctsr.marks desc , ctsr.id limit 0,100";
		return $this->db->query($sql)->result_array();
	}	
}
