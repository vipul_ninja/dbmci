<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_text_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	

	public function save_user_text_post($data){
		
		$creation_time = milliseconds();
		$data_post_counter['creation_time'] = $creation_time;
		$data_post_counter['user_id'] = $data['user_id'];
		$data_post_counter['post_type'] = "user_post_type_normal";
		$data_post_counter['post_tag'] = $data['post_tag'];
		$data_post_counter['location'] = $data['location'];
		$data_post_counter['lat'] = $data['lat'];
		$data_post_counter['lon'] = $data['lon'];


		$this->db->insert('post_counter', $data_post_counter);
		$record['text'] = (array_key_exists('text', $data))?$data['text']:"";
		$record['post_id'] = $this->db->insert_id();
		$record['post_text_type'] = $data['post_type'];

		$this->db->insert('user_post_type_text', $record);

		if($data['post_type'] == "video"){
			$insert['post_id'] = $record['post_id'];
			$insert['file_type'] = "video";
			$insert['link'] = $data['video_link'];

			$this->db->insert('post_file_meta', $insert);
		}
		
		//increase user post counter 
		$this->db->where('id',$data_post_counter['user_id']);
		$this->db->set('post_count', 'post_count+1', FALSE);
		$this->db->update("users");
		
		return $record['post_id'];
	}

	public function edit_user_text_post($data){

		$record['text'] = (array_key_exists('text', $data))?$data['text']:"";
		$record['post_text_type'] = $data['post_type'];
		
		$this->db->where('post_id',$data['post_id']);
		if($this->db->get('user_post_type_text')->row_array()){
			$this->db->where('post_id',$data['post_id']);
			$this->db->update('user_post_type_text', $record);
		}else{
			$record['post_id'] = $data['post_id'];
			$this->db->insert('user_post_type_text', $record);
		}
	}
	
	public function save_sub_cate_text_post($post_id,$sub_cate){
		$sub_cate = explode(',',$sub_cate);
		foreach($sub_cate as $sb){
			$item = array();
			$item['post_id']= $post_id;
			$item['sub_cate_id']= $sb;
			$this->db->insert('post_category_relationship', $item);
		}
	}
}