<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class User_reward_model extends CI_Model {

	function __construct() {
		parent::__construct();
    }
    
    public function get_user_rewards($id){
       return  $this->db->select('id,reward_points')->where('id',$id)->get('users')->row();

    }
	
	public function get_reward_transaction($id,$last_id){
        $this->db->select('id,round(reward) as reward , replace(area,"_"," ") as area , creation_time');
        $this->db->where('user_id',$id);
        if($last_id != ""){
            $this->db->where('id < ',$last_id);
        }
        $this->db->limit(10,0);
        $this->db->order_by('id','desc');
        return  $this->db->get('user_activity_rewards')->result_array();

    }

}
