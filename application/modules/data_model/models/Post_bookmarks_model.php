<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_bookmarks_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	public function add_to_bookmarks($data){
		
		$data['time'] = milliseconds();
		$this->db->insert('user_post_bookmark', $data);
		$this->db->insert_id();
	}

	public function is_already_bookmarked($data){
		$this->db->where("user_id",$data['user_id']);
		$this->db->where("post_id",$data['post_id']);
		$this->db->where("area",$data['area']);
		$result =  $this->db->get('user_post_bookmark')->row_array();
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function remove_from_bookmarks($data){
		$this->db->where("user_id",$data['user_id']);
		$this->db->where("post_id",$data['post_id']);
		$this->db->where("area",$data['area']);
		$this->db->delete("user_post_bookmark");		
	}

}