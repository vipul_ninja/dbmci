<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Fan_wall_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function request_user_details_by_id($id){
		$query = $this->db->query("SELECT users.id,users.dams_tokken , users.is_live_agent
									FROM users
									WHERE id = ".$id."
									");
		return $query->row_array();
	}


	public function all_posts($data){
		$where = "";
		//pre($data);die;
		if($data['last_post_id'] != ""){ $where = " And pc.id < ".$data['last_post_id'] ; }
		if($data['tag_id'] != ""){ $where .= " And pc.post_tag = ".$data['tag_id'] ; }
		/* if request is coming for expert only */
		if($data['get_expert_post'] == 1){
			// append query for if want post whom you follow join user_follow as uf on uf.user_id=  pc.user_id and uf.follower_id = 9
			$where .= " And u.is_expert = 1 " ;
		}

		if($data['get_mentor_post'] == 1){
			// append query for if want post whom you follow join user_follow as uf on uf.user_id=  pc.user_id and uf.follower_id = 9
			$where .= " And u.is_mentor = 1 " ;
		}

		$follow_join="";
		if($data['get_follower_post'] == 1 || $data['get_expert_post'] == 1  ){

			$follow_join = " join user_follow as uf on uf.user_id=  pc.user_id and uf.follower_id = ".$data['user_id']." "  ;
		}
		$joins_search_table = "";
		if($data['search_text'] != ""){
			$where .= " And (  u.name like '%".$data['search_text']."%' or uptt.text LIKE '%".$data['search_text']."%' or uptm.question LIKE '%".$data['search_text']."%') ";

			$joins_search_table = " LEFT JOIN user_post_type_text as uptt on pc.id = uptt.post_id LEFT JOIN user_post_type_mcq  as uptm on pc.id = uptm.post_id ";
		}


		$condtion_special = "";

		/* condition search of type of post */
		if($data['post_searching'] != "" && $data['post_searching'] !="user_post_type_review" ){
			$where .= " And pc.post_type = '".$data['post_searching']."' ";
		}elseif( $data['post_searching'] =="user_post_type_review"){
			$where .= " And u.is_mentor = '1' ";
		}

		if($data['subject_id']> 0 ){

			$where .= 'And psr.subject_id = '.$data['subject_id'];
		}

		/* language dependency parameter */
		if(API_REQUEST_LANG == 2 ){
			$tag_name = " vcsnm.name_2";
		}else{
			$tag_name = " vcsnm.name ";
		}

		$query = $this->db->query(" SELECT distinct(pc.id) as id , pc.user_id, pc.post_type,  pc.display_picture, pc.post_headline ,
									u.name as post_owner_name , u.profile_picture as post_owner_profile_picture , u.is_expert , u.is_mentor , 
									(SELECT group_concat(distinct($tag_name ) SEPARATOR ', ') as name FROM post_category_relationship as pcr join course_stream_name_master as csnm on pcr.sub_cate_id = csnm.id join course_stream_name_master as vcsnm on vcsnm.id = csnm.parent_id WHERE pcr.post_id = pc.id ) as post_tag , 
								  	pc.is_shared, pc.parent_id, pc.likes, pc.comments, pc.share,
								  	pc.report_abuse, pc.pinned_post, pc.creation_time ,pc.location ,pc.lat ,pc.lon
									FROM post_counter AS pc
									JOIN users AS u ON u.id = pc.user_id
									LEFT JOIN post_category_relationship AS pcr on pc.id=pcr.post_id
									left join user_preferences as upr on upr.cat_id =  pcr.sub_cate_id and upr.user_id = ".$data['user_id']."
									$joins_search_table
									$follow_join
									left join user_post_hidden as uph on uph.post_id = pc.id and uph.user_id = ".$data['user_id']."
									left join post_subject_relationship psr on psr.post_id = pc.id 
									WHERE
									uph.id is null
									and pc.status = 0
									and pc.is_shared = 0
									$condtion_special
									$where
									ORDER BY pc.creation_time DESC
									LIMIT 0 , 30
									");

		return $query->result_array(); 
	}
	public function top_pinned_post($data){
		$where = "";

		$u_data = $this->db->query("select dams_tokken , is_live_agent   from users where id = '".$data['user_id']."' limit 0,1 ")->row_array();
		// if any person have is_live_agent == 2 than he can see post of is_live_agent == 1
		if(count($u_data) > 0 && $u_data['is_live_agent'] != 2  ){
			if(count($u_data) > 0 && $u_data['dams_tokken'] == "" ){
				$where .= " And u.is_live_agent != 1 ";
			}
		}
		/* show only post which connect with user stream */
		$condtion_special  = " and urd.master_id = virtual_user.master_id ";
		if($data['get_only_feed'] > 0 ){
			$condtion_special  = " and urd.master_id = ".$data['get_only_feed']." ";
		}
		$query = $this->db->query(" SELECT pc.id , pc.user_id, pc.post_type, pc.display_picture,pc.post_headline ,
									u.name as post_owner_name , u.profile_picture as post_owner_profile_picture ,
									urd.optional_text , mclt.text as speciality ,
									CASE
								    WHEN post_tags.text IS NULL THEN ''
								    ELSE post_tags.text
								  	END AS post_tag ,
								    CASE  WHEN 	post_tags.id IS NULL THEN '' ELSE post_tags.id  END as post_tag_id  ,
								  	pc.is_shared, pc.parent_id, pc.likes, pc.comments, pc.share,
								  	pc.report_abuse, pc.pinned_post, pc.creation_time ,pc.location ,pc.lat ,pc.lon
									FROM post_counter AS pc
							    	left JOIN user_registration_data AS urd ON pc.user_id = urd.user_id
									LEFT JOIN post_tags ON pc.post_tag = post_tags.id
									JOIN users AS u ON u.id = pc.user_id
									left JOIN master_category_level_two as mclt on urd.master_id_level_two = mclt.id
									LEFT JOIN user_registration_data as virtual_user on virtual_user.user_id = ".$data['user_id']."
									WHERE pc.status = 0
									and pc.is_shared = 0
									and (pc.pinned_post !='' or pc.pinned_post ='')
									$condtion_special
									$where
									ORDER BY pc.pinned_post DESC
									LIMIT 0 , 5
									");
		return $query->result_array();
	}


	public function all_bookmaked($data){
		$where = $join = "";
		if($data['last_post_id'] != ""){ $where = " And upb.id < ".$data['last_post_id'] ; }
		if($data['search_text'] != ""){
			//$where .= " And ( post_tags.text like '%".$data['search_text']."%' or u.name like '%".$data['search_text']."%') "  ;
		}

		$post_requester = false;

		if($data['filter_type'] == 1 ){
			$where .= " and  upb.area = 1  and pc.post_type = 'user_post_type_mcq' ";
			$post_requester = true;
		}
		if($data['filter_type'] == 2 ){
			$where .= " and  upb.area = 1  and pc.post_type = 'user_post_type_quiz' ";
			$post_requester = true;
		}
		if($data['filter_type'] == 3 ){
			$where .= " and  upb.area = 1  and pc.post_type = 'user_post_type_video' ";
			$post_requester = true;
		}
		
		if($data['filter_type'] == 4 ){
			$where .= " and  upb.area = 1  and pc.post_type = 'user_post_type_article' ";
			$post_requester = true;
		}

		if($data['filter_type'] == 5 ){
			$where .= " and  upb.area = 2 ";
		}

		if($data['filter_type'] == 6 ){
			$where .= " and  pc.post_type IN ('user_post_type_vocab','user_post_type_normal','user_post_type_current_affair')";
			$post_requester = true;
		}

		if($post_requester == true){
			$join  .= " join post_counter as pc on upb.post_id = pc.id ";
		}

		$query = $this->db->query("SELECT upb.*
							FROM user_post_bookmark as upb 
							$join
							where upb.user_id = ".$data['user_id']."
							$where
							ORDER BY upb.time DESC
							LIMIT 0 , 30
							");
		return $query->result_array();
	}

	public function all_user_post($data){
		$where = "";
		if($data['last_post_id'] != ""){ $where = " And pc.id < ".$data['last_post_id'] ; }
		$query = $this->db->query("SELECT pc.id , pc.user_id, pc.post_type, pc.display_picture,pc.post_headline ,
									u.name as post_owner_name , u.profile_picture as post_owner_profile_picture ,u.is_expert ,
									u.is_mentor ,
									CASE
								    WHEN post_tags.text IS NULL THEN ''
								    ELSE post_tags.text
								  	END AS post_tag ,
									CASE  WHEN 	post_tags.id IS NULL THEN '' ELSE post_tags.id  END as post_tag_id  ,
								  	pc.is_shared, pc.parent_id, pc.likes, pc.comments, pc.share,
								  	pc.report_abuse, pc.pinned_post, pc.creation_time ,pc.location ,pc.lat ,pc.lon
									FROM post_counter AS pc
									LEFT JOIN post_tags ON pc.post_tag = post_tags.id
									JOIN users AS u ON u.id = pc.user_id
									WHERE pc.status = 0
									and pc.is_shared = 0
									and pc.user_id = ".$data['user_id']."
									$where
									ORDER BY pc.creation_time DESC
									LIMIT 0 , 30
									");
			
		return $query->result_array();
	}

	public function get_user_post_type_mcq($id){
		$this->db->select(" *, (CASE WHEN right_answer ='answer_one' THEN 1  WHEN right_answer ='answer_two' THEN 2  WHEN right_answer ='answer_three' THEN 3  WHEN right_answer ='answer_four' THEN 4  WHEN right_answer ='answer_five' THEN 5 ELSE ' ' END) as right_answer ");
		
		$this->db->where("post_id",$id);
		$this->db->limit(1,0);
		return $this->db->get('user_post_type_mcq')->row_array();
	}

	public function get_user_post_type_mcq_multiple($ids){
		$this->db->select(" *, (CASE WHEN right_answer ='answer_one' THEN 1  WHEN right_answer ='answer_two' THEN 2  WHEN right_answer ='answer_three' THEN 3  WHEN right_answer ='answer_four' THEN 4  WHEN right_answer ='answer_five' THEN 5 ELSE ' ' END) as right_answer ");
		$this->db->where_in("post_id",$ids);
		return $this->db->get('user_post_type_mcq')->result_array();
	}

	public function get_basic_post_data($id){
				$query = $this->db->query(" SELECT pc.id, pc.user_id, pc.post_type, pc.display_picture,pc.post_headline , CASE
						    WHEN post_tags.text IS NULL THEN ''
						    ELSE post_tags.text
						  	END AS post_tag ,
							CASE  WHEN 	post_tags.id IS NULL THEN '' ELSE post_tags.id  END as post_tag_id  , 
						   	pc.is_shared, pc.parent_id, pc.likes, pc.comments, pc.share, pc.report_abuse, pc.pinned_post, pc.creation_time ,
								pc.location ,pc.lat ,pc.lon
									FROM post_counter AS pc
									LEFT JOIN post_tags ON pc.post_tag = post_tags.id
									WHERE pc.status = 0
									and pc.id = '$id'
									ORDER BY pc.creation_time DESC
									LIMIT 0 ,1
									");
		return $query->row_array();

	}

	public function get_user_post_type_normal($id){
		$this->db->where("post_id",$id);
		$this->db->limit(1,0);
		return $this->db->get('user_post_type_text')->row_array();
	}

	public function get_user_post_type_normal_multiple($ids){
		$this->db->where_in("post_id",$ids);
		return $this->db->get('user_post_type_text')->result_array();
	}

	public function get_user_post_file_meta($post_id){
		$this->db->where("post_id",$post_id);
		return $this->db->get('post_file_meta')->result_array();
	}

	public function get_post_owner_info($user_id){
		$this->db->select(array("id","name","profile_picture","is_expert","is_mentor"));
		$this->db->where("id",$user_id);
		$this->db->limit(1,0);
		$data =  $this->db->get("users")->row_array();
		return $data;
	}

	public function is_liked_post($post_id,$user_id){
		$this->db->where("post_id",$post_id);
		$this->db->where("user_id",$user_id);
		$this->db->limit(1,0);
		$result = $this->db->get("user_post_like")->row_array();
		if(count($result)> 0 ){
			return true;
		}
		return false;
	}

	public function is_liked_post_multiple($ids,$request_user_id){
		$this->db->select('group_concat(post_id) as post_id');
		$this->db->where_in('post_id', $ids);
		$this->db->where("user_id",$request_user_id);
		return $this->db->get("user_post_like")->row()->post_id;
	}

	public function users_followers_check($user_id,$follower_id){
		$this->db->select('group_concat(user_id) as user_id');
		//$this->db->where_in('user_id', $user_id);
		$this->db->where("follower_id",$follower_id);
		return $this->db->get("user_follow")->row()->user_id;
	}

	public function is_bookmarked_post($post_id,$user_id){
		$this->db->where("user_id",$user_id);
		$this->db->where("post_id",$post_id);
		$this->db->limit(1,0);
		$result =  $this->db->get('user_post_bookmark')->row_array();
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function is_bookmarked_post_multiple($ids,$request_user_id){
		$this->db->select('group_concat(post_id) as post_id');
		$this->db->where_in('post_id', $ids);
		$this->db->where("user_id",$request_user_id);
		return $this->db->get("user_post_bookmark")->row()->post_id;
	}

	public function get_info_of_shared_post_owner($post_id){

		$query = $this->db->query(" SELECT u.id, u.name, u.profile_picture
									FROM post_counter AS pc
									JOIN users AS u ON pc.user_id = u.id
									WHERE pc.id = $post_id"
							);
		return $query->row_array();
	}

	public function get_user_answer_for_mcq($data){
		$this->db->select("answer");
		$this->db->where("user_id",$data['user_id']);
		$this->db->where("mcq_id",$data['mcq_id']);
		if($data = $this->db->get('user_post_type_mcq_answers')->row_array()){
			return $data['answer'];
		}
		return "";
	}

    public function get_post_tagged_people($post_id){
		$query = $this->db->query(" SELECT u.id, u.name, u.profile_picture
									FROM user_post_tag_people AS uptp
									JOIN users AS u ON uptp.tagged_user_id = u.id
									WHERE uptp.post_id = $post_id"
							);
		return $query->result_array();
	}

	// calculate mcq answer percentage
	public function calculate_percentage_with_mcq_pk($mcq_id){
		$query = "SELECT answer , count(answer) as t_answer  FROM `user_post_type_mcq_answers` WHERE mcq_id = $mcq_id group by answer";

		$data = $this->db->query($query)->result_array();
		$array = array("answer_one"=>"0","answer_two"=>"0","answer_three"=>"0","answer_four"=>"0","answer_five"=>"0");
		$total_answer = 0;
		if(count($data) > 0 ){
			foreach($data as $d){
				if($d['answer'] == 1 ){
					$array['answer_one'] = $d['t_answer'];
					$total_answer = $total_answer + $d['t_answer'];
				}
				if($d['answer'] == 2 ){
					$array['answer_two'] = $d['t_answer'];
					$total_answer = $total_answer + $d['t_answer'];
				}
				if($d['answer'] == 3 ){
					$array['answer_three'] = $d['t_answer'];
					$total_answer = $total_answer + $d['t_answer'];
				}
				if($d['answer'] == 4 ){
					$array['answer_four'] = $d['t_answer'];
					$total_answer = $total_answer + $d['t_answer'];
				}
				if($d['answer'] == 5 ){
					$array['answer_five'] = $d['t_answer'];
					$total_answer = $total_answer + $d['t_answer'];
				}
			}

			$array['answer_one'] = round(($array['answer_one']/$total_answer)*100, 2) ;

			$array['answer_one'] = (string)floor($array['answer_one']);

			$array['answer_two'] = round(($array['answer_two']/$total_answer)*100, 2) ;
			$array['answer_two'] = (string)floor($array['answer_two']);

			$array['answer_three'] = round(($array['answer_three']/$total_answer)*100, 2) ;
			$array['answer_three'] = (string)floor($array['answer_three']);

			$array['answer_four'] = round(($array['answer_four']/$total_answer)*100, 2) ;
			$array['answer_four'] = (string)floor($array['answer_four']);

			$array['answer_five'] = round(($array['answer_five']/$total_answer)*100, 2) ;
			$array['answer_five'] = (string)floor($array['answer_five']);
			return $array;
		}
		return $array;
	}

	function get_suggested_post($keyword){

		$this->db->select('pc.id as post_id');
		$this->db->join('user_post_type_text as uptt',  'pc.id = uptt.post_id','left');
		$this->db->join('user_post_type_mcq as uptm',  'pc.id = uptm.post_id','left');
		$this->db->where('pc.status','0');
		$this->db->where("(( MATCH(post_headline) AGAINST ('$keyword')) or ( MATCH(uptt.text) AGAINST ('$keyword')) or ( MATCH(uptm.question) AGAINST ('$keyword')))");
		$this->db->limit('20');
		$suggested_post=$this->db->get('post_counter as pc')->result_array();
		return $suggested_post;

	}
}
