<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class User_follow_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}


	public function user_follow($data){

		$data['creation_time'] = milliseconds();

		$this->db->insert('user_follow', $data);
		$this->db->insert_id();

		$count = $this->db->query('select count(id) as total from user_follow where user_id ='.$data['user_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['user_id']);
		$this->db->set('followers_count', $count);
		$this->db->update("users");

		$count = $this->db->query('select count(id) as total from user_follow where follower_id ='.$data['follower_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['follower_id']);
		$this->db->set('following_count', $count);
		$this->db->update("users");
	}


	public function is_already_follow_user($data){
		$this->db->where('follower_id',$data['follower_id']);
		$this->db->where('user_id',$data['user_id']);
		$result = $this->db->get("user_follow")->row_array();
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}



	public function unfollow_user($data){
		$this->db->where('follower_id',$data['follower_id']);
		$this->db->where('user_id',$data['user_id']);
		$result = $this->db->delete("user_follow");


		$count = $this->db->query('select count(id) as total from user_follow where user_id ='.$data['user_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['user_id']);
		$this->db->set('followers_count', $count);
		$this->db->update("users");

		$count = $this->db->query('select count(id) as total from user_follow where follower_id ='.$data['follower_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['follower_id']);
		$this->db->set('following_count', $count);
		$this->db->update("users");

		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}


	public function followers_list($data){
		$where ="";
		if($data['last_follow_id'] != ""){
			$where .= " AND id < ".$data['last_follow_id'];
		}
		$sql = "SELECT * FROM `user_follow` WHERE user_id = ".$data['user_id'] ." $where  order by id desc limit 0,30 ";
		$result = $this->db->query($sql)->result_array();
		$return = array();
		foreach($result as $r){
			$r['viewable_user'] = $this->db->select('id,name,profile_picture,allow_tagging, designation , is_expert')->where('id',$r['follower_id'])->get('users')->row_array();
			$return[] = $r;
		}
		return $return;
	}

	public function following_list($data){
		$where ="";
		if($data['last_follow_id'] != ""){
			$where .= " AND users.id < ".$data['last_follow_id'];
		}
		if($data['allow_tag'] == 1){
			$where .= " AND u.allow_tagging = 0 ";
		}
		$sql = "SELECT uf.* FROM user_follow as uf join users as u on u.id =  uf.user_id WHERE  uf.follower_id = ".$data['user_id'] ." $where  order by uf.id desc limit 0,30 ";
		$result = $this->db->query($sql)->result_array();
		$return = array();
		foreach($result as $r){
			$r['viewable_user'] = $this->db->select('id,name,profile_picture, allow_tagging  designation , is_expert ')->where('id',$r['user_id'])->get('users')->row_array();
			$return[] = $r;
		}
		return $return;
	}

//mme list//
public function mme_list($data){
	$where ="";
	if($data['last_id'] != ""){
		$where .= " AND u.id < ".$data['last_id'];
	}

	if(strtolower($data['user_type'])=="mentor"){
		$sql = "SELECT id,name,profile_picture, allow_tagging  FROM  users as u  WHERE  u.is_mentor = 1  $where  order by u.id desc limit 0,30 ";
		$return = $this->db->query($sql)->result_array();

	}else if(strtolower($data['user_type'])=="expert"){

		$sql = "SELECT id,name,profile_picture, allow_tagging  FROM  users as u  WHERE  u.is_expert = 1  $where  order by u.id desc limit 0,30 ";
		$return = $this->db->query($sql)->result_array();
	}else if(strtolower($data['user_type'])=="normal"){		
	//	$sql = "SELECT id,name,profile_picture, allow_tagging  FROM  users as u  WHERE  u.is_expert = 0 and u.is_mentor = 0 $where  order by u.id desc limit 0,30 ";
		$sql1 = "SELECT uf.* FROM user_follow as uf join users as u on u.id =  uf.user_id WHERE   uf.follower_id = ".$data['user_id'] ." $where  order by uf.user_id desc limit 0,30 ";
		$result = $this->db->query($sql1)->result_array();
		$return = array();
			foreach($result as $r){
				$st = $this->db->query("select id,name,profile_picture, allow_tagging , designation , is_expert from users as u where id= '".$r['user_id']."' order by id DESC")->row_array();
		//	echo $this->db->last_query();
				$r=$st;
				$return[] = $r;
			}
	}
	
	
	return $return;
}




}

