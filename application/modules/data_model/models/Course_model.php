<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Course_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_basic_active_category(){
		$this->db->where('visibilty','on');
		return $this->db->get('course_category')->result_array();
	}

	public function get_course_with_category($id){
		$date = date("y-m-d");
		$query = $this->db->query("SELECT `id`, `title`, `course_main_fk`, `course_category_fk`,
									`subject_id`, `description`, segment_information ,`cover_image`, `desc_header_image` ,
								`cover_video`, `tags`, `mrp`, `publish`, `is_new`, `state`,
								`for_dams`, `non_dams`, `instructor_id`, `instructor_share`,
								 course_rating_count as rating , `course_review_count` as review_count,
								 course_learner+fake_learner as learner,
								 (SELECT count(tcse.id) as total FROM course_segment_element as  tcse where tcse.segment_fk in (select tctm.id from course_topic_master as tctm where tctm.course_fk = cm.id) and tcse.tag =0 ) as free_segment_count,
								 (SELECT count(tcse.id) as total FROM course_segment_element as  tcse where tcse.segment_fk in (select tctm.id from course_topic_master as tctm where tctm.course_fk = cm.id) and tcse.tag =1 ) as paid_segment_count
								 from course_master as cm
								 where course_category_fk = '$id'
								 and  publish = 1 and state = 0
								 and (`start_date` = '0000-00-00' or ('$date' >= start_date and '$date' <= end_date ))");
		return $query->result_array();
	}

	public function get_single_course_raw($id){
		$query = $this->db->query("select `id`, `title`, `course_main_fk`, `course_category_fk`, `subject_id`, `description`,  segment_information , `cover_image`, `desc_header_image` ,`cover_video`, `tags`, `mrp`, `publish`, `is_new`, `state`,  `instructor_id`, `instructor_share`, course_rating_count as rating , `course_review_count` as review_count, course_learner+fake_learner as learner , is_locked  , free_ids, color_code as c_code  from course_master where id = '$id' and publish = 1 and state = 0 ");
		return $query->row_array();
	}
	/* my course list */
	public function my_course_list($user_id){
		$query = $this->db->query("select  `id`, `title`, `course_main_fk`, `course_category_fk`, `subject_id`, `description`,  segment_information , `cover_image`,`desc_header_image` ,`cover_video`, `tags`, `mrp`, `publish`, `is_new`, `state`, `instructor_id`, `instructor_share`, course_rating_count as rating , `course_review_count` as review_count, course_learner+fake_learner as learner from course_master where id in (SELECT distinct(course_id) from course_transaction_record where user_id = $user_id  and transaction_status = 1) ");
		return $query->result_array();
	}

	public function get_course_with_category_limit($id){
		$date = date("y-m-d");
		$query = $this->db->query("SELECT `id`, `title`, `course_main_fk`, `course_category_fk`,
															`subject_id`, `description`,  segment_information ,  `cover_image`,`desc_header_image` ,
															`cover_video`, `tags`, `mrp`, `publish`, `is_new`, `state`,
															`for_dams`, `non_dams`, `instructor_id`, `instructor_share`,
															course_rating_count as rating , `course_review_count` as review_count,
															course_learner+fake_learner as learner,
															(SELECT count(tcse.id) as total FROM course_segment_element as  tcse where tcse.segment_fk in (select tctm.id from course_topic_master as tctm where tctm.course_fk = cm.id) and tcse.tag =0 ) as free_segment_count,
															(SELECT count(tcse.id) as total FROM course_segment_element as  tcse where tcse.segment_fk in (select tctm.id from course_topic_master as tctm where tctm.course_fk = cm.id) and tcse.tag =1 ) as paid_segment_count
															from course_master as cm
															where course_category_fk = '$id'
															and  publish = 1
															and state = 0
															and (`start_date` = '0000-00-00' or ('$date' >= start_date and '$date' <= end_date ))
															limit 0,10");
		return $query->result_array();
	}

	/*get course with dams non_dams filter */

	public function get_course_with_category_limit_user_filter($id,$user_id){

		$user_info = $this->db->select('dams_tokken')->where('id',$user_id)->get('users')->row();

		$user_batch_info = $this->db->query("SELECT FranchiseName,CourseGroup,Session,Course,Batch  from users_dams_info where user_id = $user_id")->row();
		if(count($user_batch_info) > 0 ){
			$FranchiseName = $user_batch_info->FranchiseName;
			$CourseGroup = $user_batch_info->CourseGroup;
			$Session = $user_batch_info->Session;
			$Course = $user_batch_info->Course;
			$Batch = $user_batch_info->Batch;
		}else{
			$FranchiseName = "";
			$CourseGroup = "";
			$Session = "";
			$Course = "";
			$Batch = "";
		}


		$where = "";
		if(count($user_info) > 0 && $user_info->dams_tokken =="" ){
			$where =  " and (course_for = 1 or course_for = 2 ) ";
		}else{
			$where =  " and (course_for = 0 or course_for = 2 ) ";
		}


		$date = date("y-m-d");

		$query = $this->db->query("SELECT cm.id, `title`, `course_main_fk`, `course_category_fk`,
										`subject_id`, `description`,  segment_information ,  `cover_image`,`desc_header_image` ,
										`cover_video`, `tags`, `mrp`, `publish`, `is_new`, `state`,
										`for_dams`, `non_dams`, `instructor_id`, `instructor_share`,
										course_rating_count as rating , `course_review_count` as review_count,
										course_learner+fake_learner as learner ,
										case
										when  (select count(cf.id) from course_filtration as cf where cf.course_id = cm.id ) = 0 and  cm.course_for = 0 then 'passed'
										when cm.course_for = 0 and (select count(cf.id) from course_filtration as cf where cf.course_id = cm.id ) > 0 and (SELECT count(cfl.course_id) FROM course_filtration as cfl WHERE (cfl.FranchiseName = '*' or cfl.FranchiseName = '$FranchiseName' ) and
												(cfl.Session = '*' or cfl.Session = '$Session' ) and
												(cfl.CourseGroup = '*' or cfl.CourseGroup = '$CourseGroup' ) and
												(cfl.Course = '*' or cfl.Course = '$Course' ) and
												(cfl.Batch = '*' or cfl.Batch = '$Batch' )
										and cfl.course_id = cm.id ) > 0 then 'passed'
										when cm.course_for = 1 then 'passed'
										when cm.course_for = 2 then 'passed'
										else 'not_passed'
										end as dams_passing

										from course_master as cm
										where course_category_fk = '$id'
										and  publish = 1
										and state = 0
										and (`start_date` = '0000-00-00' or ('$date' >= start_date and '$date' <= end_date ))
										$where
										having dams_passing = 'passed'
										order by cm.id desc limit 0,10");


		return $query->result_array();
	}


	public function search_course($user_id,$search_content,$last_course_id){

		$date = date("y-m-d");
		$where = "";
		if($last_course_id > 0 ){
			$where  = " and cm.id < $last_course_id";
		}

		$user_batch_info = $this->db->query("SELECT FranchiseName,CourseGroup,Session,Course,Batch  from users_dams_info where user_id = $user_id")->row();
		if(count($user_batch_info) > 0 ){
			$FranchiseName = $user_batch_info->FranchiseName;
			$CourseGroup = $user_batch_info->CourseGroup;
			$Session = $user_batch_info->Session;
			$Course = $user_batch_info->Course;
			$Batch = $user_batch_info->Batch;
		}else{
			$FranchiseName = "";
			$CourseGroup = "";
			$Session = "";
			$Course = "";
			$Batch = "";
		}

		$user_info = $this->db->select('dams_tokken')->where('id',$user_id)->get('users')->row();
		if(count($user_info) > 0 && $user_info->dams_tokken =="" ){
			$where =  " and (course_for = 1 or course_for = 2 ) ";
		}else{
			$where =  " and (course_for = 0 or course_for = 2 ) ";
		}


		$query = "SELECT cm.id, cm.title, cm.course_main_fk,
										cm.course_category_fk, cm.subject_id,
										cm.description,  cm.segment_information ,  cm.cover_image, cm.desc_header_image ,
										cm.cover_video, cm.tags,
										cm.mrp, cm.publish, cm.is_new, cm.state,
										cm.for_dams, cm.non_dams, cm.instructor_id,
										cm.instructor_share, cm.course_rating_count as rating ,
										cm.course_review_count as review_count,
										cm.course_learner+cm.fake_learner as learner ,
										case
										when  (select count(cf.id) from course_filtration as cf where cf.course_id = cm.id ) = 0 and  cm.course_for = 0 then 'passed'
										when cm.course_for = 0 and (select count(cf.id) from course_filtration as cf where cf.course_id = cm.id ) > 0 and (SELECT count(cfl.course_id) FROM course_filtration as cfl WHERE (cfl.FranchiseName = '*' or cfl.FranchiseName = '$FranchiseName' ) and
												(cfl.Session = '*' or cfl.Session = '$Session' ) and
												(cfl.CourseGroup = '*' or cfl.CourseGroup = '$CourseGroup' ) and
												(cfl.Course = '*' or cfl.Course = '$Course' ) and
												(cfl.Batch = '*' or cfl.Batch = '$Batch' )
										and cfl.course_id = cm.id ) > 0 then 'passed'
										when cm.course_for = 1 then 'passed'
										when cm.course_for = 2 then 'passed'
										else 'not_passed'
										end as dams_passing

										From course_master as cm
										left join course_category as cct on cm.course_category_fk = cct.id
										Where cm.publish = 1
										and cct.visibilty = 'on'
										and cm.state = 0
										$where
										and cm.title like '%$search_content%'
										and (cm.start_date = '0000-00-00' or ('$date' >= cm.start_date and '$date' <= cm.end_date ))
										having dams_passing = 'passed'
										order by cm.id desc
										limit 0 , 10 ";

		$query = $this->db->query($query);
		return $query->result_array();
	}

	public function search_course_exam($user_id,$search_content,$last_course_id){

		$date = date("y-m-d");
		$where = "";
		if($last_course_id > 0 ){
			$where  = " and cm.id < $last_course_id";
		}

		$query = "SELECT cm.id, cm.title, cm.course_attribute,
								cm.cover_image, cm.desc_header_image ,
								cm.mrp ,cm.course_sp , cm.color_code ,  cm.validity
						From course_master as cm
						left join course_category as cct on cm.course_category_fk = cct.id
						Where cm.publish = 1
						and cm.state = 0  and cm.course_type = 0
						$where
						and cm.title like '%$search_content%'
						and (cm.start_date = '0000-00-00' or ('$date' >= cm.start_date and '$date' <= cm.end_date ))
						order by cm.id desc
						limit 0 , 10 ";

		$query = $this->db->query($query);


		return $query->result_array();
	}


	public function get_course_on_landing_page($input){
		$where = $join = "";
		$date = date("y-m-d");

		if($input['sub_cat'] > 0 ){
			$join  = 'join course_category_relationship as ccr on ccr.course_id = cm.id and  ccr.cate_id = '.$input['sub_cat'];
		}

		if($input['course_type'] > 0 ){
			$join  = 'join course_type_relationship as ctr on ctr.course_id = cm.id and  ctr.type_id = '.$input['course_type'];
		}

		$query = "SELECT cm.id, cm.title, cm.course_main_fk,
										cm.course_category_fk, cm.subject_id,
										cm.description, cm.segment_information , cm.cover_image, cm.desc_header_image ,
										cm.cover_video, cm.tags,
										cm.mrp, cm.publish, cm.is_new, cm.state,
										cm.for_dams, cm.non_dams, cm.instructor_id,
										cm.instructor_share, cm.course_rating_count as rating ,
										cm.course_review_count as review_count,
										cm.course_learner+cm.fake_learner as learner,cm.practice_id ,
										(SELECT count(tcse.id) as total FROM course_segment_element as  tcse where tcse.segment_fk in (select tctm.id from course_topic_master as tctm where tctm.course_fk = cm.id) and tcse.tag =0 ) as free_segment_count,
										(SELECT count(tcse.id) as total FROM course_segment_element as  tcse where tcse.segment_fk in (select tctm.id from course_topic_master as tctm where tctm.course_fk = cm.id) and tcse.tag =1 ) as paid_segment_count
										From course_master as cm
										left join course_category as cct on cm.course_category_fk = cct.id
										$join
										Where cm.publish = 1
										and cm.state = 0
										and cm.course_type = 1
										$where
										and (cm.start_date = '0000-00-00' or ('$date' >= cm.start_date and '$date' <= cm.end_date ))
										order by cm.id desc
										limit 0 , 10 ";
		$query = $this->db->query($query);
		return $query->result_array();
	}

	public function get_course_on_landing_page_exam($input){
		$where = $join = "";
		$date = date("y-m-d");

		if($input['sub_cat'] > 0 ){
			$where  .= ' and cm.course_category_fk = '.$input['sub_cat'];
		}

		// if($input['course_type'] > 0 ){
		// 	$join  .= ' join course_type_relationship as ctr on ctr.course_id = cm.id and  ctr.type_id = '.$input['course_type'];
		// }

		$query = "SELECT cm.id, cm.title, cm.course_attribute,
								cm.cover_image, cm.desc_header_image ,
								cm.mrp ,cm.course_sp , cm.color_code ,  cm.validity ,
								cm.course_learner+cm.fake_learner as learner ,
								bu.username as author
								From course_master as cm
								left join course_category as cct on cm.course_category_fk = cct.id
								join backend_user as bu on bu.id = cm.instructor_id
								$join
								Where cm.publish = 1
								and cm.state = 0
								and cm.course_type = 0
								$where
								and (cm.start_date = '0000-00-00' or ('$date' >= cm.start_date and '$date' <= cm.end_date ))
								order by cm.id desc
								limit 0 , 10 ";
		$query = $this->db->query($query);
		return $query->result_array();
	}

	public function get_course_sub_cate_landing_page($input){
		$where = $join = "";
		$date = date("y-m-d");

		if($input['sub_cat'] > 0 ){
			$join  = 'join course_category_relationship as ccr on ccr.course_id = cm.id and  ccr.cate_id = '.$input['sub_cat'];
		}

		if($input['course_type'] > 0 ){
			$join  = 'join course_type_relationship as ctr on ctr.course_id = cm.id and  ctr.type_id = '.$input['course_type'];
		}

		$query = "SELECT cm.id, cm.title, cm.course_main_fk,cm.cover_image, cm.color_code as c_code , 
										cm.course_category_fk, cm.subject_id,
										cm.description,  cm.segment_information ,  cm.cover_image, cm.desc_header_image ,
										cm.cover_video, cm.tags,
										cm.mrp, cm.publish, cm.is_new, cm.state,
										cm.instructor_id,
										cm.instructor_share, cm.course_rating_count as rating ,
										cm.course_review_count as review_count,
										(cm.course_learner+cm.fake_learner) as learner,
										cm.practice_id
										From course_master as cm
										left join course_category as cct on cm.course_category_fk = cct.id
										$join
										Where cm.publish = 1
										and cm.state = 0
										and cm.course_type = 1
										$where
										order by cm.creation_time desc
										limit 0 , 100 ";
		$query = $this->db->query($query);

		return $query->result_array();
	}

	public function get_course_sub_cate_landing_page_exam($input){
		$where = $join = "";
		$date = date("y-m-d");

		if($input['sub_cat'] > 0 ){
			$join  .= ' join course_category_relationship as ccr on ccr.course_id = cm.id and  ccr.cate_id = '.$input['sub_cat'];
		}

		if($input['course_type'] > 0 ){
			$join  .= ' join course_type_relationship as ctr on ctr.course_id = cm.id and  ctr.type_id = '.$input['course_type'];
		}

		$query = "SELECT cm.id, cm.title, cm.course_attribute,
							cm.cover_image, cm.desc_header_image ,
							cm.mrp ,cm.course_sp , cm.color_code ,  cm.validity
										From course_master as cm
										left join course_category as cct on cm.course_category_fk = cct.id
										$join
										Where cm.publish = 1
										and cm.state = 0
										and cm.course_type = 0
										$where
										and (cm.start_date = '0000-00-00' or ('$date' >= cm.start_date and '$date' <= cm.end_date ))
										order by cm.creation_time desc
										limit 0 , 100 ";
		$query = $this->db->query($query);
		return $query->result_array();
	}


	public function get_basic_sub_active_category($id){
		return $this->db->where('parent_fk',$id)->get('course_category')->result_array();
	}

}
