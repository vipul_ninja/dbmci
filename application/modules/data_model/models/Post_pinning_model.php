<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_pinning_model extends CI_Model {

	function __construct() {
		parent::__construct();
    }
    
    public function set_post_as_pinned($data){
        $this->db->where('user_id',$data['user_id']);
        $this->db->where('id',$data['post_id']);
        $update = array('pinned_post'=> milliseconds() );
        $this->db->update('post_counter',$update);
    }

    public function remove_post_from_pinned($data){
        $this->db->where('user_id',$data['user_id']);
        $this->db->where('id',$data['post_id']);
        $update = array('pinned_post'=> "" );
        $this->db->update('post_counter',$update);
    }

    public function check_if_user_is_modrated($user_id){
        $this->db->select('id');
        $this->db->where('id',$user_id);
        $this->db->where('is_moderate',1);
        
        if(count($this->db->get('users')->row()) > 0 ){
            return true;
        }
        return false;
    }
}	