<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Users_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	

	public function save_user($data){
		
		$data['creation_time'] = milliseconds();
		$this->db->insert('users', $data);
		$user_id = $this->db->insert_id();
		if($user_id){
			$array = array('user_id'=>$user_id);
			$this->db->insert('user_permission',$array);
		}
		if(array_key_exists('location', $data) && $data['location'] != ""){
			$array = explode(',',$data['location']);
			$user['user_id'] = $user_id;
			$user['country'] = $array[0];
			$user['state'] = $array[1];
			$user['city'] = $array[2];
			$user['latitude'] = $array[3];
			$user['longitude'] = $array[4];
			$user['ip_address'] = $array[5];
			$this->db->insert('user_registerd_location',$user);
		}

		return $user_id;

	}

	public function get_user($id){

		$query = $this->db->query("SELECT users.* 
										 from users 
										 where users.id = $id");								 
		$user_data =  $query->row_array();
		if(count($user_data) > 0 ){
			unset($user_data['password']);
			unset($user_data['otp']);
		}
		/* check how much experts a user is following */
		$user_data['expert_following'] =  (int)$this->db->query("SELECT count(uf.id) as total 
									FROM user_follow as uf 
									left join users as u on uf.user_id = u.id 
									WHERE uf.follower_id = $id and u.is_expert = 1")->row()->total;

		$user_data['user_likes']=$this->db->query("SELECT count(*) as count_likes FROM `post_counter` inner join user_post_like on post_counter.id=user_post_like.post_id where post_counter.user_id='".$id."'")->row()->count_likes;
		return  $user_data; 
	}

	public function get_social_user($info){
		//$this->db->where('social_type',$info['social_type']);
		$this->db->where('social_tokken',$info['social_tokken']);
		//$this->db->where('email',$info['email']);
		return $this->db->get('users')->row_array();
	}

	public function get_custum_user($info){
		$this->db->where("email",$info['email']);
		$this->db->where("password",$info['password']);
		return $this->db->get('users')->row_array();
	}

	public function update_device_tokken($info){
		$array  = array(
			"device_type"=>$info['device_type'],
			"device_tokken"=>$info['device_tokken']
			); 
		$this->db->where('id',$info['id']);
		$this->db->update('users',$array);
	}

	public function update_password_via_otp($info){
	$array  = array(
			"password"=>$info['password']	
			); 
		$this->db->where('otp',$info['otp']);
		$this->db->where('mobile',$info['mobile']);
		$this->db->or_where('email',$info['mobile']);
   		$query=	$this->db->update('users',$array);
			if($this->db->affected_rows()>0){
			return 1;
		}else{

			return 2;
		}
	}

	public function stream_registration($data){
		$this->db->insert("user_registration_data",$data);

		$this->db->where("id",$data['user_id']);
		$this->db->update("users",array('is_course_register'=>1));
	}

	public function update_stream_registration($data){
		$this->db->where("user_id",$data['user_id']);
		$this->db->update("user_registration_data",$data);
	}


	public function is_already_stream_register($user_id){
		$this->db->where('user_id',$user_id);
		if($data = $this->db->get("user_registration_data")->row_array()){
			return true;
		}
		return false;
	}

	public function is_alreday_dams_authenticated($dams_tokken){
		$this->db->where('dams_tokken',$dams_tokken);
		return $data = $this->db->get("users")->row_array();
	}
}