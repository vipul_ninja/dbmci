<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Post_mcq_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	

	public function save_user_mcq($data){
		
		$creation_time = milliseconds();
		$data_post_counter['creation_time'] = $creation_time;
		$data_post_counter['user_id'] = $data['user_id'];
		$data_post_counter['post_type'] = "user_post_type_mcq";
		$data_post_counter['post_tag'] = $data['post_tag'];
		$data_post_counter['post_for'] = $data['post_for'];

		$this->db->insert('post_counter', $data_post_counter);
		unset($data['user_id']);
		unset($data['post_tag']);
		$data['post_id'] =  $this->db->insert_id();
		$this->db->insert('user_post_type_mcq', $data);
		
		//increase user post counter 
		$this->db->where('id',$data_post_counter['user_id']);
		$this->db->set('post_count', 'post_count+1', FALSE);
		$this->db->update("users");
		
		return $data['post_id'];
	}

	public function save_user_mcq_answer($data){
		$creation_time = milliseconds();
		$info['creation_time'] = $creation_time;
		$info['user_id'] = $data['user_id'];
		$info['mcq_id'] = $data['mcq_id'];
		$info['answer'] = $data['answer'];

		$this->db->insert('user_post_type_mcq_answers', $info);
	}
	
	public function edit_user_mcq($data,$id){
		$this->db->where('post_id',$id);
		$this->db->update('user_post_type_mcq',$data);
	}

	public function check_user_mcq_answer($data){
		$this->db->where("user_id",$data['user_id']);
		$this->db->where("mcq_id",$data['mcq_id']);
		if($this->db->get('user_post_type_mcq_answers')->row_array()){
			return true;
		}
		return false;
	}
}