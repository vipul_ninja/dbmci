<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Fan_wall_banner_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_banner(){

		$query = $this->db->query(" SELECT *
									FROM fan_wall_banner
									ORDER BY RAND( ) 
									");
		return $query->row_array();
	}
	
	public function update_banner_hit_count($banner_id){
		
		$this->db->select("id,hit_count");
		$this->db->where('id',$banner_id);
		$banner_details = $this->db->get('fan_wall_banner')->row_array();
		
		$incremented_hit_count = $banner_details['hit_count'] +1 ;
		$update_banner = array('hit_count'=> $incremented_hit_count );
		
		$this->db->where('id',$banner_id);
		$updation = $this->db->update('fan_wall_banner',$update_banner);
		if($updation){
			return $incremented_hit_count; 
		}
		else{
			return false;
		}
		
	}
}	