<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Video_like_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	

	public function like_video($data){
		
		$data['creation_time'] = milliseconds();

		$this->db->insert('video_like_meta', $data);
		$this->db->insert_id();

		$count = $this->db->query('select count(id) as total from video_like_meta where video_id ='.$data['video_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;		

		$this->db->where('id',$data['video_id']);
		$this->db->set('likes', $count);
		$this->db->update("video_master");
	}


	public function dislike_video($data){
		
		$this->db->where('user_id', $data['user_id']);
		$this->db->where('video_id', $data['video_id']);
        $this->db->delete("video_like_meta");
        
		$count = $this->db->query('select count(id) as total from video_like_meta where video_id ='.$data['video_id'])->row_array();
		$count = ($count && $count['total'] > 0 )?$count['total']:0;

		$this->db->where('id',$data['video_id']);
		$this->db->set('likes', $count);
		$this->db->update("video_master");
	}

	public function is_already_like_video($data){
		$this->db->where('user_id',$data['user_id']);
		$this->db->where('video_id',$data['video_id']);
		$result = $this->db->get("video_like_meta")->row_array();
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}