<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Version extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function get_version(){

		$this->db->where('id',1);
		$this->db->limit(1,0);
		$info = $this->db->get('version_control')->row();
		$data=  array('android' => $info->android , 'ios'=> $info->ios );

		return_data(true , "version info" , $data );
	}


	public function mecache_test(){

		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file','key_prefix' => 'pro_'));
		if ( ! $version_info = $this->cache->get('version_info')){
	        $this->db->where('id',1);
			$this->db->limit(1,0);
			$info = $this->db->get('version_control')->row();
			$version_info =  array('android' => $info->android , 'ios'=> $info->ios );
	        $this->cache->save('version_info', $version_info , 300);
		}

		return_data(true ,"version info", $version_info );

		//$this->cache->delete('version_info');
		//pre($this->cache->cache_info());
	}



}	
