<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_logger extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function genrate_activity_while_following($data){
		if($data['action_for_user_id'] != $data['action_performed_by_user_id'] ){
			$array = array(
				"action_for_user_id"=>$data['action_for_user_id'],
				"action_performed_by_user_id"=>$data['action_performed_by_user_id'],
				"action_element"=>"following",
				"action_element_id"=>$data['action_element_id'],
				"creation_time"=> milliseconds()
				);
			$this->db->insert("user_activity_generator",$array);
		}
	}

	public function genrate_activity_while_post_like($data){
		if($data['action_for_user_id'] != $data['action_performed_by_user_id'] ){
			$array = array(
				"action_for_user_id"=>$data['action_for_user_id'],
				"action_performed_by_user_id"=>$data['action_performed_by_user_id'],
				"action_element"=>"post_like",
				"action_element_id"=>$data['action_element_id'],
				"creation_time"=> milliseconds()
				);
			$this->db->insert("user_activity_generator",$array);
		}

	}

	public function genrate_activity_while_post_share($data){
		if($data['action_for_user_id'] != $data['action_performed_by_user_id'] ){
			$array = array(
				"action_for_user_id"=>$data['action_for_user_id'],
				"action_performed_by_user_id"=>$data['action_performed_by_user_id'],
				"action_element"=>"post_share",
				"action_element_id"=>$data['action_element_id'],
				"creation_time"=> milliseconds()
				);
			$this->db->insert("user_activity_generator",$array);			
		}	

	}

	public function generate_activity_whlie_comment_on_post($data){
		if($data['action_for_user_id'] != $data['action_performed_by_user_id'] ){
			$array = array(
				"action_for_user_id"=>$data['action_for_user_id'],
				"action_performed_by_user_id"=>$data['action_performed_by_user_id'],
				"action_element"=>"post_comment",
				"action_element_id"=>$data['action_element_id'],
				"creation_time"=> milliseconds()
				);
			$this->db->insert("user_activity_generator",$array);			
		}	
	}

	public function	generate_activity_whlie_people_tagged_on_post($data){
		if($data['action_for_user_id'] != $data['action_performed_by_user_id'] ){
			$array = array(
				"action_for_user_id"=>$data['action_for_user_id'],
				"action_performed_by_user_id"=>$data['action_performed_by_user_id'],
				"action_element"=>"post_tagged_on",
				"action_element_id"=>$data['action_element_id'],
				"creation_time"=> milliseconds()
				);
			$this->db->insert("user_activity_generator",$array);			
		}	
	}

	public function generate_activity_whlie_people_tagged_on_comment($data){
		if($data['action_for_user_id'] != $data['action_performed_by_user_id'] ){
			$array = array(
				"action_for_user_id"=>$data['action_for_user_id'],
				"action_performed_by_user_id"=>$data['action_performed_by_user_id'],
				"action_element"=>"comment_tagged_on",
				"action_element_id"=>$data['action_element_id'],
				"creation_time"=> milliseconds()
				);
			$this->db->insert("user_activity_generator",$array);			
		}
	}

	public function get_user_activity(){
		$this->validate_get_user_activity();
		$user_id = $this->input->post('user_id');
		$where = "";
		if($this->input->post('last_activity_id') != "" ){
			$where = " and uag.id < " . $this->input->post('last_activity_id');
		}
		$query = $this->db->query(" SELECT uag.* , u.profile_picture ,u.name
							FROM user_activity_generator as uag
							join users as u on u.id = uag.action_performed_by_user_id 
							Where action_for_user_id =  $user_id 
							&& action_element != 'post_tagged_on' 
							&& action_element != 'comment_tagged_on'
							$where 
							order by uag.id desc 
							limit 0 , 20 
							");
		$data =  $query->result_array();
		$temp = array();
		foreach($data as $d){
			$array = array();
			$array['id'] = $d['id'];
			$array['action_performed_by'] =  array('id'=>$d['action_performed_by_user_id'] , 'profile_picture'=>$d['profile_picture'],'name'=>$d['name']);
			$array['creation_time'] = $d['creation_time'];
			$array['view_state'] = $d['view_state'];
			$array['activity_type'] = $d['action_element'];
			
			if($d['action_element'] !== "following"){
				$array['post_id'] = $d['action_element_id'];
			}else{
				$array['user_id'] = $d['action_element_id'];
			}
			$temp[] = $array;
		}
		if($temp){
			return_data(true,'User Notification.',$temp);
		}else{
			return_data(false,'No Notification Found.',$temp);
		}
		
	}

	public function get_user_activities(){
		
		$this->validate_get_user_activity();
		$user_id = $this->input->post('user_id');
		$where = "";
		if($this->input->post('last_activity_id') != "" ){
			$where = " and uag.id < " . $this->input->post('last_activity_id');
		}
		$query = $this->db->query(" SELECT uag.* , u.profile_picture ,u.name
							FROM user_activity_generator as uag
							join users as u on u.id = uag.action_performed_by_user_id 
							Where action_for_user_id =  $user_id 
							$where 
							order by uag.id desc 
							limit 0 , 20 
							");
		$data =  $query->result_array();
		$temp = array();
		foreach($data as $d){
			$array = array();
			$array['id'] = $d['id'];
			$array['action_performed_by'] =  array('id'=>$d['action_performed_by_user_id'] , 'profile_picture'=>$d['profile_picture'],'name'=>$d['name']);
			$array['creation_time'] = $d['creation_time'];
			$array['view_state'] = $d['view_state'];
			$array['activity_type'] = $d['action_element'];
			
			if($d['action_element'] !== "following"){
				$array['post_id'] = $d['action_element_id'];
			}else{
				$array['user_id'] = $d['action_element_id'];
			}

			if($d['action_element'] == "comment_tagged_on"){
				$array['comment_id'] = $d['action_element_id'];
			}
			$temp[] = $array;
		}
		if($temp){
			return_data(true,'User Notification.',$temp);
		}else{
			return_data(false,'No Notification Found.',$temp);
		}
		
	}


	private function validate_get_user_activity(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		if(array_key_exists('last_activity_id', $_POST) == False){
			$_POST['last_activity_id'] = "";
		}
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	public function make_activity_viewed(){

		$this->validate_make_activity_viewed();
		$this->db->where('id',$this->input->post('id'));
		$this->db->where('action_for_user_id',$this->input->post('user_id'));
		$this->db->update('user_activity_generator',array('view_state'=>1));
		return_data(true,'Activity Viewd',array());
	}

	private function validate_make_activity_viewed(){

		post_check();
		$this->form_validation->set_rules('id','id', 'trim|required');
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}		
	}

	public function all_notification_counter(){
		$this->validate_all_notification_counter();

		$this->db->where('action_for_user_id',$this->input->post('user_id'));
		$this->db->where('view_state',0);
		$count = $this->db->count_all_results('user_activity_generator');

		return_data(true,'Counter',array('counter'=>$count));

	}

	private function validate_all_notification_counter(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}		
	}

	public function set_all_read(){
		$this->validate_set_all_read();
		$this->db->where('action_for_user_id',$this->input->post('user_id'));
		$this->db->update('user_activity_generator',array('view_state'=>1));
		return_data(true,'All notification set as read.',array());
	}

	private function validate_set_all_read(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}
}	