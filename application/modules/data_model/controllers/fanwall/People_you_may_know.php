<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class People_you_may_know extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("people_you_may_know_model");
	}

	public function get_list(){

		$this->validate_get_list();

		$list = $this->people_you_may_know_model->get_list($this->input->post('user_id'));

		if(count($list)> 0 ){
			return_data(true,"People found",$list);
		}else{
			return_data(false,"People not found");
		}

	}

	private function validate_get_list(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


}	