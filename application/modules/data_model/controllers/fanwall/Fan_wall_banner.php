<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fan_wall_banner extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("fan_wall_banner_model");
	}

	public function get_fan_wall_banner(){
		$data = $this->fan_wall_banner_model->get_banner();
		if($data){
			return_data(true,"Banner",$data);
		}
		return_data(false,"No Banner Found",array());
	}
	
	
	public function update_banner_hit_count(){
		$this->validate_update_banner_hit_count();
		
		$banner_id = $this->input->post('banner_id');
		$count = $this->fan_wall_banner_model->update_banner_hit_count($banner_id);
		return_data(true,"Banner hit count",array("banner_hit_count"=>$count),array());
	}

	private function validate_update_banner_hit_count(){
		post_check();
		$this->form_validation->set_rules('banner_id','banner_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


}	