<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\CloudFront\CloudFrontClient;

class Fan_wall extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("fan_wall_model");
		// get agent 
		$this->load->library('user_agent');
	}

	
	public function get_fan_wall_for_user(){  //print_r($_POST); die;
		$this->validate_get_fan_wall_for_user();
		$request_user_id = $this->input->post('user_id');
		
		/*Requester User Details*/
		$request_user_details = $this->fan_wall_model->request_user_details_by_id($request_user_id);		
		$request_user_dams_token = $request_user_details['dams_tokken'];
		
		/* for pagination */
		if(!array_key_exists('last_post_id', $_POST)){
			$last_post_id = "";
		}else{
			$last_post_id = $this->input->post('last_post_id');
		}

		/* post with tag id */
		if(!array_key_exists('tag_id', $_POST)){
			$tag_id = "";
		}else{
			$tag_id = $this->input->post('tag_id');
		}

		/* post with search text */
		if(!array_key_exists('search_text', $_POST)){
			$search_text = "";
		}else{
			$search_text = $this->input->post('search_text');
		}


		/* get all posts */
		$post = array();
		$post_meta['post_data'] = "";

		/* has same behaviour like fan wall but it is for bookmarks */
		if($this->input->get('bookmark_request') == "yes"){ /* result sent to user saved notes */
			$input =  array(
						"user_id"=>$request_user_id,
						"last_post_id"=>$last_post_id,
						"tag_id"=> $tag_id,
						"search_text"=> $search_text
						);

			foreach($this->fan_wall_model->all_bookmaked($input) as $post_meta){
				$post[] = $this->get_single_post_with_user_id($post_meta['id'],$request_user_id);
			}
		}elseif($this->input->get('is_watcher')){ /* result sent to fan wall */
			$input =  array(
						"user_id"=>$request_user_id,
						"last_post_id"=>$last_post_id,
						"tag_id"=> $tag_id,
						"search_text"=> $search_text
						);
			/*foreach($this->fan_wall_model->all_user_post($input) as $post_meta){
				$post[] = $this->get_single_post_with_user_id($post_meta['id'],$this->input->get('is_watcher'));
			}*/
			
			// get all_post_ids
			$all_posts = $this->fan_wall_model->all_user_post($input);
			$request_user_id = $this->input->get('is_watcher');
			if(count($all_posts) > 0 ){
				$ids = $mcq = $normal = array();
				foreach($all_posts as $ap ){
					$ids[] = $ap['id'];
					if($ap['post_type'] == "user_post_type_mcq"){ $mcq[] = $ap['id']; }
					if($ap['post_type'] == "user_post_type_normal"){ $normal[] = $ap['id']; }					
				}
				// get user likes w.r.t. multiple post ids is user_liked_post_multiple 
				$like_chunk = $this->fan_wall_model->is_liked_post_multiple($ids,$request_user_id);
				$like_chunk = explode(",",$like_chunk);
				//get bookmarks 
				$bookmark_chunk = $this->fan_wall_model->is_bookmarked_post_multiple($ids,$request_user_id);
				$bookmark_chunk = explode(",",$bookmark_chunk);
				//mcq chunk 
				$mcq_chunk = array();
				if(count($mcq)> 0){
					$mcq_chunk = $this->fan_wall_model->get_user_post_type_mcq_multiple($ids);
				}

				//normal chunk 
				$normal_chunk = array();
				if(count($normal)> 0){
					$normal_chunk = $this->fan_wall_model->get_user_post_type_normal_multiple($ids);
				}

				foreach($all_posts as $post_meta){
					$post_meta['is_liked'] = (in_array($post_meta['id'], $like_chunk))?true:false;
					$post_meta['is_bookmarked'] = (in_array($post_meta['id'], $bookmark_chunk))?true:false;
					if($post_meta['post_type'] == "user_post_type_mcq"){
						// get post data with post id 
						foreach($mcq_chunk as $mc){
							if($mc['post_id'] == $post_meta['id']){
								$post_meta['post_data'] = $mc;
							}
						}
						$post_meta['post_data']['answer_given_by_user'] = "";
					}
					if($post_meta['post_type'] == "user_post_type_normal"){
						foreach($normal_chunk as $nc){
							if($nc['post_id'] == $post_meta['id']){
								$post_meta['post_data'] = $nc;
							}
						}
					}

					$post_meta['post_data']['post_file'] = $this->create_s3_video_link($post_meta['id']);
					//$post_meta['post_owner_info'] = $this->fan_wall_model->get_post_owner_info($post_meta['user_id']);
					$post_meta['post_owner_info']['id']   = $post_meta['user_id'];
					$post_meta['post_owner_info']['name']   = $post_meta['post_owner_name'];
					$post_meta['post_owner_info']['profile_picture']   = $post_meta['post_owner_profile_picture'];
					$post_meta['post_owner_info']['speciality']   = ($post_meta['speciality'] == NULL || $post_meta['speciality'] == "")?$post_meta['optional_text']:$post_meta['speciality'];
					unset($post_meta['post_owner_name'],$post_meta['post_owner_profile_picture'],$post_meta['speciality'],$post_meta['optional_text']);
					
					$post_meta['tagged_people'] = $this->fan_wall_model->get_post_tagged_people($post_meta['id']);

					$post[] = $post_meta; 
				}
			}

		}else{
			$input =  array(
				"user_id"=>$request_user_id,
				"last_post_id"=>$last_post_id,
				"tag_id"=> $tag_id,
				"search_text"=> $search_text
				);
			
			/* Where condition for differentiating dams and non dams */
			if($request_user_dams_token != ""){
				$where = "WHERE users.is_live_agent = 1 and ucheck.dams_tokken !='' and pc.post_for = 1";
			}
			else if($request_user_dams_token == ""){
				$where = "where ucheck.dams_tokken ='' and pc.post_for = 2";
			}
			
			//static work 
			$query = $this->db->query(" SELECT pc.id 
										FROM post_counter AS pc
										JOIN users ON users.id = pc.user_id
										JOIN users AS ucheck ON ucheck.id = '".$request_user_id."'	
										 
										$where
										and pc.is_shared = 0
										and pc.status = 0 
										ORDER BY pc.id DESC 
										LIMIT 0 , 1 ");
			$one_record =  $query->row_array();
			//echo $this->db->last_query(); die;
			$last_double_check = "";
			$result = array();
			if($one_record && $input['last_post_id'] == "" && $input['search_text'] == "" &&  $input['tag_id'] == "" ){
				$last_double_check = $one_record['id']; 
				$result[] = $this->get_single_post_with_user_id($one_record['id'],$request_user_id);
			}

			// get all_post_ids
			$all_posts = $this->fan_wall_model->all_posts($input);

			if(count($all_posts) > 0 ){
				$ids = $mcq = $normal = array();
				foreach($all_posts as $ap ){
					$ids[] = $ap['id'];
					if($ap['post_type'] == "user_post_type_mcq"){ $mcq[] = $ap['id']; }
					if($ap['post_type'] == "user_post_type_normal"){ $normal[] = $ap['id']; }					
				}
				// get user likes w.r.t. multiple post ids is user_liked_post_multiple 
				$like_chunk = $this->fan_wall_model->is_liked_post_multiple($ids,$request_user_id);
				$like_chunk = explode(",",$like_chunk);
				//get bookmarks 
				$bookmark_chunk = $this->fan_wall_model->is_bookmarked_post_multiple($ids,$request_user_id);
				$bookmark_chunk = explode(",",$bookmark_chunk);
				//mcq chunk 
				$mcq_chunk = array();
				if(count($mcq)> 0){
					$mcq_chunk = $this->fan_wall_model->get_user_post_type_mcq_multiple($ids);
				}

				//normal chunk 
				$normal_chunk = array();
				if(count($normal)> 0){
					$normal_chunk = $this->fan_wall_model->get_user_post_type_normal_multiple($ids);
				}

				foreach($all_posts as $post_meta){
					$post_meta['is_liked'] = (in_array($post_meta['id'], $like_chunk))?true:false;
					$post_meta['is_bookmarked'] = (in_array($post_meta['id'], $bookmark_chunk))?true:false;
					if($post_meta['post_type'] == "user_post_type_mcq"){
						// get post data with post id 
						foreach($mcq_chunk as $mc){
							if($mc['post_id'] == $post_meta['id']){
								$post_meta['post_data'] = $mc;
							}
						}
						$post_meta['post_data']['answer_given_by_user'] = "";
					}
					if($post_meta['post_type'] == "user_post_type_normal"){
						foreach($normal_chunk as $nc){
							if($nc['post_id'] == $post_meta['id']){
								$post_meta['post_data'] = $nc;
							}
						}
					}

					$post_meta['post_data']['post_file'] = $this->create_s3_video_link($post_meta['id']);
					//$post_meta['post_owner_info'] = $this->fan_wall_model->get_post_owner_info($post_meta['user_id']);
					$post_meta['post_owner_info']['id']   = $post_meta['user_id'];
					$post_meta['post_owner_info']['name']   = $post_meta['post_owner_name'];
					$post_meta['post_owner_info']['profile_picture']   = $post_meta['post_owner_profile_picture'];
					$post_meta['post_owner_info']['speciality']   = ($post_meta['speciality'] == NULL || $post_meta['speciality'] == "")?$post_meta['optional_text']:$post_meta['speciality'];
					unset($post_meta['post_owner_name'],$post_meta['post_owner_profile_picture'],$post_meta['speciality'],$post_meta['optional_text']);

					$post_meta['tagged_people'] = $this->fan_wall_model->get_post_tagged_people($post_meta['id']);
					if($last_double_check == $post_meta['id'] ){ 

					}else{ $result[] = $post_meta; }
					//pre($post_meta);die;
				}
				$post = $result;
			}	
		}

		if(count($post)> 0){
			return_data(true,"Feeds found.",$post);
		}else{
			return_data(false,"No Feeds found.",array());
		}
	}

	private function validate_get_fan_wall_for_user(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
	
	/* Extremly important function 
	*  Make Changes after taking a look on db structure of post 
	*/	
	public function get_single_post_with_user_id($request_post_id , $request_user_id){
		$post_meta = $this->fan_wall_model->get_basic_post_data($request_post_id);
		if(count($post_meta) < 1 ){
			return array();
		}
		/*********		check if post is liked  by user 		********/
		$originaly_post_type =  $post_meta['post_type']; 

		$post_meta['is_liked'] = $this->fan_wall_model->is_liked_post($post_meta['id'],$request_user_id); 
		$post_meta['is_bookmarked'] = $this->fan_wall_model->is_bookmarked_post($post_meta['id'],$request_user_id);

		if($post_meta['post_type'] == "user_post_type_mcq"){
			// get post data with post id 
			$post_meta['post_data'] = $this->fan_wall_model->get_user_post_type_mcq($post_meta['id']);
			$post_meta['post_data']['answer_given_by_user'] = "";
		}

		if($post_meta['post_type'] == "user_post_type_normal"){

			$post_meta['post_data'] = $this->fan_wall_model->get_user_post_type_normal($post_meta['id']);
		}

		$post_meta['post_data']['post_file'] = $this->create_s3_video_link($post_meta['id']);

		/*if($post_meta['post_type'] == "user_post_type_share"){


			// get parent post data
			$record  = $this->fan_wall_model->get_basic_post_data($post_meta['parent_id']);
			
			if($record['post_type'] == "user_post_type_mcq"){
				// get post data with post id 
				$post_meta['post_data'] = $this->fan_wall_model->get_user_post_type_mcq($record['id']);

				$post_meta['post_data']['answer_given_by_user'] = $this->fan_wall_model->get_user_answer_for_mcq(array("user_id"=>$request_user_id,"mcq_id"=>$post_meta['post_data']['id']));

				$post_meta['post_type'] = "user_post_type_mcq";
			}

			if($record['post_type'] == "user_post_type_normal"){

				$post_meta['post_data'] = $this->fan_wall_model->get_user_post_type_normal($record['id']);
				$post_meta['post_type'] = "user_post_type_normal";
			}

			$post_meta['post_data']['post_file'] = $this->create_s3_video_link($post_meta['parent_id']);
		}*/
		
		
		// add post owner information 
		$post_meta['post_owner_info'] = $this->fan_wall_model->get_post_owner_info($post_meta['user_id']);
		$post_meta['tagged_people'] = $this->fan_wall_model->get_post_tagged_people($post_meta['id']);
		/*if($originaly_post_type == "user_post_type_share"){
			$information =  $this->fan_wall_model->get_info_of_shared_post_owner($post_meta['parent_id']);
			$post_meta['post_shared_of'] =  array(
																	"id"=> $information['id'],
																	"name"=> $information['name']
																);
		}*/

		return $post_meta;
	}


	Public function get_single_post_data_for_user(){
		$this->validate_get_single_post_data_for_user();

		$post_data = $this->get_single_post_with_user_id($this->input->post('post_id'),$this->input->post('user_id'));
		if(count($post_data) < 1 ){
			return_data(false,"Post not found",array());
		}
		return_data(true,"Post",$post_data);
	}

	private function validate_get_single_post_data_for_user(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/* INTERNAL*/

	public function get_post_with_user_internal($data){
		$record  = $this->get_single_post_with_user_id($data['post_id'],$data['user_id']);
		return $record;
	}

	/* create video link with aws s3 */

	public function create_s3_video_link($post_id){
		$data = $this->fan_wall_model->get_user_post_file_meta($post_id);
		$temp = array();
		foreach($data as $d){
			if($d['file_type'] == "video"){

					if($this->agent->is_mobile() &&  $this->agent->agent != "" ){
						$var = explode("/",$this->agent->agent);
						if(strtolower($var[0]) == "dalvik"){
							$d['link'] = $d['link'];
						}else{
							$d['link'] = $this->aws_create_video_link($d['link']);
						}
					}else{
						$d['link'] = $this->aws_create_video_link($d['link']);
					}
			}
			$temp[] = $d; 
		}
		return $temp;
	}

	private function aws_create_video_link($name) {
		require_once(FCPATH.'aws/aws-autoloader.php');
		$config = $this->aws_config();
		$s3 = S3Client::factory([
		'version'     => 'latest',
        'region'      => 'us-east-2',
		'key' => $config['s3']['key'],
		'secret' => $config['s3']['secret']
 		]);
 		$cloudfront = CloudFrontClient::factory([
		'version'     => 'latest',
        'region'      => 'us-east-2',
		'private_key' => FCPATH.'pk-APKAINLPC4YDYYIC6ZOA.pem',
		'key_pair_id' => 'APKAINLPC4YDYYIC6ZOA'
 		]);
 		$object = $name;
		$expiry = new DateTime('+100 minute');
	
		$url = $cloudfront->getSignedUrl([
				'url' => "{$config['cloudfront']['url']}/{$object}",
				'expires' => $expiry->getTimestamp(),
				'private_key' => 'pk-APKAINLPC4YDYYIC6ZOA.pem',
				'key_pair_id' => 'APKAINLPC4YDYYIC6ZOA'
		]);

		return $url;
	}

	public function aws_config() {
		return [
		's3' => [
			'key' => 'AKIAJOX5DIUSM6P6AIYA',
			'secret' => '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',
			'bucket' => 'dams-apps-production'
		],
		'cloudfront' => [
			'url' => 'https://d3qn7x9z1pnfz8.cloudfront.net'
		]
	];
	}


	// create video link with video name from database name to amazone server 

	public function on_request_create_video_link(){
		$this->validate_on_request_create_video_link();
		$link = $this->aws_create_video_link($this->input->post("name"));
		return_data(true,"Link view.",array("link"=>$link),array());
	}

	public function validate_on_request_create_video_link(){
		post_check();

		$this->form_validation->set_rules('name','name', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

}