<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_hit extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("video_model");
		$this->load->model("user_follow_model");
	}
    /*
    * we are using this api to get
    * people you may know list of 50
    * expert list of 50
    * all banner list
    */
	public function content(){
        $this->validate_content();
		$user_id = $this->input->post('user_id');

        $rand =  rand(1498634128235 , milliseconds());
		$query = $this->db->query(" SELECT u.id, u.name, u.profile_picture
									FROM users AS u
									JOIN user_registration_data AS urd ON u.id = urd.user_id
									JOIN user_registration_data AS urdv ON $user_id = urdv.user_id
									LEFT JOIN user_follow as uf on $user_id = uf.follower_id and u.id = uf.user_id
									WHERE u.is_course_register = 1
									AND u.id != $user_id
									AND urd.master_id = urdv.master_id
									AND uf.id IS NULL
									AND u.creation_time < $rand

									LIMIT 0 , 20 ");
									/*order by u.id desc */
        $return['people_you_may_know_list'] = $query->result_array();

		$query = $this->db->query(" SELECT u.id, u.name, u.profile_picture , u.followers_count , uf.follower_id , u.designation
									FROM users AS u
									LEFT JOIN user_follow as uf on  $user_id = uf.follower_id  and  u.id = uf.user_id
									WHERE  u.id != $user_id and u.is_expert = 1 
									LIMIT 0 , 50 ");
		$expert_list = $query->result_array();
		$temp = array();
		foreach($expert_list as $d){
			$d['watcher_following'] = false;
			if($d['follower_id'] != null ){
				$d['watcher_following'] = true;
			}
			$temp[] = $d;
		}
		$return['expert_list'] = $temp;

        $query = $this->db->query(" SELECT *  FROM fan_wall_banner");

        $return['banner_list'] =  array();//$query->result_array();
		/* get all tags */
		$return['all_tags'] =  $this->db->get('post_tags')->result_array();

		$sub_id = $this->input->post('sub_cat');
		$return['suggested_videos'] =  array();// $this->video_model->get_videos_for_cat($user_id,$sub_id,"time","",0,"");

		//$return['suggested_videos_query'] = $this->db->last_query();
		/* get suggested courses */

		$date = date("y-m-d");
		// $query = $this->db->query("SELECT `id`, `title`, `course_main_fk`, `course_category_fk`,
		// 							`subject_id`, `description`, `cover_image`, `desc_header_image` ,
		// 							`cover_video`, `tags`, `mrp`, `publish`, `is_new`, `state`,
		// 							`for_dams`, `non_dams`, `instructor_id`, `instructor_share`,
		// 							 course_rating_count as rating , `course_review_count` as review_count,
		// 							 `course_learner` as learner
		// 							 from course_master
		// 							 where in_suggested_list = '1'
		// 							 and  publish = 1 and state = 0
		// 							 and (`start_date` = '0000-00-00' or ('$date' >= start_date and '$date' <= end_date )) limit 0 , 10 ");
		//
		$return['suggested_course'] =  array();// $query->result_array();

		

		if(API_REQUEST_LANG == 2 ){
			$name = " name_2 as name";
		}else{
			$name = " name ";
		}

		$return['course_subject_master'] = $this->db->select("id,$name,image,color_code")->where('status',0)->get('course_subject_master')->result_array();
		$return['course_type_master'] = $this->db->select("id,$name")->get('course_type_master')->result_array();
        return_data(true,'Master hit content',$return,array());
	}

    public function validate_content(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		if($this->input->post('sub_cat') == "" || !array_key_exists('sub_cat',$_POST)){
			$_POST['sub_cat'] = 0;
		}

		if(array_key_exists('user_info',$_POST) && array_key_exists('user_id',$_POST)){
			$array = array('user_info'=>$_POST['user_info']);
			$this->db->where('id',$this->input->post('user_id'));
			$this->db->update('users',$array);
		}

        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();

        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }
    }

	public function daily_dose_menu(){
		$this->validate_daily_dose_menu();
		if(API_REQUEST_LANG == 2 ){
			$name = " name_2 as name ";
		}else{
			$name = " name ";
		}
		$this->db->select("id , $name , image , child_type");
		$data  = $this->db->get('menu_master')->result();

		return_data(true,'Menu.',$data);
	}

    public function validate_daily_dose_menu(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');

        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();

        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }
    }

}
