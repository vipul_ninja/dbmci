<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Live_stream extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function top_video_stream(){
		$this->validate_top_video_stream();

		$this->db->where("id",1);
		$live_row = $this->db->get('live_manager')->row_array();

		if(count($live_row) > 0 && $live_row['state'] == 1 ){
			$user_id =  $live_row['user_id'];

			$this->db->where('id',$user_id);
			$record =  $this->db->get('users')->row_array();

			$data = array(
				"id"=>$record['id'],
				"name"=>$record['name'],
				"profile_picture"=>$record['profile_picture'],
				"hls"=>"http://dwl0ojped2eu.cloudfront.net/hls-live/livepkgr/_definst_/liveevent/livestream.m3u8",
				);
			return_data(true,'Streaming is available',$data);
		}
		
		return_data(false,'Streaming is not  available',array());
	}


	private function validate_top_video_stream(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
}	