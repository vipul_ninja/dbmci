<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\CloudFront\CloudFrontClient;

class Fan_wall extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->helper("data_meta");
		$this->load->model("fan_wall_model");
		// get agent
		$this->load->library('user_agent');
	}

	public function get_fan_wall_for_user(){

		$this->validate_get_fan_wall_for_user();

		/* very importance post types EDIT CAREFULLY*/
		$common_types =  array("user_post_type_normal",
														"user_post_type_review",
														"user_post_type_vocab",
														"user_post_type_article",
														"user_post_type_current_affair",
														"user_post_type_quiz",
														"user_post_type_video",
												);
		$mcq_coins=get_db_meta_key('MCQ_COINS');

		$request_user_id = $this->input->post('user_id');
		/*Requester User Details*/
		$request_user_details = $this->fan_wall_model->request_user_details_by_id($request_user_id);
		$request_user_dams_token = $request_user_details['dams_tokken'];
		$request_user_is_live_agent = $request_user_details['is_live_agent'];

		/* for pagination */
		if(!array_key_exists('last_post_id', $_POST)){
			$last_post_id = "";
		}else{
			$last_post_id = $this->input->post('last_post_id');
		}

		/* post with tag id */
		if(!array_key_exists('tag_id', $_POST)){
			$tag_id = "";
		}else{
			$tag_id = $this->input->post('tag_id');
		}

		/* post with search text */
		if(!array_key_exists('search_text', $_POST)){
			$search_text = "";
		}else{
			$search_text = $this->input->post('search_text');
			$search_text = addslashes($search_text);
		}

		/* get all posts */
		$post = array();
		$post_meta['post_data'] = "";

		/* has same behaviour like fan wall but it is for bookmarks */
		if($this->input->get('bookmark_request') == "yes"){ /* result sent to user saved notes */
			
			$input =  array(
						"user_id"=>$request_user_id,
						"last_post_id"=>$last_post_id,
						"tag_id"=> $tag_id,
						"search_text"=> $search_text,
						'filter_type'=>$this->input->get('filter_type')
						);

			foreach($this->fan_wall_model->all_bookmaked($input) as $post_meta){

				if($post_meta['area'] == 1 ){
					$temp = $this->get_single_post_with_user_id($post_meta['post_id'],$request_user_id);
					$temp['segment_type'] = 'post';
					$post[]  = $temp;

				}

				if($post_meta['area'] == 2 ){
					$temp = $this->db->where(array('id'=>$post_meta['post_id']))->get('course_topic_file_meta_master')->row();
					$temp->segment_type = 'concept';
					$temp->is_bookmarked = true;
					$post[]  = $temp;
				}
			}
		}elseif($this->input->get('is_watcher')){ /* result sent to user persnal screen  */

			$input =  array(
						"user_id"=>$request_user_id,
						"last_post_id"=>$last_post_id,
						"tag_id"=> $tag_id,
						"search_text"=> $search_text
						);
			// get all_post_ids

			$all_posts = $this->fan_wall_model->all_user_post($input);

			$request_user_id = $this->input->get('is_watcher');
			if(count($all_posts) > 0 ){
				$ids = $mcq = $normal = array();
				foreach($all_posts as $ap ){
					$ids[] = $ap['id'];
					if($ap['post_type'] == "user_post_type_mcq"){ $mcq[] = $ap['id']; }
					if(in_array($ap['post_type'], $common_types)){ $normal[] = $ap['id']; }
					$post_user[]=$ap['user_id'];
				}
				// get user likes w.r.t. multiple post ids is user_liked_post_multiple
				$like_chunk = $this->fan_wall_model->is_liked_post_multiple($ids,$request_user_id);
				$like_chunk = explode(",",$like_chunk);
				$follow_data = $this->fan_wall_model->users_followers_check($post_user,$request_user_id);
                  //echo $this->db->last_query(); die;
				$follow_data = explode(",",$follow_data);
				
				//get bookmarks
				$bookmark_chunk = $this->fan_wall_model->is_bookmarked_post_multiple($ids,$request_user_id);
				$bookmark_chunk = explode(",",$bookmark_chunk);
				//mcq chunk
				$mcq_chunk = array();
				if(count($mcq)> 0){
					$mcq_chunk = $this->fan_wall_model->get_user_post_type_mcq_multiple($ids);
				}

				//normal chunk
				$normal_chunk = array();
				if(count($normal)> 0){
					$normal_chunk = $this->fan_wall_model->get_user_post_type_normal_multiple($ids);
				}

				foreach($all_posts as $post_meta){
					$post_meta['is_liked'] = (in_array($post_meta['id'], $like_chunk))?true:false;
					$post_meta['is_bookmarked'] = (in_array($post_meta['id'], $bookmark_chunk))?true:false;
					$post_meta['is_followed'] = (in_array($post_meta['user_id'], $follow_data))?true:false;
					
					if($post_meta['post_type'] == "user_post_type_mcq"){
						// get post data with post id
						foreach($mcq_chunk as $mc){
							if($mc['post_id'] == $post_meta['id']){
								$post_meta['post_data'] = $mc;
							}
						}
						$post_meta['post_data']['answer_given_by_user'] = $this->fan_wall_model->get_user_answer_for_mcq(array("user_id"=>$request_user_id,"mcq_id"=>$post_meta['post_data']['id']));
						$post_meta['post_data']['mcq_voting'] = $this->fan_wall_model->calculate_percentage_with_mcq_pk($post_meta['post_data']['id']);
					 
						$post_meta['post_data']['mcq_coins']=$mcq_coins;
						$post_meta['post_data']['mcq_attempt'] = $this->db->select(' count(DISTINCT(user_id)) as total ')->where('mcq_id', $post_meta['post_data']['id'])->get('user_post_type_mcq_answers')->row()->total;
						
						//echo $this->db->last_query();
					}
					if(in_array($post_meta['post_type'], $common_types)){
						foreach($normal_chunk as $nc){
							if($nc['post_id'] == $post_meta['id']){
								$post_meta['post_data'] = $nc;
							}
						}
						// check quiz id if avail
						if($post_meta['post_type'] == "user_post_type_quiz"){
							$this->db->where('post_id',$post_meta['id']);
							$post_meta['quiz_id'] = $this->db->select('quiz_id')->get('user_post_quiz_relationship')->row()->quiz_id;
						}
						if($post_meta['post_type'] == "user_post_type_video"){
							$this->db->where('post_id',$post_meta['id']);
							$post_meta['video_id'] = $this->db->select('video_id')->get('user_post_video_relationship')->row()->video_id;
						}
					}
					$post_meta['post_data']['post_file'] = $this->create_s3_video_link($post_meta['id']);
					$post_meta['post_owner_info']['id']   = $post_meta['user_id'];
					$post_meta['post_owner_info']['name']   = $post_meta['post_owner_name'];
					$post_meta['post_owner_info']['profile_picture']   = $post_meta['post_owner_profile_picture'];
					$post_meta['post_owner_info']['is_mentor']   = $post_meta['is_mentor'];
					$post_meta['post_owner_info']['is_expert']   = $post_meta['is_expert'];
					unset($post_meta['post_owner_name'],$post_meta['post_owner_profile_picture'],$post_meta['is_mentor'],$post_meta['is_expert'],$post_meta['optional_text']);

					$post_meta['tagged_people'] = $this->fan_wall_model->get_post_tagged_people($post_meta['id']);

					$post[] = $post_meta;
				}
			}
		}else{

			$input =  array(
				"user_id"=>$request_user_id,
				"last_post_id"=>$last_post_id,
				"tag_id"=> $tag_id,
				"search_text"=> $search_text
				);

			$result = array();
			/*
			* check if request is coming for expert user
			*/
			$input['get_expert_post'] =  0 ;
			if($this->input->get('get_expert_post') == 1){
				$input['get_expert_post'] = 1;
			}

			/*
			* check if request is coming for expert user
			*/
			$input['get_mentor_post'] =  0 ;
			if($this->input->get('get_mentor_post') == 1){
				$input['get_mentor_post'] = 1;
			}

			/*
			* check if request is coming for following user
			*/
			$input['get_follower_post'] =  0 ;
			if($this->input->get('get_follower_post') == 1){
				$input['get_follower_post'] = 1;
			}

			/* special task on client requirement  fetch medical post*/
			$input['get_only_feed'] = 0;
			if($this->input->get('get_only_feed') > 0){
				$input['get_only_feed'] = $this->input->get('get_only_feed');
			}

			$input['post_searching'] = "";
			if($this->input->get('post_searching') != ""){
				$input['post_searching'] = $this->input->get('post_searching');
			}
			$input['subject_id'] = (array_key_exists('subject_id',$this->input->post())?$this->input->post('subject_id'):'');
			// get all_post_ids
			$all_posts = $this->fan_wall_model->all_posts($input);
			//echo $this->db->last_query();die;
			$pinned = array();
			/* remove pinned post if searching for anything */
			if($last_post_id == 0 && $search_text == ""){
				$pinned = array();//$this->fan_wall_model->top_pinned_post($input);
				if(count($pinned) > 0 ){
					foreach($all_posts as $a){
						$pinned[] = $a;
					}
				}else{
					$pinned = $all_posts;
				}
				$all_posts = $pinned;
			}

			if(count($all_posts) > 0 ){
				$ids = $mcq = $normal = array();
				foreach($all_posts as $ap ){
					$ids[] = $ap['id'];
					if($ap['post_type'] == "user_post_type_mcq"){ $mcq[] = $ap['id']; }

					if(in_array($ap['post_type'], $common_types)){ $normal[] = $ap['id']; }
					$post_user[]=$ap['user_id'];

				}
				// get user likes w.r.t. multiple post ids is user_liked_post_multiple
				$like_chunk = $this->fan_wall_model->is_liked_post_multiple($ids,$request_user_id);
				$like_chunk = explode(",",$like_chunk);
				$follow_data = $this->fan_wall_model->users_followers_check($post_user,$request_user_id);
				//echo $this->db->last_query(); die;
			  $follow_data = explode(",",$follow_data);
			  
				//get bookmarks
				$bookmark_chunk = $this->fan_wall_model->is_bookmarked_post_multiple($ids,$request_user_id);
				$bookmark_chunk = explode(",",$bookmark_chunk);
				//mcq chunk

				$mcq_chunk = array();
				if(count($mcq)> 0){
					$mcq_chunk = $this->fan_wall_model->get_user_post_type_mcq_multiple($ids);
				}

				//normal chunk
				$normal_chunk = array();
				if(count($normal)> 0){
					$normal_chunk = $this->fan_wall_model->get_user_post_type_normal_multiple($ids);
				}

				foreach($all_posts as $post_meta){
					$post_meta['is_liked'] = (in_array($post_meta['id'], $like_chunk))?true:false;
					$post_meta['is_bookmarked'] = (in_array($post_meta['id'], $bookmark_chunk))?true:false;
					$post_meta['is_followed'] = (in_array($post_meta['user_id'], $follow_data))?true:false;
					if($post_meta['post_type'] == "user_post_type_mcq"){
						// get post data with post id

						foreach($mcq_chunk as $mc){
							if($mc['post_id'] == $post_meta['id']){
								$post_meta['post_data'] = $mc;
							}
						}

						$post_meta['post_data']['answer_given_by_user'] = $this->fan_wall_model->get_user_answer_for_mcq(array("user_id"=>$request_user_id,"mcq_id"=>$post_meta['post_data']['id']));
						$post_meta['post_data']['mcq_voting'] = $this->fan_wall_model->calculate_percentage_with_mcq_pk($post_meta['post_data']['id']);
						$post_meta['post_data']['mcq_coins']=$mcq_coins;
						$post_meta['post_data']['mcq_attempt'] = $this->db->select(' count(DISTINCT(user_id)) as total ')->where('mcq_id', $post_meta['post_data']['id'])->get('user_post_type_mcq_answers')->row()->total;
						
					}

					if(in_array($post_meta['post_type'], $common_types)){
						foreach($normal_chunk as $nc){
							if($nc['post_id'] == $post_meta['id']){
								$post_meta['post_data'] = $nc;
							}
						}
						// check quiz id if avail
						if($post_meta['post_type'] == "user_post_type_quiz"){
							$this->db->where('post_id',$post_meta['id']);
							$post_meta['quiz_id'] = $this->db->select('quiz_id')->get('user_post_quiz_relationship')->row()->quiz_id;
							$quiz_id = $post_meta['quiz_id'];
							$post_meta['quiz_coins'] = $this->db->select('reward_points')->where('id',"$quiz_id")->get('course_test_series_master')->row()->reward_points;
							$post_meta['quiz_attempt'] = $this->db->select(' count(DISTINCT(user_id)) as total ')->where('test_series_id',"$quiz_id")->get('course_test_series_report')->row()->total;
							$quiz_relation=$this->db->query("select total_questions,time_in_mins
							FROM `course_test_series_master` as `ctsm` where id='".$quiz_id. "'")->row_array();
						//	echo $this->db->last_query();
							$post_meta['quiz_no_of_questions'] =$quiz_relation['total_questions'] ;
							$post_meta['quiz_time']=$quiz_relation['time_in_mins'];
						}

						if($post_meta['post_type'] == "user_post_type_video"){

							$post_meta['video_id'] = $this->db->select('video_id')->where('post_id',$post_meta['id'])->get('user_post_video_relationship')->row()->video_id;
							$post_meta['display_picture'] = $this->db->select( " CASE WHEN thumbnail_url IS NULL THEN ''
							ELSE thumbnail_url END AS thumbnail_url ")->where('id',$post_meta['video_id'])->get('video_master')->row()->thumbnail_url;

						}
					}

					$post_meta['post_data']['post_file'] = $this->create_s3_video_link($post_meta['id']);

					$post_meta['post_owner_info']['id']   = $post_meta['user_id'];
					$post_meta['post_owner_info']['name']   = $post_meta['post_owner_name'];
					$post_meta['post_owner_info']['profile_picture']   = $post_meta['post_owner_profile_picture'];
					$post_meta['post_owner_info']['is_expert']   = $post_meta['is_expert'];
					$post_meta['post_owner_info']['is_mentor']   = $post_meta['is_mentor'];
					unset($post_meta['is_expert'],$post_meta['is_mentor'],$post_meta['post_owner_name'],$post_meta['post_owner_name'],$post_meta['post_owner_profile_picture']);

					$post_meta['tagged_people'] = $this->fan_wall_model->get_post_tagged_people($post_meta['id']);

					$result[] = $post_meta;
				}
				$post_meta['segment_type'] = 'post';
				//$query = "SELECT group_concat(distinct(vcsnm.name)) as name FROM post_category_relationship as pcr join course_stream_name_master as csnm on pcr.sub_cate_id = csnm.id join course_stream_name_master as vcsnm on vcsnm.id = csnm.parent_id WHERE pcr.post_id = ".$post_meta['id'];
				//$post_meta['post_tag'] = $this->db->query($query)->row()->name;
				
				$post = $result;
			}
		}

		if(count($post)> 0){
			return_data(true,"Feeds found.",$post);
		}else{
			return_data(false,"No Feeds found.",array());
		}
	}

	private function validate_get_fan_wall_for_user(){

		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		if($this->input->get('post_searching') == "user_post_type_quiz" ){
			$this->form_validation->set_rules('subject_id','subject_id', 'trim');
		}
		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/* Extremly important function
	*  Make Changes after taking a look on db structure of post
	*/
	public function get_single_post_with_user_id($request_post_id , $request_user_id){
		$post_meta = $this->fan_wall_model->get_basic_post_data($request_post_id);
		if(count($post_meta) < 1 ){
			return array();
		}
		/*********		check if post is liked  by user 		********/
		$originaly_post_type =  $post_meta['post_type'];
		$post_meta['segment_type'] = 'post';
		$post_meta['is_liked'] = $this->fan_wall_model->is_liked_post($post_meta['id'],$request_user_id);
		$post_meta['is_bookmarked'] = $this->fan_wall_model->is_bookmarked_post($post_meta['id'],$request_user_id);

		$mcq_coins=get_db_meta_key('MCQ_COINS');
		
		if($post_meta['post_type'] == "user_post_type_mcq"){
			// get post data with post id
			$post_meta['post_data'] = $this->fan_wall_model->get_user_post_type_mcq($post_meta['id']);
			$post_meta['post_data']['answer_given_by_user'] = $this->fan_wall_model->get_user_answer_for_mcq(array("user_id"=>$request_user_id,"mcq_id"=>$post_meta['post_data']['id']));
			$post_meta['post_data']['mcq_voting'] = $this->fan_wall_model->calculate_percentage_with_mcq_pk($post_meta['post_data']['id']);
			$post_meta['post_data']['mcq_coins']=$mcq_coins;
			$post_meta['post_data']['mcq_attempt'] = $this->db->select(' count(DISTINCT(user_id)) as total ')->where('mcq_id', $post_meta['post_data']['id'])->get('user_post_type_mcq_answers')->row()->total;	
			
		}
		/* very importance post types EDIT CAREFULLY*/
		$common_types =  array("user_post_type_normal",
										"user_post_type_vocab",
										"user_post_type_article",
										"user_post_type_current_affair",
										"user_post_type_quiz",
										"user_post_type_video"
												);

		if(in_array($post_meta['post_type'],$common_types)){

			$post_meta['post_data'] = $this->fan_wall_model->get_user_post_type_normal($post_meta['id']);

		}


		if($post_meta['post_type'] == "user_post_type_quiz"){
			$this->db->where('post_id',$post_meta['id']);
			$post_meta['quiz_id'] = $this->db->select('quiz_id')->get('user_post_quiz_relationship')->row()->quiz_id;
			$quiz_id = $post_meta['quiz_id'];
			$post_meta['quiz_coins'] = $this->db->select('reward_points')->where('id',"$quiz_id")->get('course_test_series_master')->row()->reward_points;
			$post_meta['quiz_attempt'] = $this->db->select(' count(DISTINCT(user_id)) as total ')->where('test_series_id',"$quiz_id")->get('course_test_series_report')->row()->total;
			$quiz_relation=$this->db->query("select total_questions,time_in_mins
			FROM `course_test_series_master` as `ctsm` where id='".$quiz_id. "'")->row_array();
		//	echo $this->db->last_query();
			$post_meta['quiz_no_of_questions'] =$quiz_relation['total_questions'] ;
			$post_meta['quiz_time']=$quiz_relation['time_in_mins'];
		}


		if($post_meta['post_type'] == "user_post_type_video"){
			$this->db->where('post_id',$post_meta['id']);
			$post_meta['video_id'] = $this->db->select('video_id')->get('user_post_video_relationship')->row()->video_id;
		}
		$post_meta['post_data']['post_file'] = $this->create_s3_video_link($post_meta['id']);

		// add post owner information
		$post_meta['post_owner_info'] = $this->fan_wall_model->get_post_owner_info($post_meta['user_id']);
		$post_meta['tagged_people'] = $this->fan_wall_model->get_post_tagged_people($post_meta['id']);
		$post_meta['sub_cate_id'] = $this->db->query('select GROUP_CONCAT(sub_cate_id) as sub_cate_id from post_category_relationship where post_id = '.$post_meta['id'])->row()->sub_cate_id;
		return $post_meta;
	}


	Public function get_single_post_data_for_user(){
		$this->validate_get_single_post_data_for_user();

		$post_data = $this->get_single_post_with_user_id($this->input->post('post_id'),$this->input->post('user_id'));
		if(count($post_data) < 1 ){
			return_data(false,"Post not found",array());
		}
		return_data(true,"Post",$post_data);
	}

	private function validate_get_single_post_data_for_user(){

		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/* INTERNAL*/

	public function get_post_with_user_internal($data){
		$record  = $this->get_single_post_with_user_id($data['post_id'],$data['user_id']);
		return $record;
	}

	/* create video link with aws s3 */

	public function create_s3_video_link($post_id){
		$data = $this->fan_wall_model->get_user_post_file_meta($post_id);
		$temp = array();
		foreach($data as $d){
			if($d['file_type'] == "video"){

					if($this->agent->is_mobile() &&  $this->agent->agent != "" ){
						$var = explode("/",$this->agent->agent);
						if(strtolower($var[0]) == "dalvik"){
							$d['link'] = $d['link'];
						}else{
							$d['link'] = $this->aws_create_video_link($d['link']);
						}
					}else{
						$d['link'] = $this->aws_create_video_link($d['link']);
					}
			}
			$temp[] = $d;
		}
		return $temp;
	}

	private function aws_create_video_link($name) {
		require_once(FCPATH.'aws/aws-autoloader.php');
		$config = $this->aws_config();
		$s3 = S3Client::factory([
		'version'     => 'latest',
        'region'      => 'us-east-2',
		'key' => $config['s3']['key'],
		'secret' => $config['s3']['secret']
 		]);
 		$cloudfront = CloudFrontClient::factory([
		'version'     => 'latest',
        'region'      => 'us-east-2',
		'private_key' => FCPATH.'pk-APKAINLPC4YDYYIC6ZOA.pem',
		'key_pair_id' => 'APKAINLPC4YDYYIC6ZOA'
 		]);
 		$object = $name;
		$expiry = new DateTime('+100 minute');

		$url = $cloudfront->getSignedUrl([
				'url' => "{$config['cloudfront']['url']}/{$object}",
				'expires' => $expiry->getTimestamp(),
				'private_key' => 'pk-APKAINLPC4YDYYIC6ZOA.pem',
				'key_pair_id' => 'APKAINLPC4YDYYIC6ZOA'
		]);

		return $url;
	}

	public function aws_config() {
		return [
		's3' => [
			'key' => 'AKIAJOX5DIUSM6P6AIYA',
			'secret' => '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',
			'bucket' => 'dams-apps-production'
		],
		'cloudfront' => [
			'url' => 'https://d3qn7x9z1pnfz8.cloudfront.net'
		]
	];
	}


	// create video link with video name from database name to amazone server

	public function on_request_create_video_link(){
		$this->validate_on_request_create_video_link();
		$link = $this->aws_create_video_link($this->input->post("name"));
		return_data(true,"Link view.",array("link"=>$link),array());
	}

	public function validate_on_request_create_video_link(){
		post_check();

		$this->form_validation->set_rules('name','name', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}



	public function suggested_post(){
		$this->validate_suggested_post();
		$data_to_match=$this->input->post('string_to_match');
		$request_user_id=$this->input->post('user_id');
		
		$suggested_post_id=$this->fan_wall_model->get_suggested_post($data_to_match);
		$post = array();
		foreach($suggested_post_id as $post_id ){
			$post[] = $this->get_single_post_with_user_id($post_id['post_id'],$request_user_id);
		}
		return_data(true,"Suggested Posts.",$post);
	}

	public function validate_suggested_post(){
		post_check();
		$this->form_validation->set_rules('string_to_match','string_to_match', 'trim|required');
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
}
