<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}


	function get_testimonial(){
$this->db->select('ut.*,users.name,users.profile_picture');
$this->db->join('users','ut.user_id=users.id');
    $data=	$this->db->get('user_testimonial as ut')->result_array();
    if(  count($data)>1 || count($data)==1){

        return_data(true,'User Testimonial',$data);
        
    }else{

        return_data(false,'No Testimonial Found',$data);
        
    }
	}
}
