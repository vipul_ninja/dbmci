<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_category_handling extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('user_category_handling_model');
		$this->load->helper("services");
	}

	public function get_category_basic(){
		
		return_data(true,'List fetched.',$this->user_category_handling_model->get_basic_list());
	}

	public function get_category_basic_level_one($id){
		
		return_data(true,'List fetched.',$this->user_category_handling_model->get_basic_list_level_one($id));
	}

	public function get_category_basic_level_two($id){
		
		return_data(true,'List fetched.',$this->user_category_handling_model->get_basic_list_level_two($id));
	}

	public function get_intersted_courses($id){
		$this->db->where('master_category',$id);
		$r = $this->db->get('course_intersted_in_title')->result();
		/*$temp = array();
		foreach($r as $t){
			$this->db->where('parent_id', $t->id);
			$temp[][$t->text] = $this->db->get('course_intersted_in_list')->result_array();
		}*/
		$temp = array();
		$temp["title"] = $r;
		$temp["title_data"] = $this->db->get('course_intersted_in_list')->result_array();
		return_data(true,'List fetched.',$temp);

	}
}	