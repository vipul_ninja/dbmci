<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\Sns\SnsClient;

class Mobile_auth extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("mobile_auth_model");
		$this->load->helper("message_sender_helper");
	}

	/* during sign up */
	public function send_otp(){

		$this->validate_send_otp();

		$to = $this->input->post('mobile');
		$c_code = $this->input->post('c_code');
		$otp =  rand(1000,9999);
		send_message_global($c_code,$to,CONFIG_PROJECT_NICK_NAME." OTP is $otp");

		// modules::run('data_model/emailer/Send_otp_on_email_sign_up/otp_for_sign_up',
		// 							array("otp"=>$otp,"email"=>$this->input->post('email'))
		// 						);
		return_data(true,'OTP sent successfully.',array('otp'=>$otp));
	}

	private function validate_send_otp(){

		post_check();

		//$this->form_validation->set_rules('email','Email address', 'trim|required|is_unique[users.email]');
		$this->form_validation->set_rules('mobile','Mobile number', 'trim|required|is_unique[users.mobile]');

		/* check country code */
		if(!array_key_exists('c_code', $_POST)){
			$_POST['c_code'] = "+91" ;
		}else{
			$this->form_validation->set_rules('c_code','Country code', 'trim|required');
		}

		//$this->form_validation->set_message('is_unique', '%s is already exist.');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	public function send_otp_for_change_password(){

		$this->validate_send_otp_for_change_password();
		$to = $this->input->post('mobile');
		$c_code = $this->input->post('c_code');
		$otp = rand(1000,9999);

		send_message_global($c_code,$to,CONFIG_PROJECT_NICK_NAME." otp for forget password is $otp");
		/* send email with otp
		* Do not change variable name
		*/
		$user_data =  $this->mobile_auth_model->get_user_with_mobile($this->input->post("mobile"));

		// modules::run('data_model/emailer/Send_otp_on_forget_password/send_otp_on_email',
		// 							array("otp"=>$otp,"email"=>$user_data['email'])
		// 						);

		$this->mobile_auth_model->add_otp($this->input->post("mobile"),$otp);

		return_data(true,'Successfully sent an otp on registered mobile number and on '.$user_data['email'].'.',array("otp"=>$otp));
	}

	private function validate_send_otp_for_change_password(){

		post_check();
		$this->form_validation->set_rules('mobile','mobile', 'trim|required');
		/* check country code */
		if(!array_key_exists('c_code', $_POST)){
			$_POST['c_code'] = "+91" ;
		}else{
			$this->form_validation->set_rules('c_code','Country code', 'trim|required');
		}
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}

		if(!$this->mobile_auth_model->get_user_with_mobile($this->input->post("mobile"))){

			return_data(false,'Mobile number is not valid or registered with social login.',array());
		}
	}

/***           
 *      _    _             _         _                            _      _  _
 *     | |  | |           | |       | |                          | |    (_)| |
 *     | |  | | _ __    __| |  __ _ | |_  ___   _ __ ___    ___  | |__   _ | |  ___
 *     | |  | || '_ \  / _` | / _` || __|/ _ \ | '_ ` _ \  / _ \ | '_ \ | || | / _ \
 *     | |__| || |_) || (_| || (_| || |_|  __/ | | | | | || (_) || |_) || || ||  __/
 *      \____/ | .__/  \__,_| \__,_| \__|\___| |_| |_| |_| \___/ |_.__/ |_||_| \___|
 *             | |
 *             |_|
 */
	public function send_otp_for_mobile_change(){

		$this->validate_send_otp_for_mobile_change();

		$to = $this->input->post('mobile');
		$c_code = $this->input->post('c_code');
		$otp =  rand(1000,9999);
		send_message_global($c_code,$to,CONFIG_PROJECT_NICK_NAME." otp for change mobile number is $otp");
		return_data(true,'OTP Sent.',array('otp'=>$otp));
	}

	private function validate_send_otp_for_mobile_change(){

		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('mobile','Mobile number', 'trim|required');
		/* check country code */
		if(!array_key_exists('c_code', $_POST)){
			$_POST['c_code'] = "+91" ;
		}else{
			$this->form_validation->set_rules('c_code','Country code', 'trim|required');
		}
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
		/* check if mobile number not in relation with another account */
		$m = $this->input->post('mobile');
		$u = $this->input->post('user_id');
		$out =  $this->db->query("select id from users where mobile = '$m' and id != '$u'")->row();
		if(count($out) > 0 ){
			return_data(false,'Mobile number is already registered with different account .',array());
		}
	}

	/* send message on successfull purchase */
	public function send_message_on_purchase($user = array()){
		$user_id =  $user['user_id'];
		$data =  $this->db->query("select c_code , mobile from users where id = '$user_id'")->row();
		$to = $data->mobile;
		$c_code = $data->c_code;
		send_message_global($c_code,$to,"Thanks for doing transaction in  ".CONFIG_PROJECT_NICK_NAME." . Payment successfull.");
	}

	public function send_while_user_submit_feedback($user = array()){
		$user_id =  $user['user_id'];
		$data =  $this->db->query("select c_code , mobile from users where id = '$user_id'")->row();
		$to = $data->mobile;
		$c_code = $data->c_code;
		send_message_global($c_code,$to,"Thanks for providing feedback on ".CONFIG_PROJECT_NICK_NAME." !");
	}
}
