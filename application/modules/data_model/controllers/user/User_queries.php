<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_queries extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('user_queries_model');
		$this->load->helper("services");

	}

	public function submit_query(){

		$this->validate_submit_query();
		$this->user_queries_model->submit_query($this->input->post());
		/* send email to admin */
		$user = services_helper_user_basic($this->input->post('user_id'));
		$email['from'] = $user['email'];
		$email['name'] = $user['name'];
		$email['description'] =$this->input->post('description');

		modules::run('data_model/emailer/User_query_email/send_email_to_support', $email);
		/* send message to user */
		modules::run('data_model/user/Mobile_auth/send_while_user_submit_feedback',  array(
			"user_id"=>$this->input->post('user_id')));
		
		return_data(true,'Thank you, we have received your message, we will get back to you as soon as possible.');
	}

	private function validate_submit_query(){
		
		post_check();
		
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('category','category', 'trim|required');
		$this->form_validation->set_rules('title','title', 'trim|required');
		$this->form_validation->set_rules('description','description', 'trim|required');
		
		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

}