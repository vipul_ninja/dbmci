<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dams_user_verification extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function user_verification(){

		$this->validate_user_verification();
		if($this->check_api($this->input->post('user_tokken')) == True){
			return_data(true,'Id is valid.');
		}else{
			return_data(false,'Dams id is not valid.');
		}
		
	}

	private function validate_user_verification(){
		
		post_check();

		$this->form_validation->set_rules('user_tokken','user_tokken', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function check_api($tokken){


		$option = array(
			'url' => 'http://dams.sisonline.in/MYAPI.asmx/ProfileDetail?usrname="'.$tokken.'"'
		);

		$ch = curl_init($option['url']);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		curl_close($ch);
		$result = str_replace("(","",$result);
		$result = str_replace(");","",$result);
		$result = json_decode($result);
		if(is_array($result) && array_key_exists('AdmnNo', $result[0]) && $result[0]->AdmnNo != ""){
			//pre($result[0]->AdmnNo);
			return true;
		}
		return false;
	}


}
