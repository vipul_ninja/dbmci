<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_mobile_change extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function update_mobile_number(){
		
		$this->validate_update_mobile_number();
		$mobile = $this->input->post('mobile');
		$c_code = $this->input->post('c_code');
		$this->db->where('id',$this->input->post('user_id'));
		$this->db->update('users',array('mobile'=>$mobile,'c_code'=>$c_code));
		return_data(true,'Mobile Number verification done successfully.',array());
	}

	private function validate_update_mobile_number(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('mobile','Mobile number', 'trim|required');
		/* check country code */
		if(!array_key_exists('c_code', $_POST)){ 
			$_POST['c_code'] = "+91" ;
		}else{
			$this->form_validation->set_rules('c_code','Country code', 'trim|required');
		}

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
		/* check if mobile number not in relation with another account */
		$m = $this->input->post('mobile');
		$u = $this->input->post('user_id');
		$out =  $this->db->query("select id from users where mobile = '$m' and id != '$u'")->row();
		if(count($out) > 0 ){
			return_data(false,'This mobile number is already registered with another account .',array());
		}
	}
}
