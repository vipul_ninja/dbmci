<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_meta_tags extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function get_list_tags($user_id=""){
		if($user_id == ""){
			return_data(false,"Please send user id.",array());
		}
		$query = $this->db->query(" SELECT id , text , status from post_tags
									where master_id = (
															SELECT master_id
															FROM user_registration_data
															WHERE user_id = '$user_id'
														) 
									And status = 1 	order by position 				
									");

		$data = $query->result_array();
		
		if($data){
			return_data(true,"Tags list",$data);
		}
		return_data(false,"No tags found.",array());
	}

}	