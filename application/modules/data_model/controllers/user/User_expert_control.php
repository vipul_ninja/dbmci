<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_expert_control extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function make_user_expert(){

		$this->validate_make_user_expert();
        $user_for =$this->input->post('user_for');
        $this->db->where('id',$user_for);
        $this->db->update('users',array('is_expert'=>1));
		return_data(true,'Expert role assigned to user.');
	}

	private function validate_make_user_expert(){
		post_check();
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('user_for','user_for', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
    }
    
	public function remove_user_expert(){
        $this->validate_remove_user_expert();
        $user_for =$this->input->post('user_for');
        $this->db->where('id',$user_for);
        $this->db->update('users',array('is_expert'=>0));
        return_data(true,'Expert role removed from user.');
    }

    private function validate_remove_user_expert(){
        post_check();
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('user_for','user_for', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }
    }    

}