<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_hide extends MX_Controller {	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function hide_post(){
		//$this->validate_hide_post();		
		$data =array(
			'user_id'=>$this->input->post('user_id'),
			'post_id'=>$this->input->post('post_id'),
			'creation_time'=>'');
		
		$this->db->where('user_id',$data['user_id']);	
		$this->db->where('post_id',$data['post_id']);	
		$result	=$this->db->get('user_post_hidden')->row();
		if(count($result) > 0 ){
			return_data(true,'Post Hide successfully.',array());
		}else{
			$this->db->insert('user_post_hidden',$data);
		}
		return_data(true,'Post Hide successfull.',array());
	}

	private function validate_hide_post(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
}	