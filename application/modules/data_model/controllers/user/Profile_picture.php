<?php

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_picture extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
   }
   
   Public function update_profile_picture(){
      $this->validate_profile_picture();
      $user_id = $this->input->post('user_id');
      /* get image */
      $sTempFileName = $this->input->post('url');
      $aSize = getimagesize($sTempFileName);
      /* quality of image */
    //  echo $aSize;

      $image_ori_width = $aSize[0];
      $image_ori_height = $aSize[1];

      switch($aSize[2]) {
         case IMAGETYPE_JPEG:
         $sExt = '.jpg';
         $vImg = @imagecreatefromjpeg($sTempFileName);
         $info_source_base_64 = "data:image/jpeg;base64,";
         break;
         case IMAGETYPE_PNG:
         $sExt = '.png';
         $vImg = @imagecreatefrompng($sTempFileName);
         $info_source_base_64 = "data:image/png;base64,";
         break;
      }

      $iJpgQuality = 100;
      /* required width */
      $iWidth = 300;
      $iHeight = 300 / ($image_ori_width/$image_ori_height); 
      // create a new truee color image
      $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
      // copy and resize part of an image with resampling
      $data =    imagecopyresampled($vDstImg, $vImg, 0, 0, 0, 0, $iWidth, $iHeight, $image_ori_width, $image_ori_height);
      ob_start ();
      imagejpeg($vDstImg,null, $iJpgQuality);
      $image_data = ob_get_contents ();
      ob_end_clean ();
      $source_base_64 = $info_source_base_64.base64_encode($image_data);

      $file_name = rand(99999,999999999);
      $image_ori = $this->amazon_s3_upload_with_base64('user_display',$file_name,$source_base_64);

      $iJpgQuality = 100;
      $iWidth =200;
      $iHeight = 200 / ($image_ori_width/$image_ori_height); 
      $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
      // copy and resize part of an image with resampling
      $data =    imagecopyresampled($vDstImg, $vImg, 0, 0, 0, 0, $iWidth, $iHeight, $image_ori_width, $image_ori_height);
      ob_start ();
      imagejpeg($vDstImg,null, $iJpgQuality);
      $image_data = ob_get_contents ();
      ob_end_clean ();
      $source_base_64 = $info_source_base_64.base64_encode($image_data);
      $this->amazon_s3_upload_with_base64('user_display_200',$file_name,$source_base_64);

      $iWidth =100;
      $iHeight = 100 / ($image_ori_width/$image_ori_height); 
      $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
      // copy and resize part of an image with resampling
      $data =    imagecopyresampled($vDstImg, $vImg, 0, 0, 0, 0, $iWidth, $iHeight, $image_ori_width, $image_ori_height);
      ob_start ();
      imagejpeg($vDstImg,null, $iJpgQuality);
      $image_data = ob_get_contents ();
      ob_end_clean ();
      $source_base_64 = $info_source_base_64.base64_encode($image_data);
      $this->amazon_s3_upload_with_base64('user_display_100',$file_name,$source_base_64);

      $array = array('profile_picture'=>$image_ori);
      $this->db->where('id',$user_id)->update('users',$array);

      return_data(true,"Picture updated successfully.",$array);
   }

   private function validate_profile_picture(){
      post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('via','via', 'trim|required');
		$this->form_validation->set_rules('url','url', 'trim|required|prep_url');
		
	
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
   }

   private function amazon_s3_upload_with_base64($aws_path,$file_name,$source_base_64) {
	 $image_parts = explode(";base64,", $source_base_64);
    $image_type_aux = explode("image/", $image_parts[0]);
    $image_type = $image_type_aux[1];
    $image_base64 = base64_decode($image_parts[1]);
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.$file_name,
							'ContentType'=> 'image/' . $image_type,
							'Body' => $image_base64,
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array('param1' => 'value 1','param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
   }

}