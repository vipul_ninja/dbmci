<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_pinning extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("post_pinning_model");
    }
 
    public function pin_a_post(){
        $this->validate_pin_a_post();
    
        $input['user_id'] = $user_id = $this->input->post('user_id');
        $input['post_id'] = $post_id = $this->input->post('post_id');

        // check if user is modrated 
        $checker = $this->post_pinning_model->check_if_user_is_modrated($user_id);
        if($checker == false){
            return_data(false,'you are not authorised to perform this action.',array());
        }
        $this->post_pinning_model->set_post_as_pinned($input);

        /* get post data */
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
                                    array( "post_id"=>$post_id,"user_id"=>$user_id)
                                    );

        return_data(true,'Post pinned successfully.',$post_data);
    }

    private function validate_pin_a_post(){
        post_check();
        
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('post_id','post_id', 'trim|required');

        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();

        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }

    }

    public function pin_a_post_remove(){
        $this->validate_pin_a_post_remove();
        $input['user_id'] = $user_id = $this->input->post('user_id');
        $input['post_id'] = $post_id = $this->input->post('post_id');

        // check if user is modrated 
        $checker = $this->post_pinning_model->check_if_user_is_modrated($user_id);
        if($checker == false){
            return_data(false,'you are not authorised to perform this action.',array());
        } 

        $this->post_pinning_model->remove_post_from_pinned($input);

        /* get post data */
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
        array( "post_id"=>$post_id,"user_id"=>$user_id)
        );
        return_data(true,'Post removed from pinned successfully.',$post_data );
    }

    private function validate_pin_a_post_remove(){
        post_check();
        
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('post_id','post_id', 'trim|required');

        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();

        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }
    }

}    