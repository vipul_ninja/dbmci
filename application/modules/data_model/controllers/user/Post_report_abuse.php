<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_report_abuse extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("post_report_abuse_model");
	}

	Public function report(){
		
		$this->validate_report();
		$this->post_report_abuse_model->report($this->input->post());
		return_data(true,$this->lang->line('report_abuse_done'));
	}

	private function validate_report(){

		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->set_rules('reason_id','reason_id', 'trim');
		$this->form_validation->set_rules('comment','comment', 'trim');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
		
		/* check if already reported */
		if($this->post_report_abuse_model->is_already_reported($this->input->post())){
			return_data(false,$this->lang->line('report_abuse_already'));
		}		
	}
	
	
	public function get_all_report_reasons(){

       $reasons =  $this->post_report_abuse_model->report_reasons(API_REQUEST_LANG);
		
        if($reasons){
            $data['report_reasons'] = $reasons;          
            return_data(true,"Report Reasons",$data);
        }
        return_data(false,"Report reasons not found.",array());
    }

}