<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_delete extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function delete_post(){
		$this->validate_delete_post();
		// check if user is owner of post 
		$this->db->where("user_id",$this->input->post('user_id'));		
		$this->db->where("id",$this->input->post('post_id'));		
		$post = $this->db->get('post_counter')->row_array();
		if($post){
			$id= $this->input->post('post_id');
			$where = 'id = "'. $id .'" OR parent_id = "'.$id.'"';
			$update=array('status'=>1);
			$this->db->where($where);
			$this->db->update('post_counter',$update);
			// update counter while deleting for perticuler user 
			$count = $this->db->query('select count(id) as total from post_counter where status = 0 and parent_id = 0 and  user_id ='.$this->input->post('user_id'))->row_array();
			$count = ($count && $count['total'] > 0 )?$count['total']:0;		

			$this->db->where('id',$this->input->post('user_id'));
			$this->db->set('post_count', $count);
			$this->db->update("users");

			return_data(true,'Post deleted.',array());
		}else{
			return_data(false,'Sorry you can not delete post of other users.',array());	
		}
		
	}

	private function validate_delete_post(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
}	