<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_reward extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
        $this->load->model("user_reward_model");
        // dependency of sudo yum  install php70-bcmath.x86_64  on centos aws
        // for ubuntu sudo apt install php-bcmath
        $this->load->library('pseudocrypt');
    }
    
    public function get_user_rewards(){
        $this->validate_get_user_rewards();
        $user_id =  $this->input->post('user_id');
        $info =  $this->user_reward_model->get_user_rewards($user_id);
        if(count($info)> 0 ){
            $info->refer_code =  $this->pseudocrypt->hash($info->id,7);
            $info->conversion_rate = get_db_meta_key('RUPEE_CONVERSION_RATE'); 
            $info->total_earned_point = $this->db->query("SELECT sum(reward) as total  FROM user_activity_rewards where user_id = $user_id and reward > 0" )->row()->total;
            $info->current_wallet = (string)(int)($info->reward_points/$info->conversion_rate);
            $info->total_earned_wallet = (string)(int)($info->total_earned_point/$info->conversion_rate);
            $info->minimum_coin_to_redeem =  get_db_meta_key('MINIMUM_COIN_TO_REDEEM'); ;

            return_data(true,'User info.',$info);
        }
        return_data(false,'User information not found.',array());
    }  
    
	private function validate_get_user_rewards(){
        
        post_check();
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');

        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();

        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }	
    }    

    public function give_user_rewards_point($data){
        $refer_code = $data['refer_code'];
        $used_by    = $data['used_by'];
        $refer_owner =  $this->pseudocrypt->unhash($refer_code);
        $give_points =  get_db_meta_key('REFER_POINTS'); 
        /* put entry in db */
        /* check in db */
        $find = $this->db->where(array('user_id'=>$used_by,'refer_code'=>$refer_code,'code_owner'=>$refer_owner))->get('user_refer_earn')->row();
        if(count($find) > 0 ){

        }else{
            $array = array('user_id'=>$used_by,'refer_code'=>$refer_code,'code_owner'=>$refer_owner,'earning'=>$give_points);
            if($this->db->insert('user_refer_earn',$array)){
                // update user points
                activity_rewards($refer_owner,"REFER_POINTS",$do="+");
            }
        }
    }
	
	public function get_reward_transaction(){
        $this->validate_get_reward_transaction();
        $user_id =  $this->input->post('user_id');
        $info =  $this->user_reward_model->get_reward_transaction($user_id,$this->input->post('last_id'));
        if(count($info)> 0 ){           
            return_data(true,'Reward transaction info.',$info);
        }
        return_data(false,'Reward transaction information not found.',array());
    }  
    
	private function validate_get_reward_transaction(){
        
        post_check();
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');
        if(!array_key_exists('last_id', $_POST)){
			$_POST['last_id'] = "";
		}else{
			$this->input->post('last_id');
		}
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();

        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }	
    } 

}    