<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_bookmarks extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("post_bookmarks_model");
	}

	public function add_to_bookmarks(){

		$this->validate_add_to_bookmarks();
		// first check if already bookmarked

		if($this->post_bookmarks_model->is_already_bookmarked($this->input->post())){
			return_data(false,'Post already added to bookmarks');
		}

		$this->post_bookmarks_model->add_to_bookmarks($this->input->post());
		$user_id = $this->input->post('user_id');
		$post_id = $this->input->post('post_id');
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
									array( "post_id"=>$post_id,"user_id"=>$user_id)
								);
		
		if($this->input->post('area')  == 2 ){
			$bookmark_text = $this->lang->line('concept_added_to_bookmarks');
		}else{
			$bookmark_text = $this->lang->line('post_added_to_bookmarks');
		}						
		return_data(true,$bookmark_text,$post_data);
	}

	private function validate_add_to_bookmarks(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->set_rules('area','area', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function remove_from_bookmarks(){

		$this->validate_remove_from_bookmarks();
		$this->post_bookmarks_model->remove_from_bookmarks($this->input->post());

		$user_id = $this->input->post('user_id');
		$post_id = $this->input->post('post_id');
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
									array( "post_id"=>$post_id,"user_id"=>$user_id)
								);

		if($this->input->post('area')  == 2 ){
			$bookmark_text = $this->lang->line('concept_removed_from_bookmarks');
		}else{
			$bookmark_text = $this->lang->line('post_removed_from_bookmarks');
		}												
		return_data(true,$bookmark_text,$post_data);
	}

	private function validate_remove_from_bookmarks(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->set_rules('area','area', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}	

}