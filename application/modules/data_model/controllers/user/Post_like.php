<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_like extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("post_like_model");
	}

	public function like_post(){

		$this->validate_like_post();
		// first check if already liked 
		if($this->post_like_model->is_already_like_post($this->input->post())){
			return_data(false,'Post Already liked.');
		}

		$this->post_like_model->like_post($this->input->post());
		
		$user_id = $this->input->post('user_id');
		$post_id = $this->input->post('post_id');

		/* send push */
		modules::run('data_model/pusher/push_post_liked/push_on_like', 
									array( "post_id"=>$post_id,"user_id"=>$user_id)
								);
		
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
									array( "post_id"=>$post_id,"user_id"=>$user_id)
								);
		
		/*	Generate activity log 
		*   HIGHLY SENSITIVE CODE
		*/
		modules::run('data_model/notification_genrator/activity_logger/genrate_activity_while_post_like', 
									array( "action_for_user_id"=>$post_data['user_id'],"action_element_id"=>$post_id,"action_performed_by_user_id"=>$user_id)
								);		
		/*	 
		*   Give points to user 
		*	HIGHLY SENSITIVE CODE
		*   calling helper function 
		*/			
		// like points trigger 
		activity_rewards($user_id,'POST_LIKE','+');

		return_data(true,'Post liked.',$post_data);
	}

	private function validate_like_post(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	public function dislike_post(){

		$this->validate_dislike_post();
		$this->post_like_model->dislike_post($this->input->post());
		$user_id = $this->input->post('user_id');
		$post_id = $this->input->post('post_id');
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
									array( "post_id"=>$post_id,"user_id"=>$user_id)
								);
		activity_rewards($user_id,'POST_LIKE',"-");						
		return_data(true,'Post disliked.',$post_data);
	}

	private function validate_dislike_post(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function get_post_like_users(){
		
		$this->validate_get_post_like_users();
		$post_id = $this->input->post('post_id');
		$last_id = $this->input->post('last_id');
		if($this->input->get('is_watcher')){
			$data = $this->post_like_model->user_liked_list_with_watcher($post_id , $last_id , $this->input->get('is_watcher'));
		}else{
			$data = $this->post_like_model->user_liked_list($post_id , $last_id);	
		}
		return_data(true,'User List.',$data);
	}

	private function validate_get_post_like_users(){
		post_check();
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->set_rules('last_id','last_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

}