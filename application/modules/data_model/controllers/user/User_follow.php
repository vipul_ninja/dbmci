<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_follow extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("user_follow_model");
	}

	public function follow_user(){

		$this->validate_follow_user();
		// first check if already follow
		if($this->user_follow_model->is_already_follow_user($this->input->post())){
			return_data(false,'User Already Followed.');
		}

		$this->user_follow_model->user_follow($this->input->post());

		/*	Generate activity log 
		*   HIGHLY SENSITIVE CODE
		*/
		modules::run('data_model/notification_genrator/activity_logger/genrate_activity_while_following', 
									array( "action_for_user_id" => $this->input->post('user_id'),
											"action_element_id" => $this->input->post('user_id'),
											"action_performed_by_user_id" => $this->input->post('follower_id')
										)
								);	
		
		/* send push */
		modules::run('data_model/pusher/Push_following/push_on_following', 
							array( "follower_id"=>$this->input->post('follower_id'),"user_id"=>$this->input->post('user_id'))
		);	
		activity_rewards($this->input->post('follower_id'),'USER_FOLLOW',"+");					
		return_data(true,'User Followed.');
	}

	private function validate_follow_user(){

		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('follower_id','follower_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}

		if($this->input->post('user_id') == $this->input->post('follower_id') ){
			return_data(false,"You cant follow yourself.",array());
		}
	}


	public function unfollow_user(){

		$this->validate_unfollow_user();
		$this->user_follow_model->unfollow_user($this->input->post());
		activity_rewards($this->input->post('follower_id'),'USER_FOLLOW',"-");	
		return_data(true,'Unfollow Successfully.');
	}

	private function validate_unfollow_user(){

		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('follower_id','follower_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function followers_list(){

		$this->validate_followers_list();
		$data = $this->user_follow_model->followers_list($this->input->post());

		if($this->input->get('is_watcher')){
			$return = array();
			foreach($data as $d){
				$d['watcher_following'] = false;
				if($this->user_follow_model->is_already_follow_user(array("follower_id"=>$this->input->get('is_watcher'),"user_id"=>$d['follower_id']))){
					$d['watcher_following'] = true;
				}
				$return[] = $d;
			}	
		}else{
			$return = $data;
		}
		if(count($return) > 0  ){
			return_data(true,'Followers List.',$return);
		}
		return_data(false,'No Followers List Found.',array());
	}

	private function validate_followers_list(){

		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		if(!array_key_exists('last_follow_id',$_POST) || $_POST['last_follow_id'] == 0 ){
			$_POST['last_follow_id'] = "";
		}
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	public function following_list(){
		$this->validate_following_list();
		$_POST['allow_tag'] = $this->input->post('allow_tag');
		$data=$this->user_follow_model->following_list($this->input->post());

		if($this->input->get('is_watcher')){
			$return = array();
			foreach($data as $d){
				$d['watcher_following'] = false;
				if($this->user_follow_model->is_already_follow_user(array("follower_id"=>$this->input->get('is_watcher'),"user_id"=>$d['user_id']))){
					$d['watcher_following'] = true;
				}
				$return[] = $d;
			}	
		}else{
			$return = $data;
		}

		if(count($return) > 0  ){
			return_data(true,'Following List.',$return);
		}
		return_data(false,'No Following List Found.',array());
	}


	private function validate_following_list(){

		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		if(!array_key_exists('last_follow_id',$_POST) || $_POST['last_follow_id'] == 0){
			$_POST['last_follow_id'] = "";
		}
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function users_list(){
		$this->validate_users_list();
		$_POST['allow_tag'] = $this->input->post('allow_tag');
		$data=$this->user_follow_model->mme_list($this->input->post());
		$return = $data;
		if(count($return) > 0 ){
			return_data(true,'User List.',$return);
		}
		return_data(false,'User List not found.',array());
	}


	private function validate_users_list(){

		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('user_type','user_type', 'trim|required');
		if(!array_key_exists('last_id',$_POST) || $_POST['last_id'] == 0){
			$_POST['last_id'] = "";
		}
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


}
