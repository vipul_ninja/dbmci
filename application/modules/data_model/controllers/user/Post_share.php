<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_share extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("post_share_model");
	}

	public function share_post(){

		$this->validate_share_post();
		if($this->post_share_model->is_already_share_post($this->input->post())){
			return_data(false,'Post Already Shared.');
		}

		$this->post_share_model->share_post($this->input->post());

		$user_id = $this->input->post('user_id');
		$post_id = $this->input->post('post_id');
		/* send push */
		modules::run('data_model/pusher/Push_post_shared/push_on_share', 
									array( "post_id"=>$post_id,"user_id"=>$user_id)
								);
		
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
									array( "post_id"=>$post_id,"user_id"=>$user_id)
								);

		/*	Generate activity log 
		*   HIGHLY SENSITIVE CODE
		*/
		modules::run('data_model/notification_genrator/activity_logger/genrate_activity_while_post_share', 
									array( "action_for_user_id"=>$post_data['user_id'],"action_element_id"=>$post_id,"action_performed_by_user_id"=>$user_id)
								);	
		

		return_data(true,'Post Shared.',$post_data);

	}

	private function validate_share_post(){

		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

}
