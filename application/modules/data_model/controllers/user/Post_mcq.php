<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_mcq extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("post_mcq_model");
	}

	public function add_mcq(){

		$this->validate_add_mcq();
		$sub_cate_id = $this->input->post('sub_cate_id');
		$post_subject_id = $this->input->post('subject_id');

		unset($_POST['sub_cate_id']);
		unset($_POST['subject_id']);
		$post_data = $this->input->post();

		unset($post_data['file'],$post_data['tag_people']); 
		$post_id = $this->post_mcq_model->save_user_mcq($post_data);

		activity_rewards($this->input->post('user_id'),'SURVEY_ADD',"+");

		/* save sub cate to post */
		$this->post_mcq_model->save_sub_cate_text_post($post_id,$sub_cate_id);

		if(array_key_exists('file', $_POST) && $_POST != ""){

			$file =  json_decode($_POST['file'], True);
			if(is_array($file)){
				foreach($file as $f ){
					$r['post_id'] = $post_id;
					$r['file_type'] = $f['file_type'];
					$r['link'] = $f['link'];
					$r['file_info'] = (array_key_exists('file_info', $f))?$f['file_info']:"";
					$this->db->insert('post_file_meta',$r);
				}
			}
		}

		if($post_subject_id){
			$in=array();
			$this->db->where('post_id',$post_id);
			$this->db->delete('post_subject_relationship');
			$in['post_id'] = $post_id;
			$in['subject_id'] = $post_subject_id;
			$this->db->insert('post_subject_relationship',$in); 
		}		

		/* add tagged people */
		$to_tag_people = is_comma_seprated($_POST['tag_people'],True);
		if(is_array($to_tag_people) && count($to_tag_people) > 0 ){
			foreach($to_tag_people as $v ){
				if($v > 0 ){
					/* check if already tagged */
					$this->db->select("id");
					$this->db->where("post_id",$post_id);
					$this->db->where("tagged_user_id",$v);
					if(!ISSET($this->db->get("user_post_tag_people")->row()->id)){
						$this->db->insert("user_post_tag_people",array('post_id'=> $post_id ,
																	"tagged_user_id"=>$v ,
																	"creation_time"=>milliseconds()
																));
						/*	Generate activity log 
						*   HIGHLY SENSITIVE CODE
						*/
						modules::run('data_model/notification_genrator/activity_logger/generate_activity_whlie_people_tagged_on_post', 
													array( "action_for_user_id"=>$v,"action_element_id"=>$post_id,"action_performed_by_user_id"=>$this->input->post('user_id'))
												);
						/*
						* send push notification to tagged user 
						*/
						modules::run('data_model/pusher/push_post_on_tag/push_on_tag', 
									array( "post_id"=>$post_id,"user_id"=>$this->input->post('user_id'),"tagged_user_id"=>$v)
								);												
					}	
				}
			}
		}	
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
					array( "post_id"=>$post_id,"user_id"=>$this->input->post('user_id'))
				);
		$post_data['coins_for_post'] = 	get_db_meta_key('SURVEY_ADD');
		return_data(true,'Post has been posted successfully.',$post_data);
	}

	private function validate_add_mcq(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('question','question', 'trim|required');
		/*this key is required to add filters lator */
		$this->form_validation->set_rules('sub_cate_id','sub_cate_id', 'trim|required');
		$this->form_validation->set_rules('subject_id','subject_id', 'trim');
		$this->form_validation->set_rules('right_answer','right_answer', 'trim|required');
		if($this->input->post('answer_one') == "" && $this->input->post('answer_two') == "" &&
			$this->input->post('answer_three') == "" && $this->input->post('answer_four') == "" && 
			$this->input->post('answer_five') == ""){
			return_data('false',"Please provide two option(s)");
		}
		$_POST['post_tag'] = (array_key_exists('post_tag', $_POST))?$_POST['post_tag']:"";
		
		$_POST['location'] = (array_key_exists('location', $_POST))?$_POST['location']:"";
		$_POST['lat'] = (array_key_exists('lat', $_POST))?$_POST['lat']:"";
		$_POST['lon'] = (array_key_exists('lon', $_POST))?$_POST['lon']:"";
		$this->form_validation->run();

		/* people tagging if key not found */
		if(!array_key_exists('tag_people', $_POST) ){ $_POST['tag_people'] = ""; }
        /* if found */
		if($_POST['tag_people'] != "" && is_comma_seprated($_POST['tag_people']) === false){
			return_data('false',"Please provide valid input for tagging people.");
		}


		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	public function answer_post_mcq(){

		$this->validate_answer_post_mcq();

		$this->post_mcq_model->save_user_mcq_answer($this->input->post());
		/* send post data in return */

		$this->db->select('post_id');
		$this->db->where('id',$this->input->post('mcq_id'));
		$post_id =  $this->db->get('user_post_type_mcq')->row()->post_id;
		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
									array( "post_id"=>$post_id,"user_id"=>$this->input->post('user_id'))
									);
		return_data(true,'Answer saved successfully.',$post_data);		
	}

	private function validate_answer_post_mcq(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('mcq_id','mcq_id', 'trim|required');
		$this->form_validation->set_rules('answer','answer', 'trim|required');
		$this->form_validation->run();

		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
		/* check if already answered*/

		if($this->post_mcq_model->check_user_mcq_answer($this->input->post())){
			return_data(false,"You already submit your answer");
		}

	}

	public function edit_mcq(){

		$this->validate_edit_mcq();
		$post_data = $this->input->post();
		$id = $post_data['post_id'];
		$delete_meta = $this->input->post('delete_meta');
		$post_subject_id = $this->input->post('subject_id');
		unset($post_data['file']);
		unset($post_data['post_id']);
		unset($post_data['user_id']);
		unset($post_data['post_tag']);
		unset($post_data['delete_meta']);
		unset($post_data['tag_people'], $post_data['remove_tag_people']); 
		unset($_POST['subject_id']);
		
		$post_id = $this->post_mcq_model->edit_user_mcq($post_data,$id);
		
		//update post tag 
		$this->db->where('id',$id);
		$this->db->update('post_counter',array('post_tag'=>$this->input->post('post_tag')));

		if(array_key_exists('file', $_POST) && $_POST != ""){
			$file =  json_decode($_POST['file'], True);
			if(is_array($file)){
				foreach($file as $f ){
					$r['post_id'] = $id;
					$r['file_type'] = $f['file_type'];
					$r['link'] = $f['link'];
					$r['file_info'] = (array_key_exists('file_info', $f))?$f['file_info']:"";
					$this->db->insert('post_file_meta',$r);
				}
			}
		}

		if($post_subject_id){
			$in=array();
			$this->db->where('post_id',$post_id);
			$this->db->delete('post_subject_relationship');
			$in['post_id'] = $post_id;
			$in['subject_id'] = $post_subject_id;
			$this->db->insert('post_subject_relationship',$in); 
		}	
		//delete older post 
		if($delete_meta != ""){
			$ids = explode(',',$delete_meta);
			$this->db->where_in('id', $ids);
			$this->db->delete('post_file_meta');
		}

		/* add tagged people */
		$to_tag_people = is_comma_seprated($_POST['tag_people'],True);
		if(is_array($to_tag_people) && count($to_tag_people) > 0 ){
			foreach($to_tag_people as $v ){
					/* check if already tagged */
					$this->db->select("id");
					$this->db->where("post_id",$this->input->post('post_id'));
					$this->db->where("tagged_user_id",$v);
					if(!ISSET($this->db->get("user_post_tag_people")->row()->id)){
						$this->db->insert("user_post_tag_people",array('post_id'=> $this->input->post('post_id') ,
																	"tagged_user_id"=>$v ,
																	"creation_time"=>milliseconds()
																));
						/*	Generate activity log 
						*   HIGHLY SENSITIVE CODE
						*/
						modules::run('data_model/notification_genrator/activity_logger/generate_activity_whlie_people_tagged_on_post', 
													array( "action_for_user_id"=>$v,"action_element_id"=>$this->input->post('post_id'),"action_performed_by_user_id"=>$this->input->post('user_id'))
												);
						/*
						* send push notification to tagged user 
						*/
						modules::run('data_model/pusher/push_post_on_tag/push_on_tag', 
									array( "post_id"=>$this->input->post('post_id'),"user_id"=>$this->input->post('user_id'),"tagged_user_id"=>$v)
								);												
					}		
			}
		}

		/* remove tagged people */
		$remove_tag_people = is_comma_seprated($_POST['remove_tag_people'],True);
		if(is_array($remove_tag_people) && count($remove_tag_people) > 0 ){
			foreach($remove_tag_people as $v ){
				if($v > 0 ){
					$this->db->where('post_id', $this->input->post('post_id'));
					$this->db->where('tagged_user_id', $v);
  					$this->db->delete('user_post_tag_people');
				}	
			}
		}

		$post_data = modules::run('data_model/fanwall/fan_wall/get_post_with_user_internal', 
									array( "post_id"=>$this->input->post('post_id'),"user_id"=>$this->input->post('user_id'))
								);
		return_data(true,'Post updated.',$post_data);
	}

	private function validate_edit_mcq(){
		
		post_check();
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('question','question', 'trim|required');
		$this->form_validation->set_rules('subject_id','subject_id', 'trim|required');
		$this->form_validation->set_rules('right_answer','right_answer', 'trim|required');
		if($this->input->post('answer_one') == "" && $this->input->post('answer_two') == "" &&
			$this->input->post('answer_three') == "" && $this->input->post('answer_four') == "" && 
			$this->input->post('answer_five') == ""){
			return_data('false',"Please provide two option(s)");
		}

		$_POST['post_tag'] = (array_key_exists('post_tag', $_POST))?$_POST['post_tag']:"";
		/* people tagging if key not found */
		if(!array_key_exists('tag_people', $_POST) ){ $_POST['tag_people'] = ""; }
        /* if found */
		if($_POST['tag_people'] != "" && is_comma_seprated($_POST['tag_people']) === false){
			return_data('false',"Please provide valid input for tagging people.");
		}

		/* people tagging if key not found */
		if(!array_key_exists('remove_tag_people', $_POST) ){ $_POST['remove_tag_people'] = ""; }
        /* if found */
		if($_POST['remove_tag_people'] != "" && is_comma_seprated($_POST['remove_tag_people']) === false){
			return_data('false',"Please provide valid input for removing tagging people.");
		}
		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

}