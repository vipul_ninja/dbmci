<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
	}

	public function get_slider(){

		$this->validate_get_slider();
      $data = $this->db->where('slider_id',$this->input->post('slider_id'))->get('slider_image_master')->result();
		return_data(true,'Slider Data.',$data);
	}

	private function validate_get_slider(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('slider_id','slider_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
}