<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instructor extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model('course_model');
	}
/***
 *       _____        _     _____              _                       _
 *      / ____|      | |   |_   _|            | |                     | |
 *     | |  __   ___ | |_    | |   _ __   ___ | |_  _ __  _   _   ___ | |_  ___   _ __
 *     | | |_ | / _ \| __|   | |  | '_ \ / __|| __|| '__|| | | | / __|| __|/ _ \ | '__|
 *     | |__| ||  __/| |_   _| |_ | | | |\__ \| |_ | |   | |_| || (__ | |_| (_) || |
 *      \_____| \___| \__| |_____||_| |_||___/ \__||_|    \__,_| \___| \__|\___/ |_|
 *
 *
 */
	public function get_instructor_profile(){
		$this->validate_get_instructor_profile();
		$instructor_id = $this->input->post('instructor_id');
		$user_id = $this->input->post('user_id');

		$d = $this->db->select('id,username as name ,email,profile_picture as profile_pic')->where(array('id'=>$instructor_id))->get("backend_user")->row_array();
		$sql = "SELECT user_id , rating_count , review_count , learner_count from course_instructor_information where user_id = $instructor_id";
		$r_count =  $this->db->query($sql)->row_array();

		$d['students'] =	(string)$r_count['learner_count'];
		$d['about'] =	"";
		$d['courses'] =	$this->db->select('count(id) as total')->where(array('instructor_id'=>$instructor_id,"publish"=>1))->get("course_master")->row()->total;
		$d['rating'] =	$r_count['rating_count'];
		$d['review'] =  $r_count['review_count'];
		/* get two related reviews for this course */
		$two_reviews =  "SELECT cir.id , u.id as user_id ,  u.name as name , u.profile_picture as profile_picture , text  , rating , cir.creation_time
		FROM `course_instructor_rating` as cir
		Join users as u on u.id = cir.user_id
		WHERE cir.instructor_id = $instructor_id  order by cir.id desc limit 0,2 ";
		$reviews_result =  $this->db->query($two_reviews)->result_array();
		if(count($reviews_result)>0 ){
			$d['reviews'] = $reviews_result;
		}
		/* get instructor courses */

		// $query = $this->db->query("select * , course_learner as learner , course_rating_count as rating  from course_master where instructor_id = '$instructor_id' and  publish = 1 and state = 0 limit 0 ,3 ");
		// $courses =   $query->result_array();
			$user_info = $this->db->select('dams_tokken')->where('id',$user_id)->get('users')->row();
			$where = "";
			if(count($user_info) > 0 && $user_info->dams_tokken =="" ){
				$where =  " and (course_for = 1 or course_for = 2 ) ";
			}else{
				$where =  " and (course_for = 0 or course_for = 2 ) ";
			}

			$user_batch_info = $this->db->query("SELECT CONCAT(FranchiseName,CourseGroup,Session,Course,Batch) as batch from users_dams_info where user_id = $user_id")->row(); 
			if(count($user_batch_info) > 0 ){
				$user_batch_name = $user_batch_info->batch;
			}else{
				$user_batch_name = "";
			}

			$date = date("y-m-d");
			$sql = "SELECT id ,title ,cover_image,desc_header_image,mrp, 
							is_new ,for_dams ,non_dams ,  course_rating_count as rating ,
							`course_review_count` as review_count, `course_learner` as learner,
							case 
							when  (select count(cf.id) from course_filtration as cf where cf.course_id = cm.id ) = 0 and  cm.course_for = 0 then 'passed'
							when cm.course_for = 0 and (select count(cf.id) from course_filtration as cf where cf.course_id = cm.id ) > 0 and (SELECT count(cfl.course_id) FROM course_filtration as cfl WHERE CONCAT(cfl.FranchiseName, cfl.CourseGroup, cfl.Session, cfl.Course, cfl.Batch) = '$user_batch_name' and cfl.course_id = cm.id ) > 0 then 'passed' 
							when cm.course_for = 1 then 'passed' 
							when cm.course_for = 2 then 'passed' 
							else 'not_passed'
							end as dams_passing 
						from course_master as cm
						where  cm.instructor_id = '$instructor_id' 
						and publish = 1 
						$where
						and (`start_date` = '0000-00-00' or ('$date' >= start_date and '$date' <= end_date ))
						having dams_passing = 'passed'
						limit 0,3 ";
			$courses = $this->db->query($sql)->result_array();

		if(count($courses) > 0 ){
			$d['course_list'] =  $courses;
		}
		/* review given by users */
		$user_given_review = "SELECT cir.id , u.id as user_id ,  u.name as name , u.profile_picture as profile_picture , text  , rating , cir.creation_time
		FROM `course_instructor_rating` as cir
		Join users as u on u.id = cir.user_id
		WHERE cir.user_id = $user_id  order by cir.id desc limit 0,1 ";
		$user_given_review =  $this->db->query($user_given_review)->row_array();
		if(count($user_given_review) > 0 ){
			$d['user_given_review'] = $user_given_review;
		}
		return_data(true,"Instructor information.",$d);
	}

	private function validate_get_instructor_profile(){

		post_check();
		$this->form_validation->set_rules('instructor_id','instructor_id', 'trim|required');
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
}
