<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test_series extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model('test_series_model');
	}

    public function get_test_series_with_id(){
        $this->validate_get_test_series_with_id();
        $test_series_id = $this->input->post('test_series_id');
        if($test = $this->test_series_model->get_test_series($test_series_id) ){
            $data['basic_info'] = $test;
            $data['question_bank'] = $this->test_series_model->get_test_series_question($test_series_id);
            return_data(true,"Test series.",$data);
        }
        return_data(false,"Test series not found.",array());
    }

	private function validate_get_test_series_with_id(){

		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('test_series_id','test_series_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
	/* save test series */
	public function save_test(){
		$this->validate_save_test();
		$user_id = $this->input->post('user_id');
		$test_series_id = $this->input->post('test_series_id');
		$question_dump = $this->input->post('question_dump');

		$return_id = $this->test_series_model->save_test_series($this->input->post());

		// get test series type
		/* claculate result after saving the result */
		if($return_id != ""){

			$test_type = $this->db->where('id',$test_series_id)->get('course_test_series_master')->row();
				//echo "inside else";die;
				$php_question_dump =  json_decode($question_dump);
				$question_list = array();
				$unattempt_count = $correct_count = $incorrect_count =  0;
				/* getting attempt question list */
				foreach($php_question_dump as $pqd){
					$question_list[] = $pqd->question_id;
					/* fill unattempt count */
					if($pqd->answer == ""){
						$unattempt_count++;
					}
				}
				/* get answers of attempt question dump */
				$attempt_answers_array =  $this->test_series_model->get_questions_answer(implode(',',$question_list));
				$final_array =  array();
				foreach($php_question_dump as $pqd){
					$pqd->is_correct = "0";
					$incorrect_count++;

					foreach($attempt_answers_array as $aaa){
						/*
						* in this question check we need to updated total attempt and right and wrong attempt of the question 
						*/
                        $right = $wrong = 0 ;
						if($aaa->id == $pqd->question_id and $aaa->answer == $pqd->answer ){
							$pqd->is_correct = "1";
							$correct_count++;
							$incorrect_count = $incorrect_count-1;
							$right = 1;
						}else{
							$wrong = 1;
						}

						$query = "UPDATE course_question_bank_master Set total_attempt = (total_attempt+1) , total_right = (total_right+$right) , total_wrong = (total_wrong+$wrong) where id = $pqd->question_id";
						$this->db->query($query);

					}
					$final_array[] =  $pqd ;
				}
				/* check users attempt & update if it is first */
				$check_attempt = $this->test_series_model->check_attempt($user_id,$test_series_id);
				if($check_attempt){
					$first_attempt = 0;
				}
				else{
					$first_attempt = 1;
				}

				/* get test series details for marks calculations */
				$test_series_details =  $this->test_series_model->test_series_details($test_series_id);
				$test_series_marks = $test_series_details->marks_per_question*$test_series_details->total_questions;
				$incorrect_total_count = $incorrect_count-$unattempt_count;
				$correct_marks = $test_series_details->marks_per_question*$correct_count;
				$incorrect_marks = $test_series_details->negative_marking*$incorrect_total_count;
				$marks = $correct_marks  - $incorrect_marks ;

				/* Adding test-series reward points to user rewards */
				$test_series_rewards = $test_series_details->reward_points;
				if($first_attempt == 1 &&  $test_series_rewards > 0 ){
					$p['area'] = "TEST_POINTS";
					$p['user_id'] = $user_id;
					$p['reward']  = $test_series_rewards;
					$p['creation_time'] = milliseconds();
					$this->db->insert('user_activity_rewards',$p);
					$this->db->query("Update users set reward_points = (SELECT sum(reward) as total FROM `user_activity_rewards` WHERE user_id = $user_id) where id = $user_id");
				}else{
					$test_series_rewards = 0 ;
				}

				$resave = array(
					"id"=> $return_id,
					"correct_count"=> $correct_count,
					"incorrect_count"=>$incorrect_count-$unattempt_count,
					"non_attempt"=>$unattempt_count,
					"question_dump"=>json_encode($final_array),
					"reward_points" =>$test_series_rewards,
					"total_test_series_time" =>$test_series_details->time_in_mins,
					"test_series_marks" =>$test_series_marks,
					"marks" =>$marks,
					"first_attempt" =>$first_attempt
				);
			/* save calculated result*/
			$this->test_series_model->save_result($resave);
			log_message('error', 'Some variable did not contain a value.'.json_encode($final_array));

			foreach($final_array as $fa){
				//id 	report_id 	q_id 	answer 	is_correct 
				$array = array(
								'report_id'=>$return_id,
								'answer'=>$fa->answer,
								'is_correct'=>$fa->is_correct,
								'q_id'=>$fa->question_id
							);
				$this->db->insert('course_test_series_report_question_dump',$array);			
			}
			return_data(true,"Test series report.",$this->get_test_series_data($return_id,$user_id));
		}
	}

	private function validate_save_test(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('test_series_id','test_series_id', 'trim|required');
		$this->form_validation->set_rules('question_dump','question_dump', 'trim|required');
		$this->form_validation->set_rules('time_spent','time_spent', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
	/* provide test-series result */
	public function get_test_series_result(){
		$this->validate_get_test_series_result();
		/* we will send here the question attempt and with description */
		/* also here we will get the answer on the basis of user result primary id */
		/* so lets do it */
		$test_segment_id = $this->input->post('test_segment_id'); // got it !
		$user = $this->input->post('user_id'); // user who did it
		/* request db person to find it */
		$request_out = $this->test_series_model->question_report_of_segment($user,$test_segment_id); // here we go db
		return_data(true,"Test series answer found successfully.",$request_out);
	}

	private function validate_get_test_series_result(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('test_segment_id','test_segment_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/* Service for getting user's rewards */

	public function get_user_reward_points(){
		$this->validate_get_user_reward_points();
		$user_id = $this->input->post('user_id'); // user's id

		$request_out = $this->test_series_model->get_user_reward_points($user_id); // here we go db
		return_data(true,"User's reward points found successfully.",$request_out);
	}

	private function validate_get_user_reward_points(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/* get basic information of test series result  with user_id and $result_id  */
	public function get_test_series_basic_result(){
		$this->validate_get_test_series_basic_result();

		$test_segment_id = $this->input->post('test_segment_id'); // got it !
		$user = $this->input->post('user_id'); // user who did it
		/* request db person to find it */
		$request_out = $this->get_test_series_data($test_segment_id,$user); // here we go db
		return_data(true,"Test series result basic information.",$request_out);
	}

	private function validate_get_test_series_basic_result(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('test_segment_id','test_segment_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/* get basic information of test series result  with user_id and $result_id  */
	public function top_list(){
		$this->validate_top_list();

		$primary_id = $this->input->post('test_segment_id'); // got it !
		$user_id = $this->input->post('user_id'); // user who did it
		/* request db person to find it */
		$get_meta_info = $this->test_series_model->test_series_metainformation($primary_id,$user_id);

		$get_meta_info->total_user_attempt = $this->test_series_model->total_user_test_attempt($get_meta_info->test_series_id);

		$get_meta_info->user_rank = $this->test_series_model->user_test_rank($get_meta_info->test_series_id,$user_id);
		//pre($get_meta_info->total_user_attempt);die;
		/* list of top 10 people */
		$get_meta_info->top_list =   $this->test_series_model->top_list($get_meta_info->test_series_id);

		return_data(true,"Test series result basic information.",$get_meta_info);
	}

	private function validate_top_list(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('test_segment_id','test_segment_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}



	public function get_user_given_test_series(){
		$this->validate_get_user_given_test_series();
		$user = $this->input->post('user_id'); // user who did it
		$sql =  "SELECT ctsr.id ,ctsr.test_series_id ,  ctsm.test_series_name , ctsm.image , ctsr.result , ctsr.reward_points , ctsr.creation_time
				FROM course_test_series_report as ctsr
				join course_test_series_master as ctsm on ctsr.test_series_id = ctsm.id
				WHERE ctsr.user_id = $user AND ctsr.first_attempt = 1 order by ctsr.id desc";
		$out =  $this->db->query($sql)->result_array();
		if(count($out)>0){
			return_data(true,"Test series list.",$out);
		}

		return_data(false,"Test series list not found.",array());
	}

	private function validate_get_user_given_test_series(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
	/* for internal use only */
	private function get_test_series_data($primary_id,$user_id){
		$get_meta_info = $this->test_series_model->test_series_metainformation($primary_id,$user_id);

		$get_meta_info->total_user_attempt = $this->test_series_model->total_user_test_attempt($get_meta_info->test_series_id);

		$get_meta_info->user_rank = $this->test_series_model->user_test_rank($get_meta_info->test_series_id,$user_id);
		//pre($get_meta_info->total_user_attempt);die;
		/* list of top 10 people */
		$get_meta_info->top_ten_list =   $this->test_series_model->top_ten_list($get_meta_info->test_series_id);
		return $get_meta_info;
	}

	/* test given mysql data */

	public function get_question_dump(){
		$this->validate_get_question_dump();
		$segment_id = $this->input->post('test_segment_id') ;

		$query = "SELECT cqd.* , cqbm.subject_id , cqbm.topic_id , csm.name as subject_name , cstm.topic as topic_name 
					FROM course_test_series_report_question_dump as cqd 
					join course_question_bank_master as cqbm on cqd.q_id = cqbm.id 
					left join course_subject_master as csm on csm.id = cqbm.subject_id 
					left join course_subject_topic_master as cstm on cstm.id = cqbm.topic_id 
					where cqd.report_id = $segment_id";
		$data['dump'] =  $this->db->query($query)->result();

		/* find out subject */
		$tempu =  array();
		foreach($data['dump'] as $qd){
			$tempu[$qd->subject_id] = array('name'=>$qd->subject_name);
		}
		//$tempu = array_unique($tempu);

		// make subject  topic list

		foreach($tempu as $keys=>$values){
			$holder = array();
			$q_counter = 0;
			$correct_couont = 0;
			foreach($data['dump'] as $qd){
				if($keys == $qd->subject_id){
					
					/* topic question manupulation */
					$total_question_in_topic = 0;
					$total_correct_question_in_topic = 0;

					foreach($data['dump'] as $ft){
						if($qd->topic_id == $ft->topic_id){
							if($ft->is_correct == 1 ){
								$total_correct_question_in_topic++;
							}
							$total_question_in_topic++;
						}
					}

					$holder[$qd->topic_id] = array(
															'name'=>$qd->topic_name,
															'total_question'=>$total_question_in_topic,
															'correct_question'=>$total_correct_question_in_topic
														);									
					$q_counter++;
					if($qd->is_correct == 1 ){
						$correct_couont++;
					}


				}
			}
			$orig  = $tempu[$keys];
			$orig['topic_data']  = $holder;
			$orig['total_question']  = $q_counter;
			$orig['correct_question']  = $correct_couont;
			$tempu[$keys] = $orig;
			$tempu[$keys] = $orig;
		}

      $data['subject_dump'] = $tempu;
		$query = "SELECT 
					SUM(CASE WHEN answer = '' THEN 1 ELSE 0 END)  AS total_unanswered,
  					SUM(CASE WHEN answer != '' THEN 1 ELSE 0 END)  AS total_answered, 
					SUM(CASE WHEN is_correct = 1 THEN 1 ELSE 0 END)  AS total_correct,
  					SUM(CASE WHEN is_correct != 1  THEN 1 ELSE 0 END)  AS total_incorrect
					FROM course_test_series_report_question_dump  as ctsrqd
					where report_id = $segment_id";
		$data['basic'] =  $this->db->query($query)->row();

		return_data(true,"Test series list.",$data);
	}

	private function validate_get_question_dump(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('test_segment_id','test_segment_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}

	}
}
