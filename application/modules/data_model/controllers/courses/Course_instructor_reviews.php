
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_instructor_reviews extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper("services");
        $this->load->model('course_instructor_rating_reviews_model');
    }
/***
 *                   _      _   _____               _
 *         /\       | |    | | |  __ \             (_)
 *        /  \    __| |  __| | | |__) | ___ __   __ _   ___ __      __
 *       / /\ \  / _` | / _` | |  _  / / _ \\ \ / /| | / _ \\ \ /\ / /
 *      / ____ \| (_| || (_| | | | \ \|  __/ \ V / | ||  __/ \ V  V /
 *     /_/    \_\\__,_| \__,_| |_|  \_\\___|  \_/  |_| \___|  \_/\_/
 *
 *
 */
    public function add_review_to_instructor(){
        $this->validate_add_review_to_instructor();
        /* check if already given */
        $check = $this->course_instructor_rating_reviews_model->if_review_already_given($this->input->post('instructor_id'),$this->input->post('user_id'));
        if(count($check)>0){
            return_data(false,"Rating already given.",$check);
        }
        $return = $this->course_instructor_rating_reviews_model->add_instructor_review($this->input->post());
        if($return){
            return_data(true,"Review added.",array());
        }
        activity_rewards($this->input->post('user_id'),'COURSE_INSTRUCTOR_REVIEW',"+");
        return_data(false,"Not able to add your review.",array());
    }

    private function validate_add_review_to_instructor(){
        post_check();
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('instructor_id','instructor_id ', 'trim|required');
        $this->form_validation->set_rules('rating','rating', 'trim|required');
        $this->form_validation->set_rules('text','text', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }
    }

/***
 *      ______     _  _  _      _____                                           _
 *     |  ____|   | |(_)| |    / ____|                                         | |
 *     | |__    __| | _ | |_  | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_
 *     |  __|  / _` || || __| | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
 *     | |____| (_| || || |_  | |____| (_) || | | | | || | | | | ||  __/| | | || |_
 *     |______|\__,_||_| \__|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
 *
 *
 */
    public function edit_review_to_instructor(){
        $this->validate_edit_review_to_instructor();
        $return = $this->course_instructor_rating_reviews_model->edit_instructor_review($this->input->post());
        if($return){
            return_data(true,"Review edited.",array());
        }
        return_data(false,"Not able to edit your review.",array());
    }

    private function validate_edit_review_to_instructor(){
        post_check();
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('instructor_id','instructor_id', 'trim|required');
        $this->form_validation->set_rules('rating','rating', 'trim|required');
        $this->form_validation->set_rules('text','text', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }
    }
  /***
   *      _____         _        _          _____               _
   *     |  __ \       | |      | |        |  __ \             (_)
   *     | |  | |  ___ | |  ___ | |_  ___  | |__) | ___ __   __ _   ___ __      __
   *     | |  | | / _ \| | / _ \| __|/ _ \ |  _  / / _ \\ \ / /| | / _ \\ \ /\ / /
   *     | |__| ||  __/| ||  __/| |_|  __/ | | \ \|  __/ \ V / | ||  __/ \ V  V /
   *     |_____/  \___||_| \___| \__|\___| |_|  \_\\___|  \_/  |_| \___|  \_/\_/
   *
   *
   */
    public function delete_review_from_instructor(){
        $this->validate_delete_review_from_instructor();
        $return = $this->course_instructor_rating_reviews_model->delete_instructor_review($this->input->post());
        if($return){
            return_data(true,"Review deleted.",array());
        }
        activity_rewards($this->input->post('user_id'),'COURSE_INSTRUCTOR_REVIEW',"-");
        return_data(false,"Not able to delete your review.",array());
    }

    private function validate_delete_review_from_instructor(){
        post_check();
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('instructor_id','instructor_id', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }
    }

/***
 *      _____               _                        _       _       _
 *     |  __ \             (_)                      | |     (_)     | |
 *     | |__) | ___ __   __ _   ___ __      __ ___  | |      _  ___ | |_
 *     |  _  / / _ \\ \ / /| | / _ \\ \ /\ / // __| | |     | |/ __|| __|
 *     | | \ \|  __/ \ V / | ||  __/ \ V  V / \__ \ | |____ | |\__ \| |_
 *     |_|  \_\\___|  \_/  |_| \___|  \_/\_/  |___/ |______||_||___/ \__|
 *
 *
 */
    public function get_list_of_reviews(){
        $this->validate_get_list_of_reviews();
        $data =  $this->course_instructor_rating_reviews_model->get_reviews_list($this->input->post());
        if($data){
            return_data(true,"Reviews Found.",$data);
        }
        return_data(false,"No Reviews Found.",array());
    }

    private function validate_get_list_of_reviews(){
        post_check();
        $this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('instructor_id','instructor_id', 'trim|required');
        $this->form_validation->set_rules('last_review_id','last_review_id', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if($error){
            return_data(false,array_values($error)[0],array(),$error);
        }
    }

}
