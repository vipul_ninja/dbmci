<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model('course_model');
	}

	public function get_exchange_rate($in='USD'){
		$data = array();
		if($in == 'USD'){
			$data['rate'] = 65.00;
		}
		return_data(true,"Data found.",$data);
	}


	/* Get basic information on landing page */
	public function get_landing_page_data(){
		$this->validate_get_landing_page_data();
		$data['user_id'] 			= $this->input->post('user_id');
		$data['course_type'] 	= $this->input->post('course_type');
		$data['main_cat']			= $this->input->post('main_cat');
		$data['sub_cat']      = $this->input->post('sub_cat');
		$main_category     =  $this->course_model->get_basic_sub_active_category($data['main_cat']);
		$return_merge_data = array();
		foreach($main_category as $mc){
			$segment 					  = array();
			$segment['category_info'] = $mc;

			$data['sub_cat'] 				= $mc["id"];
			//$segment['course_list'] =  $this->course_model->get_course_on_landing_page($data);

			$list =  $this->course_model->get_course_on_landing_page($data);
			$records =  array();
			foreach($list as $l ){
				$l['element_meta'] = $this->db->query('SELECT id , topic_name , image , position , (select count(id) as total from course_segment_element as cse where cse.segment_fk = ctm.id ) as own FROM course_topic_master ctm WHERE ctm.course_fk = '.$l['id'])->result_array();
				$l['viewed_meta'] = $this->db->query('SELECT segment_id FROM user_section_complition_master WHERE user_id = '.$data['user_id'].' and section_id= '.$l['id'])->result_array();
				
				$records[] = $l;
			}
			$segment['course_list'] = $records;
			$return_merge_data[] 		= $segment;
		}

		if(count($return_merge_data)>0){
			return_data(true,"Course Found.",$return_merge_data);
		}
		return_data(false,"No Course Found.",array());
	}

	private function validate_get_landing_page_data(){

		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required|integer');
		// $this->form_validation->set_rules('course_type','course_type', 'trim|is_natural_no_zero');
		// $this->form_validation->set_rules('main_cat','main_cat', 'trim|required|is_natural_no_zero');
		// $this->form_validation->set_rules('sub_cat','sub_cat', 'trim|is_natural_no_zero');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	/* Get basic information on landing page */
	public function get_landing_page_data_exam(){
		$this->validate_get_landing_page_data();
		$data['user_id'] 			= $this->input->post('user_id');
		$data['course_type'] 	= $this->input->post('course_type');
		$data['main_cat']			= $this->input->post('main_cat');
		$data['sub_cat']        = $this->input->post('sub_cat');

		$main_category     =  $this->course_model->get_basic_sub_active_category(1);
		$return_merge_data = array();
		foreach($main_category as $mc){
			$segment 					  = array();
			$data['sub_cat'] 			  = $mc["id"];
			$list =  $this->course_model->get_course_on_landing_page_exam($data);
			$records =  array();
			foreach($list as $l ){
				//$l['viewed_meta'] = $this->db->query('SELECT segment_id FROM user_section_complition_master WHERE user_id = '.$data['user_id'].' and section_id= '.$l['id'])->result_array();
				$l['learner_list'] = $this->db->query("SELECT u.name , u.profile_picture from course_transaction_record as ctr join users as u on u.id = ctr.user_id where ctr.course_id = '".$l['id']."' order by ctr.id desc limit 0,4 ")->result_array();
				$records[] = $l;
			}
			
			if(count($records) > 0 ){
				$segment['category_info'] = $mc;
				$segment['course_list'] = $records;
				$return_merge_data[] 		= $segment;
			}
			
		}

		if(count($return_merge_data)>0){
			return_data(true,"Exam found.",$return_merge_data);
		}
		return_data(false,"No Exam Found.",array());
	}



/***
 *       _____                          _      _                  _____
 *      / ____|                        | |    (_)                / ____|
 *     | (___    ___   __ _  _ __  ___ | |__   _  _ __    __ _  | |      ___   _   _  _ __  ___   ___
 *      \___ \  / _ \ / _` || '__|/ __|| '_ \ | || '_ \  / _` | | |     / _ \ | | | || '__|/ __| / _ \
 *      ____) ||  __/| (_| || |  | (__ | | | || || | | || (_| | | |____| (_) || |_| || |   \__ \|  __/
 *     |_____/  \___| \__,_||_|   \___||_| |_||_||_| |_| \__, |  \_____|\___/  \__,_||_|   |___/ \___|
 *                                                        __/ |
 *                                                       |___/
 */

	public function search_course(){
		$this->validate_search_course();
		$user_id =  $this->input->post('user_id');
		$last_course_id =  $this->input->post('last_course_id');
		$search_content =  $this->input->post('search_content');
		$search_content = addslashes($search_content);
		/* get course */
		$list =  $this->course_model->search_course($user_id,$search_content,$last_course_id);
		if(count($list)>0){
			return_data(true,"Course found.",$list);
		}
		return_data(false,"No Course found.",array());
	}

	private function validate_search_course(){

		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('search_content','search_content', 'trim|required');
		if($this->input->post('last_course_id') == "" || !array_key_exists('last_course_id',$_POST)){
			$_POST['last_course_id'] = "";
		}
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function search_course_exam(){
		$this->validate_search_course();
		$user_id =  $this->input->post('user_id');
		$last_course_id =  $this->input->post('last_course_id');
		$search_content =  $this->input->post('search_content');
		$search_content = addslashes($search_content);
		/* get course */
		$list =  $this->course_model->search_course_exam($user_id,$search_content,$last_course_id);
		if(count($list)>0){
			return_data(true,"Course found.",$list);
		}
		return_data(false,"No Course found.",array());
	}


/***
 *       _____  _                _          _____                                 _____          __
 *      / ____|(_)              | |        / ____|                               |_   _|        / _|
 *     | (___   _  _ __    __ _ | |  ___  | |      ___   _   _  _ __  ___   ___    | |   _ __  | |_  ___
 *      \___ \ | || '_ \  / _` || | / _ \ | |     / _ \ | | | || '__|/ __| / _ \   | |  | '_ \ |  _|/ _ \
 *      ____) || || | | || (_| || ||  __/ | |____| (_) || |_| || |   \__ \|  __/  _| |_ | | | || | | (_) |
 *     |_____/ |_||_| |_| \__, ||_| \___|  \_____|\___/  \__,_||_|   |___/ \___| |_____||_| |_||_|  \___/
 *                         __/ |
 *                        |___/
 */
	public function get_single_course_info_raw(){

		$this->validate_get_single_course_info_raw();
		$course_id =  $this->input->post('course_id');
		$user_id = $this->input->post('user_id');

		$course_info = $this->course_model->get_single_course_raw($this->input->post('course_id'));
		$course_info['gst'] = get_db_meta_key('GST_VALUE');
		if($course_info){ 
			/* get two related reviews for this course */
			$two_reviews =  "SELECT cur.id , u.id as user_id ,  u.name as name , u.profile_picture as profile_picture , text  , rating , cur.creation_time
							FROM `course_user_rating` as cur
							Join users as u on u.id = cur.user_id
							WHERE cur.course_fk_id =$course_id  order by cur.id desc limit 0,2 ";
			$reviews_result =  $this->db->query($two_reviews)->result_array();
			if(count($reviews_result)>0 ){
				$course_info['reviews'] = $reviews_result;
			}

			/* instructor information in this section START HERE */

			$course_info['instructor_data'] = $this->db->select('id,username as name ,email,profile_picture as profile_pic')->where(array('id'=>$course_info['instructor_id']))->get("backend_user")->row_array();
			$course_info['instructor_data']['about'] = "";
			/* insturctor count */
			$i_count = $this->db->select('rating_count, learner_count ')->where(array('user_id'=>$course_info['instructor_id']))->get("course_instructor_information")->row_array();
			$course_info['instructor_data']['students'] =	(string)$i_count['learner_count'];
			$course_info['instructor_data']['courses'] =	$this->db->select('count(id) as total')->where(array('instructor_id'=>$course_info['instructor_id'],"publish"=>1))->get("course_master")->row()->total;
			$course_info['instructor_data']['rating'] =	(string)$i_count['rating_count'];
			/* instructor information in this section END HERE */

			/* curriculam  information in this section START HERE */
			$course_info['curriculam'] = array();
			$course_info['curriculam']['title'] = "";

					$sql =  " SELECT cse.* ,ctm.id as topic_id ,  ctm.topic_name as topic_name ,ctfmm.id as file_id,
							 (CASE
							 WHEN ctfmm.file_type = 1 THEN 'pdf'
							 WHEN ctfmm.file_type = 2 THEN 'ppt'
							 WHEN ctfmm.file_type = 3 THEN 'video'
							 WHEN ctfmm.file_type = 4 THEN 'epub'
							 WHEN ctfmm.file_type = 5 THEN 'doc'
							 WHEN ctfmm.file_type = 6 THEN 'image'
							 ELSE 'test'
							 END ) AS file_type ,
							 ctfmm.file_url ,  ctfmm.title , ctfmm.description ,ctsm.id as test_series_id ,
								ctsm.test_series_name , ctsm.description as test_description
							 FROM course_segment_element as cse
							 LEFT JOIN course_topic_file_meta_master as ctfmm ON cse.element_fk = ctfmm.id and cse.type != 'test'
							 RIGHT JOIN course_topic_master 	as ctm ON ctm.id = cse.segment_fk and ctm.course_fk = $course_id and ctm.is_preview = 1
							 LEFT JOIN course_test_series_master as ctsm ON ctsm.id = cse.element_fk and cse.type = 'test'
							 WHERE cse.id is not null
							 ORDER BY ctm.position asc";

			$result_files =  $this->db->query($sql)->result_array();
			if(count($result_files)> 0 ){
				foreach($result_files as $r_f){
					$course_info['curriculam']['title'] = $r_f['topic_name'];
					$course_info['curriculam']['topic_id'] = $r_f['topic_id'];
					if($r_f['type']=="test"){
						$seg = array();
						$seg['id']= $r_f['file_id'];
						$seg['file']= $r_f['file_type'];
						$seg['link']= $r_f['test_series_id'];
						$seg['title'] = $r_f['test_series_name'];
						$seg['description'] = $r_f['test_description'];
						/* check if user attempt this test series */
						$check_user_attemp =  $this->db->query("select id
															from course_test_series_report
															WHERE user_id = $user_id
															and test_series_id =  ".$r_f['test_series_id'])->row_array();
						$seg['is_user_attemp'] = ($check_user_attemp > 0)?$check_user_attemp['id']:"";

					}else{
						$seg = array(
							'id'=> $r_f['file_id'],
							'file'=> $r_f['file_type'],
							"link"=>  $r_f['file_url'],
							"title"=>  $r_f['title'],
							"description"=>  $r_f['description']
						);
					}
					$seg['tag'] = $r_f['tag'];
					$course_info['curriculam']['file_meta'][] = $seg;
				}
			}
			//
			/* check if user already purchsethis course */
			$this->db->where("user_id",$user_id);
			$this->db->where("course_id",$course_id);
			$this->db->where("transaction_status",1);
			$out =  $this->db->get('course_transaction_record')->row();
			$course_info['is_purchased'] = "0";
			if($out){$course_info['is_purchased'] = "1";}
			/* get user review */
			$this->db->where('user_id',$user_id);
			$this->db->where('course_fk_id',$course_id);
			$review = $this->db->get('course_user_rating')->row();
			if(count($review)){
				$course_info['review']['rating']= $review->rating;
				$course_info['review']['text']= $review->text;
			}

			$date = date("y-m-d");

			$sql = "SELECT id ,title ,cover_image,desc_header_image,mrp,
							is_new , course_rating_count as rating ,
							`course_review_count` as review_count, `course_learner` as learner
					from course_master as cm
					where match(cm.tags) against((select cmt.tags
															from course_master cmt
															where cmt.id = $course_id ) in boolean mode)
					and id != $course_id
					and publish = 1
					and (`start_date` = '0000-00-00' or ('$date' >= start_date and '$date' <= end_date ))
					limit 0,3 ";


			$match = $this->db->query($sql)->result_array();

			if(count($match) > 0){
				$course_info['related_course'] = $match;
			}

			/* points conversion rate */
			$course_info['points_conversion_rate'] = get_db_meta_key('RUPEE_CONVERSION_RATE');


			$course_info['element_meta'] = $this->db->query('SELECT id , topic_name ,image, position , (select count(id) as total from course_segment_element as cse where cse.segment_fk = ctm.id ) as own FROM course_topic_master ctm WHERE ctm.course_fk = '.$course_info['id'])->result_array();
			$course_info['viewed_meta'] = $this->db->query('SELECT segment_id FROM user_section_complition_master WHERE user_id = '.$user_id.' and section_id= '.$course_info['id'])->result_array();
			$concept_meta = $this->concept_no_of_user($course_id);
			$course_info['concept_learner'] = $concept_meta['concept_learner'];
			$course_info['concept_learner_list'] = $concept_meta['concept_learner_list'];
			return_data(true,"Course information.",$course_info);
		}
		return_data(false,"Course not found.",array());
	}

	private function validate_get_single_course_info_raw(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('course_id','course_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
/***
 *       _____                                           _  _    _                    _
 *      / ____|                                         (_)| |  | |                  | |
 *     | |      ___   _   _  _ __  ___   ___  __      __ _ | |_ | |__      ___  __ _ | |_  ___   __ _   ___   _ __  _   _
 *     | |     / _ \ | | | || '__|/ __| / _ \ \ \ /\ / /| || __|| '_ \    / __|/ _` || __|/ _ \ / _` | / _ \ | '__|| | | |
 *     | |____| (_) || |_| || |   \__ \|  __/  \ V  V / | || |_ | | | |  | (__| (_| || |_|  __/| (_| || (_) || |   | |_| |
 *      \_____|\___/  \__,_||_|   |___/ \___|   \_/\_/  |_| \__||_| |_|   \___|\__,_| \__|\___| \__, | \___/ |_|    \__, |
 *                                                                                               __/ |               __/ |
 *                                                                                              |___/               |___/
 */

	public function get_all_category_data(){
		$this->validate_get_all_category_data();
		$data['user_id'] 			= $this->input->post('user_id');
		$data['course_type'] 	= $this->input->post('course_type');
		$data['sub_cat']      = $this->input->post('sub_cat');
		$return = $this->course_model->get_course_sub_cate_landing_page($data);
		if($return){

			$records =  array();
			foreach($return as $l ){
				$l['element_meta'] = $this->db->query('SELECT id , topic_name , position , (select count(id) as total from course_segment_element as cse where cse.segment_fk = ctm.id ) as own , image FROM course_topic_master ctm WHERE ctm.course_fk = '.$l['id'])->result_array();
				$l['viewed_meta'] = $this->db->query('SELECT segment_id FROM user_section_complition_master WHERE user_id = '.$data['user_id'].' and section_id= '.$l['id'])->result_array();
				$records[] = $l;
			}
			return_data(true,"Course information.",$records);
		}
		return_data(false,"Course not found.",array());

	}

	private function validate_get_all_category_data(){

		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('course_type','course_type', 'trim|is_natural_no_zero');
		$this->form_validation->set_rules('sub_cat','sub_cat', 'trim|is_natural_no_zero');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function get_all_category_data_exam(){
		$this->validate_get_all_category_data();
		$data['user_id'] 			= $this->input->post('user_id');
		$data['course_type'] 	= $this->input->post('course_type');
		$data['sub_cat']      = $this->input->post('sub_cat');
		$return = $this->course_model->get_course_sub_cate_landing_page_exam($data);
		if($return){
			$records =  array();
			foreach($return as $l ){
				$l['element_meta'] = $this->db->query('SELECT id , topic_name , position , (select count(id) as total from course_segment_element as cse where cse.segment_fk = ctm.id ) as own , image FROM course_topic_master ctm WHERE ctm.course_fk = '.$l['id'])->result_array();
				$l['viewed_meta'] = $this->db->query('SELECT segment_id FROM user_section_complition_master WHERE user_id = '.$data['user_id'].' and section_id= '.$l['id'])->result_array();
				$records[] = $l;
			}
			return_data(true,"Exam information.",$records);
		}
		return_data(false,"Exam not found.",array());
	}


	public function get_all_reviews_of_course(){

		$reviews = array(array("id"=>"1",
								"name"=>"Vipul Sharma",
								"profile_picture"=>"https://s3.ap-south-1.amazonaws.com/dams-apps-production/profile_images/1_1498643188582_image.jpg",
								"text"=>"It was nice to be here.",
								"creation_time"=>"1498634128235",
								"rating"=>"4.1"),
						array("id"=>"2",
								"name"=>"Ansul Sharma",
								"profile_picture"=>"https://s3.ap-south-1.amazonaws.com/dams-apps-production/Test_Folder/profile_images/4_1505918790744_image.jpg",
								"text"=>"It was nice to be here.",
								"creation_time"=>"1498634128235",
								"rating"=>"4.2")
						);
		return_data(true,"Review list.",$reviews);
	}

	private function validate_get_all_reviews_of_course(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('course_id','course_id', 'trim|required');
		$this->form_validation->set_rules('last_id','last_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *                              _
 *         /\                  | |
 *        /  \    _ __   _ __  | | _   _    ___  ___   _   _  _ __    ___   _ __
 *       / /\ \  | '_ \ | '_ \ | || | | |  / __|/ _ \ | | | || '_ \  / _ \ | '_ \
 *      / ____ \ | |_) || |_) || || |_| | | (__| (_) || |_| || |_) || (_) || | | |
 *     /_/    \_\| .__/ | .__/ |_| \__, |  \___|\___/  \__,_|| .__/  \___/ |_| |_|
 *               | |    | |         __/ |                    | |
 *               |_|    |_|        |___/                     |_|
 */
	public function apply_coupon_code(){
		$this->validate_apply_coupon_code();
		/*
		* check if coupon code is active or availle this time
		SELECT coupon_tilte , date(start) , (SELECT id  FROM `course_transaction_record` WHERE `user_id` = 34 and `course_id` = 1 and `coupon_applied` = 1 ) as coupon_applied_id
		FROM `course_coupon_master` as ccm
		join coupon_course_relation_master ccrm on ccrm.course_id = 1 and ccrm.coupon_id = ccm.id
		WHERE coupon_tilte = "ck"
		and ccm.state = 0
		and "2018-09-12" between date(start) and date(end)
		*/
		$user_id = $this->input->post('user_id');
		$course_id = $this->input->post('course_id');
		$coupon_code = $this->input->post('coupon_code');
		$c_date = date("Y-m-d");

		$coupon_query  = "SELECT ccm.id , ccm.coupon_type , ccm.coupon_value , ccm.coupon_for
						FROM `course_coupon_master` as ccm
						WHERE coupon_tilte = '$coupon_code'
						and ccm.state = 0
						and '$c_date' between date(start) and date(end)
						and (SELECT id
									FROM course_transaction_record
									WHERE user_id = $user_id
									and coupon_applied = ccm.id  limit 0,1 ) is NULL ";
		$check =  $this->db->query($coupon_query)->row();

		if(count($check)>0){
			if($check->coupon_for == 0 ){
				$coupon_query  = "SELECT ccm.id , ccm.coupon_type , ccm.coupon_value
						FROM `course_coupon_master` as ccm
						join coupon_course_relation_master ccrm on ccrm.course_id = $course_id and ccrm.coupon_id = ccm.id
						WHERE coupon_tilte = '$coupon_code'
						and ccm.state = 0
						and '$c_date' between date(start) and date(end)
						and (SELECT id
									FROM course_transaction_record
									WHERE user_id = $user_id
									and course_id =  $course_id
									and coupon_applied = ccm.id  limit 0,1 ) is NULL ";
				$coupon_details =  $this->db->query($coupon_query)->row_array();

				if(count($coupon_details) > 0 ){
					return_data(true,"Coupon code is valid .",$coupon_details);
				}
			}
			if($check->coupon_for == 1 ){
				$user_query  = "SELECT *
						FROM coupon_user_relation
						WHERE coupon_id = '$check->id'
						and user_id = '$user_id'

						";
				$user_c_get =  $this->db->query($coupon_query)->row();
				if(count($user_c_get) > 0 ){
					return_data(true,"Coupon code is valid .",$check);
				}
			}

		}
		return_data(false,"Coupon code is not valid .",array());
	}

	public function validate_apply_coupon_code(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('course_id','course_id', 'trim|required');
		$this->form_validation->set_rules('coupon_code','coupon_code', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
/***
 *                _  _    __  _  _         _          __
 *         /\    | || |  / _|(_)| |       (_)        / _|
 *        /  \   | || | | |_  _ | |  ___   _  _ __  | |_  ___
 *       / /\ \  | || | |  _|| || | / _ \ | || '_ \ |  _|/ _ \
 *      / ____ \ | || | | |  | || ||  __/ | || | | || | | (_) |
 *     /_/    \_\|_||_| |_|  |_||_| \___| |_||_| |_||_|  \___/
 *
 *
 */
	public function get_all_file_info(){
		$this->validate_get_all_file_info();
		$course_id =  $this->input->post('course_id');
		$user_id =  $this->input->post('user_id');
		/* get file information*/
			$sql =  "SELECT cse.* ,ctm.id as topic_id ,  ctm.topic_name as topic_name , ctm.image as file_image  ,ctfmm.id as file_id,if(ctfmm.video_type='', '0',ctfmm.video_type) as video_type,
					(CASE
					WHEN ctfmm.file_type = 1 THEN 'pdf'
					WHEN ctfmm.file_type = 2 THEN 'ppt'
					WHEN ctfmm.file_type = 3 THEN 'video'
					WHEN ctfmm.file_type = 4 THEN 'epub'
					WHEN ctfmm.file_type = 5 THEN 'doc'
					WHEN ctfmm.file_type = 6 THEN 'image'
					WHEN ctfmm.file_type = 7 THEN 'concept'
					ELSE 'test'
					END ) AS file_type ,if(upb.id!='',1,0) AS book_mark_status,
					ctfmm.file_url ,  ctfmm.title ,ctfmm.thumbnail_url as image  ,  ctfmm.description ,ctsm.id as test_series_id ,
						ctsm.test_series_name , ctsm.description as test_description
					FROM course_segment_element as cse
					LEFT JOIN course_topic_file_meta_master as ctfmm ON cse.element_fk = ctfmm.id and cse.type != 'test'
					left join user_post_bookmark as upb on ctfmm.id=upb.post_id
					RIGHT JOIN course_topic_master 	as ctm ON ctm.id = cse.segment_fk and ctm.course_fk = $course_id
					LEFT JOIN course_test_series_master as ctsm ON ctsm.id = cse.element_fk and cse.type = 'test'
					WHERE cse.id is not null
					ORDER BY ctm.position , cse.segment_fk asc";
		$file_info  =  $this->db->query($sql)->result_array();
		//echo $this->db->last_query();
		$title = "";
		$array = array();
		$temp = array();
		foreach($file_info as $fk){
			if($fk['type']=="test"){
				$fk['title'] = $fk['test_series_name'];
				$fk['description'] = $fk['test_description'];
				$fk['file_url'] = $fk['test_series_id'];

				$check_user_attemp =  $this->db->query("select id
				from course_test_series_report
				WHERE user_id = $user_id
				and test_series_id =  ".$fk['test_series_id'])->row_array();
			}

			if($title != $fk['topic_name']){
				if($temp){$array[] = $temp;$temp = array();}

				$title = $temp["title"] = $fk['topic_name'];
				$temp["topic_id"] = $fk['topic_id'];
				$temp["image"] = $fk['file_image'];
				$ins = array(
							'id'=>$fk['file_id'],
							'file'=>$fk['file_type'],
							'link'=>$fk['file_url'],
							'title'=>$fk['title'],
							'image'=>$fk['image'],
							'description'=>$fk['description'],
							'video_type'=>$fk['video_type'],
							'is_bookmarked'=>$fk['book_mark_status']
						);
				if($fk['type']=="test"){
					$ins['is_user_attemp'] = ($check_user_attemp > 0)?$check_user_attemp['id']:"";
				}
				$ins['tag'] = $fk['tag'];

				$temp['file_meta'][] = $ins;
			}elseif($title == $fk['topic_name']){
			 	$ins = array(
							'id'=>$fk['file_id'],
							'file'=>$fk['file_type'],
							'link'=>$fk['file_url'],
							'title'=>$fk['title'],
							'image'=>$fk['image'],
							'description'=>$fk['description'],
							'video_type'=>$fk['video_type'],	
							'is_bookmarked'=>$fk['book_mark_status']
						);
				if($fk['type']=="test"){
					$ins['is_user_attemp'] = ($check_user_attemp > 0)?$check_user_attemp['id']:"";
				}
				$ins['tag'] = $fk['tag'];
				$temp['file_meta'][] = $ins;
			}
		}
		if($temp){$array[] = $temp;$temp = array();}
		return_data(true,"Course file description.",$array);
	}

	private function validate_get_all_file_info(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('course_id','course_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *       _____        _     ______        ____   _
 *      / ____|      | |   |  ____|/\    / __ \ ( )
 *     | |  __   ___ | |_  | |__  /  \  | |  | ||/ ___
 *     | | |_ | / _ \| __| |  __|/ /\ \ | |  | |  / __|
 *     | |__| ||  __/| |_  | |  / ____ \| |__| |  \__ \
 *      \_____| \___| \__| |_| /_/    \_\\___\_\  |___/
 *
 *
 */
	public function get_faq(){
		$this->validate_get_faq();

		$this->db->where('course_id',1);
		$r = $this->db->get('course_faq_master')->result_array();

		return_data(true,"Faq list.",$r);
	}

	private function validate_get_faq(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('course_id','course_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *      _____        _      _   _______                                    _    _
 *     |  __ \      (_)    | | |__   __|                                  | |  (_)
 *     | |__) |__ _  _   __| |    | | _ __  __ _  _ __   ___   __ _   ___ | |_  _   ___   _ __
 *     |  ___// _` || | / _` |    | || '__|/ _` || '_ \ / __| / _` | / __|| __|| | / _ \ | '_ \
 *     | |   | (_| || || (_| |    | || |  | (_| || | | |\__ \| (_| || (__ | |_ | || (_) || | | |
 *     |_|    \__,_||_| \__,_|    |_||_|   \__,_||_| |_||___/ \__,_| \___| \__||_| \___/ |_| |_|
 *
 *
 */

	/* INITIALIZE PAYMENT */
	public function initialize_course_transaction(){
		$this->validate_initialize_course_transaction();
		$user_id = $this->input->post('user_id');
		$course_id = $this->input->post('course_id');
		$key = rand(111,999).substr(md5(time()),5);
		$array = array(
			'user_id'=>$user_id ,
			'is_dams' => 1 ,
			'course_id'=> $course_id ,
			'pre_transaction_id'=>$key ,
			'course_price'=>$this->input->post('course_price') ,
			'tax'=>$this->input->post('tax') ,
			'points_used'=>$this->input->post('points_used') ,
			'points_rate'=>$this->input->post('points_rate') ,
			'coupon_applied' => $this->input->post('coupon_applied'),
			'creation_time'=> milliseconds()
			);

		/*
		* get course instructor and add his share w.r.t. course
		*/
		$query = " SELECT instructor_id , instructor_share  FROM `course_master` where  id =  $course_id ";
		$instructor_relation = $this->db->query($query)->row_array();

		if(count($instructor_relation) > 0 ){
			$array['instructor_id'] = $instructor_relation['instructor_id'];
			$array['instructor_share'] = ($this->input->post('course_price') * $instructor_relation['instructor_share'])/100;
		}
		if($this->input->get('pay_via') == "CCAVENUE"){
			$array['pay_via'] = 'CCAVENUE';
		}else{
			$array['pay_via'] = 'PAY_TM';
		}

		$this->db->insert('course_transaction_record',$array);
		return_data(true,"Payment initialized.",array('pre_transaction_id'=>$key));
	}

	private function validate_initialize_course_transaction(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('course_id','course_id', 'trim|required');
		$this->form_validation->set_rules('course_price','course_price', 'trim|required');
		if(!array_key_exists('coupon_applied', $_POST)){
			$_POST['coupon_applied'] = "";
		}
		if(!array_key_exists('tax', $_POST)){
			$_POST['tax'] = "";
		}
		if(!array_key_exists('points_rate', $_POST)){
			$_POST['points_rate'] = "";
		}
		if(!array_key_exists('points_used', $_POST)){
			$_POST['points_used'] = "";
		}
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();


		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
		if($this->input->get('pay_via') != ""){
			if($this->input->get('pay_via') !== "CCAVENUE" || $this->input->get('pay_via') !== "PAY_TM" ){
				return_data(false,"Please provide valid payment gateway !",array(),array());
			}
		}
	}
	/* COMPLETE PAYMENT */
	public function complete_course_transaction(){
		$this->validate_complete_course_transaction();
		$user_id = $this->input->post('user_id');
		$course_id = $this->input->post('course_id');
		$pre_transaction_id = $this->input->post('pre_transaction_id');
		$post_transaction_id = $this->input->post('post_transaction_id');
		$array = array(
			'post_transaction_id'=>$post_transaction_id ,
			'transaction_status'=>1
			);
		$this->db->where('pre_transaction_id',$pre_transaction_id);
		$this->db->update('course_transaction_record',$array);

		/* select transaction record */
		$this->db->where('post_transaction_id',$post_transaction_id);
		$c_record = $this->db->get('course_transaction_record')->row();
		if($c_record->points_used > 0 ){
			activity_rewards($user_id ,"REDEEM_FOR_COURSE","-");
		}

		/* update learner for this course we have course id */
		$count = $this->db->query('select count(id) as total from  course_transaction_record where transaction_status = 1 and  course_id  ='.$course_id)->row_array();
		$learner = ($count && $count['total'] > 0 )?$count['total']:0;
		$this->db->where('id',$course_id);
    	$array = array('course_learner'=>$learner);
		$this->db->update("course_master",$array);

		/* update instructor counter */
		//get instructor id from row
		$ins_record = $this->db->select('instructor_id')->where('post_transaction_id',$post_transaction_id)->get('course_transaction_record')->row();
		if(count($ins_record)){
			$instructor = $ins_record->instructor_id;
			$learner_count_query =  "select count(id) as total  from course_transaction_record where instructor_id =  $instructor and transaction_status = 1";
			$learner_count_query = $this->db->query($learner_count_query)->row();
			if(count($learner_count_query) > 0 ){
				$this->db->where('user_id',$instructor);
				$array = array('learner_count'=>$learner_count_query->total);
				$this->db->update('course_instructor_information',$array);
			}
		}

		/* send message on mobile about it */
		modules::run("data_model/user/mobile_auth/send_message_on_purchase",array('user_id'=>$user_id));
		/* send message on email about it */
		modules::run("data_model/emailer/payment_email/payment_done",array('user_id'=>$user_id,'pre_transaction_id'=>$pre_transaction_id));

		return_data(true,"Payment complete.",array());
	}

	private function validate_complete_course_transaction(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('pre_transaction_id','pre_transaction_id', 'trim|required');
		$this->form_validation->set_rules('post_transaction_id','post_transaction_id', 'trim|required');
		$this->form_validation->set_rules('course_id','course_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *      ______                  _______                                          _    _
 *     |  ____|                |__   __|                                        | |  (_)
 *     | |__  _ __  ___   ___     | | _ __  __ _  _ __   ___   __ _   ___  _ __ | |_  _   ___   _ __
 *     |  __|| '__|/ _ \ / _ \    | || '__|/ _` || '_ \ / __| / _` | / __|| '__|| __|| | / _ \ | '_ \
 *     | |   | |  |  __/|  __/    | || |  | (_| || | | |\__ \| (_| || (__ | |   | |_ | || (_) || | | |
 *     |_|   |_|   \___| \___|    |_||_|   \__,_||_| |_||___/ \__,_| \___||_|    \__||_| \___/ |_| |_|
 *
 *
 */
	public function free_course_transaction(){
		$this->validate_free_course_transaction();
		$user_id = $this->input->post('user_id');
		$course_id = $this->input->post('course_id');
		$key = md5(time());
		$array = array(
			'user_id'=>$user_id ,
			'is_dams' => 1 ,
			'course_id'=> $course_id ,
			'pre_transaction_id'=>$key ,
			'post_transaction_id'=>md5($key) ,
			'course_price'=>$this->input->post('course_price'),
			'transaction_status'=>1,
			'creation_time'=> milliseconds(),
			'course_price'=>0 ,
			'coupon_applied' => $this->input->post('coupon_applied'),
			'tax'=>$this->input->post('tax') ,
			'points_used'=>$this->input->post('points_used') ,
			'points_rate'=>$this->input->post('points_rate')
		);

		/*
		* get course instructor and add his share w.r.t. course
		*/
		$query = " SELECT instructor_id , instructor_share  FROM `course_master` where  id =  $course_id ";
		$instructor_relation = $this->db->query($query)->row_array();

		if(count($instructor_relation) > 0 ){
			$array['instructor_id'] = $instructor_relation['instructor_id'];
			$array['instructor_share'] = 0;
		}
		$this->db->insert('course_transaction_record',$array);

		/* update learner for this course we have course id */
		$count = $this->db->query('select count(id) as total from  course_transaction_record where transaction_status = 1 and  course_id  ='.$course_id)->row_array();
		$learner = ($count && $count['total'] > 0 )?$count['total']:0;
		$this->db->where('id',$course_id);
		$array = array('course_learner'=>$learner);
		$this->db->update("course_master",$array);
		/* update instructor counter */
		//get instructor id from row

		if(count($instructor_relation) > 0 ){
			$instructor = $instructor_relation['instructor_id'];
			$learner_count_query =  "select count(id) as total  from course_transaction_record where instructor_id =  $instructor and transaction_status = 1";
			$learner_count_query = $this->db->query($learner_count_query)->row();
			if(count($learner_count_query) > 0 ){
				$this->db->where('user_id',$instructor);
				$array = array('learner_count'=>$learner_count_query->total);
				$this->db->update('course_instructor_information',$array);
			}
		}
		return_data(true,"Added to my course.",array());
	}

	private function validate_free_course_transaction(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('course_id','course_id', 'trim|required');

		if(!array_key_exists('coupon_applied', $_POST)){
			$_POST['coupon_applied'] = "";
		}
		if(!array_key_exists('tax', $_POST)){
			$_POST['tax'] = "";
		}
		if(!array_key_exists('points_rate', $_POST)){
			$_POST['points_rate'] = "";
		}
		if(!array_key_exists('points_used', $_POST)){
			$_POST['points_used'] = "";
		}

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function complete_element(){
		$this->validate_complete_element();


		$array = array(
			"user_id"=>$this->input->post('user_id'),
			"section_type"=>$this->input->post('section_type'),
			"segment_id"=>$this->input->post('segment_id'),
			"section_id"=>$this->input->post('section_id'),
			"creation_time"=>milliseconds()
		);

		// check if already viewed
		$this->db->where('user_id',$array['user_id']);
		$this->db->where('section_type',$array['section_type']);
		$this->db->where('segment_id',$array['segment_id']);
		$this->db->where('section_id',$array['section_id']);
		$if_exsit = $this->db->get('user_section_complition_master')->row();

		if(count($if_exsit) > 0 ){

		}else{
			$this->db->insert('user_section_complition_master',$array);
		}
		return_data(true,"Viewed.",array());
	}

	private function validate_complete_element(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('section_type','section_type', 'trim|required');
		$this->form_validation->set_rules('segment_id','segment_id', 'trim|required');
		$this->form_validation->set_rules('section_id','section_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	private function concept_no_of_user($section_id){
		$return = array();
		$this->db->where('section_id',$section_id);
		$this->db->group_by('user_id'); 
		$return['concept_learner'] = $count_user = $this->db->get('user_section_complition_master')->num_rows();
		$return['concept_learner_list'] = array(); 
		if(count($count_user) > 0 ){
			$this->db->select ('user_section_complition_master.user_id,users.name,users.profile_picture');
			$this->db->join('users', 'users.id = user_section_complition_master.user_id'); 
			$this->db->where('section_id',$section_id);
			$this->db->group_by('user_section_complition_master.user_id'); 
			$this->db->order_by('user_section_complition_master.id','DESC');
			$this->db->limit(5);
			$return['concept_learner_list'] = $this->db->get('user_section_complition_master')->result_array();
		}
		return $return;
	}
}
