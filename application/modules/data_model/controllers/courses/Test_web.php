<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test_web extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model('test_series_model');
	}

   public function view(){
      $test_series_id = $this->input->get('test_series_id');
      if($test = $this->test_series_model->get_test_series($test_series_id) ){
          $data['basic_info'] = $test;
          $data['question_bank'] = $this->test_series_model->get_test_series_question($test_series_id);
      }
      $this->load->view('test_web_view/test', $data);
   }


    public function result(){
        $this->load->view('test_web_view/result_view', array());
    }


    public function view_solution(){
        $this->load->view('test_web_view/view_solution', array());
    }

}   