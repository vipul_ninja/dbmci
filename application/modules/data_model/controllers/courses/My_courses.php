<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_courses extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model('course_model');
	}

	public function get_list_of_my_courses(){

		$this->validate_get_list_of_my_courses();
		$user_id = $this->input->post('user_id');

		$return['course_list'] =  $this->course_model->my_course_list($user_id);

		if($return['course_list']){
			return_data(true,"Course information.",$return);
		}
		return_data(false,"Course not found.",array());
	}

	private function validate_get_list_of_my_courses(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}
}	