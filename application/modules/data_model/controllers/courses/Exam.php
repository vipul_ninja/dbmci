<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Exam extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper("services");
        $this->load->model('exam_model');
    }

    /***
     *                   _     _                  _       
     *                  | |   | |                (_)      
     *       __ _   ___ | |_  | |__    __ _  ___  _   ___ 
     *      / _` | / _ \| __| | '_ \  / _` |/ __|| | / __|
     *     | (_| ||  __/| |_  | |_) || (_| |\__ \| || (__ 
     *      \__, | \___| \__| |_.__/  \__,_||___/|_| \___|
     *       __/ |                                        
     *      |___/                                         
     */

    public function get_basic_data(){
        $this->validate_get_basic_data();
        $in = array('course_id'=>$this->input->post('id'));
        $data['basic'] = $this->exam_model->get_exam_basic_data($in);
        $data['tiles'] = array();
        if($this->exam_model->get_test_list_count($in)){
            $type_text = $this->lang->line('tests');
            $data['tiles'][] = array('tile_name' => $type_text, 'type' => 'test', 'c_code' => '#0000CC');
        }
        
        if($this->exam_model->get_video_list_count($in)){
            $video_text = $this->lang->line('videos');
            $data['tiles'][] = array('tile_name' =>  $video_text, 'type' => 'video', 'c_code' => '#F68930');
        }

        // if($this->exam_model->get_course_practice_list_count($in)){
        //     $data['tiles'][] = array('tile_name' => 'Practice', 'type' => 'practice', 'c_code' => '#2c5e87');
        // }
        
        if($data['basic']['description'] != "" ){
            $book_text = $this->lang->line('books');
            $data['tiles'][] = array('tile_name' =>  $book_text, 'type' => 'book', 'c_code' => '#9c5e87');
        }

        if($this->exam_model->get_concept_list_count($in)){
            $concept_text = $this->lang->line('concepts');
            $data['tiles'][] = array('tile_name' =>  $concept_text , 'type' => 'concept', 'c_code' => '#FF2D2D');
        }        

        return_data(true, 'course found', $data);
    }

    private function validate_get_basic_data(){
        post_check();
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required|integer');
        $this->form_validation->set_rules('id', 'id', 'trim|required|is_natural_no_zero');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }
    }

    /*
     *                   _     _______          _   
     *                  | |   |__   __|        | |  
     *       __ _   ___ | |_     | |  ___  ___ | |_ 
     *      / _` | / _ \| __|    | | / _ \/ __|| __|
     *     | (_| ||  __/| |_     | ||  __/\__ \| |_ 
     *      \__, | \___| \__|    |_| \___||___/ \__|
     *       __/ |                                  
     *      |___/                                   
     */

    public function get_test_data(){
        $this->validate_get_test_data();
        //get test starting point 
        $layer = $this->input->post('layer');
        $course_id = $this->input->post('id');

        $main_id =  ($this->input->post('main_id'))?$this->input->post('main_id'):'';

        /* give with all master categories */
        if ($layer == 1) {
            $data['layer'] = $layer;
            $data['list'] = $this->exam_model->test_main_category_list($course_id);
            
            if(count($data['list']) > 1){
                $a  =  $data['list'];
                $temp = array();
                foreach($a as $out){
                    $find = array('course_id'=>$course_id,'main_id'=>$out->id);
                    $out->total = $this->exam_model->test_type_count($find);
                    $temp[] = $out;
                }
                $data['list'] = $temp;
            }elseif(count($data['list']) == 1){
                $layer = "2" ;
                $main_id  = $data['list'][0]->id;
            }
            
        }



        /* give with sub categories */
        if($layer == 2 ){
            $data['layer'] = $layer;
            $in = array('main_id'=>$main_id,'course_id'=>$course_id);
            $data['list'] = $this->exam_model->test_sub_category_list($in);

            if(count($data['list'])){
                $a  =  $data['list'];
                $temp = array();
                foreach($a as $out){
                    $find = array('course_id'=>$course_id,'sub_id'=>$out->id);
                    $out->total = $this->exam_model->test_type_count($find);
                    $temp[] = $out;
                }
                $data['list'] = $temp;
            }
        }

        /* give all test on the basis of test type */
        if ($layer == 3) {
            $data['layer'] = $layer;
            $in = array('sub_id'=>$this->input->post('sub_id'),
                        'course_id'=>$course_id,
                        'test_type'=>$this->input->post('test_type')
                        );

            $data['list']  = $this->exam_model->get_test_list($in);
            
        }
        return_data(true, 'Test data', $data);
    }

    private function validate_get_test_data(){
        post_check();
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required|integer');
        $this->form_validation->set_rules('id', 'id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('layer', 'layer', 'trim|required|is_natural_no_zero');
        $layer = $this->input->post('layer');

        if ($layer == 2) {
            $this->form_validation->set_rules('main_id', 'main_id', 'trim|required|is_natural_no_zero');
        }
        
        if ($layer == 3) {
            $this->form_validation->set_rules('sub_id', 'sub_id', 'trim|required|is_natural_no_zero');
            $this->form_validation->set_rules('test_type', 'test_type', 'trim|required|is_natural_no_zero');
        }

        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }
    }

    /*     * *
     *       _____        _    __      __ _      _                   
     *      / ____|      | |   \ \    / /(_)    | |                  
     *     | |  __   ___ | |_   \ \  / /  _   __| |  ___   ___   ___ 
     *     | | |_ | / _ \| __|   \ \/ /  | | / _` | / _ \ / _ \ / __|
     *     | |__| ||  __/| |_     \  /   | || (_| ||  __/| (_) |\__ \
     *      \_____| \___| \__|     \/    |_| \__,_| \___| \___/ |___/
     *                                                               
     *                                                               
     */

    public function get_video_data(){
        $this->validate_get_video_data();
        //get test starting point 
        $layer = $this->input->post('layer');
        $course_id = $this->input->post('id');
        $subject_id =  ($this->input->post('subject_id'))?$this->input->post('subject_id'):'';
        if($layer == 1 ){
            $data['layer'] = $layer;
            $find = array('course_id'=>$course_id);
            $data['list'] = $this->exam_model->get_course_video_subject($find);
            if(count($data['list']) > 1){
                $a  =  $data['list'];
                $temp = array();
                foreach($a as $out){
                    $out->total = array(array('count'=>$out->total,'text'=>$this->video_text($out->total)));
                    $temp[] = $out;
                }
                $data['list'] = $temp;
            }elseif(count($data['list']) == 1){
                $layer = "2" ;
                $subject_id  = $data['list'][0]->id;
            }
        }
        if($layer == 2 ){
            $data['layer'] = $layer;
            $find = array('course_id'=>$course_id,'subject_id'=>$subject_id);
            $data['list'] = $this->exam_model->get_course_video_subject_topic($find);
            if(count($data['list'])){
                $a  =  $data['list'];
                $temp = array();
                foreach($a as $out){
                    $out->total = array(array('count'=>$out->total,'text'=>$this->video_text($out->total)));
                    $temp[] = $out;
                }
                $data['list'] = $temp;
            }
        }
        if ($layer == 3) {
            $data['layer'] = $layer;
            $find = array(
                        'course_id' =>$course_id,
                        'topic_id' =>$this->input->post('topic_id')
                        );
            $data['list']  = $this->exam_model->get_video_list($find);
        }
        return_data(true, 'Video data', $data);
    }

    private function video_text($number){
        if($number == 1 ){
            return 'Video';
        }
        return 'Videos';
    }

    private function validate_get_video_data(){
        post_check();
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required|integer');
        $this->form_validation->set_rules('id', 'id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('layer', 'layer', 'trim|required|is_natural_no_zero');
        $layer = $this->input->post('layer');

        if ($layer == 2) {
            $this->form_validation->set_rules('subject_id', 'subject_id', 'trim|required|is_natural_no_zero');
        }
        if ($layer == 3) {
            $this->form_validation->set_rules('topic_id', 'topic_id', 'trim|required|is_natural_no_zero');
        }

        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }
    }

    /*     * *
     *       _____        _      _____                                 _   
     *      / ____|      | |    / ____|                               | |  
     *     | |  __   ___ | |_  | |      ___   _ __    ___  ___  _ __  | |_ 
     *     | | |_ | / _ \| __| | |     / _ \ | '_ \  / __|/ _ \| '_ \ | __|
     *     | |__| ||  __/| |_  | |____| (_) || | | || (__|  __/| |_) || |_ 
     *      \_____| \___| \__|  \_____|\___/ |_| |_| \___|\___|| .__/  \__|
     *                                                         | |         
     *                                                         |_|         
     */

    public function get_concept(){
        $this->validate_get_concept();
        $layer = $this->input->post('layer');
        $course_id = $this->input->post('id');

        $subject_id =  ($this->input->post('subject_id'))?$this->input->post('subject_id'):'';

        if($layer == 1 ){
            $data['layer'] = $layer;
            $find = array('course_id'=>$course_id);
            $data['list'] = $this->exam_model->get_course_concept_subject($find);
            if(count($data['list']) > 1 ){
                $a  =  $data['list'];
                $temp = array();
                foreach($a as $out){
                    $out->total = array(array('count'=>$out->total,'text'=>$this->concept_text($out->total)));
                    $temp[] = $out;
                }
                $data['list'] = $temp;
            }elseif(count($data['list']) == 1){
                $layer = "2" ;
                $subject_id  = $data['list'][0]->id;
            }

        }

        if($layer == 2 ){
            $data['layer'] = $layer;
            $find = array('course_id'=>$course_id,'subject_id'=>$subject_id);
            $data['list'] = $this->exam_model->get_course_concept_subject_topic($find);
            if(count($data['list'])){
                $a  =  $data['list'];
                $temp = array();
                foreach($a as $out){
                    $out->total = array(array('count'=>$out->total,'text'=>$this->concept_text($out->total)));
                    $temp[] = $out;
                }
                $data['list'] = $temp;
            }
        }

        if($layer == 3 ){
            // get concept on the basis on topic 
            $data['layer'] = $layer;
            $find = array(
                        'course_id' =>$course_id,
                        'topic_id' =>$this->input->post('topic_id'),
                        'user_id' => $this->input->post('user_id')
                        );
            $data['list']  = $this->exam_model->get_concept_list($find);
        }
        return_data(true, 'Test data', $data);
    }

    private function concept_text($number){
        if($number == 1 ){
            return 'Concept';
        }
        return 'Concepts';
    }

    private function validate_get_concept(){
        post_check();
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required|integer');
        $this->form_validation->set_rules('id', 'id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('layer', 'layer', 'trim|required|is_natural_no_zero');
        $layer = $this->input->post('layer');

        if ($layer == 2) {
            $this->form_validation->set_rules('subject_id', 'subject_id', 'trim|required|is_natural_no_zero');
        }

        if ($layer == 3) {
            $this->form_validation->set_rules('topic_id', 'topic_id', 'trim|required|is_natural_no_zero');
        }

        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }
    }

    /*     * *
     *       _____        _     _____                    _    _            
     *      / ____|      | |   |  __ \                  | |  (_)           
     *     | |  __   ___ | |_  | |__) |_ __  __ _   ___ | |_  _   ___  ___ 
     *     | | |_ | / _ \| __| |  ___/| '__|/ _` | / __|| __|| | / __|/ _ \
     *     | |__| ||  __/| |_  | |    | |  | (_| || (__ | |_ | || (__|  __/
     *      \_____| \___| \__| |_|    |_|   \__,_| \___| \__||_| \___|\___|
     *                                                                     
     *                                                                     
     */
    
    public function get_practice(){
        $this->validate_get_practice();

        $layer = $this->input->post('layer');
        $course_id = $this->input->post('id');

        $subject_id =  ($this->input->post('subject_id'))?$this->input->post('subject_id'):'';

        if($layer == 1 ){
            $data['layer'] = $layer;
            $find = array('course_id'=>$course_id);
            $data['list'] = $this->exam_model->get_course_practice_subject($find);
            if(count($data['list']) > 1){
                $a  =  $data['list'];
                $temp = array();
                foreach($a as $out){
                    $out->total = array(array('count'=>$out->total,'text'=>'practice'));
                    $temp[] = $out;
                }
                $data['list'] = $temp;
            }elseif(count($data['list']) == 1){
                $layer = "2" ;
                $subject_id  = $data['list'][0]->id;
            }
        }

        if($layer == 2 ){
            $data['layer'] = $layer;
            $find = array('course_id'=>$course_id,'subject_id'=>$subject_id);
            $data['list'] = $this->exam_model->get_course_practice_subject_topic($find);
            if(count($data['list'])){
                $a  =  $data['list'];
                $temp = array();
                foreach($a as $out){
                    $out->total = array(array('count'=>$out->total,'text'=>'practice'));
                    $temp[] = $out;
                }
                $data['list'] = $temp;
            }
        }

        if($layer == 3 ){
            // get concept on the basis on topic 
            $data['layer'] = $layer;
            $find = array(
                        'course_id' =>$course_id,
                        'topic_id' =>$this->input->post('topic_id')
                        );
            $data['list']  = $this->exam_model->get_course_practice_list($find);
        }
        return_data(true, 'Test data', $data);
    }

    public function validate_get_practice(){
        post_check();
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required|integer');
        $this->form_validation->set_rules('id', 'id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('layer', 'layer', 'trim|required|is_natural_no_zero');
        $layer = $this->input->post('layer');

        if ($layer == 2) {
            $this->form_validation->set_rules('subject_id', 'subject_id', 'trim|required|is_natural_no_zero');
        }

        if ($layer == 3) {
            $this->form_validation->set_rules('topic_id', 'topic_id', 'trim|required|is_natural_no_zero');
        }

        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }   
    }


    public function video_feedback(){
        $this->validate_video_feedback();
        $array = array('user_id'=>$this->input->post('user_id'),
                        'video_id'=>$this->input->post('id'),
                        'point'=>$this->input->post('point'),
                        'text'=>$this->input->post('text')
        );
        // check if already given

        $this->exam_model->provide_feedback_for_video($array);
        return_data(true, 'Feedback Saved', array());
    }
    
    private function validate_video_feedback(){
        post_check();
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required|integer');
        $this->form_validation->set_rules('id', 'id', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('point', 'point', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('text', 'text', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }  
    }


    public function info(){
        // 13.126.67.127/index.php/data_model/courses/exam/info
        $html = <<<EOD
        1) get bsic data of course 
        api =  data_model/courses/exam/get_basic_data
        method post 
        keys are user_id , course_id 
        /* test */
        2) onclick test layer 1 info 
        api =  data_model/courses/exam/get_test_data
        method post 
        keys are user_id , course_id , layer 

        3) onclick test layer 2 info 
        api =  data_model/courses/exam/get_test_data
        method post 
        keys are user_id , course_id , layer ,  main_id 

        3) onclick test layer 3 info 
        api =  data_model/courses/exam/get_test_data
        method post 
        keys are user_id , course_id , layer , main_id , text_type      
        text_type

        /* concept */
        4) onclick concept layer 1 info
        api = data_model/courses/exam/get_concept 
        method post 
        keys are user_id , course_id , layer 

        5) onclick concept layer 2 info
        api = data_model/courses/exam/get_concept 
        method post 
        keys are user_id , course_id , layer    , subject_id 
        
        6) onclick concept layer 3 info
        api = data_model/courses/exam/get_concept 
        method post 
        keys are user_id , course_id , layer    , topic_id 
        
        /* video */
        7) onclick video layer 1 info
        api = data_model/courses/exam/get_video_data 
        method post 
        keys are user_id , course_id , layer 
        
        8) onclick video layer 2 info
        api = data_model/courses/exam/get_video_data 
        method post 
        keys are user_id , course_id , layer , subject_id

        9) onclick video layer 3 info
        api = data_model/courses/exam/get_video_data 
        method post 
        keys are user_id , course_id , layer , topic_id

        /* practice */
        10) onclick practice layer 1 info
        api = data_model/courses/exam/get_practice 
        method post 
        keys are user_id , course_id , layer 

        11) onclick practice layer 2 info
        api = data_model/courses/exam/get_practice 
        method post 
        keys are user_id , course_id , layer , subject_id 

EOD;

        echo $html;
    }

}
