<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_rating_reviews extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model('course_rating_reviews_model');
	}
    // Add 
    public function add_review_to_course(){
        $this->validate_add_review_to_course();
        /* check if already given */
        $check = $this->course_rating_reviews_model->if_review_already_given($this->input->post('course_id'),$this->input->post('user_id'));
        if(count($check)>0){
            return_data(false,"Rating already given.",$check);
        }
        $return = $this->course_rating_reviews_model->add_course_review($this->input->post());
        if($return){
            return_data(true,"Review added.",array());
        }
        activity_rewards($this->input->post('user_id'),'COURSE_REVIEW',"+");
        return_data(false,"Not able to add your review.",array());
    }

    private function validate_add_review_to_course(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('course_id','course_id', 'trim|required');
        $this->form_validation->set_rules('rating','rating', 'trim|required');
        $this->form_validation->set_rules('text','text', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}

    // edit comment 
     public function edit_review_to_course(){
        $this->validate_edit_review_to_course();
        $return = $this->course_rating_reviews_model->edit_course_review($this->input->post());
        if($return){
            return_data(true,"Review edited.",array());
        }
        return_data(false,"Not able to edit your review.",array());
    }

    private function validate_edit_review_to_course(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('course_id','course_id', 'trim|required');
        $this->form_validation->set_rules('rating','rating', 'trim|required');
        $this->form_validation->set_rules('text','text', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}
    // delete comment
     public function delete_review_from_course(){
        $this->validate_delete_review_from_course();
        $return = $this->course_rating_reviews_model->delete_course_review($this->input->post());
        if($return){
            return_data(true,"Review deleted.",array());
        }
        activity_rewards($this->input->post('user_id'),'COURSE_REVIEW',"-");
        return_data(false,"Not able to delete your review.",array());
    }

    private function validate_delete_review_from_course(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('course_id','course_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}

    public function get_list_of_reviews(){
        $this->validate_get_list_of_reviews();
        $data =  $this->course_rating_reviews_model->get_reviews_list($this->input->post());
        if($data){
            return_data(true,"Reviews Found.",$data);
        }
        return_data(false,"No Reviews Found.",array());
    }

    private function validate_get_list_of_reviews(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
        $this->form_validation->set_rules('course_id','course_id', 'trim|required');
        $this->form_validation->set_rules('last_review_id','last_review_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}

}    