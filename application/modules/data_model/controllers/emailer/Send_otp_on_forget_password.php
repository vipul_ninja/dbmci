<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Send_otp_on_forget_password extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function send_otp_on_email($data){
		$input =  array(
			"SENDER"=> "support@dbmci.com",
			"RECIPIENT"=> $data['email'] ,
			"SUBJECT"=> "Otp for change password DBMCI !" ,
			"HTMLBODY"=> $this->load->view('emailer/email_for_change_password', $data, True) ,
			"TEXTBODY"=> "Otp for change password MME !"
		 );
		$this->aws_emailer->send_aws_email($input);
	}
}
