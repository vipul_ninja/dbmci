<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function send_welcome_email($data){
		$input =  array(
			"SENDER"=> "support@dbmci.com",
			"RECIPIENT"=> $data['email'] ,
			"SUBJECT"=> "Welcome to DBMCI!" ,
			"HTMLBODY"=> $this->load->view('emailer/welcome_new_registration', $data, True) , 
			"TEXTBODY"=> "Welcome to eMedicoz!"
		 );
		$this->aws_emailer->send_aws_email($input); 
	}
}	