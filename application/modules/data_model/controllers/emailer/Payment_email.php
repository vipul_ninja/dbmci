<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_email extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function payment_done($record){
    $data = $this->db->select('name,email')->where('id',$record['user_id'])->get('users')->row_array();
		$data['pre_transaction_id'] =  $record['pre_transaction_id'];

		$input =  array(
			"SENDER"=> "support@dbmci.com",
			"RECIPIENT"=> $data['email'] ,
			"SUBJECT"=> "Transaction done DBMCI!" ,
			"HTMLBODY"=> $this->load->view('emailer/payment_done', $data, True) ,
			"TEXTBODY"=> "Payment done"
		 );
		$this->aws_emailer->send_aws_email($input);
    }
}
