<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_query_email extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function send_email_to_support($data=array()){

		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_port' => 587,
			'smtp_user' => 'support@dbmci.com',
			'smtp_pass' => '',
		);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($data['from'], $data['name']);
		$this->email->to("support@dbmci.com");
		$this->email->subject(CONFIG_PROJECT_NICK_NAME.' feedback !');
		$this->email->message($data['description']);
		$this->email->set_mailtype("html");
		$this->email->send();
	}


}