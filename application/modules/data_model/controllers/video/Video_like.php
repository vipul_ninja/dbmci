<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video_like extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("video_like_model");
	}

	public function like_video(){

		$this->validate_like_video();
		// first check if already liked 
		if($this->video_like_model->is_already_like_video($this->input->post())){
			return_data(false,'Video Already liked.');
		}

		$this->video_like_model->like_video($this->input->post());
		return_data(true,'Video liked.',array());
	}

	private function validate_like_video(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	public function dislike_video(){

		$this->validate_dislike_video();
		$this->video_like_model->dislike_video($this->input->post());
		$user_id = $this->input->post('user_id');
		$post_id = $this->input->post('video_id');
		return_data(true,'Video disliked.',array());
	}

	private function validate_dislike_video(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
    }
}