<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video_channel extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("video_model");
	}

	/* Get basic information on landing page */
	public function get_single_video_data(){
		$this->validate_get_single_video_data();
		$video =  array(
			"id"=>"1",
			"video_title"=>"#Radiology-Why do best medical graduates choose Radiology?",
			"video_type" =>"1",
			"url"=>"https://www.youtube.com/watch?v=Uek1gfyEgLQ",
			"author_name"=>"damsdelhi",
			"thumbnail_url"=>"https://i.ytimg.com/vi/Uek1gfyEgLQ/hqdefault.jpg",
			"video_desc"=>"The acquisition of medical images is usually carried out by the radiographer, often known as a Radiologic Technologist. Depending on location, the Diagnostic Radiologist, or Reporting Radiographer, then interprets or reads the images and produces a report of their findings and impression or diagnosis. This report is then transmitted to the Clinician who requested the imaging. The report can initially be made as a wet-read which is a rapid preliminary response to a clinical question, which will generally followed later by a final report.[1] Medical images are stored digitally in the picture archiving and communication system (PACS) where they can be viewed by all members of the healthcare team within the same health system and compared later on with future imaging exams.",
			"screen_tag"=>"#Radiology,#dams",
			"featured"=>"1",
			"is_new"=>"1",
			"allow_comments"=>"1",
			"creation_time"=>"1509524120922"
		);
		return_data(true,"Video found.",$video);
    }

	private function validate_get_single_video_data(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}

	/* get list of videos in perticular tag */
	public function get_videos_for_tag_list(){
		$this->validate_get_videos_for_tag_list();
		$user_id = $this->input->post('user_id');
		$sub_id = $this->input->post('sub_cat');
		$sort_by = $this->input->post('sort_by');
		$last_video_id = $this->input->post('last_video_id');
		$page_segment = $this->input->post('page_segment');
		$search_content = $this->input->post('search_content');
		/* get videos */
		$list =  $this->video_model->get_videos_for_cat($user_id,$sub_id,$sort_by,$last_video_id,$page_segment,$search_content);
		
		if(count($list)>0){
			return_data(true,"Video found.",$list);
		}
		return_data(false,"No Video found.",array());
	}

	private function validate_get_videos_for_tag_list(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('sub_cat','sub_cat', 'trim|required');
		$sort_defination = array('views','time','likes');
		if(!in_array($this->input->post('sort_by'),$sort_defination) ){
			$_POST['sort_by'] = "time";
		}
		
		if($this->input->post('last_video_id') == "" || !array_key_exists('last_video_id',$_POST)){
			$_POST['last_video_id'] = "";
		}
		if($this->input->post('page_segment') == "" || !array_key_exists('page_segment',$_POST)){
			$_POST['page_segment'] = 0;
		}
		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}
	/* increase video counter */
	public function add_video_counter(){
		$this->validate_add_video_counter();
		$user_id = $this->input->post('user_id');
		$video_id = $this->input->post('video_id');
		$this->video_model->add_view($user_id,$video_id);
		return_data(true,'View updated',array(),array());
	}

	private function validate_add_video_counter(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}	
}
