<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video_comment extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("video_comment_model");
	}
/***
 *                   _      _    _____                                           _   
 *         /\       | |    | |  / ____|                                         | |  
 *        /  \    __| |  __| | | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_ 
 *       / /\ \  / _` | / _` | | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
 *      / ____ \| (_| || (_| | | |____| (_) || | | | | || | | | | ||  __/| | | || |_ 
 *     /_/    \_\\__,_| \__,_|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
 *                                                                                   
 *                                                                                   
 */
	public function add_comment(){

		$this->validate_add_comment();
		$comment_data['user_id'] = $this->input->post('user_id');
		$comment_data['video_id'] = $this->input->post('video_id');
		$comment_data['comment'] = $this->input->post('comment');
		$comment_data['parent_id'] = $this->input->post('parent_id');
		/* save and get comment id */
		$comment_id = $this->video_comment_model->add_comment($comment_data);
		return_data(true,'Comment added.',array());
	}

	private function validate_add_comment(){
		
		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');
		$this->form_validation->set_rules('comment','comment', 'trim|required');
		/* parent comment id  if key not found */
		if(!array_key_exists('parent_id', $_POST) ){ $_POST['parent_id'] = 0 ; }

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
/***
 *      _    _             _         _           _____                                           _   
 *     | |  | |           | |       | |         / ____|                                         | |  
 *     | |  | | _ __    __| |  __ _ | |_  ___  | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_ 
 *     | |  | || '_ \  / _` | / _` || __|/ _ \ | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
 *     | |__| || |_) || (_| || (_| || |_|  __/ | |____| (_) || | | | | || | | | | ||  __/| | | || |_ 
 *      \____/ | .__/  \__,_| \__,_| \__|\___|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
 *             | |                                                                                   
 *             |_|                                                                                   
 */	
	public function update_comment(){

		$this->validate_update_comment();
		$comment_data['id'] = $this->input->post('id');
		$comment_data['user_id'] = $this->input->post('user_id');
		$comment_data['video_id'] = $this->input->post('video_id');
		$comment_data['comment'] = $this->input->post('comment');
		/* save and get comment id */
		$return = $this->video_comment_model->update_comment($comment_data);
		/* return the single comment */

		return_data(true,'Comment updated.',$return);
	}

	private function validate_update_comment(){
		
		post_check();
		
		$this->form_validation->set_rules('id','id', 'trim|required');
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');
		$this->form_validation->set_rules('comment','comment', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *      _____         _        _           _____                                           _   
 *     |  __ \       | |      | |         / ____|                                         | |  
 *     | |  | |  ___ | |  ___ | |_  ___  | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_ 
 *     | |  | | / _ \| | / _ \| __|/ _ \ | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
 *     | |__| ||  __/| ||  __/| |_|  __/ | |____| (_) || | | | | || | | | | ||  __/| | | || |_ 
 *     |_____/  \___||_| \___| \__|\___|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
 *                                                                                             
 *                                                                                             
 */		
	
	public function delete_comment(){

		$this->validate_delete_comment();
		$this->video_comment_model->delete_comment($this->input->post());

		return_data(true,'Comment deleted.');
	}

	private function validate_delete_comment(){
		
		post_check();
		
		$this->form_validation->set_rules('id','id', 'trim|required');
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *       _____                                           _     _       _       _   
 *      / ____|                                         | |   | |     (_)     | |  
 *     | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_  | |      _  ___ | |_ 
 *     | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __| | |     | |/ __|| __|
 *     | |____| (_) || | | | | || | | | | ||  __/| | | || |_  | |____ | |\__ \| |_ 
 *      \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__| |______||_||___/ \__|
 *                                                                                 
 *                                                                                 
 */		
	
	public function get_video_comment(){

		$this->validate_get_video_comment();

		if(!array_key_exists('last_comment_id', $_POST)){
			$last_comment_id = "";
		}else{
			$last_comment_id = $this->input->post('last_comment_id');
		}
		
		$info =  array(
			"video_id" =>$this->input->post('video_id'),
			'last_comment_id'=>$last_comment_id,
			'parent_id'=>$this->input->post('parent_id')
		);

		$comments = $this->video_comment_model->get_post_comment($info);
		
		if(count($comments) > 0 ){
			return_data(true,'Comment details.',$comments);	
		}else{
			return_data(false,'No comment found.');
		}
		
	}

	private function validate_get_video_comment(){
		
		post_check();
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');
		/* parent comment id  if key not found */
		if(!array_key_exists('parent_id', $_POST) ){ $_POST['parent_id'] = 0 ; }

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *       _____  _                _          _____                                           _   
 *      / ____|(_)              | |        / ____|                                         | |  
 *     | (___   _  _ __    __ _ | |  ___  | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_ 
 *      \___ \ | || '_ \  / _` || | / _ \ | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
 *      ____) || || | | || (_| || ||  __/ | |____| (_) || | | | | || | | | | ||  __/| | | || |_ 
 *     |_____/ |_||_| |_| \__, ||_| \___|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
 *                         __/ |                                                                
 *                        |___/                                                                 
 */     

    public function get_single_comment_data(){
    	$this->validate_get_single_comment_data();

    	$comment = $this->video_comment_model->get_single_comment_data($this->input->post('comment_id'));

    	if(count($comment) > 0 ){
			return_data(true,'Comment details.',$comment);	
		}else{
			return_data(false,'No comment found.');
		}

    }

    private function validate_get_single_comment_data(){
    	post_check();
    	$this->form_validation->set_rules('comment_id','comment_id', 'trim|required');		
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
    }



}