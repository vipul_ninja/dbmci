<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video_channel extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->model("video_model");
	}
/***
 *       _____  _                _        __      __ _      _              
 *      / ____|(_)              | |       \ \    / /(_)    | |             
 *     | (___   _  _ __    __ _ | |  ___   \ \  / /  _   __| |  ___   ___  
 *      \___ \ | || '_ \  / _` || | / _ \   \ \/ /  | | / _` | / _ \ / _ \ 
 *      ____) || || | | || (_| || ||  __/    \  /   | || (_| ||  __/| (_) |
 *     |_____/ |_||_| |_| \__, ||_| \___|     \/    |_| \__,_| \___| \___/ 
 *                         __/ |                                           
 *                        |___/                                            
 */
	/* Get basic information on landing page */
	public function get_single_video_data(){
		$this->validate_get_single_video_data();
		$user_id = $this->input->post('user_id');
		$video_id = $this->input->post('video_id');
		$sql =  "SELECT vm.* , 
				IFNULL((SELECT GROUP_CONCAT(tag_name) from video_search_tag_list where find_in_set (id , (SELECT screen_tag FROM video_master WHERE id = vm.id))),'') as screen_tag ,
				(select count(id) from video_like_meta as vlm where vlm.user_id = $user_id and vlm.video_id = vm.id limit 0,1) as is_like,
				(select count(id) from video_view_meta as vvm where vvm.user_id = $user_id and vvm.video_id = vm.id limit 0,1) as is_viewed  
				from video_master as vm where vm.id =  $video_id ";
		if($data = $this->db->query($sql)->row_array()){
			return_data(true,"Video found.",$data);
		}
		return_data(false,"No video found.",array());
    }

	private function validate_get_single_video_data(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}
/***
 *       _____                          _      _               
 *      / ____|                        | |    (_)              
 *     | (___    ___   __ _  _ __  ___ | |__   _  _ __    __ _ 
 *      \___ \  / _ \ / _` || '__|/ __|| '_ \ | || '_ \  / _` |
 *      ____) ||  __/| (_| || |  | (__ | | | || || | | || (_| |
 *     |_____/  \___| \__,_||_|   \___||_| |_||_||_| |_| \__, |
 *                                                        __/ |
 *                                                       |___/ 
 */

	public function get_videos_for_tag_list(){
		$this->validate_get_videos_for_tag_list();
		$user_id = $this->input->post('user_id');
		$sub_id = $this->input->post('sub_cat');
		$sort_by = $this->input->post('sort_by');
		$last_video_id = $this->input->post('last_video_id');
		$page_segment = $this->input->post('page_segment');
		$search_content = $this->input->post('search_content');
		$related_video_id = $this->input->post('related_video_id');
		/* get videos */
		$list =  $this->video_model->get_videos_for_cat($user_id,$sub_id,$sort_by,$last_video_id,$page_segment,$search_content,$related_video_id);
		if(count($list)>0){
			return_data(true,"Video found.",$list);
		}
		return_data(false,"No Video found.",array());
	}

	private function validate_get_videos_for_tag_list(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		//$this->form_validation->set_rules('sub_cat','sub_cat', 'trim|required');
		$sort_defination = array('views','time','likes');
		if(!in_array($this->input->post('sort_by'),$sort_defination) ){
			$_POST['sort_by'] = "time";
		}
		
		if($this->input->post('last_video_id') == "" || !array_key_exists('last_video_id',$_POST)){
			$_POST['last_video_id'] = "";
		}
		if($this->input->post('page_segment') == "" || !array_key_exists('page_segment',$_POST)){
			$_POST['page_segment'] = 0;
		}

		if($this->input->post('search_content') == "" || !array_key_exists('search_content',$_POST)){
			$_POST['search_content'] = "";
		}		

		if($this->input->post('related_video_id') == "" || !array_key_exists('related_video_id',$_POST)){
			$_POST['related_video_id'] = "";
		}		

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}
	
/***
 *                   _      _  __      __ _                 
 *         /\       | |    | | \ \    / /(_)                
 *        /  \    __| |  __| |  \ \  / /  _   ___ __      __
 *       / /\ \  / _` | / _` |   \ \/ /  | | / _ \\ \ /\ / /
 *      / ____ \| (_| || (_| |    \  /   | ||  __/ \ V  V / 
 *     /_/    \_\\__,_| \__,_|     \/    |_| \___|  \_/\_/  
 *                                                          
 *                                                          
 */
	public function add_video_counter(){
		$this->validate_add_video_counter();
		$user_id = $this->input->post('user_id');
		$video_id = $this->input->post('video_id');
		$this->video_model->add_view($user_id,$video_id);
		return_data(true,'View updated',array(),array());
	}

	private function validate_add_video_counter(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}
	

/***
 *                   _      _    __               
 *         /\       | |    | |  / _|              
 *        /  \    __| |  __| | | |_  __ _ __   __ 
 *       / /\ \  / _` | / _` | |  _|/ _` |\ \ / / 
 *      / ____ \| (_| || (_| | | | | (_| | \ V /_ 
 *     /_/    \_\\__,_| \__,_| |_|  \__,_|  \_/(_)
 *                                                
 *                                                
 */

	public function add_favourite_video(){
		$this->validate_add_favourite_video();
		$user_id = $this->input->post('user_id');
		$video_id = $this->input->post('video_id');
		$this->video_model->add_favourite_video($user_id,$video_id);
		if($this->db->insert_id()>0){
			return_data(true,'Video Added to favourite list.');
		}else{
			return_data(false,'Video already Added to favourite list.');
		}
		
				
	}
		
	private function validate_add_favourite_video(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

/***
 *      _____                                       __               
 *     |  __ \                                     / _|              
 *     | |__) | ___  _ __ ___    ___ __   __ ___  | |_  __ _ __   __ 
 *     |  _  / / _ \| '_ ` _ \  / _ \\ \ / // _ \ |  _|/ _` |\ \ / / 
 *     | | \ \|  __/| | | | | || (_) |\ V /|  __/ | | | (_| | \ V /_ 
 *     |_|  \_\\___||_| |_| |_| \___/  \_/  \___| |_|  \__,_|  \_/(_)
 *                                                                   
 *                                                                   
 */

	public function remove_favourite_video(){
		$this->validate_add_favourite_video();
		$user_id = $this->input->post('user_id');
		$video_id = $this->input->post('video_id');
		$this->video_model->remove_favourite_video($user_id,$video_id);
		return_data(true,'Video removed from favourite list.');
	}
		
	private function validate_remove_favourite_video(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('video_id','video_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if($error){
		return_data(false,array_values($error)[0],array(),$error);
		}
	}



	/*--favourite_list-*/ 
		
	public function get_favourite_video_list(){
		$this->validate_get_favourite_video_list();
		$user_id = $this->input->post('user_id');
		$sort_by = $this->input->post('sort_by');
		$last_video_id = $this->input->post('last_video_id');
		$page_segment = $this->input->post('page_segment');
		$search_content = $this->input->post('search_content');
		/* get videos */
		$list =  $this->video_model->favourite_video_list($user_id,$sort_by,$last_video_id,$page_segment,$search_content);
		//echo $this->db->last_query();
		if(count($list)>0){
			return_data(true,"Video found.",$list);
		}
		return_data(false,"No Video found.",array());
	}

	private function validate_get_favourite_video_list(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}	
	}
}
