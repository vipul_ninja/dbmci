<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_post_for_expert extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper("push_helper");
	}
	
	/*****************************   PUSH CODE FROM 501 ******************************/

	public function push_for_expert($data){

		$user_id = $data['user_id'];
		$post_id = $data['post_id'];

		$query = "SELECT id FROM users WHERE FIND_IN_SET('".$data['post_tag']."', expert_tag_id) and is_expert =1 ";
        $ex_user  = $this->db->query($query)->result_array();

		if(count($ex_user) > 0 ){
			foreach($ex_user as $ex){

				$reciver_id = $ex['id'];

				$this->db->where('id',$user_id);
				$user_who_tagged = $this->db->get("users")->row_array();

				$this->db->where('id',$reciver_id);
				$user_who_got_tagged = $this->db->get("users")->row_array();


				$push_data = json_encode(
					array(
						'notification_code' => 901,
						'message' => $user_who_tagged['name'] ." added a new post.",
						'data' => array('post_id' => $post_id)
					)
				);
				//$permi = $this->db->query("select * from user_permission where user_id = '".$reciver_id."'")->row_array();

				if($user_id != $reciver_id  /*&&  $permi['tag_notification'] == 1*/){

					if($user_who_got_tagged['device_type'] == 1){
						/* android */
						$token = $user_who_got_tagged['device_tokken'];
						$device = "android";
						$result = generatePush($device, $token, $push_data);
						//logger
						//log_message('error', "android push notification expert push . token is $token and data is ".json_encode($push_data).$result);
					}
					if($user_who_got_tagged['device_type'] == 2){
						/* ios */
						$token = $user_who_got_tagged['device_tokken'];
						$device = "ios";
						generatePush($device, $token, $push_data);
						//logger
						//log_message('error', "ios push notification expert push. token is $token and data is ".json_encode($push_data));
					}
				}				
			}
		}

	}

}
