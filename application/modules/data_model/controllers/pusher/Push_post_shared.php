<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_post_shared extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper("push_helper");
	}
	/*****************************   PUSH CODE FROM 301 ******************************/

	public function push_on_share($data = array()){
		$post_shared_by  = $data['user_id'];
		$post_id = $data['post_id'];

		$query ="select * from post_counter where id = '$post_id'";
		$query = $this->db->query($query);
		$data =  $query->row_array();

		$this->db->where('id',$post_shared_by);
		$user_who_share = $this->db->get("users")->row_array();

		$this->db->where('id',$data['user_id']);
		$user_whom_post_shared = $this->db->get("users")->row_array();


		$push_data = json_encode(
			array(
				'notification_code' => 301,
				'message' => $user_who_share['name'] ." shared on your post on fanwall.",
				'data' => array('post_id' => $post_id)
			)
		);
		//other_notification
		$permi = $this->db->query("select * from user_permission where user_id = '".$data['user_id']."'")->row_array();

		if($post_shared_by != $user_whom_post_shared['id'] &&  $permi['other_notification'] == 1 ){
			if($user_whom_post_shared['device_type'] == 1){
				/* android */
				$token = $user_whom_post_shared['device_tokken'];
				$device = "android";
				generatePush($device, $token, $push_data);
			}
			if($user_whom_post_shared['device_type'] == 2){
				/* ios */
				$token = $user_whom_post_shared['device_tokken'];
				$device = "ios";
				generatePush($device, $token, $push_data);
			}

		}

	}
}
