<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_post_liked extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper("push_helper");
	}
	/*****************************   PUSH CODE FROM 101 ******************************/

	public function push_on_like($data){
		$user_id = $data['user_id'];
		$post_id = $data['post_id'];

		$query ="SELECT upl.user_id AS user_who_like, pc.user_id AS user_whom_post_liked
					FROM user_post_like AS upl
					JOIN post_counter AS pc ON pc.id = upl.post_id
					WHERE upl.post_id = '$post_id'
					AND upl.user_id = '$user_id'";
		$query = $this->db->query($query);
		$data =  $query->row_array();

		$this->db->where('id',$data['user_who_like']);
		$user_who_like = $this->db->get("users")->row_array();

		$this->db->where('id',$data['user_whom_post_liked']);
		$user_whom_post_liked = $this->db->get("users")->row_array();


		$push_data = json_encode(
			array(
				'notification_code' => 101,
				'message' => $user_who_like['name'] ." liked your post.",
				'data' => array('post_id' => $post_id)
			)
		);
		
		$permi = $this->db->query("select * from user_permission where user_id = '".$data['user_whom_post_liked']."'")->row_array();

		if($data['user_who_like'] != $data['user_whom_post_liked'] &&  $permi['post_like_notification'] == 1  ){

			if($user_whom_post_liked['device_type'] == 1){
				/* android */
				$token = $user_whom_post_liked['device_tokken'];
				$device = "android";
				$result = generatePush($device, $token, $push_data);
				//logger
				log_message('error', "android push notification . token is $token and data is ".json_encode($push_data).$result);
			}
			if($user_whom_post_liked['device_type'] == 2){
				/* ios */
				$token = $user_whom_post_liked['device_tokken'];
				$device = "ios";
				generatePush($device, $token, $push_data);
				//logger
				log_message('error', "ios push notification . token is $token and data is ".json_encode($push_data));
			}
		}

	}

	public function ios($tokken=""){
		$push_data = json_encode(
			array(
				'notification_code' => 101,
				'message' => "testing of ios push",
				'data' => array('post_id' => 1)
			)
		);
		$token = $tokken;
		$device = "ios";
		generatePush($device, $token, $push_data);

	}

}
