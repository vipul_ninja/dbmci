<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_post_comment extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper("push_helper");
	}
	/*****************************   PUSH CODE FROM 201 ******************************/

	public function push_on_comment($data = array()){
		$user_id = $data['user_id'];
		$post_id = $data['post_id'];

		$query ="SELECT upc.user_id AS user_who_comment, pc.user_id AS user_whom_post_commented
					FROM user_post_comment AS upc
					JOIN post_counter AS pc ON pc.id = upc.post_id
					WHERE upc.post_id = '$post_id'
					AND upc.user_id = '$user_id'";
		$query = $this->db->query($query);
		$data =  $query->row_array();

		$this->db->where('id',$data['user_who_comment']);
		$user_who_comment = $this->db->get("users")->row_array();

		$this->db->where('id',$data['user_whom_post_commented']);
		$user_whom_post_commented = $this->db->get("users")->row_array();


		$push_data = json_encode(
			array(
				'notification_code' => 201,
				'message' => $user_who_comment['name'] ." commented on your post.",
				'data' => array('post_id' => $post_id)
			)
		);
		$permi = $this->db->query("select * from user_permission where user_id = '".$data['user_whom_post_commented']."'")->row_array();

		if($data['user_who_comment'] != $data['user_whom_post_commented'] &&  $permi['comment_on_post_notification'] == 1 ){
			if($user_whom_post_commented['device_type'] == 1){
				/* android */
				$token = $user_whom_post_commented['device_tokken'];
				$device = "android";
				generatePush($device, $token, $push_data);
			}
			if($user_whom_post_commented['device_type'] == 2){
				/* ios */
				$token = $user_whom_post_commented['device_tokken'];
				$device = "ios";
				generatePush($device, $token, $push_data);
			}

		}
	}
}
