<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_following extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper("push_helper");
	}
	/*****************************   PUSH CODE FROM 701 ******************************/

	public function push_on_following($data = array()){
		$user_id = $data['user_id']; // done by
		$follower_id = $data['follower_id']; // done for

		$query ="SELECT u.id ,u.device_type , u.device_tokken , fu.name as follower_name
                    FROM users as u
                    join users as fu on fu.id = $follower_id
                    WHERE u.id = $user_id";
		$query = $this->db->query($query);
		$data =  $query->row_array();

		$push_data = json_encode(
			array(
				'notification_code' => 701,
				'message' => $data['follower_name'] ." started following you.",
				'data' => array('user_id' =>$follower_id)
			)
		);
		$permi = $this->db->query("select * from user_permission where user_id = $user_id")->row_array();
		if($user_id != $follower_id &&  $permi['follow_notification'] == 1 ){
			if($data['device_type'] == 1){
				/* android */
				$token = $data['device_tokken'];
				$device = "android";
				generatePush($device, $token, $push_data);
			}
			if($data['device_type'] == 2){
				/* ios */
				$token = $data['device_tokken'];
				$device = "ios";
				generatePush($device, $token, $push_data);
			}
		}
	}
}
