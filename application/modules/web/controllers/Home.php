<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct(){
        parent::__construct();
		$this->load->helper(array('url', 'custom', 'custom_helper','message_sender'));
        $this->load->library('form_validation');
    }

	public function index(){
		$url = site_url()."/data_model/user/registration/get_all_category_db";
        $document = array('file_url' => $url);
        $data['category_details'] = file_curl_contents($document);
        
        $url = site_url()."/data_model/Testimonial/get_testimonial";
        $document = array('file_url' => $url);
        $data['testimonial'] = file_curl_contents($document);
      
		$this->load->view('index', $data);
	}


    function send_sms(){

        $number = $this->input->post('number');
        $message = "Please check out our exciting new apps on Google play store https://www.google.com and on iTunes store https://www.facebook.com";
        $res =send_message_global('+91',$number,$message);
            // print_r($res);die;
        echo "success";
         

    }
	function g_login()
    {   
        include_once APPPATH . "libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH . "libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
    
        //create google login and registration authentication url..
        $clientId = '474830604768-7q864jnrfq1le81lfke1eoo66g1uefkv.apps.googleusercontent.com';
        $clientSecret = 'EEWRosjc0oIf3ozdSJpYJFp4';
        $redirectUrl = site_url()."/web/home/g_login";
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to http://52.66.205.74/index.php');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_GET['code'])) {
            $gClient->authenticate();
            $this->session->set_userdata('token', $gClient->getAccessToken());
            redirect($redirectUrl);
        }

        $token = $this->session->userdata('token');
        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }
        
        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();
			
            // Preparing data for database insertion
            // $userData['oauth_provider'] = 'google';
            // $userData['oauth_uid'] = $userProfile['id'];
            // $userData['name'] = $userProfile['given_name'];
            // $userData['email'] = $userProfile['email'];
            // $userData['login_type'] = '2';
            // $userData['g_id'] = $userProfile['id'];
            // $userData['user_type'] = '';
            // $userData['locale'] = $userProfile['locale'];
            // $userData['profile_image'] = $userProfile['picture'];
            // $userData['create_date'] = date("Y-m-d h:i:s");
            $g_data = array(
                'file_url' => site_url() . "/data_model/user/registration/login_authentication", 
                'email' => $userProfile['email'],
                'is_social' => 1,
                'device_type' => 0,
                'social_type' => 2,
                'social_tokken' =>  $userProfile['id'],
                'name' => $userProfile['given_name'],
                'email' => $userProfile['email'],
                'profile_picture' => $userProfile['picture'],
            );
            $res = file_curl_contents($g_data);
            if($res['status']){
                $ses_data = array(
                        'username' => $res['data']['email'],
                        'name' => $res['data']['name'],
                        'id' => $res['data']['id'],
                        'pro_img' => $res['data']['profile_picture']
                    );
                $this->session->set_userdata($ses_data);
                // redirect('Feeds');
                // $this->load->view('start');
               
                    
            }else{
                $this->session->set_userdata('mobModel','user not registered');
                redirect('Home');
            }

           
            
        } 
                      
    }
    function g_signUp()
    {
        include_once APPPATH . "libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH . "libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
    
        //create google login and registration authentication url..
        $clientId = '474830604768-7q864jnrfq1le81lfke1eoo66g1uefkv.apps.googleusercontent.com';
        $clientSecret = 'EEWRosjc0oIf3ozdSJpYJFp4';
        $redirectUrl = site_url()."web/home/g_login";
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to http://52.66.205.74/index.php');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl); 
        $google_oauthV2 = new Google_Oauth2Service($gClient);

        // if (isset($_GET['code'])) {
        //     $gClient->authenticate();
        //     $this->session->set_userdata('token', $gClient->getAccessToken());
        //     redirect($redirectUrl);
        // }

        $token = $this->session->userdata('token');
        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }
        
        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();

        $g_data = array(
            'file_url' => site_url() . "/data_model/user/registration", 
            'mobile' => $this->input->post('mob_g'),
            'email' => $userProfile['email'],
            'is_social' => 1,
            'device_type' => 0,
            'social_type' => 2,
            'social_tokken' =>  $userProfile['id'],
            'name' => $userProfile['given_name'],
            'profile_picture' => $userProfile['picture'],
        );
       
        $res = file_curl_contents($g_data);
        if($res['status']){
            $ses_data = array(
                'username' => $res['data']['email'],
                'name' => $res['data']['name'],
                'id' => $res['data']['id'],
                'pro_img' => $res['data']['profile_picture']
            );
            $this->session->set_userdata($ses_data);
            echo "success";
        }else{
            echo $res['message']; 
        }
     }

    }

    function like()
    {
        $like_data = array(
            'file_url' => site_url() . "/data_model/user/post_like/like_post",
            'post_id' => $this->input->post('post_id'),
            'user_id' => $this->session->id
        );
        $res = file_curl_contents($like_data);
        echo json_encode($res);
    

    }
    function dislike()
    {
        $dislike_data = array(
            'file_url' => site_url() . "/data_model/user/post_like/dislike_post",
            'post_id' => $this->input->post('post_id'),
            'user_id' => $this->session->id
        );
        $res = file_curl_contents($dislike_data);
        echo json_encode($res);
    }

}
