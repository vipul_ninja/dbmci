<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DailyDose extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url', 'custom', 'custom_helper'));
       $this->load->library('session');
         $this->CI = & get_instance();
    }
    function index(){

//        $url = START_URL . "//data_model/fanwall/master_hit/content";
        $url = site_url()."/data_model/user/registration/get_all_category_db";
        $document = array('file_url' => $url);
        $data['category_details'] = file_curl_contents($document);
        $data['active'] = 'DailyDose';
        $this->load->view('dailydose/daily_dose', $data);
       
    }
    
    public function header_notify(){
        $url3 = site_url() . "/data_model/notification_genrator/activity_logger/all_notification_counter";
        $document3 = array('file_url' => $url3, 'user_id' => 53);
        $notifyCount = file_curl_contents($document3);
        $count = $notifyCount['data']['counter'];
        echo $count;
    }

//=============================List of quiz===================================================    
    public function dailyquiz(){
         $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_quiz";
        $document = array('file_url' => $url, 'user_id' => 53);
        $data = file_curl_contents($document);
        
      $data['d1']=$data['data'];
        $this->load->view('dailydose/daily_quiz',$data);

    }
   //==========================================================================================
//=============================List of quiz===================================================    
    public function affair(){
         $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_current_affair";
        $document = array('file_url' => $url, 'user_id' => 53);
        $data = file_curl_contents($document);
         
      $data['d1']=$data['data'];
    // echo "<pre>";print_r($data);die;
        $this->load->view('dailydose/dailydose_current_affair',$data);

    }
   //=============================================================================================
    //============================= Bookmarked ===================================================    
    public function bookmark_bookmarked(){
        $post_id = $this->input->post('post_id');
        $url = site_url() . "/data_model/user/post_bookmarks/add_to_bookmarks";
        $document = array('file_url' => $url, 'user_id' => 53,'post_id'=>$post_id);
        $data = file_curl_contents($document);
        echo "<pre>";print_r($data);die;
        $data['d1']=$data['data'];
        // echo "<pre>";print_r($data);die;
        $this->load->view('dailydose/dailydose_current_affair',$data);

    }
   //==========================================================================================
//=============================Quiz By Id===========================================
    public function quiz_by_id($quiz_id=0,$view_question=0){
       
        $test_id = $this->input->post('id');
        
         if($test_id==0)
         {
             $test_id=$quiz_id;
            
         }
    $url =site_url() . "/data_model/courses/test_series/get_test_series_with_id";
        $document = array('file_url' => $url, 'user_id' => 53,'test_series_id'=>$test_id);
        $data = file_curl_contents($document);
        
        if($view_question==0){
        	echo json_encode($data);
        die;
        
        }
        else if($view_question==1 && !empty($test_id))
        {
           
             $data['test_data'] =$data['data'];
            
             $this->load->view('dailydose/quiz',$data);
             // $this->load->view('dailydose/engineering',$data);
        }
    }
//=============================================================================================

    //=============================Quiz By Id===========================================
    public function paragraph_quiz_by_id(){
    
        // $url =site_url() . "/data_model/courses/test_series/get_test_series_with_id";
        // $document = array('file_url' => $url, 'user_id' => 53,'test_series_id'=>12);
        // $data = file_curl_contents($document);   
        // $data['test_data'] =$data['data'];
        // $this->load->view('dailydose/quiz',$data);
        
        ////======================only test==============================
        
        $url =site_url() . "/data_model/courses/test_series/get_test_series_with_id";
        $document = array('file_url' => $url, 'user_id' => 53,'test_series_id'=>11);
        $data = file_curl_contents($document);
        $datas = array($data, true);
        $status = $datas[0]['status'];
        $data['detail'] = $datas[0]['data'];
        $this->load->view('dailydose/test_series',$data);
      // $this->load->view('dailydose/translate');
        
    }
//=============================================================================================
    public function ajax_attempt_result(){
        
      $test_series_id =  $this->input->post('test_segment_id');
       
        $time_spent =$this->input->post('time_spent');
         $question_dump=$this->input->post('question_dump');
//         echo "<pre>";    print_r(json_encode($question_dump));die;
       $url =site_url() . "/data_model/courses/Test_series/save_test";
        $document = array('file_url' => $url, 'user_id' => 53,'test_series_id'=>$test_series_id,'time_spent'=>$time_spent,'question_dump'=>json_encode($question_dump));
        $res = file_curl_contents($document);
        $status=$res['status'];
        if($status==1)
        {
           
         $result= "<td>".$res['data']['test_series_name']."</td>
                     <td>".$res['data']['correct_count']."</td>
                     <td>".$res['data']['marks']."</td>
                     <td>".$res['data']['time_spent']."</td>
                     <td>".$res['data']['user_rank']."</td>";

            $result_id =$res['data']['id'];
            $final_array=json_encode(array('status'=>true,'result'=>$result,'u_id'=>$result_id));
                echo $final_array;
        }    
    }


//=============================LEADER BOARD===================================================    
    public function basic_rank_result($id){
         $url = site_url() . "/data_model/courses/Test_series/get_test_series_basic_result";
         $test_segment_id =$id;//it is comming form append url from ajax
         
        $document = array('file_url' => $url, 'user_id' => 53,'test_segment_id'=>$test_segment_id);
        $data = file_curl_contents($document);
        $data['d1']=$data['data'];
        $this->load->view('dailydose/leaderboard',$data);

    }
   //==========================================================================================

    //=============================VIDEO===================================================    
   
    public function video($data=0){
        $url = site_url() . "/data_model/video/Video_channel/get_videos_for_tag_list";
        $url2=site_url() . "/data_model/fanwall/master_hit/content";
        $url1 = site_url() . "/data_model/slider/get_slider";
        $sub_cat=0;
        $is_favourite='0';
        $last_video_id='';
        $document = array('file_url' => $url, 'user_id' => 53,'sub_cat'=>$sub_cat,'last_video_id'=>$last_video_id
                ,'sort_by'=>'');
        $data = file_curl_contents($document);
        $document1 = array('file_url' => $url2, 'user_id' => 53);
        $data2 = file_curl_contents($document1);
        $document2 = array('file_url' => $url1, 'user_id' => 53, 'slider_id'=>1);
        $data['slider_images'] = file_curl_contents($document2);
        $data['d3']=$data2['data'];
        $data['videos']=$data;
 // echo "<pre>";print_r($data);die;
        $this->load->view('dailydose/dailydose_video',$data);
        

    }
   //==========================================================================================

    //=============================VIDEO Lists===================================================    
   
    public function video_lists($sub_cat,$name){

      $last_video_id = '';
        $url = site_url() . "/data_model/video/Video_channel/get_videos_for_tag_list";
        $document = array('file_url' => $url, 'user_id' => 53,'sub_cat'=>$sub_cat,'last_video_id'=>$last_video_id
                ,'sort_by'=>'');
        $data['d3'] = file_curl_contents($document);
        $data['title']=$name;
        
        $this->load->view('dailydose/video_lists',$data);
        

    }
   //==========================================================================================
    //============================= Get latest, trending and favourite ====================================    
    public function get_video_ajax(){
        $sub_cat=0;
        $last_video_id='';
        $videoValue = $this->input->post('videoValue');
        if($videoValue == 1){//latest
            $sort = '';
            $favourite = ''; 
        }
        if($videoValue == 2){//trending
            $sort = 'views';
            $favourite = '';         
        }
        if($videoValue == 3){//favourite
            $sort = '';
            $favourite = 'favourite';   
        }
        $url = site_url() . "/data_model/video/Video_channel/get_videos_for_tag_list";
        $document = array('file_url' => $url, 'user_id' => 53,'sub_cat'=>$sub_cat,'last_video_id'=>$last_video_id
                ,'sort_by'=>$sort,'favourite'=>$favourite);
        $data['videos'] = file_curl_contents($document); 
        $data['videoValue'] = $videoValue;
        // echo "<pre>";print_r($data);die;
        $this->load->view('dailydose/get_videos_ajax',$data);
    }
   //==========================================================================================
    //===============================single video=================================================
        public function single_video($id){
          $selected_video_url = site_url() . "/data_model/video/Video_channel/get_single_video_data";
        $suggested_video_url=site_url() . "/data_model/fanwall/master_hit/content";

         $video_id = $id;
        $document = array('file_url' => $selected_video_url, 'user_id' => 53,'video_id'=>$video_id);
        $data = file_curl_contents($document);
        $data['d1']=$data;

        //suggested video
        $document1 = array('file_url' => $suggested_video_url, 'user_id' => 53);
        $suggested_video_data = file_curl_contents($document1);
        $data['d3']=$suggested_video_data;
        // echo "<pre>";
        // print_r($data);die;
        $this->load->view('dailydose/dailydose_single_video',$data);

    }
     //==============================================================================================   
     //=============================VOCABULARY===================================================    
    public function vocab(){
         $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_vocab";
                         
        $document = array('file_url' => $url, 'user_id' => 53);
        $data = file_curl_contents($document);
        $data['d1']=$data['data'];
        $this->load->view('dailydose/dailydose_vocab',$data);

    }
   //==========================================================================================

//=============================REVIEW===================================================    
    public function review(){
         $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_review";
                         
        $document = array('file_url' => $url, 'user_id' => 53, 'subject_id'=>'');
        $data = file_curl_contents($document);
        $data['d1']=$data['data'];
      // echo "<pre>";print_r($data);die;
        $this->load->view('dailydose/dailydose_review',$data);
       // $this->load->view('current_affair_final');

    }
   //==========================================================================================
     //=============================ARTICLE===================================================    
    public function article(){
         $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_article";
        $document = array('file_url' => $url, 'user_id' => 53);
        $data = file_curl_contents($document);
        $data['d1']=$data['data'];
        $this->load->view('dailydose/dailydose_article',$data);

    }
   //==========================================================================================
    // ================================Read More=================================================
    function read_more($id,$type){

        if($type==1){
        $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_current_affair";
        $result['title'] = 'Current Affairs & Banking';
        $result['page'] = 'affair';
        }else if($type==2){
        $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_article";
        $result['title'] = 'Articles';
        $result['page'] = 'article';
        }else if($type==3){
        $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_vocab";
        $result['title'] = 'Vocab Dose';
        $result['page'] = 'vocab';
        }
        // else if($type==4){
        // $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_review";
        // $result['title'] = 'Review of the Day';
        // $result['page'] = 'review';
        // }
       
        $document = array('file_url' => $url, 'user_id' => 53);
        $data = file_curl_contents($document);
        foreach ( $data['data'] as $d3) {
         	 if($d3['id']==$id){
         	 		$result['d1'] =$d3;  
            }
        }
        $this->load->view('common_read_more',$result);
  }


  // ===========================Pagination==========================================================


  public function ajax_post() {
      
        $l_p_i = $this->input->post('last_post_id');
        $type=$this->input->post('type');
        if($type=="1"){
        $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_current_affair"; 
      }else if($type=="2"){
          $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_article";
      }else if($type=="3"){
          $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_vocab";
      }else{
          $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?post_searching=user_post_type_review";
      }
   
        $document1 = array('file_url' => $url1, 'user_id' => 53, 'last_post_id' => $l_p_i);
        $post_list = file_curl_contents($document1);
     
       if($post_list['status']==true)
       { 
            $post_list=$post_list['data'];
            $data['d2'] = $post_list; 
              // pre($data);die;
            $this->load->view('dailydose/ajax_bottom_loader', $data);
       }else{
        $this->load->view('dailydose/ajax_bottom_loader');
       }
       
    }
 // ========================END Pagination========================================================== 

// ===========================Ajax Subject Wise Series==============================================
  public function ajax_subject_wise_series() {
    $url =site_url() . "/data_model/courses/test_series/get_test_series_with_id";
    $document = array('file_url' => $url, 'user_id' => 53,'test_series_id'=>43);
    $data = file_curl_contents($document);
    $data['test_data'] =$data['data'];
    $data['main'] = $this->input->post('main');
    $this->load->view('dailydose/ajax_test_tab_click',$data); 
    }
// ========================END Ajax Subject Wise Series=================================================


   public function remote_file_exists( $source, $extra_mile = 0 ) { 
      # EXTRA MILE = 0 
      ////////////////////////////////// Does it exist? 
      # EXTRA MILE = 1 
      ////////////////////////////////// Is it an image? 
      if ( $extra_mile === 1 ) { 
        $img = @getimagesize($source); 
        if ( !$img ) return 0; 
        else{ 
          switch ($img[2]){ 
            case 0: 
            return 0;     
            break; 
            case 1: 
            return $source; 
            break; 
            case 2: 
            return $source; 
            break; 
            case 3: 
            return $source; 
            break; 
            case 6: 
            return $source; 
            break; 
            default: 
            return 0;         
            break; 
          } 
        } 
      }else { 
        if (@FClose(@FOpen($source, 'r')))  
          return 1; 
        else 
          return 0; 
        } 
    } 

     
      public function check_time_ago_in_php($timestamp){
  
      date_default_timezone_set("Asia/Kolkata");         
      $time_ago = round($timestamp/1000);  
      $current_time  = time();
      $time_difference = $current_time - $time_ago;
      $seconds  = $time_difference;
      $minutes = round($seconds / 60); // value 60 is seconds  
      $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
      $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;  
      $weeks   = round($seconds / 604800); // 7*24*60*60;  
      $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
      $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60
       if ($days > 7){
       	return 1;
       } else {
		return 0;
		}
    	}
     public function time_ago_in_php($timestamp){
  
      date_default_timezone_set("Asia/Kolkata");         
      $time_ago = round($timestamp/1000);  
      $current_time  = time();
      $time_difference = $current_time - $time_ago;
      $seconds  = $time_difference;
      
      $minutes = round($seconds / 60); // value 60 is seconds  
      $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
      $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;  
      $weeks   = round($seconds / 604800); // 7*24*60*60;  
      $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
      $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60
                    
      if ($seconds <= 60){

        return "Just Now";

      } else if ($minutes <= 60){

        if ($minutes == 1){

          return "one minute ago";

        } else {

          return "$minutes minutes ago";

        }

      } else if ($hours <= 24){

        if ($hours == 1){

          return "an hour ago";

        } else {

          return "$hours hours ago";

        }

      } else if ($days <= 7){

        if ($days == 1){

          return "yesterday";

        } else {

          return "$days days ago";

        }

      } else if ($weeks <= 4.3){

        if ($weeks == 1){

          return "a week ago";

        } else {

          return "$weeks weeks ago";

        }

      } else if ($months <= 12){

        if ($months == 1){

          return "a month ago";

        } else {

          return "$months months ago";

        }

      } else {
        
        if ($years == 1){

          return "one year ago";

        } else {

          return "$years years ago";

        }
      }
    }

}