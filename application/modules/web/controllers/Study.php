<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Study extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'custom', 'custom_helper'));
        $this->load->library('session'); 
        $this->CI = & get_instance();   
    }

    public function index(){

        $url = site_url()."/data_model/slider/get_slider";
        $url1 = site_url()."/data_model/courses/course/get_all_category_data";
        $url2 = site_url()."/data_model/user/registration/get_all_category_db";
        if($this->session->id ==""){
            $document = array('file_url' => $url, 'user_id' => 0, 'slider_id'=>1);
            $document1 = array('file_url' => $url1, 'user_id' => 0, 'main_cat'=>1);
            $document2 = array('file_url' => $url2);
        }else{
            $document = array('file_url' => $url, 'user_id' => $this->session->id, 'slider_id'=>1);
            $document1 = array('file_url' => $url1, 'user_id' => $this->session->id, 'main_cat'=>1);
            $document2 = array('file_url' => $url2);
        }
        $data['slider_images'] = file_curl_contents($document);
        $data['category_data'] = file_curl_contents($document1);
        $data['category_details'] = file_curl_contents($document2);
        $data['active'] = 'Study';
         // echo "<pre>";print_r($data);die;
        $this->load->view('study/study', $data); 
    }
    
//=============================List of quiz===================================================    
    public function particularCategory($category_id){

        $url = site_url()."/data_model/courses/course/get_single_course_info_raw";
        $url1 = site_url()."/data_model/courses/course/get_all_file_info";
        if($this->session->id ==""){
            $document = array('file_url' => $url, 'user_id' =>0,'course_id'=>$category_id);
            $document1 = array('file_url' => $url1, 'user_id' => 0,'course_id'=>$category_id);
        }else{
            $document = array('file_url' => $url, 'user_id' => $this->session->id,'course_id'=>$category_id);
            $document1 = array('file_url' => $url1, 'user_id' => $this->session->id,'course_id'=>$category_id);
        }
        $data['name'] = file_curl_contents($document);
        $data['categoty_details'] = file_curl_contents($document1);
        $this->load->view('study/learn_course',$data);
    }
//==========================================================================================

//=============================List of quiz===================================================    
    public function particularCourse($course_id,$cat_id){

        $url = site_url()."/data_model/courses/course/get_single_course_info_raw";
        $url1 = site_url()."/data_model/courses/course/get_all_file_info";
        if($this->session->id ==""){
            $document = array('file_url' => $url, 'user_id' =>0,'course_id'=>$course_id);
            $document1 = array('file_url' => $url1, 'user_id' => 0,'course_id'=>$course_id);
        }else{
            $document = array('file_url' => $url, 'user_id' => $this->session->id,'course_id'=>$course_id);
            $document1 = array('file_url' => $url1, 'user_id' => $this->session->id,'course_id'=>$course_id);
        }
        $data['course_details'] = file_curl_contents($document);
        $data['categoty_details'] = file_curl_contents($document1);
        $data['cirruculam_id'] = $cat_id;
        $data['course_id'] = $course_id;
        // echo "<pre>";print_r($data);die;
        $this->load->view('study/course_info',$data);
    }
//==========================================================================================

//=============================List of quiz===================================================    
     public function startPractise($cat_id, $cirruculam_id){

        $url = site_url()."/data_model/courses/course/get_all_file_info";
        $document = array('file_url' => $url, 'user_id' => $this->session->id,'course_id'=>$cat_id);
        $data['categoty_details'] = file_curl_contents($document);
        $data['cirruculam_id'] = $cirruculam_id;
        // echo "<pre>";
        // print_r($data);die;

        $this->load->view('study/startPractise', $data);
    }
//==========================================================================================

//=============================List of quiz===================================================    
     public function testSeriesdetails(){

        $tid = $this->input->post('tid');
        $url = site_url()."/data_model/courses/test_series/get_test_series_with_id";
        $document = array('file_url' => $url, 'user_id' => $this->session->id,'test_series_id'=>$tid);
        $data = file_curl_contents($document);
        echo json_encode($data);
        die;
    }

//==========================================================================================
    public function quiz_by_id($quiz_id){
   
	    $url =site_url()."/data_model/courses/test_series/get_test_series_with_id";
	    $document = array('file_url' => $url, 'user_id' => $this->session->id,'test_series_id'=>$quiz_id);
	    $data = file_curl_contents($document);
	    $data['test_data'] =$data['data'];

	    $this->load->view('study/quiz',$data);  
    }
//==========================================================================================
    // public function study_question($course_id, $cat_id){
   
    //     $url = site_url()."/data_model/courses/course/get_all_file_info";
    //     $document = array('file_url' => $url, 'user_id' => $this->session->id,'course_id'=>$course_id);
    //     $data['categoty_details'] = file_curl_contents($document);
	   //  $data['cirruculam_id'] = $cat_id;
        
	   //  $this->load->view('study/study_question', $data); 
    // }
//==========================================================================================

//==========================================================================================
    // public function study_videos($course_id, $cat_id){
   
    //     $url = site_url()."/data_model/courses/course/get_all_file_info";
    //     $document = array('file_url' => $url, 'user_id' => $this->session->id,'course_id'=>$course_id);
    //     $data['categoty_details'] = file_curl_contents($document);
	   //  $data['cirruculam_id'] = $cat_id;
        
	   //  $this->load->view('study/study_videos', $data); 
    // }
//==========================================================================================

//==========================================================================================
    // public function study_concepts($course_id, $cat_id){
   
    //     $url = site_url()."/data_model/courses/course/get_all_file_info";
    //     $document = array('file_url' => $url, 'user_id' => $this->session->id,'course_id'=>$course_id);
    //     $data['categoty_details'] = file_curl_contents($document);
	   //  $data['cirruculam_id'] = $cat_id;
        
	   //  $this->load->view('study/study_concepts', $data); 
    // }
//==========================================================================================
}