<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url','custom','cookie'));
    }

    public function index(){
        $error['error'] = "";
        if($this->session->userdata('username') && $this->session->userdata('user_id') ){
            redirect(site_url('/web/feeds'));
            die;
        }
        if ($this->input->post()) {
            $this->form_validation->set_error_delimiters(' ', ' ');
            $this->form_validation->set_rules('password', 'Password', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');

            if ($this->form_validation->run() == FALSE) { }
                $email= $this->input->post('email');
                $password = $this->input->post('password');
                $url= site_url()."/data_model/user/registration/login_authentication";
                $document = array('file_url' => $url,'email'=>$email,'password'=>$password,'social_type'=>0,'social_tokken'=>'','device_type'=>0,'is_social'=>'','device_tokken'=>'');
                $post_list = file_curl_contents($document); 
                // echo "<pre>"; print_r($post_list);die;
                if($post_list['status'] == 1){
                    $session_var = array(
                        'username' => $post_list['data']['email'],
                        'user_id'=> $post_list['data']['id'],
                        'name'=> $post_list['data']['name'],
                        'pro_img' => $post_list['data']['profile_picture'],
                        'pass'=>$post_list['data']['password']
                    );
                    $this->session->set_userdata($session_var);
                    redirect(site_url('/web/feeds'));
                    die;
                }else{
                    $error['error'] = $post_list['message'];
                }
        }
        $this->load->view('login', $error);
    }

    function register()
    {
        $sub_cat_id =$this->input->post('sub_cat_id');
       
         if($sub_cat_id != '')
         {
             $sub_cat_id= implode(",",$sub_cat_id);
         }


     // $name = $this->input->post('first_name')." ".$this->input->post('last_name');
    	$name =$this->input->post('first_name');
        // echo $name." ".$this->input->post('pwd') ." ".$this->input->post('emailid');die;
      $data = array(
        'file_url' => site_url()."/data_model/user/registration", 
        'name' => $name,
        'email' => $this->input->post('emailid'),
        'mobile' => '',
        'profile_picture' => '',
        'is_social' => 0,
        'device_type' => 0,
        'social_type' => 0,
        'password' => $this->input->post('pwd')
    );
  
    $res = file_curl_contents($data);
        if($res['status']){  //store data in session
            $ses_data = array(
                'username' => $res['data']['email'],
                'name' => $res['data']['name'],
                'id' => $res['data']['id'],
                'pro_img' => $res['data']['profile_picture']
            );
                 $this->session->set_userdata($ses_data);


                 // ========================
                if($sub_cat_id != '')
                { 
                $url = site_url() . "/data_model/user/registration/update_user_prefrences";
                $document = array('file_url' => $url, 'user_id' => $this->session->id,'prefrences'=>$sub_cat_id);
                $save_prefrences= file_curl_contents($document);
                 // print_r($save_prefrences);die;
                 }
                 // =============
          
            echo "success";            
        }else{
            //echo "Something went wrong";
            echo $res['message'];
        }
}


    public function login(){
        $email= $this->input->post('user_name');
        $password = $this->input->post('pass_word');
        $remember_me =$this->input->post('remember_me');
        $url= site_url()."/data_model/user/registration/login_authentication";
        $document = array(
                'file_url' => $url, 
                'email'=>$email,
                'password'=>$password,
                'social_type'=>0,
                'social_tokken'=>'',
                'device_type'=>0,
                'is_social'=>'',
                'device_tokken'=>''
            );
        $post_list = file_curl_contents($document); 

        if($post_list['status'] == 1){
            if($remember_me == 1){
               set_cookie("cookie[username]", $post_list['data']['email'],'3600');
               set_cookie("cookie[id]", $post_list['data']['id'],'3600');
               set_cookie("cookie[name]", $post_list['data']['name'],'3600');
               set_cookie("cookie[pro_img]", $post_list['data']['profile_picture'],'3600');
               set_cookie("cookie[pass]", $password,'3600');//password no encrpt
            }
            $session_var = array(
                    'username' => $post_list['data']['email'],
                    'id'=> $post_list['data']['id'],
                    'name'=> $post_list['data']['name'],
                    'pro_img' => $post_list['data']['profile_picture'],
                    'pass'=>$post_list['data']['password']
            );
            $this->session->set_userdata($session_var);
            echo json_encode($post_list);               
        }else{
            echo json_encode($post_list);        
        }
    }

    public function logout(){
        $data = array(
           'username' => '',
           'name' => '',
           'id' => '',
           'pro_img' => ''
        );
        $this->session->set_userdata($data);
        $this->session->sess_destroy();
        redirect('web/login');
    }
   

   
}

 