<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url', 'custom', 'custom_helper'));
       $this->load->library('session');
         $this->CI = & get_instance();
    } 
     function index(){
	
		//$url = START_URL ."//data_model/fanwall/master_hit/content";  
        $url3 = site_url() . "/data_model/user/registration/get_prefrences";
        $document3 = array('file_url' => $url3, 'user_id' => $this->session->id);
        $sub_cat = file_curl_contents($document3);
        // $sub_cat=$sub_cat['data']['cat_id'];
        $data['sub_cat'] =$sub_cat;
        // print_r($data);die;

        $url = site_url()."/data_model/user/registration/get_all_category_db";
        $document = array('file_url' => $url);
        $data['category_details'] = file_curl_contents($document);

        $data['active'] = 'Profile';
        $this->load->view('profile/profile',$data);
       
    }
     public function header_notify(){
        $url3 = site_url() . "//data_model/notification_genrator/activity_logger/all_notification_counter";
        $document3 = array('file_url' => $url3, 'user_id' => $this->session->id);
        $notifyCount = file_curl_contents($document3);
        $count = $notifyCount['data']['counter'];
        echo $count;
    }
   
    function profiletest($id){
         $url3 = site_url() . "/data_model/user/registration/get_prefrences";
        $document3 = array('file_url' => $url3, 'user_id' => $id);
        $sub_cat = file_curl_contents($document3);
        $data['sub_cat'] =$sub_cat;

         $url = site_url() . "/data_model/user/Registration/get_active_user/".$id;
        $document = array('file_url' => $url);
        $user_detail = file_curl_contents($document);
        $data['profile'] =$user_detail;

          
        $this->load->view('profile/html',$data);
    }
}