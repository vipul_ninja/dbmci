<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feeds extends CI_Controller {
    public $CI = NULL;
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'custom', 'custom_helper'));
        $this->load->library('session');   
        $this->CI = & get_instance();

    }

    public function index(){
       
        $data = [];

        $url = site_url() . "/data_model/fanwall/master_hit/content";
        $document = array('file_url' => $url, 'user_id' => $this->session->id);
        $master_hit = file_curl_contents($document);

        $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user";
        $document1 = array('file_url' => $url1, 'user_id' => $this->session->id, "search_text"=>'',"tag_id"=>0);
        $post_list = file_curl_contents($document1);

        $url = site_url()."/data_model/user/registration/get_all_category_db";
        $document = array('file_url' => $url);
        $data['category_details'] = file_curl_contents($document);

        // $url2 = site_url()."/data_model/user/registration/get_prefrences";
        // $document2 = array('file_url' => $url2, 'user_id' => $this->session->id);
        // $get_preferences = file_curl_contents($document2);

        // $data['preferences'] = explode(",", $get_preferences['data']['cat_id']);

        $url3 = site_url() . "/data_model/user/User_follow/users_list";
        $document3 = array('file_url' => $url3, 'user_id' => $this->session->id,'user_type'=>'expert','last_id'=>'');
        $data['get_tag_people'] = file_curl_contents($document3);
        // echo "<pre>"; print_r($data);die;
        $data['lists'] = $master_hit;
        $data['posts'] = $post_list; 
        $data['active'] ='Feeds';
        // echo "<pre>"; print_r($data);die; 
        $this->load->view('feeds/feeds', $data);
    }
    
    public function single_post($post_id){
        $url = site_url() . "/data_model/fanwall/fan_wall/get_single_post_data_for_user";
        $document = array('file_url' => $url, 'user_id' => $this->session->id,'post_id'=>$post_id);
        $data['posts'] = file_curl_contents($document);
        // echo "<pre>"; print_r($data);die;
        $this->load->view('feeds/single_post', $data);
    }

    public function sub_category_data(){
        $category_id = $this->input->post('category_id');
        $url = site_url()."/data_model/user/registration/get_all_category_db";
        $document = array('file_url' => $url);

        $url2 = site_url()."/data_model/user/registration/get_prefrences";
        $document2 = array('file_url' => $url2, 'user_id' => $this->session->id);
        $get_preferences = file_curl_contents($document2);

        $data['preferences'] = explode(",", $get_preferences['data']['cat_id']);
        $data['category_details'] = file_curl_contents($document);
        $data['category_id'] = $category_id;
        $this->load->view('feeds/popup_cat_data', $data);
    }

    public function subject_data(){
        $data['sub_category_id'] = $this->input->post('sub_category_id');

        $url = site_url()."/data_model/user/registration/get_all_category_db";
        $document = array('file_url' => $url);
        $data['category_details'] = file_curl_contents($document);

        $url1 = site_url()."/data_model/user/registration/get_prefrences";
        $document1 = array('file_url' => $url1, 'user_id' => $this->session->id);
        $get_preferences = file_curl_contents($document1);
        $data['preferences'] = explode(",", $get_preferences['data']['cat_id']);

        $url2 = site_url() . "/data_model/fanwall/master_hit/content";
        $document2 = array('file_url' => $url2, 'user_id' => $this->session->id);
        $master_hits = file_curl_contents($document2);
        $data['subjects'] = $master_hits['data'];

        // echo "<pre>"; print_r($data);die;
        $this->load->view('feeds/popup_subject_data', $data);
    }

    public function add_post(){
        $user_id = $this->session->id;
        $post_type = $this->input->post('post_type');
        $subject_id = $this->input->post('subject_id');
        $sub_cate_id = $this->input->post('sub_cate_id');
        $text = $this->input->post('text');
        // echo $post_type;die;
        if($post_type == 'text'){
            $tag_people = $this->input->post('tag_people');
            $url = site_url()."data_model/user/post_text/add_post";
            $document = array('file_url' => $url,'user_id'=>$this->session->id, 'post_type'=>$post_type, 'subject_id'=>$subject_id, 'post_tag'=>'','sub_cate_id'=>$sub_cate_id,'lat'=>'','location'=>'','text'=>$text, 'tag_people'=>$tag_people, 'video_link'=>'', 'file'=>'', 'lon'=>'');
        }else if($post_type == 'mcq'){
            $answer_one = $this->input->post('answer_one');
            $answer_two = $this->input->post('answer_two');
            $answer_three = $this->input->post('answer_three');
            $answer_four = $this->input->post('answer_four');
            $answer_five = $this->input->post('answer_five');
            $right_answer = $this->input->post('right_answer');
            $url = site_url()."data_model/user/post_mcq/add_mcq";
            $document = array('file_url' => $url,'user_id'=>$this->session->id, 'subject_id'=>$subject_id, 'post_tag'=>'','sub_cate_id'=>$sub_cate_id,'lat'=>'','location'=>'','question'=>$text, 'tag_people'=>'', 'lon'=>'','answer_one'=>$answer_one,'answer_two'=>$answer_two,'answer_three'=>$answer_three,'answer_four'=>$answer_four,'answer_five'=>$answer_five,'right_answer'=>$right_answer);
        }
        $data['posts'] = file_curl_contents($document);
        $this->load->view('feeds/ajax_new_post', $data);
    }


    public function get_tag_people(){
        $user_type = $this->input->post('user_type');
        $last_id = $this->input->post('last_id');

        $url = site_url() . "/data_model/user/User_follow/users_list";
        $document = array('file_url' => $url, 'user_id' => $this->session->id,'user_type'=>$user_type,'last_id'=>$last_id);
        $data = file_curl_contents($document);
        // echo "<pre>"; print_r($data);die;
            if(!empty($data['data'])){
                foreach($data['data'] as $tag_people){ ?>
                    <label class='container select-chk'>
                        <div class='author-thumb'>
                            <img src='<?=$tag_people['profile_picture'];?>'>
                        </div>
                        <h4><?=$tag_people['name'];?></h4>
                        <input type='checkbox' name="<?=$user_type;?>_people" value="<?=$tag_people['id'];?>" data-name="<?=$tag_people['name'];?>">
                        <span class='checkmark1'></span>
                    </label>
        <?php   }
            }
    }


    public function set_tag_people(){
        $tag_peoples = $this->input->post('tag_peoples');
        $tag_peoples_name = $this->input->post('tag_peoples_name');
        $count_tag_people_id = sizeof($tag_peoples);
        for($i=0;$i<$count_tag_people_id;$i++){ ?>
            <span class="tag-fir-more" id="tag_<?=$tag_peoples[$i];?>"> 
                <span style="color: #0080ff;"><?=$tag_peoples_name[$i];?></span> 
                <a class="delete_tag_people" href="javascript:void(0);"  id="<?=$tag_peoples[$i];?>" name="<?=$tag_peoples_name[$i];?>">
                    <span><i class="fa fa-times text-danger" aria-hidden="true"></i></span> 
                </a>
            </span>
    <?php   } 
    }

    public function get_single_post($post_id){
        $url = site_url() . "/data_model/fanwall/fan_wall/get_single_post_data_for_user";
        $document = array('file_url' => $url, 'user_id' => $this->session->id,'post_id'=>$post_id);
        $data = file_curl_contents($document);
        if($data['data']['post_type'] == 'user_post_type_mcq'){
            echo $data['data']['post_data']['question']; 
        }else if($data['data']['post_type'] == 'user_post_type_normal'){
            echo $data['data']['post_data']['text']; 
        }
    }



//================================================1sts Dropdown Change=======================================
    public function main_category() {
        $data = [];
        $category_id = $this->input->post('category_id');

        $url2 = site_url() . "/data_model/user/registration/get_all_category_db";
        $document2 = array('file_url' => $url2, 'user_id' => $this->session->id);
        $post_list1 = file_curl_contents($document2);

        $data = $post_list1['data']['all_tags'];
        $result = '';
        for ($i = 0; $i < sizeof($post_list1['data']['all_tags']); $i++) {
            if ($data[$i]['master_id'] == $category_id) {
                $result .= '<option value="' . $data[$i]['id'] . '">' . $data[$i]['text'] . '</option>';
            }
        }
        $data = array('status' => true, 'result' => $result);
        $json = json_encode($data);
        echo $json;



        // $this->load->view('feed_ajax',$data);        
    }
     function like(){
        $like_data = array(
            'file_url' => site_url() . "/data_model/user/post_like/like_post",
            'post_id' => $this->input->post('post_id'),
            'user_id' => $this->session->id
        );
        $res = file_curl_contents($like_data);
        echo json_encode($res);
    }
    function dislike(){
        $dislike_data = array(
            'file_url' => site_url() . "/data_model/user/post_like/dislike_post",
            'post_id' => $this->input->post('post_id'),
            'user_id' => $this->session->id
        );
        $res = file_curl_contents($dislike_data);
        echo json_encode($res);
    }

    function postComment() {
        
        $comment_data = array(
            'file_url' => site_url() . "/data_model/user/post_comment/add_comment",
            'post_id' => $this->input->post('post_id'),
            'user_id' => $this->session->id,
            'comment' => $this->input->post('commentmsg')
        );
        // print_r($comment_data);die;
        $res = file_curl_contents($comment_data);
        echo json_encode($res);        
    }
 
    public function comment() { // Get Comment for particular post
        $data = []; 
        $postid = $this->input->post('postid');
        $url = site_url() . "/data_model/user/post_comment/get_post_comment";
        $document = array('file_url' => $url, 'post_id' => $postid,'user_id'=>$this->session->id);
        $post_list = file_curl_contents($document);

        if(!empty($post_list['data'])){
        foreach ($post_list['data'] as $comments) {
             //echo "<pre>" ;    print_r($post_list);die;
            ?>
          
            <li class="comment-item" >
                <div class="post__author author vcard inline-items">
                    <?php
                    if ($this->remote_file_exists($comments['profile_picture'], 1)) {
                        ?>
                        <img src="<?= $comments['profile_picture'] ?>" alt="author">
                    <?php } else { ?>
                        <i class="fa fa-user-circle" alt="author" style="font-size:28px"></i>
                    <?php } ?>
                    <div class="author-date">
                        
                        <div class="post__date">
                            <time class="published" datetime="2017-03-24T18:18">
                                <?php echo $this->time_ago_in_php($comments['time']); ?> 
                            </time>
                        </div>
                    </div>                 
                </div>
                <p style="text-align: left;"><?= $comments['comment'] ?></p>
           
            <a href="javascript:void(0)" id="<?= $comments['id'] ?>" class="rep<?= $comments['id'] ?>" main="1" onclick="reply(<?= $comments['post_id'] ?>,this.id,this.getAttribute('main'))"><?php if($comments['sub_comment_count'] == 0){ echo "Reply";}else { echo "See ".$comments['sub_comment_count']."  previous replies";}?></a>
                <div id="sub<?= $comments['id'] ?>" style="display:block;"></div>
            </li>
            <?php
        }
        }else{
            echo FALSE;
        }
    }

  function reply(){
        $data = []; 
        $postid = $this->input->post('postid');
        $comment_id= $this->input->post('comment_id');

        $url = site_url() . "/data_model/user/post_comment/get_post_comment";
        $document = array('file_url' => $url, 'post_id' => $postid,'parent_id'=> $comment_id,'user_id'=>$this->session->id);
        $post_list = file_curl_contents($document);
        
        if(!empty($post_list['data'])){ ?>  
           <?php foreach ($post_list['data'] as $comments) {?>
            <div id="reply<?= $comments['id'] ?>" class="" >
                <ul class="comments-list showmydata" id="load_comment<?= $comments['id'] ?>" >
                    <li class="comment-item">
                        <div class="post__author author vcard inline-items">
                            <?php if ($this->remote_file_exists($comments['profile_picture'], 1)) { ?>
                                <img src="<?= $comments['profile_picture'] ?>" alt="author">
                            <?php } else { ?>
                                <i class="fa fa-user-circle" alt="author" style="font-size:28px"></i>
                            <?php } ?>
                            <div class="author-date">
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        <?php echo $this->time_ago_in_php($comments['time']); ?> 
                                    </time>
                                </div>
                            </div>                 
                        </div>
                        <p><?= $comments['comment'] ?></p>           
                    </li>
               </ul>
            </div>
            <?php } ?>
             
                <form  class="comment-form inline-items" >
                    <div class="post__author author vcard inline-items">
                          <?php if ($this->remote_file_exists($this->session->pro_img, 1)) { ?>
                                <img src="<?= $this->session->pro_img ?>" alt="author">
                          <?php } else { ?>
                                <i class="fa fa-user-circle" alt="author" style="font-size:28px"></i>
                          <?php } ?>
                                <div class="form-group with-icon-right ">
                                    <textarea class="form-control" placeholder="" id="replydata<?= $postid ?>"></textarea> 
                                </div>
                    </div>
                        <?php if (isset($this->session->userdata['username'])) { ?>
                            <button type="button" class="btn btn-md-2 btn-info"  style="background-color: #3d87ff;" class="more-comments" onclick="replyComment('<?=$postid;?>','<?=$comment_id;?>')">Reply </button>
                        <?php } else { ?>
                            <button type="button" class="btn btn-md-2 btn-info" style="background-color: #3d87ff;" class="more-comments" data-toggle="modal" data-target="#registration-login-form-popup" onclick="replyComment('<?=$postid;?>','<?=$comment_id;?>')">Reply </button>
                        <?php } ?>
                            <button type="button" class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color"  onclick="reply(<?php echo $postid ?>,'<?=$comment_id;?>','cancel')" >Cancel</button>
                </form>
            
            <?php }else{  ?>

             <form  class="comment-form inline-items" >

                          <div class="post__author author vcard inline-items">
                              <?php if ($this->remote_file_exists($this->session->pro_img, 1)) { ?>
                                  <img src="<?= $this->session->pro_img ?>" alt="author">
                              <?php } else { ?>
                                  <i class="fa fa-user-circle" alt="author" style="font-size:28px"></i>
                              <?php } ?>
                              <div class="form-group with-icon-right ">
                                <textarea class="form-control" placeholder=""  id="replydata<?= $postid ?>" ></textarea> 
                              </div>
                          </div>
                          <?php if (isset($this->session->userdata['username'])) { ?>
                              <button type="button" class="btn btn-md-2 btn-info" style="background-color: #3d87ff;" class="more-comments" onclick="replyComment('<?=$postid;?>','<?=$comment_id;?>')" >Reply</button>
                          <?php } else { ?>
                              <button type="button" class="btn btn-md-2 btn-info" style="background-color: #3d87ff;" class="more-comments" data-toggle="modal" data-target="#registration-login-form-popup" onclick="replyComment('<?=$postid;?>','<?=$comment_id;?>')">Reply</button>
                          <?php } ?>
                          <button type="button" href="javascript:void(0)" class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color" onclick="reply(<?php echo $postid ?>,'<?=$comment_id;?>','cancel')">Cancel</button>
                    </form>

        <?php }

  }

    function replyComment(){                
        $comment_data = array(            
            'file_url' => site_url() . "/data_model/user/post_comment/add_comment",            
            'post_id' => $this->input->post('post_id'),            
            'parent_id' => $this->input->post('parent_id'),            
            'user_id' => $this->session->id,            
            'comment' => $this->input->post('commentmsg')        
        );     
         // print_r($comment_data);die;   
        $res = file_curl_contents($comment_data);
        // print_r($res);die;        
        echo json_encode($res);            
    }

    //single post data
    public function post($id) {

        $url = site_url() . "/data_model/fanwall/master_hit/content";
        $document = array('file_url' => $url, 'user_id' => $this->session->id);
        $master_hit = file_curl_contents($document);
        $data['lists'] = $master_hit;

        $url = site_url() . "/data_model/user/post_comment/get_post_comment";
        $document = array('file_url' => $url, 'post_id' => $id,'last_comment_id'=>'');
        $data['comment'] = file_curl_contents($document);
        $data['count'] = count($data['comment']['data']);

        $url1 = START_URL . "/data_model/fanwall/fan_wall/get_single_post_data_for_user";
        $document1 = array('file_url' => $url1, 'user_id' => $this->session->id, 'post_id' => $id);
        $post = file_curl_contents($document1);
        $data['posts'] = $post;
        $this->load->view('post_feed', $data);
    }
   

     public function ajax_post() {
        $data = [];
        $search = '';
        $l_p_i = $this->input->post('last_post_id');
        $type = $this->input->post('type');
        if(!empty($this->input->post('search'))){
            $search = $this->input->post('search');
        }
        if($type == 'all' || $type == 'search'){
            $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user";
        }else if($type == 'mme'){
            $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?get_expert_post=1";
        }else if($type == 'mentor'){
            $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?get_mentor_post=1"; 
        }else if($type == 'follow'){
            $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?get_mentor_post=1"; 
        }
        $document1 = array('file_url' => $url1, 'user_id' => $this->session->id, 'last_post_id' => $l_p_i, 'search_text'=>$search,'tag_id' => 0);
        $post_list = file_curl_contents($document1);

        $data['posts'] = $post_list; 
        $data['all'] = $type;  
         // pre($data);die;
        $this->load->view('feeds/ajax_bottom_loader', $data);
    }

    public function follow() {
        $data = [];
        $follower_id = $this->input->post('follower_id');
        $url = site_url() . "/data_model/user/user_follow/follow_user";
        $document = array('file_url' => $url, 'user_id' => $this->session->id, 'follower_id' => $follower_id);
        $follow = file_curl_contents($document);
        echo json_encode($follow);
        die;
    }

    public function unfollow() {
        $data = [];
        $follower_id = $this->input->post('follower_id');
        $url = site_url() . "/data_model/user/user_follow/unfollow_user";
        $document = array('file_url' => $url, 'user_id' => $this->session->id, 'follower_id' => $follower_id);
        $unfollow = file_curl_contents($document);
        echo json_encode($unfollow);
        die;
    }

    //===============================single video=================================================
        public function single_video($id){
        $selected_video_url = site_url() . "/data_model/video/Video_channel/get_single_video_data";
        $suggested_video_url=site_url() . "/data_model/fanwall/master_hit/content";

         $video_id = $id;
        $document = array('file_url' => $selected_video_url, 'user_id' => $this->session->id,'video_id'=>$video_id);
        $data = file_curl_contents($document);
        $data['d1']=$data;

        //suggested video
        $document1 = array('file_url' => $suggested_video_url, 'user_id' => $this->session->id);
        $suggested_video_data = file_curl_contents($document1);
        $data['d3']=$suggested_video_data;
        // echo "<pre>";
        // print_r($data);die;
        $this->load->view('dailydose/dailydose_single_video',$data);

    }
     //============================================================================================== 

    public function mcq_answer() {
        $data = [];
        $mcq_id = $this->input->post('mcq_id');
        $answer = $this->input->post('answer');
        $url = site_url() . "/data_model/user/post_mcq/answer_post_mcq";
        $document = array('file_url' => $url, 'user_id' => $this->session->id, 'mcq_id' => $mcq_id, 'answer'=> $answer);
        $mcqData = file_curl_contents($document);
        echo json_encode($mcqData);
        die;
    }

    public function ajax_post_dropdown() {
        $data = [];
        $search = '';
        $category = $this->input->post('category');
        if(!empty($this->input->post('srch_txt'))){
            $search = $this->input->post('srch_txt');
        }
        if($category == 'all' || $category == 'search'){
            $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user";   
        }else if($category == 'mme'){
            $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?get_expert_post=1";
        }else if($category == 'mentor'){
            $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?get_mentor_post=1";
        }else if($category == 'follow'){
            $url1 = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?get_mentor_post=1";    
        }
        $document1 = array('file_url' => $url1, 'user_id' =>$this->session->id, 'tag_id' => 0,'search_text'=>$search);
        $post_list = file_curl_contents($document1);
        $data['posts'] = $post_list;
        $data['all'] = $category;
        $this->load->view('feeds/ajax_bottom_loader', $data);
    }

    public function delete_post(){
        $post_id =$this->input->post('post_id');

         $url = site_url() . "/data_model/user/Post_delete/delete_post";
        $document = array('file_url' => $url, 'user_id' => $this->session->id, 'post_id'=>$post_id);
        $res = file_curl_contents($document);
        // $res =json_encode($res);

        if($res['status'] == true){
               echo json_encode($res['message']);
        }
        else{
                  echo json_encode($res['message']);
        }
    }
    public function notification(){
        $url = site_url() . "/data_model/notification_genrator/activity_logger/all_notification_counter";
        $document = array('file_url' => $url, 'user_id' => $this->session->id);
        $res = file_curl_contents($document);
        $res=$res['data'];
        $res['counter'] =$res['counter'];
        $this->load->view($res);
    }
    public function remote_file_exists( $source, $extra_mile = 0 ) { 
      # EXTRA MILE = 0 
      ////////////////////////////////// Does it exist? 
      # EXTRA MILE = 1 
      ////////////////////////////////// Is it an image? 
      if ( $extra_mile === 1 ) { 
        $img = @getimagesize($source); 
        if ( !$img ) return 0; 
        else{ 
          switch ($img[2]){ 
            case 0: 
            return 0;     
            break; 
            case 1: 
            return $source; 
            break; 
            case 2: 
            return $source; 
            break; 
            case 3: 
            return $source; 
            break; 
            case 6: 
            return $source; 
            break; 
            default: 
            return 0;         
            break; 
          } 
        } 
      }else { 
        if (@FClose(@FOpen($source, 'r')))  
          return 1; 
        else 
          return 0; 
        } 
    } 

    public function time_ago_in_php($timestamp){
  
      date_default_timezone_set("Asia/Kolkata");         
      $time_ago = round($timestamp/1000);  
      $current_time  = time();
      $time_difference = $current_time - $time_ago;
      $seconds  = $time_difference;
      
      $minutes = round($seconds / 60); // value 60 is seconds  
      $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
      $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;  
      $weeks   = round($seconds / 604800); // 7*24*60*60;  
      $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
      $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60
                    
      if ($seconds <= 60){

        return "Just Now";

      } else if ($minutes <= 60){

        if ($minutes == 1){

          return "one minute ago";

        } else {

          return "$minutes minutes ago";

        }

      } else if ($hours <= 24){

        if ($hours == 1){

          return "an hour ago";

        } else {

          return "$hours hours ago";

        }

      } else if ($days <= 7){

        if ($days == 1){

          return "yesterday";

        } else {

          return "$days days ago";

        }

      } else if ($weeks <= 4.3){

        if ($weeks == 1){

          return "a week ago";

        } else {

          return "$weeks weeks ago";

        }

      } else if ($months <= 12){

        if ($months == 1){

          return "a month ago";

        } else {

          return "$months months ago";

        }

      } else {
        
        if ($years == 1){

          return "one year ago";

        } else {

          return "$years years ago";

        }
      }
    }

}