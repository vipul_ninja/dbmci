<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Header extends CI_Controller {
    public $CI = NULL;
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'custom', 'custom_helper'));
        $this->load->library('session');   
        $this->CI = & get_instance();

    }

    public function notification_count(){
        $url3 = site_url() . "/data_model/notification_genrator/activity_logger/all_notification_counter";
        if($this->session->id == ""){
          $document3 = array('file_url' => $url3, 'user_id' => 0);
        }else{
          $document3 = array('file_url' => $url3, 'user_id' => $this->session->id);
        }
        $notifyCount = file_curl_contents($document3);
        $count = $notifyCount['data']['counter'];
        if($count > 0){
          echo $count;
        }
    }

    public function header_notifications(){
        $url = site_url() . "/data_model/notification_genrator/activity_logger/get_user_activity";
        if($this->session->id == ""){
          $document = array('file_url' => $url, 'user_id' => 0,'last_activity_id'=>'');
        }else{
          $document = array('file_url' => $url, 'user_id' => $this->session->id,'last_activity_id'=>'');
        }
        $data['notifications'] = file_curl_contents($document);
        $this->load->view('notification_header_data_ajax', $data);
    }

    public function notifications(){
        $url = site_url() . "/data_model/notification_genrator/activity_logger/get_user_activity";
        if($this->session->id == ""){
          $document = array('file_url' => $url, 'user_id' => 0,'last_activity_id'=>'');
        }else{
          $document = array('file_url' => $url, 'user_id' => $this->session->id,'last_activity_id'=>'');
        }
        $data['notifications'] = file_curl_contents($document);
        // echo "<pre>"; print_r($data);die;
         $this->load->view('notifications', $data);
    }

    public function viewed_notifications($id,$did,$upid){

        $url = site_url() . "/data_model/notification_genrator/activity_logger/make_activity_viewed";
        $document = array('file_url' => $url, 'user_id' => $this->session->id,'id'=>$id);
        $data = file_curl_contents($document);
        if($data['status'] == true){
          if($did == '1'){
            redirect('/web/Feeds/single_post/'.$upid);
          }else if($did == '2'){
            redirect('/web/Profile/profiletest/'.$upid);
          }
        }
    }

    public function read_all_notifications(){

      $url1 = site_url() . "/data_model/notification_genrator/Activity_logger/set_all_read";
      $document1 = array('file_url' => $url1, 'user_id' => $this->session->id);  
      $data = file_curl_contents($document1);
      echo json_encode($data);die;
    }
      
    public function my_notes(){
        $url = site_url() . "/data_model/fanwall/fan_wall/get_fan_wall_for_user?bookmark_request=yes";
        if($this->session->id == ""){
          $document = array('file_url' => $url, 'user_id' => 0);
        }else{
          $document = array('file_url' => $url, 'user_id' => $this->session->id);
        }
        $data['posts'] = file_curl_contents($document);
        // echo "<pre>";print_r($data);die;
        $this->load->view('my_notes', $data);
    }
    
    public function remote_file_exists( $source, $extra_mile = 0 ) { 
      # EXTRA MILE = 0 
      ////////////////////////////////// Does it exist? 
      # EXTRA MILE = 1 
      ////////////////////////////////// Is it an image? 
      if ( $extra_mile === 1 ) { 
        $img = @getimagesize($source); 
        if ( !$img ) return 0; 
        else{ 
          switch ($img[2]){ 
            case 0: 
            return 0;     
            break; 
            case 1: 
            return $source; 
            break; 
            case 2: 
            return $source; 
            break; 
            case 3: 
            return $source; 
            break; 
            case 6: 
            return $source; 
            break; 
            default: 
            return 0;         
            break; 
          } 
        } 
      }else { 
        if (@FClose(@FOpen($source, 'r')))  
          return 1; 
        else 
          return 0; 
        } 
    } 

    public function time_ago_in_php($timestamp){
  
      date_default_timezone_set("Asia/Kolkata");         
      $time_ago = round($timestamp/1000);  
      $current_time  = time();
      $time_difference = $current_time - $time_ago;
      $seconds  = $time_difference;
      
      $minutes = round($seconds / 60); // value 60 is seconds  
      $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
      $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;  
      $weeks   = round($seconds / 604800); // 7*24*60*60;  
      $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
      $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60
                    
      if ($seconds <= 60){

        return "Just Now";

      } else if ($minutes <= 60){

        if ($minutes == 1){

          return "one minute ago";

        } else {

          return "$minutes minutes ago";

        }

      } else if ($hours <= 24){

        if ($hours == 1){

          return "an hour ago";

        } else {

          return "$hours hours ago";

        }

      } else if ($days <= 7){

        if ($days == 1){

          return "yesterday";

        } else {

          return "$days days ago";

        }

      } else if ($weeks <= 4.3){

        if ($weeks == 1){

          return "a week ago";

        } else {

          return "$weeks weeks ago";

        }

      } else if ($months <= 12){

        if ($months == 1){

          return "a month ago";

        } else {

          return "$months months ago";

        }

      } else {
        
        if ($years == 1){

          return "one year ago";

        } else {

          return "$years years ago";

        }
      }
    }
}