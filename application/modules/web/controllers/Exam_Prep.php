<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Exam_Prep extends CI_Controller {
	public $CI = NULL;
	function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'custom', 'custom_helper'));
        $this->load->library('session');   
        $this->CI = & get_instance();

    }

    public function index(){
        $url = site_url() . "/data_model/courses/course/get_all_category_data_exam";
        if($this->session->id ==""){
            $document = array('file_url' => $url, 'cat_id'=> 1, 'course_type' => 1,'user_id'=>0,'main_cat'=>1);
        }else{
            $document = array('file_url' => $url, 'cat_id'=> 1, 'course_type' => 1,'user_id'=>$this->session->id,'main_cat'=>1);
        }

        $data["cat"] = file_curl_contents($document);

        $url1 = site_url()."/data_model/user/registration/get_all_category_db";
        $document1 = array('file_url' => $url1);
        $data['category_details'] = file_curl_contents($document1);
        $data['active'] = 'ExamPrep';
        // echo "<pre>";print_r($data);die;    
        $this->load->view('examprep/exam-prepration', $data);
    }

    public function ajax_dropdown(){
    	$id=$this->input->post('id');
        $category=$this->input->post('category');
        $cat_id = $this->input->post('cat_id');
    	$url = site_url() . "/data_model/courses/course/get_all_category_data_exam";
        if($this->session->id ==""){
            $document = array('file_url' => $url, 'cat_id'=> $cat_id, 'course_type' => $category,'user_id'=>0,'main_cat'=>$id);
        }else{
            $document = array('file_url' => $url, 'cat_id'=> $cat_id, 'course_type' => $category,'user_id'=>$this->session->id,'main_cat'=>$id);
        }
        $data["cat"] = file_curl_contents($document);
        $url1 = site_url()."/data_model/user/registration/get_all_category_db";
        $document1 = array('file_url' => $url1);
        $data['category_details'] = file_curl_contents($document1);
        $data['parent_id'] = $id;
        $this->load->view('examprep/ajax_exam',$data); 
    }

    public function subCatInfo($topic_id){
        $url = site_url() . "/data_model/courses/exam/get_basic_data";
        $url1 = site_url() . "/data_model/courses/exam/get_video_data";
        $url2 = site_url() . "/data_model/courses/exam/get_test_data";
        $url3 = site_url() . "/data_model/courses/exam/get_concept";
        $url4 = site_url() . "/data_model/courses/exam/get_practice";
        if($this->session->id ==""){
            $document = array('file_url' => $url, 'user_id'=>0,'id'=>$topic_id);
            $document1 = array('file_url' => $url1, 'user_id'=>0,'id'=>$topic_id, 'layer'=>1);
            $document2 = array('file_url' => $url2, 'user_id'=>0,'id'=>$topic_id, 'layer'=>1);
            $document3 = array('file_url' => $url3, 'user_id'=>0,'id'=>$topic_id, 'layer'=>1);
            $document4 = array('file_url' => $url4, 'user_id'=>0,'id'=>$topic_id, 'layer'=>1);
        }else{
            $document = array('file_url' => $url, 'user_id'=>$this->session->id,'id'=>$topic_id);
            $document1 = array('file_url' => $url1, 'user_id'=>$this->session->id,'id'=>$topic_id, 'layer'=>1);
            $document2 = array('file_url' => $url2, 'user_id'=>$this->session->id,'id'=>$topic_id, 'layer'=>1);
            $document3 = array('file_url' => $url3, 'user_id'=>$this->session->id,'id'=>$topic_id, 'layer'=>1);
            $document4 = array('file_url' => $url4, 'user_id'=>$this->session->id,'id'=>$topic_id, 'layer'=>1);
        }
        $data["topic_data"] = file_curl_contents($document);
        $data["video_data"] = file_curl_contents($document1);
        $data["test_data"] = file_curl_contents($document2);
        $data["concept_data"] = file_curl_contents($document3);
        $data["book_data"] = file_curl_contents($document4);
        $this->load->view('examprep/sub_cat_details', $data); 
    }
    public function subjectDetails(){
        $this->load->view('examprep/subject_details'); 
    }
    public function subjectDetailsLast(){
        $this->load->view('examprep/subject_details_last'); 
    }
    public function multiPost(){
        $this->load->view('profile/multi-post'); 
    }
    public function singlePost(){
        $this->load->view('profile/single-post'); 
    }

}