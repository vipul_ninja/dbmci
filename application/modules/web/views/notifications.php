<?php if(!$this->session->userdata('username')){
  header("Location: Home");
}?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title>Notifications</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
	<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
	<!-- Ibt Styles CSS -->
	<link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">


</head>
<style type="text/css">
	.subject_video_box{background-color: #fff;
    display: block;
    position: relative;
    margin-bottom: 30px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: 0px 0px 30px 0px rgba(0,0,0,0.1);
    -moz-box-shadow: 0px 0px 30px 0px rgba(0,0,0,0.1);
    box-shadow: 0px 0px 30px 0px rgba(0,0,0,0.1);}

    .post-video .video-content .video_title .title {
    display: inline-flex;
}

     .favourite_icon{
    display: -webkit-inline-box;
    float: right;
    max-width: 15px;
}

.responsive {
  
    max-width: 200px;
    height: 200px;
    object-fit:cover;
}
</style>
<body>


<div class="header-spacer"></div>

<?php $this->load->view('include/header.php');?>

<!-- quiz-box -->
<div class="container">
  <div class="row">
    <?php if(!empty($notifications)){ ?>
    <div class="col-md-8 offset-md-2">
    	<div class="page-flow">
            <ul class="breadcrumbs">
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/Feeds">Home</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item active">
            <span>Notifications</span>
          </li>
        </ul>
      </div>
    	<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Notifications</h6>
					<span style="float: right;"><a href="javascript:void(0);" id="read_all">Read All</a></span>
				</div>

				
				<!-- Notification List -->
				
				<ul class="notification-list">
				<?php $i = 0;
          foreach($notifications['data'] as $notify) {  
            if(!empty($notify['post_id'])){
              $did = '1';
              $id = $notify['post_id'];
            }
            if(!empty($notify['user_id'])){
              $did = '2';
              $id = $notify['user_id'];
            }
        ?> 
         <a class="content" href="<?php echo site_url('/web/Header/viewed_notifications/'.$notify['id']."/".$did."/".$id); ?>"> 
					<li class="remove_read_all" style="background-color: <?php if($notify['view_state'] == 0){ echo "#d3def2;"; }else{echo "#fff;";}?>">
						<div class="author-thumb">
							<?php if ($this->CI->remote_file_exists($notify['action_performed_by']['profile_picture'], 1)) { ?>
					            <img src="<?= $notify['action_performed_by']['profile_picture']; ?>" alt="author">
					        <?php } else { ?>
					            <img src="/web_assets/img/img_avatar.png" alt="author">
					        <?php } ?>
						</div>
						<div class="notification-event">
							<span href="#" class="h6 notification-friend"><?=$notify['action_performed_by']['name']." ";?></span> 
							  	 <?php 
                              if($notify['activity_type'] == "post_like"){
                                $text = 'Liked your post. ';
                              }else if($notify['activity_type'] == "post_comment"){
                                $text = 'Commented on your post ';
                              }else if($notify['activity_type'] == "following"){
                                $text = 'Following You. ';
                              }else{
                                $text = 'Tagged you. ';
                              }?><?php echo $text." ";?>
						          <span href="#" class="notification-link"> </span>
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18"><?php echo $this->CI->time_ago_in_php($notify['creation_time']); ?></time></span>
						</div>
						
					</li>
        </a>
				<?php } } ?>
			</ul>
				
				<!-- ... end Notification List -->

			</div>
      
  
     

    </div>
  </div>
  </div>
<script type="text/javascript">
$(document).ready(function () {
  $('#read_all').click(function(){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: '<?php echo site_url('web/Header/read_all_notifications'); ?>',
      success: function (data) {
        if(data.status == true) {
          $('.remove_read_all').css({"background-color:":"#fff"});
          $('#noti_count').html('');
        }
      }
    });
  });
});
</script>
<!-- JS Scripts -->
<script src="/web_assets/js/jquery-3.2.1.js"></script>
<script src="/web_assets/js/mediaelement-playlist-plugin.min.js"></script>
<script src="/web_assets/js/sticky-sidebar.js"></script>
<script src="/web_assets/js/base-init.js"></script>
<script src="/web_assets/Bootstrap/dist/js/bootstrap.bundle.js"></script>


</body>
</html>