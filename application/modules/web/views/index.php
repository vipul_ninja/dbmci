<?php if($this->session->userdata('username')){
   header("Location: Feeds");
}?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>DBMCI</title> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 
  <link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/web_assets/Bootstrap/dist/css/bootstrap.css"> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/web_assets/css/ibt_main.css"> 
  <link rel="stylesheet" href="<?php echo base_url(); ?>/web_assets/appy/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/web_assets/appy/css/linearicons.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/web_assets/appy/css/style.css"> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script type="text/javascript">
      $(window).load(function () {
          $(".loader").fadeOut("slow");
      });
  </script> 
  <style>
    *{
      margin:0;
      padding: 0;
    }
    body{
      margin: 0;
      padding: 0;
    }
  </style>
</head>
<body style="background-color: #fff;">
<div class="loader"></div> 
<?php include('include/header.php');?> 
   <div class="header-spacer"></div> 
  <section class="hero-banner-i">
    <div class="container ibt-home1">
      <div class="row">
        <div class="col-md-7">
          <div class="hero-banner-content">
            <div class="hero-banner-content-title">
              <h2 class="section-title"><span>1 Million+</span>Aspirants study from makemyexam Everyday. Join Now!! </h2>
            </div>
            <ul>
              <li><img src="<?php echo base_url(); ?>/web_assets/img/search.png" alt=""><span>Search for exam videos & Quiz</span> </li>
              <li> <img src="<?php echo base_url(); ?>/web_assets/img/group.png" alt=""><span>Ask your doubts, Create MCQ & Share info</span> </li>
              <li><img src="<?php echo base_url(); ?>/web_assets/img/test_white.png" alt=""><span>Attempt unlimited Questions & Test for free</span> </li>
              <li><img src="<?php echo base_url(); ?>/web_assets/img/video_course_white.png" alt=""><span>Watch Interactive Video Lectures.</span></li>
            </ul>
          </div>
        </div>   
        <div class="col-md-5 login-hidden">
          <div class="hero-banner-form">
            <h4>Sign up &amp; be apart of India's Leading Community </h4>
            <div class="socials-shared">
              <a href="https://www.facebook.com/v2.6/dialog/oauth?client_id=702273420148786&amp;state=1829dc26b2a6b555e89bb6b7a00a7901&amp;response_type=code&amp;sdk=php-sdk-5.0.0&amp;redirect_uri=http%3A%2F%2F13.232.24.221%2Fhttp%3A%2F%2F13.126.67.127%2Findex.php%2Fweb%2Fhome&amp;scope=email" class="social-item bg-facebook">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                Facebook
              </a>            
              <a href="https://accounts.google.com/o/oauth2/auth?response_type=code&amp;redirect_uri=http%3A%2F%2F13.126.67.127%2Findex.php%2Fweb%2Fhome%2Fg_login&amp;client_id=474830604768-7q864jnrfq1le81lfke1eoo66g1uefkv.apps.googleusercontent.com&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&amp;access_type=offline&amp;approval_prompt=force" class="social-item bg-google">
                <i class="fa fa-google-plus" aria-hidden="true"></i>
                Google
              </a>
            </div>
            <form class="content" id="register2">
                <div class="row">
                  <span style="color: red;margin-left: 20px;" id="msg1"></span> 
                <div class="or"></div>
                  <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12"> 
                    <div class="form-group label-floating is-empty">
                        <input class="form-control" placeholder="Your Name" type="text" name="first_name" id="register_name1">
                        <span class="material-input"></span>
                    </div> 
                    <div class="form-group label-floating is-empty"> 
                        <input class="form-control" placeholder="Your Email" type="email" name="emailid" id="register_emailid1">
                        <span class="material-input"></span>
                    </div> 
                    <div class="form-group label-floating is-empty">
                        <input class="form-control" placeholder="Your Password" type="password" name="pwd" id="register_pwd1">
                        <span class="material-input"></span>
                    </div> 
                      <a href="#" class="btn btn-lg btn-primary full-width btn-i next">Next</a>
                      <p class="para">Have an account? <a href="javascript:void(0);" data-toggle="modal" data-target="#registration-login-form-popup"><b>Login Now!</b></a> </p>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="choose-exam">
    <div class="container">
      <div class="section-title text-center heading-h3" style="color: black">
        <h3> Select Your Exam Below <img src="http://13.232.24.221/web_assets/img/down-arrow.gif" class="img-down"></h3>
      </div>
      <div class="row">
        <div class="col-md-2 col-xs-12 scroll-bar-wrap">
          <div class="subject-list scroll-box">
            <div class="nav flex-column nav-pills text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical"> 
              <?php if(!empty($category_details)){
                    $i = 1;
                      foreach ($category_details['data']['main_category'] as $category) {
                        if($i ==1){
              ?>
                  <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-<?=$category['id'];?>" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <div class="subject-list-nu"><div class="custom-bg" style="background-color:<?=$category['color'];?>"> <img src="<?=$category['image'];?>" alt=""></div>
                      <p><?=$category['text'];?></p>
                    </div>
                  </a>
                  <?php }else{ ?>
                  <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-<?=$category['id'];?>" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    <div class="subject-list-nu"> <div class="custom-bg" style="background-color:<?=$category['color'];?>"><img src="<?=$category['image'];?>" alt=""></div>
                      <p><?=$category['text'];?></p>
                    </div>
                  </a>
                <?php } $i++; }  } ?>
         </div>
          </div>
        </div>
        <div class="col-md-10 col-xs-12">
          <div class="subject-exam-item">
             <?php if(!empty($category_details)){ ?>
            <div class="tab-content" id="v-pills-tabContent">
            <?php   $i = 1; 
                  foreach ($category_details['data']['main_category'] as $category) {
                  if($i == 1){ ?>
            <div class="tab-pane fade show active" id="v-pills-<?=$category['id'];?>" role="tabpanel" aria-labelledby="v-pills-home-tab">
   
               <div class="row">
                <?php foreach($category_details['data']['main_sub_category'] as $subCategory){
                        if($category['id'] == $subCategory['parent_id']){ ?>
                   <div class="col-md-2 text-center  text-center1 ">
                    <a href="javascript:void(0)" class="clickcheck" id="<?=$subCategory['id'];?>">
                       <!-- <label class="customcheck">
                          <input type="checkbox" id="check<?=$subCategory['id'];?>"  value="<?=$subCategory['id'];?>" disabled>
                            <span class="checkmark">
                            </span>
                        </label> -->
                            <div class="subject-cate-list"><div class="custom-bg" style="background-color:<?=$subCategory['color'];?>"> <img src="<?=$subCategory['image'];?>" alt=""></div>
                             <p><?=$subCategory['text'];?></p>
                            </div>
                          </a>                    
                    </div>
                <?php } }  ?>
                 </div>
              </div>
               <?php }else { ?>
            <div class="tab-pane fade" id="v-pills-<?=$category['id'];?>" role="tabpanel" aria-labelledby="v-pills-home-tab">

                <div class="row">
                <?php foreach($category_details['data']['main_sub_category'] as $subCategory){
                        if($category['id'] == $subCategory['parent_id']){ ?>
                   <div class="col-md-2 text-center text-center1">
                    <a href="javascript:void(0)" class="clickcheck" id="<?=$subCategory['id'];?>">
                       <!-- <label class="customcheck" >
                          <input type="checkbox" id="check<?=$subCategory['id'];?>" value="<?=$subCategory['id'];?>" disabled>
                            <span class="checkmark">
                            </span>
                        </label> -->
                            <div class="subject-cate-list"><div class="custom-bg" style="background-color:<?=$subCategory['color'];?>"> <img src="<?=$subCategory['image'];?>" alt=""></div>
                             <p><?=$subCategory['text'];?></p>
                            </div> 
                             </a>
                    </div>
                   
                <?php } } ?>
                 </div>
              </div>
            <?php } $i++;} ?>
            </div>
          <?php } ?>
          </div>
        </div> 
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 offset-md-4">
        <div class="lets-start-pre ">
          <button class="btn btn-lg btn-primary full-width btn-i startprep" data-toggle="modal" data-target="#registration-login-form-popup" data-tooltip-id="1"  style="color:#fff;" >Let's Start Preparation</button>
        </div>
      </div>
    </div>
  </section> 
  <section class="feature-area section-padding-top" id="features_page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h5 class="title">Features</h5>
                        <div class="space-10"></div>
                        <h3 style="color: white">Why Choose Make My Exam?</h3>
                        <div class="space-60"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="/web_assets/img/daily_dose1.png" alt="">
                        </div>
                        <h4>Daily Quiz</h4>
                        <p>
                            Practice on question banks and mock tests Mock
                            Tests created by the experts,
                        </p>
                    </div>
                    <div class="space-60"></div>
                    <div class="service-box wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="/web_assets/img/current_affair.png" alt="">
                        </div>
                        <h4>Current Affairs</h4>
                        <p>
                            Cover all important current affairs important for exam
                            preparation.
                        </p>
                    </div>
                    <div class="space-60"></div>
                    <div class="service-box wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="/web_assets/img/article.png" alt="">
                        </div>
                        <h4>Articles</h4>
                        <p>
                            Expert’s study tips that will help you to ace
                            competitive entrance.
                        </p>
                    </div>
                    <div class="space-60"></div>
                </div>
                <div class="hidden-xs hidden-sm col-md-4">
                    <figure class="mobile-image">
                        <img src="/web_assets/img/feature-image.png" alt="Feature Photo">
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="/web_assets/img/job_alerts.png" alt="">
                        </div>
                        <h4>Review of the day</h4>
                        <p>
                            Get a review of exam analysis, job alerts, exam
                            dates, call letter &amp; exam syllabus.
                        </p>
                    </div>
                    <div class="space-60"></div>
                    <div class="service-box wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="/web_assets/img/video_lectures1.png" alt="">
                        </div>
                        <h4>Video Lectures</h4>
                        <p>
                            Online Video Lectures for Bank PO/SO/Clerical &amp;
                            SSC Exams.
                        </p>
                    </div>
                    <div class="space-60"></div>
                    <div class="service-box wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="/web_assets/img/vocab.png" alt="">
                        </div>
                        <h4>Vocabulary Dose</h4>
                        <p>
                            Daily Vocabulary Words dose uses to help you build
                            strong and rich English.
                        </p>
                    </div>
                    <div class="space-60"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonials-mob" id="testimonial_page" style="padding-bottom: 0" >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h5 class="title">SUCCESS STORY</h5>
                        <h3 class="dark-color">Our Aspirants Loves Us</h3>
                        <div class="space-60"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="team-slide">
                      <?php if(!empty($testimonial)){ 
                          foreach($testimonial['data'] as $testimonials){?>
                              <div class="team-box">
                                  <div class="team-image">
                                      <img src="<?=$testimonials['profile_picture'];?>" alt="">
                                  </div>
                                  <h4><?=$testimonials['name'];?></h4>
                                  <!-- <h6 class="position">IBPS CLERK</h6> -->
                                  <p class="testimonials"><?=$testimonials['review'];?></p>
                              </div>
                    <?php } } ?>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
  <section class="download-app">
    <div class="row download_app_before">
      <div class="col-6 img-mob">
        <img src="/web_assets/img/download-image.png" class="mobile_img_home">
      </div>
      <div class="col-6">
        <div>
        <div class="section-title text-center2" style="">
          <h2>Download MakeMyExam APP</h2>
          <h4>We are coming soon!</h4>
          <p>
            "Empower your bank, SSC & Cat Exam Preparation with MakeMyExam App. Free app to download from Android and IOS store."
          </p>
        </div>
      <div class="container" style="">
        <div class="row row-custom">
          <div class="col-md-3 col-xs-6">
            <a href="" class="app-store"><img class="img-fluid" src="http://13.232.24.221//web_assets/img/google-play-store.png" alt=""></a> 
          </div>
          <div class="col-md-3 col-xs-6"> 
            <a href="" class="app-store"><img class="img-fluid img-fluid1" src="http://13.232.24.221//web_assets/img/apple-store.png" alt=""></a> 
          </div> 
        </div>
        <div class="row"> 
        <form class="form-inline" method="post" action="" id="mobile_number">
          <div class="form-group mx-sm-3 mb-2 ans1 col-xs-12"> 
            <input required="" onkeyup="check(); return false;" type="text" class="form-control" id="mobile-nu" name="mobile-nu" placeholder="Enter Valid Number" pattern="[0-9]{10}"> 
          </div> 
          <button type="button" id="send_sms" class="btn btn-primary get-sms btn-i ds1 btn-res">GET SMS</button>
          <div class="form-group mx-sm-3 mb-2"> 
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  </div>
</section> 
<?php include('include/footer.php'); ?>

<script type="text/javascript">
// ===================================================================================================

//  var sub_cat_id=[],i=0;
// $('.clickcheck').click(function(){
//   var id = $(this).attr('id');
//  sub_cat_id[i++]=id;

//   if($('#check'+id). prop("checked") == false){
//     $('#check'+id).attr('checked', 'checked');
//   }else{
//     $('#check'+id).removeAttr('checked');
//   }
// });
// ===================================================================================================
$('.startprep').click(function(){
  var checked = $("input[type=checkbox]:checked").length;
  $(this).attr('title','');
  if(!checked){
     // alert('You must check atleast one category');
     $(this).attr('title','You must check atleast one category');
      $(this).addClass("on");
        $(this).tooltip({
            items: '.trigger.on'
        });
        $(this).trigger('mouseenter');
  }else{
      var val = [];
      $(':checkbox:checked').each(function(i){          
        val[i] = $(this).val();                 
      }); 
      $('#registration-login-form-popup').modal('show');
  }
});
// =================================================================================================
//==================================================================================================
$(function() {
  $('#mobile-nu').on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})

function check(){

 var number=$('#mobile-nu').val();
 if(number.length!=10){
    $('#message').css('color','red');
    $('#message').html("required 10 digits, match requested format!");
 }
 if(number.length==10){
    $('#message').html("");
 }
}


$('#send_sms').click(function(){
  var number = $('#mobile-nu').val();
  if(number == '' || number.length != 10 ){
      alert("Enter Your Correct Mobile Number");
      return;
  }else{
    $.ajax({
        data : {number:number},
        type:'post',
        url:'<?php echo base_url();?>index.php/web/Home/send_sms',
        success:function(data){
          if(data=='success'){
              alert('Your Sms has been sent');
              $('#mobile-nu').val('');
          }else{
              alert('Somethig went wrong');
          }
        },
        error:function(err){
          console.log(err);
        }
    });
  }
});
//==================================================================================================
</script>
    <script src="<?php echo base_url(); ?>/web_assets/appy/js/vendor/jquery-1.12.4.min.js"></script> 
    <script src="<?php echo base_url(); ?>/web_assets/appy/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>/web_assets/appy/js/scrollUp.min.js"></script> 
    <script src="<?php echo base_url(); ?>/web_assets/appy/js/main.js"></script>

</body>
</html>