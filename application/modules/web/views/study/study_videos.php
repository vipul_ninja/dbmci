<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
    <title>Document</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
  <style type="text/css">

  #example1 {border: 0px solid #42f4ad;padding: 45px;background-color: #51b5ef;border-bottom-right-radius: 100%;border-bottom-left-radius: 100%;}
  .button {background-color: #1276cc;border: none;color: white;padding: 15px;text-align: center;text-decoration: none;display: inline-block;font-size: 17px;margin: 4px 2px;cursor: pointer;font-weight: bold;margin-left: 45%;}
  .button4 {border-radius: 25px;}
.add {
    display: inline-block;
    width: 70px;
    height: 70px;
    margin: 6px;
    background-color: #fff;
    margin-left: 600px;
  }
  /*body {margin:2rem;}*/

.modal-dialog {
      max-width: 1100px;
      margin: 30px auto;
  }

.modal-body {
  position:relative;
  padding:0px;
}
.close {
  position:absolute;
  right:-30px;
  top:0;
  z-index:999;
  font-size:2rem;
  font-weight: normal;
  color:#fff;
  opacity:1;
}
  p.dashed {border-style: dashed;color: white;padding: 7px;margin-top:12px;    width: 62%;
    margin-left: 20%;}
  .video-sec{margin-bottom: 25px;}
  .h3-heading{margin-left: 40px;margin-top: 20px;text-align: center; font-weight: 500; color: black}
  .titile-dis{ width: 100%;word-break: break-all; padding: 15px;font-size: 16px;}
  </style> 
</head>
<body>
    <?php $this->load->view('../include/header');?>
        <br><br><br><br>
  <section>
    <?php 
      if(!empty($categoty_details)){
        $countervideo = 0;
        foreach($categoty_details['data'] as $category){
          if($cirruculam_id == $category['topic_id']){
            $title = $category['title'];
        foreach($category['file_meta'] as $file_meta){
          if($file_meta['file'] == 'video'){
            $countervideo++;
            }
          }
        }
      }    
    }
    ?>
<div id="example1">
  <span  class="rounded-circle add"><img src="<?php echo base_url('web_assets/img/videos.png'); ?>" style="width:35px;height: 35px;margin-left: 18px;margin-top: 15px;">
  </span> 
    <h4 align="center" style="color: white;font-weight: bold;">Videos</h4> 
    <h6 align="center" style="color: white;font-weight: bold;margin-top: 18px;">
      <img src="<?php echo base_url('web_assets/img/videos.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      <img src="<?php echo base_url('web_assets/img/videos.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      <img src="<?php echo base_url('web_assets/img/videos.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      <img src="<?php echo base_url('web_assets/img/videos.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      +7 others learning this
    </h6>
      <p class="dashed"><b><?=$title;?></b><b style="float: right;"><?=$countervideo;?> Videos</b></p>
</div> 
    <h3 class="h3-heading" style=""><b>Subject-wise Videos</b></h3>
    <br> 
    <div class="row" style="height:100px;margin-left: 41px;margin-right: 35px;" >
              <?php 
              if(!empty($categoty_details)){
                $countervideo = 1;
                  foreach($categoty_details['data'] as $category){
                   if($cirruculam_id == $category['topic_id']){
                     $title = $category['title'];
                    foreach($category['file_meta'] as $file_meta){
                     if($file_meta['file'] == 'video'){
                      $videoType = videoType($file_meta['link']);
                       if($videoType == 'youtube'){
                  	   $arrayurl = explode("watch?v=", $file_meta['link']);
                       $video = explode("&", $arrayurl[1]);
                  	   $url = $arrayurl[0]."embed/".$video[0];
                         }else{
                  	     $url = $file_meta['link'];
                        }
        			     
                    ?> 
                    <div class="col-3">
                      <div class="video-sec">
      	<a href="javascript:void(0);" class="video-btn" data-toggle="modal" data-id="<?=$file_meta['id'];?>" data-main="<?=$videoType;?>" data-src="<?=$url;?>" data-target="#myModal<?=$file_meta['id'];?>" style="text-decoration: none;">
        <div class="list-group-item list-group-item-action" style="padding: 0 0; width: 100%; height: auto;">
          <img src="<?php echo base_url('web_assets/img/play-button-overlay.png'); ?>" style="width:100%; height: auto;">
           <div class="titile-dis" style="">
              <span style="font-weight: bold;">
          	   <?php $titlecount = strlen($file_meta['title']);
          	     if($titlecount >20){
          	     	$dot= "...";
          	   }else{$dot = "";}
          	     echo ucfirst(substr($file_meta['title'],0,20)).$dot;?></span>
               <p><?php $descripcount = strlen(strip_tags($file_meta['description']));
          	     if($descripcount <=50){
          		    echo $str = $file_meta['description'];
                   	}else{$dots = "";
          		      $str = strip_tags($file_meta['description']);
          	   	echo substr($str,0,20)."...";
          	     } 
          		?> 
              </p>
            </div>
         </div>
       </a>
     </div>
        </div>
 
  <!-- model -->
		<div class="modal fade" id="myModal<?=$file_meta['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      
      <div class="modal-body">

       <button type="button" class="close" data-main="<?=$videoType?>" data-id="<?=$file_meta['id'];?>" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>        
        <!-- 16:9 aspect ratio -->
<div class="embed-responsive embed-responsive-16by9">

	<?php
	       if($videoType == 'mp4'){ ?>
	       	<video controls autoplay>
			    <source  id="video<?=$videoType.$file_meta['id'];?>"  src="" type="video/mp4">
			    <source  id="videoogg<?=$videoType.$file_meta['id'];?>" src="" type="video/ogg">
		</video>
	     <?php }else{ ?>
	     	 <iframe class="embed-responsive-item" src="" id="videos<?=$videoType.$file_meta['id'];?>"  allowscriptaccess="always"></iframe>
	   <?php  } ?>
 
  
</div>
        
        
      </div>

    </div>
  </div>
</div>  
<!-- end model -->
	<?php		}
		$countervideo++;  
		
		}
	}
  }    
}

function videoType($url) {
    if (strpos($url, 'youtube') > 0) {
        return 'youtube';
    } elseif (strpos($url, 'vimeo') > 0) {
        return 'vimeo';
    } elseif(strpos($url, 'mp4') > 0) {
        return 'mp4';
    }else{
    	return 'unknown';
    }
}
?>
  </div>


  
     
</section>
</br>
</br>
<section>

</section>
<?php $this->load->view('../include/footer');?>
<script type="text/javascript">
$(document).ready(function() {
	$('.video-btn').click(function() {
	     var videoSrc = $(this).data( "src" );
	     var videoType = $(this).data( "main" );
       var videoId = $(this).data( "id" );
      if(videoType == 'mp4'){
          $("#video"+videoType+videoId).attr('src',videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" );
          $("#videoogg"+videoType+videoId).attr('src',videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1");
     }else{
          $("#videos"+videoType+videoId).attr('src',videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" );
     }
  });

  $('.close').click(function() {
       var videoType = $(this).data( "main" );
       var videoId = $(this).data( "id" );
      if(videoType == 'mp4'){
          $("#video"+videoType+videoId).attr('src', '');
          $("#videoogg"+videoType+videoId).attr('src','');
     }else{
          $("#videos"+videoType+videoId).attr('src','');
     }
	});
});
</script>
</body>
</html>