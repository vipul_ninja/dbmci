    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $name['data']['title']?></title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
      <style type="text/css">
      body{
        /*background-image: linear-gradient( 135deg, #52E5E7 10%, #130CB7 100%)*/
        background-color: #3B95D6
  }
        .circle1 {
  width: 60px;
  height: 60px;
  border-radius: 50%;
  font-size: 40px;
  color: #000;
  /*line-height: 200px;*/
  text-align: center;
  background: #fff
}
 #example1 {border: 0px solid #42f4ad;padding: 45px;background-color: #42f4ad;border-bottom-right-radius: 100%;border-bottom-left-radius: 100%;}
 .add {
    display: inline-block;
    width: 70px;
    height: 70px;
    margin: 6px;
    background-color: #fff;
    margin-left: 600px;
  }
  .sub-sec{
    padding: 10px;
    /*display: grid;*/
    width: 19%;
    padding-left: 5%;
    margin-bottom: -20px;
  }
  .sub-sec-span{
    font-size: 17px;
        width: 35%;
  }
  .sub-sec span{
    color: 
  }
  .all-subject-ques{
list-style: none;
margin-left: -28px;
display: -webkit-box;

}
.all-subject-ques-scr{
list-style: none;
}
.list-group a li {
    color: #000!important;
    font-size: 17px;
    font-weight: 500
}
.list-group a{
  border: none;
  border-radius: 8px;
}
.image{
  width: 5%;
    margin-left: 46.5%;
}
.right-fa{
      color: black;
    position: absolute;
    right: 2%;
    top: 34%;
    font-size: 31px;
}
 .titile-dis{ width: 100%;word-break: break-all; padding: 15px;font-size: 16px;}
 .computer-awareness a{
    margin-bottom: 20px;
 }
    </style>
</head>
<body>
  <div class="main-box">
        <?php $this->load->view('../include/header');?>
        
        <br><br><br><br><br>
  <div class="container">
    <div class="page-flow">
            <ul class="breadcrumbs">
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/Feeds">Home</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/Study">Study</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item active">
            <span><?php echo $name['data']['title']?></span>
          </li>
        </ul>
      </div>
    </div>
        <?php if($name['data']['cover_image'] != ""){
                $cover_image = $name['data']['cover_image'];
              }else{
                $cover_image = base_url('web_assets/img/makemyexam.png');
              }
        ?>
        <img class="image" src="<?php echo $cover_image; ?>" >
  <?php
        if(!empty($categoty_details)){ ?>
            <h2 style="text-align: center; font-weight: 500; color: white"><?php echo $name['data']['title']?></h2><br>
        <?php 
    ?>

    <div class="list-group computer-awareness" style="height: 100%"> 
        <?php 
        $i = 0;
        //echo "<pre>"; print_r($categoty_details['data']);die;
        foreach($categoty_details['data'] as $category) {

        ?> 
        <!-- border-radius: 50%; -->
         
            <a style="text-decoration: none;" href="<?php echo site_url('/web/Study/particularCourse/'.$name['data']['id']."/".$category['topic_id']); ?>">
              <div class="list-group-item list-group-item-action" style="background-color:; font-weight: bold; color: white;font-size: 20px;">
              <i class="fa fa-chevron-right right-fa"></i>
              <span style="font-size: 22px;color: white;">
                
                <?php echo $category['title']; ?>
                 
              </span>
              <?php 
              $counterpdf = 0;$counterconcept = 0;$countervideo = 0;$counterquiz = 0;
              foreach($category['file_meta'] as $file_meta){ 
                if($file_meta['file'] == 'pdf'){
                  $counterpdf++;
                }else if($file_meta['file'] == 'concept'){ 
                  $counterconcept++;
                }else if($file_meta['file'] == 'video'){
                  $countervideo++;
                }else if($file_meta['file'] == 'test'){
                  $counterquiz++;
                }
              }?>
              <!-- <div class="sub-sec">
                <span class="sub-sec-span"> <?=$countervideo; ?><span class="float-right">video</span></span> 
                <span  class="sub-sec-span"> <?=$counterconcept; ?><span class="float-right" > Concept </span></span> 
                <span class="sub-sec-span"> <?=$counterquiz; ?><span class="float-right" > Quizes </span></span> 
                <span class="sub-sec-span"> <?=$counterpdf; ?><span class="float-right" > Questions </span></span> 
              </div> -->
                 <div class="row sub-sec">
                    <div class="col-8">
                    <ul class="all-subject-ques">
                    <li class="sub-sec-span"><?=$countervideo; ?></li><li>video,</li>&nbsp;
                    <li class="sub-sec-span"><?=$counterconcept; ?></li> <li>Concept,</li>&nbsp;
                    <li class="sub-sec-span"><?=$counterquiz; ?></li><li> Quizes,</li>&nbsp;
                    <li class="sub-sec-span"><?=$counterpdf; ?></li> <li>Questions</li>
                    </ul>
                    </div>
                    <!-- <div class="col-4">
                    <ul class="all-subject-ques-scr">
                    
                    
                    
                    
                    </ul>
                    </div> -->
                    </div>
                     

            </div></a> 
        <?php $i++; }?>
     
  </div>
    <?php }?> 
      
   </div>        
<?php $this->load->view('../include/footer');?>
</body>
</html>