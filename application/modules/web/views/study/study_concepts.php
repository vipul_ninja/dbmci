<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
    <title>Document</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
  <style type="text/css">

  #example1 {border: 0px solid #42f4ad;padding: 45px;background-color: #51b5ef;border-bottom-right-radius: 100%;border-bottom-left-radius: 100%;}
  .button {background-color: #1276cc;border: none;color: white;padding: 15px;text-align: center;text-decoration: none;display: inline-block;font-size: 17px;margin: 4px 2px;cursor: pointer;font-weight: bold;margin-left: 45%;}
  .button4 {border-radius: 25px;}
.add {
    display: inline-block;
    width: 70px;
    height: 70px;
    margin: 6px;
    background-color: #fff;
    margin-left: 600px;
  }
  .content-height{height:370px; margin-left: 40px;margin-right: 40px; overflow-y: auto;overflow-x: hidden; 
    color: black;
    font-family: unset;
    text-align: justify;}
  .div-inner{border: 1px solid #c6c4c4;
    margin-top: 15px; border-radius: 4px}
    .content-height p{font-size: 16px;}
    .h4-heading{margin-left: 40px;margin-top: 20px; font-weight: 500; font-size: 26px; color: black}
    .h3-heading{margin-left: 40px;margin-top: 20px;text-align: center; font-weight: 500; color: black}
  </style> 
</head>
<body>
   <?php $this->load->view('../include/header');?>
        <br><br><br><br>
  <section>
<div id="example1">
    <span  class="rounded-circle add"><img src="<?php echo base_url('web_assets/img/concepts.png'); ?>" style="width:35px;height: 35px;margin-left: 18px;margin-top: 15px;"></span>
    <h4 align="center" style="color: white;font-weight: bold;">Concepts</h4>
  <h6 align="center" style="color: white;font-weight: bold;margin-top: 18px;">
      <img src="<?php echo base_url('web_assets/img/concepts.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      <img src="<?php echo base_url('web_assets/img/concepts.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      <img src="<?php echo base_url('web_assets/img/concepts.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      <img src="<?php echo base_url('web_assets/img/concepts.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
    +7 others learning this
  </h6>
</div>
<h3 class="h3-heading" style=""><b>Our Concepts</b></h3>
<div class="container-fluid">
<div class="row">
<?php 
if(!empty($categoty_details)){
  foreach($categoty_details['data'] as $category){
    if($cirruculam_id == $category['topic_id']){
      foreach($category['file_meta'] as $file_meta){
        if($file_meta['file'] == 'concept'){ 
          $title = $file_meta['title']; ?>
          <div class="col-4"><div class="div-inner">
      		<h4 class="h4-heading" style=""><b><?=$file_meta['title'];?></b></h4> 
  			<div class=" content-height"> 
     		 <?= $file_meta['description'];?>   
  			</div>
  		</div>
	</div>
    <?php     
        }
      }
    }
  }    
}
 // echo site_url('Study/particularCategory/'); 
?>
</div>
</div>  
</section>
</br>
</br>
<section>

</section>
<?php $this->load->view('../include/footer');?>
</body>
</html>