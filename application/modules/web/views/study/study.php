    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Study</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
  <style>
  body{
  	    background-color: #3B95D6;
  }
  	.list-group img {
    width: 20%;
    height: auto;
    margin-left: 43%;
    margin-top: 15px;
    margin-bottom: 13px;
}
	.list-group span{
	    color: #000;
	    margin-left: 25%; 
	    font-size: 19px;
	    margin-top: 4%;
	    font-weight: 500
	}
	.list-group a { 
    font-weight: bold;
    color: #189917!important;
    font-size: 20px;
    text-align: center;
    border: none;  
    /*border-radius-: 8px!important; */
    margin-top: 8%;
    background-color: #f1f1f1 !important;
    border-top: 8px !important;
    }
	.colm{
		background-color: #fff;
		border-radius: 8px;
		margin-bottom: 15px;
		/*border: 1px solid black;*/
	}
	.list-group a:hover {
		color: #2EB9F7 !important;
	} 
  </style>
</head>
<body>
  <div class="main-box">
        <?php $this->load->view('../include/header');?>
      
  <br>  <br>  <br>  <br>  <br> 
  <div class="container">
    <div class="page-flow">
            <ul class="breadcrumbs">
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/Feeds">Home</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item active">
            <span>Study</span>
          </li>
        </ul>
      </div>
    <br>
      <div class="col col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 filter-by-feeds" style="">
        <div class="w-select w-select1">
          <span class=" title1">Filter By:</span>
            <select name="feed" id="feed" class="select-content">
              <?php if(!empty($category_details)){ 
                foreach ($category_details['data']['main_category'] as $cat) {?>
              <option value="all"><?php echo $cat['text'];?></option>  
              <?php } }?>
            </select>
        </div>
      </div> 
    </div>
  <div class="container" style="width: 100%;"> 
  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php if(!empty($slider_images)){
            $i = 0;
            foreach($slider_images['data'] as $sliders){
                if($i == 0){
                    $class = 'active';
                }else{
                    $class = '';
                }
        ?>

      <div class="item <?php echo $class; ?> slider-img">
        <img src="<?php echo $sliders['image_url']; ?>" alt="">
      </div>

    <?php $i++;  } }?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
</br>
<?php //echo "<pre>";
      //print_r($category_data);exit;?>
       <?php if(!empty($category_data)){ 

        ?>
    <div class="list-group list-group1">
    	<div class="row"> 
       <?php foreach($category_data['data'] as $category) {
        ?>
        
        <div class="col-md-4 col-xs-12"> 
        	<div class="colm"> 
              <?php if($category['cover_image'] != ""){
                      $cover_image = $category['cover_image'];
                    }else{
                      $cover_image = base_url('web_assets/img/makemyexam.png');
                    }
              ?>
          		<img src="<?php echo $cover_image; ?>" style="background-color: <?=$category['c_code'];?>;"> 
          		<span><?php echo $category['title']; ?></span> 
              <div style="margin-left: 85px; color:grey;font-size: 14px;"><?php echo $category['segment_information']; ?></div>
          	 
          		<a class="list-group-item list-group-item-action" href="<?php echo site_url('/web/Study/particularCategory/'.$category['id']); ?>"> Start Learning 
          		</a> 
          	 
        </div>
    	</div>
       <?php } ?> 
       </div>
    </div>
<?php  }  ?>
</div> 
<?php  $this->load->view('../include/footer'); ?>           

</body>
</html>


           	
          		