<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Quiz</title>

        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
        <!-- Main Font -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/webfontloader.min.js"></script>
         <script src="<?php echo base_url(); ?>web_assets/js/quiz.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/ssc.js"></script>
      <script>
            WebFont.load({
                google: {
                    families: ['Roboto:300,400,500,700:latin']
                }
            });
        </script>
  <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_assets/Bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_assets/Bootstrap/dist/css/bootstrap-grid.css">

        <!-- Main Styles CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_assets/css/submit_popup.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_assets/css/main.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_assets/css/fonts.min.css">
    </head>
   <body>

        <!-- Header-BP -->
        <header class="header bg-blue" id="site-header">
            <div class="page-title test-title text-center">
                <h6>Make My Exam</h6>
            </div>
            <div class="header-content-wrapper"> </div>
        </header>

        <!-- ... end Header-BP -->

        <!-- Responsive Header-BP -->

        <header class="header header-responsive bg-blue" id="site-header-responsive"> <a href="javascript:void(0);" class="logo">
                <div class="img-wrap"> <img src="<?php echo base_url(); ?>web_assets/img/logo-colored-small.png" alt="Olympus"> </div>
                <div class="title-block">
                    <h6 class="logo-title">IBT</h6>
                    <div class="sub-title">Online Exam</div>
                </div>
            </a> </header>
<!-- <?php //echo "<pre>"; print_r($test_data); echo  die;?> -->
        <!-- ... end Responsive Header-BP -->
        <div class="header-spacer"></div>

        <!--======================================INSTRUCTION===================================================-->
        <div class="tab-content" id="instruct">
            <div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">

                <div class="question-box content" id="">

                    <label>
                        <h2>Instructions</h2>
                    </label>
                    <br>
                    <br>
                    <label><strong>1.</strong> You must only use paper handed out at the examination venue.</label>
                    <br>
                    <label><strong>2.</strong> Your paper must be written with a black or blue ballpoint pen.</label>
                    <br>
                    <label><strong>3.</strong> At digital examinations, you write your answer directly in the program.</label>
                    <br>
                    <label><strong>4.</strong> You may work on your exam paper until the time allotted for the examination expires.</label>
                    <br>
                    <label><strong>5.</strong> At digital examinations, you write your answer directly in the program.</label>
                    <br>
                    <label><strong>6.</strong> You may work on your exam paper until the time allotted for the examination expires.</label>
                    <br>
                    <br>
                    <button type="button" class="btn btn-primary" id="start" onclick="startTest_Timer();">Start Test</button>
                </div>
            </div>
        </div>
        <!--===================================END INSTRUCTION===================================================-->


        <!-- ... start test tabs -->
        <div style="display:none;" id="form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col col-xl-9 col-lg-8 col-md-12 col-sm-12 col-12">
                        <div class="ui-block">
                            <div class="test-box-top bg-grey"> <span class="title">Text Size</span> <span class="text-btn"> <span> <a href="javascript:void(0);" class="btn btn-white btn-control" id="sizePlus" onclick="changeFontSize(this.id)">A+</a></span> <span> <a href="javascript:void(0);" class="btn btn-white btn-control" id="sizeMinus" onclick="changeFontSize(this.id)">A-</a></span> </span> </div>
                            <!-- text Size Function -->


                            <div class="ui-block-content minmax" id="minmax" onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false">
                                <p class="text-right">(Mark:
                                    <?php echo $test_data['basic_info']['total_marks'] ?>) (Negative Mark/s -
                                    <?php echo $test_data['basic_info']['negative_marking'] ?>)</p>
                               
                    <span id="total" totalq="<?php echo $total = $test_data['basic_info']['total_questions']; ?>"></span>

                                <?php
                                static $index;
                                $test_data1 = $test_data['question_bank'];

                                $count = count($test_data1);
                                $que_id = array();
                                $data = array();

                                for ($i = 0; $i < $count; $i++) {
                                    $index = $i + 1;
                                    if ($i == 0) {
                                        $style = 'block';
                                    } else {
                                        $style = 'none';
                                    }
                                    ?>
                                    <span class="local_data" test_id=<?php echo $test_data['basic_info']['id'];  ?>
                                     question_id= <?php echo  $test_data1[$i]['id']; ?>  ></span>

                                    <div class="question-box content" id="content-<?php echo $i + 1; ?>" style="display: <?php echo $style; ?>;">
                                        <div><span class="quenmbr">Q
                                                <?php echo $i + 1; ?>.</span>
                                            <?php echo $test_data1[$i]['question'] ?>
                                        </div>
                                        <div class="options"> <br>
                                            <div class="radio">
                                                <label id="color1<?php echo $i + 1; ?>" class='cls'  seq="<?php echo $i + 1; ?>" >
                                                    <input type="radio" class="radio<?php 
                                                    echo ($i + 1) . " ";
                                                    if ($count == ($i + 1)) {
                                                        echo 'last_que';
                                                    }
                                                    ?>" name="optionsRadios<?php echo $i + 1; ?>" value="<?php echo $test_data1[$i]['option_1'] ?>" id="1" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                                                    A.
                                                    <?php echo $test_data1[$i]['option_1'] ?> </label>
                                            </div>
                                            <div class="radio">
                                                <label id="color2<?php echo $i + 1; ?>" class='cls' seq="<?php echo $i + 1; ?>" >
                                                    <input type="radio" class="radio<?php
                                                    echo ($i + 1) . " ";
                                                    if ($count == ($i + 1)) {
                                                        echo 'last_que';
                                                    }
                                                    ?>" name="optionsRadios<?php echo $i + 1; ?>" value="<?php echo $test_data1[$i]['option_2'] ?>" id="2" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                                                    B.
                                                    <?php echo $test_data1[$i]['option_2'] ?>
                                                 </label>
                                            </div>
                                            <div class="radio">
                                                <label id="color3<?php echo $i + 1; ?>" class='cls' seq="<?php echo $i + 1; ?>" >
                                                    <input type="radio" class="radio<?php
                                                    echo ($i + 1) . " ";
                                                    if ($count == ($i + 1)) {
                                                        echo 'last_que';
                                                    }
                                                    ?>" name="optionsRadios<?php echo $i + 1; ?>"  value="<?php echo $test_data1[$i]['option_3'] ?>" id="3" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                                                    C.
                                                    <?php echo $test_data1[$i]['option_3'] ?> </label>
                                            </div>
                                            <div class="radio">
                                                <label id="color4<?php echo $i + 1; ?>" class='cls' seq="<?php echo $i + 1; ?>" >
                                                    <input type="radio" class="radio<?php
                                                    echo ($i + 1) . " ";
                                                    if ($count == ($i + 1)) {
                                                        echo 'last_que';
                                                    }
                                                    ?> " name="optionsRadios<?php echo $i + 1; ?>" value="<?php echo $test_data1[$i]['option_4'] ?>" id="4" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                                                    D.
                                                    <?php echo $test_data1[$i]['option_4'] ?> </label>
                                            </div>
                                        </div>

                                    </div>
                                
                                    <?php
                                    $que_id[$i] = $test_data1[$i]['id'];
                                    $data[] = $que_id[$i];
                                }
                                ?>

                               
                            </div>

                        </div>
                    </div>
                    <aside class="col col-xl-3 order-xl-3 col-lg-3 order-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="ui-block">
                            <div class="student-test-test_datas">
                                <img src="<?php echo base_url(); ?>web_assets/img/avatar1.jpg" alt="" />
                                <div class="student-test_datas">
                                    <span class="notification-date"><time class="entry-date updated bold" datetime="2004-07-24T18:18">Time Left: <span id="time">20:00 </span></time></span>
                                    <input type="hidden" id="time_val"/><br>
                                    <span class="student-roll"><span>Name:</span><span> Ajay Kumar</span></span>

                                    <span class="student-roll"><span>Roll No:</span><span> 123456789</span></span>
                                </div>



                                <div>
                                </div>

                            </div>
                            <!-- W-Activity-Feed -->

                            <!-- .. end W-Activity-Feed -->
                        </div>
                        <div class="ui-block">
                            <div class="ui-block-title">
                                <h6 class="title">Question Palette</h6>
                            </div>

                            <!-- W-Activity-Feed -->

                            <div class="question-status">
                                <span id="test_id" val1= "<?php echo $test_data['basic_info']['id']; ?>" ></span>
                                <ul>
                                    <?php
                                    $total = $test_data['basic_info']['total_questions'];
                                    for ($i = 0; $i < $total; $i++) {
                                        ?>
                                        <li><a id="optionsRadios<?php echo $i + 1; ?>" class="common tagged current1" current ="1" onclick="change_que('<?php echo $i + 1; ?>')" >Q<?= $i + 1; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>

                            <!-- .. end W-Activity-Feed -->

                        </div>
                    </aside>
                </div>
                <!--
                        <div class="question-submit fixed-bottom">
                            <div class="row">
                                <div class="col col-md-6">
                                    <div> <a href="javascript:void(0);"  class="btn btn-blue btn-md-2 next">Next</a> </div>
                                </div>
                
                            </div>
                        </div>
                -->
            </div>


            <!--================================== Result Popup Modal =======================================-->

            <div id="myModal" class="modal" style="z-index:99;">

                <!-- Modal content -->
                <div class="modal-content">
        <!--            <span class="close">&times;</span>-->

                    <div style="text-align: center;"><h3>Test Summary</h3></div>
                    <table border="1" style="text-align:center;cellpadding:10px;padding:10px;font-size:20px;width:100%;">
                        <tr style="background:grey;color:white;">
                        <b>
                            <th>Title</th>
                            <th>Correct Answer</th>
                            <th>Marks</th>
                            <th>Time Spent</th>
                            <th>User Rank</th>
                        </b>
                        </tr>
                        <tr id="tr_id">

                        </tr>


                    </table>
                    <div style="text-align: center; margin-top:10px; "><h5><a id="show_res"  href="<?php echo site_url(); ?>/web/My_test/result_question/53/">View Result</a></h5></div>
                    <div style="text-align: center; margin-top:10px; "><h5><a id="leader_board"   href="<?php echo site_url(); ?>/web/DailyDose/basic_rank_result/">LeaderBoard</a></h5></div>
                </div>

            </div>
            <!--==================================End Result Popup Modal =======================================-->

        </div>

        <!-- ... end start test tabs -->
        <footer class="fixed-bottom"></footer>

        <script>

            function startTest_Timer() {
                var duration = 60 * 20,
                        display = document.querySelector('#time');
                var timer = duration,
                        minutes, seconds;
                var s = 0;
               var myVar = setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;

                    display.textContent = minutes + ":" + seconds;
                    s++;
                    $("#time_val").val(s);

                    if (--timer < 0) {
                        clearInterval(myVar)
                        result();
                    }
                }, 1000);
            }


            // =============================Toggle Screen==============================================================
            $(document).ready(function () {
  
        var array=[];
        var flag =0;
        function settestid(test_ids){

            var st = localStorage.getItem("eTvZ(5@9ETs");
            
            if(st){
            var decodestring = atob(st);
            var ss = JSON.parse(decodestring);
          
            for(let i=0;i<ss.length;i++){
                  if(test_ids == ss[i]){  //check whether two same id
                   flag=1; break;
                   }
            }

            if(flag!=1){
                    flag=0;
                    
                    var finalArray = ss.map(function (obj) {
                    return obj.test_id;
                    });
                  
                    ss.push(test_ids);
                    var string = JSON.stringify(ss);
                    var encodedString = btoa(string);
                    localStorage.setItem("eTvZ(5@9ETs", encodedString);
                   return;
                }
            }

            else{
             
            // var myArray1 ={test_id: test_ids};

            array.push(test_ids);
            var string = JSON.stringify(array);
            var encodedString = btoa(string);
            localStorage.setItem("eTvZ(5@9ETs", encodedString);
            }
            var st = localStorage.getItem("eTvZ(5@9ETs");
            var decodestring = atob(st);
            var ss = JSON.parse(decodestring);
           
            var finalArray = ss.map(function (obj) {
            return obj.test_id;
            });


        }
        
        settestid(<?php echo $test_data['basic_info']['id']; ?>);
        
  //==========================before close quiz========================================================
                window.onbeforeunload = function (e) {
                  var e = e || window.event;

                  //IE & Firefox
                  if (e) {
                    e.returnValue = 'Are you sure?';
                  }

                  // For Safari
                  return 'Are you sure?';
                };
  //==========================END before close quiz========================================================       
            
               $('#start').click(function (e) {

                    $('#form').css({
                        "display": "block"
                    });
                    $('#instruct').css({
                        "display": "none"
                    });

                    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
                            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                        if (document.documentElement.requestFullScreen) {
                            document.documentElement.requestFullScreen();
                        } else if (document.documentElement.mozRequestFullScreen) {
                            document.documentElement.mozRequestFullScreen();
                        } else if (document.documentElement.webkitRequestFullScreen) {
                            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                        }
                        if (e.requestFullscreen) {
                            e.requestFullscreen();
                        } else if (e.mozRequestFullScreen) {
                            e.mozRequestFullScreen();
                        } else if (e.webkitRequestFullscreen) {
                            e.webkitRequestFullscreen();
                        } else if (e.msRequestFullscreen) {
                            e.msRequestFullscreen();
                        } else {
                            if (document.cancelFullScreen) {
                                //document.cancelFullScreen();  
                            } else if (document.mozCancelFullScreen) {
                                document.mozCancelFullScreen();
                            } else if (document.webkitCancelFullScreen) {
                                // document.webkitCancelFullScreen();  
                            }
                        }
                    }
                });
            });
            // ===========================End Toggle Screen==============================================================        

            function result()
            {
                 var modal = document.getElementById('myModal');
                var btn = document.getElementsByClassName("last_que");

                var total = <?= $count; ?>;
                var radioValue;
                var radio_val = [];
                var dump = [];
                for (let i = 0; i < total; i++) {
                    radioValue = $("input[name='optionsRadios" + i + "']:checked").attr('id');
                    if (radioValue == undefined) {
                        radio_val[i] = '';
                    } else {
                        radio_val[i] = radioValue;
                    }
                }
                for (let i = 0; i < total; i++) {
                    dump[i] = radio_val[i + 1];

                }

                var data = <?php echo json_encode($data); ?>;

                var dump_val = [];
                for (let k = 0; k < total; k++) {
                    if (dump[k] == undefined) {
                        dump[k] = '';
                    }
                    dump_val[k] = {
                        "question_id": data[k],
                        "answer": dump[k]
                    };

                }
                //               var res_id= $("#show_res").attr('href');
                ////////////////
                //              var t = $("#time_val").val();
                ///////////////////


                var Url = "<?php echo site_url(); ?>";
                var test_segment_id = $('#test_id').attr('val1');
                var res_id = $("#show_res").attr('href');//result id for Result Url
                var t = $("#time_val").val();
                var time_spent=parseInt(t);
                $('#local_data').attr('time',parseInt(t));//set value to localstorage
                //for leader board
                var leader_url = $("#leader_board").attr('href');//result id for Result Url

                $.ajax({

                    data: {test_segment_id: test_segment_id,
                        time_spent: time_spent,
                        question_dump: dump_val},
                    type: 'post',
                    dataType: 'json',
                    url: Url + '/web/DailyDose/ajax_attempt_result',
                    success: function (data) {

                        $("#tr_id").html(data.result);
                        $("#show_res").attr('href', res_id + data.u_id);//setting controller url
                        $("#leader_board").attr('href', leader_url + data.u_id);//setting controller url

                    }, error: function (data) {
                        console.log("error" + data);
                    }
                });
                modal.style.display = "block";
            }
            $(".last_que").click(function () {
               result();
            });


           
        </script>

        <!-- JS Scripts -->

        <script src="<?php echo base_url(); ?>web_assets/js/jquery-3.2.1.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/jquery.appear.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/jquery.mousewheel.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/perfect-scrollbar.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/jquery.matchHeight.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/svgxuse.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/imagesloaded.pkgd.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/Headroom.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/velocity.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/ScrollMagic.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/jquery.waypoints.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/jquery.countTo.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/material.min.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/bootstrap-select.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/smooth-scroll.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/selectize.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/swiper.jquery.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/moment.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/daterangepicker.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/simplecalendar.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/fullcalendar.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/isotope.pkgd.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/ajax-pagination.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/Chart.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/chartjs-plugin-deferred.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/circle-progress.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/loader.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/run-chart.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/jquery.magnific-popup.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/jquery.gifplayer.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/mediaelement-and-player.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/mediaelement-playlist-plugin.min.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/base-init.js"></script>

        <script defer src="<?php echo base_url(); ?>web_assets/fonts/fontawesome-all.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/Bootstrap/dist/js/bootstrap.bundle.js"></script>
    </body>

</html>
