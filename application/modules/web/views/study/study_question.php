    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Study Question</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<meta charset="UTF-8">
    <title>Document</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">

  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
      <style type="text/css">
body  {
    /*background-image: url("<?php echo base_url('web_assets/img/question.png');?>");*/
}
.bar {
  height: 81%;
  background: white;
  border-top: 3px solid #ccc;
  border-bottom: 3px solid #ccc;
  margin-top: 2.0em;
}
.media img{
  width: 25%;
}
.h1-heading{font-weight: 500;  color: black; margin-top: 15px;}
 #example1 {border: 0px solid #42f4ad;padding: 45px;background-color: #51b5ef;border-bottom-right-radius: 100%;border-bottom-left-radius: 100%;}
 .add {
    display: inline-block;
    width: 70px;
    height: 70px;
    margin: 6px;
    background-color: #fff;
    margin-left: 600px;
}
</style>
</head>
<body>
  <?php $this->load->view('../include/header');?>
        <br><br>

<div id="example1">
  <span  class="rounded-circle add"><img src="<?php echo base_url('web_assets/img/questions.png'); ?>" style="width:35px;height: 35px;margin-left: 18px;margin-top: 15px;">
  </span> 
    <h4 align="center" style="color: white;font-weight: bold;">Questions</h4> 
    <h6 align="center" style="color: white;font-weight: bold;margin-top: 18px;">
      <img src="<?php echo base_url('web_assets/img/questions.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      <img src="<?php echo base_url('web_assets/img/questions.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      <img src="<?php echo base_url('web_assets/img/questions.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      <img src="<?php echo base_url('web_assets/img/questions.png'); ?>" style="width:20px;height: 20px;margin-left: 1px;">
      +7 others learning this
    </h6>
       
</div>
<h3 align="center" class="h1-heading"><b>Questions</b></h3>
<div class=" container-fluid">
<div class="row">
<?php 
if(!empty($categoty_details)){
  foreach($categoty_details['data'] as $category){
    if($cirruculam_id == $category['topic_id']){
      $title = $category['title'];
      foreach($category['file_meta'] as $file_meta){
        if($file_meta['file'] == 'pdf'){ ?>
          <div class="col-4">
          <a class="list-group-item list-group-item-action bar" style="text-decoration: none;" href="<?=$file_meta['link']; ?>" target="_blank">
            <div class="media">
             <img src="<?php echo base_url('web_assets/img/pdf_img.jpg'); ?>" >
              <div class="media-body">
                <h5 class="mt-0" style="color: #000; margin-left: 10px;"><?=ucfirst($file_meta['title']); ?></h5>
                <h6 style=" margin-left: 10px;"><?=ucfirst($file_meta['description']); ?></h6>
              </div>
            </div>
          </a>
        </div>
<?php   }
      }
    }
  }    
}
?>
</div>
</div>
<?php $this->load->view('../include/footer');?>
</body>
</html>