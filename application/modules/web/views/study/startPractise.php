    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Learn Course</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>web_assets/js/quiz.js"></script>
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
    <script type="text/javascript">
      
      var flag =0;
function gettestid(test_ids){
            var st = localStorage.getItem("eTvZ(5@9ETs");
            if(st){

            var decodestring = atob(st);
            var ss = JSON.parse(decodestring);
             for(let i=0;i<ss.length;i++){

              if(test_ids == ss[i]){  //check whether two same id
               flag=1; break;
               }
            }

              if(flag==1){
               flag=0;
               // alert("succe");
               return test_ids;
               }
               else
               {
                 return 0;
               }
             }else{ return 0;}
             
            
            
             }
    </script>
</head>
<body>
  <?php
         if(!empty($categoty_details)){ 
         ?>


<div class="list-group" style="height: 100%">

  <?php foreach($categoty_details['data'] as $category){ 
          if($cirruculam_id == $category['topic_id']){ ?>
          <h2 style="text-align: center;"><?php echo $category['title'];?></h2>
          <?php  foreach($category['file_meta'] as $files){
              if($files['file'] == 'test'){
       ?>
  
    <div class="list-group-item list-group-item-action" style="background-color:#f4f4f4; font-weight: bold;font-size: 20px;">
      <span style="width: 100px; height: 100px;font-size: 25px;color: #000;background: #fff;text-align: center;"><?=$files['title'];?>
      </span>
      <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal<?=$files['link'];?>" class="start" test_id="<?=$files['link'];?>"><span style="float:right;font-size: 20px; " id="colors<?php echo $files['link']?>" class="btn btn-success"> <img src="<?php echo base_url('web_assets/img/quizes.png'); ?>" style="width:35px;height: 35px;"> </br><?php echo '<script type="text/javascript">',
         'var a = gettestid('.$files['link'].');
         if(a=='.$files['link'].'){ document.write("Resume Quiz");$("#colors'.$files['link'].'").removeAttr("class");$("#colors'.$files['link'].'").attr("class", "btn btn-info"); }else{ document.write("Start Quiz");}',
        '</script>';
        ?></span></a>
      
      <div style="font-size: 15px;margin-top: 20px;color: #2b6fbc;"><i class="glyphicon glyphicon-star" style="font-size: 15px;color: #f48342;"></i> 15 Coins  &nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-asterisk" style="font-size: 15px;color: #ada7a4;"></i>  &nbsp;&nbsp;&nbsp;15+ Attempts</div>
    </div>
    </br>
    <!-- The Modal -->
  <div class="modal" id="myModal<?=$files['link'];?>">
    <div class="modal-dialog">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal"><span style="color: red;" class="glyphicon glyphicon-remove"></span></button>
      <h4 class="modal-header" align="center" id="testname<?=$files['link'];?>"></h4>

        <div class="modal-body">
          <div><span>Number of Questions</span><span style="float: right;" id="nq<?=$files['link'];?>"></span></div>
          </br>
          <div><span>Time (in Minutes)</span><span style="float: right;" id="t<?=$files['link'];?>"></span></div>
          </br>
          <div><span>Total Marks</span><span style="float: right;" id="tm<?=$files['link'];?>"></span></div>
          </br>
          <div><span>Marks for Correct Answer</span><span style="float: right;" id="ca<?=$files['link'];?>"></span></div>
          </br>
          <div><span>Marks for Wrong Answer</span><span style="float: right;" id="wa<?=$files['link'];?>"></span></div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <a style="margin-right: 40%" onClick="window.open('<?=site_url();?>/web/Study/quiz_by_id/<?php echo $files['link'];?>', '_blank')" href="javascript:void(0);" class="btn btn-info" data-dismiss="modal">Get Started</a>
        </div>
        
      </div>
    </div>
  </div>

<?php } } } } ?>
</div>
  <?php } ?>
  
<?php $this->load->view('../include/footer');?>
</body>
<script type="text/javascript">
  $(document).ready(function(){
    window.base_url = '<?php echo site_url();?>';
    $(document).on('click', '.start', function(){
      var id = $(this).attr('test_id');
      $.ajax({
        dataType: 'JSON',
        url : base_url+'/web/Study/testSeriesdetails',
        type :'POST',
        data :  {
          tid: $(this).attr('test_id')
        },
        success: function(data){
          if(data.status == true){
            $('#testname'+id).html(data.data.basic_info.test_series_name);
            $('#nq'+id).html(data.data.basic_info.total_questions);
            $('#t'+id).html(data.data.basic_info.time_in_mins);
            $('#tm'+id).html(data.data.basic_info.total_marks);
            $('#ca'+id).html(data.data.basic_info.marks_per_question);
            $('#wa'+id).html(data.data.basic_info.negative_marking);
          }else{
            alert('No data found.');
          } 
        },
        error: function(jqXHR,textStatus,errorThrown){
          $("#errorvehicletype").addClass("text-red").text('Some issue found please check and try again.');
        }
      });
    });  
  });
  
</script>
</html>