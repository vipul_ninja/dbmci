<div class="modal fade" id="update-header" tabindex="-1" role="dialog" aria-labelledby="share-post" aria-hidden="true" style="overflow-y: auto;">
    <div class="modal-dialog window-popup edit-widget edit-widget-twitter comment_popup" role="document">
        <div class="modal-content model-custom" >
            <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon">
                <use xlink:href="<?php echo base_url();?>/web_assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                </svg>
            </a>
             <form id="regForm"> 
              <!-- One "tab" for each step in the form: -->
              <div class="tab">
                <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="post__author author vcard inline-items inline-items1">
                                    <img src="<?= $this->session->pro_img; ?>" alt="author">
                                    <div class="author-date">
                                        <a href="javascript:void(0)" class="h6 post__author-name fn" >Create a post</a>
                                    </div>
                                </div>
                            </div>
                            <ul class="nav nav-pills post-nav-link" role="tablist" id="configuration_sidebar_content">
                                <li class="nav-item">
                                    <a class="nav-link active" data-id="1" data-text="text" data-toggle="pill" href="#home" href="javascript:void(0);">
                                        <div class="create-tab-for-post-type">
                                            <div class="create-tab-for-post-type-icon inline-items">
                                                <img src="<?php echo base_url();?>/web_assets/img/post_query.png" alt="">
                                                <h4>Post a Query</h4>
                                            </div>                      
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" data-id="2" data-text="mcq" data-toggle="pill" href="#menu1">
                                        <div class="create-tab-for-post-type">
                                            <div class="create-tab-for-post-type-icon inline-items">
                                                <img src="<?php echo base_url();?>/web_assets/img/share_mcq.png" alt="">
                                                <h4>Share MCQ</h4>
                                            </div>                    
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" data-id="3" data-text="text" data-toggle="pill" href="#menu2">
                                        <div class="create-tab-for-post-type">
                                            <div class="create-tab-for-post-type-icon inline-items">
                                                <img src="<?php echo base_url();?>/web_assets/img/share_info.png" alt="">
                                                <h4>Share Info</h4>
                                            </div>                    
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" data-id="4" data-text="text" data-toggle="pill" href="#Share">
                                        <div class="create-tab-for-post-type">
                                            <div class="create-tab-for-post-type-icon inline-items">
                                                <img src="<?php echo base_url();?>/web_assets/img/share_examexperience.png" alt="">
                                                <h4>Share Exam Experience</h4>
                                            </div>                    
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" data-id="5" data-text="video" data-toggle="pill" href="#UploadV">
                                        <div class="create-tab-for-post-type">
                                            <div class="create-tab-for-post-type-icon inline-items">
                                                <img src="<?php echo base_url();?>/web_assets/img/upload_videos.png" alt="">
                                                <h4>Upload Videos</h4>
                                            </div>                    
                                        </div>
                                    </a>
                                </li>  
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content feed-tab-new">
                                <div id="home" class="container tab-pane active"><br>
                                    <h4 class="h4-h4">Key Note</h4>
                                    <ul class="key-note">
                                        <li>Don't write lengthy questions, write to the point.</li>
                                        <li>Post complete question to get desired answers.</li>
                                        <li>In case of posting question through image, make sure image must be cleared and not blurred.</li>
                                    </ul>
                                    <hr>
                                    <div class="row">
                                        <div class="col-12 feed-key">
                                            <div class="author-thumb ">
                                                <!-- <img src="<?= $this->session->pro_img; ?>" alt=""> -->
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <h4><?= $this->session->name; ?></h4>
                                            <p>Now</p>
                                        </div>
                                    </div>
                                    <span id="error_for_length" style="color: red;"></span>
                                    <div class="create-your-post-options">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form>                
                                                        <div class="form-group with-icon label-floating is-empty">
                                                            <textarea class="form-control" id="pquery" placeholder="Write your query/doubt, community members will help you soon..."></textarea>
                                                            <span class="material-input"></span>
                                                        </div>      
                                                    </form>
                                                </div>
                                            </div>
                                        </div>          
                                    </div>
                                </div>
                                <div id="menu1" class="container tab-pane fade"><br>
                                    <div class="row">
                                        <div class="col-12 feed-key">
                                            <div class="author-thumb ">
                                                <img src="<?= $this->session->pro_img; ?>" alt="">
                                            </div>
                                            <h4><?= $this->session->name; ?></h4>
                                            <p>Now</p>
                                        </div>
                                    </div>
                                    <span id="error_for_question_length" style="color: red;"></span>
                                    <div class="create-your-post-options">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <!-- <form>  -->               
                                                        <div class="form-group with-icon label-floating is-empty">             
                                                            <textarea class="form-control" id="question_text" placeholder="Write your question here"></textarea>
                                                            <span class="material-input"></span>
                                                        </div>
                                                        <div id='TextBoxesGroup'>
                                                            <div id="TextBoxDivA" class="TextBoxCls">
                                                                <label> A </label> <input type='text' id='textboxA' value="" placeholder="Write your option here">
                                                            </div>
                                                            <div id="TextBoxDivB" class="TextBoxCls">
                                                                <label> B </label> <input type='text' id='textboxB' value="" placeholder="Write your option here">
                                                            </div>
                                                        </div>

                                                        
                                                        <a href="javascript:void(0)" class="dv-add-more" id='addButton'><span><i class="fa fa-plus" aria-hidden="true"></i></span> add more</a>
                                                       
                                                        <a href="javascript:void(0)" class="dv-add-more" id='removeButton'><span><i class="fa fa-times text-danger" aria-hidden="true"></i></span> Remove</a>                                                                    
                                                        <!-- <input type='button' value='Remove Button' id='removeButton'> -->
                                                        <hr>
                                                        <div id="remo" class="dv-answ">Correct Answer 
                                                            <span onclick="active_question_span(this.id)" id="ins_ans_A" text="answer_one">A</span>
                                                            <span onclick="active_question_span(this.id)" id="ins_ans_B" text="answer_two">B</span>
                                                        </div>                                                            
                                                    <!-- </form> -->
                                                </div>
                                            </div>
                                        </div>          
                                    </div>
                                </div>
                                <div id="menu2" class="container tab-pane fade"><br>
                                    <div class="row">
                                        <div class="col-12 feed-key">
                                            <div class="author-thumb ">
                                                <img src="<?= $this->session->pro_img; ?>" alt="">
                                            </div>
                                            <h4><?= $this->session->name; ?></h4>
                                            <p>Now</p>
                                        </div>
                                    </div>
                                    <span id="error_for_shareinfo_length" style="color: red;"></span>
                                    <div class="create-your-post-options">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form>                
                                                        <div class="form-group with-icon label-floating is-empty">             
                                                            <textarea class="form-control" id="shareinfo" placeholder="Share valuable information, Tips & Tricks which can help others."></textarea>
                                                            <span class="material-input"></span>
                                                        </div>       
                                                    </form>
                                                </div>
                                            </div>
                                        </div>          
                                    </div>
                                </div>
                                <div id="Share" class="container tab-pane fade"><br>
                                    <div class="row">
                                        <div class="col-12 feed-key">
                                            <div class="author-thumb ">
                                                <img src="<?= $this->session->pro_img; ?>" alt="">
                                            </div>
                                            <h4><?= $this->session->name; ?></h4>
                                            <p>Now</p>
                                        </div>
                                    </div>
                                    <span id="error_for_shareexam_length" style="color: red;"></span>
                                    <div class="create-your-post-options">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form>                
                                                        <div class="form-group with-icon label-floating is-empty">             
                                                            <textarea class="form-control" id="shareexam" placeholder="Share your exam experience to help other users to crack Exam."></textarea>
                                                            <span class="material-input"></span>
                                                        </div>       
                                                    </form>
                                                </div>
                                            </div>
                                        </div>          
                                    </div>
                                </div>
                                <div id="UploadV" class="container tab-pane fade"><br>
                                    <div class="row">
                                        <div class="col-12 feed-key">
                                            <div class="author-thumb ">
                                                <img src="<?= $this->session->pro_img; ?>" alt="">
                                            </div>
                                            <h4><?= $this->session->name; ?></h4>
                                            <p>Now</p>
                                        </div>
                                    </div>
                                    <div class="create-your-post-options">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form>                
                                                        <div class="form-group with-icon label-floating is-empty">             
                                                            <textarea class="form-control" placeholder="Share what you are thinking here..."></textarea>
                                                            <span class="material-input"></span>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>          
                                    </div>
                                </div>
                            </div>
                    		<div id="social" class="social-icon-ibt">
                            <div class="row">
                                <div class="col-12" id="tag_people_name_with_id">
						        	
                                </div>
                            </div>
                            <br>
                            <div class="row">
                            <div class="col-12 feed-key post-img-path" style="display: none;">
                                <figure class="upload_img" style="display: none;">
                                    <img src="" id="profile-img-tag" width="100px" height="100px"/>
                                </figure>
                                    <p id="image_name"></p>
                            </div>
                            </div>
                            <br>
                            <div class="add-options-message">
                                <a href="javascript:void(0);" class="options-message" data-toggle="tooltip" data-placement="top" data-original-title="ADD PHOTOS">
                                    <label for="file-upload121" class="custom-file-upload121">     
                                        <i class="fa fa-camera" aria-hidden="true"></i>                                  
                                    </label> 
                                    <input id="file-upload121" type="file" accept="image/*" onchange="return readURL(this)">
                                                                       
                                </a>
                                <a href="javascript:void(0);" class="options-message tagmodal" data-toggle="tooltip" data-placement="top" data-original-title="TAG YOUR FRIENDS"  data-toggle="modal" data-target="#tadFrd"> 
                                    <label for="" class="olymp-computer-icon">
                                        <i class="fa fa-desktop" aria-hidden="true"></i>
                                    </label>
                                </a>            
                                <a href="javascript:void(0);" class="options-message" data-toggle="tooltip" data-placement="top" data-original-title="ADD ATTACHMENT">
                                    <label for="file-upload" class="custom-file-upload">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    </label>
                                     <input id="file-upload" type="file" accept="application/*" onchange="return readURL(this)">
                                </a>             
                            </div>
                            </div> 
                        </div>
                    </div>
              </div>
              <div class="tab"> 
                <h2 class="title title11">Select Category</h2>
                <section class="mb60 my-cour">
                <!-- <span style="color: red;text-align: center;margin-left: 20px;" id="msg3"></span> -->
                <div class="container">
                    <div class="row">
                    <div class="col col-xl-12 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
                        <div id="accordion">
                        <?php if(!empty($preferences)){ 
                                for($i = 0;$i<sizeof($preferences);$i++){ 
                                    foreach($category_details['data']['main_sub_category'] as $sub_cat){
                                        if($sub_cat['id'] == $preferences[$i]){
                                            $arr[] = $sub_cat['parent_id'];
                                        } 
                                    } 
                                } 
                            } 
                            $unique = array_keys(array_flip($arr));
                        ?>
                        <?php for($j =0;$j<sizeof($unique);$j++){ 
                                foreach($category_details['data']['main_category'] as $cat){
                                    if($cat['id'] == $unique[$j]){
                        ?>
                        <div class="card">
                            <div class="card-header div-cat category" id="<?=$cat['id'];?>">
                                <img src="<?=$cat['image'];?>"  class="roshit" alt="<?php echo $cat['text']; ?>"  style="background-color: <?=$cat['color'];?>">
                                <a class="card-link active" data-toggle="collapse" href="#collapseOne<?php echo $cat['id'] ?>">
                                    <?php echo $cat['text']; ?> 
                                </a>        
                            </div> 
                        </div>
                    <?php } } }?>
                    </div>               
                    </div>
                    </div>
                </div>
                </section>
              </div>
              <div class="tab">
                 <h2 class="titl title11">Select Exam</h2>
                <section class="mb60 my-cour">
                <span id="count_exam" style="display: none;"></span>
                <div class="container">
                    <div class="row" >
                        <div class="col col-xl-12 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
                            <div id="accordion" class="sub_category_data">
                            
                            </div>               
                        </div>
                    </div>
                </div>
                </section>
              </div>
              <div class="tab">
                <h2 class="titl title11">Select Subject</h2>
                <section class="mb60 my-cour">
                    <span id="count_subject" style="display: none;"></span>
                    <a href="javascript:void(0)" onclick="nextPrev(1);" class="skip-btn">Skip</a>
                    <div class="container">
                        <div class="row">
                            <div class="col col-xl-12 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
                                <div id="accordion" class="subject_data">
                                
                                </div>               
                            </div>
                        </div>
                    </div>
                </section>
              </div>
               <div class="tab">
                <h2 class="titl title11">Suggested Post </h2>
                <section class="mb60 my-cour">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col col-xl-12 m-auto col-lg-10 col-md-12 col-sm-12 col-12"> 
                                <h5 id="queryt"></h5>
                                <span id="tag_ids" style="display: none;"></span>
                                <div class="last-para">
                                <p>We have filtered some posts those might answer your query.<br>Please go through them or continue posting.</p>
                                </div>               
                            </div>
                        </div>
                    </div>
                </section>
                <div style="overflow:auto;margin-top: -52px;">
                <div style="float:right; padding-right: 15px;">
                    <button type="button"  class="btn btn-primary2 btn-md-2" id="discard_post">Discard</button>
                    <button type="button"  class="btn btn-primary1 btn-md-2" id="adding_post">Post</button>
                </div>
              </div>
                
              </div>
              <!-- <div class="t
              ab">
                  sdfghj
              </div> -->
              <div style="overflow:auto;margin-top: -52px;float: right;">
                <div style="float:right; padding-right: 15px;">
                  <button type="button" class="btn btn-primary btn-md-2" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                  <button type="button"  class="btn btn-primary1 btn-md-2" id="nextBtn">Next</button> 
                </div>
              </div>
              <!-- Circles which indicates the steps of the form: -->
              <div style="text-align:center;margin-top:40px;">
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
              </div>
            </form>
        </div>
    </div>
   
</div>
<!-- ============================ Tag Friend ==============================-->
<!-- The Modal -->
<div class="modal fade hide" id="tadFrd" style="overflow-y: auto;">
     <!-- ============================ Tag Friend ==============================-->

    <div class="modal-dialog heading_popup">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header categroy-header">
          <h4 class="modal-title">Tag your Friends</h4>
          
          <button type="button" class="close tag_close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <span style="float: right;"><a href="javascript:void(0);" class="tag_done">Done</a></span>
            <h2>Select Category</h2>
            <label class="container tag-frdCateg">MME Expert
                <input type="radio" checked="checked" name="radio" value="expert" class="people">
                <span class="checkmark"></span>
            </label>
            <label class="container tag-frdCateg">Mentor
                <input type="radio" name="radio" value="mentor" class="people">
                <span class="checkmark"></span>
            </label>
            <label class="container tag-frdCateg">Users I Follow 
                <input type="radio" name="radio" value="normal" class="people">
                <span class="checkmark"></span>
            </label>
        </div>
        <br>
        <div class="row">
            <div class="col-12 feed-key" id="people_tag">
                <?php if(!empty($get_tag_people)){ 
                        foreach($get_tag_people['data'] as $tag_people){
                ?>
                <label class="container select-chk">
                    <div class="author-thumb ">
                        <img src="<?=$tag_people['profile_picture'];?>" alt="">
                    </div>
                    <h4><?=$tag_people['name'];?></h4>
                    <input type="checkbox"  name="expert_people" value="<?=$tag_people['id'];?>" data-name="<?=$tag_people['name'];?>">
                    <span class="checkmark1"></span>
                </label>
                <?php } } ?>
            </div>
        </div>

        
      </div>
    </div>
    <!-- ============================ End Tag Friend ==============================-->
  </div>

<!-- ============================ End Tag Friend ==============================-->
<script type="text/javascript">
 
    function readURL(input) {
        $('#image_name').html('');
        var url = input.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0]&& (ext == "png" || ext == "jpeg" || ext == "jpg")) {
            $('.post-img-path').css({"display":"block"});
            $('.upload_img').css({"display":"block"});
            var filename = $('#file-upload121').val();
                if (filename.substring(3,11) == 'fakepath') {
                    filename = filename.substring(12);
                    $('#image_name').html(filename);
                }
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
                document.getElementById("file-upload").disabled = true;
                $('#file-upload').attr('data-original-title','You cannot add other Media');
            }
             reader.readAsDataURL(input.files[0]);
        }
     }

    $('.write_modal').click(function(){
        $('#regForm').find("input,textarea,select").val('');
        $('#queryt').html('');
        $('#tag_ids').html('');
        $('#remo > span').removeClass('active14'); 
    });
    $('.nav-link').click(function(){
    	if($(this).attr('data-id') == '2'){
    		$('#social').css({"display":"none"});
    	}else{
    		$('#social').css({"display":"block"});
    	}
    });
    $('.tagmodal').click(function(){
        $('#update-header').modal('hide');
        $('#tadFrd').modal('show');
    });
    $('.tag_close').click(function(){
        $('#tadFrd').modal('hide');
        $('#update-header').modal('show');
    });
</script>
<script>
        function active_question_span(id) {
            // alert('hello');
            $("span[id^='ins_ans']").removeClass("active14");            
            $("#"+id).addClass("active14");   
        }
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->
<script>
$(document).ready(function(){  
//================================== For Add Post ==================================================
    $('#nextBtn').click(function(){
        $('#error_for_length').html('');
        $('#error_for_question_length').html('');
        $('#error_for_shareinfo_length').html('');
        $('#error_for_shareexam_length').html('');

        var tab_id = $("#configuration_sidebar_content li").find("a.active").attr('data-id');
        if(tab_id == '1'){
        	var data = $('textarea#pquery').val();
        	var textlen = data.length;
            if(textlen < 5){
                $('#error_for_length').html('Please type minimum 5 lettes to create post.');
            }else{
                $('#queryt').html(data);
                nextPrev(1);
                $('#nextBtn').css({"display":"none"});
            }
        }
        else if(tab_id == '2'){
            var data = $('textarea#question_text').val();
            var textlen = data.length;
            if(textlen < 5){
                $('#error_for_question_length').html('Please type minimum 5 lettes to create post.');
            }else if($('#textboxA').val() == ""){
                $('#error_for_question_length').html('please enter options A');
            }else if($('#textboxB').val() == ""){
                $('#error_for_question_length').html('please enter options B');
            }else if($('#textboxC').val() == ""){
                $('#error_for_question_length').html('please enter options C');
            }else if($('#textboxD').val() == ""){
                $('#error_for_question_length').html('please enter options D');
            }else if($('#textboxE').val() == ""){
                $('#error_for_question_length').html('please enter options E');
            }else if($('#remo > span').hasClass('active14') == false){
                $('#error_for_question_length').html('please select correct answer');
            }else{
                $('#queryt').html(data);
                nextPrev(1);
                $('#nextBtn').css({"display":"none"});
            }
        }else if(tab_id == '3'){
        	var data = $('textarea#shareinfo').val();
            var textlen = data.length;
            if(textlen < 5){
                $('#error_for_shareinfo_length').html('Please type minimum 5 lettes to create post.');
            }else{
                $('#queryt').html(data);
                nextPrev(1);
                $('#nextBtn').css({"display":"none"});
            }
        }else if(tab_id == '4'){
        	var data = $('textarea#shareexam').val();
        	var textlen = data.length;
        	var err = 'error_for_shareexam_length';
            if(textlen < 5){
                $('#error_for_shareexam_length').html('Please type minimum 5 lettes to create post.');
            }else{
                $('#queryt').html(data);
                nextPrev(1);
                $('#nextBtn').css({"display":"none"});
            }
        }else if(tab_id == '5'){
        	
        }
    });
//==================================================================================================
    $('.category').click(function(){
        var category_id = $(this).attr('id');
        $('.sub_category_data').html('');
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('web/Feeds/sub_category_data'); ?>',
            data: {category_id:category_id},
            success: function (html) {
                nextPrev(1);
                $('.sub_category_data').append(html);

            }
        });
    });
//=====================================================================================================
    $('.card .div-cat').click(function(){
        $('.div-cat').removeClass("active11");
        $(this).addClass("active11");
    });

	$('#discard_post').click(function(){
		if(confirm('Are you sure you want to discard this post?')){
			$('#update-header').modal('hide');			
			window.location.href = '<?php echo site_url();?>/web/Feeds';
		}
	});
//=====================================================================================================
    $('.people').click(function(){
        var user_type = $(this).val();
        var last_id = '';
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('web/Feeds/get_tag_people'); ?>',
            data: {user_type:user_type,last_id:last_id},
            success: function (html) {
                $('#people_tag').html(html);
            }
        });
    });
//========================================================================================================
    $('.tag_done').click(function(){
        var isChecked = $("input[name='radio']:checked").val();
        var tag_peoples = [];
        var tag_peoples_name = [];
        var tag_str = '';
        var name = "input[name='"+isChecked+"_people']:checked";
        $.each($(name), function(){            
            tag_peoples.push($(this).val());
            tag_peoples_name.push($(this).attr('data-name'));
        });
        var tag_people_count = tag_peoples.length;
        if(tag_people_count === 0){
            alert('please choose atleast one people.');
        }else{
            $('#tadFrd').modal('hide');
            $('#update-header').modal('show');
            $('#tag_ids').html(tag_peoples.toString());
         //    $.ajax({
	        //     type: 'POST',
	        //     url: '<?php// echo site_url('web/Feeds/set_tag_people'); ?>',
	        //     data: {tag_peoples:tag_peoples,tag_peoples_name:tag_peoples_name},
	        //     success: function (html) {
         //         $('#tag_people_name_with_id').html(html);
         //    	}
        	// });
            // <div class="tag-fir-more" id="tag_149"> 
                                
            //                    <div style="color: #0080ff;">Aarav Deep
            //                    <a href="javascript:void(0);" class="delete_tag_people" id="149" name="Aarav Deep"><i class="fa fa-times text-danger" aria-hidden="true"></i> </a></div>  
                                
            //                 </div>
            for(var i=0;i<tag_people_count;i++){
                tag_str += `<div class="tag-fir-more" id="tag_`+tag_peoples[i]+`"> 
                                <div style="color: #0080ff;">`+ tag_peoples_name[i] +` <a href="javascript:void(0);" class="delete_tag_people" id="`+tag_peoples[i]+`" name="`+tag_peoples_name[i]+`"><i class="fa fa-times text-danger" aria-hidden="true"></i></a>
                                </div> 
                            </div>`
            }
           $('#tag_people_name_with_id').html(tag_str);
        }
    });
//==========================================================================================================
 	$('.delete_tag_people').on('click', this, function(){
 		alert();
 		var tag_people_id = $(this).attr('id');
 		var tag_people_name = $(this).attr('name');

 	// 	var index_id = tag_peoples.indexOf(tag_people_id);
 	// 	var index_name = tag_peoples_name.indexOf(tag_people_name);
		// 	if (index_id > -1) {
		// 	  tag_peoples.splice(index_id, 1);
		// 	}
		// 	if (index_name > -1) {
		// 	  tag_peoples_name.splice(index_name, 1);
		// 	}
		// $('#tag_'+tag_people_id).remove();
		// console.log(tag_peoples);
		// console.log(tag_peoples_name);
 	});
//==========================================================================================================
});
//=====================================================================================================
    $('#adding_post').click(function(){
        var sub_cate_id = $('.div-cat1.active11').attr('id');
        var subject_id = $('.div-cat2.active11').attr('id');
        var text = $('#queryt').html();
        var tag_people = $('#tag_ids').html();
        var post_type = $("#configuration_sidebar_content li").find("a.active").attr('data-text');
        if(post_type == 'text'){
            var request = {post_type:post_type,subject_id:subject_id,sub_cate_id:sub_cate_id,text:text,tag_people:tag_people};
        }else if(post_type == 'mcq'){
            var answer_three = '', answer_four = '', answer_five = '';
            var answer_one = $('#textboxA').val();
            var answer_two = $('#textboxB').val();
            if($('#textboxC').val() != undefined){
                var answer_three = $('#textboxC').val();
            }
            if($('#textboxD').val() != undefined){
                var answer_four = $('#textboxD').val();
            }
            if($('#textboxE').val() != undefined){
                var answer_five = $('#textboxE').val();
            }
            if($('#remo > span').hasClass('active14') == true){
                var right_answer = $('#remo > span.active14').attr('text');   
            }
            var request = {post_type:post_type,subject_id:subject_id,sub_cate_id:sub_cate_id,text:text,answer_one:answer_one,answer_two:answer_two,answer_three:answer_three,answer_four:answer_four,answer_five:answer_five,right_answer:right_answer};
        }
         // console.log(request);
        $.ajax({
            type: 'POST',
            // dataType: 'JSON',
            url: '<?php echo site_url('web/Feeds/add_post'); ?>',
            data:request,
            success: function (data) {
                 // console.log(data);
                $('#update-header').modal('hide');
                alert('Your Post has been successfully posted.');
                $('#new_post').html(data).addClass('abc');
                setTimeout(function(){
                    $('.abc').fadeIn();
                    $('.abc').addClass('noHighlight');
                },3000);
            }
        });
    });
//=====================================================================================================
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
     document.getElementById("prevBtn").style.display = "inline";
  }
  
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("checkbox");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}

</script>
<style type="text/css">
.abc {border:6px; border-style:solid; border-color:#ccffe6;transition:all 1s;}

.abc.noHighlight {background:transparent;border-color:transparent;}
</style>
