<?php
$com = 0;
if(!empty($posts['data'])){
  // foreach ($posts['data'] as $posts['data'])
  // print_r($posts['data']);
?>
                                                       <div class="ui-block">
                                            <article class="hentry post has-post-thumbnail shared-photo">
                                                <!------------------------------------ Upper feed Section  --------------------------------------->
                                                <div class="post__author author vcard inline-items">
                                                    <?php if ($this->CI->remote_file_exists($posts['data']['post_owner_info']['profile_picture'], 1)) { ?>
                                                        <img src="<?= $posts['data']['post_owner_info']['profile_picture'] ?>" alt="author" onclick="alert('Under construction');">
                                                    <?php } else { ?>
                                                        <i class="fa fa-user-circle" alt="author" style="font-size:45px;" onclick="alert('Under construction');"></i>
                                                    <?php } ?>

                                                    <div class="author-date">
                                                        <a  href="<?php echo site_url('/web/Profile/profiletest/' . $posts['data']['post_owner_info']['id']); ?>" ><?= $posts['data']['post_owner_info']['name'] ?>
                                                        </a>
                                                        <?php if ($posts['data']['pinned_post'] != '') { ?>
                                                            <span style="color:#ff5e3a">  Featured Post</span>     
                                                        <?php } ?>
                                                        <div class="post__date">
                                                            <time class="published" datetime="2017-03-24T18:18">
                                                                <?php echo $this->CI->time_ago_in_php($posts['data']['creation_time']); ?> 
                                                            </time>
                                                        </div>
                                                    </div>

                                                    <!-- <div class="more">
                                                                    <svg class="olymp-three-dots-icon">
                                                                    <use xlink:href="/web_assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                                                    </svg>
                                                                    <ul class="more-dropdown">
                                                                    <li><a href="#">Edit Post</a></li>
                                                                        <li><a href="#">Delete Post</a></li>
                                                                        <li><a href="#">Turn Off Notifications</a></li>
                                                                        <li><a href="#">Select as Featured</a></li>
                                                                    </ul>
                                                            </div> -->
                                                </div>
<!------------------------------------ End Upper feed Section  ------------------------------>                
                                                <div id="pre_text<?= $posts['data']['user_id'] ?>">
                                                    <span class="pre_text <?= $posts['data']['user_id'] ?> ">
<!----------------------------- For Text ---------------------------------------------------->
<?php if ($posts['data']['post_type'] == 'user_post_type_normal') { ?>
                                                            <a href="<?php echo site_url('/web/Feeds/single_post/'.$posts['data']['post_data']['post_id']);?>"><div style="color:#000;" id="upper<?= $posts['data']['post_data']['id'] ?>">
                                                                <?php echo substr($posts['data']['post_data']['text'], 0, 100); ?>
                                                                <?php if (strlen($posts['data']['post_data']['text']) >= 100) { ?>....
                                                                    <span class="expand" id="expand<?= $posts['data']['user_id']; ?>" onclick="toggle_data(this.id,<?= $posts['data']['user_id'] ?>,<?= $posts['data']['post_data']['id'] ?>)"> 
                                                                        <a href="javascript:void(0);">show more</a>
                                                                    </span> 
                                                                <?php } ?>
                                                            </div>
                                                            <span style="display: none;" class="pre_text expand<?= $posts['data']['user_id']; ?> "><?= $posts['data']['post_data']['text']; ?> 
                                                            </span>
                                                            <?php if (strlen($posts['data']['post_data']['text']) >= 100) { ?>
                                                                <span class="expand" id="expand<?= $posts['data']['user_id']; ?>" onclick="toggle_data_c(this.id,<?= $posts['data']['user_id'] ?>,<?= $posts['data']['post_data']['id'] ?>)"> 
                                                                    <a id="hiexpand<?= $posts['data']['user_id']; ?>" style="display:none;" href="javascript:void(0);">show less</a>
                                                                </span>

                                                            <?php } ?> 
                                                        </a>
<!------------------------------------------------------------------------------------------->
<!------------------------ For Current Affair, Review, Article and Vocab -------------------->
<?php } else if ($posts['data']['post_type'] == 'user_post_type_current_affair' || $posts['data']['post_type'] == 'user_post_type_review' || $posts['data']['post_type'] == 'user_post_type_article' || $posts['data']['post_type'] == 'user_post_type_vocab') { ?>
                                                    <a href="<?php echo site_url('/web/Feeds/single_post/'.$posts['data']['post_data']['post_id']);?>">
                                                        <div style="color:#000;" id="upper<?= $posts['data']['post_data']['id'] ?>">
                                                            <?php echo substr($posts['data']['post_headline'], 0, 100); ?>
                                                            <?php if (strlen($posts['data']['post_headline']) >= 100) { ?>....
                                                                <span class="expand" id="expand<?= $posts['data']['user_id']; ?>" onclick="toggle_data(this.id,<?= $posts['data']['user_id'] ?>,<?= $posts['data']['post_data']['id'] ?>)"> 
                                                                    <a href="javascript:void(0);">show more</a>
                                                                </span> 
                                                            <?php } ?>
                                                        </div>
                                                        <span style="display: none;" class="pre_text expand<?= $posts['data']['user_id']; ?> "><?= $posts['data']['post_headline']; ?> 
                                                        </span>
                                                        <?php if (strlen($posts['data']['post_headline']) >= 100) { ?>
                                                            <span class="expand" id="expand<?= $posts['data']['user_id']; ?>" onclick="toggle_data_c(this.id,<?= $posts['data']['user_id'] ?>,<?= $posts['data']['post_data']['id'] ?>)"> 
                                                                <a id="hiexpand<?= $posts['data']['user_id']; ?>" style="display:none;" href="javascript:void(0);">show less</a>
                                                            </span>

                                                    <?php } ?> 
                                                </a>
<!------------------------------------------------------------------------------------------->
<!---------------------------------------- For MCQ ------------------------------------------>
<?php } else if ($posts['data']['post_type'] == 'user_post_type_mcq') { ?>
                                                        <a href="<?php echo site_url('/web/Feeds/single_post/'.$posts['data']['post_data']['post_id']);?>">
                                                            <div style="color:#000;"><?php echo $posts['data']['post_data']['question']; ?>
                                                                <span style="float: right;border: 2px solid #f2f7ff;border-radius: 10px;padding: 3px;">
                                                                    <img src="/web_assets/img/coins.png" style="width: 30px;height: 30px;">
                                                                    <span style="color: orange;font-size: 16px;"><b>+<?= $posts['data']['post_data']['mcq_coins'] ?></b></span> Coins
                                                                </span>
                                                            </div>
                                                        </a>
                                                            <?php
                                                            if ($posts['data']['post_data']['answer_given_by_user'] != '') {
                                                                $rightclass = 'inner-sec';
                                                                $wrongclass = 'inner-sec-red';
                                                            } else {
                                                                $rightclass = '';
                                                                $wrongclass = '';
                                                            }
                                                            ?>
                                                            <ul id="myProgress">
                                                                <?php if ($posts['data']['post_data']['answer_three'] == "") { ?>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_1">  
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 1) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_1" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_one'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> A </div>
                                                                            <span class="answer" id="1" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_one']; ?> </span>   
                                                                        </div> 
                                                                    </li>

                                                                    <li  class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_2">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 2) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_2" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_two'] . '%'; ?>">

                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> B </div>
                                                                            <span class="answer" id="2" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_two']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                <?php } else if ($posts['data']['post_data']['answer_four'] == "") { ?>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_1">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 1) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_1" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_one'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> A </div>
                                                                            <span class="answer" id="1" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_one']; ?> </span>   
                                                                        </div> 
                                                                    </li>

                                                                    <li  class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_2">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 2) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_2" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_two'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> B </div>
                                                                            <span class="answer" id="2" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_two']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_3">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 3) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_3" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_three'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> C </div>
                                                                            <span class="answer" id="3" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_three']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                <?php } else if ($posts['data']['post_data']['answer_five'] == "") { ?>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_1">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 1) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_1" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_one'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> A </div>
                                                                            <span class="answer" id="1" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_one']; ?> </span>   
                                                                        </div> 
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_2">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 2) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_2" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_two'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> B </div>
                                                                            <span class="answer" id="2" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_two']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_3">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 3) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_3" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_three'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> C </div>
                                                                            <span class="answer" id="3" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_three']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_4">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 4) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_4" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_four'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> D </div>
                                                                            <span class="answer" id="4" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_four']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                <?php } else { ?>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_1">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 1) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_1" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_one'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> A </div>
                                                                            <span class="answer" id="1" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_one']; ?> </span>   
                                                                        </div> 
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_2">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 2) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_2" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_two'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> B </div>
                                                                            <span class="answer" id="2" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_two']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_3">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 3) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_3" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_three'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> C </div>
                                                                            <span class="answer" id="3" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_three']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_4">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 4) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_4" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_four'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> D </div>
                                                                            <span class="answer" id="4" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_four']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $posts['data']['post_data']['id'] ?>_5">
                                                                        <div class="<?php
                                                                        if ($posts['data']['post_data']['answer_given_by_user'] == 5) {
                                                                            if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $posts['data']['post_data']['id'] ?>_5" style="width: <?= $posts['data']['post_data']['mcq_voting']['answer_five'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> E </div>
                                                                            <span class="answer" id="5" mcq="<?= $posts['data']['post_data']['id']; ?>"><?php echo $posts['data']['post_data']['answer_five']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                <?php } ?>
                                                            </ul>

                                                            <?php
                                                            if ($posts['data']['post_data']['answer_given_by_user'] != '') {
                                                                if ($posts['data']['post_data']['answer_given_by_user'] == $posts['data']['post_data']['right_answer']) {
                                                                    ?>
                                                                    <div id="coins_<?= $posts['data']['post_data']['id']; ?>" style="background-color: #FFC107;padding: 5px;color: #030303;font-size: 14px;margin-bottom: 5px;">            Congrats you earned <?= $posts['data']['post_data']['mcq_coins']; ?> Coins
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                            <div id="coins_<?= $posts['data']['post_data']['id']; ?>" style="background-color: #FFC107;padding: 5px;color: #030303;font-size: 14px;margin-bottom: 5px;display: none">Congrats you earned <?= $posts['data']['post_data']['mcq_coins']; ?> Coins
                                                            </div>
<?php } ?>
<!------------------------------------- End MCQ ---------------------------------------------->
                                                    </span>
                                                </div>                
<!------------------------------------------------------------------------------------------->
<!------------------------ File Type like image, video and pdf ------------------------------>
 <?php if ($posts['data']['post_type'] == 'user_post_type_normal') { ?>
                                            <?php if (!empty($posts['data']['post_data']['post_file'])) { ?>
<!------------------------ File Type Image ------------------- ------------------------------->                          
                                                <?php if ($posts['data']['post_data']['post_file'][0]['file_type'] == 'image') { ?>

                                                    <div class="post-thumb" style="height: 340px;">
                                                        <img class="post_img" src="<?= $posts['data']['post_data']['post_file'][0]['link'] ?>" alt="photo" style="width:100%;height:100%;">
                                                    </div>
<!----------------------------------------------------------------------------------------------->
<!------------------------ File Type Video ------------------------------------------------------>
                                                <?php } else if ($posts['data']['post_data']['post_file'][0]['file_type'] == 'video') { ?>

                                                    <div class="video-thumb">
                                                        <img src="/web_assets/img/video9.jpg" alt="photo">
                                                        <a href="https://youtube.com/watch?v=excVFQ2TWig" class="play-video">
                                                            <svg class="olymp-play-icon">
                                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-play-icon"></use>
                                                            </svg>
                                                        </a>
                                                    </div>                   
<!----------------------------------------------------------------------------------------------->
<!------------------------ File Type PDF -------------------------------------------------------->
                                                <?php } else if ($posts['data']['post_data']['post_file'][0]['file_type'] == 'pdf') { ?>

                                                    <div class="post-thumb" style="border:none;">
                                                        <a href="<?= $posts['data']['post_data']['post_file'][0]['link']; ?>" target="_blank" title="Click to Download PDF"> 
                                                            <img src="/web_assets/img/PDF-Download.jpg" alt="photo" style="width:auto; height:300px;">
                                                        </a>
                                                    </div>

                                                <?php } ?>
<!----------------------------------------------------------------------------------------------->
                                            <?php } ?>
<?php } else if ($posts['data']['post_type'] == 'user_post_type_current_affair' || $posts['data']['post_type'] == 'user_post_type_vocab' || $posts['data']['post_type'] == 'user_post_type_article' || $posts['data']['post_type'] == 'user_post_type_review') { ?>

<!------------------------ File Type Image ------------------- ------------------------------->                          
                                             
                                                  <div class="post-thumb" style="height:340px;">
                                                      <img class="post_img" src="<?= $posts['data']['display_picture']; ?>" alt="photo" style="width:100%;height:100%;">
                                                  </div>
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<?php } ?>
                                                <?php if (!empty($posts['data']['tagged_people'])) { ?>
                                                    <b style="font-size: 12px; color: #babec4">Tagged People:
                                                        <br> 
                                                        <div class="tagg_sect">
                                                            <?php foreach ($posts['data']['tagged_people'] as $tag) { ?>
                                                                <span><?= $tag['name']; ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    </b>
                                                <?php } ?>
                                                <?php if ($posts['data']['post_type'] == 'user_post_type_mcq') { ?>

                                                    <div style="font-weight: 500;color: #B4B4B4;font-size: 13px;margin-bottom: 5px;margin-top: 5px;">
                                                        <span><?= $posts['data']['post_data']['mcq_attempt']; ?></span> attempts |  
                                                        <span class="count-action" id="like<?= $posts['data']['post_data']['post_id'] ?>">
                                                            <?php
                                                            if ($posts['data']['likes'] > 0) {
                                                                echo $posts['data'] ['likes'];
                                                            }
                                                            ?>
                                                        </span> 
                                                        <span>
                                                            <?php
                                                            if ($posts['data']['likes'] == 1) {
                                                                echo 'Like';
                                                            } else {
                                                                echo "Likes";
                                                            }
                                                            ?>
                                                        </span> | 
                                                        <span id="comment_count<?= $posts['data']['post_data']['post_id'] ?>"><?= $posts['data'] ['comments']; ?></span> comments
                                                    </div>

                                                <?php } else { ?>
                                                    <div style="font-weight: 500;color: #B4B4B4;font-size: 13px;margin-bottom: 5px;margin-top: 5px;">
                                                        <span class="count-action" id="like<?= $posts['data']['post_data']['post_id'] ?>">
                                                            <?php
                                                            if ($posts['data']['likes'] > 0) {
                                                                echo $posts['data'] ['likes'];
                                                            }
                                                            ?>
                                                        </span> 
                                                        <span>
                                                            <?php
                                                            if ($posts['data']['likes'] == 1) {
                                                                echo 'Like';
                                                            } else {
                                                                echo "Likes";
                                                            }
                                                            ?>
                                                        </span> | 
                                                        <span  id="comment_count<?= $posts['data']['post_data']['post_id'] ?>"><?= $posts['data'] ['comments']; ?></span> comments
                                                    </div>
                                                <?php } ?>                 
<!------------------------------------------------------------------------------------------------>
<!-------------------------------------- Likes --------------------------------------------------->
                                                <div class="post-additional-info inline-items">
                                                    <?php
                                                    if ($posts['data']['is_liked'] == 1) {
                                                        $color = "#38a9ff";
                                                        $s = 1;
                                                        $dcolor = 'dcolor';
                                                        $hov = '';
                                                    } else {
                                                        $color = "#717f8f";
                                                        $s = 0;
                                                        $dcolor = 'gcolor';
                                                        $hov = 'hov';
                                                    }
                                                    ?>    
                                                    <a href="javascript:void(0)" main-like="<?= $s ?>" id="lik<?= $posts['data']['post_data']['post_id'] ?>" onclick="likeF('<?= $posts['data']['post_data']['post_id'] ?>')" class="post-add-icon inline-items <?= $dcolor; ?> <?= $hov; ?>">
                                                        <!-- <svg class="olymp-like-post-icon-hand" >
                                                            <use xlink:href="<?php echo base_url(); ?>web_assets/svg-icons/sprites/icons.svg#olymp-like-post-icon-hand" ></use>
                                                        </svg> -->
                                                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                                        <span>Like</span>
                                                    </a> 
<!------------------------------------------------------------------------------------------->
<!---------------------------------------- Comment --------------------------------------->
                                                    <a href="javascript:void(0);"  class="post-add-icon inline-items" style="text-align:center;" 
                                                       id="<?= $posts['data']['post_data']['post_id']; ?>" onclick="comnts(this.id)">
                                                        <!-- <svg class="olymp-comments-post-icon">
                                                            <use xlink:href="<?php echo base_url(); ?>web_assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                                                        </svg>   -->
                                                        <i class="fa fa-commenting-o" aria-hidden="true"></i>
                                                        <span>Comment</span>
                                                    </a>
<!------------------------------------------------------------------------------------------->
<!--------------------------------- Share  -------------------------------------------------->
                                                    <a data-toggle="modal" data-target="#share-post" id="<?= $posts['data']['post_data']['post_id']; ?>" style="text-align:right;"  class="post-add-icon inline-items share_post">
                                                        <!-- <img src="<?php //echo base_url('/web_assets/img/blueshare_xxhdpi.png'); ?>">  -->
                                                        <i class="fa fa-share-alt-square" aria-hidden="true"></i>
                                                        <span>Share</span>
                                                    </a>
<!------------------------------------------------------------------------------------------->
                                                </div>
                                            </article>
<!------------------------------- End Article------------------------------------------------>
                                            <div id="commt<?= $posts['data']['post_data']['post_id'] ?>" class="" style="display:none">
                                                <ul class="comments-list showmydata" id="load_comment<?= $posts['data']['post_data']['post_id'] ?>" ></ul>
                                                <form  class="comment-form inline-items" >
                                                    <div class="post__author author vcard inline-items">
                                                        <?php if ($this->CI->remote_file_exists($this->session->pro_img, 1)) { ?>
                                                            <img src="<?= $this->session->pro_img ?>" alt="author">
                                                        <?php } else { ?>
                                                            <i class="fa fa-user-circle" alt="author" style="font-size:28px"></i>
                                                        <?php } ?>
                                                        <div class="form-group with-icon-right ">
                                                            <textarea class="form-control" placeholder="" id="commentdata<?= $posts['data']['post_data']['post_id'] ?>">
                                                            </textarea> 
                                                        </div>
                                                    </div>
                                                    <?php if (isset($this->session->userdata['username'])) { ?>
                                                        <button type="button" class="btn btn-md-2 btn-primary" class="more-comments" onclick="postComment('<?= $posts['data']['post_data']['post_id'] ?>')">Comment</button>
                                                    <?php } else { ?>
                                                        <button type="button" class="btn btn-md-2 btn-primary" class="more-comments" data-toggle="modal" data-target="#registration-login-form-popup">Comment</button>
                                                    <?php } ?>
                                                    <button type="button" class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color"  onclick="comnts(<?= $posts['data']['post_data']['post_id'] ?>, 'cancel')" >Cancel</button>
                                                </form>
                                            </div>
                                        </div>
                                        <?php  } ?>