<?php if(!$this->session->userdata('username')){
  header("Location: Home");
}?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>My Notes</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">

</script>

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
<!-- Main Styles CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
<!-- <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css"> -->
<link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
<style>

.dcolor{
  color:#38a9ff;
  fill:#38a9ff;
}
.gcolor{
  color:#717f8f;
  fill:#717f8f;
}
.hov:hover{
  color:#38a9ff;
  fill:#38a9ff;
}

.bottom_loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 10%;
    height: 10%;
    z-index: 9999;
    background: url('/web_assets/img/data_loader.gif') 50% 50% no-repeat rgb(249,249,249);
    opacity: 1.9;
}
#myProgress {
    list-style: none;
    margin-top: 30px;
}
#myProgress li {
    background-color: #FAFAFA;
    margin-bottom: 10px;
    height: 40px;
    position: relative;
    margin-top: 10px;
}
.inner-sec {
    position: absolute;
    background-color: #28ff27;
    opacity: 0.2;
    height: 90%;
}
.city {
        padding: 10px;
    background-color: #ddd;
}
.city1 {
  padding: 6px 10px 10px;
  background-color: #ddd;
      cursor: pointer;
}

.a {
    background-color: white;
    border: 1px solid #00000087;
    border-radius: 50%;
    width: 25px;
    height: 25px;
    padding: 1px 7px;
    float: left;
    text-align: center;
    font-size: 12px;
    font-weight: 500;
}

.a1 {
    background-color: white;
    border: 1px solid black;
    border-radius: 50%;
    width: 24px;
    height: 24px;
    padding: 0px 7px;
    float: left;
    margin-left:12px;
    margin-top:8px;
}
.answer {
    margin-left: 25px;
    font-size: 14px;
    font-weight: 500;
}
.inner-sec-red {
    position: absolute;
    background-color: red;
    opacity: 0.2;
    height: 90%;
}

.badge-notify{
    background: red;
    position: relative;
    top: -12px;
    left: -12px;
    color: white;
    font-size: 12px;
  }

  .post-additional-info {
      padding: 5px 0 0;
  }
  .post {
    padding: 25px 25px 10px;
  }
/* write something section css 14.11.2018 */

.write-box-your {
    background-color: #fff;
    border-radius: 5px;
    border: 0px solid #e6ecf5;
    padding: 7px;
    display: flex;
}
.write-line {
    padding-left: 20px;
    border: 1px solid #00000045;
    border-radius: 50px;
    padding: 18px;
    flex-grow: 1;
}
.write-box-your .author-thumb{padding: 10px;}
.create-tab-for-post-type{border: 1px solid #d7d7d7;
    padding: 8px 10px;
    /* flex-basis: 32%; */
    justify-content: center;
    align-items: center;
    margin-bottom: 10px;}
    .create-tab-for-post-type-icon h4{
    font-size: 15px;
    vertical-align: -webkit-baseline-middle;
}
.create-your-post .post__author{padding-top: 20px;}
.create-your-post-options .form-group.with-icon:after {
    content: '';
    position: absolute;
    display: block;
    height: 100%;
    width: 1px;
    background-color: transparent;
    top: 0;
    left: 0px;
}
 .create-your-post-options .label-floating.with-icon label.control-label{left:10px;}
 

 .create-your-post-options .label-floating.with-icon .form-control, .create-your-post-options .label-floating.with-icon input, .create-your-post-options .label-floating.with-icon textarea{padding-left: 10px;}
 .create-your-post-options .add-options-message {

    padding: 10px 25px;}
    /*------*/
input[type="file"] {
    display: none;
}
.custom-file-upload {
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
.select-content{
  border: 1px solid white !important;
  width: 15%;
}
</style>
</head>

<body style="background:#fff;">

<?php include('include/header.php'); ?>
<div class="main-box">
<div class="fixed-height"></div>
<section class="write-box" >
 
    <div class="container">
      <div class="row">

      <div class="col col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 filter-by-feeds feeds-sect-left">

        <div id="post_list">
          <?php

            $com = 0;
            if(!empty($posts['data'])){
              foreach ($posts['data'] as $post_list) {
              	if(!array_key_exists("user_id",$post_list)){
              		continue;
              	}
          ?>

                <div class="ui-block">
                  <article class="hentry post has-post-thumbnail shared-photo">
<!------------------------------------ Upper feed Section  --------------------------------------->
                    <div class="post__author author vcard inline-items">
                      <?php if ($this->CI->remote_file_exists($post_list['post_owner_info']['profile_picture'], 1)) { ?>
                        <img src="<?= $post_list['post_owner_info']['profile_picture'] ?>" alt="author" onclick="alert('Under construction');">
                      <?php } else { ?>
                        <i class="fa fa-user-circle" alt="author" style="font-size:45px;" onclick="alert('Under construction');"></i>
                      <?php } ?>
                        <div class="author-date">
                          <a  href="<?php echo site_url('/web/Profile/profiletest/'.$post_list['post_owner_info']['id']);?>" ><?= $post_list['post_owner_info']['name'] ?></a>
                            <?php if ($post_list['pinned_post'] != '') { ?>
                              <span style="color:#ff5e3a">  Featured Post</span>     
                            <?php } ?>
                          <div class="post__date">
                              <time class="published" datetime="2017-03-24T18:18">
                                 <?php echo $this->CI->time_ago_in_php($post_list['creation_time']); ?> 
                              </time>
                          </div>
                        </div>
                     <!--      <div class="more">
                  <svg class="olymp-three-dots-icon">
                    <use xlink:href="/web_assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                  </svg>
                  <ul class="more-dropdown">
                    <li>
                      <a href="#">Edit Post</a>
                    </li>
                    <li>
                      <a href="#">Delete Post</a>
                    </li>
                    <li>
                      <a href="#">Turn Off Notifications</a>
                    </li>
                    <li>
                      <a href="#">Select as Featured</a>
                    </li>
                  </ul>
                </div> -->
                    </div>

<!------------------------------------ End Upper feed Section  ------------------------------>
                    
                      <div id="pre_text<?= $post_list['user_id'] ?>">
                          <span class="pre_text <?= $post_list['user_id'] ?> ">
<!----------------------------- For Text ---------------------------------------------------->
                          
                            <?php if($post_list['post_type'] == 'user_post_type_normal') { ?>
                          <div style="color:#000;" id="upper<?= $post_list['post_data']['id'] ?>">
                            <?php echo substr($post_list['post_data']['text'], 0,100);?>
                            <?php if(strlen($post_list['post_data']['text']) >= 100) { ?>....
                            <span class="expand" id="expand<?= $post_list['user_id']; ?>" onclick="toggle_data(this.id,<?= $post_list['user_id'] ?>,<?= $post_list['post_data']['id'] ?>)"> 
                            <a href="javascript:void(0);">show more</a>
                            </span> <?php } ?>
                         </div>
                             <span style="display: none;" class="pre_text expand<?= $post_list['user_id']; ?> "><?= $post_list['post_data']['text']; ?> 
                             </span>
                             <?php if(strlen($post_list['post_data']['text']) >= 100) { ?>
                            <span class="expand" id="expand<?= $post_list['user_id']; ?>" onclick="toggle_data_c(this.id,<?= $post_list['user_id'] ?>,<?= $post_list['post_data']['id'] ?>)"> 
                              <a id="hiexpand<?= $post_list['user_id']; ?>" style="display:none;" href="javascript:void(0);">show less</a>
                            </span>
                            
                        <?php } ?> 
<!------------------------------------------------------------------------------------------->
<!---------------------------------------- For MCQ ------------------------------------------>
                          <?php  } else if($post_list['post_type'] == 'user_post_type_mcq'){ ?>

                              <div><?php echo $post_list['post_data']['question'];?>
                                <span style="float: right;border: 2px solid #f2f7ff;border-radius: 10px;padding: 3px;">
                                  <img src="/web_assets/img/coins.png" style="width: 30px;height: 30px;">
                                  <span style="color: orange;font-size: 16px;"><b>+<?= $post_list['post_data']['mcq_coins'] ?></b></span> Coins
                                </span>
                              </div>
                              <?php if($post_list['post_data']['answer_given_by_user'] != ''){
                                        $rightclass = 'inner-sec';
                                        $wrongclass = 'inner-sec-red';  
                                    }else{
                                        $rightclass = '';
                                        $wrongclass = '';
                                    }
                              ?>
                       <div class="">
                        <ul id="myProgress">

                          <?php if($post_list['post_data']['answer_three'] == ""){ ?>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_1">  
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 1){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_1" style="width: <?=$post_list['post_data']['mcq_voting']['answer_one'].'%'; ?>">
                            </div>
                            <div class="city1" style="">
                              <div class="a"> A </div>
                              <span class="answer" id="1" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_one'];?> </span>   
                            </div> 
                          </li>

                          <li  class="mcqop" main-id="<?=$post_list['post_data']['id']?>_2">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 2){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_2" style="width: <?=$post_list['post_data']['mcq_voting']['answer_two'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> B </div>
                              <span class="answer" id="2" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_two'];?> </span>
                            </div>
                          </li>

                        <?php }else if($post_list['post_data']['answer_four'] == ""){ ?>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_1">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 1){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_1" style="width: <?=$post_list['post_data']['mcq_voting']['answer_one'].'%';?>"></div>
                              <div class="city1" style="">
                                <div class="a"> A </div>
                                <span class="answer" id="1" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_one'];?> </span>   
                              </div> 
                          </li>

                          <li  class="mcqop" main-id="<?=$post_list['post_data']['id']?>_2">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 2){ 
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_2" style="width: <?=$post_list['post_data']['mcq_voting']['answer_two'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> B </div>
                              <span class="answer" id="2" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_two'];?> </span>
                            </div>
                          </li>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_3">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 3){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_3" style="width: <?=$post_list['post_data']['mcq_voting']['answer_three'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> C </div>
                              <span class="answer" id="3" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_three'];?> </span>
                            </div>
                          </li>

                       <?php }else if($post_list['post_data']['answer_five'] == ""){ ?>

                           <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_1">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 1){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_1" style="width: <?=$post_list['post_data']['mcq_voting']['answer_one'].'%';?>"></div>
                              <div class="city1" style="">
                                <div class="a"> A </div>
                                <span class="answer" id="1" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_one'];?> </span>   
                              </div> 
                          </li>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_2">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 2){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_2" style="width: <?=$post_list['post_data']['mcq_voting']['answer_two'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> B </div>
                              <span class="answer" id="2" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_two'];?> </span>
                            </div>
                          </li>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_3">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 3){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_3" style="width: <?=$post_list['post_data']['mcq_voting']['answer_three'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> C </div>
                              <span class="answer" id="3" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_three'];?> </span>
                            </div>
                          </li>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_4">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 4){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_4" style="width: <?=$post_list['post_data']['mcq_voting']['answer_four'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> D </div>
                              <span class="answer" id="4" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_four'];?> </span>
                            </div>
                          </li>

                        <?php }else{ ?>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_1">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 1){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_1" style="width: <?=$post_list['post_data']['mcq_voting']['answer_one'].'%';?>"></div>
                              <div class="city1" style="">
                                <div class="a"> A </div>
                                <span class="answer" id="1" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_one'];?> </span>   
                              </div> 
                          </li>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_2">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 2){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_2" style="width: <?=$post_list['post_data']['mcq_voting']['answer_two'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> B </div>
                              <span class="answer" id="2" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_two'];?> </span>
                            </div>
                          </li>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_3">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 3){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_3" style="width: <?=$post_list['post_data']['mcq_voting']['answer_three'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> C </div>
                              <span class="answer" id="3" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_three'];?> </span>
                            </div>
                          </li>

                          <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_4">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 4){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_4" style="width: <?=$post_list['post_data']['mcq_voting']['answer_four'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> D </div>
                              <span class="answer" id="4" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_four'];?> </span>
                            </div>
                          </li>

                           <li class="mcqop" main-id="<?=$post_list['post_data']['id']?>_5">
                            <div class="<?php if($post_list['post_data']['answer_given_by_user'] == 5){
                              if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){echo $rightclass;}else{ echo $wrongclass; } }?>" id="<?=$post_list['post_data']['id']?>_5" style="width: <?=$post_list['post_data']['mcq_voting']['answer_five'].'%';?>"></div>
                            <div class="city1" style="">
                              <div class="a"> E </div>
                              <span class="answer" id="5" mcq="<?=$post_list['post_data']['id'];?>"><?php echo $post_list['post_data']['answer_five'];?> </span>
                            </div>
                          </li>

                        <?php  } ?>
                       </ul>
                    </div>
                    <?php  if($post_list['post_data']['answer_given_by_user'] != ''){
                            if($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']){ ?>
                               <div id="coins_<?=$post_list['post_data']['id'];?>" style="background-color: #FFC107;padding: 5px;color: #030303;font-size: 14px;margin-bottom: 5px;">Congrats you earned <?=$post_list['post_data']['mcq_coins'];?> Coins</div>
                         <?php } } ?>
                         <div id="coins_<?=$post_list['post_data']['id'];?>" style="background-color: #FFC107;padding: 5px;color: #030303;font-size: 14px;margin-bottom: 5px;display: none">Congrats you earned <?=$post_list['post_data']['mcq_coins'];?> Coins</div>

                         
                       <?php } ?>
<!------------------------------------- End MCQ ---------------------------------------------->
                          </span>
                      </div>
                   
<!------------------------------------------------------------------------------------------->
<!------------------------ File Type like image, video and pdf ------------------------------>
                        <?php if (!empty($post_list['post_data']['post_file'])) { ?>
<!------------------------ File Type Image ------------------- ------------------------------->                          
              <?php if($post_list['post_data']['post_file'][0]['file_type'] == 'image'){ ?>
                                  <div class="post-thumb" style="border:none;">
                                    <img class="post_img" src="<?= $post_list['post_data']['post_file'][0]['link'] ?>" alt="photo" style="object-fit: cover;width:100%;height:300px;">
                                  </div>
                      
<!----------------------------------------------------------------------------------------------->
<!------------------------ File Type Video ------------------------------------------------------>
                          <?php }else if($post_list['post_data']['post_file'][0]['file_type'] == 'video'){ ?>
                                  <div class="video-thumb">
                                    <img src="/web_assets/img/video9.jpg" alt="photo">
                                    <a href="https://youtube.com/watch?v=excVFQ2TWig" class="play-video">
                                      <svg class="olymp-play-icon">
                                      <use xlink:href="svg-icons/sprites/icons.svg#olymp-play-icon"></use>
                                      </svg>
                                    </a>
                                  </div>
                      
<!----------------------------------------------------------------------------------------------->
<!------------------------ File Type PDF -------------------------------------------------------->
                          <?php }else if($post_list['post_data']['post_file'][0]['file_type'] == 'pdf'){ ?>
                                  <div class="post-thumb" style="border:none;">
                                    <a href="<?= $post_list['post_data']['post_file'][0]['link']; ?>" target="_blank" title="Click to Download PDF"> 
                                      <img src="/web_assets/img/PDF-Download.jpg" alt="photo" style="width:auto; height:300px;">
                                   </a>
                                  </div>
                        
                       <?php  } ?>
<!----------------------------------------------------------------------------------------------->
                    <?php    } ?>
                    <?php if(!empty($post_list['tagged_people'])){?>
                    <b style="font-size: 12px; color: #babec4">Tagged People:
                    <br> 
                    <div class="tagg_sect">
                      <?php foreach($post_list['tagged_people'] as $tag){ ?>
                        
                          <span><?=$tag['name'];?></span>
                        
                      <?php } ?>
                      </div>
                    </b>
                    <?php } ?>
                    <?php if($post_list['post_type'] == 'user_post_type_mcq'){ ?>
                    <div style="font-weight: 500;color: #B4B4B4;font-size: 13px;margin-bottom: 5px;margin-top: 5px;">
                          <span class="count-action" id="like<?= $post_list['post_data']['post_id'] ?>"><?php
                                    if ($post_list['likes'] > 0) {
                                        echo $post_list ['likes'];
                                    }
                                    ?></span> <span>
                                    <?php
                                    if ($post_list['likes'] == 1) {
                                        echo 'Like';
                                    } else {
                                        echo "Likes";
                                    }
                                    ?></span> | <span id="comment_count<?= $post_list['post_data']['post_id'] ?>"><?=$post_list ['comments'];?></span> comments
                           
                    </div>
                    <?php } else{ ?>
                      <div style="font-weight: 500;color: #B4B4B4;font-size: 13px;margin-bottom: 5px;margin-top: 5px;">
                         <span class="count-action" id="like<?= $post_list['post_data']['post_id'] ?>"><?php
                                    if ($post_list['likes'] > 0) {
                                        echo $post_list ['likes'];
                                    }
                                    ?></span> <span>
                                    <?php
                                    if ($post_list['likes'] == 1) {
                                        echo 'Like';
                                    } else {
                                        echo "Likes";
                                    }
                                    ?></span> | <span  id="comment_count<?= $post_list['post_data']['post_id'] ?>"><?=$post_list ['comments'];?></span> comments
                    </div>
                  <?php  } ?>
                    
<!------------------------------------------------------------------------------------------------>
<!-------------------------------------- Likes --------------------------------------------------->
                    <div class="post-additional-info inline-items">
                        <?php if (isset($this->session->userdata['username'])) { ?>
                            <?php //echo "chk ".$post_list['is_liked'];die;
                           
                                if($post_list['is_liked']==1){
                                   $color="#38a9ff";
                                   $s=1;
                                   $dcolor = 'dcolor';
                                   $hov =''; 
                                }else{
                                 $color="#717f8f"; 
                                   $s=0;
                                   $dcolor = 'gcolor';
                                   $hov = 'hov';
                                }
                                 ?>
                                
                                
                            <a href="javascript:void(0)" main-like="<?=$s?>" id="lik<?= $post_list['post_data']['post_id'] ?>" onclick="likeF('<?= $post_list['post_data']['post_id'] ?>')" class="post-add-icon inline-items <?=$dcolor;?> <?=$hov;?>">
                                <svg class="olymp-like-post-icon-hand" >
                                <use xlink:href="<?php echo base_url(); ?>web_assets/svg-icons/sprites/icons.svg#olymp-like-post-icon-hand" ></use>
                                </svg>
                                 <span><?php echo "Like"; ?></span>
                            </a> 
                        <?php } else { ?>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#registration-login-form-popup" class="post-add-icon inline-items">
                                <svg class="olymp-like-post-icon-hand">
                                <use xlink:href="<?php echo base_url(); ?>web_assets/svg-icons/sprites/icons.svg#olymp-like-post-icon-hand"></use>
                                </svg>

                                <span class="count-action"><?php
                                    if ($post_list['likes'] > 0) {
                                        echo $post_list ['likes'];
                                    }
                                    ?></span> <span><?php
                                    if ($post_list['likes'] == 1) {
                                        echo 'Like';
                                    } else {
                                        echo "Likes";
                                    }
                                    ?></span>
                            </a>
                        <?php } ?>
<!------------------------------------------------------------------------------------------->
<!---------------------------------------- Comment --------------------------------------->
                        <a href="javascript:void(0);"  class="post-add-icon inline-items" style="text-align:center;" id="<?= $post_list['post_data']['post_id']; ?>" onclick="comnts(this.id)">
                       
                            <svg class="olymp-comments-post-icon">
                            <use xlink:href="<?php echo base_url(); ?>web_assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                            </svg>
                            <span>Comment</span>

                        </a>
<!------------------------------------------------------------------------------------------->
<!--------------------------------- Share  -------------------------------------------------->

                        <a data-toggle="modal" data-target="#share-post"  style="text-align:right;cursor: pointer;"  class="post-add-icon inline-items">

                            <img src="<?php echo base_url('/web_assets/img/blueshare_xxhdpi.png');?>">

                          <span>Share</span>
                        </a>
<!------------------------------------------------------------------------------------------->

                    </div>
                  </article>
<!------------------------------- End Article------------------------------------------------>
                  <div id="commt<?= $post_list['post_data']['post_id'] ?>" class="" style="display:none">
                    <ul class="comments-list showmydata" id="load_comment<?= $post_list['post_data']['post_id'] ?>" >

                    </ul>

                    <form  class="comment-form inline-items" >

                          <div class="post__author author vcard inline-items">
                              <?php if ($this->CI->remote_file_exists($this->session->pro_img, 1)) { ?>
                                  <img src="<?= $this->session->pro_img ?>" alt="author">
                              <?php } else { ?>
                                  <i class="fa fa-user-circle" alt="author" style="font-size:28px"></i>
                              <?php } ?>
                              <div class="form-group with-icon-right ">
                                <textarea class="form-control" placeholder="" id="commentdata<?= $post_list['post_data']['post_id'] ?>"></textarea> 
                              </div>
                          </div>
                          <?php if (isset($this->session->userdata['username'])) { ?>
                              <button type="button" class="btn btn-md-2 btn-primary" class="more-comments" onclick="postComment('<?= $post_list['post_data']['post_id'] ?>')">Comment</button>
                          <?php } else { ?>
                              <button type="button" class="btn btn-md-2 btn-primary" class="more-comments" data-toggle="modal" data-target="#registration-login-form-popup">Comment</button>
                          <?php } ?>
                          <button type="button" class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color"  onclick="comnts(<?= $post_list['post_data']['post_id'] ?>,'cancel')" >Cancel</button>
                    </form>
                  </div>
                </div>

          <?php $com++;  } ?> 
      <?php  //$pid = $post_list['post_data']['post_id']; ?>
            <!-- <span id="search>" main="all" >
              <center><img  src="<?php //echo base_url(); ?>web_assets/img/data_loader.gif" class="bottom_loaders" style="display: none;width:80px;height:80px;"></center>
                
            </span> -->
        <?php }else{ ?>
               <!-- <center><h4>No feeds found!!</h4></center> -->
          <?php  } ?>
        </div>



      </div>

<!--               <div class="col col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
        <div class="ui-block responsive-flex1200">
        <div class="ui-block-title">
          <div class="form-group has-search"> 
            <div>
              <div class="w-search">
              <div class="form-group with-button is-empty">
                <input class="form-control" id="searching" main="search" type="text" placeholder="Search">
                <button style="background-color: #38a9ff;">
                  <svg class="olymp-magnifying-glass-icon"><i class="fa fa-search ds_ico" aria-hidden="true"></i></svg>
                </button>
              <span class="material-input"></span>
              </div>
                    </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    </div>



    </div>
  </section>
<!-- <section class="choose-exam-list">
  <div class="container">
    <div class="row">
      <div class="col col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
      </div>
		  <div class="col col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 filter-by-feeds" style="margin-bottom: 10px;">
        <div class="w-select">
          <span class="title" style="color: grey;font-weight: bold;font-size: 15px;">Filter By:</span>
            <select name="feed" id="feed" style="width: 25%;">
              <option value="all">All Posts </option> 
              <option value="mme">MME Expert Post</option>   
              <option value="mentor">Mentor Post</option> 
              <option value="follow">My Following</option>       
            </select>
        </div>
      </div>
	</div>
</section> -->


<!-- Window-popup Share Post -->
<div class="modal fade" id="share-post" tabindex="-1" role="dialog" aria-labelledby="share-post" aria-hidden="true">
  <div class="modal-dialog window-popup edit-widget edit-widget-twitter" role="document">
    <div class="modal-content">
      <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
        <svg class="olymp-close-icon"><use xlink:href="<?php echo base_url('/web_assets/svg-icons/sprites/icons.svg#olymp-close-icon');?>"></use></svg>
      </a>
      <div class="modal-header">
        <h6 class="title">Share post by</h6>
      </div>
      <div class="modal-body">
         <a href="http://www.facebook.com/sharer.php?u=https://www.ibtindia.com/" class="btn bg-facebook btn-lg full-width btn-icon-left" target="_blank"><i class="fab fa-facebook" aria-hidden="true"></i>Share by Facebook</a>
         <a href="https://plus.google.com/share?url=https://www.ibtindia.com/" class="btn bg-google  btn-lg full-width btn-icon-left" target="_blank"><i class="fab fa-google-plus" aria-hidden="true"></i>Share by Google Plus</a>
         <a href="http://twitter.com/share?text=IBT&url=https://www.ibtindia.com/" class="btn bg-twitter btn-lg full-width btn-icon-left" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i>Share by Twitter</a>
      </div>
    </div>
  </div>
</div>
<!-- ... end Window-popup Share Post -->




<!-- ... end Window-popup for post_query-->
<div class="header-spacer"></div>
<?php include('include/footer.php'); ?>
<script src="<?php echo base_url(); ?>/web_assets/js/jquery-3.2.1.js"></script> 
<script src="<?php echo base_url(); ?>/web_assets/js/base-init.js"></script>  
<script src="<?php echo base_url(); ?>/web_assets/js/svgxuse.js"></script>
<script defer src="<?php echo base_url(); ?>/web_assets/fonts/fontawesome-all.js"></script> 
<script src="<?php echo base_url(); ?>/web_assets/Bootstrap/dist/js/bootstrap.bundle.js"></script> 

<script type="text/javascript">

//  ================================= Show Comment Toggle and call Comment List ===========================
    function comnts(id,data=''){
      $('#commt'+id).slideToggle(); 
      if(data != 'cancel' ){
        comment(id); 
      }
      if($("#"+id).attr("data")==1){
        $("#"+id).children().css("color","#717f8f");
        $("#"+id).children('svg').css("fill","#717f8f");
        $("#"+id).attr("data",0);
      }else{
        $("#"+id).children().css("color","#38a9ff");
        $("#"+id).children('svg').css("fill","#38a9ff");
        $("#"+id).attr("data",1);
      }     
    }
//  =================================post like dislike=====================================
                    function likeF(pid){
                       var attrs = $('#lik'+pid).attr('main-like');
                      if(attrs==1){
                        $('#lik'+pid).css({"fill":"#717f8f","color":"#717f8f"});
                        $('#lik'+pid).attr('main-like','0');
                      }else{
                        $('#lik'+pid).css({"fill":"#38a9ff","color":"#38a9ff"});
                        $('#lik'+pid).attr('main-like','1');
                      }
                            $.ajax({
                                url: '<?= site_url('web/Feeds/like') ?>',
                                type: 'POST',
                                dataType: 'json',
                                data: {post_id : pid},
                                beforeSend: function() {
                                },
                                success: function(data){

                                    if(data.status){
                                        $("#like"+pid).html(data.data.likes);
                                    }else{
                                        $.ajax({
                                            url: '<?= site_url('web/Feeds/dislike') ?>',
                                            type: 'POST',
                                            dataType: 'json',
                                            data: {post_id : pid},
                                            success: function(data){
                                                //alert(data.data.likes);
                                                $("#like"+pid).html(data.data.likes); 
                                            }
                                        });
                                    }
                                },
                                complete: function(){
                                }

                            });
                    }
        //==========================================end post likke dislike==============================
//=======================================comment======================================================
          function comment(postid) {
                
                $.ajax({
                    type: 'Post',
                    data: {postid: postid},
                    url: '<?php echo site_url(); ?>/web/Feeds/comment',
                    success: function (data) {
                      console.log(data);
                        $('#load_comment'+postid).html(data);
                    },
                    error: function (err) {
                        console.log("error" + err);
                    }
                }); 
            }
             function reply(post_id,comment_id,data='') {
              if(data == 'cancel' || data == '2'){
                $('#sub'+comment_id).css({"display":"none"}); 
                $('.rep'+comment_id).attr('main', '1');
              }else{
                $('#sub'+comment_id).css({"display":"block"});
                $('.rep'+comment_id).attr('main', '2');
                    $.ajax({
                        type: 'Post',
                        data: {postid: post_id,comment_id:comment_id},
                        url: '<?php echo site_url(); ?>/web/Feeds/reply',
                        success: function (data) {
                            $('#sub'+comment_id).html(data);
                        },
                        error: function (err) {
                            console.log("error" + err);
                        }
                    });
              }
            }
            // function search_filter(page_num) { 
            //     page_num = page_num ? page_num : 0;
            //     //alert(page_num);
            //     var type = $('#search'+page_num).attr('main');
            //     if($('#searching').val() == ""){
            //     	var details = {last_post_id:page_num,type:type};
            //     }else{
            //     	var search = $('#searching').val();
            //     	var details = {last_post_id:page_num,type:type,search:search};
            //     }
            //     $.ajax({
            //         type: 'POST',
            //         url: '<?php //echo site_url('web/Feeds/ajax_post'); ?>',
            //         data: details,

            //         beforeSend: function () {
            //             $('.loader').show();
            //         },
            //         success: function (html) {
            //             $('#post_list').append(html);
            //             $('#search'+page_num).css({"display":"none"});
            //             $('.loader').fadeOut("slow");
            //         }
            //     });
            // }


            //=============================================reply post------------------=================                    
      function replyComment(pid,comment_id){                                                
        var comd = $("#replydata"+pid).val();                        
          if(comd == ""){                            //do nothing 
            
          }else{                            
            $.ajax({                                
                url: '<?php echo site_url('web/Feeds/replyComment'); ?>',                                
                type: 'POST',                                
                dataType:'json',                                
                data:{post_id: pid, commentmsg: comd,parent_id:comment_id},                                
                beforeSend: function() {                                  
                  $("#wave").show();                                
                },                                
                success: function(data){                                   
                 if(data.status){                                        
                  reply(pid,comment_id);                                        
                  $("#replydata"+pid).val('');                                   
                  }                                                                  
                },                                
                error:function(err){                                    
                //alert("ERR"+err);                                
              },                                
              complete: function(){                                    
                $("#wave").hide();                                
              }                            
        });                        
      }  
  }                  
   //==================================End comment====================================================== 
            //=============================================comment post------------------=================
                    function postComment(pid){
                        
                        var comd = $("#commentdata"+pid).val();

                        if(comd == ""){
                            //do nothing
                        }else{
                            $.ajax({
                                url: '<?= site_url('web/Feeds/postComment') ?>',
                                type: 'POST',
                                dataType:'json',
                                data:{post_id: pid, commentmsg: comd},
                                beforeSend: function() {
                                  $("#wave").show();
                                },
                                success: function(data){
                                    if(data.status)
                                    {
                                      $("#comment_count"+pid).html(data.data.comments);
                                        comment(pid); // get comments
                                        $("#commentdata"+pid).val('');
                                    }                                  
                                },
                                error:function(err){
                                    //alert("ERR"+err);
                                },
                                complete: function(){
                                    $("#wave").hide();
                                }
                            });
                        }
                    }
   //==================================End comment======================================================        

    //=================================== content toggle ===================================
                     function toggle_data(id, dt,post_id) {
                        $('.' + id).show();
                        // $('#expand' + id).hide();
                        // $('#pre_text' + dt).hide();
                        $('#upper' + post_id).css('display','none');
                        $('#hi' + id).show();
                    }
                    function toggle_data_c(id, dt,post_id) {
                        $('.' + id).hide();
                        // $('#pre_text' + dt).show();
                          $('#upper' + post_id).css('display','block');
                        $('#hi' + id).hide();
                    }
        //=================================== end content toggle===================================

        //=================== Follow and unfollow ============================================//
        function follow(follower_id){
            var name = document.getElementById("follow"+follower_id).value;
            if(name == 'Follow'){
              $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    url: '<?php echo site_url('web/Feeds/follow'); ?>',
                    data: {follower_id:follower_id},
                    success: function (data) {
                       if(data.status == true){
                        document.getElementById("follow"+follower_id).value = 'Following';
                        document.getElementById("follow"+follower_id).style.backgroundColor = '#38a9ff';
                        document.getElementById("follow"+follower_id).style.color = 'white';
                       }
                    }
              });
            }else{
              $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    url: '<?php echo site_url('web/Feeds/unfollow'); ?>',
                    data: {follower_id:follower_id},
                    success: function (data) {
                        if(data.status == true){
                        document.getElementById("follow"+follower_id).value = 'Follow';
                        document.getElementById("follow"+follower_id).style.backgroundColor = 'white';
                        document.getElementById("follow"+follower_id).style.color = '#38a9ff';
                       }
                    }
              });
            } 
        }
</script>
<script>
$(document).ready(function(){
  $('.mcqop').on('click', this , function(){
    var answer = $(this).find('span').attr('id');
    var mcq_id = $(this).find('span').attr('mcq');
    var id = $(this).attr('main-id');
    // alert(answer);
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: '<?php echo site_url('web/Feeds/mcq_answer'); ?>',
      data: {answer:answer,mcq_id:mcq_id},
      success: function (data) {
      	console.log(data);
        if(data.status == true){

          if(data.data.post_data.right_answer == answer){
            $('#'+id).addClass('inner-sec');
            $('#coins_'+mcq_id).css({"display":"block"}).html('Congrats you earned '+ data.data.post_data.mcq_coins + 'Coins');
          }else{
            $('#'+id).addClass('inner-sec-red');
          }
        }
      }
    });
  });
/// For categories==============================================================================
  $('#feed').on('change', this, function(){
  	var category = $(this).val();
  	$.ajax({
        type: 'POST',
        url: '<?php echo site_url('web/Feeds/ajax_post_dropdown'); ?>',
        data: {category:category},
        beforeSend: function () {
            $('.loader').show();
        },
        success: function (data) {

            $('#post_list').html(data);
            $('.loader').fadeOut("slow");
        },
        error:function(err){
            console.log("ERR"+err);
        },
  	});
  });
 //==============================================================================================
 /// For Search==============================================================================
  $('#searching').on('change', this, function(){
  	var search = $(this).val();
    var category =$(this).attr('main');
  	$.ajax({
        type: 'POST',
        url: '<?php echo site_url('web/Feeds/ajax_post_dropdown'); ?>',
        data: {srch_txt:search,category:category},
        beforeSend: function () {
            $('.loader').show();
        },
        success: function (data) {

            $('#post_list').html(data);
            $('.loader').fadeOut("slow");
        },
        error:function(err){
            console.log("ERR"+err);
        },
  	});
  });

 //==============================================================================================
 // var processing;
 //    $(document).scroll(function(e){

 //        if (processing)
 //            return false;

 //        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 700){
 //            processing = true;
            
 //             var pid = "<?php //echo $post_list['post_data']['post_id']; ?>";
 //             var type = $('#search'+pid).attr('main');
 //             var details = {last_post_id:pid,type:type};
 //            $.ajax({
 //                    type: 'POST',
 //                    url: '<?php //echo site_url('web/Feeds/ajax_post'); ?>',
 //                    data: details,

 //                    beforeSend: function () {
 //                        $('.bottom_loaders').css({"display":"block"});
 //                    },
 //                    success: function (html) {
 //                        $('#post_list').append(html);
 //                        // $('#search'+page_num).css({"display":"none"});
 //                      $('.bottom_loaders').css({"display":"none"});
 //                    }
 //                });
 //        }
 //    });

});
          
</script>
</body>
</html>
