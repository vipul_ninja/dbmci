<!DOCTYPE html>
<html lang="en">
<head>

	<title><?=$title;?></title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>web_assets/Bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>web_assets/Bootstrap/dist/css/bootstrap-grid.css">
	<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>web_assets/css/main.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>web_assets/css/fonts.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>web_assets/css/ibt_main.css">
	<!-- Main Font -->
	<script src="<?php echo base_url();?>web_assets/js/webfontloader.min.js"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>
	<style>
	body{
		background-color: #edf2f6;
	}
		.single-post-additional {
     margin-bottom: 0px; 
	}
	.post-content-wrap.col-12 {
    padding: 0px 20px 30px 30px;
	}
	.post-thumb img {
    height: 350px;
    width: 100%;
	}
	.single-post-v2 {
    padding: 15px 24px;
    text-align: center;
}
	</style>

</head>
<body>
<?php $this->load->view('include/header.php'); ?>	
<div class="header-spacer"></div>
<?php if(!empty($d1)){ ?>

<div class="container">
	<div class="row mt50">
		<div class="col col-xl-8 m-auto col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="page-flow w-select2">
          <ul class="breadcrumbs">
              <li class="breadcrumbs-item">
                <a href="<?php echo site_url(); ?>/web/Feeds">Home</a>
                <span class="fc-icon fc-icon-right-single-arrow"></span>
              </li>
              <li class="breadcrumbs-item">
                <a href="<?php echo site_url(); ?>/web/DailyDose">Daily Dose</a>
                <span class="fc-icon fc-icon-right-single-arrow"></span>
              </li>
               <li class="breadcrumbs-item">
                <a href="<?php echo site_url('/web/DailyDose/'.$page); ?>"><?=$title;?></a>
                <span class="fc-icon fc-icon-right-single-arrow"></span>
              </li>
             <li class="breadcrumbs-item active">
                <span>Description</span>
              </li>
          </ul>
        </div>
			<div class="ui-block">

				
				<!-- Single Post -->
				
				<article class="hentry blog-post single-post single-post-v2">
				
					<!-- <a href="#" class="post-category bg-blue-light">THE COMMUNITY</a> -->
					<h2 class="h1 post-title"><?php echo $d1['post_headline']; ?></h2>

					<div class="row">
					<div class="single-post-additional inline-items col-12">
						<div class="post__author author vcard inline-items">

							<?php if($this->CI->remote_file_exists($d1['post_owner_info']['profile_picture'],1)){ ?>
								<img alt="author" src="<?php echo $d1['post_owner_info']['profile_picture']; ?>" class="avatar">
						 <?php } else {  ?>
                                  <i class="fa fa-user-circle" alt="author" style="font-size:28px"></i>
                          <?php } ?>
							
							<div class="author-date not-uppercase">
								<a class="h6 post__author-name fn" href="#"><?php echo $d1['post_owner_info']['name']; ?></a>
								<div class="author_prof">
									Author
								</div>
							</div>
						</div>
						<div class="post-date-wrap inline-items">
							<svg class="olymp-calendar-icon">
								<use xlink:href="<?php echo base_url();?>web_assets/icons/icons.svg#olymp-calendar-icon"></use>
							</svg>
							<div class="post-date">
								<a class="h6 date" href="#"><?php echo date("Y/m/d H:i:s",$d1['creation_time']/1000 );?></a>
								<span>Date</span>
							</div>
						</div>
						<!-- <div class="post-comments-wrap inline-items">
							<svg class="olymp-comments-post-icon">
								<use xlink:href="<?php echo base_url();?>web_assets/icons/icons.svg#olymp-comments-post-icon"></use>
							</svg>
							<div class="post-comments">
								<a class="h6 comments" href="#">14</a>
								<span>Comments</span>
							</div>
						</div> -->
					</div>
				</div>
					</article>
				<div class="row">
					<div class="post-thumb col-12">
						<?php if($this->CI->remote_file_exists($d1['display_picture'],1)){ ?>
								<img src="<?php echo $d1['display_picture']; ?>" alt="author">
						 <?php } else {  ?>
                                 <img src="/web_assets/img/image_placeholder.jpg" alt="author">
                          <?php } ?>
					</div>
				
				 
					<div class="post-content-wrap col-12">
				
						<div class="post-content" style="font-family: Roboto!important;"> 
							<?php echo $d1['post_data']['text']; ?> 
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>
</div>

<?php } ?>
<div class="header-spacer"></div>
<?php $this->load->view('include/footer.php'); ?>



</body>
</html>