        <?php if(!empty($notifications)){ 
          $i=0;
          foreach($notifications['data'] as $notify){
            if($i <15){
              if(!empty($notify['post_id'])){
                $did = '1';
                $id = $notify['post_id'];
              }
              if(!empty($notify['user_id'])){
                $did = '2';
                $id = $notify['user_id'];
              }
        ?>
         <a class="content" href="<?php echo site_url('/web/Header/viewed_notifications/'.$notify['id']."/".$did."/".$id); ?>"> 
        <div class="notifications-wrapper">
           <div class="notification-item">
            <ul class="notification-list">
              <li style="background-color: <?php if($notify['view_state'] == 0){ echo "#d3def2;"; }else{echo "#fff;";}?>">
              <div class="author-thumb">
             <?php if ($this->CI->remote_file_exists($notify['action_performed_by']['profile_picture'], 1)) { ?>
                <img src="<?= $notify['action_performed_by']['profile_picture']; ?>" alt="author">
            <?php } else { ?>
                <img src="/web_assets/img/img_avatar.png" alt="author">
            <?php } ?>
              </div>
              <div class="notification-event">
               
              <span href="javascript:void(0);" class="h6 notification-friend"><?=$notify['action_performed_by']['name']." ";?> </span>  <span>
                <?php 
                              if($notify['activity_type'] == "post_like"){
                                echo $text = 'Liked your post. ';
                              }else if($notify['activity_type'] == "post_comment"){
                                echo $text = 'Commented on your post ';
                              }else if($notify['activity_type'] == "following"){
                                echo $text = 'Following You. ';
                              }else{
                                echo $text = 'Tagged you. ';
                              }?></span>                     
              <span href="#" class="notification-link"> </span>
              <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18"><?php echo $this->CI->time_ago_in_php($notify['creation_time']); ?></time></span>
              </div> 
              </li>
            </ul>
          </div> 
       </div>
      </a> 
      <?php $i++; } }  } ?>