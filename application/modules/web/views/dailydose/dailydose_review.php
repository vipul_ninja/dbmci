<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
    <title>Review of the Day</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <!-- Bootstrap CSS -->
   <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style type="text/css">
.vl {
    border-left: 2px solid white;
    height: 10px;
}
.modal1 {
  max-width: 900px;
  margin: 30px auto;
}
.modal2 {
  position:relative;
  padding:0px;
}
#myProgress {
    list-style: none;
    margin-top: 30px;
}
#myProgress li {
    background-color: #FAFAFA;
    margin-bottom: 10px;
    height: 60px;
    position: relative;
    margin-top: 21px;
}
.inner-sec {
    position: absolute;
    background-color: #28ff27;
    opacity: 0.2;
    height: 100%;
}
.city {
    padding: 10px;
    background-color: #ddd;
}
.city1 {
    padding: 21px;
    background-color: #ddd;
}

.a {
    background-color: white;
    border: 1px solid black;
    border-radius: 50%;
    width: 21px;
    height: 23px;
    padding-left: 4px;
    float: left;
}
.answer {
    margin-left: 25px;
}
.inner-sec-red {
    position: absolute;
    background-color: red;
    opacity: 0.2;
    height: 100%;
}

.badge-notify{
    background: red;
    position: relative;
    top: -12px;
    left: -12px;
    color: white;
    font-size: 12px;
  } 
  .quiz_box .quiz_box_image img {
    max-height:100%;
    max-width: 72%;
    margin: 14px 10px;
}
.post.shared-photo .post-thumb{height: 340px;}
.post-thumb img {
    height: 100%;
    width: 100%;
}

</style>
</head>

<body>
<div class="main-box">

<div class="header-spacer"></div>
<?php $this->load->view('../include/header');?>
<div class="container">
  <div class="row">
<?php if(!empty($d1)){ ?>
    <div class="col-md-8 offset-md-2">
      <h3 class="text-center revi-of-dy">Review of the Day</h3>
      <div class="page-flow">
            <ul class="breadcrumbs">
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/Feeds">Home</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/DailyDose">Daily Dose</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item active">
            <span>Review of the Day</span>
          </li>
        </ul>
      </div>
        <div  id="post_list">
      <?php $i = 0;$second_date = '';
        foreach($d1 as $post_list) {  
		$first_date = date("F d", $post_list['creation_time']/1000);
          ?>
          <?php if($first_date == $second_date){ 
            $second_date = $first_date;?>
          <?php }else{ 
            $second_date = $first_date;?>
           <h2 class="date">
            <?php 
              if($this->CI->check_time_ago_in_php($post_list['creation_time'])){
                echo date("F d", $post_list['creation_time']/1000); 
              }else{
                echo $this->CI->time_ago_in_php($post_list['creation_time']);
              }  
           ?></h2>
          <?php }?>
             <div class="quiz_box">
        <div class="row no-gutters">
        <div class="col-md-2 col-xl-2 col-lg-2 col-sm-3 col-4">
          <div class="quiz_box_image">
            <?php if($post_list['display_picture'] != ""){
                  $cover_image =  $post_list['display_picture'];
                }else{
                   $cover_image =  base_url('web_assets/img/makemyexam.png');
                }
            ?>
            <img src="<?php echo $cover_image; ?>" alt="author">
          </div>
        </div>
        <div class="col-md-8 col-xl-8 col-lg-8 col-sm-6 col-8">
          <div class="quiz_box_details">
            <h2><?php echo $post_list['post_headline']; ?></h2>
            <div class="quiz-box-date"><?php 
           
            if($this->CI->check_time_ago_in_php($post_list['creation_time'])){
              echo date("F d, Y", $post_list['creation_time']/1000);
           
           }else{
              echo $this->CI->time_ago_in_php($post_list['creation_time']);
            } 
             ?></div>
          </div>
          <div class="quiz_box_bottom">
            <ul class="list-inline">
              <li class="list-inline-item"> <span> <?php echo $post_list['likes']; ?> Likes</span></li>
               <li class="list-inline-item"> <span> <?php echo $post_list['comments']; ?> Comment </span></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2 col-xl-2 col-lg-2 col-sm-3">
          <div class="quiz_box_bookmar">
            <ul class="list-inline">
              <li class="list-inline-item"><a href="" class="bookmark" ><img src="<?php echo base_url();?>web_assets/img/bookmark.png" alt=""></a></li>
              <li class="list-inline-item"><a href="" class="bookmark"><img src="<?php echo base_url();?>web_assets/img/blueshare_xxhdpi.png" alt=""></a></li>
            </ul>
            <a  href="javascript:void(0);" class="btn btn-sm bg-blue" data-toggle="modal" data-target="#myModal<?=$post_list['id'];?>" style="margin-top: 37px;">Read More</a>
          </div>
        </div>
      </div>
 </div>

  

<!-- Window-popup Blog Post Popup -->

<div class="modal fade" id="myModal<?=$post_list['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal1 modal11" role="document">
<div class="modal-content">

 
  <div class="modal-body modal2">
        <div class="ui-block">
                                            <article class="hentry post has-post-thumbnail shared-photo">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
                                                <!------------------------------------ Upper feed Section  --------------------------------------->
                                                <div class="post__author author vcard inline-items">
                                                    <?php if ($this->CI->remote_file_exists($post_list['post_owner_info']['profile_picture'], 1)) { ?>
                                                        <img src="<?= $post_list['post_owner_info']['profile_picture'] ?>" alt="author" onclick="alert('Under construction');">
                                                    <?php } else { ?>
                                                        <i class="fa fa-user-circle" alt="author" style="font-size:45px;" onclick="alert('Under construction');"></i>
                                                    <?php } ?>

                                                    <div class="author-date">
                                                        <a  href="<?php echo site_url('/web/Profile/profiletest/' . $post_list['post_owner_info']['id']); ?>" ><?= $post_list['post_owner_info']['name'] ?>
                                                        </a>
                                                        <?php if ($post_list['pinned_post'] != '') { ?>
                                                            <span style="color:#ff5e3a">  Featured Post</span>     
                                                        <?php } ?>
                                                        <div class="post__date">
                                                            <time class="published" datetime="2017-03-24T18:18">
                                                                <?php echo $this->CI->time_ago_in_php($post_list['creation_time']); ?> 
                                                            </time>
                                                        </div>
                                                    </div>

                                                    <!-- <div class="more">
                                                                    <svg class="olymp-three-dots-icon">
                                                                    <use xlink:href="/web_assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                                                    </svg>
                                                                    <ul class="more-dropdown">
                                                                    <li><a href="#">Edit Post</a></li>
                                                                        <li><a href="#">Delete Post</a></li>
                                                                        <li><a href="#">Turn Off Notifications</a></li>
                                                                        <li><a href="#">Select as Featured</a></li>
                                                                    </ul>
                                                            </div> -->
                                                </div>
<!------------------------------------ End Upper feed Section  ------------------------------>                
                                                <div id="pre_text<?= $post_list['user_id'] ?>">
                                                    <span class="pre_text <?= $post_list['user_id'] ?> ">
<!----------------------------- For Text ---------------------------------------------------->
<?php if ($post_list['post_type'] == 'user_post_type_normal') { ?>
                                                            <div style="color:#000;" id="upper<?= $post_list['post_data']['id'] ?>">
                                                                <?php echo substr($post_list['post_data']['text'], 0, 100); ?>
                                                                <?php if (strlen($post_list['post_data']['text']) >= 100) { ?>....
                                                                    <span class="expand" id="expand<?= $post_list['user_id']; ?>" onclick="toggle_data(this.id,<?= $post_list['user_id'] ?>,<?= $post_list['post_data']['id'] ?>)"> 
                                                                        <a href="javascript:void(0);">show more</a>
                                                                    </span> 
                                                                <?php } ?>
                                                            </div>
                                                            <span style="display: none;" class="pre_text expand<?= $post_list['user_id']; ?> "><?= $post_list['post_data']['text']; ?> 
                                                            </span>
                                                            <?php if (strlen($post_list['post_data']['text']) >= 100) { ?>
                                                                <span class="expand" id="expand<?= $post_list['user_id']; ?>" onclick="toggle_data_c(this.id,<?= $post_list['user_id'] ?>,<?= $post_list['post_data']['id'] ?>)"> 
                                                                    <a id="hiexpand<?= $post_list['user_id']; ?>" style="display:none;" href="javascript:void(0);">show less</a>
                                                                </span>

                                                            <?php } ?> 
<!------------------------------------------------------------------------------------------->
<!------------------------ For Current Affair, Review, Article and Vocab -------------------->
<?php } else if ($post_list['post_type'] == 'user_post_type_current_affair' || $post_list['post_type'] == 'user_post_type_review' || $post_list['post_type'] == 'user_post_type_article' || $post_list['post_type'] == 'user_post_type_vocab') { ?>
                                                        <div style="color:#000;" id="upper<?= $post_list['post_data']['id'] ?>">
                                                            <?php echo substr($post_list['post_headline'], 0, 100); ?>
                                                            <?php if (strlen($post_list['post_headline']) >= 100) { ?>....
                                                                <span class="expand" id="expand<?= $post_list['user_id']; ?>" onclick="toggle_data(this.id,<?= $post_list['user_id'] ?>,<?= $post_list['post_data']['id'] ?>)"> 
                                                                    <a href="javascript:void(0);">show more</a>
                                                                </span> 
                                                            <?php } ?>
                                                        </div>
                                                        <span style="display: none;" class="pre_text expand<?= $post_list['user_id']; ?> "><?= $post_list['post_headline']; ?> 
                                                        </span>
                                                        <?php if (strlen($post_list['post_headline']) >= 100) { ?>
                                                            <span class="expand" id="expand<?= $post_list['user_id']; ?>" onclick="toggle_data_c(this.id,<?= $post_list['user_id'] ?>,<?= $post_list['post_data']['id'] ?>)"> 
                                                                <a id="hiexpand<?= $post_list['user_id']; ?>" style="display:none;" href="javascript:void(0);">show less</a>
                                                            </span>

                                                    <?php } ?> 
<!------------------------------------------------------------------------------------------->
<!---------------------------------------- For MCQ ------------------------------------------>
<?php } else if ($post_list['post_type'] == 'user_post_type_mcq') { ?>
                                                            <div style="color:#000;"><?php echo $post_list['post_data']['question']; ?>
                                                                <span style="float: right;border: 2px solid #f2f7ff;border-radius: 10px;padding: 3px;">
                                                                    <img src="/web_assets/img/coins.png" style="width: 30px;height: 30px;">
                                                                    <span style="color: orange;font-size: 16px;"><b>+<?= $post_list['post_data']['mcq_coins'] ?></b></span> Coins
                                                                </span>
                                                            </div>
                                                            <?php
                                                            if ($post_list['post_data']['answer_given_by_user'] != '') {
                                                                $rightclass = 'inner-sec';
                                                                $wrongclass = 'inner-sec-red';
                                                            } else {
                                                                $rightclass = '';
                                                                $wrongclass = '';
                                                            }
                                                            ?>
                                                            <ul id="myProgress">
                                                                <?php if ($post_list['post_data']['answer_three'] == "") { ?>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_1">  
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 1) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_1" style="width: <?= $post_list['post_data']['mcq_voting']['answer_one'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> A </div>
                                                                            <span class="answer" id="1" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_one']; ?> </span>   
                                                                        </div> 
                                                                    </li>

                                                                    <li  class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_2">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 2) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_2" style="width: <?= $post_list['post_data']['mcq_voting']['answer_two'] . '%'; ?>">

                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> B </div>
                                                                            <span class="answer" id="2" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_two']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                <?php } else if ($post_list['post_data']['answer_four'] == "") { ?>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_1">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 1) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_1" style="width: <?= $post_list['post_data']['mcq_voting']['answer_one'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> A </div>
                                                                            <span class="answer" id="1" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_one']; ?> </span>   
                                                                        </div> 
                                                                    </li>

                                                                    <li  class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_2">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 2) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_2" style="width: <?= $post_list['post_data']['mcq_voting']['answer_two'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> B </div>
                                                                            <span class="answer" id="2" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_two']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_3">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 3) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_3" style="width: <?= $post_list['post_data']['mcq_voting']['answer_three'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> C </div>
                                                                            <span class="answer" id="3" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_three']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                <?php } else if ($post_list['post_data']['answer_five'] == "") { ?>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_1">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 1) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_1" style="width: <?= $post_list['post_data']['mcq_voting']['answer_one'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> A </div>
                                                                            <span class="answer" id="1" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_one']; ?> </span>   
                                                                        </div> 
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_2">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 2) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_2" style="width: <?= $post_list['post_data']['mcq_voting']['answer_two'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> B </div>
                                                                            <span class="answer" id="2" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_two']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_3">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 3) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_3" style="width: <?= $post_list['post_data']['mcq_voting']['answer_three'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> C </div>
                                                                            <span class="answer" id="3" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_three']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_4">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 4) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_4" style="width: <?= $post_list['post_data']['mcq_voting']['answer_four'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> D </div>
                                                                            <span class="answer" id="4" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_four']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                <?php } else { ?>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_1">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 1) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_1" style="width: <?= $post_list['post_data']['mcq_voting']['answer_one'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> A </div>
                                                                            <span class="answer" id="1" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_one']; ?> </span>   
                                                                        </div> 
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_2">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 2) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_2" style="width: <?= $post_list['post_data']['mcq_voting']['answer_two'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> B </div>
                                                                            <span class="answer" id="2" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_two']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_3">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 3) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_3" style="width: <?= $post_list['post_data']['mcq_voting']['answer_three'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> C </div>
                                                                            <span class="answer" id="3" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_three']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_4">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 4) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_4" style="width: <?= $post_list['post_data']['mcq_voting']['answer_four'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> D </div>
                                                                            <span class="answer" id="4" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_four']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                    <li class="mcqop" main-id="<?= $post_list['post_data']['id'] ?>_5">
                                                                        <div class="<?php
                                                                        if ($post_list['post_data']['answer_given_by_user'] == 5) {
                                                                            if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                                echo $rightclass;
                                                                            } else {
                                                                                echo $wrongclass;
                                                                            }
                                                                        }
                                                                        ?>" id="<?= $post_list['post_data']['id'] ?>_5" style="width: <?= $post_list['post_data']['mcq_voting']['answer_five'] . '%'; ?>">
                                                                        </div>
                                                                        <div class="city1">
                                                                            <div class="a"> E </div>
                                                                            <span class="answer" id="5" mcq="<?= $post_list['post_data']['id']; ?>"><?php echo $post_list['post_data']['answer_five']; ?> </span>
                                                                        </div>
                                                                    </li>

                                                                <?php } ?>
                                                            </ul>

                                                            <?php
                                                            if ($post_list['post_data']['answer_given_by_user'] != '') {
                                                                if ($post_list['post_data']['answer_given_by_user'] == $post_list['post_data']['right_answer']) {
                                                                    ?>
                                                                    <div id="coins_<?= $post_list['post_data']['id']; ?>" style="background-color: #FFC107;padding: 5px;color: #030303;font-size: 14px;margin-bottom: 5px;">            Congrats you earned <?= $post_list['post_data']['mcq_coins']; ?> Coins
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                            <div id="coins_<?= $post_list['post_data']['id']; ?>" style="background-color: #FFC107;padding: 5px;color: #030303;font-size: 14px;margin-bottom: 5px;display: none">Congrats you earned <?= $post_list['post_data']['mcq_coins']; ?> Coins
                                                            </div>
<?php } ?>
<!------------------------------------- End MCQ ---------------------------------------------->
                                                    </span>
                                                </div>                
<!------------------------------------------------------------------------------------------->
<!------------------------ File Type like image, video and pdf ------------------------------>
 <?php if ($post_list['post_type'] == 'user_post_type_normal') { ?>
                                            <?php if (!empty($post_list['post_data']['post_file'])) { ?>
<!------------------------ File Type Image ------------------- ------------------------------->                          
                                                <?php if ($post_list['post_data']['post_file'][0]['file_type'] == 'image') { ?>

                                                    <div class="post-thumb" style="height: 340px;">
                                                        <img class="post_img" src="<?= $post_list['post_data']['post_file'][0]['link'] ?>" alt="photo" style="width:100%;height:100%;">
                                                    </div>
<!----------------------------------------------------------------------------------------------->
<!------------------------ File Type Video ------------------------------------------------------>
                                                <?php } else if ($post_list['post_data']['post_file'][0]['file_type'] == 'video') { ?>

                                                    <div class="video-thumb">
                                                        <img src="/web_assets/img/video9.jpg" alt="photo">
                                                        <a href="https://youtube.com/watch?v=excVFQ2TWig" class="play-video">
                                                            <svg class="olymp-play-icon">
                                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-play-icon"></use>
                                                            </svg>
                                                        </a>
                                                    </div>                   
<!----------------------------------------------------------------------------------------------->
<!------------------------ File Type PDF -------------------------------------------------------->
                                                <?php } else if ($post_list['post_data']['post_file'][0]['file_type'] == 'pdf') { ?>

                                                    <div class="post-thumb" style="border:none;">
                                                        <a href="<?= $post_list['post_data']['post_file'][0]['link']; ?>" target="_blank" title="Click to Download PDF"> 
                                                            <img src="/web_assets/img/PDF-Download.jpg" alt="photo" style="width:auto; height:300px;">
                                                        </a>
                                                    </div>

                                                <?php } ?>
<!----------------------------------------------------------------------------------------------->
                                            <?php } ?>
<?php } else if ($post_list['post_type'] == 'user_post_type_current_affair' || $post_list['post_type'] == 'user_post_type_vocab' || $post_list['post_type'] == 'user_post_type_article' || $post_list['post_type'] == 'user_post_type_review') { ?>

<!------------------------ File Type Image ------------------- ------------------------------->                          
                                             
                                                  <div class="post-thumb" style="height:340px;">
                                                      <img class="post_img" src="<?= $post_list['display_picture']; ?>" alt="photo" style="width:100%;height:100%;">
                                                  </div>
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<?php } ?>
                                                <?php if (!empty($post_list['tagged_people'])) { ?>
                                                    <b style="font-size: 12px; color: #babec4">Tagged People:
                                                        <br> 
                                                        <div class="tagg_sect">
                                                            <?php foreach ($post_list['tagged_people'] as $tag) { ?>
                                                                <span><?= $tag['name']; ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    </b>
                                                <?php } ?>
                                                <?php if ($post_list['post_type'] == 'user_post_type_mcq') { ?>

                                                    <div style="font-weight: 500;color: #B4B4B4;font-size: 13px;margin-bottom: 5px;margin-top: 5px;">
                                                        <span><?= $post_list['post_data']['mcq_attempt']; ?></span> attempts |  
                                                        <span class="count-action" id="like<?= $post_list['post_data']['post_id'] ?>">
                                                            <?php
                                                            if ($post_list['likes'] > 0) {
                                                                echo $post_list ['likes'];
                                                            }
                                                            ?>
                                                        </span> 
                                                        <span>
                                                            <?php
                                                            if ($post_list['likes'] == 1) {
                                                                echo 'Like';
                                                            } else {
                                                                echo "Likes";
                                                            }
                                                            ?>
                                                        </span> | 
                                                        <span id="comment_count<?= $post_list['post_data']['post_id'] ?>"><?= $post_list ['comments']; ?></span> comments
                                                    </div>

                                                <?php } else { ?>
                                                    <div style="font-weight: 500;color: #B4B4B4;font-size: 13px;margin-bottom: 5px;margin-top: 5px;">
                                                        <span class="count-action" id="like<?= $post_list['post_data']['post_id'] ?>">
                                                            <?php
                                                            if ($post_list['likes'] > 0) {
                                                                echo $post_list ['likes'];
                                                            }
                                                            ?>
                                                        </span> 
                                                        <span>
                                                            <?php
                                                            if ($post_list['likes'] == 1) {
                                                                echo 'Like';
                                                            } else {
                                                                echo "Likes";
                                                            }
                                                            ?>
                                                        </span> | 
                                                        <span  id="comment_count<?= $post_list['post_data']['post_id'] ?>"><?= $post_list ['comments']; ?></span> comments
                                                    </div>
                                                <?php } ?>                 
<!------------------------------------------------------------------------------------------------>
<!-------------------------------------- Likes --------------------------------------------------->
                                                <div class="post-additional-info inline-items">
                                                    <?php
                                                    if ($post_list['is_liked'] == 1) {
                                                        $color = "#38a9ff";
                                                        $s = 1;
                                                        $dcolor = 'dcolor';
                                                        $hov = '';
                                                    } else {
                                                        $color = "#717f8f";
                                                        $s = 0;
                                                        $dcolor = 'gcolor';
                                                        $hov = 'hov';
                                                    }
                                                    ?>    
                                                    <a href="javascript:void(0)" main-like="<?= $s ?>" id="lik<?= $post_list['post_data']['post_id'] ?>" onclick="likeF('<?= $post_list['post_data']['post_id'] ?>')" class="post-add-icon inline-items <?= $dcolor; ?> <?= $hov; ?>">
                                                        <!-- <svg class="olymp-like-post-icon-hand" >
                                                            <use xlink:href="<?php echo base_url(); ?>web_assets/svg-icons/sprites/icons.svg#olymp-like-post-icon-hand" ></use>
                                                        </svg> -->
                                                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                                        <span>Like</span>
                                                    </a> 
<!------------------------------------------------------------------------------------------->
<!---------------------------------------- Comment --------------------------------------->
                                                    <a href="javascript:void(0);"  class="post-add-icon inline-items" style="text-align:center;" 
                                                       id="<?= $post_list['post_data']['post_id']; ?>" onclick="comnts(this.id)">
                                                        <!-- <svg class="olymp-comments-post-icon">
                                                            <use xlink:href="<?php echo base_url(); ?>web_assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                                                        </svg>   -->
                                                        <i class="fa fa-commenting-o" aria-hidden="true"></i>
                                                        <span>Comment</span>
                                                    </a>
<!------------------------------------------------------------------------------------------->
<!--------------------------------- Share  -------------------------------------------------->
                                                    <a data-toggle="modal" data-target="#share-post" style="text-align:right;"  class="post-add-icon inline-items">
                                                        <!-- <img src="<?php //echo base_url('/web_assets/img/blueshare_xxhdpi.png'); ?>">  -->
                                                        <i class="fa fa-share-alt-square" aria-hidden="true"></i>
                                                        <span>Share</span>
                                                    </a>
<!------------------------------------------------------------------------------------------->
                                                </div>
                                            </article>
<!------------------------------- End Article------------------------------------------------>
                                            <div id="commt<?= $post_list['post_data']['post_id'] ?>" class="" style="display:none">
                                                <ul class="comments-list showmydata" id="load_comment<?= $post_list['post_data']['post_id'] ?>" ></ul>
                                                <form  class="comment-form inline-items" >
                                                    <div class="post__author author vcard inline-items">
                                                        <?php if ($this->CI->remote_file_exists($this->session->pro_img, 1)) { ?>
                                                            <img src="<?= $this->session->pro_img ?>" alt="author">
                                                        <?php } else { ?>
                                                            <i class="fa fa-user-circle" alt="author" style="font-size:28px"></i>
                                                        <?php } ?>
                                                        <div class="form-group with-icon-right ">
                                                            <textarea class="form-control" placeholder="" id="commentdata<?= $post_list['post_data']['post_id'] ?>">
                                                            </textarea> 
                                                        </div>
                                                    </div>
                                                    <?php if (isset($this->session->userdata['username'])) { ?>
                                                        <button type="button" class="btn btn-md-2 btn-primary" class="more-comments" onclick="postComment('<?= $post_list['post_data']['post_id'] ?>')">Comment</button>
                                                    <?php } else { ?>
                                                        <button type="button" class="btn btn-md-2 btn-primary" class="more-comments" data-toggle="modal" data-target="#registration-login-form-popup">Comment</button>
                                                    <?php } ?>
                                                    <button type="button" class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color"  onclick="comnts(<?= $post_list['post_data']['post_id'] ?>, 'cancel')" >Cancel</button>
                                                </form>
                                            </div>
                                        </div>   

    
  </div>

</div>
</div>
</div> 

<!-- ... end Window-popup Blog Post Popup -->


    <?php } ?>
    <?php  $pid = $post_list['post_data']['post_id'] ;  ?>
            <a id="search<?= $pid ?>" main="all" onclick="search_filter(<?=$pid;?>)"  class="btn btn-control btn-more">
                <svg class="olymp-three-dots-icon">
                <use xlink:href="<?php echo base_url(); ?>web_assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                </svg>
            </a>
        <?php }else{ ?>
               <center><h4>No feeds found!!</h4></center>
          <?php  } ?>
    </div>
   
      </div>
    </div>
    <div class="header-spacer"></div>

    <!-- Window-popup Share Post -->
<div class="modal fade" id="share-post" tabindex="-1" role="dialog" aria-labelledby="share-post" aria-hidden="true">
  <div class="modal-dialog window-popup edit-widget edit-widget-twitter" role="document">
    <div class="modal-content">
      <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
        <svg class="olymp-close-icon"><use xlink:href="<?php echo base_url('/web_assets/svg-icons/sprites/icons.svg#olymp-close-icon');?>"></use></svg>
      </a>
      <div class="modal-header">
        <h6 class="title">Share post by</h6>
      </div>
      <div class="modal-body">
         <a href="http://www.facebook.com/sharer.php?u=https://www.ibtindia.com/" class="btn bg-facebook btn-lg full-width btn-icon-left" target="_blank"><i class="fab fa-facebook" aria-hidden="true"></i>Share by Facebook</a>
         <a href="https://plus.google.com/share?url=https://www.ibtindia.com/" class="btn bg-google  btn-lg full-width btn-icon-left" target="_blank"><i class="fab fa-google-plus" aria-hidden="true"></i>Share by Google Plus</a>
         <a href="http://twitter.com/share?text=IBT&url=https://www.ibtindia.com/" class="btn bg-twitter btn-lg full-width btn-icon-left" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i>Share by Twitter</a>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ... end Window-popup Share Post -->
<?php $this->load->view('../include/footer');?>
</div>
  <?php


function remote_file_exists( $source, $extra_mile = 0 ) { 
     
                                        # EXTRA MILE = 0 
                                        ////////////////////////////////// Does it exist? 
                                        # EXTRA MILE = 1 
                                        ////////////////////////////////// Is it an image? 
                                             
                                            if ( $extra_mile === 1 ) { 
                                                    $img = @getimagesize($source); 
                                                     
                                                    if ( !$img ) return 0; 
                                                    else 
                                                    { 
                                                            switch ($img[2]) 
                                                            { 
                                                                    case 0: 
                                                                            return 0;     
                                                                            break; 
                                                                    case 1: 
                                                                            return $source; 
                                                                            break; 
                                                                    case 2: 
                                                                            return $source; 
                                                                            break; 
                                                                    case 3: 
                                                                            return $source; 
                                                                            break; 
                                                                    case 6: 
                                                                            return $source; 
                                                                            break; 
                                                                    default: 
                                                                            return 0;         
                                                                            break; 
                                                            } 
                                                    } 
                                            } 
                                            else 
                                            { 
                                                if (@FClose(@FOpen($source, 'r')))  
                                                            return 1; 
                                                 else 
                                                            return 0; 
                                            } 
                                    }  

?>         

<!-- JS Scripts -->
<script type="text/javascript">
  
//  ================================= Show Comment Toggle and call Comment List ===========================
    function comnts(id,data=''){
      $('#commt'+id).slideToggle(); 
        if(data != 'cancel' ){
          comment(id); 
        }    
    }
//  =================================post like dislike=====================================
                    function likeF(pid){
                       var attrs = $('#lik'+pid).attr('main-like');
                      if(attrs==1){
                        $('#lik'+pid).css({"fill":"#717f8f","color":"#717f8f"});
                        $('#lik'+pid).attr('main-like','0');
                      }else{
                        $('#lik'+pid).css({"fill":"#38a9ff","color":"#38a9ff"});
                        $('#lik'+pid).attr('main-like','1');
                      }
                            $.ajax({
                                url: '<?= site_url('web/Feeds/like') ?>',
                                type: 'POST',
                                dataType: 'json',
                                data: {post_id : pid},
                                beforeSend: function() {
                                },
                                success: function(data){

                                    if(data.status){
                                        $("#like"+pid).html(data.data.likes);
                                    }else{
                                        $.ajax({
                                            url: '<?= site_url('web/Feeds/dislike') ?>',
                                            type: 'POST',
                                            dataType: 'json',
                                            data: {post_id : pid},
                                            success: function(data){
                                                //alert(data.data.likes);
                                                $("#like"+pid).html(data.data.likes); 
                                            }
                                        });
                                    }
                                },
                                complete: function(){
                                }

                            });
                    }
        //==========================================end post likke dislike==============================
//=======================================comment======================================================
          function comment(postid) {
                
                $.ajax({
                    type: 'Post',
                    data: {postid: postid},
                    url: '<?php echo site_url(); ?>/web/Feeds/comment',
                    success: function (data) {
                      // console.log(data);
                        $('#load_comment'+postid).html(data);
                    },
                    error: function (err) {
                        console.log("error" + err);
                    }
                }); 
            }
             function reply(post_id,comment_id,data='') {
              if(data == 'cancel' || data == '2'){
                $('#sub'+comment_id).css({"display":"none"}); 
                $('.rep'+comment_id).attr('main', '1');
              }else{
                $('#sub'+comment_id).css({"display":"block"});
                $('.rep'+comment_id).attr('main', '2');
                    $.ajax({
                        type: 'Post',
                        data: {postid: post_id,comment_id:comment_id},
                        url: '<?php echo site_url(); ?>/web/Feeds/reply',
                        success: function (data) {
                            $('#sub'+comment_id).html(data);
                        },
                        error: function (err) {
                            console.log("error" + err);
                        }
                    });
              }
            }
            // function search_filter(page_num) { 
            //     page_num = page_num ? page_num : 0;
            //     //alert(page_num);
            //     var type = $('#search'+page_num).attr('main');
            //     if($('#searching').val() == ""){
            //     	var details = {last_post_id:page_num,type:type};
            //     }else{
            //     	var search = $('#searching').val();
            //     	var details = {last_post_id:page_num,type:type,search:search};
            //     }
            //     $.ajax({
            //         type: 'POST',
            //         url: '<?php //echo site_url('web/Feeds/ajax_post'); ?>',
            //         data: details,

            //         beforeSend: function () {
            //             $('.loader').show();
            //         },
            //         success: function (html) {
            //             $('#post_list').append(html);
            //             $('#search'+page_num).css({"display":"none"});
            //             $('.loader').fadeOut("slow");
            //         }
            //     });
            // }


            //=============================================reply post------------------=================                    
      function replyComment(pid,comment_id){                                                
        var comd = $("#replydata"+pid).val();                        
          if(comd == ""){                            //do nothing 
            
          }else{                            
            $.ajax({                                
                url: '<?php echo site_url('web/Feeds/replyComment'); ?>',                                
                type: 'POST',                                
                dataType:'json',                                
                data:{post_id: pid, commentmsg: comd,parent_id:comment_id},                                
                beforeSend: function() {                                  
                  $("#wave").show();                                
                },                                
                success: function(data){                                   
                 if(data.status){                                        
                  reply(pid,comment_id);                                        
                  $("#replydata"+pid).val('');                                   
                  }                                                                  
                },                                
                error:function(err){                                    
                //alert("ERR"+err);                                
              },                                
              complete: function(){                                    
                $("#wave").hide();                                
              }                            
        });                        
      }  
  }                  
   //==================================End comment====================================================== 
            //=============================================comment post------------------=================
                    function postComment(pid){
                        
                        var comd = $("#commentdata"+pid).val();

                        if(comd == ""){
                            //do nothing
                        }else{
                            $.ajax({
                                url: '<?= site_url('web/Feeds/postComment') ?>',
                                type: 'POST',
                                dataType:'json',
                                data:{post_id: pid, commentmsg: comd},
                                beforeSend: function() {
                                  $("#wave").show();
                                },
                                success: function(data){
                                    if(data.status)
                                    {
                                        comment(pid); // get comments
                                        $("#commentdata"+pid).val('');
                                    }                                  
                                },
                                error:function(err){
                                    //alert("ERR"+err);
                                },
                                complete: function(){
                                    $("#wave").hide();
                                }
                            });
                        }
                    }
   //==================================End comment======================================================        

    //=================================== content toggle ===================================
                     function toggle_data(id, dt,post_id) {
                        $('.' + id).show();
                        // $('#expand' + id).hide();
                        // $('#pre_text' + dt).hide();
                        $('#upper' + post_id).css('display','none');
                        $('#hi' + id).show();
                    }
                    function toggle_data_c(id, dt,post_id) {
                        $('.' + id).hide();
                        // $('#pre_text' + dt).show();
                          $('#upper' + post_id).css('display','block');
                        $('#hi' + id).hide();
                    }
        //=================================== end content toggle===================================
$(document).ready(function(){
  $('.mcqop').on('click', this , function(){
    var answer = $(this).find('span').attr('id');
    var mcq_id = $(this).find('span').attr('mcq');
    var id = $(this).attr('main-id');
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: '<?php echo site_url('web/Feeds/mcq_answer'); ?>',
      data: {answer:answer,mcq_id:mcq_id},
      success: function (data) {
        if(data.status == true){
          if(data.data.post_data.right_answer == answer){
            $('#'+id).addClass('inner-sec');
            $('#coins_'+mcq_id).html('Congrats you earned '+ data.data.post_data.mcq_coins + 'Coins');
          }else{
            $('#'+id).addClass('inner-sec-red');
          }
        }
      }
    });
  });
});

  ///==============================================PAGINATION===============================================
   function search_filter(page_num) { 
                page_num = page_num ? page_num : 0;
                //alert(page_num);
                var type = $('#search'+page_num).attr('main');
               
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('web/DailyDose/ajax_post'); ?>',
                    data: {last_post_id:page_num,type:"4"},

                    beforeSend: function () {
                        $('.loader').show();
                    },
                    success: function (html) {

                        $('#post_list').append(html);
                        $('#search'+page_num).css({"display":"none"});
                        $('.loader').fadeOut("slow");
                    }
                });
            }
//======================================END PAGINATION===============================================
</script>
</body>
</html>

