<!DOCTYPE html>
<html lang="en">
<head>
<title>Quiz</title>
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<!-- Main Font -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/web_assets/js/webfontloader.min.js"></script>
<script src="<?php echo base_url(); ?>web_assets/js/webfontloader.min.js"></script>
         <script src="<?php echo base_url(); ?>web_assets/js/quiz.js"></script>
        <script src="<?php echo base_url(); ?>web_assets/js/ssc.js"></script>
      <script>
            WebFont.load({
                google: {
                    families: ['Roboto:300,400,500,700:latin']
                }
            });
        </script>

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">

<!-- Main Styles CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
<link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
        <style>
#footer {
    position: fixed;
    bottom: 0;
    width: 100%;
    background-color: #333333 ;
    color:white;
    text-align:center;
    padding:5px;
}

.checkbox .checkbox-material .check {
    position: relative;
    display: inline-block;
    width: 15px;
    height: 15px;
    border: 1px solid #000000!important;
    overflow: hidden;
    z-index: 1;
    border-radius: 3px!important;
}


.checkbox label {
    cursor: pointer;
    padding-left: 0;
    margin-bottom: 0;
    padding: 8px;
}
.test-title {
    padding: 12px 20px 26px 20px !important;
}
</style>
</head>
<body>

<!-- Header-BP -->
<header class="header bg-blue" id="site-header">
  <div class="page-title test-title" style="padding: 12px 20px 26px 20px !important;">
   <img src="<?php echo base_url(); ?>web_assets/img/logo.png" >
  </div>
  <div class="header-content-wrapper"> </div>
</header>

<!-- ... end Header-BP --> 

<!-- Responsive Header-BP -->

<header class="header header-responsive bg-blue" id="site-header-responsive"> 
<a href="" class="logo">
				<div class="img-wrap">
					<img src="/web_assets/img/logo-colored-small.png" alt="Olympus">
				</div>
				<div class="title-block">
					<h6 class="logo-title">IBT</h6>
					<div class="sub-title">Online Exam</div>
				</div>
			</a>
  
</header>

<!-- ... end Responsive Header-BP -->
<div class="header-spacer"></div>

<!-- ... start test tabs -->
<form>
<div class="container-fluid">
  <div class="row">
    <div class="col col-xl-10 col-lg-8 col-md-12 col-sm-12 col-12">
      <div class="ui-block-title">
        <div class="h6 title text-center"><?php echo $test_data['basic_info']['test_series_name'];?></div>
 </div>
      <div class="ui-block">
        <!-- News Feed Form  -->
          <div class="news-feed-form">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"> <a class="nav-link active inline-items subs" data-toggle="tab" main="home-1" href="#home-1" role="tab" aria-expanded="true"><span>Physics</span> </a> </li>
            <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="che" href="#che" role="tab" aria-expanded="false"><span>Chemistary</span> </a> </li>
            <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="bio" href="#bio" role="tab" aria-expanded="false"> <span>Bioloagy</span> </a> </li>
            <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="math" href="#math" role="tab" aria-expanded="false"> <span>Mathematic</span> </a> </li>
             <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="hin" href="#hin" role="tab" aria-expanded="false"> <span>Hindi</span> </a> </li>
              <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="eng" href="#eng" role="tab" aria-expanded="false"> <span>English</span> </a> </li>
          </ul>
          <!-- Tab panes -->
          <span id="total" totalq="<?php echo $total = $test_data['basic_info']['total_questions']; ?>"></span>
          <div class="tab-content" id="tabData">
          <div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">
            <?php
              static $index;
              $test_data1 = $test_data['question_bank'];

              $count = count($test_data1);
              $que_id = array();
              $data = array();

              for ($i = 0; $i < $count; $i++) {
                  $index = $i + 1;
                  if ($i == 0) {
                      $style = 'block';
                  } else {
                      $style = 'none';
                  }
            ?>                         
            <span class="local_data" test_id=<?php echo $test_data['basic_info']['id'];  ?>
                                       question_id= <?php echo  $test_data1[$i]['id']; ?>  ></span>
            <div class="content" id="content-<?php echo $i + 1; ?>" style="display: <?php echo $style; ?>;">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <div class="question-language">
                      <span>Question <?php echo $i + 1; ?>.</span>
                      <span class="choose-language">
                        <div class="w-select">
                          <div class="title">Language</div>
                          <fieldset class="form-group">
                            <select class="selectpicker form-control">
                              <option value="DA">English</option>
                              <option value="NU">Hindi</option>
                            </select>
                          </fieldset>
                        </div>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <p> <?php echo $test_data1[$i]['question'] ?></p>
                  </div>
                  <div class="col-md-6">
                    <div class="radio">
                        <label id="color1<?php echo $i + 1; ?>" class='cls'  seq="<?php echo $i + 1; ?>" >
                            <input type="radio" class="radio<?php 
                            echo ($i + 1) . " ";
                            if ($count == ($i + 1)) {
                                echo 'last_que';
                            }
                            ?>" name="optionsRadios<?php echo $i + 1; ?>" value="<?php echo $test_data1[$i]['option_1'] ?>" id="1" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                            A.
                            <?php echo $test_data1[$i]['option_1'] ?> </label>
                            <img id="1_<?php echo  $test_data1[$i]['id']; ?>" src="">
                    </div>

                    <div class="radio">
                        <label id="color2<?php echo $i + 1; ?>" class='cls' seq="<?php echo $i + 1; ?>" >
                            <input type="radio" class="radio<?php
                            echo ($i + 1) . " ";
                            if ($count == ($i + 1)) {
                                echo 'last_que';
                            }
                            ?>" name="optionsRadios<?php echo $i + 1; ?>" value="<?php echo $test_data1[$i]['option_2'] ?>" id="2" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                            B.
                            <?php echo $test_data1[$i]['option_2'] ?>
                         </label>
                         <img id="2_<?php echo  $test_data1[$i]['id']; ?>" src="">
                    </div>

                    <div class="radio">
                        <label id="color3<?php echo $i + 1; ?>" class='cls' seq="<?php echo $i + 1; ?>" >
                            <input type="radio" class="radio<?php
                            echo ($i + 1) . " ";
                            if ($count == ($i + 1)) {
                                echo 'last_que';
                            }
                            ?>" name="optionsRadios<?php echo $i + 1; ?>"  value="<?php echo $test_data1[$i]['option_3'] ?>" id="3" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                            C.
                            <?php echo $test_data1[$i]['option_3'] ?> </label>
                            <img id="3_<?php echo  $test_data1[$i]['id']; ?>" src="">
                    </div>

                    <div class="radio">
                        <label id="color4<?php echo $i + 1; ?>" class='cls' seq="<?php echo $i + 1; ?>" >
                            <input type="radio" class="radio<?php
                            echo ($i + 1) . " ";
                            if ($count == ($i + 1)) {
                                echo 'last_que';
                            }
                            ?> " name="optionsRadios<?php echo $i + 1; ?>" value="<?php echo $test_data1[$i]['option_4'] ?>" id="4" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                            D.
                            <?php echo $test_data1[$i]['option_4'] ?> </label>
                            <img id="4_<?php echo  $test_data1[$i]['id']; ?>" src="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
          
          </div>
        </div>
        <!-- ... end News Feed Form  -->
      </div>
    </div>
    <aside class="col col-xl-2 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
      <div class="ui-block"> 
        
        <!-- W-Activity-Feed -->
        
        <ul class="widget w-activity-feed notification-list">
          <li>
            <div class="author-thumb"> <img src="img/avatar49-sm.jpg" alt="author"> </div>
            
            <div class="notification-event">
            <span class="h6 notification-friend"><?php  if($this->session->userdata){ echo $this->session->userdata('name');}else{ echo "Ajay Singh"; }?></span>
							
							<span class="notification-date"><time class="entry-date updated bold" datetime="2004-07-24T18:18">Time Left: 10:00 </time></span>
               <input type="hidden" id="time_val"/><br>

                                   

						</div>

          </li>
        </ul>
        
        <!-- .. end W-Activity-Feed --> 
      </div>
      <div class="ui-block">
        <div class="ui-block-title">
          <h6 class="title">You are viewing Physics section Question Palette</h6>
        </div>
        
        <!-- W-Activity-Feed -->
        <div class="question-status">
                                
          <ul>
              <?php
              $total = $test_data['basic_info']['total_questions'];
              for ($i = 0; $i < $total; $i++) {
                  ?>
                  <li><a id="optionsRadios<?php echo $i + 1; ?>" class="common tagged current1" current ="1" onclick="change_que('<?php echo $i + 1; ?>')" >Q<?= $i + 1; ?></a></li>
              <?php } ?>
          </ul>
      </div>
        
         <div class="gides">
              <h6>Legends:</h6>
              <ul class="list-inline">
            <!-- <li><span> <span class="statistics-point answerd"></span> Answerd </span></li> -->
            <li><span> <span class="statistics-point not-visited"></span> Correct </span></li>
            <li><span> <span class="statistics-point marked"></span>Incorrect </span></li>
            <li><span> <span class="statistics-point not-answerd"></span>Not Answerd </span></li>
          </ul>
        </div>
        <!-- <div class="action">
          
            <button class="btn btn-border-think c-grey btn-transparent custom-color">View QP</button>
            <button class="btn btn-border-think c-grey btn-transparent custom-color" style="float: right;">Instructions</button>
            <br>
            <button class="btn btn-blue full-width">Submit</button>
          
        </div> -->
        
        <!-- .. end W-Activity-Feed --> 
      </div>
    </aside>
    
   <!--  <div class="fixed-bottom for-submit-test">
    <div class="post-additional-info inline-items"> <a href="#" class="btn btn-smoke btn-sm btn-light-bg">Mark For Review</a><a href="#" class="btn btn-smoke btn-sm btn-light-bg">Skip</a> <a href="#" class="btn btn-smoke btn-sm btn-light-bg">Reset</a>
                <div class="comments-shared"> <a href="#" class="btn btn-blue btn-md-2">Save & Next</a> </div>
              </div>
    
    </div> -->
  </div>
</div>
</form>
<!--================================== Result Popup Modal =======================================-->

            <div id="myModal" class="modal" style="z-index:99;">

                <!-- Modal content -->
                <div class="modal-content">
        <!--            <span class="close">&times;</span>-->

                    <div style="text-align: center;"><h3>Test Summary</h3></div>
                    <table border="1" style="text-align:center;cellpadding:10px;padding:10px;font-size:20px;width:100%;">
                        <tr style="background:grey;color:white;">
                        <b>
                            <th>Title</th>
                            <th>Correct Answer</th>
                            <th>Marks</th>
                            <th>Time Spent</th>
                            <th>User Rank</th>
                        </b>
                        </tr>
                        <tr id="tr_id">

                        </tr>


                    </table>
                    <div style="text-align: center; margin-top:10px; "><h5><a id="show_res"  href="<?php echo site_url(); ?>/web/My_test/result_question/53/">View Result</a></h5></div>
                    <div style="text-align: center; margin-top:10px; "><h5><a id="leader_board"   href="<?php echo site_url(); ?>/web/DailyDose/basic_rank_result/">LeaderBoard</a></h5></div>
                </div>

            </div>

<!--==================================End Result Popup Modal =======================================-->

<a class="back-to-top" href="#"> 
  <img src="/web_assets/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
 </a>
<div  id="footer" class="clear" >
Copyright © 2018 MakeMyExam INDIA, All Rights Reserved.</div>
<!-- ... end start test tabs --> 
 <script>

            function startTest_Timer() {
                var duration = 60 * 10,
                        display = document.querySelector('#time');
                var timer = duration,
                        minutes, seconds;
                var s = 0;
               var myVar = setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;

                    display.textContent = minutes + ":" + seconds;
                    s++;
                    $("#time_val").val(s);

                    if (--timer < 0) {
                        clearInterval(myVar)
                        result();
                    }
                }, 1000);
            }


// =============================Toggle Screen==============================================================

    $(document).ready(function() {
  
        var array=[];
        var flag =0;
        function settestid(test_ids){

            var st = localStorage.getItem("eTvZ(5@9ETs");
            
            if(st){
            var decodestring = atob(st);
            var ss = JSON.parse(decodestring);
          
            for(let i=0;i<ss.length;i++){
                  if(test_ids == ss[i]){  //check whether two same id
                   flag=1; break;
                   }
            }

            if(flag!=1){
                    flag=0;
                    
                    var finalArray = ss.map(function (obj) {
                    return obj.test_id;
                    });
                  
                    ss.push(test_ids);
                    var string = JSON.stringify(ss);
                    var encodedString = btoa(string);
                    localStorage.setItem("eTvZ(5@9ETs", encodedString);
                   return;
                }
            }

            else{
             
            // var myArray1 ={test_id: test_ids};

            array.push(test_ids);
            var string = JSON.stringify(array);
            var encodedString = btoa(string);
            localStorage.setItem("eTvZ(5@9ETs", encodedString);
            }
            var st = localStorage.getItem("eTvZ(5@9ETs");
            var decodestring = atob(st);
            var ss = JSON.parse(decodestring);
           
            var finalArray = ss.map(function (obj) {
            return obj.test_id;
            });


      }//end of function
       
        settestid(<?php echo $test_data['basic_info']['id']; ?>);

        
  //==========================before close quiz========================================================


                window.onbeforeunload = function (e) {
                  var e = e || window.event;

                  //IE & Firefox
                  if (e) {
                    e.returnValue = 'Are you sure?';
                  }

                  // For Safari
                  return 'Are you sure?';
                }; 


//=======================================Tab Test Series=================================================


  $('.subs').on('click', this, function(){
    var Url = "<?php echo site_url(); ?>";
    $.ajax({
          type: 'post',
          url: Url + '/web/DailyDose/ajax_subject_wise_series',
          data: {main:$(this).attr('main')},
          success: function (data) {
            $('#tabData').html(data);
          }, error: function (data) {
            console.log("error" + data);
          }
      });
  });

//=======================================================================================================
 });
 
  //==========================END before close quiz========================================================       
            
               $('#start').click(function (e) {
                   
                var ckb_status = $("#term_chkbox").prop('checked');
               if(ckb_status==1){
                    $('#form').css({
                        "display": "block"
                    });
                    $('#instruct').css({
                        "display": "none"
                    });

                    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
                            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                        if (document.documentElement.requestFullScreen) {
                            document.documentElement.requestFullScreen();
                        } else if (document.documentElement.mozRequestFullScreen) {
                            document.documentElement.mozRequestFullScreen();
                        } else if (document.documentElement.webkitRequestFullScreen) {
                            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                        }
                        if (e.requestFullscreen) {
                            e.requestFullscreen();
                        } else if (e.mozRequestFullScreen) {
                            e.mozRequestFullScreen();
                        } else if (e.webkitRequestFullscreen) {
                            e.webkitRequestFullscreen();
                        } else if (e.msRequestFullscreen) {
                            e.msRequestFullscreen();
                        } else {
                            if (document.cancelFullScreen) {
                                //document.cancelFullScreen();  
                            } else if (document.mozCancelFullScreen) {
                                document.mozCancelFullScreen();
                            } else if (document.webkitCancelFullScreen) {
                                // document.webkitCancelFullScreen();  
                            }
                        }
                    }
                } 
                   else{
                   alert("Please accept the terms and condtion before proceeding.");
                   }
                });

           
// ===========================End Toggle Screen======================================================
            function result()
            {
                 var modal = document.getElementById('myModal');
                var btn = document.getElementsByClassName("last_que");

                var total = <?= $count; ?>;
                var radioValue;
                var radio_val = [];
                var dump = [];
                for (let i = 0; i < total; i++) {
                    radioValue = $("input[name='optionsRadios" + i + "']:checked").attr('id');
                    if (radioValue == undefined) {
                        radio_val[i] = '';
                    } else {
                        radio_val[i] = radioValue;
                    }
                }
                for (let i = 0; i < total; i++) {
                    dump[i] = radio_val[i + 1];

                }

                var data = <?php echo json_encode($data); ?>;
              var dump_val = [];
                for (let k = 0; k < total; k++) {
                    if (dump[k] == undefined) {
                        dump[k] = '';
                    }
                    dump_val[k] = {
                        "question_id": data[k],
                        "answer": dump[k]
                    };

                }
                //               var res_id= $("#show_res").attr('href');
                ////////////////
                //              var t = $("#time_val").val();
                ///////////////////


                var Url = "<?php echo site_url(); ?>";
                var test_segment_id = $('#test_id').attr('val1');
                var res_id = $("#show_res").attr('href');//result id for Result Url
                var t = $("#time_val").val();
                var time_spent=parseInt(t);
                $('#local_data').attr('time',parseInt(t));//set value to localstorage
                //for leader board
                var leader_url = $("#leader_board").attr('href');//result id for Result Url

                $.ajax({

                    data: {test_segment_id: test_segment_id,
                        time_spent: time_spent,
                        question_dump: dump_val},
                    type: 'post',
                    dataType: 'json',
                    url: Url + '/web/DailyDose/ajax_attempt_result',
                    success: function (data) {

                        $("#tr_id").html(data.result);
                        $("#show_res").attr('href', res_id + data.u_id);//setting controller url
                        $("#leader_board").attr('href', leader_url + data.u_id);//setting controller url

                    }, error: function (data) {
                        console.log("error" + data);
                    }
                });
                modal.style.display = "block";
            }
            $(".last_que").click(function () {
               result();
            });


           
        </script>

 

<!-- JS Scripts -->
<script src="<?php echo base_url(); ?>web_assets/js/jquery-3.2.1.js"></script>
<script src="/web_assets/js/jquery.appear.js"></script>
<script src="/web_assets/js/jquery.mousewheel.js"></script>
<script src="/web_assets/js/perfect-scrollbar.js"></script>
<script src="/web_assets/js/jquery.matchHeight.js"></script>
<script src="/web_assets/js/svgxuse.js"></script>
<script src="/web_assets/js/imagesloaded.pkgd.js"></script>
<script src="/web_assets/js/Headroom.js"></script>
<script src="/web_assets/js/velocity.js"></script>
<script src="/web_assets/js/ScrollMagic.js"></script>
<script src="/web_assets/js/jquery.waypoints.js"></script>
<script src="/web_assets/js/jquery.countTo.js"></script>
<script src="/web_assets/js/popper.min.js"></script>
<script src="/web_assets/js/material.min.js"></script>
<script src="/web_assets/js/bootstrap-select.js"></script>
<script src="/web_assets/js/smooth-scroll.js"></script>
<script src="/web_assets/js/selectize.js"></script>
<script src="/web_assets/js/swiper.jquery.js"></script>
<script src="/web_assets/js/moment.js"></script>
<script src="/web_assets/js/daterangepicker.js"></script>
<script src="/web_assets/js/simplecalendar.js"></script>
<script src="/web_assets/js/fullcalendar.js"></script>
<script src="/web_assets/js/isotope.pkgd.js"></script>
<script src="/web_assets/js/ajax-pagination.js"></script>
<script src="/web_assets/js/Chart.js"></script>
<script src="/web_assets/js/chartjs-plugin-deferred.js"></script>
<script src="/web_assets/js/circle-progress.js"></script>
<script src="/web_assets/js/loader.js"></script>
<script src="/web_assets/js/run-chart.js"></script>
<script src="/web_assets/js/jquery.magnific-popup.js"></script>
<script src="/web_assets/js/jquery.gifplayer.js"></script>
<script src="/web_assets/js/mediaelement-and-player.js"></script>
<script src="/web_assets/js/mediaelement-playlist-plugin.min.js"></script>
<script src="/web_assets/js/base-init.js"></script>
<script defer src="/web_assets/fonts/fontawesome-all.js"></script>
<script src="/web_assets/Bootstrap/dist/js/bootstrap.bundle.js"></script>
</body>
</html>
