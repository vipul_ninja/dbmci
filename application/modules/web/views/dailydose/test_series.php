<!DOCTYPE html>
<html lang="en">
<head>
<title>Test</title>

<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
<!-- Main Font -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/web_assets/js/webfontloader.min.js"></script>
<script src="/web_assets/js/webfontloader.min.js"></script>

            WebFont.load({
                google: {
                    families: ['Roboto:300,400,500,700:latin']
                }
            });
</script>

<script type="text/javascript">
        function color(name, num) {
            $('#' + name).css("background-color", "#32CD32");
            if (num == 4) {
                $.ajax({
                    url: '<?php echo site_url();?>result',
                    type: 'POST',
                    data: {},
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = jQuery.parseJSON(data);
                        $("#id").html('<td>Ajay Srivastava</td><td>' + obj.data.marks + '</td><td>' + obj.data.user_rank + '</td><td>' + obj.data.time_spent + '</td> <td>' + obj.data.result + '</td>');
                    },
                    error: function(err) {
                        console.log(err);

                    }
                });
                modal.style.display = "block";
            }

//            span.onclick = function() {
//
//                modal.style.display = "none";
//                window.close();
//
//            }

        }


    </script>

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">

<!-- Main Styles CSS -->
 <link rel="stylesheet" type="text/css" href="/web_assets/css/submit_popup.css">
<link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
<link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
<style>

.answerd {
    background-color: #38a9ff;
    padding: 10px;
    border-radius: 5px;
    color: #fff;
    display: block;
}
.statistics-point {
    display: block;}
    .for-submit-test {
    background: #fff;
    padding: 0 20px;
    width: 80%;}
</style>
<!--================================== Submit confirm Popup CSS =======================================-->

<style>

/* The Modal (background) */
.modal2 {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */

}

/* Modal Content */
.modal-content2 {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 40%; border-radius:10px;
}

/* The Close Button */
.close2 {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close2:hover,
.close2:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>
<!--==================================End Submit confirm Popup CSS =======================================-->

</head>
<body oncontextmenu="return false;">
   
<!-- Header-BP -->
<header class="header bg-blue" id="site-header">
  <div class="page-title test-title" style="padding: 12px 20px 26px 20px !important;">
   <img src="<?php echo base_url(); ?>web_assets/img/logo.png" >
  </div>
  <div class="header-content-wrapper"> </div>
</header>

<!-- ... end Header-BP --> 

<!-- Responsive Header-BP -->

<header class="header header-responsive bg-blue" id="site-header-responsive"> 
<a href="" class="logo">
				<div class="img-wrap">
					<img src="/web_assets/img/logo-colored-small.png" alt="Olympus">
				</div>
				<div class="title-block">
					<h6 class="logo-title">IBT</h6>
					<div class="sub-title">Online Exam</div>
				</div>
			</a>
  
</header>

<!-- ... end Responsive Header-BP -->
<div class="header-spacer"></div>




<!--======================================INSTRUCTION===================================================-->
        
        <div class="tab-content" id="instruct">
            <div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">

                <div class="question-box content" id="">

                        <table class="table table-striped" border="1">
                            <tr class="">
                                <th scope="col">Test Name</th>
                                <th scope="col">Total Questions</th>
                                 <th scope="col">Total Time</th>
                                  <th scope="col">Maximum Marks</th>
                            </tr>
                            <tr>
                                <td><?php echo $detail['basic_info']['test_series_name'] ?></td>
                                <td><?php echo $detail['basic_info']['total_questions'];?></td>
                                <td><?php echo $detail['basic_info']['time_in_mins']." Mins";?></td>
                                <td><?php echo $detail['basic_info']['total_marks'];?></td>
                            </tr>
                        </table>
                     <label>
                        <h6><b>General Instructions</b></h6>
                    </label>
                    <br>
                    <label><b>Please read the following instructions very carefully:</b></label>
                    <br>
                    <label><strong>1.</strong> You must only use paper handed out at the examination venue.</label>
                    <br>
                    <label><strong>2.</strong> Your paper must be written with a black or blue ballpoint pen.</label>
                    <br>
                    <label><strong>3.</strong> At digital examinations, you write your answer directly in the program.</label>
                    <br>
                    <label><strong>4.</strong> You may work on your exam paper until the time allotted for the examination expires.</label>
                    <br>
                    <label><strong>5.</strong> You are not allowed to use any calculator and any other computing machine.</label>
                    <br>
                    <label><strong>6.</strong> Click on the question number in the Question Palette to go to that question directly.</label>
                    <br>
                    <br>

                    <div class="checkbox" style="background-color: #e5e5e5;
                                                    
                                                border-radius: 2px;">
                        <label style="color: red;margin-top: 2px;
                                                margin-left: 8px;
                                                ">
                            <input name="optionsCheckboxes" type="checkbox" id="term_chkbox" ><span class="checkbox-material" ><span ></span></span style="color: red;">
                           I have read and understood the instructions. I agree that in case of not adhering to the exam instructions, I will be disqualified 
                        </label>
                    </div>
                    <br>
                    <button type="button" class="btn btn-md-2 bg-blue" id="start" onclick="startTest_Timer();">Start Test</button>
                </div>
            </div>
        </div>
        <!--===================================END INSTRUCTION===================================================-->
<!-- ... start test tabs -->
<form style="display: none;" id="form">
<div class="container-fluid" >
  <div class="row">
    <div class="col col-xl-10 col-lg-8 col-md-12 col-sm-12 col-12">
      <div class="ui-block-title">
        <div class="h6 title text-center"><?php echo $detail['basic_info']['test_series_name'] ?>
        </div>
 </div>
      <div class="ui-block">
        <!-- News Feed Form  -->
          <div class="news-feed-form">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"> <a class="nav-link active inline-items subs" data-toggle="tab" main="home-1" href="#home-1" role="tab" aria-expanded="true"><span>Physics</span> </a> </li>
            <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="che" href="#che" role="tab" aria-expanded="false"><span>Chemistary</span> </a> </li>
            <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="bio" href="#bio" role="tab" aria-expanded="false"> <span>Bioloagy</span> </a> </li>
            <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="math" href="#math" role="tab" aria-expanded="false"> <span>Mathematic</span> </a> </li>
             <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="hin" href="#hin" role="tab" aria-expanded="false"> <span>Hindi</span> </a> </li>
              <li class="nav-item"> <a class="nav-link inline-items subs" data-toggle="tab" main="eng" href="#eng" role="tab" aria-expanded="false"> <span>English</span> </a> </li>
          </ul>
          <!-- Tab panes -->
<!--          <span id="total" totalq="<?php //echo $total = $test_data['basic_info']['total_questions']; ?>"></span>-->
          <div class="tab-content" id="tabData">
          <div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">
            <?php
              $detail = $detail['question_bank'];
              $count = count($detail);
                $que_id = array();
                $data = array();
                for ($i = 0; $i < $count; $i++) {
                  static $num = 0;
                    if ($i == 0) {
                        $style = 'block';
                    } else {
                        $style = 'none';
                    }
            ?>                         
            
            <div class="content" id="content-<?php echo $i + 1; ?>" style="display: <?php echo $style; ?>;">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <div class="question-language">
                      <span>Question <?php echo $i + 1; ?>.</span>
                      <span class="choose-language">
                        <div class="w-select">
                          <div class="title">Language</div>
                          <fieldset class="form-group">
                            <select class="selectpicker form-control">
                              <option value="DA">English</option>
                              <option value="NU">Hindi</option>
                            </select>
                          </fieldset>
                        </div>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <?php $len=strlen($detail[$i]['question']);
                  if($len>500){
                  ?>
                  <div class="col-md-6">
                    <p> <?php echo $detail[$i]['question']; ?></p>
                  </div>
                  <div class="col-md-6">
                    <div class="radio" >
                                            <label>
                                                <input type="radio" name="optionsRadios<?php echo $i + 1; ?>" value="1" onclick="color(this.name,<?php echo $i?>)">
                                                A.
                                                <?php echo $detail[$i]['option_1']; ?> </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios<?php echo $i + 1; ?>" value="2" onclick="color(this.name,<?php echo $i ?>)">
                                                B.
                                                <?php echo $detail[$i]['option_2']; ?> </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios<?php echo $i + 1; ?>" value="3" onclick="color(this.name,<?php echo $i ?>)">
                                                C.
                                                <?php echo $detail[$i]['option_3']; ?> </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios<?php echo $i + 1; ?>" value="4" onclick="color(this.name,<?php echo $i ?>)">
                                                D.
                                                <?php echo $detail[$i]['option_4']; ?> </label>
                                        </div>
                  </div>
                <?php }else{ ?>
                  <div class="col-md-12">
                        <p><?php echo $detail[$i]['question']; ?></p>
              
                  <div class="radio" >
                                            <label>
                                                <input type="radio" name="optionsRadios<?php echo $i + 1; ?>" value="1" onclick="color(this.name,<?php echo $i?>)">
                                                A.
                                                <?php echo $detail[$i]['option_1']; ?> </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios<?php echo $i + 1; ?>" value="2" onclick="color(this.name,<?php echo $i ?>)">
                                                B.
                                                <?php echo $detail[$i]['option_2']; ?> </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios<?php echo $i + 1; ?>" value="3" onclick="color(this.name,<?php echo $i ?>)">
                                                C.
                                                <?php echo $detail[$i]['option_3']; ?> </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios<?php echo $i + 1; ?>" value="4" onclick="color(this.name,<?php echo $i ?>)">
                                                D.
                                                <?php echo $detail[$i]['option_4']; ?> </label>
                                        </div>
                  </div>
                    <?php } ?>
                </div>
              </div>
            </div>
            <?php 
        $num++;
 $que_id[$i]=$detail[$i]['id'];
$data[]=$que_id[$i]; 
}
 ?>
          </div>
          
          </div>
        </div>
        <!-- ... end News Feed Form  -->
      </div>
    </div>
    <aside class="col col-xl-2 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
      <div class="ui-block"> 
        
        <!-- W-Activity-Feed -->
        
        <ul class="widget w-activity-feed notification-list">
          <li>
            <div class="author-thumb"> 
            <?php if(remote_file_exists($this->session->pro_img, 1)) { ?>
            <img src="<?= $this->session->pro_img ?>" alt="author">
            <?php } else { ?>
            <i class="fa fa-user-circle" alt="author" style="font-size:37px"></i>
            <?php } ?>
              </div>
            
            <div class="notification-event">
            <span class="h6 notification-friend">
            <?php  if($this->session->userdata){ echo $this->session->userdata('name');}else{ echo "Ajay Singh"; }?></span>
							
							<span class="notification-date"><time class="entry-date updated bold" datetime="2004-07-24T18:18">Time Left: <span id="time">10:00</span> </time></span>
               <input type="hidden" id="time_val"/><br>

                                   

						</div>

          </li>
        </ul>
        
        <!-- .. end W-Activity-Feed --> 
      </div>
      <div class="ui-block">
        <div class="ui-block-title">
          <h6 class="title">You are viewing Physics section Question Palette</h6>
        </div>
        
        <!-- W-Activity-Feed -->
        <div class="question-status">
                                
          <ul>
              <?php $count = count($detail);
                for ($i = 0; $i < $count; $i++) { ?>
                   <li style="">
                                    <a href="#q<?php echo $i+1; ?>" id='optionsRadios<?php echo $i+1; ?>' class="common attempt" val1="<?php echo $i+1;?>" onclick="click_count(<?php echo $i+1; ?>)">Q
                                        <?php echo $i+1; ?>
                                    </a>
                                </li>
              <?php } ?>
          </ul>
      </div>
        
         <div class="gides">
              <h6>Legends:</h6>
              <ul class="list-inline">
            <li><span> <span class="statistics-point answerd" style="background-color: rgb(72, 61, 139);"></span>Review </span></li>
            <li><span> <span class="statistics-point not-visited"></span> Answerd  </span></li>
            <li><span> <span class="statistics-point marked"></span>Not Answerd</span></li>
            <li><span> <span class="statistics-point not-answerd"></span>Not Visited </span></li>
          </ul>
        </div>
        <div class="action">
         <!--  
            <button class="btn btn-border-think c-grey btn-transparent custom-color">View QP</button>
            <button class="btn btn-border-think c-grey btn-transparent custom-color" style="float: right;">Instructions</button>
            <br>-->
            <button type="button" onclick="submit_test()" class="btn btn-blue full-width" id="myBtn2" href="javascript:void(0);">Submit</button>
          
        </div> 
        
        <!-- .. end W-Activity-Feed --> 
      </div>
    </aside>
    
     <div class="fixed-bottom for-submit-test">
        <div class="post-additional-info inline-items">
                 <a href="#" class="btn btn-smoke btn-sm btn-light-bg review1 counting" clear2="1" current="<?php echo count($detail); ?>" current_val="1">Mark For Review</a>
<!--                <a href="#" class="btn btn-smoke btn-sm btn-light-bg">Skip</a>-->
                    <a href="#" class="btn btn-smoke btn-sm btn-light-bg clear " clear1="1" style="cursor:pointer" onclick="erase_radio()" current_val="1">Clear</a>

               
                 <a href="javascript:void(0);" class="btn btn-blue btn-md-2 save counting" name="save" total="<?php echo count($detail); ?>" current_val="1">Save &amp; Next</a>
                <!-- <div class="comments-shared counting"></div> -->
        </div>
    
    </div> 
  </div>
</div>
</form>
<!--================================== Submit confirm Popup Modal =======================================-->
<!-- The Modal -->
<div id="myModal2" class="modal2">
<!-- Modal content -->
  <div class="modal-content2">
   <div> <span class="close2">&times;</span>
   </div>
   <p>select the appropriate Option:</p>
   <div class="radio" >
      <label>
          <input type="radio" name="rdio" value="submit" >I want to submit the test.
         
      </label>
  </div>
  <div class="radio" >
      <label>
        
          <input type="radio" name="rdio" value="continue" >I want to continue taking the test.
      </label>
  </div>
    <!-- <p style="float:left;display: inline-block;"><input type="radio" name="rdio" value="submit">I want to submit the test.</p>
    <p><input type="radio"name="rdio" value="continue">I want to continue taking the test.</p> -->
 <p>    <button type="button" onclick="pop_click()" class="btn btn-md-2 bg-blue"  href="javascript:void(0);">Submit</button></p>
  </div>
</div>
<script>
// Get the modal
var modal2 = document.getElementById('myModal2');
// Get the button that opens the modal
var btn2 = document.getElementById("myBtn2");
// Get the <span> element that closes the modal
var span2 = document.getElementsByClassName("close2")[0];
// When the user clicks the button, open the modal 
btn2.onclick = function() {
    modal2.style.display = "block";
}
// When the user clicks on <span> (x), close the modal
span2.onclick = function() {
    modal2.style.display = "none";
}
function pop_click(){
  var value=$("input[name='rdio']:checked").val();
  if(value=='submit'){
    $('input[name="rdio"]').prop('checked', false);
    submit_test()
  }else{
   $('input[name="rdio"]').prop('checked', false);
    modal2.style.display = "none";
    }
}
</script>
<!--================================== End Submit confirm  Popup Modal ======================================-->
<!--================================== Result Popup Modal =======================================-->

            <div id="myModal" class="modal" style="z-index:99;">

                <!-- Modal content -->
                <div class="modal-content">
        <!--  <span class="close">&times;</span>-->

                    <div style="text-align: center;"><h3>Test Summary</h3></div>
                    <table border="1" style="text-align:center;cellpadding:10px;padding:10px;font-size:20px;width:100%;">
                        <tr style="background:grey;color:white;">
                        <b>
                            <th>Title</th>
                            <th>Correct Answer</th>
                            <th>Marks</th>
                            <th>Time Spent</th>
                            <th>User Rank</th>
                        </b>
                        </tr>
                <tr id="id"></tr>
    </table>
                    <div style="text-align: center; margin-top:10px; "><h5><a id="show_res"  href="<?php echo site_url(); ?>/web/My_test/result_question/53/">View Result</a></h5></div>
                    <div style="text-align: center; margin-top:10px; ">
                    <h5>
                        <a id="leader_board"   href="<?php echo site_url(); ?>/web/DailyDose/basic_rank_result/">LeaderBoard</a>
                    </h5>
                    </div>
                </div>

            </div>
            <!--==================================End Result Popup Modal =======================================-->

<a class="back-to-top" href="#"> <img src="/web_assets/svg-icons/back-to-top.svg" alt="arrow" class="back-icon"> </a>

<!-- ... end start test tabs --> 
  <script>
        
        
 
   $(document).on("keydown",function(ev){
	
	if(ev.keyCode===116||ev.keyCode===27)
        return false;
})

            //#############################for escape key######################
        
        $(document).keydown(function(e) { 
            if (e.keyCode === 27) 
                {
                 e.preventDefault();            
                    return false;           
                }
        });        
//        $( ".selector" ).dialog({ closeOnEscape: false });
//        window.onkeydown = function(e){
//    if(e.keyCode === 27){ // Key code for ESC key
//        e.preventDefault();
//    }
//};
//        $("body").on("keyup", function(e){
//    if (e.which === 27){
//        return false;
//    } 
//});
//        $('body').keyup(function(e) {
//    if(e.which === 27){ 
//        alert();
//        $(modal).trigger('reveal:close'); 
//    } 
//});
//--========================End disable key=========================================================-->       
        
        
        
        
// ======================click on question number======================================================
        function click_count(id) {
            $('.content').hide();
            document.getElementById('content-' + id).style.display = "block";

        }
// ======================End click on question number==================================================
        
        
        
        
// =============================Toggle Screen==============================================================
        $(document).ready(function() {
            $('#start').click(function(e) {
 var ckb_status = $("#term_chkbox").prop('checked');
               if(ckb_status==1){
                $('#form').css({
                    "display": "block"
                });
                $('#instruct').css({
                    "display": "none"
                });

                if ((document.fullScreenElement && document.fullScreenElement !== null) ||
                    (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                    if (document.documentElement.requestFullScreen) {
                        document.documentElement.requestFullScreen();
                    } else if (document.documentElement.mozRequestFullScreen) {
                        document.documentElement.mozRequestFullScreen();
                    } else if (document.documentElement.webkitRequestFullScreen) {
                        document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                    }
                    if (e.requestFullscreen) {
                        e.requestFullscreen();
                    } else if (e.mozRequestFullScreen) {
                        e.mozRequestFullScreen();
                    } else if (e.webkitRequestFullscreen) {
                        e.webkitRequestFullscreen();
                    } else if (e.msRequestFullscreen) {
                        e.msRequestFullscreen();
                    } else {
                        if (document.cancelFullScreen) {
                            //document.cancelFullScreen();  
                        } else if (document.mozCancelFullScreen) {
                            document.mozCancelFullScreen();
                        } else if (document.webkitCancelFullScreen) {
                            // document.webkitCancelFullScreen();  
                        }
                    }
                }
                    } 
                   else{
                   alert("Please accept the terms and condtion before proceeding.");
                   }
            });
        });
// ===========================End Toggle Screen==============================================================        
        
        
        
//===================================submit test========================================================
        // $('#submit2').click(function() {
          function submit_test(){
            var modal = document.getElementById('myModal');
            var btn = document.getElementById("submit2");
            //  var btn1 = document.getElementsByClassName("sbt4");
//            var span = document.getElementsByClassName("close")[0];

            var total = <?=$count;?>;
            var radioValue;
            var radio_val = [];
            var dump = [];
            for (let i = 0; i < total; i++) {
                radioValue = $("input[name='optionsRadios" + i + "']:checked").val();
                if (radioValue == undefined) {
                    radio_val[i] = '';
                } else {
                    radio_val[i] = radioValue;
                }
            }
            for (let i = 0; i < total; i++) {
                dump[i] = radio_val[i + 1];

            }

            var data = <?php echo json_encode($data); ?>;

            var dump_val = [];
            for (let k  = 0; k < total; k++) {
                if (dump[k] == undefined) {
                    dump[k] = '';
                }
                dump_val[k] = {
                    "question_id": data[k],
                    "answer": dump[k]
                };

            }
               var res_id= $("#show_res").attr('href');
               var test_segment_url=$("#leader_board").attr("href");
               ////////////////
              var t = $("#time_val").val();
                ///////////////////
        
            $.ajax({
                url: '<?php echo site_url();?>/web/My_test/on_test_submit',
                type: 'POST',
                dataType: "json",
                data: {
                    user_id: 53,
               
                    test_series_id: 11,
                    time_spent: parseInt(t),//spent
                    question_dump: dump_val
                },
                success: function(data) {
                   
//                    $("#id").html(data);
                    console.log(data);
                     $("#id").html(data.result);
                    $("#show_res").attr('href',res_id + data.u_id);
                    $("#leader_board").attr('href',test_segment_url+ data.u_id)
                },
                error: function(err) {
                    console.log(err);
                    // alert("error"+err);
                }

            });
            modal.style.display = "block";
        }
/////===================================submit test===================================================
    </script>

    <!-- JS Scripts -->
    <script src="/web_assets/js/jquery-3.2.1.js"></script>
    <script src="/web_assets/js/jquery.appear.js"></script>
    <script src="/web_assets/js/jquery.mousewheel.js"></script>
    <script src="/web_assets/js/perfect-scrollbar.js"></script>
    <script src="/web_assets/js/jquery.matchHeight.js"></script>
    <script src="/web_assets/js/svgxuse.js"></script>
    <script src="/web_assets/js/imagesloaded.pkgd.js"></script>
    <script src="/web_assets/js/Headroom.js"></script>
    <script src="/web_assets/js/velocity.js"></script>
    <script src="/web_assets/js/ScrollMagic.js"></script>
    <script src="/web_assets/js/jquery.waypoints.js"></script>
    <script src="/web_assets/js/jquery.countTo.js"></script>
    <script src="/web_assets/js/popper.min.js"></script>
    <script src="/web_assets/js/material.min.js"></script>
    <script src="/web_assets/js/bootstrap-select.js"></script>
    <script src="/web_assets/js/smooth-scroll.js"></script>
    <script src="/web_assets/js/selectize.js"></script>
    <script src="/web_assets/js/swiper.jquery.js"></script>
    <script src="/web_assets/js/moment.js"></script>
    <script src="/web_assets/js/daterangepicker.js"></script>
    <script src="/web_assets/js/simplecalendar.js"></script>
    <script src="/web_assets/js/fullcalendar.js"></script>
    <script src="/web_assets/js/isotope.pkgd.js"></script>
    <script src="/web_assets/js/ajax-pagination.js"></script>
    <script src="/web_assets/js/Chart.js"></script>
    <script src="/web_assets/js/chartjs-plugin-deferred.js"></script>
    <script src="/web_assets/js/circle-progress.js"></script>
    <script src="/web_assets/js/loader.js"></script>/
    <script src="/web_assets/js/run-chart.js"></script>
    <script src="/web_assets/js/jquery.magnific-popup.js"></script>
    <script src="/web_assets/js/jquery.gifplayer.js"></script>
    <script src="/web_assets/js/mediaelement-and-player.js"></script>
    <script src="/web_assets/js/mediaelement-playlist-plugin.min.js"></script>
    <script src="/web_assets/js/base-init.js"></script>
    <script defer src="/web_assets/fonts/fontawesome-all.js"/></script>
    <script src="/web_assets/js/test1.js"></script>
    <script src="/web_assets/Bootstrap/dist/js/bootstrap.bundle.js"></script>
<!--    //=========================================Timer=======================================================-->
        <script>
            function startTest_Timer() {
            var duration = (60*10)-1,
                display = document.querySelector('#time');
            var timer = duration,
                minutes, seconds;
                var s = 0;
          var Countdown=  setInterval(function() {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.textContent = minutes + ":" + seconds;
                s++;
                $("#time_val").val(s);
                  if (--timer < 0) {
                  
                    clearInterval(Countdown);
                   submit_test();
                }
            }, 1000);
        }
       
        </script>
<!-- //=========================================Timer=======================================================-->
</body>

</html>

<!--================================= function for check remote url=============================== -->
<?php
function remote_file_exists($source, $extra_mile = 0) {

  if ($extra_mile === 1) {
      $img = @getimagesize($source);

      if (!$img)
          return 0;
      else {
          switch ($img[2]) {
              case 0:
                  return 0;
                  break;
              case 1:
                  return $source;
                  break;
              case 2:
                  return $source;
                  break;
              case 3:
                  return $source;
                  break;
              case 6:
                  return $source;
                  break;
              default:
                  return 0;
                  break;
          }
      }
  } else {
      if (@FClose(@FOpen($source, 'r')))
          return 1;
      else
          return 0;
  }
}
?>
<!--==============================End function for check remote url=============================== -->