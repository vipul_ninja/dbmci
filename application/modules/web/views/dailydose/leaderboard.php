<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Articles</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
    body {  background-color: #b5d1ff; } 
  </style>
</head>
<body>
<h2 align="center"> <b>Leader Board</b></h2>

<?php if(!empty($d1)){ ?>

    <h2 align="center">Your Rank:- <b style="color: green;"><?php echo $d1['user_rank']; ?></b></h2>

    <h4 align="center">Total User Attempt:- <b style="color: orange;"><?php echo $d1['total_user_attempt']; ?></b></h4>

 

<div class="list-group">
<h4 align="center" class="list-group-item list-group-item-action active">Top Ten List</h4>
</br>
<?php foreach($d1['top_ten_list'] as $d2){ ?>
    <div class="list-group-item list-group-item-action" style="padding: 0 0;">
      <img  style="height: 50px;width: 50px;" src="<?php echo $d2['profile_picture']; ?>">
      <b style="margin-left: 20px;"><?php echo $d2['name']; ?></b>
    </div>
   </br>
<?php } ?>
 
</div>
<?php } ?>
</body>
</html>