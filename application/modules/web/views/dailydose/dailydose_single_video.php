<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Dailydose video</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
  <link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16"> 
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
  <style type="text/css">
  .subject_video_box{background-color: #fff;
    display: block;
    position: relative;
    margin-bottom: 30px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: 0px 0px 30px 0px rgba(0,0,0,0.1);
    -moz-box-shadow: 0px 0px 30px 0px rgba(0,0,0,0.1);
    box-shadow: 0px 0px 30px 0px rgba(0,0,0,0.1);}
    .olymp-play-icon{margin-top:16px;}
    .video-heading{font-size: 18px; color: black;}
    .video-date{font-size: 14px; color: darkgray}
    .video-dis{font-size: 15px; color: black}
    .suggested h1{font-weight: 500 }
    .video-content .h4{font-weight: 500 !important; font-size: 18px}
    .post-video .video-content p {
    margin: 10px 0;
    font-size: 13px;color: black}
</style> 
</head>
<body>
  <?php $this->load->view('../include/header.php');?>
  <div class="header-spacer"></div>
<?php
        function videoType($url) {
          if (strpos($url, 'youtube') > 0) {
              return 'youtube';
          } elseif (strpos($url, 'vimeo') > 0) {
              return 'vimeo';
          } else {
              return 'unknown';
          }
        }
      ?>
   <?php if(!empty($d1['data'])){ ?>
    <div class="container">
    <div align="center">
       
         <?php    if(videoType($d1['data']['URL']) == 'youtube'){ 
            $link =  $d1['data']['URL'];
            $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
            if (empty($video_id[1]))
            $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..
            $video_id = explode("&", $video_id[1]); // Deleting any other params
            $video_id = $video_id[0];
           ?>
   
            <iframe width="100%" height="90%" src="https://www.youtube.com/embed/<?php echo $video_id ; ?>" frameborder="0" allow="encrypted-media" allowfullscreen ></iframe>

     <?php  } else if(videoType($d1['data']['URL']) == 'vimeo'){ ?>
            
              <iframe width="100%" height="90%" 
              src="<?php  echo $d1['data']['URL']; ?>" 
              frameborder="0"  allow="encrypted-media" allowfullscreen preload="none">
              </iframe>

      <?php }else{ ?>

          <?php echo "no video found";?>

      <?php  } ?>
    </div>
    </div>
  </br>
  <div class="container" style="padding-left: 30px;">
    <div class="row">
    <span class="video-heading">
      <b><?php  echo $d1['data']['video_title'];?></b>
      <br>
      <span class="video-date"> <?php  echo date('d-M h:i A',$d1['data']['creation_time']/1000);?></span>
    </span>
    <br>
    <br>
    <br>  
    <div class="video-dis"> <?php  echo $d1['data']['video_desc'];?></div>
 </div>
</div>
<hr>
<div class="container suggested">
      <h1 align="center"> Suggested Video</h1>
  <div class="row">
      <?php if(!empty($d3))
 
 {  ?>
<?php foreach ($d3['data']['suggested_videos'] as $data) { 
  if($d1['data']['id'] != $data['id'] ){
  ?>
  <div class="col-md-4 col-sm-12 col-xs-12">
      <div class="subject_video_box">
        <div class="post-video">
              <div class="video-thumb">
                <img src="<?php echo $data['thumbnail_url'] ?>" alt="photo" style="width:337px;height: 200px;">
                <a href="<?php echo site_url('/web/DailyDose/single_video/'.$data['id']); ?>" class="play-video">
                  <svg class="olymp-play-icon"><use xlink:href="/web_assets/svg-icons/sprites/icons.svg#olymp-play-icon"></use></svg>
                </a>
              </div>
          
              <div class="video-content">
                <a href="#" class="h4 title"><?php echo ucfirst($data['video_title']); ?></a>
                <div class="quiz-box-date"><?php  echo date('d-M h:i A',$data['creation_time']/1000);?></div>
                <p><?php $descripcount = strlen(strip_tags($data['video_desc']));
            if($descripcount <=150){
              echo $str = $data['video_desc'];
            }else{$dots = "";
              $str = strip_tags($data['video_desc']);
              echo substr($str,0,400)."...";
            } 
              ?>
                </p>
                
              </div>
            </div> 
      </div> 
    </div>
        <?php } } }  ?>
</div>
</div>
<div class="footer-space"></div>
<?php }else{ ?>
<h3 align="center">No Data Found</h3>
<?php } ?> 
</body>
<?php $this->load->view('../include/footer');?>
</html>