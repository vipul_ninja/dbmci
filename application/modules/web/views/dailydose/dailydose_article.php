<?php  //echo "<pre>"; print_r($d1); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Articles</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
  <!-- Ibt Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

      <style type="text/css">
.vl {
    border-left: 2px solid white;
    height: 10px;
}
    </style>
</head>
<body>
  <div class="main-box">
  <?php $this->load->view('../include/header.php');?>
  <div class="header-spacer"></div>
  <div class="container">
  <div class="row">
<?php if(!empty($d1)){ ?>
    <div class="col-md-8 offset-md-2">
      <h3 class="text-center revi-of-dy">Articles</h3>
        <div class="page-flow">
          <ul class="breadcrumbs">
              <li class="breadcrumbs-item">
                <a href="<?php echo site_url(); ?>/web/Feeds">Home</a>
                <span class="fc-icon fc-icon-right-single-arrow"></span>
              </li>
              <li class="breadcrumbs-item">
                <a href="<?php echo site_url(); ?>/web/DailyDose">Daily Dose</a>
                <span class="fc-icon fc-icon-right-single-arrow"></span>
              </li>
              <li class="breadcrumbs-item active">
                <span>Articles</span>
              </li>
          </ul>
        </div>
      <div  id="post_list">
      <?php $i = 0; $second_date = '';
        foreach($d1 as $d2) { 
          $first_date = date("F d", $d2['creation_time']/1000);
          ?>
          <?php if($first_date == $second_date){ 
            $second_date = $first_date;?>
          <?php }else{ 
            $second_date = $first_date;?>
           <h2 class="date"><?php
            if($this->CI->check_time_ago_in_php($d2['creation_time'])){
             echo date("F d", $d2['creation_time']/1000);
           
           }else{
           echo $this->CI->time_ago_in_php($d2['creation_time']);
            } 
            ?></h2>
          <?php }?>
             <div class="quiz_box">
        <div class="row no-gutters">
        <div class="col-md-2 col-xl-2 col-lg-2 col-sm-3 col-4">
          <div class="quiz_box_image">
            <img src="<?php echo base_url('web_assets/img/makemyexam.png'); ?>" alt="author">
          </div>
        </div>
        <div class="col-md-8 col-xl-8 col-lg-8 col-sm-6 col-8">
          <div class="quiz_box_details">
            <h2><?php echo $d2['post_headline']; ?></h2>
            <div class="quiz-box-date"><?php 
              if($this->CI->check_time_ago_in_php($d2['creation_time'])){
            
           echo date("F d, Y", $d2['creation_time']/1000); 
           }else{
           echo $this->CI->time_ago_in_php($d2['creation_time']);
            }  
             ?></div>
          </div>
          <div class="quiz_box_bottom">
            <ul class="list-inline">
              <li class="list-inline-item"> <span> <?php echo $d2['likes']; ?> Likes</span></li>
               <li class="list-inline-item"> <span> <?php echo $d2['comments']; ?> Comment </span></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2 col-xl-2 col-lg-2 col-sm-3">
          <div class="quiz_box_bookmar">
            <ul class="list-inline">
              <li class="list-inline-item"><a href="" class="bookmark" ><img src="/web_assets/img/bookmark.png" alt=""></a></li>
              <li class="list-inline-item"><a href="" class="bookmark"><img src="/web_assets/img/blueshare_xxhdpi.png" alt=""></a></li>
            </ul>
            <a  href="<?php echo site_url('/web/DailyDose/read_more/'.$d2['id'].'/2'); ?>"class="btn btn-sm bg-blue" style="margin-top: 37px;">Read More</a>
          </div>
        </div>
      </div>

      </div>
    <?php } ?>

      <?php  $pid = $d2['id'];  ?>
            <a id="search<?= $pid ?>" main="all" onclick="search_filter(<?=$pid;?>)"  class="btn btn-control btn-more">
                <svg class="olymp-three-dots-icon">
                <use xlink:href="<?php echo base_url(); ?>web_assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                </svg>
            </a>
        <?php }else{ ?>
               <center><h4>No feeds found!!</h4></center>
          <?php  } ?>
    </div>
    </div>
  
      </div>
    </div>
     <?php $this->load->view('../include/footer'); ?>
     </div> 
  <?php


function remote_file_exists( $source, $extra_mile = 0 ) { 

# EXTRA MILE = 0 
////////////////////////////////// Does it exist? 
# EXTRA MILE = 1 
////////////////////////////////// Is it an image? 

if ( $extra_mile === 1 ) { 
    $img = @getimagesize($source); 
     
    if ( !$img ) return 0; 
    else 
    { 
            switch ($img[2]) 
            { 
                    case 0: 
                            return 0;     
                            break; 
                    case 1: 
                            return $source; 
                            break; 
                    case 2: 
                            return $source; 
                            break; 
                    case 3: 
                            return $source; 
                            break; 
                    case 6: 
                            return $source; 
                            break; 
                    default: 
                            return 0;         
                            break; 
            } 
    } 
} 
else 
{ 
if (@FClose(@FOpen($source, 'r')))  
            return 1; 
 else 
            return 0; 
} 
}  

?>         
  <script type="text/javascript">
    

    ///==============================================PAGINATION===============================================
   function search_filter(page_num) { 
                page_num = page_num ? page_num : 0;
                //alert(page_num);
                var type = $('#search'+page_num).attr('main');
               
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('web/DailyDose/ajax_post'); ?>',
                    data: {last_post_id:page_num,type:"2"},

                    beforeSend: function () {
                        $('.loader').show();
                    },
                    success: function (html) {

                        $('#post_list').append(html);
                        $('#search'+page_num).css({"display":"none"});
                        $('.loader').fadeOut("slow");
                    }
                });
            }
//======================================END PAGINATION===============================================
  </script>

<script src="/web_assets/Bootstrap/dist/js/bootstrap.bundle.js"></script>
</body>
</html>