<?php 
if(!empty($videos)){
	if($videoValue == 1){
		$class = 'show active';
		$id = 'latest';
	}else if($videoValue == 2){
		$class = 'show active';
		$id = 'trending';
	}else if($videoValue == 3){
		$class = 'show active';
		$id = 'favourite';
	}
?>


<div class="tab-pane fade <?=$class;?>" id="<?=$id;?>" role="tabpanel" aria-labelledby="<?=$id;?>-tab">
    <div class="row">
    	<?php foreach($videos['data'] as $video){  
	    		$videoType = videoType($video['URL']);
		          if($videoType == 'youtube'){
		           $arrayurl = explode("watch?v=", $video['URL']);
		            $vid = explode("&", $arrayurl[1]);
		            $url = $arrayurl[0]."embed/".$vid[0];
		          }else{
		            $url =  $video['URL'];
		          }
      	?>
  
	       	
	       	<div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 ">
	      		<div class="ui-block video-item">
      			 	
			        	<div class="video-player">
			          		<img src="<?=$video['thumbnail_url'];?>" alt="photo">
			          		<a href="javascript:void(0);"class="video-btn play-video" data-toggle="modal" data-id="<?=$video['id'];?>" data-main="<?=$videoType;?>" data-src="<?=$url;?>" data-target="#myModal<?=$video['id']?>" style="text-decoration: none;">
			          			<svg class="olymp-play-icon"><use xlink:href="/web_assets/svg-icons/sprites/icons.svg#olymp-play-icon"></use></svg>
			          		</a>
			          		<div class="overlay overlay-dark"></div>
			        	</div>
		   
	      			<div class="ui-block-content video-content">
			          	<p class="h6 title">
		          			<?php 	$titlecount = strlen($video['video_title']);
						            if($titlecount >20){
						              $dot= "...";
						            }else{$dot = "";}
             						echo ucfirst(substr($video['video_title'],0,20)).$dot;
         					?>
 						</p>
			          	<p class="published">
				          	<?php $descripcount = strlen(strip_tags($video['video_desc']));
					            if($descripcount <=50){
					              echo $str = $video['video_desc'];
					            }else{$dots = "";
					              $str = strip_tags($video['video_desc']);
					              echo substr($str,0,20)."...";
					            }
	              			?>
          				</p>
			        </div>
		      	</div>
	   		</div>
   	  <!-- model -->
    <div class="modal fade" id="myModal<?=$video['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      
      <div class="modal-body">

       <button type="button" class="close" data-main="<?=$videoType?>" data-id="<?=$video['id'];?>" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>        
        <!-- 16:9 aspect ratio -->
<div class="embed-responsive embed-responsive-16by9">
  <?php
         if($videoType == 'mp4'){ ?>
          <video controls autoplay>
          <source  id="video<?=$videoType.$video['id'];?>"  src="" type="video/mp4">
          <source  id="videoogg<?=$videoType.$video['id'];?>" src="" type="video/ogg">
    </video>
       <?php }else{ ?>
         <iframe class="embed-responsive-item" src="" id="videos<?=$videoType.$video['id'];?>"  allowscriptaccess="always"></iframe>
     <?php  } ?>
 
  
</div>
        
        
      </div>

    </div>
  </div>
</div> 
   	<?php } ?>
    </div>
</div>
<?php } ?>
 
<?php 
function videoType($url) {
    if (strpos($url, 'youtube') > 0) {
        return 'youtube';
    } elseif (strpos($url, 'vimeo') > 0) {
        return 'vimeo';
    } elseif(strpos($url, 'mp4') > 0) {
        return 'mp4';
    }else{
      return 'unknown';
    }
}
?>


<script type="text/javascript">
$(document).ready(function() {
  $('.video-btn').click(function() {
       var videoSrc = $(this).data( "src" );
       var videoType = $(this).data( "main" );
       var videoId = $(this).data( "id" );
      if(videoType == 'mp4'){
          $("#video"+videoType+videoId).attr('src',videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" );
          $("#videoogg"+videoType+videoId).attr('src',videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1");
     }else{
          $("#videos"+videoType+videoId).attr('src',videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" );
     }
  });

  $('.close').click(function() {
       var videoType = $(this).data( "main" );
       var videoId = $(this).data( "id" );
      if(videoType == 'mp4'){
          $("#video"+videoType+videoId).attr('src', '');
          $("#videoogg"+videoType+videoId).attr('src','');
     }else{
          $("#videos"+videoType+videoId).attr('src','');
     }
  });
});
</script>