<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dailydose video</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
	<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16"> 
	<link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
	<style>
		.modal1 {
		  max-width: 1100px;
		  margin: 30px auto;
		}
		.modal2 {
		  position:relative;
		  padding:0px;
		}
		.carousel-inner img {
		  width: 100%;
		  height: 100%;
		}
		.close {
		  position:absolute;
		  right:-30px;
		  top:0;
		  z-index:999;
		  font-size:2rem;
		  font-weight: normal;
		  color:#fff;
		  opacity:1;
		}
		.video-item .video-player {
		    margin: 0;
		    border-radius: 3px 3px 0 0;
		    max-height: 150px;
		}
		.ui-block-content {
		    padding: 24px 23px 0px; 
		}
	</style>
</head> 
<body>
	<?php $this->load->view('../include/header.php'); ?>
	<div class="header-spacer"></div>
	<h3 class="text-center revi-of-dy">Videos Lectures</h3>
	<section class="video_lectures_slider">
	  <div class="container">
	    <div class="row">
	      <div class="col-md-12">
	        <div class="page-flow">
	        <ul class="breadcrumbs">
	          <li class="breadcrumbs-item">
	            <a href="<?php echo site_url(); ?>/web/Feeds">Home</a>
	            <span class="fc-icon fc-icon-right-single-arrow"></span>
	          </li>
	          <li class="breadcrumbs-item">
	            <a href="<?php echo site_url(); ?>/web/DailyDose">Daily Dose</a>
	            <span class="fc-icon fc-icon-right-single-arrow"></span>
	          </li>
	          <li class="breadcrumbs-item active">
	            <span>Videos Lectures</span>
	          </li>
	        </ul>
	      </div>
	    <div id="demo" class="carousel slide" data-ride="carousel"> 
	      <div class="carousel-inner">
	        <?php if(!empty($slider_images)){
	            $i = 0;
	            foreach($slider_images['data'] as $sliders){
	                if($i == 0){
	                    $class = 'active';
	                }else{
	                    $class = '';
	                }?>
	        <div class="carousel-item <?php echo $class; ?>">
	          <img class="slider-img-inner" src="<?php echo $sliders['image_url']; ?>" alt="Los Angeles">
	        </div>
	        <?php $i++;  } }?>
	      </div> 
	      <a class="carousel-control-prev" href="#demo" data-slide="prev">
	        <span class="carousel-control-prev-icon"></span>
	      </a>
	      <a class="carousel-control-next" href="#demo" data-slide="next">
	        <span class="carousel-control-next-icon"></span>
	      </a>
    	</div>
	</div>
</div>
</div>
</section>
<div class="video_category">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div  class="tab_box"> 
          <ul class="tab-buttons btn-box">
            <li data-tab="#pricing-tab-3" id="weekly" data="1" class="tab-btn active-btn videos">Latest</li>
            <li data-tab="#pricing-tab-1" id="monthly"  data="2" class="tab-btn videos">Trending</li>
            <li data-tab="#pricing-tab-2" id="yearly"  data="3" class="tab-btn videos">Favourite</li> 
          </ul> 
         </div>
      </div>
    </div> 
	<div class="tab-content" id="myTabContent">
	<?php 
		if(!empty($videos)){?>
	  	<div class="tab-pane fade show active" id="latest" role="tabpanel" aria-labelledby="latest-tab">
		    <div class="row">
		    	<?php foreach($videos['data'] as $video){  
		    		  $videoType = videoType($video['URL']);
			          if($videoType == 'youtube'){
			           $arrayurl = explode("watch?v=", $video['URL']);
		            $vid = explode("&", $arrayurl[1]);
		            $url = $arrayurl[0]."embed/".$vid[0];
		          }else{
		            $url =  $video['URL'];
		          }
      		?>
	       <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 ">
	      		<div class="ui-block video-item"> 
			        	<div class="video-player">
                  		<?php if($this->CI->remote_file_exists($video['thumbnail_url'], 1) ) { ?>
                        <img src="<?=$video['thumbnail_url'];?>" alt="photo">
                        <?php } else { ?>
                          <img src="<?php echo base_url('/web_assets/img/vdo_placeholder.jpg');?>" alt="photo">
                        <?php } ?> 
			          		<a href="javascript:void(0);"class="video-btn play-video" data-toggle="modal" data-id="<?=$video['id'];?>" data-main="<?=$videoType;?>" data-src="<?=$url;?>" data-target="#myModal<?=$video['id']?>" style="text-decoration: none;">
			          			<svg class="olymp-play-icon"><use xlink:href="/web_assets/svg-icons/sprites/icons.svg#olymp-play-icon"></use></svg>
			          		</a>
			          		<div class="overlay overlay-dark"></div>
			        	</div> 
		      			<div class="ui-block-content video-content">
				          	<p class="h6 para-title">
			          			<?php 	$titlecount = strlen($video['video_title']);
					            if($titlecount >20){
					              $dot= "...";
					            }else{$dot = "";}
	     						echo ucfirst(substr($video['video_title'],0,20)).$dot;
	         					?>
	 						</p>
				          	<p class="published">
					          	<?php $descripcount = strlen(strip_tags($video['video_desc']));
						            if($descripcount <=50){
						              echo $str = $video['video_desc'];
						            }else{$dots = "";
						              $str = strip_tags($video['video_desc']);
						              echo substr($str,0,25)."...";
						            }
		              			?>
	          				</p>
				        </div>
		      	</div>
	   		</div> 
			<div class="modal fade" id="myModal<?=$video['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal1" role="document">
					<div class="modal-content"> 
					  <div class="modal-body modal2"> 
					   <button type="button" class="close" data-main="<?=$videoType?>" data-id="<?=$video['id'];?>" data-dismiss="modal" aria-label="Close">
					      <span aria-hidden="true">&times;</span>
					    </button>         
					<div class="embed-responsive embed-responsive-16by9">
					<?php
					     if($videoType == 'mp4'){ ?>
					      <video controls autoplay>
					      <source  id="video<?=$videoType.$video['id'];?>"  src="" type="video/mp4">
					      <source  id="videoogg<?=$videoType.$video['id'];?>" src="" type="video/ogg">
						  </video>
					   <?php }else{ ?>
					     <iframe class="embed-responsive-item" src="" id="videos<?=$videoType.$video['id'];?>"  allowscriptaccess="always"></iframe>
							<?php  } ?> 
						</div> 
					  </div> 
					</div>
				</div>
			</div> 
		<?php } ?>
	    </div>
	</div>
	<?php } ?>
</div> 
</div>
</div>
<?php 
function videoType($url) {
    if (strpos($url, 'youtube') > 0) {
        return 'youtube';
    } elseif (strpos($url, 'vimeo') > 0) {
        return 'vimeo';
    } elseif(strpos($url, 'mp4') > 0) {
        return 'mp4';
    }else{
      return 'unknown';
    }
}
?>
<a class="back-to-top" href="#">
  <img src="/web_assets/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>
<div class="list-group full-width1 row">
  <div class="col-md-12 col-xs-12">
    <h3 align="center"><b>Subject-wise videos</b></h3>
  </div>
  <div class="col-md-12 col-xs-12 ">
  <?php if(!empty($d3['course_subject_master'])){ ?>
    <div class="row">
    <?php foreach($d3['course_subject_master'] as $data) { ?>
      <div class="col-md-4 col-xs-12 col- col-sm-12" style="margin-bottom: 10px">
        <a class="list-group-item list-group-item-action" style="font-weight: bold; color: white;font-size: 17px;" href="<?php echo site_url(); ?>/web/DailyDose/video_lists/<?php echo $data['id']; ?>/<?php echo $data['name']; ?>">
          <img src="<?php echo base_url('web_assets/img/makemyexam.png'); ?>" style="width:50px;height: 50px;">
          <span class="span-title"><?php echo $data['name']; ?></span>
          <span class="span-title-icon"><i class="fa fa-chevron-right" style="font-size:20px"></i></span>
        </a>
      </div>
    <?php } ?>
    </div>
  <?php } ?>
  </div>
  <br>  
</div>
<?php $this->load->view('../include/footer');?>
</body>
<script>
	$(document).ready(function(){
  $('.videos').on('click', this, function(){
    var videoValue = $(this).attr('data');
    $.ajax({
        type: 'POST',
        url: '<?php echo site_url('web/DailyDose/get_video_ajax'); ?>',
        data: {videoValue:videoValue},
        success: function (data) {
             $('#myTabContent').html(data); 
        },
        error:function(err){
            console.log("ERR"+err);
        },
    });
  });

  $('.video-btn').click(function() {
       var videoSrc = $(this).data( "src" );
       
       var videoType = $(this).data( "main" );
       var videoId = $(this).data( "id" );
      if(videoType == 'mp4'){
          $("#video"+videoType+videoId).attr('src',videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" );
          $("#videoogg"+videoType+videoId).attr('src',videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1");
     }else{
          $("#videos"+videoType+videoId).attr('src',videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" );
     }
  }); 
  $('.close').click(function() {
       var videoType = $(this).data( "main" );
       var videoId = $(this).data( "id" );
      if(videoType == 'mp4'){
          $("#video"+videoType+videoId).attr('src', '');
          $("#videoogg"+videoType+videoId).attr('src','');
     }else{
          $("#videos"+videoType+videoId).attr('src','');
     }
  });

});
</script>
<script>
    $("#monthly").click(function(){
      $("#pricing-tab-1").show();
      $("#pricing-tab-2").hide();  
      $("#pricing-tab-3").hide();    
      $('#yearly').removeClass("active-btn");
      $('#monthly').addClass("active-btn");
      $('#weekly').removeClass("active-btn");
    });

    $("#yearly").click(function(){      
      $("#pricing-tab-2").show();   
      $("#pricing-tab-1").hide();      
      $("#pricing-tab-3").hide();    
      $('#monthly').removeClass("active-btn");     
      $('#weekly').removeClass("active-btn");
      $('#yearly').addClass("active-btn");
    });

    $("#weekly").click(function(){      
      $("#pricing-tab-3").show(); 
      $("#pricing-tab-1").hide();
      $("#pricing-tab-2").hide();      
      $('#monthly').removeClass("active-btn");
      $('#yearly').removeClass("active-btn");
      $('#weekly').addClass("active-btn");
    });
 
</script>
</html>