<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php 
	echo str_replace('%20', ' ', $title); ?></title> 
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge"> 
	<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css"> 
  <link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
	<link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
	<link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css"> 
	<link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
</head>
<style type="text/css">
	.subject_video_box{
		background-color: #fff;
	    display: block;
	    position: relative;
	    margin-bottom: 30px;
	    -webkit-border-radius: 3px;
	    -moz-border-radius: 3px;
	    -ms-border-radius: 3px;
	    border-radius: 3px;
	    -webkit-box-shadow: 0px 0px 30px 0px rgba(0,0,0,0.1);
	    -moz-box-shadow: 0px 0px 30px 0px rgba(0,0,0,0.1);
	    box-shadow: 0px 0px 30px 0px rgba(0,0,0,0.1);
	}

    .post-video .video-content .video_title .title {
    	display: inline-flex;
	}

 	.favourite_icon{
	    display: -webkit-inline-box;
	    float: right;
	    max-width: 15px;
	}
  .video-thumb {
    position: relative;
     float: left; 
}

    .video-heading{font-size: 18px; color: black;}
    .video-date{font-size: 14px; color: darkgray}
    .video-dis{font-size: 15px; color: black}
    .suggested h1{font-weight: 500 }
    .video-content .h4{font-weight: 500 !important; font-size: 18px}
</style>
<body>
<?php $this->load->view('../include/header.php'); ?>
<div class="header-spacer"></div>

<div class="container">
  <div class="row">
  <div class="col-md-8 offset-md-2">

      <h2 align="center"><?= str_replace('%20', ' ', $title); ?></h2>
      <div class="page-flow">
        <ul class="breadcrumbs">
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/Feeds">Home</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/DailyDose">Daily Dose</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item">
          	 <a href="<?php echo site_url(); ?>/web/DailyDose/video">Videos Lectures</a>
          	 <span class="fc-icon fc-icon-right-single-arrow"></span>
           
          </li>
          <li class="breadcrumbs-item active">
           <span>Videos </span>
          </li>
        </ul>
      </div>
      <?php if(!empty($d3['data'])){  
 		foreach ($d3['data'] as $data) { ?>
      <div class="row">
        <div class="col-md-6 col-xs-12">
      <div class="subject_video_box subject_video_box1">
        <div class="post-video">
              <div class="video-thumb">
                <img src="<?php echo $data['thumbnail_url'] ?>" alt="photo" style="width:100%;height: 200px;">
                <a href="<?php echo site_url('/web/DailyDose/single_video/'.$data['id']); ?>" class="play-video">
                  <svg class="olymp-play-icon"><use xlink:href="/web_assets/svg-icons/sprites/icons.svg#olymp-play-icon"></use></svg>
                </a>
              </div>
          
              <div class="video-content">
              		<div class="video_title">
		                <a href="#" class="h4 title"><?php echo ucfirst($data['video_title']); ?></a>
		                <div class="favourite_icon">
							<a href="javascript:void(0);"><img src="/web_assets/img/bookmark.png" alt="" class="img-fluid"></a>
						</div>
					</div>
          <div class="quiz-box-date"><?php  echo date('d-M h:i A',$data['creation_time']/1000);?></div>
                <p class="video-dis"><?php $descripcount = strlen(strip_tags($data['video_desc']));
            if($descripcount <=150){
              echo $str = $data['video_desc'];
            }else{$dots = "";
              $str = strip_tags($data['video_desc']);
              echo substr($str,0,160)."...";
            }
              
              ?>
                </p>
                
              </div>
            </div>
            <div class="video_like">
              </div>
            </div>
          </div>
      </div>

<?php  }   ?>
    </div>
</div>
</div>
<?php }else{ ?>
<h3 align="center">No Video Found</h3>
<?php } ?>

</body>
<?php $this->load->view('../include/footer.php'); ?> 
</html>