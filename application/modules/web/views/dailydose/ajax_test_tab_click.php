<div class="tab-pane active" id="<?=$main[0];?>" role="tabpanel" aria-expanded="true">
  <?php
    static $index;
    $test_data1 = $test_data['question_bank'];

    $count = count($test_data1);
    $que_id = array();
    $data = array();

    for ($i = 0; $i < $count; $i++) {
        $index = $i + 1;
        if ($i == 0) {
            $style = 'block';
        } else {
            $style = 'none';
        }
  ?>                         
  <span class="local_data" test_id=<?php echo $test_data['basic_info']['id'];  ?> question_id= <?php echo  $test_data1[$i]['id']; ?>></span>
  <div class="content" id="content-<?php echo $i + 1; ?>" style="display: <?php echo $style; ?>;">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="question-language">
            <span>Question <?php echo $i + 1; ?>.</span>
            <span class="choose-language">
              <div class="w-select">
                <div class="title">Language</div>
                <fieldset class="form-group">
                  <select class="selectpicker form-control">
                    <option value="DA">English</option>
                    <option value="NU">Hindi</option>
                  </select>
                </fieldset>
              </div>
            </span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <p> <?php echo $test_data1[$i]['question'] ?></p>
        </div>
        <div class="col-md-6">
          <div class="radio">
              <label id="color1<?php echo $i + 1; ?>" class='cls'  seq="<?php echo $i + 1; ?>" >
                  <input type="radio" class="radio<?php 
                  echo ($i + 1) . " ";
                  if ($count == ($i + 1)) {
                      echo 'last_que';
                  }
                  ?>" name="optionsRadios<?php echo $i + 1; ?>" value="<?php echo $test_data1[$i]['option_1'] ?>" id="1" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                  A.
                  <?php echo $test_data1[$i]['option_1'] ?> </label>
                  <img id="1_<?php echo  $test_data1[$i]['id']; ?>" src="">
          </div>

          <div class="radio">
              <label id="color2<?php echo $i + 1; ?>" class='cls' seq="<?php echo $i + 1; ?>" >
                  <input type="radio" class="radio<?php
                  echo ($i + 1) . " ";
                  if ($count == ($i + 1)) {
                      echo 'last_que';
                  }
                  ?>" name="optionsRadios<?php echo $i + 1; ?>" value="<?php echo $test_data1[$i]['option_2'] ?>" id="2" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                  B.
                  <?php echo $test_data1[$i]['option_2'] ?>
               </label>
               <img id="2_<?php echo  $test_data1[$i]['id']; ?>" src="">
          </div>

          <div class="radio">
              <label id="color3<?php echo $i + 1; ?>" class='cls' seq="<?php echo $i + 1; ?>" >
                  <input type="radio" class="radio<?php
                  echo ($i + 1) . " ";
                  if ($count == ($i + 1)) {
                      echo 'last_que';
                  }
                  ?>" name="optionsRadios<?php echo $i + 1; ?>"  value="<?php echo $test_data1[$i]['option_3'] ?>" id="3" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                  C.
                  <?php echo $test_data1[$i]['option_3'] ?> </label>
                  <img id="3_<?php echo  $test_data1[$i]['id']; ?>" src="">
          </div>

          <div class="radio">
              <label id="color4<?php echo $i + 1; ?>" class='cls' seq="<?php echo $i + 1; ?>" >
                  <input type="radio" class="radio<?php
                  echo ($i + 1) . " ";
                  if ($count == ($i + 1)) {
                      echo 'last_que';
                  }
                  ?> " name="optionsRadios<?php echo $i + 1; ?>" value="<?php echo $test_data1[$i]['option_4'] ?>" id="4" onclick="check(this.id, '<?php echo $i + 1; ?>', '<?php echo $test_data1[$i]["answer"]; ?>','<?php echo  $test_data1[$i]['id']; ?>')">
                  D.
                  <?php echo $test_data1[$i]['option_4'] ?> </label>
                  <img id="4_<?php echo  $test_data1[$i]['id']; ?>" src="">
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>