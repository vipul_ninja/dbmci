 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Daily Quiz</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
  <link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
  <!-- Ibt Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
  <style type="text/css">
    .quiz_box .quiz_box_image img {
    max-height: 100px;
    max-width: 100px;
    padding: 15px;
	}
  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>web_assets/js/quiz.js"></script>

    <script type="text/javascript">
      
      var flag =0;
function gettestid(test_ids){
            var st = localStorage.getItem("eTvZ(5@9ETs");
            if(st){

            var decodestring = atob(st);
            var ss = JSON.parse(decodestring);
             for(let i=0;i<ss.length;i++){

              if(test_ids == ss[i]){  //check whether two same id
               flag=1; break;
               }
            }

              if(flag==1){
               flag=0;
               // alert("succe");
               return test_ids;
               }
               else
               {
                 return 0;
               }
             }else{ return 0;}
             
            
            
             }
    </script>
</head>
<body>
  <?php $this->load->view('../include/header.php');?>
<div class="header-spacer"></div>
<!-- quiz-box -->
<div class="container contant-footer">
  <div class="row">
   <?php  if(!empty($d1)){ 
           
         ?>
    <div class="col-md-8 offset-md-2">
      <h3 class="text-center revi-of-dy">Daily Quiz</h3>
      <div class="page-flow">
            <ul class="breadcrumbs">
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/Feeds">Home</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item">
            <a href="<?php echo site_url(); ?>/web/DailyDose">Daily Dose</a>
            <span class="fc-icon fc-icon-right-single-arrow"></span>
          </li>
          <li class="breadcrumbs-item active">
            <span>Daily Quiz</span>
          </li>
        </ul>
      </div>
       <?php $second_date = '';
        foreach($d1 as $data){
            $first_date = date("F d", $data['creation_time']/1000);
          ?>
          <?php if($first_date == $second_date){ 
            $second_date = $first_date;?>
          <?php }else{ 
            $second_date = $first_date;?>
           <h2 class="date"><?php 
            if($this->CI->check_time_ago_in_php($data['creation_time'])){
             echo date("F d", $data['creation_time']/1000);
           
           }else{
           echo $this->CI->time_ago_in_php($data['creation_time']);
            } 
            ?></h2>
          <?php }?>
      <div class="quiz_box">
        <div class="row no-gutters">
        <div class="col-md-2 col-xl-2 col-lg-2 col-sm-3 col-4">
          <div class="quiz_box_image">
            <img src="/web_assets/img/clock.png" alt="author">
          </div>
        </div>
        <div class="col-md-8 col-xl-8 col-lg-8 col-sm-6 col-8">
          <div class="quiz_box_details">
            <h2><?php echo $data['post_headline']?></h2>
            <div class="quiz-box-date"><?php 
              if($this->CI->check_time_ago_in_php($data['creation_time'])){
            
           echo date("F d, Y", $data['creation_time']/1000); 
           }else{
           echo $this->CI->time_ago_in_php($data['creation_time']);
            }  
             ?></div>
          </div>
          <div class="quiz_box_bottom">
            <ul class="list-inline">
              <li class="list-inline-item"><img src="/web_assets/img/coin.png"> <span><?php echo $data['quiz_coins']?> Coin </span></li>
               <li class="list-inline-item"><img src=""> <span><?php echo $data['quiz_attempt']?> Attempts </span></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2 col-xl-2 col-lg-2 col-sm-3">
          <div class="quiz_box_bookmar">
            <ul class="list-inline">
              <li class="list-inline-item"><a href="" class="bookmark" ><img src="/web_assets/img/bookmark.png" alt=""></a></li>
              <li class="list-inline-item"><a href="" class="bookmark"><img src="/web_assets/img/blueshare_xxhdpi.png" alt=""></a></li>
            </ul>
            <a style="margin-top: 37px;" href="javascript:void(0);"  setid="<?php echo $data['quiz_id']?>" class="btn btn-sm bg-blue start pop1" data-toggle="modal" data-target="#quiz-pop-up<?php echo $data['quiz_id']?>"  id="colors<?php echo $data['quiz_id']?>" onClick="window.open('<?=site_url();?>/web/DailyDose/quiz_by_id/<?php echo $data["quiz_id"];?>/1','width=1400,height=700,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0')" ><?php echo '<script type="text/javascript">',
         'var a = gettestid('.$data['quiz_id'].');
         if(a=='.$data['quiz_id'].'){ document.write("Resume Quiz"); }else{ document.write("Start Quiz");}',
        '</script>';
        ?></a>
          </div>
        </div>

<!-- //===========================================popup on start quiz===================================== -->
      <!--   <div class="modal fade" id="quiz-pop-up<?php echo $data['quiz_id']?>" tabindex="-1" role="dialog" aria-labelledby="quiz-pop-up" aria-hidden="true">
  <div class="modal-dialog window-popup create-friend-group quiz-pop-up" role="document">
    <div class="modal-content">
      <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
        <svg class="olymp-close-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
      </a>
      <div class="modal-header">
        <h6 class="title" id="testname<?php echo $data['quiz_id']?>">Quiz-Title</h6>
      </div>

      <div class="modal-body">
        <ul class="quiz-details-list">
              <li>
                Number of question<span id="nq<?php echo $data['quiz_id']?>"></span>
              </li>
              <li>
                Time (in minutes)<span id="t<?php echo $data['quiz_id']?>"></span>
              </li>
              <li>
                Total Marks <span id="tm<?php echo $data['quiz_id']?>"></span>
              </li>
              <li>
                Marks for Correct Answer<span id="ca<?php echo $data['quiz_id']?>"></span>
              </li>
              <li>
                Penalty for Wrong Answer<span id="wa<?php echo $data['quiz_id']?>"></span>
              </li>
            </ul>
       <a  type="button" class="btn btn-blue btn-lg full-width" data-dismiss="modal" onClick="window.open('<?=site_url();?>/web/DailyDose/quiz_by_id/<?php echo $data["quiz_id"];?>/1','width=1400,height=700,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0')", >Start Now
          </a>

      </div>
    </div>
  </div>
</div>
 -->
<!-- //=====================================end popup on start quiz===================================== -->
      </div>

      </div>
<?php } ?>
    </div>
  <?php } ?>
  </div>
  </div>
</br>
<?php $this->load->view('../include/footer.php'); ?>
 
<!-- Quiz-window pop-up -->



<!-- ... end Quiz-window pop-up  -->
<!-- JS Scripts -->

<script src="/web_assets/js/sticky-sidebar.js"></script>

<script src="/web_assets/js/base-init.js"></script>
<script defer src="/web_assets/fonts/fontawesome-all.js"></script>

<script src="/web_assets/Bootstrap/dist/js/bootstrap.bundle.js"></script>

   
   <script>

         // var modal = document.getElementById('myModal'); 
//===========================================popup on start==========================================
       //  $('.pop1').click(function() {
       // var id = $(this).attr('setid');
       //      var test_id = id;
       //     $.ajax({

       //          url: '<?php echo site_url()?>/web/DailyDose/quiz_by_id',
       //          type: 'post',
       //          data: {
       //              id: test_id
       //          },
       //          dataType:'json',
       //          success(data) {
       //             console.log(data);
       //      $('#testname'+test_id).html(data.data.basic_info.test_series_name);
       //      $('#nq'+test_id).html(data.data.basic_info.total_questions);
       //      $('#t'+test_id).html(data.data.basic_info.time_in_mins);
       //      $('#tm'+test_id).html(data.data.basic_info.total_marks);
       //      $('#ca'+test_id).html(data.data.basic_info.marks_per_question);
       //      $('#wa'+test_id).html(data.data.basic_info.negative_marking);
       //    },
       //          error(err) {
       //            console.log(err);

       //          }
       //      });
            
       //  });
//========================================End popup on start==========================================
    </script>

</body>