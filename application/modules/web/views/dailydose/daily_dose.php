<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Daily Dose</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
  <link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
     <style>
.col-md-6{
   text-align:center;
}
/* body{
    background: #ddd;
} */
.glyphicon{
    padding: 40px;
    font-size: 30px;
    color:white;
    border-radius:50%;
    
}
.col-md-3{
    padding: 20px;
    border: 1px solid #eee;
    text-align: center;     
}   
 .header-spacer {
    display: block;
    height: 110px;
}
.b{font-size: 20px; padding-top:10px;}
.daily_dose_grid{
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
}
#feed{width: 35%; background-color: white}
    .daily_dose_ds{
      background: white;
      text-align: center;
      border-radius: 10px; 
      margin: 8px auto; 
      max-width: max-content;  
      max-width: -moz-max-content;  
          height:fit-content;
    }
    .daily_dose_ds img{width: 50%;padding: 10px;}
    .daily_dose_ds .bottom-heading-quiz{
      background: #e9e9e9;
      margin-left: -15px;
      margin-right: -15px;
      padding: 7px;
      border-bottom-left-radius: 10px;
      border-bottom-right-radius: 10px;
    }
    .daily_dose_ds h5{
      color: #38a9ff;
      font-size: 20px;
      font-weight: 500;
    }
    </style>    

</head>
<?php $this->load->view('../include/header');?>

<body>

  <div class="main-box">
    <?php //$this->load->view('../include/header');?>
    
    <div class="header-spacer"></div>

    <div class="container "><!--daily_dose_grid-->
      <br>
      <div class="row">
      <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 filter-by-feeds" style="margin-bottom: 10px;">
        <div class="w-select w-select2">
          <span class="title" style="color: black;font-weight: bold;font-size: 15px;">Filter By:</span>
            <select name="feed" id="feed" class="select-content">
              <?php if(!empty($category_details)){ 
                foreach ($category_details['data']['main_category'] as $cat) {?>
              <option value="all"><?php echo $cat['text'];?></option>  
              <?php } }?>
            </select>
        </div>
      </div>

      
        <div class="col-md-4 daily_dose_ds col-xs-6">
            <img src="<?php echo base_url('web_assets/img/daily_quiz.png'); ?>">
          <a href="<?php echo site_url(); ?>/web/DailyDose/dailyquiz">
            <div class="bottom-heading-quiz">
              <h5>Daily Quiz</h5>
            </div>   
          </a>
        </div>
        <div class="col-md-4 daily_dose_ds col-xs-6">
            <img src="<?php echo base_url('web_assets/img/review_of_the_day.png'); ?>">
          <a href="<?php echo site_url(); ?>/web/DailyDose/review">
            <div class="bottom-heading-quiz">
              <h5>Review of the Day</h5>
            </div>   
          </a>
        </div>
        <div class="col-md-4 daily_dose_ds col-xs-6">
            <img src="<?php echo base_url('web_assets/img/current_affairs.png'); ?>">
          <a href="<?php echo site_url(); ?>/web/DailyDose/affair">
            <div class="bottom-heading-quiz">
              <h5>Current affair and Banking</h5>
            </div>   
          </a>
        </div>

        <div class="col-md-4 daily_dose_ds col-xs-6">
            <img src="<?php echo base_url('web_assets/img/video_lectures.png'); ?>">
          <a href="<?php echo site_url(); ?>/web/DailyDose/video">
            <div class="bottom-heading-quiz">
              <h5>Video Lectures</h5>
            </div>   
          </a>
        </div>
        <div class="col-md-4 daily_dose_ds col-xs-6">
            <img src="<?php echo base_url('web_assets/img/articles.png'); ?>">
          <a href="<?php echo site_url(); ?>/web/DailyDose/article">
            <div class="bottom-heading-quiz">
              <h5>Article</h5>
            </div>   
          </a>
        </div>
        <div class="col-md-4 daily_dose_ds col-xs-6">
            <img src="<?php echo base_url('web_assets/img/vocab_dose.png'); ?>">
          <a href="<?php echo site_url(); ?>/web/DailyDose/vocab">
            <div class="bottom-heading-quiz">
              <h5>Vocab Dose</h5>
            </div>   
          </a>
        </div>
        
      </div>
      

        <!-- <div class="row" style="height:200px;" >
          <a class="list-group-item list-group-item-action col-md-6 a1" href="<?php echo site_url(); ?>/web/DailyDose/dailyquiz">
            <div style="margin-top: 30px;"><img src="<?php //echo base_url('web_assets/img/daily_quiz.png'); ?>" style="width:100px;height: 100px;"><p><b class="b">Daily Quiz</b></p></div>
          </a>
          <a class="list-group-item list-group-item-action col-md-6 a1" href="<?php echo site_url(); ?>/web/DailyDose/review">
            <div style="margin-top: 30px;"><img src="<?php// echo base_url('web_assets/img/review_of_the_day.png'); ?>" style="width:100px;height: 100px;"><p><b class="b">Review of the Day</b></p></div>
          </a>
        </div>

        <div class="row" style="height:200px;" >
          <a class="list-group-item list-group-item-action col-md-6 a1" href="<?php echo site_url(); ?>/web/DailyDose/affair">
            <div style="margin-top: 30px;"><img src="<?php// echo base_url('web_assets/img/current_affairs.png'); ?>" style="width:100px;height: 100px;"><p><b class="b">Current affair and Banking</b></p></div>
          </a>
          <a class="list-group-item list-group-item-action col-md-6 a1" href="<?php echo site_url(); ?>/web/DailyDose/video">
            <div style="margin-top: 30px;"><img src="<?php //echo base_url('web_assets/img/video_lectures.png'); ?>" style="width:100px;height: 100px;"><p><b class="b">Video Lectures</b></p></div>
          </a>
        </div>

        <div class="row" style="height:200px;">
          <a class="list-group-item list-group-item-action col-md-6 a1" href="<?php echo site_url(); ?>/web/DailyDose/article">
            <div style="margin-top: 30px;"><img src="<?php //echo base_url('web_assets/img/articles.png'); ?>" style="width:100px;height: 100px;"><p><b class="b">Article</b></p></div>
          </a>
          <a class="list-group-item list-group-item-action col-md-6 a1" href="<?php echo site_url(); ?>/web/DailyDose/vocab">
            <div style="margin-top: 30px;"><img src="<?php// echo base_url('web_assets/img/vocab_dose.png'); ?>" style="width:100px;height: 100px;"><p><b class="b">Vocab Dose</b></p></div>
          </a>
        </div> -->

    
     </div>      
<div class="header-spacer"></div>
<?php  $this->load->view('../include/footer'); ?>

</div>

</body>
</html>