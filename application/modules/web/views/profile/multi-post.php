<?php if(!$this->session->userdata('username')){
  header("Location: Home");
}?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Feeds</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">
$(window).load(function() {
    $(".loader").fadeOut("slow");
});
</script>

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
<link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
<!-- Main Styles CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
<link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css">
<link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
<!-- <link rel="stylesheet" type="text/css" href="/web_assets/css/main.min-cs.css"> -->
	

<?php $this->load->view('include/header.php'); ?>
</head>
<style>
  .feeds-post-bydev{
    background: #fff;
    padding: 25px;
    width: 668px;
    border-top: 1px solid lightgray;
    /* border-bottom: 1px solid lightgray; */
    margin-top: -16px;
}
/* .header {
  padding: 10px 16px;
  background: #555;
  color: #f1f1f1;
} */

/* .content {
  padding: 16px;
} */

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 52px;
}
</style>
<body style="background:#f5f5f5;min-height:700px;">
<div class="loader"></div>

<div class="header-spacer"></div>
<div style="min-height:700px;">
  <div class="container">
    <div class="page-flow">
      <ul class="breadcrumbs">
        <li class="breadcrumbs-item">
          <a href="http://13.232.24.221/index.php/web/Feeds">Home</a>
          <span class="fc-icon fc-icon-right-single-arrow"></span>
        </li>
        <li class="breadcrumbs-item active">
          <span>Study</span>
        </li>
      </ul>
    </div>  
  </div>
  <div class="container">
    <div class="row">
      <div class="col-8">
        <div class="multi_post">
          <h2>Bank & Insurance Exams 2018</h2>
          <span>Oct 26, 2018 - 13:42</span>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
          </p>
        </div>
        <section id="tabs">
          <div class="container">
          <hr>
            <!-- <h6 class="section-title h1">Tabs</h6> -->
            <div class="row">
              <div class="col-xs-12 single-post-feeds" style="border: 1px solid lightgray;background: #fff;">
                <nav class="header" id="myHeader" style="background: lightgray;font-size: 18px;">
                  <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#feeds" role="tab" aria-controls="nav-home" aria-selected="true">Feeds</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#all-feeds" role="tab" aria-controls="nav-profile" aria-selected="false">All Feeds</a>
                  
                  </div>
                </nav>
                <div class="tab-content py-3 px-3 px-sm-0 content" id="nav-tabContent">
                  <div class="tab-pane fade show active" id="feeds" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="feeds-post-bydev">
                      <div class="row">
                        <div class="col-12">
                          <h5>Deva sheesh</h5>
                          <p> hours ago</p>
                          <span> 1 attempts | 2 Like | 5 comments</span>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-4 text-center">
                          <span><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class='far fa-comment'></i> Comment</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class="fa fa-share-alt" aria-hidden="true"></i> Share</span>
                        </div>                      
                      </div>
                    </div>                   
                  </div>
                  <div class="tab-pane fade" id="all-feeds" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="feeds-post-bydev">
                      <div class="row">
                        <div class="col-12">
                          <h5>Deva sheesh</h5>
                          <p> hours ago</p>
                          <span> 1 attempts | 2 Like | 5 comments</span>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-4 text-center">
                          <span><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class='far fa-comment'></i> Comment</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class="fa fa-share-alt" aria-hidden="true"></i> Share</span>
                        </div>                      
                      </div>
                    </div>
                    <div class="feeds-post-bydev">
                      <div class="row">
                        <div class="col-12">
                          <h5>Deva sheesh</h5>
                          <p> hours ago</p>
                          <span> 1 attempts | 2 Like | 5 comments</span>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-4 text-center">
                          <span><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class='far fa-comment'></i> Comment</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class="fa fa-share-alt" aria-hidden="true"></i> Share</span>
                        </div>                      
                      </div>
                    </div>
                    <div class="feeds-post-bydev">
                      <div class="row">
                        <div class="col-12">
                          <h5>Deva sheesh</h5>
                          <p> hours ago</p>
                          <span> 1 attempts | 2 Like | 5 comments</span>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-4 text-center">
                          <span><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class='far fa-comment'></i> Comment</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class="fa fa-share-alt" aria-hidden="true"></i> Share</span>
                        </div>                      
                      </div>
                    </div>
                    <div class="feeds-post-bydev">
                      <div class="row">
                        <div class="col-12">
                          <h5>Deva sheesh</h5>
                          <p> hours ago</p>
                          <span> 1 attempts | 2 Like | 5 comments</span>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-4 text-center">
                          <span><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class='far fa-comment'></i> Comment</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class="fa fa-share-alt" aria-hidden="true"></i> Share</span>
                        </div>                      
                      </div>
                    </div>
                    <div class="feeds-post-bydev">
                      <div class="row">
                        <div class="col-12">
                          <h5>Deva sheesh</h5>
                          <p> hours ago</p>
                          <span> 1 attempts | 2 Like | 5 comments</span>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-4 text-center">
                          <span><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class='far fa-comment'></i> Comment</span>
                        </div>
                        <div class="col-4 text-center">
                          <span><i class="fa fa-share-alt" aria-hidden="true"></i> Share</span>
                        </div>                      
                      </div>
                    </div>   
                  </div>                  
                </div> 
              </div>
            </div>                    
          </div>
        </section>
      </div>
      <div class="col-4">
      <img src="https://i.ytimg.com/vi/LkBeuTt63Pg/hqdefault.jpg" style="width:190px;height: 130px;">
      </div>
    </div>
  </div>
</div>
<div class="header-spacer"></div>
</body>
<?php $this->load->view('include/footer.php'); ?>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header121.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
</html>