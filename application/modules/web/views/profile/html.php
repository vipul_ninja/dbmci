<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/web_assets/css/main.min.css">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/web_assets/Bootstrap/dist/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/web_assets/css/ibt_main.css"> 
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
 
 
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    /* .row.content {height: 1500px} */
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
    .sidenav{
        margin-top: 40px;
    }
    .profile-page .profile-left{padding: 40px 30px;}
    .user-profile{
        border: 2px solid #e8e8e8;
        border-radius: 5px;
        padding: 10px;
        margin-left: 0px;
        margin-right: 0px;
    }
    .profile-post{
        border: 2px solid #e8e8e8;
        padding: 10px;
        margin-top: 10px;
    }
    .profile-video{
        border: 2px solid #e8e8e8;
        padding: 10px;
        margin-top: 10px;
    }
    .user-profile .col-sm-3 img{
        width: 75%;
        border-radius: 50%;
    }
    /* .mentor-tick {
    border-radius: 100%;
    height: 18px;
    width: 18px;
    padding: 5px 4px;
    -ms-grid-row-align: center;
    align-self: center;
    margin-left: 5px;
    }
    .rocket-green-bg {
        background-color: #45b97c;
    }
    .safed-tick-icon use {
        stroke: #FFF;
    }    */
    .nav-tabs.profile-nav{
        border: 2px solid #e8e8e8;
        width: 100%;
    }
    .nav-tabs.profile-nav>li{
        width: 50%;
        text-align: center;  
    }
    .nav-tabs.profile-nav>li>a{
    font-size: 18px;
    }
    .nav-tabs.profile-nav>li>a, .nav-tabs>li>a:focus, .nav-tabs>li>a:hover{
        background-color: transparent;
        border: none;  
    }    
    .nav-tabs.profile-nav>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background-color: transparent;
        border: none;
        border-bottom: 2px solid green;
    }
    .tab-content{padding:0px;}
    .tab-content>.active{
        border: 2px solid #e8e8e8;
        padding: 10px;
        margin-top: 10px;
    }
    .pro-left-cont i{
        float: left;
        font-size: 32px;
        padding-right: 15px;    
    }
    .cont-lft{width:50%;    float: left;}
    .cont-rt{width: 50%;    float: right;}
    .active-since h4{margin-top: -2px;}
    .active-since p{padding-left: 44px;}
    .profile-post img{
        width: 5%;
        border-radius: 50%;
        float: left;
        margin-right:20px;
    }
    .poster-emp{margin-top: -10px;}

    .profile-video .left-vio{ 
        text-align: center;
        border: 1px solid gainsboro;
        width: min-content; 
    }
    .profile-video .right-vio{
        text-align: center; 
        border: 1px solid gainsboro;
        width: min-content;
    }
    .profile-video p{padding: 10px;}
    .sidenav {
    background-color: transparent;
    }
    .profile-video p {
        padding: 10px 10px;
        border: 1px solid;
        width: 76%;
    }
    .profile-ibt{
        margin-top: 80px;
    }
    .profile-ibt h3{
        font-weight: 500;
        font-size: 20px;
    }
    .profile-ibt h4{
        font-size: 17px;
        font-weight: 500;
    }
  </style>
</head>
<body>
<?php $this->load->view('include/header');?>
<div class="container profile-ibt" style="">
  <div class="row content profile-page">
    <div class="col-sm-9 profile-left">
      <!-- <h4><small>RECENT POSTS</small></h4>
      <hr> -->
      <?php// pre($profile);pre($sub_cat);die;?>
      <div class="row user-profile">
          <div class="col-sm-3">
              <img src="<?php echo  $profile['data']['profile_picture']?>" alt="">
          </div>
          <div class="col-sm-9">
              <h2 style="color:#000;"><?php echo  $profile['data']['name']?></h2>
              
              <br>
              <p><?php echo  $profile['data']['location']?></p>
          </div>
      </div>
      
      <br>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs profile-nav" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">ABOUT</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu1">FOLLOWER</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <!-- <div id="home" class="container tab-pane active"><br>
                <h3>ABOUT</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div> -->

            <div id="home" class="tab-pane in active pro-left-cont">    
                <h3>Info</h3>
                <div class="row">
                    <div class="col-sm-6">            
                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                        <div class="active-since">
                            <h4>Active Since</h4>
                            <p><?php echo  date('M, Y',$profile['data']['creation_time']/1000)?></p>
                        </div>
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <div >
                            <h4>Exam Category</h4>
                            <p>Bank & Insurance</p>
                        </div>
                       
                    </div>
                    <div class="col-sm-6">  
                        <i class="fa fa-bolt" aria-hidden="true"></i>
                        <div >
                            <h4>Highlights</h4>
                            <p>English expert at gradeup</p>
                        </div>           
                    </div>
                </div>
            </div>

            <div id="menu1" class="container tab-pane"><br>
                <h3>FOLLOWER</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
            <div class="profile-post">
                <h3>Posts</h3>
                <img src="<?php echo  $profile['data']['profile_picture']?>" alt="">
                <div  class="poster-emp">
                    <h4><?php echo  $profile['data']['name']?></h4>
                    <p style="padding-left:44px;"><?php echo  $profile['data']['post_count']?></p>
                </div>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p>
            </div>
            <div class="profile-video">
                <h3>Videos</h3>
                <div class="row">
                    <div class="col-6">
                        <div class="left-vio121">
                            <img src="/web_assets/img/video1.png" alt="">
                            <p>
                                IBPS SO 2018 Notification Out for IT Officer/Agriculture/Mar
                            </p>
                        </div>                        
                    </div>
                    <div class="col-6">
                        <div class="right-vio121">
                            <img src="/web_assets/img/video2.png" alt="">
                            <p>
                                ESIC SSO Prelims 2018 | Exam Strategy | Expected Pattern | E
                            </p>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
      <!-- <div class="row">
        <ul class="nav nav-tabs profile-nav">
            <li class="active"><a data-toggle="tab" href="#home">ABOUT</a></li>
            <li><a data-toggle="tab" href="#menu1">FOLLOWER</a></li>
        </ul>

        <div class="tab-content">
            
            <div id="home" class="tab-pane fade in active pro-left-cont">    
                <h3>Info</h3>
                <div class="row">
                    <div class="col-sm-6">            
                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                        <div class="active-since">
                            <h4>Active Since</h4>
                            <p><?php echo  date('M, Y',$profile['data']['creation_time']/1000)?></p>
                        </div>
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <div >
                            <h4>Exam Category</h4>
                            <p>Bank & Insurance</p>
                        </div>
                       
                    </div>
                    <div class="col-sm-6">  
                    <i class="fa fa-bolt" aria-hidden="true"></i>
                        <div >
                            <h4>Highlights</h4>
                            <p>English expert at gradeup</p>
                        </div>          
                        <!-- <i class="fa fa-briefcase" aria-hidden="true"></i>
                        <div  class="active-since">
                            <h4>Queries Answered</h4>
                            <p style="padding-left:44px;">11</p>
                        </div> -->
                    <!--</div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <h3>flowers</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div> 
            <div class="profile-post">
                <h3>Posts</h3>
                <img src="https://ibtapp-project.s3.ap-south-1.amazonaws.com/user_display/288463543" alt="">
                <div  class="poster-emp">
                    <h4><?php echo  $profile['data']['name']?></h4>
                    <p style="padding-left:44px;"><?php echo  $profile['data']['post_count']?></p>
                </div>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p>
            </div>
            <div class="profile-video">
                <h3>Videos</h3>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="left-vio">
                            <img src="/web_assets/img/video1.png" alt="">
                            <p>
                                IBPS SO 2018 Notification Out for IT Officer/Agriculture/Mar
                            </p>
                        </div>                        
                    </div>
                    <div class="col-sm-6">
                        <div class="right-vio">
                            <img src="/web_assets/img/video2.png" alt="">
                            <p>
                                ESIC SSO Prelims 2018 | Exam Strategy | Expected Pattern | E
                            </p>
                        </div>
                       
                    </div>
                </div>
            </div>              
        </div>
      </div> -->
    </div>
    <br>
    <br>
    <div class="col-sm-3 sidenav">
        <img src="/web_assets/img/add.png" width="100%" alt="">
        <br>
      <!-- <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Blog..">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>
      </div> -->
    </div>
  </div>
</div>

<!-- <footer class="container-fluid">
  <p>Footer Text</p>
</footer> -->

</body>
</html>
<script src="<?php echo base_url(); ?>/web_assets/js/jquery-3.2.1.js"></script> 
<script src="<?php echo base_url(); ?>/web_assets/js/base-init.js"></script>  
<script src="<?php echo base_url(); ?>/web_assets/js/svgxuse.js"></script>
<!-- <script defer src="<?php echo base_url(); ?>/web_assets/fonts/fontawesome-all.js"></script>  -->
<script src="<?php echo base_url(); ?>/web_assets/Bootstrap/dist/js/bootstrap.bundle.js"></script> 