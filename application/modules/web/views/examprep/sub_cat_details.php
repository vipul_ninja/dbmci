<!DOCTYPE html>
<html>
<head>
	<title>Exam Preparation</title>
	<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
  <link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  
<!-- Main Styles CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
<!-- <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css"> -->
<link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
<style>
    .sub-cat-exm{    
        margin: 30px auto;
        /* width: 50%; */
    }
	.sub-cat-exm .sub-link-nav1 .nav-item a{
        padding: 10px 25px;
        border: 1px solid #fff;
        font-size: 16px;
        color: #fff;
        font-weight: 600;
    }
    .sub-cat-exm .sub-link-nav1 .nav-item{
        padding: 27px 20px;
    }
    .sub-cat-exm .heading-unimg {
        background: #fff;
        border: 1px solid #dad1d1;
        border-radius: 4px;
        /* box-shadow: 1px 1px 1px 1px; */
    }
    .sub-cat-exm .heading-unimg img{
        width: 20%;
        float: left;
    }
    .sub-link-nav1{
        margin: 0 auto;
        width: fit-content;
    }
    .sub-cat-exm .sub-tab-cont{ width: -webkit-fill-available; }
    .sub-cat-exm .video-thumb img{
        width: 35%;
        padding: 10px;
    }
    .tab-pane-sub .tab-dis{
        background-color: white;
        /* box-shadow: 1px 1px 1px 1px; */
    }
    .subject_video_box .video_title a{font-weight: 500;}
    .video-thumb1 img{width:60%; margin:5px;}
    .sub-cat-exm .heading-unimg h2{
        font-weight: 400;
        font-size: 26px;
        padding-top: 15px;
    }
    .she-img{padding: 20px 28px;}
    .she-img img{float: left;}
    .subject_video_box .video_title span{
        font-size: 16px;
        padding: 10px;
        color: #000;
    }
    .sub-cat-exm .tab-content{
        padding: 0px 100px;
    } 
    .nav-item a::before{ background-color: transparent;}
    .nav-pills .nav-link{
        color: #000;
    }    
    .nav-pills .nav-link.active{
        background-color: #ffffff;
        color: #fff !important;
        font-size: 16px;
        border: 1px solid #38a9ff !important;
        text-decoration: underline;
    }
    .sub-cat-exm button{
        padding: 15px 60px;
        font-size: 18px;
        background: #3b95d6;
        border: none;
    }
    .post-video .video-content{
        height: 100%;
    }
    .sub-cat-exm button:hover{background: #3b95d6;}
    .bg-test{ background: #0000CC; }
    .bg-vid{ background: #F68930; }
    .bg-book{ background: #9C5E87;  }
    .bg-concept{ background: #E52828; }
</style>
</head> 
<body>
    <div class="bottom_loader" style="display: none;"></div>
    <div class="main-box">
        <?php $this->load->view('../include/header');?>  
    <div class="header-spacer"></div>

    <!-- ******************* Start Sub Cat Details ********************** -->
    
    
    <div class="container sub-cat-exm">
        <div class="heading-unimg  text-center">
            <div class="row">
                <div class="col-md-4 col-xs-12 she-img">
                    <img src="<?=$topic_data['data']['basic']['image_icon'];?>" alt="author">
                    <h2><?=$topic_data['data']['basic']['title'];?></h2>                        
                </div>   
                <div class="col-md-8 col-xs-12">
                    <!-- Nav pills -->
                    <ul class="nav nav-pills sub-link-nav1" role="tablist">
                        <?php if(!empty($topic_data['data']['tiles'])){ 
                            $i =0;
                                foreach($topic_data['data']['tiles'] as $tiles){
                                if($i == 0){
                                    $active = 'active show';
                                }else{
                                    $active = '';
                                } ?>
                            <li class="nav-item">
                            <a class="nav-link <?=$active;?>" data-toggle="pill" href="#<?=$tiles['type'];?>" style="background-color:<?=$tiles['c_code'];?>"><?=$tiles['tile_name'];?></a>
                            </li>
                        <?php $i++; } }?>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Tab panes -->
        <div class="tab-content">
            <div id="test" class="container tab-pane tab-pane-sub active"><br>
                <div class="row">
                <?php if(!empty($test_data['data']['list'])){
                        foreach($test_data['data']['list'] as $test){
                ?>
               
                    <div class="tab-dis col-xs-12"> 
                        <div class="subject_video_box"> 
                            <div class="post-video post-video11 row">
                                <div class="video-thumb1 col-md-3 col-xs-12">
                                    <?php if($test['image_icon'] != ""){
                                        $test_image = $test['image_icon'];
                                    }else{
                                        $test_image = base_url('web_assets/img/makemyexam.png');
                                    }?>
                                    <img src="<?=$test_image;?>" alt="photo" style="background-color: <?=$test['c_code'];?>"> 
                                </div> 
                                <div class="video-content col-md-9 col-xs-12">
                                    <div class="video_title">
                                        <a href="<?php echo site_url('/web/Exam_Prep/subjectDetails/');?>" class="h4 title" ><?=$test['title'];?></a>
                                        <?php $test_count = sizeof($test['total']);
                                                for($t = 0;$t<$test_count;$t++){ ?>
                                        <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <?=$test['total'][$t]['count']." ".$test['total'][$t]['text'];?></span>
                                        <?php } ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                <?php } } ?>
            </div>
            </div>
               
            <div id="video" class="container tab-pane tab-pane-sub fade"><br>
                 <?php if(!empty($video_data['data']['list'])){
                        foreach($video_data['data']['list'] as $video){
                ?>
                    <div class="tab-dis col-xs-12"> 
                        <div class="subject_video_box"> 
                            <div class="post-video post-video11 row">
                                <div class="video-thumb1 col-md-3 col-xs-12">
                                    <?php if($video['image_icon'] != ""){
                                        $video_image = $video['image_icon'];
                                    }else{
                                        $video_image = base_url('web_assets/img/makemyexam.png');
                                    }?>
                                    <img src="<?=$video_image;?>" alt="photo" style="background-color: <?=$video['c_code'];?>"> 
                                </div> 
                                <div class="video-content col-md-9 col-xs-12">
                                    <div class="video_title">
                                        <a href="javascript:void(0);" class="h4 title" ><?=$video['title'];?></a>
                                        <?php $video_count = sizeof($video['total']);
                                                for($v = 0;$v<$video_count;$v++){ ?>
                                        <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <?=$video['total'][$v]['count']." ".$video['total'][$v]['text'];?></span>
                                        <?php } ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } } ?>
            </div>
            <div id="book" class="container tab-pane tab-pane-sub fade"><br>
                 <?php if(!empty($book_data['data']['list'])){
                        foreach($book_data['data']['list'] as $book){
                ?>
                    <div class="tab-dis col-xs-12"> 
                        <div class="subject_video_box"> 
                            <div class="post-video post-video11 row">
                                <div class="video-thumb1 col-md-3 col-xs-12">
                                    <?php if($book['image_icon'] != ""){
                                        $book_image = $book['image_icon'];
                                    }else{
                                        $book_image = base_url('web_assets/img/makemyexam.png');
                                    }?>
                                    <img src="<?=$book_image;?>" alt="photo" style="background-color: <?=$book['c_code'];?>"> 
                                </div> 
                                <div class="video-content col-md-9 col-xs-12">
                                    <div class="video_title">
                                        <a href="javascript:void(0);" class="h4 title" ><?=$book['title'];?></a>
                                        <?php $book_count = sizeof($book['total']);
                                                for($b = 0;$b<$book_count;$b++){ ?>
                                        <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <?=$book['total'][$b]['count']." ".$book['total'][$b]['text'];?></span>
                                        <?php } ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } } ?>
            </div>
            <div id="concept" class="container tab-pane tab-pane-sub fade"><br>
                 <?php if(!empty($concept_data['data']['list'])){
                        foreach($concept_data['data']['list'] as $concept){
                ?>
                    <div class="tab-dis col-xs-12"> 
                        <div class="subject_video_box"> 
                            <div class="post-video post-video11 row">
                                <div class="video-thumb1 col-md-3 col-xs-12">
                                    <?php if($concept['image_icon'] != ""){
                                        $concept_image = $concept['image_icon'];
                                    }else{
                                        $concept_image = base_url('web_assets/img/makemyexam.png');
                                    }?>
                                    <img src="<?=$concept_image;?>" alt="photo" style="background-color: <?=$concept['c_code'];?>"> 
                                </div> 
                                <div class="video-content col-md-9 col-xs-12">
                                    <div class="video_title">
                                        <a href="javascript:void(0);" class="h4 title" ><?=$concept['title'];?></a>
                                        <?php $concept_count = sizeof($concept['total']);
                                                for($c = 0;$c<$concept_count;$c++){ ?>
                                        <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <?=$concept['total'][$c]['count']." ".$concept['total'][$c]['text'];?></span>
                                        <?php } ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } } ?>
            </div>
        </div>
        <div class="text-center">
            <button type="button" class="btn btn-primary">Buy Now</button>
        </div>
    </div>
    <!-- ******************* End Sub Cat Details ************************ -->
    
  <div class="header-spacer"></div>
    <?php  $this->load->view('../include/footer'); ?>

</body>
</html>