<!DOCTYPE html>
<html>
<head>
	<title>Exam Preparation</title>
	<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
  <link rel="icon" href="/web_assets/img/favicon.png" type="image/png" sizes="16x16">
  
<!-- Main Styles CSS -->
<link rel="stylesheet" type="text/css" href="/web_assets/css/main.min.css">
<!-- <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css"> -->
<link rel="stylesheet" type="text/css" href="/web_assets/css/ibt_main.css">
<style>
    .sub-cat-exm{    
        margin: 30px auto;
        /* width: 50%; */
    }
	.sub-cat-exm .sub-link-nav1 .nav-item a{
        padding: 10px 25px;
        border: 1px solid #fff;
        font-size: 16px;
        color: #fff;
    }
    .sub-cat-exm .sub-link-nav1 .nav-item{
        padding: 27px 20px;
    }
    .sub-cat-exm .heading-unimg {
        background: #fff;
        border: 1px solid #dad1d1;
        border-radius: 4px;
        /* box-shadow: 1px 1px 1px 1px; */
    }
    .sub-cat-exm .heading-unimg img{
        width: 20%;
        float: left;
    }
    .sub-link-nav1{
        margin: 0 auto;
        width: fit-content;
    }
    .sub-cat-exm .sub-tab-cont{ width: -webkit-fill-available; }
    .sub-cat-exm .video-thumb img{
        width: 35%;
        padding: 10px;
    }
    .tab-pane-sub .tab-dis{
        background-color: white;
        /* box-shadow: 1px 1px 1px 1px; */
    }
    .subject_video_box .video_title a{font-weight: 500;}
    .video-thumb1 img{width:60%; margin:5px;}
    .sub-cat-exm .heading-unimg h2{
        font-weight: 400;
        font-size: 26px;
        padding-top: 15px;
    }
    .she-img{padding: 20px 28px;}
    .she-img img{float: left;}
    .subject_video_box .video_title span{
        font-size: 16px;
        padding: 10px;
        color: #000;
    }
    .sub-cat-exm .tab-content{
        padding: 0px 100px;
    } 
    .nav-item a::before{ background-color: transparent;}
    .nav-pills .nav-link.active{
        background-color: #ffffff;
        color: black !important;
        font-size: 16px;
        border: 1px solid #38a9ff !important;
    }
    .sub-cat-exm button{
        padding: 15px 60px;
        font-size: 18px;
        background: #3b95d6;
        border: none;
    }
    .sub-cat-exm button:hover{background: #3b95d6;}
    .bg-test{ background: #0000CC; }
    .bg-vid{ background: #F68930; }
    .bg-book{ background: #9C5E87;  }
    .bg-concept{ background: #E52828; }
    .sub-cat-exm .h1-h3 h1{
        font-weight: 400;
        color: #000;
    }
    .sub-cat-exm .h1-h3 h3{
        font-weight: 400;
        color: #000;
        padding: 0px 70px;
    }
</style>
</head> 
<body>
    <div class="bottom_loader" style="display: none;"></div>
    <div class="main-box">
        <?php $this->load->view('../include/header');?>  
    <div class="header-spacer"></div>

    <!-- ******************* Start Sub Cat Details ********************** -->
    
    
    <div class="container sub-cat-exm">
        <div class="h1-h3">
            <h1 class="text-center">SBI PO & CLERK</h1>
            <h3>Banking Exam</h3>
        </div>
        
        <div class="tab-content">
            <div id="home" class="container tab-pane tab-pane-sub active"><br>
                
                    <div class="tab-dis"> 
                        <div class="subject_video_box">
                            
                            <div class="post-video row">
                                
                                    <div class="video-thumb1 col-3">
                                        <img src="https://ibtapp-project.s3.ap-south-1.amazonaws.com/course_file_meta/1738653education-icon-300x300.png" alt="photo"> 
                                    </div> 
                                    <div class="video-content col-9">
                                        <div class="video_title">
                                            <a href="<?php echo site_url('/web/Exam_Prep/subjectDetailsLast/');?>" class="h4 title">Trignometry</a>
                                            <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> 7 Full length test</span>
                                            
                                        </div>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                
                
                <div class="tab-dis"> 
                    <div class="subject_video_box">
                        <div class="post-video row">
                            <div class="video-thumb1 col-3">
                                <img src="https://ibtapp-project.s3.ap-south-1.amazonaws.com/course_file_meta/1738653education-icon-300x300.png" alt="photo"> 
                            </div> 
                            <div class="video-content col-9">
                                <div class="video_title">
                                    <a href="javascript:void(0);" class="h4 title">Trignometry</a>
                                    <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> 7 videos</span>
                                   
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-dis"> 
                    <div class="subject_video_box">
                        <div class="post-video row">
                            <div class="video-thumb1 col-3">
                                <img src="https://ibtapp-project.s3.ap-south-1.amazonaws.com/course_file_meta/1738653education-icon-300x300.png" alt="photo"> 
                            </div> 
                            <div class="video-content col-9">
                                <div class="video_title">
                                    <a href="javascript:void(0);" class="h4 title">Trignometry</a>
                                    <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> 7 videos</span>
                                   
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-dis"> 
                    <div class="subject_video_box">
                        <div class="post-video row">
                            <div class="video-thumb1 col-3">
                                <img src="https://ibtapp-project.s3.ap-south-1.amazonaws.com/course_file_meta/1738653education-icon-300x300.png" alt="photo"> 
                            </div> 
                            <div class="video-content col-9">
                                <div class="video_title">
                                    <a href="javascript:void(0);" class="h4 title">Trignometry</a>
                                    <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> 7 videos</span>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-dis"> 
                    <div class="subject_video_box">
                        <div class="post-video row">
                            <div class="video-thumb1 col-3">
                                <img src="https://ibtapp-project.s3.ap-south-1.amazonaws.com/course_file_meta/1738653education-icon-300x300.png" alt="photo"> 
                            </div> 
                            <div class="video-content col-9">
                                <div class="video_title">
                                    <a href="javascript:void(0);" class="h4 title">Trignometry</a>
                                    <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i> 7 videos</span>
                                   
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <!-- <div class="text-center">
            <button type="button" class="btn btn-primary">Buy Now</button>
        </div> -->
    </div>
    <!-- ******************* End Sub Cat Details ************************ -->
    
  <div class="header-spacer"></div>
    <?php  $this->load->view('../include/footer'); ?>

</body>
</html>