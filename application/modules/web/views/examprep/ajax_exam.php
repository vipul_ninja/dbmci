        <div class="col-3 ul-li">
        <ul class="tabs-li ab">
          <?php if(!empty($category_details)){
                  $j = 0;
                  foreach ($category_details['data']['main_sub_category'] as $sub_cate) {
                    // $active= '';
                    if($j == 0){
                      $active = 'active';
                    }else{
                      $active ='';
                    }
                    if($parent_id == $sub_cate['parent_id']){ 
                       ?>
                      <li id="<?=$sub_cate['id'];?>" class="cate <?=$active;?>"><a href="javascript:void(0);" id="<?=$sub_cate['id'];?>"><?=$sub_cate['text'];?></a></li>
          <?php } ++$j; } } ?>
        </ul>
       </div>
       <div class="col-9">
               
              <div class="row heading-info" id="ibpspo">
                <div class="col-6"><h4></h4></div>
              </div>
              <div class="row">
                  <?php if(!empty($cat)){
                        foreach ($cat['data'] as $lists) {
                         ?>   
              <div class="col-4">
              <div class="tab-dis"> 
                <div class="subject_video_box">
                  <div class="post-video">
                    <a href="<?php echo site_url('/web/Exam_Prep/subCatInfo/'.$lists['id']);?>">
                    <div class="video-thumb">
                      <img src="<?=$lists['cover_image'];?>" alt="photo"> 
                    </div> 
                    <div class="video-content">
                      <div class="video_title">
                        <a href="javascript:void(0);" class="h4 title"><?=$lists['title']; ?></a>
                      </div>
                      <ul>
                        <?php $course_contr = explode(",",$lists['course_attribute']);
                        for($i=0;$i<sizeof($course_contr);$i++){ ?>
                        <li><?=$course_contr[$i];?></li>
                      <?php } ?>
                      </ul>
                      <div class="quiz-box-date"> 
                        <span><i class="fa fa-rupee"></i><?=$lists['course_sp'];?></span>&nbsp;
                        <span><strike><i class="fa fa-rupee"></i><?=$lists['mrp'];?></strike></span>
                        <span style="float: right;">Validity <?=$lists['validity'];?> month</span>
                      </div>
                    </div>
                  </a>
                  </div>
                </div>
            </div>
            </div>     
        <?php } } ?>
          </div>
      </div>
<script type="text/javascript">
  $('.cate').click(function(){
    var main_id = $('#feed').children("option:selected").val();
    var category = $('.a li').find("a.active").attr('value');
    var cat_id = $(this).find('a').attr('id');
    $.ajax({
        data:{id:main_id,category:category,cat_id:cat_id},
        type:'post',
        url:'<?php echo site_url();?>/web/Exam_Prep/ajax_dropdown',
       beforeSend: function () {
            $('.bottom_loader').css({"display":"block"});
        },
        success:function(data){
          $('#filter_data').html('');
          $('#filter_data').html(data);
          $('.tabs-li li.active').removeClass('active');
          $('#'+cat_id).addClass('active');
          $('.bottom_loader').css({"display":"none"});
        },error:function(err){
          alert(err);
          console.log(err);
        }
    });
});
</script>
</script>