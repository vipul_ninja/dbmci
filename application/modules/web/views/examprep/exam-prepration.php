<!DOCTYPE html>
<html>
<head>
	<title>My Courses</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/Bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
    <link rel="icon" href="http://admin.dbmci.com/uploads/1933.jpg" type="image/png" sizes="16x16">
  <!-- Main Styles CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/css/main.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/css/ibt_main.css">
</head> 
<body>
<div class="main-box">
  <?php $this->load->view('../include/header');?>  
  <div class="header-spacer"></div>
  <div class="container ">
    <div class="container" style="padding: 30px;">
      <div class="row">
        <div class="col col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 filter-by-feeds" style="margin-bottom: 10px;">
          <div class="w-select">
            <span class="title" style="color: black;font-weight: bold;font-size: 15px;">Filter By:</span>
              <select name="feed" id="feed"  class="select-content">
                <?php if(!empty($category_details)){ 
                  $i =0;
                  foreach ($category_details['data']['main_category'] as $cate) {
                    if($i ==0){
                      $parent_id = $cate['id'];
                    }?>
                <option class="val" value="<?php echo $cate['id'];?>"><?php echo $cate['text'];?></option>  
                <?php $i++; } }?>
              </select>
          </div>
        </div>
      </div>
    </div>  
  </div>
</div>
<div class="header-spacer"></div>
<?php  $this->load->view('../include/footer'); ?>
</body>
</html>