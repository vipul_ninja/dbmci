
<?php  
  $social_auth = social_login_authUrl();
  $google_authUrl = $social_auth['google_authUrl'];
  $facebook_authUrl = $social_auth['facebook_authUrl'];
?>
<style type="text/css">
.badge-notify{
  background: red;
  position: relative;
  top: -12px;
  left: -12px;
  color: white;
  font-size: 12px;
}
.goog-te-banner-frame.skiptranslate {
    display: none !important;
    } 
body {
    top: 0px !important; 
    }
    .btn1:hover{
      background-color: #FDDDCF !important;
    }
    .btn2:hover{
      background-color: #C4EEF5 !important;
    }
    /* CSS used here will be applied after bootstrap.css */
.notification-list li { 
     border-bottom: none; 
    display: block;
    position: relative;
    transition: all .3s ease; 
    padding: 4px 10px;
}

.glyphicon-bell {
   
    font-size:1.5rem;
  }

.notifications {
   min-width:420px; 
  }
  
  .notifications-wrapper {
    overflow: auto;
    max-height: 250px;
    width: 100%;
}
    
 .menu-title {
     color:#3aa4ff;
     font-size:17px;
      display:inline-block;
      font-weight: 500;
      float: right;
      }
      .menu-title1 {
     color:black;
     font-size:17px;
      display:inline-block;
      font-weight: 500
      }
 
.glyphicon-circle-arrow-right {
      margin-left:10px;     
   }
  
   
 .notification-heading, .notification-footer  {
  padding:2px 10px;
       }
      
        
.dropdown-menu.divider {
  margin:5px 0;          
  }

.item-title {
  
 font-size:1.3rem;
 color:#000;
    
}

.notifications a.content {
 text-decoration:none;
 background:#ccc;

 }
    
.notification-item {
 /*padding:10px;*/
 margin:5px;
 /*background:#ccc;*/
 border-radius:4px;
 }
 .notification-list li{
  background-color: #d3def2
 }




</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $.ajax({
      type: 'POST',
      url: '<?php echo site_url('/web/Header/notification_count'); ?>',
      success: function (data) {
          $('#noti_count').html(data);
      },
      error: function (err) {
          console.log("ERR" + err);
      },
  });
  $.ajax({
      type: 'POST',
      url: '<?php echo site_url('/web/Header/header_notifications'); ?>',
      success: function (data) {
          $('#notification').html(data);
      },
      error: function (err) {
          console.log("ERR" + err);
      },
  });
});


</script>
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Page 1-1</a></li>
            <li><a href="#">Page 1-2</a></li>
            <li><a href="#">Page 1-3</a></li>
          </ul>
        </li>
        <li><a href="#">Page 2</a></li>
        <li><a href="#">Page 3</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
  <header class="main-header-i fixed-top">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="Navbar">
            <nav class="Navbar__Items">
              <?php if(!$this->session->userdata('username')){ ?>
                <div class="Navbar__Link Navbar__Link-brand"> <a href="<?php echo site_url('web/Home'); ?>"><img src="<?php echo base_url(); ?>/web_assets/img/logo.png" alt="" ></a> </div>
              <?php }else{ ?>
                 <div class="Navbar__Link Navbar__Link-brand"> <a href="<?php echo site_url('web/Feeds'); ?>"><img src="<?php echo base_url(); ?>/web_assets/img/logo.png" alt="" ></a> </div>
           <?php  } ?>
             
            </nav>
            <nav class="Navbar__Items Navbar__Items--right">
              <?php if(!$this->session->userdata('username')){ ?>
               
              <div class="Navbar__Link"> <a href="<?php echo site_url(); ?>/web/Exam_Prep/"><img src="<?php echo base_url(); ?>/web_assets/img/test.png" alt="">
                <p>Test Series</p>
                </a> </div>
              <div class="Navbar__Link"> <a href="<?php echo site_url(); ?>/web/Exam_Prep"><img src="<?php echo base_url(); ?>/web_assets/img/video_course.png" alt="">
                <p>Video course</p>
                </a> </div>
                <div class="Navbar__Link"> <a href="<?php echo site_url(); ?>/web/DailyDose"><img src="<?php echo base_url(); ?>/web_assets/img/daily_dose_blue.png" alt="">
                <p>Daily Dose</p>
                </a> </div>
              <div class="Navbar__Link"> <a href="<?php echo site_url(); ?>/web/Study"><img src="<?php echo base_url(); ?>/web_assets/img/store.png" alt="">
                <p>Study</p>
                </a> </div>
              <?php }else{ ?>
                 <div class="Navbar__Link">
                <div class="main_search_bar"> 
                  <!-- Actual search box -->
                  <div class="form-group has-search"> <span class="fa fa-search form-control-feedback"></span>
                    <input type="text" class="form-control"  id="searching" main="search" placeholder="Search">
                  </div>
                </div>
              </div>
                    <div class="Navbar__Link"> 
                      <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal">
                        <img src="<?php echo base_url(); ?>/web_assets/img/language.png" alt="">
                        <p>Select Language</p>
                      </a> 
                    </div>
                    
                    <!-- <li style="display:none"><div id="google_translate_element" ></div></li> -->
                    <div class="Navbar__Link"> 
                      <a href="javascript:void(0);"  data-toggle="modal" data-target="#update-header-photo">
                        <img src="<?php echo base_url(); ?>/web_assets/img/dougths.png" alt="">
                        <p>Doubts/Query</p>
                      </a> 
                    </div>
                    <div class="Navbar__Link"> 
                      <!-- <a href="javascript:void(0);" data-toggle="modal" data-target="#notification">
                        <img src="<?php echo base_url(); ?>/web_assets/img/notification.png" alt="">
                        <span class="badge badge-notify" id="noti_count"></span>
                        <p>Notification</p>
                      </a>  -->
                      <div class="dropdown">
                         <a href="javascript:void(0);" data-target="#" data-toggle="dropdown" >
                                              <img src="<?php echo base_url(); ?>/web_assets/img/notification.png" alt="">
                                              <span class="badge badge-notify" id="noti_count"></span>
                                              <p>Notification</p>
                                            </a> 
                        
                        <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel" style="    height: 500px;
    overflow-y: auto;
    overflow-x: hidden;">
                          
                          <div class="notification-heading"><h3 class="menu-title1">Notifications</h3><a href="http://13.232.24.221/index.php/web/Header/notifications"> 
                            <h3 class="menu-title pull-right">View all<i class="glyphicon glyphicon-circle-arrow-right"></i></h3></a>
                          </div>
                          <li class="divider"></li>
                          <div id="notification"></div>

                          <li class="divider"></li> 
                        </ul>
                        
                      </div>
                    </div>
                    <div class="Navbar__Link"> 
                      <a href="<?php echo site_url('web/Header/my_notes');?>" >
                        <img src="<?php echo base_url(); ?>/web_assets/img/my_notes.png" alt="">
                        <p>My Notes</p>
                      </a> 
                    </div>
              <?php } ?>
               <div class="Navbar__Link">
                   
           <?php 
           if($this->session->userdata('username')){?> 
                 <!--  <a href="<?php //echo site_url();?>/web/Login_register/logout" class="btn btn-lg btn-primary full-width btn-i chck_sessions" id="gradient">
                Logout
                </a>  -->
            <?php  }else{ ?> 
                  <a href="javascript:void(0)" class="btn btn-lg btn-primary full-width btn-i chck_sessions" id="gradient1" data-toggle="modal" data-target="#registration-login-form-popup">
                 Login/Register
                </a> 
         <?php     } ?>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </div>
   <!-- Header Standard -->

  </header>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script type="text/javascript">

        function googleTranslateElementInit() {
            new google.translate.TranslateElement('google_translate_element');
        }

        function changeLanguageByButtonOneClick() {
            var language = document.getElementById("language").getAttribute('value');
            var selectField = document.querySelector("#google_translate_element select");
            for (var i = 0; i < selectField.children.length; i++) {
                var option = selectField.children[i];
                if (option.value == language) {
                    selectField.selectedIndex = i;
                    selectField.dispatchEvent(new Event('change'));
                    break;
                }
            }
            $("#myModal").modal("hide");
        }

        function changeLanguageByButtonClick() {
            var language1 = document.getElementById("language_english").getAttribute('value');
            var selectField1 = document.querySelector("#google_translate_element select");
            for (var j = 0; j < selectField1.children.length; j++) {
                var option1 = selectField1.children[j];
                if (option1.value == language1) {
                    selectField1.selectedIndex = j;
                    selectField1.dispatchEvent(new Event('change'));
                    break;
                }
            }
            $("#myModal").modal("hide");
        }
</script>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content"> 
<div class="modal-body">
  <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button> 
  <h3>Choose your Language</h3> <br>
  <center>    

    <button onclick="changeLanguageByButtonClick()" value="en" id="language_english" class="btn btn1 btn-light-blue btn-lg btn-block mb-3 waves-effect waves-light" style="cursor: pointer;background-color: #00ace6;"><img src="http://13.232.24.221//web_assets/img/04.png" style="width: 14%"> ENGLISH</button>
    <button onclick="changeLanguageByButtonOneClick()" value="hi" id="language" class="btn btn2 btn-light-blue btn-lg btn-block mb-3 waves-effect waves-light" style="cursor: pointer;background-color: #2BBBAD;"><img src="http://13.232.24.221//web_assets/img/05.png" style="width: 14%">HINDI</button>
  </center>
  <div style="display:none" id="google_translate_element"></div>    
</div>

</div>
</div>
</div>

