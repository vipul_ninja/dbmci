        <footer>
            <?php if($this->session->userdata('username')){ ?>
                <section class="fix-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bottom-fix-bg" id="menu" style="">
                                    <ul class="list-inline">
                                        <li class="list-inline-item <?php if(!empty($active)){ if($active == "Feeds"){echo "active";}else{ echo "";} }?>">
                                            <a href="<?php echo site_url(); ?>/web/Feeds">
                                                <img src="<?php echo base_url();?>/web_assets/img/feed.png" alt="">
                                                <p>All Courses</p>
                                            </a>
                                        </li>
                                        <li class="list-inline-item <?php if(!empty($active)){ if($active  == 'ExamPrep'){echo "active";}else{ echo "";} }?>">
                                            <a href="<?php echo site_url(); ?>/web/Exam_Prep/">
                                                <img src="<?php echo base_url();?>/web_assets/img/exam_pre.png" alt=""><p>My Courses</p>
                                            </a>
                                        </li>
                                    </ul>  
                                </div> 
                            </div> 
                        </div>
                    </div>
                </section>
            <?php } ?>
            <a class="back-to-top" href="#"> <img src="<?php echo base_url(); ?>/web_assets/svg-icons/back-to-top.svg" alt="arrow" class="back-icon"> </a>
        </footer>
        <!-- JS Scripts --> 
        <script src="<?php echo base_url(); ?>/web_assets/js/jquery-3.2.1.js"></script> 
        <script src="<?php echo base_url(); ?>/web_assets/js/base-init.js"></script>  
        <script src="<?php echo base_url(); ?>/web_assets/js/svgxuse.js"></script>
        <script src="https://www.w3schools.com/lib/w3.js"></script>
        <script src="<?php echo base_url(); ?>/web_assets/Bootstrap/dist/js/bootstrap.bundle.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script> -->

    </body>
</html>