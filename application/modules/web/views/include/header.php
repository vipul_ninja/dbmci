
<!DOCTYPE html>
  <html lang="en">
    <head>
      <title>All Courses</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/Bootstrap/dist/css/bootstrap-reboot.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/Bootstrap/dist/css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/Bootstrap/dist/css/bootstrap-grid.css">
      <link rel="icon" href="http://admin.dbmci.com/uploads/1933.jpg" type="image/png" sizes="16x16">
      <!-- Main Styles CSS -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/css/main.min.css">
      <!-- <link rel="stylesheet" type="text/css" href="/web_assets/css/fonts.min.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/css/ibt_main.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/css/owl.carousel.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/css/linearicons.css"> -->

      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/web_assets/css/dmci.css">
      <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css"> -->
      <!-- <script src="<?php echo base_url(); ?>/web_assets/js/jquery-3.2.1.js"></script>  -->
      <script src="<?php echo base_url(); ?>/js/dmci.js"></script>

    </head>

    <body>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color bg-bgdmci">
      <!-- Navbar brand -->
      <!-- <a class="navbar-brand" href="#">Navbar</a> -->

      <!-- Collapse button -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
        aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Collapsible content -->
      <div class="collapse navbar-collapse" id="basicExampleNav">

        <!-- Links -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <?php if(!$this->session->userdata('username')){ ?>
            <div class="Navbar__Link Navbar__Link-brand"> <a href="<?php echo site_url('/web/Home'); ?>"><img src="http://admin.dbmci.com/uploads/1933.jpg" alt="" style="width: 80%;"></a> </div>
            <?php }else{ ?>
              <div class="Navbar__Link Navbar__Link-brand"> <a href="<?php echo site_url('/web/Feeds'); ?>"><img src="http://admin.dbmci.com/uploads/1933.jpg" alt="" style="width: 80%;"></a> </div>
            <?php  } ?>
          </li>          
        </ul>
        <!-- Links -->
      <form class="form-inline">
        <div class="md-form my-0">
          <h5>User Name</h5>
        </div>
        <div class="md-form my-0">
          <div class="Navbar__Link Navbar__Items--right">
            <a href="<?php echo site_url();?>/web/login/logout" class="btn btn-lg btn-primary full-width btn-i chck_sessions" id="gradient">Logout</a> 
          </div>
        </div>
      </form>
    </div>
    <!-- Collapsible content -->
  </nav>
<!--/.Navbar-->
        
<!--/.Navbar-->
      <!-- <header class="main-header-i fixed-top">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="Navbar">
                <nav class="Navbar__Items">
                  <?php if(!$this->session->userdata('username')){ ?>
                    <div class="Navbar__Link Navbar__Link-brand"> <a href="<?php echo site_url('/web/Home'); ?>"><img src="http://admin.dbmci.com/uploads/1933.jpg" alt="" style="width: 80%;"></a> </div>
                  <?php }else{ ?>
                    <div class="Navbar__Link Navbar__Link-brand"> <a href="<?php echo site_url('/web/Feeds'); ?>"><img src="http://admin.dbmci.com/uploads/1933.jpg" alt="" style="width: 80%;"></a> </div>
                  <?php  } ?>
                  <i class="fa fa-bars custom-fa-fa"></i>
                </nav>
                <nav class="Navbar__Items Navbar__Items--right">
                  <div class="Navbar__Link">                      
                    <?php if($this->session->userdata('username')){?> 
                      <a href="<?php echo site_url();?>/web/login/logout" class="btn btn-lg btn-primary full-width btn-i chck_sessions" id="gradient">Logout</a> 
                    <?php  } ?> 
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </header> -->

      <!-- <script>
        $(".custom-fa-fa").click(function(){ 
          $(".Navbar__Items--right").slideToggle(500);
          $('.Navbar__Items').attr("style", "display: block !important");
      });
      </script> -->
