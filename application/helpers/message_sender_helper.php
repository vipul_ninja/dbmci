<?php
if (!function_exists('send_message_global')) {
    function send_message_global($c_code,$mobile,$message) { //mobile and message.   
        $mobile = $c_code.$mobile;
        require_once(APPPATH . 'third_party/twilio-php/Services/Twilio.php');
        $account_sid = twilio_account_sid;
        $auth_token = twilio_auth_token;
        $client = new Services_Twilio($account_sid, $auth_token); //echo  $mobile;
		$result = array();
		try {
			$result = $client->account->messages->create(
                            array(
                                'To' => $mobile,
                                'From' => "+15413938090",
                                'Body' => $message
                            )
                        );
		} catch (Services_Twilio_RestException $e) {
			return false;
		}

		if (count($result) > 0 && $result->sid != '') {
			return true;
		} else {
			return false;
		}
    }

}
