<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('pre')) {

    function pre($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }

}

function sorting_with_multi_array($a, $b) {
    return strcmp($a["discount"], $b["discount"]);
}

if (!function_exists('is_json')) {

    function is_json($string, $return_data = false) {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }

}

if (!function_exists('check_inputs')) {

    function check_inputs() {
        //pre($this->input->post());
        $json = file_get_contents('php://input');
        if ($json) {
            return ((array) json_decode($json));
        } else if ($_POST) {
            //echo'dddd'.pre($_POST);
            return $_POST;
        } else {
            //pre($_POST);
            echo json_encode(array("status" => false, "message" => "Invalid Input", "Result" => array()));
        }
    }

}

if (!function_exists('create_log_file')) {

    function create_log_file($data) { //mobile and message.  
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . base_url("index.php/data_model/") . $data['url'] . PHP_EOL;
        $log = $log . "Request " . json_encode($data['request']) . PHP_EOL . PHP_EOL;
        file_put_contents('logs/' . $data['api'] . '.txt', $log, FILE_APPEND);
    }

}

if (!function_exists('return_data')) {

    function return_data($status = false, $mesage = "", $result) { //mobile and message. 
        echo json_encode(array("status" => $status, "message" => $mesage, "Result" => $result));
        die;
    }

}

if (!function_exists('send_sms')) {

    function send_sms($document) {
        return file_get_contents("http://43.242.214.11:8787/mobicomm//submitsms.jsp?user=ANUPAM&key=bf22da64eeXX&mobile=" . $document['mobile'] . "&message=" . $document['message'] . "&senderid=INTSOL&accusage=1");
        //return json_decode($get_data);
//        $service_url = "http://43.242.214.11:8787/mobicomm//submitsms.jsp?user=ANUPAM&key=bf22da64eeXX&mobile=" . $document['mobile'] . "&message=" . $document['message'] . "&senderid=INTSOL&accusage=1";
//        $curl = curl_init($service_url);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        $curl_response = curl_exec($curl);
//        if ($curl_response === false) {
//            $info = curl_getinfo($curl);
//            curl_close($curl);
//            die('error occured during curl exec. Additioanl info: ' . var_export($info));
//        }
//        curl_close($curl);
//        $decoded = json_decode($curl_response);
//        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
//            die('error occured: ' . $decoded->response->errormessage);
//        }
//        return $curl_response;
    }

}

if (!function_exists('file_curl_contents')) {

    function file_curl_contents($document) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $document['file_url']);
        curl_setopt($ch, CURLOPT_POST, 1);
        unset($document['file_url']);
        //pre($document); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $document);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //pre($server_output); 
        curl_close($ch);
        $data = json_decode($server_output, true);
        //pre($data); 
        return $data;
    }

}


if (!function_exists('send_mail')) {

    function send_mail($details) {
        $to_email = $details['mail'];
        $subject = 'Activate Account';
        $message = 'You are successfully registered.. Now you can activate your account ' . $details['link'];
        $headers = 'From: noreply@accounting_anywhere.com';
        mail($to_email, $subject, $message, $headers);
        return true;
    }

}

function compress($source, $destination, $quality) {
    $info = getimagesize($source);
    if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($source);
    elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($source);
    elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($source);
    imagejpeg($image, $destination, $quality);
    return $destination;
}
