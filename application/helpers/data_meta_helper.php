<?php

function get_db_meta_key($key){
    if($key == ""){
       return ""; 
    }else{
        // get value in db 
        $CI = & get_instance();
        $query = "SELECT meta_value
                    FROM meta_information 
                    WHERE meta_name = '$key' "; 
        if(count($r = $CI->db->query($query)->row()) > 0 ){
            return $r->meta_value; 
        }else{
            return ""; 
        }
    }
}

function set_db_meta_key($key,$value){
    $CI = & get_instance();
    if($key == "" ){
        return false ; 
    }else{
        $query = "SELECT meta_value
        FROM meta_information 
        WHERE meta_name = '$key' "; 

        if(count($CI->db->query($query)->row()) > 0 ){
            $data = array("meta_value"=>$value);
            $CI->db->where('meta_name',$key);
            $CI->db->update('meta_information',$data);
        }else{
            $data = array("meta_value"=>$value,"meta_name"=>$key);
            $CI->db->insert('meta_information',$data);        
        }
    }
}