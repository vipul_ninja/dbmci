<?php
use Aws\Ses\SesClient;
use Aws\Ses\Exception\SesException;
class AWS_emailer {

    public function __construct() {

    }
    function send_aws_email($input){
             /* sample input
             $input =  array(
                "SENDER"=> "abc@gmail.com",
                "RECIPIENT"=> "abc@gmail.com" ,
                "SUBJECT"=> "subject email " ,
                "HTMLBODY"=> "<h1>abc</h1>" , 
                "TEXTBODY"=> "about this email "
             );
             */
            // Replace path_to_sdk_inclusion with the path to the SDK as described in 
            // http://docs.aws.amazon.com/aws-sdk-php/v3/guide/getting-started/basic-usage.html
            define('REQUIRED_FILE',FCPATH.'aws/aws-autoloader.php'); 
        
            // Replace sender@example.com with your "From" address. 
            // This address must be verified with Amazon SES.
            define('SENDER', $input['SENDER']);           
        
            // Replace recipient@example.com with a "To" address. If your account 
            // is still in the sandbox, this address must be verified.
            define('RECIPIENT', $input['RECIPIENT']);    
        
            // Specify a configuration set. If you do not want to use a configuration
            // set, comment the following variable, and the 
            // 'ConfigurationSetName' => CONFIGSET argument below.
            define('CONFIGSET','emailer');
        
            // Replace us-west-2 with the AWS Region you're using for Amazon SES.
            define('REGION','us-west-2'); 
        
            define('SUBJECT', $input['SUBJECT']);    
        
            define('HTMLBODY', $input['HTMLBODY']);
            define('TEXTBODY',$input['TEXTBODY']);
        
            define('CHARSET','UTF-8');
        
            require REQUIRED_FILE;         
            $client = SesClient::factory(array(
                                                'version'=> 'latest',     
                                                'region' => REGION ,
                                                'credentials' => [        
                                                'key'    => 'AKIAJOX5DIUSM6P6AIYA',
                                                'secret' => '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',  
                                                ],
                                                ));
        
            try {
                $result = $client->sendEmail([
                                            'Destination' => [
                                                            'ToAddresses' => [
                                                            RECIPIENT,
                                                            ],
                                            ],
                                            'Message' => [
                                                        'Body' => [
                                                        'Html' => [
                                                        'Charset' => CHARSET,
                                                        'Data' => HTMLBODY,
                                                        ],
                                                        'Text' => [
                                                        'Charset' => CHARSET,
                                                        'Data' => TEXTBODY,
                                                        ],
                                                        ],
                                                        'Subject' => [
                                                        'Charset' => CHARSET,
                                                        'Data' => SUBJECT,
                                                        ],
                                            ],
                                            'Source' => SENDER,
                                            // If you are not using a configuration set, comment or delete the
                                            // following line
                                            'ConfigurationSetName' => CONFIGSET,
                                            ]);
                $messageId = $result->get('MessageId');
                return ("Email sent! Message ID: $messageId"."\n");
            } catch (SesException $error) {
                return ("The email was not sent. Error message: ".$error->getAwsErrorMessage()."\n");
            }
        }

}