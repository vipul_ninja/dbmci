<?php
require('Fpdf/Fpdf.php');

class Fpdf_course_transaction extends FPDF{

  function Header(){
    if ($this->page == 1){
        $this->Image('http://18.223.244.127/website_panel_assets/images/logo.png',10,6,20);		  
        $this->SetFont('Arial','B',20);
        //Move to the right
        $this->Cell(100);//margin left         
        $this->Cell(50,0,'MME',0,90,'C');   //width,margin top,text, 0-nosquare 1-square,center(C)
        $this->SetFont('Arial','B',12);


        $this->Cell(25);//margin left         
        $this->Cell(5,25,'List of Transactions',0,0,'C');    //width,margin top,text, 0-nosquare 1-square,margin-bottom,center(C)
        $this->SetFont('Arial','B',9);


        //Date
        date_default_timezone_set('Asia/Kolkata');
        $currentdate = "Date: ".date("m-d-Y");  
        $this->Cell(20);
        $this->Cell(0,10,$currentdate,0,0,'R');


        $this->Cell(50);
        $this->SetFont('Arial','B',15);
        $this->Cell(0,10,'List of Users',0,0,'l');
        //Line break
        $this->Ln(20);
    }
      if($this->page>1){

       $this->set_table_headers();
      }
  }
  function Footer(){
    // Go to 1.5 cm from bottom
    $this->SetY(-10);
    // Select Arial italic 8
    $this->SetFont('Arial','I',8);
    // Print centered page number
    $this->Cell(0,0,'Page '.$this->PageNo(),0,0,'C');
  
}
  function set_table_headers(){


    //$header = array('S.no','Name','Email','Mobile','Is_dams','Dams id','Moderater','Status','Creation time');
	 $header = array('S.no','Name','Course name','time','Price','Gst','Net Amt','Ins Share','Profit');
    // Colors, line width and bold font
    $this->SetFont('Arial','B',10); //table heading
    $this->SetFillColor(49,179,238);
    $this->SetTextColor(255);
    $this->SetDrawColor(0,0,0);
    $this->SetLineWidth(.2);
    // $this->SetFont('','');

    //Header
    //$w = array(15,45,55,30,20,30,20,20,35);
    $w = array(15,45,90,30,15,15,17,17,15);
    for($i=0;$i<count($header);$i++)
    $this->Cell($w[$i],6,$header[$i],1,0,'C',true);                  //7 colmn height increse
    $this->Ln();
  }

  function FancyTable($data){
      $this->set_table_headers();
      $w = array(15,45,90,30,15,15,17,17,15);
      // Color and font restoration
      $this->SetFillColor(224,235,255);
      $this->SetTextColor(0);
      //$this->SetFont('');
      $this->SetFont('Arial','',8); 
      // Data
      $fill = false;
      foreach($data as $row){
        $this->Cell($w[0],4.5,$row[0],'LR',0,'L',$fill);
        $this->Cell($w[1],4.5,$row[1],'LR',0,'L',$fill);
		    $this->Cell($w[2],4.5,$row[2],'LR',0,'L',$fill);
        $this->Cell($w[3],4.5,$row[8],'LR',0,'L',$fill);        
        $this->Cell($w[4],4.5,$row[10],'LR',0,'L',$fill);
        $this->Cell($w[5],4.5,$row[11],'LR',0,'L',$fill);
        $this->Cell($w[6],4.5,$row[12],'LR',0,'L',$fill);
        $this->Cell($w[7],4.5,$row[13],'LR',0,'L',$fill);
        $this->Cell($w[8],4.5,$row[14],'LR',0,'L',$fill);
        $this->Ln();
        $fill = !$fill;
      }
      // Closing line
      $this->Cell(array_sum($w),0,'','T');
  }

   function pdf_out($data){
    $this->SetFont('Arial','',8);
    $this->Header();
    $this->AddPage('L');

    $this->FancyTable($data);
    $this->Output();
   }

}

