<?php
/**
* System messages translation for CodeIgniter(tm)
*
* @author	CodeIgniter community
* @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
* @license	http://opensource.org/licenses/MIT MIT License
* @link	https://codeigniter.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');

/* report abuse */
$lang['report_abuse_done'] = 'Thank you for reporting. Our team will review it & take the required action.';
$lang['report_abuse_already'] = 'This post has already been marked as abuse by you.';

$lang['post_added_to_bookmarks'] = 'Post added to My Notes.';
$lang['concept_added_to_bookmarks'] = 'Concept added to My Notes.';

$lang['post_removed_from_bookmarks'] = 'Post removed from My Notes.';
$lang['concept_removed_from_bookmarks'] = 'Concept removed from My Notes.';

$lang['preference_updated'] = 'Preference updated successfully.';

$lang['test'] = 'Test';
$lang['tests'] = 'Tests';
$lang['video'] = 'Video';
$lang['videos'] = 'Videos';
$lang['book'] = 'Book';
$lang['books'] = 'Books';
$lang['concept'] = 'Concept';
$lang['concepts'] = 'Concepts';