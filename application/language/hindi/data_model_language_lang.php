<?php
/**
* System messages translation for CodeIgniter(tm)
*
* @author	CodeIgniter community
* @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
* @license	http://opensource.org/licenses/MIT MIT License
* @link	https://codeigniter.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');

/* report abuse */
$lang['report_abuse_done'] = 'रिपोर्टिंग के लिए धन्यवाद। हमारी टीम इसकी समीक्षा करेगी और आवश्यक कार्रवाई करेगी।';
$lang['report_abuse_already'] = 'यह पोस्ट आपके द्वारा पहले से दुरुपयोग के रूप में चिह्नित किया गया है।';

$lang['post_added_to_bookmarks'] = 'पोस्ट मेरे नोट्स में जोड़ा गया।';
$lang['concept_added_to_bookmarks'] = 'संकल्पना मेरे नोट्स में जोड़ा गया।';

$lang['post_removed_from_bookmarks'] = 'पोस्ट मेरी नोट्स से हटा दिया गया।';
$lang['concept_removed_from_bookmarks'] = 'संकल्पना मेरे नोट्स से हटा दी गई।';

$lang['preference_updated'] = 'वरीयता सफलतापूर्वक अपडेट की गई।';

$lang['test'] = 'परीक्षा';
$lang['tests'] = 'परीक्षा';
$lang['video'] = 'वीडियो';
$lang['videos'] = 'वीडियो';
$lang['book'] = 'किताब';
$lang['books'] = 'पुस्तकें';
$lang['concept'] = 'संकल्पना';
$lang['concepts'] = 'संकल्पना';