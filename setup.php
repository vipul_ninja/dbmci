<?php
include('application/config/config.ini.php');

if (is_writable('application/config')){
  $dir_permission= '';
} else {
    $dir_permission= '* Config folder is not  writable, please give 777  permission to config folder in application/config/ ';
}
$php_required_version = 7;
//error_reporting(0);
$phpversion        = explode('.',phpversion());
$installation_status = $mysql_error =  $php_v_error = "";
if($_POST){

	$config_base_url            =    $_POST['base_url'];
	$config_project_full_name   =	   $_POST['project_full_name'];
	$config_project_nick_name   =	   $_POST['project_nick_name'];
	$config_host_name           =	   $_POST['host_name'];
	$config_db_name             =	   $_POST['db_name'];
	$config_user_name           =	   $_POST['user_name'];
	$config_db_password         =	   $_POST['db_password'];
	$config_aws_buckect_name    =	   $_POST['aws_buckect_name'];
	$config_aws_s3_key          =	   $_POST['aws_s3_key'];
	$config_aws_secret_key      =	   $_POST['aws_secret_key'];
	$config_aws_s3_region       =	   $_POST['aws_s3_region'];
	$config_aws_emailer_key     =	   $_POST['aws_emailer_key'];
	$config_aws_emailer_region  =	   $_POST['aws_emailer_region'];
	$config_gsm_key             =	   $_POST['gsm_key'];

	$con = mysqli_connect($config_host_name,$config_user_name,$config_db_password,$config_db_name);
	// Check connection
	if (mysqli_connect_errno()){
		$mysql_error    =  "Wrong MySql Credentials !!";
  }
  if($phpversion[0] >= $php_required_version
      && mysqli_connect_errno() != true
      && (extension_loaded('gd') && function_exists('gd_info'))
	){
			$myfile = fopen("application/config/config.ini.php", "w") or die("Unable to open file!");
			$txt = "<?php \n";

			$txt .= '$config_installation        =    "1";'."\n";
			$txt .= '$config_base_url            =    "'. $_POST['base_url'].'";'."\n";
			$txt .= '$config_project_full_name   =    "'. $_POST['project_full_name'].'";'."\n";
			$txt .= '$config_project_nick_name   =    "'. $_POST['project_nick_name'].'";'."\n";
			$txt .= '$config_host_name           =    "'. $_POST['host_name'].'";'."\n";
			$txt .= '$config_db_name             =    "'. $_POST['db_name'].'";'."\n";
			$txt .= '$config_user_name           =    "'. $_POST['user_name'].'";'."\n";
			$txt .= '$config_db_password         =    "'. $_POST['db_password'].'";'."\n";
			$txt .= '$config_aws_buckect_name    =    "'. $_POST['aws_buckect_name'].'";'."\n";
			$txt .= '$config_aws_s3_key          =    "'. $_POST['aws_s3_key'].'";'."\n";
			$txt .= '$config_aws_secret_key      =    "'. $_POST['aws_secret_key'].'";'."\n";
			$txt .= '$config_aws_s3_region       =    "'. $_POST['aws_s3_region'].'";'."\n";
			$txt .= '$config_aws_emailer_key     =    "'. $_POST['aws_emailer_key'].'";'."\n";
			$txt .= '$config_aws_emailer_region  =    "'. $_POST['aws_emailer_region'].'";'."\n";
			$txt .= '$config_gsm_key             =    "'. $_POST['gsm_key'].'";'."\n";

			$add_details = fwrite($myfile, $txt);
			$txt = "?>";
			fwrite($myfile, $txt);
			fclose($myfile);
			if($add_details){
				$installation_status = "Setup Completed.";
			}
		}else{ }
}

if($phpversion[0] < $php_required_version){
	$php_v_error ="Not Compatible php version. $php_required_version + reqd. ";
}


if (extension_loaded('gd') && function_exists('gd_info')) {
  $gd_check= "PHP GD library is installed on your web server";
}else{
  $gd_check_error= "<p style='color:red;'>PHP GD library is NOT installed on your web server</p>";
}
?>

<!DOCTYPE HTML>
<HTML>
    <head>
        <title>Installation Setup</title>
       <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
       <div class="container">
      <div class="text-center">
       <!--  <img class="d-block mx-auto mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"> -->
			 	<i class="fa fa-cog fa-4x fa-spin" aria-hidden="true"></i>
				<i class="fa fa-cog fa-2x fa-spin" aria-hidden="true"></i>
				<i class="fa fa-cog  fa-spin" aria-hidden="true"></i>
				<i class="fa fa-cog fa-2x fa-spin" aria-hidden="true"></i>
				<i class="fa fa-cog fa-4x fa-spin" aria-hidden="true"></i>
        <h2><i aria-hidden="true" class="fa fa-wrench  fa-x "></i>Installation Setup</h2>

        <p class="lead">Following details must me filled in order to install the setup of the software</p>
				  <small class="text-danger"><?php echo $dir_permission; ?></small>
          <small class="text-success bold ">
            <?php
            if($installation_status != ''){
              echo $installation_status; ?>
                <a href="<?php echo $config_base_url;?>">Visit Site</a>
              <?php  } ?>
          </small>
      </div>




      <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Guidelines</span>
            <span class="badge badge-secondary badge-pill">3</span>
          </h4>
          <ul class="list-group mb-3">
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">PHP Version</h6>
                <small class="text-danger"><?php echo $php_v_error; ?></small>
              </div>
              <span class="text-muted"><?php echo  phpversion();?></span>
            </li>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">MySql</h6>
               <small class="text-danger"><?php echo $mysql_error; ?></small>
              </div>
              <span class="text-muted"></span>
            </li>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">GD Library</h6>
               <small class="text-danger"><?php echo $gd_check_error; ?></small>
              </div>
              <span class="text-muted"><?php if (extension_loaded('gd') && function_exists('gd_info')) {
    
   echo  $gd_check;
}
 ?></span>
            </li>
            <!-- <li class="list-group-item d-flex justify-content-between lh-condensed" >
              <div>
                <h6 class="my-0">Third item</h6>
                <small class="text-muted">Brief description</small>
              </div>
              <span class="text-muted">$5</span>
            </li>
            <li class="list-group-item d-flex justify-content-between bg-light" >
              <div class="text-success">
                <h6 class="my-0">Promo code</h6>
                <small>EXAMPLECODE</small>
              </div>
              <span class="text-success">-$5</span>
            </li>
            <li class="list-group-item d-flex justify-content-between" >
              <span>Total (USD)</span>
              <strong>$20</strong>
            </li> -->
          </ul>


        </div>
        <div class="col-md-8 order-md-1">
			 <form class="needs-validation" novalidate name="setup_form" action="" method="POST">
			<div class="mb-3 text-info">
			  <h4 class="mb-3">Base URL</h4>
              <input type="text" class="form-control" name="base_url" value="<?php if($config_base_url){ echo $config_base_url;}?>"  placeholder="Enter Base URL">
            </div>

			<h4 class="mb-3 text-info">Project Names</h4>
		     <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Project Full Name</label>
                <input type="text" class="form-control" name="project_full_name" placeholder="Enter project full name" value="<?php if($config_project_full_name){ echo $config_project_full_name;}?>" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Project Nick Name</label>
                <input type="text" class="form-control" name="project_nick_name" placeholder="Enter project nick name" value="<?php if($config_project_nick_name){ echo $config_project_nick_name;}?>" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>

          <h4 class="mb-3 text-info">Database Details</h4>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Host Name</label>
                <input type="text" class="form-control" name="host_name" placeholder="Enter hostname" value="<?php if($config_host_name){ echo $config_host_name;}?>" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Database Name</label>
                <input type="text" class="form-control" name="db_name" placeholder="Enter database name" value="<?php if($config_db_name){ echo $config_db_name;}?>" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
				<div class="col-md-6 mb-3">
                <label for="firstName">User Name</label>
                <input type="text" class="form-control" name="user_name" placeholder="Enter username" value="<?php if($config_user_name){ echo $config_user_name;}?>" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Password</label>
                <input type="text" class="form-control" name="db_password" placeholder="Enter password" value="<?php if($config_db_password){ echo $config_db_password;}?>" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>

		   <h4 class="mb-3 text-info">AWS Services Details</h4>
		     <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Bucket Name</label>
                <input type="text" class="form-control" name="aws_buckect_name" placeholder="Enter aws bucket name" value="<?php if($config_aws_buckect_name){ echo $config_aws_buckect_name;}?>" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">s3 Key</label>
                <input type="text" class="form-control" name="aws_s3_key" placeholder="Enter s3 key" value="<?php if($config_aws_s3_key){ echo $config_aws_s3_key;}?>" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
			  <div class="col-md-6 mb-3">
                <label for="lastName">Secret Key</label>
                <input type="text" class="form-control" name="aws_secret_key" placeholder="Enter secret key" value="<?php if($config_aws_secret_key){ echo $config_aws_secret_key;}?>" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
			  <div class="col-md-6 mb-3">
                <label for="lastName">s3 Region</label>
                <input type="text" class="form-control" name="aws_s3_region" placeholder="Enter s3 region" value="<?php if($config_aws_s3_region){ echo $config_aws_s3_region;}?>" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>

			 <h4 class="mb-3 text-info">AWS Emailer Details</h4>
		     <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Key</label>
                <input type="text" class="form-control" name="aws_emailer_key" placeholder="Enter aws emailer key " value="<?php if($config_aws_emailer_key){ echo $config_aws_emailer_key;}?>" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Secret Key</label>
                <input type="text" class="form-control" name="aws_secret_key" placeholder="Enter aws emailer secret key" value="<?php if($config_aws_secret_key){ echo $config_aws_secret_key;}?>" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
			  <div class="col-md-6 mb-3">
                <label for="lastName">Region</label>
                <input type="text" class="form-control" name="aws_emailer_region" placeholder="Enter aws emailer region" value="<?php if($config_aws_emailer_region){ echo $config_aws_emailer_region;}?>" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
             </div>
			  <div class="mb-3 text-info">
				  <h4 class="mb-3">Android GSM Key</h4>
				  <input type="text" class="form-control" name="gsm_key" value="<?php if($config_gsm_key){ echo $config_gsm_key;}?>"  placeholder="Enter android gsm key for push services">
			  </div>


            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block submit_details" type="submit">Submit Details</button>
          </form>
        </div>
      </div>

      <!-- <footer class="my-5 pt-5 hide  text-muted text-center text-small">
        <p class="mb-1">&copy; 2017-2018 Company Name</p>
        <ul class="hide list-inline">
          <li class="list-inline-item"><a href="#">Privacy</a></li>
          <li class="list-inline-item"><a href="#">Terms</a></li>
          <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
      </footer> -->
    </div>
    </body>
</HTML>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

</script>
